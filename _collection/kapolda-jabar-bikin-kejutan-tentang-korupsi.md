---
layout: collection
title: Kapolda Jabar bikin kejutan tentang korupsi
---

Berikut di bawah ini disajikan berita yang sangat menarik -- dan sangat penting !--, yang dimuat oleh harian Pikiran Rakyat (Bandung) edisi 10 Februari 2008, tentang tekad Kapolda Jabar Drs Susno Duadji dalam menangani pemberantasan korupsi. Tekadnya yang keras itu tercermin dalam ungkapannya yang berani, terus-terang, dan jelas tentang jahatnya korupsi yang sudah sejak lama merajalela di negeri kita. Mengingat bahwa pandangan-pandangan Kapolda Jabar ini merupakan sesuatu yang “agak lain” daripada pernyataan para “tokoh” lainnya di negeri kita, dan mengingat juga bahwa banyak kalangan perlu mengetahui tentang adanya fenomena yang memberikan harapan akan adanya perbaikan moral yang sudah sangat bobrok di negeri kita ini, maka berikut ini disiarkan kembali teks berita itu selengkapnya.

Bisa diharapkan bahwa suara Kapolda Jabar Susno Duadji akan merupakan “angin segar” bagi kehidupan negara yang sudah lama dipenuhi bau busuk karena kebejatan moral yang berupa korupsi (ingat, antara lain : kekayaan Suharto, anak-anak Suharto, para kroninya yang bikin ruwet BLBI, soal dana Bank Indonesia, korupsi di kalangan Mahkamah Agung, DPR, dan Kejaksaan Agung dll dll dll)

Jelas sekali bahwa negeri kita memerlukan pemimpin-pemimpin yang betul-betul mempunyai kemauan keras dan ketulusan hati untuk membongkar kejahatan-kejahatan di kalangan elite bangsa. Kita butuhkan banyak sosok-sosok seperti Kapolda Susno Duadji. Mudah-mudahan, apa-apa yang secara baik dilakukan di Jawa Barat ini akan bisa menjadi teladan bagi seluruh Indonesia.

A. Umar Said

· * *
·
Teks berita, yang aslinya berjudul “Jangan pernah setori saya” itu adalah sebagai berikut :

“RABU (30/1) lalu, Kapolda Jabar Irjen Pol. Drs. Susno Duadji, S.H., M.Sc.,
mengumpulkan seluruh perwira di Satuan Lalu Lintas mulai tingkat polres
hingga polda. Para perwira Satlantas itu datang ke Mapolda Jabar sejak pagi
karena diperintahkan demikian. Pertemuan itu baru dimulai pukul 16.00 WIB.

Dalam rapat itu, kapolda hanya berbicara tidak lebih dari 10 menit. Meski
dilontarkan dengan santai, tetapi isi perintahnya "galak" dan "menyentak".
Saking "galaknya", anggota Satlantas harus ditanya dua kali tentang kesiapan mereka menjalani
perintah tersebut.

Isi perintah itu ialah tidak ada lagi pungli di Satlantas, baik di lapangan
(tilang) maupun di kantor (pelayanan SIM, STNK, BPKB, dan lainnya). "Tidak
perlu ada lagi setoran-setoran. Tidak perlu ingin kaya. Dari gaji sudah
cukup. Kalau ingin kaya jangan jadi polisi, tetapi pengusaha. Ingat, kita
ini pelayan masyarakat. Bukan sebaliknya, malah ingin dilayani," tutur pria
kelahiran Pagaralam, Sumatera Selatan itu.

Pada akhir acara, seluruh perwira Satlantas yang hadir, mulai dari pangkat
AKP hingga Kombespol, diminta menandatangani pakta kesepakatan bersama. Isi
kesepakatan itu pada intinya ialah meningkatkan pelayanan kepada masyarakat
yang tepat waktu, tepat mutu, dan tepat biaya.
Susno memberi waktu tujuh hari bagi anggotanya untuk berbenah, menyiapkan,
dan membersihkan diri dari pungli. "Kalau minggu depan masih ada yang nakal, saatnya main
copot-copotan jabatan," kata suami dari Ny. Herawati itu.

Pernyataan Susno itu menyiratkan, selama ini ada praktik pungli di
lingkungan kepolisian. Hasil pungli, secara terorganisasi, mengalir ke
pimpinan teratas. Genderang perang melawan pungli yang ditabuh Susno tidak
lepas dari perjalanan hidupnya sejak lahir hingga menjabat Wakil Kepala
PPATK (Pusat Pelaporan Analisis dan Transaksi Keuangan). PPATK adalah sebuah
lembaga yang bekerja sama dengan KPK (Komisi Pemberantasan Korupsi)
menggiring para koruptor ke jeruji besi.

Berikut petikan wawancara wartawan "PR" Satrya Graha dan Dedy Suhaeri dengan pria
yang telah berkeliling ke-90 negara lebih untuk belajar menguak korupsi.

Apa yang membuat Anda begitu antusias memberantas pungli atau korupsi?

Saya anak ke-2 dari 8 bersaudara. Ayah saya, Pak Duadji, bekerja sebagai
seorang supir. Ibu saya, Siti Amah pedagang kecil-kecilan. Terbayang ¢kan
betapa sulitnya membiayai 8 anak dengan penghasilan yang pas-pasan. Oleh
karena itu, saat lulus SMA saya memilih ke Akpol karena gratis.
Nah, waktu sekolah, kira-kira SMP, saya punya banyak teman. Beberapa di
antaranya dari kalangan orang kaya, seperti anak pejabat. Sepertinya, enak
sekali mereka ya, bisa beli ini-itu dari uang rakyat. Sejak itulah, terpatri di benak saya,
ada yang tidak benar di negara ini dengan kemakmuran yang dimiliki oleh para pejabat.
Maka, saya sangat bersyukur bisa berperan memberantas korupsi saat mengabdi di PPATK.
Itulah tugas saya yang paling berkesan selama ini karena bisa menjebloskan menteri, mantan
menteri, dan direktur BUMN, yang memakan uang rakyat. Ada kepuasan batin.

Pengalaman di PPATK itukah yang membuat Anda menabuh genderang perang
melawan pungli saat masuk ke Polda Jabar?

Seperti itulah. Akan tetapi, harusnya diubah, bukan pungli. Kalau pungli,
terkesan perbuatan itu ketercelaannya kecil. Yang benar adalah korupsi.
Pungli adalah korupsi. Mengapa korupsi yang saya usung? Karena sejak zaman
Majapahit dulu, korupsi itu salah. Apalagi, jika aparat hukum yang korup.
Bagaimana kita, sebagai aparat hukum, bisa memberantas korupsi kalau kitanya sendiri korupsi.

Oleh karena itu, sebagai tahap awal, saya "bersihkan" dulu di dalam, baru
membersihkan yang di luar. Bagaimana saya mau menangkap bupati, direktur,
dan lain-lain kalau di dalamnya belum bersih dari korupsi. Kalau aparatnya
korupsi, tamatlah republik ini.
Tahap awalnya biasa saja. Umumkan, lalu periksa ke atasan tertingginya,
yaitu saya, selanjutnya keluarga saya. Setelah itu pejabat-pejabat di Polda. Baru kemudian ke
kapolwil, kapolres, dan seterusnya.
Kenapa harus dimulai dari saya. Karena saya pimpinan tertinggi di Polda
Jabar ini. Ingat, memberantas korupsi bukan dimulai dari polisi yang
bertugas di jalan raya. Kalau di pemerintah, bukan dari tukang ketik, atau
petugas kecamatan yang melayani pembuatan akte kelahiran. Akan tetapi,
dimulai dari pimpinan tertinggi di kantor itu.

Artinya, saya sebagai pimpinan jangan korupsi. Bentuknya macam-macam,
seperti mendapat setoran dari bawahan, setoran dari pengusaha-pengusaha,
mengambil jatah bensin bawahan, atau mengambil anggaran anggota saya. Oleh
karena itu, saya tidak akan minta duit dari dirlantas, direskrim, atau
kapolwil. Tidak juga mengambil anggaran mereka, atau uang bensin mereka.
Jadi, kalau di provinsi, misalnya, ada korupsi, yang salah bukan
karyawannya, tetapi gubernurnya. Memberantasnya bagaimana?

Mudah saja. Tinggal copot saja orang tertinggi di instansi itu.
Untuk program "bersih-bersih" itu, kira-kira Anda punya target sampai kapan?

Secepatnya. Ya, dua-tiga bulan. Kalau tidak segera, bagaimana kita
menunjukkan kinerja kepada rakyat. Kita tidak perlu malu dan takut nama kita jatuh
kalau bersih-bersih dari korupsi di dalam.
Kita tidak akan jatuh merek dengan menangkap seorang kolonel polisi atau
polisi berbintang yang korupsi. Kalau perlu, tulis gede-gede itu di koran.
Dan, anggota saya yang ketahuan korupsi, akan saya pecat. Jika memang saya
harus kehabisan anggota saya di Polda Jabar karena semuanya saya pecat
gara-gara korupsi, kenapa tidak.
Apa yang harus ditakutkan. Saya yakin, rakyat pasti senang kalau polisi
bebas dari korupsi. Polisi itu bukan milik saya, tetapi milik rakyat. Saya
justru merasa lebih tidak terhormat kalau memimpin kesatuan yang anggotanya
banyak korupsi.

Berbicara soal penanganan kasus korupsi. Betulkah mengusut kasus korupsi
bagaikan mengurai benang kusut. Pasalnya, para penyidik tipikor Polda Jabar
mengaku kesulitan mengungkap kasus korupsi dengan alasan perlu kajian yang
mendalam atas bukti-bukti sehingga memakan waktu lama?

Hahaha.... (Susno tertawa lepas). Mengusut kasus korupsi itu jauh lebih
mudah ketimbang mengusut kasus pencurian jemuran. Mengungkap kasus pencurian
jemuran perlu polisi yang pintar karena banyak kemungkinan pelakunya, seperti orang
yang iseng, orang yang lewat, dan beberapa kemungkinan lainnya.
Kalau kasus korupsi, tidak perlu polisi yang pintar-pintar amat. Misal, uang anggaran
sebuah dinas ada yang tidak sesuai. Tinggal dicari ke mana uangnya lari. Orang-orang
yang terlibat juga mudah ditebak. Korupsi itu paling melibatkan bosnya, bagian keuangan,
kepala projek, dan rekanan. Itu saja.
Jadi, kata siapa sulit? Sulit dari mananya. Tidak ada yang sulit dalam
memberantas korupsi. Kuncinya hanya satu, kemauan yang kuat. Harus diakui,
itu (memberantas korupsi) memang susah karena korupsi itu nikmat. Apalagi,
saat memegang sebuah jabatan.

Contohnya saja posisi kapolda. Siapa sih yang tidak mau jadi kapolda.
Ibaratnya, tinggal batuk, apa yang kita inginkan langsung datang.
Pertanyaannya, mau atau tidak terjerumus di dalamnya (korupsi). Kalau saya,
jelas tidak. Itu hanya kenikmatan duniawi sesaat saja. Untuk apa sih duit
banyak-banyak hingga tidak habis tujuh turunan. Gaji saya saja sekarang
sudah besar. Mobil dikasih. Bensin gratis. Ada uang tunjangan ini-itu. Sudah lebih
dari cukup. Anak-anak saya juga sudah kerja semua. Bahkan, gajinya lebih besar dari saya.

Lalu, langkah apa yang akan Anda buat agar Polda Jabar giat mengungkap kasus korupsi?

Seperti saya katakan tadi, bersih-bersih dulu di dalam. Jika sudah bersih di dalam, baru
membersihkan di luar. Dan kasus korupsi akan menjadi salah satu target kami. Kami akan
genjot pengungkapan kasus korupsi biar Jabar bergetar.
Untuk itu, kami akan berkoordinasi dengan PPATK untuk mengusut kasus-kasus
korupsi di Jabar yang melibatkan pejabat publik. PPATK pasti mau membantu
asalkan anggota saya bersih dan bisa dipercaya. Kita juga bisa diberi
kasus-kasus. Kalau tidak bersih dan tetap "bermain" bagaimana bisa
dipercaya. Kalau orang sudah percaya sama kita, maka banyak kasus yang
masuk.

Akan tetapi, bukan karena basic saya di korupsi sehingga korupsi digenjot.
Kasus lainnya juga dikerjakan. Dan, untuk itu harus tertib administrasi,
salah satunya dengan membuat sistem pelaporan perkara berbasis IT yang
terintegrasi dari polsek hingga ke polda. Untuk apa? Agar kita tahu setiap
ada perkara yang masuk. Jadi, alangkah bodohnya seorang kapolda jika tidak
mengetahui jumlah perkara di jajarannya. Kalau jumlahnya saja tidak tahu,
bagaimana tahu isi perkaranya. Dalam sistem pelaporan perkara tersebut,
nantinya ada klasifikasi perkara. Perkara mana yang porsinya polda, polwil,
polres, dan polsek. Untuk polda, misalnya kasus teror dan korupsi. Soal lapor
boleh di mana saja.

Kita juga harus mempertanggungjawabkan hal itu ke pelapor dengan mengirim
surat kepada pelapor bahwa kasusnya ditangani oleh penyidik ini, ini, dan
ini. Kemajuannya dilaporkan secara berkala.
Ini akan menjadi standar penilaian untuk penyidik. Dan kapolda mengetahui
semua ini karena sistemnya ada sehingga tidak pabaliut. Saya paling tidak
suka yang pabaliut-pabaliut.

Mungkin, bagi sebagian orang, pabaliut itu enak karena sesuatu yang tidak
tertib administrasi itu paling enak untuk diselewengkan. Benar tidak?

Langkah Anda memberantas pungli dan korupsi di tubuh Polda Jabar kemungkinan
akan memberi efek pada pengungkapan kasus dengan alasan anggaran yang minim.
Menurut Anda?

Kalau kita pandang minim, pasti minim terus. Kapan cukupnya. Kalau anggaran
sudah habis, jangan dipaksakan memeras orang untuk menyidik. Mencari klien
yang kehilangan barang di sini, memeras di tempat lain. Siapa yang suruh?
Bilang saja sama rakyat, anggaran kita sudah habis untuk menyidik. Kita
tidak perlu sok pahlawan.

Perilaku memeras atau menerima setoran itu zaman jahiliah. Tidak perlu ada
lagi anggota setor ke kasat lantas atau kasat serse, lalu kasat serse setor
ke kapolres, dan kapolres setor ke kapolwil untuk melayani kapolda. Jangan
pernah setori saya. Lingkaran setan itu saya putus agar tidak ada lagi
sistem setoran.

Bukan zamannya lagi seorang kapolsek, kapolres atau kapolwil bangga karena
mampu membangun kantornya dengan megah. Dari mana duitnya kalau bukan dari
setoran orang-orang yang takut ditangkap, seperti pengusaha judi, dan
penyelundupan. Tidak mungkin dari gaji, wong gajinya hanya Rp 5-6 juta.
Menurut saya, anggota yang melakukan itu hanya satu alasannya, ingin kaya.
Kalau ingin kaya, jangan jadi polisi, tetapi jadilah pengusaha.

Sikap Anda tersebut kemungkinan memunculkan pro dan kontra di lingkungan
kepolisian?

Lho, kenapa harus jadi pro dan kontra. Peraturannya sudah jelas mana yang
boleh dilakukan dan mana yang tidak boleh. Korupsi jelas-jelas dilarang dan
ancamannya bisa dipecat. Jadi, tidak perlu diperdebatkan. Titik.

Bagi saya, siapa yang menjadi pemimpin harus mau mengorbankan kenikmatan
dan kepuasan semu. Nikmat dengan pelayanan, dengan sanjungan, serta nikmat
dengan pujian palsu. Malu dong bintang dua jalan petantang-petenteng, tetapi anak
buah yang dipimpinnya korupsi dan memberikan pelayanan tidak sesuai dengan standar.
Malu juga dong kita lewat seenaknya pakai nguing-nguing (pengawalan), sementara
rakyat macet. Itu juga korupsi.

Polisi yang korup sama saja dengan melacurkan diri. Jadi, kalau saya korup
dengan menerima setoran-setoran tidak jelas, apa bedanya saya dengan
pelacur.(Kutipan dari Pikiran Rakyat selesai)
