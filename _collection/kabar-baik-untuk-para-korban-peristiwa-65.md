---
layout: collection
title: Kabar Baik Untuk Para Korban Peristiwa 65
---

Pada tanggal 18 Mei 2005 website http://perso.club-internet.fr/kontak mendapat kabar baik dari kalangan LBH Jakarta yang bisa menggembirakan para korban peristiwa 65 (yang jumlahnya puluhan juta orang di seluruh Indonesia itu) bahwa dalam sidang yang ke-empat (tanggal 18 Mei 2005) di Pengadilan Negeri Jakarta Pusat mengenai gugatan LBH Jakarta, telah diputuskan oleh Majelis Hakim bahwa “gugatan class action stigma PKI/korban tragedi 65” adalah sah menurut hukum.

Ini berarti bahwa di kemudian hari persidangan dapat dilanjutkan dengan sidang-sidang untuk memeriksa pokok perkara, yang tercatum dalam surat gugatan LBH Jakarta kepada Pengadilan Negeri Jakarta Pusat, yang terdiri dari dari lebih dari 35 halaman (lihat “Surat gugatan LBH Jakarta” dalam website http://perso.club-internet.fr/kontak). Untuk itu Majelis Hakim telah memerintahkan agar LBH Jakarta mengadakan pengumuman/notifikasi di media massa nasional atas penetapan Majelis Hakim Pengadilan Negeri Jakarta Pusat.

Majelis Hakim yang diketuai Cicut Sutiarso dalam Penetapannya Nomor : 75/Pdt.G/2005/PN.JKT.PST menyatakan :

1. Menyatakan Gugatan Perwakilan Kelompok (class action) dapat diteruskan ;
2. Melanjutkan subtansi pemeriksaan perkara ;
3. Biaya perkara ditentukan pada akhir pemeriksaan pokok perkara ;

Setelah adanya penetapan Pengadilan Negeri Jakarta Pusat itu, penggugat (LBH Jakarta) akan meneruskan/ menyampaikan notifikasi (pemberitahuan) kepada seluruh anggota kelompok (korban) yang diperkirakan berjumlah 20 juta (di seluruh Indonesia) melalui media massa dan instansi pemerintahan. Pemberitahuan kepada para korban ini sudah harus dilakukan selama 60 hari. Direncanakan oleh LBH Jakarta bahwa pemberitahuan/notifikasi kepada para korban itu sudah bisa diumumkan/diedarkan dalam minggu terakhir bulan ini (bulan Mei).

Sidang yang ke-empat (tanggal 18 Mei) dihadiri oleh kuasa hukum mantan presiden Suharto, Habibi, Abdurrahman Wahid, dan juga kuasa hukum presiden yang sekarang (SBY), sedangkan kuasa hukum mantan presiden Megawati tidak kelihatan hadir. Sidang yang ketiga (tanggal 11 Mei) telah mendapat kunjungan banyak sekali orang (diperkirakan lebih dari 100 orang) sehingqa ruangan menjadi penuh sesak. Di antara hadirin juga terdapat tokoh internasional yang terkenal, yaitu Carmel Budiardjo, yang sudah puluhan tahun menjadi pimpinan LSM di London yang banyak melakukan berbagai kegiatan untuk HAM di Indonesia dan khususnya untuk para eks-Tapol.

Dengan keputusan Majelis Hakim Pengadilan Negeri Jakarta Pusat yang mengesahkan gugatan class action LBH Jakarta atas nama para korban peristiwa 65 ini, maka terbukalah kemungkinan untuk mengupayakan rehabilitasi dan kompensasi bagi banyak orang yang selama puluhan tahun sudah dilanggar hak-hak mereka, secara sewenang-wenang - dan tidak berperikemanusiaan - , sebagai warganegara oleh Orde Baru di bawah Suharto.

Juga didapat kabar dari LBH Jakarta bahwa sidang selanjutnya (yang kelima) akan dilangsungkan kembali tanggal 20 Juli 2005. Sudah bisa diperkirakan bahwa sidang-sidang selanjutnnya akan menarik perhatian lebih banyak lagi perhatian dari masyarakjat (termasuk kalangan hukum) dan media massa. Karena, sidang-sidang selanjutnya sudah akan memasuki pokok perkara, antara lain masalah penyalahgunaan kekuasaan dan berbagai tindakan sewenang-wenang oleh (terutama sekali) mantan Presiden Suharto.
