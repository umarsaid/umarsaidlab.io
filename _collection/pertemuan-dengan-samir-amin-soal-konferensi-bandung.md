---
layout: collection
title: Pertemuan dengan Samir Amin soal Konferensi Bandung
---

Beberapa hari yang lalu, pada tanggal 5 April 2010, telah diadakan pertemuan antara tokoh raksasa gerakan perjuangan anti-imperialisme atau anti-neoliberalisme dalam skala internasional, Samir Amin, dengan beberapa orang Indonesia di Restoran INDONESIA di Paris. Hadir dalam pertemuan itu, A. Umar Said, Darwis Khudori pengajar di Universitas Le Havre (Prancis), Fahru Novrian (pengajar di Universitas Indonesia, Jakarta).



Dalam pertemuan yang berlangsung sekitar tiga jam itu telah dibicarakan banyak sekali soal-soal yang berkaitan dengan perkembangan gerakan menentang neo-liberalisme atau anti-imperalisme di berbagai negeri di dunia pada dewasa ini, dan juga menyinggung sejarah perjuangan Samir Amin sendiri sebagai pejuang komunis atau marxist sejak lebih dari 50 tahun.



Banyak soal tentang Konferensi Bandung yang sangat bersejarah bagi perjuangan rakyat berbagai negeri di dunia, terutama rakyat Asia Afrika, telah dibicarakan dalam pertemuan itu. Samir Amin adalah pengagum Bung Karno dan Nasser, di samping Nehru, dan Zhou Enlai. Ia menilai sangat penting Konferensi Bandung yang merupakan sumbangan besar bagi ummat manusia.



Dalam pertemuan itu Darwis Khudori, sebagai salah seorang pengorganisasi peringatan 55 Tahun Konferensi Bandung, yang puncaknya akan dilangsungkan di bulan Oktober yang akan datang di Indonesia, menyampaikan informasi bahwa dalam rangka peringatan 55 Tahun Konferensi Bandung ini akan diselenggarakan pertemuan besar dan penting yang dipusatkan di Universitas Gajah Mada di Jogya.



Dengan bantuan dari kalangan resmi di berbagai negeri, akan diusahakan hadirnya dalam pertemuan besar di Jogya itu banyak tokoh-tokoh terkemuka dari berbagai negeri, termasuk Samir Amin. Ketika dibicarakan betapa pentingnya kehadiran Samir Amin dalam peringatan bersejarah di Jogya itu, Samir Amin menyambut gagasan ini dengan antusiasme yang besar. Ia menyanggupi untuk menghadiri peringatan 55 Tahun Konferensi Bandung di Jogya ini, dan sekaligus berhubungan dengan berbagai gerakan anti-neoliberal di Indonnesia, dan memperbarui pengenalannya tentang situasi di Indonesia dewasa ini dengan mengunjungi daerah-daerah.





(Dalam tulisan tersendiri akan diusahakan, dalam waktu dekat,  penyajian berbagai fikiran Samir Amin tentang ini semua  sebagai tokoh utama perjuangan dan pemikiran gerakan kiri di dunia. Sebagai bahan « informasi permulaan » bagi mereka yang kebetulan belum mengenal sama sekali sejarah atau sosok Samir Amin dianjurkan untuk membuka Internet dan mengetik dalam Google kata kunci « Samir Amin ». Maka akan tersedia bahan-bahan (campur aduk) dalam bahasa Inggris sebanyak 250 000 halaman. Juga tersedia banyak bahan dalam bahasa Prancis, atau bahasa-bahasa lainnya.

Sayang sekali, bahan-bahan di Google tentang Samir Amin dalam bahasa Indonesia masih tidak (atau masih  belum) terlalu banyak.



Dari bahan-bahan di Google itu, didapat informasi tentang Samir Amin, antara lain sebagai berikut :



Samir Amin lahir di Cairo dalam tahun 1932 sebagai anak dari seorang bapak orang Mesir dan ibu wanita Prancis, kedua-duanya dokter. Dalam usia kecilnya ia belajar di sekolah Prancis di Port Said (Mesir) dan kemudian meneruskan di Prancis. Ia mendapat diploma Ph D  ilmu politik dalam tahun 1952, diploma ilmu statistik dalam tahun 1956, dan ilmu ekonomi tahun 1957.



Sejak datang ke Paris, dalam usia mudanya ia menjadi anggota Partai Komunis Prancis, tetapi kemudian menjauhkan diri dari Partai ini akibat ketidak persetujuannya terhadap politik Partai Komunis Uni Soviet. Dalam jangka lama sekali ia dikenal sebagai intelektual Maois.



Dalam tahun 1957 ia mengeluarkan thesisnya The origins of underdevelopment - capitalist accumulation on a world scale . The structural effects of the international integration of precapitalist economies. A theoretical study of the mechanism which creates so-called underdeveloped economies  (Asal usul keterbelakangan –akumulasi kapitalis dalam skala dunia. ......  ......   ).



Sejak lama Samir Amin dikenal sebagai guru besar di bidang ilmu ekonomi politik, terutama soal-soal development,  Ia mengajar ekonomi di Universitas Poitiers (Prancis), Paris dan Dakar (Senegal). Ia telah menulis banyak sekali (berpuluh-puluh) buku tentang hukum, masyarakat sipil, sosialisme, pembangunan ekonomi di negara-negara Afrika dan negara-negara Arab atau Islam. Ia menetap sudah agak lama di Dakar, tetapi sering mondar-mandir antara Senegal, Prancis dan Mesir.



Selama 10 tahun  (dari 1970-1980) ia menjabat sebagai direktur badan PBB  « UN African Institute for EconomicDevelopment and Planning » di Dakar, sesudah ia menjadi pejabat tinggi di Kementerian Planning di Mali ketika negara ini baru merdeka (tahun 1960-1963).



Karena keunggulannya sebagai tokoh besar anti-kapitalisme dan anti-imperialisme (terutama AS) maka ia dipilih oleh berbagai gerakan di dunia untuk menjabat sebagai Ketua dari Third World Forum dan juga sebagai pimpinan utama dari World Forum of Alternatives.



Dengan latar belakang yang seperti disajikan secara singkat inilah maka kelihatan pentingnya kehadiran Samir Amin sebagai tokoh besar internasional dalam rangka peringatan 55 Tahun Konferensi Bandung di Indonesia. Peringatan 55 Tahun Konferensi Bandung di bulan Oktober nantinya akan mempunyai bobot atau arti yang lebih besar lagi dengan partisipasinya seorang yang sangat terkenal di kalangan gerakan menentang kolonialisme, imperialisme atau neo-liberalisme, seperti Samir Amin.
