---
layout: collection
title: Lanjutkan Revolusi Dengan Berpegang Pada Ajaran Bung Karno
---

Di bawah adalah kutipan sebuah artikel dari Berdikari Online, website PRD (Partai Rakyat Demokratik), yang bertanggal 1 Oktober 2010. Artikel tersebut selengkapnya tampil sebagai berikut :



Berikut ini disajikan wawancara tertulis Berdikari Online dengan wartawan senior, A. Umar Said,  yang sekarang tinggal sebagai pensiunan di Paris, dalam rangka mengenang Peristiwa 30 September 1965. Wawancara singkat dengannya yang sekarang berusia 82 tahun (lahir 26 Oktober 1928) ini merupakan sekelumit dari pendapat seorang di antara mereka yang mengalami masa-masa jaman Presiden Sukarno sebelum peristiwa 1965 itu.


Ia meninggalkan Indonesia 2 minggu sebelum terjadinya peristiwa 30 September 1965 untuk menghadiri konferensi  International Organisation of Journalists yang diadakan di Santiago (Chili, Amerika Latin). Kemudian ia terpaksa tinggal di Peking dan bekerja di Sekretariat Persatuan Wartawan Asia-Afrika sampai tahun 1973.


Dalam tahun 1974 ia pindah ke Prancis sebagai political refugee, dan hidup sederhana sambil melakukan berbagai kegiatan.


= = =


Hari ini adalah 45 tahun peristiwa “Gerakan 30 September”. Sebagai salah satu dari sekian banyak orang Indonesia di luar negeri yang terhalang pulang, bagaimana perasaan anda?


Setelah 45 tahun sejak terjadinya peristiwa 30 September 1965, dan terpaksa hidup di luar tanahair karena dihalang-halangi pulang oleh kekuasaan Suharto, maka saya masih sedih dan marah kalau ingat kepada peristiwa itu. Merasa sedih karena saya ingat kepada banyak kawan-kawan saya yang dibunuh secara biadab atau dipenjarakan atau dibuang ke pulau Buru. Saya mempunyai banyak kawan dekat  atau sahabat kental di berbagai kalangan, sebelum terjadinya peristiwa 30 September itu. Karena saya waktu itu menjadi Pemimpin Redaksi Harian Ekonomi Nasional di Jakarta, merangkap Bendahara di PWI Pusat dan juga bendahara Persatuan Wartawan Asia-Afrika. Saya juga dipilih sebagai bendahara KIAPMA, Konferensi Internasional Anti Pangkalan Militer Asing, yang diadakan di Jakarta dan dibuka oleh Presiden Sukarno di Hotel Indonesia.


Karena saya bekerja sebagai wartawan di Jakarta, maka saya mempunyai banyak kenalan, sahabat dan kawan terdekat di kalangan pemerintahan, partai politik, organisasi massa di tingkat nasional. Di antara mereka itu banyak yang ditangkapi, dipenjarakan, bahkan banyak yang dibunuh, tanpa diketahui liang kuburnya. Kenalan, sahabat dan kawan saya itu adalah teman-teman seperjuangan saya dalam mendukung berbagai politik dan ajaran-ajaran revolusioner Bung Karno.


Saya masih ingat bahwa sebagian terbesar di antara mereka kebanyakan adalah pemimpin atau tokoh-tokoh atau aktivis-aktivis terkemuka di bidang mereka masing-masing. Mereka ditangkapi atau dibunuh karena menjadi anggota PKI, atau simpatisan PKI, atau pendukung Bung Karno. Jadi, mereka  dianggap musuh oleh Suharto karena berhaluan kiri, menyokong ajaran-ajaran revolusioner Bung Karno yang pada pokoknya anti-kapitalisme, anti-kolonialisme,  anti-imperialisme dan pro-sosialisme.


Saya sedih kalau mengingat mereka, karena mereka pada umumnya orang-orang yang mau berjuang secara tulus untuk kepentingan rakyat banyak, terutama rakyat miskin. Mereka pada umumnya adalah orang-orang yang jujur, tidak korupsi, mau bekerja keras untuk kepentingan rakyat, dan menyokong Bung Karno. Orang-orang macam ini sekarang ini sulit sekali ditemukan.


Jika memperhatikan perkembangan situasi di tanah air akhir-akhir ini, dengan begitu banyak persoalan seperti ekonomi, politik, sosial, dan budaya, bagaimana pendapat anda sebagai orang berdarah Indonesia?


Terus terang, saya sangat prihatin bahwa situasi negara dan bangsa kita dewasa ini makin  menjauh sekali dari tujuan proklamasi 17 Agustus 45. Kebobrokan, kerusakan, kebejatan moral kelihatan makin menyeluruh di tubuh bangsa, terutama sekali  di kalangan atasan,  baik di bidang eksekutif, legislatif, maupun judikatif, dan juga di kalangan masyarakat dan kalangan swasta.


Rejim militer Orde Baru di bawah Suharto sudah membikin kerusakan-kerusakan besar sekali di bidang jiwa bangsa, dengan memalsu Pancasila, melecehkan Bhinneka Tunggal Ika, merusak persatuan bangsa dengan Ketetapan MPRS no 25/1966 yang melarang kegiatan PKI beserta ormas-ormasnya, dan dengan memusuhi ajaran-ajaran Bung Karno.


Kerusakan jiwa  bangsa yang dibikin Suharto dan para jenderalnya ini merupakan sumber utama dari berbagai macam kerusakan-kerusakan lainnya, yang berbagai hasilnya kita saksikan dewasa ini, antara lain dengan meluasnya pertentangan atau permusuhan antar agama, kerusuhan antar-etnik. Ini adalah pengkhianatan kepada Pancasila dan Bhinneka Tunggal Ika.


Situasi negara dan bangsa kita yang seperti dewasa ini pastilah tidak diinginkan oleh para pejuang untuk kemerdekaan, baik yang sudah dibuang ke kamp pengasingan di Boven Digul, yang ikut dalam pembrontakan melawan penjajahan Belanda dalam  tahun 1926, maupun yang ikut berjuang selama masa revolusi Agustus  1945.


Negara kita yang kaya raya dengan sumber alam, dewasa ini dihuni oleh rakyat yang sebagian terbesar adalah rakyat miskin, dan dikuras terus oleh modal raksasa asing dalam sistem neo-liberal yang dianut oleh pemerintahan SBY.


Sebagai orang yang pernah hidup di jaman Bung Karno, apa perbandingan paling penting dengan jaman sekarang ini?


Sebagai  orang yang pernah hidup di jaman Bung Karno, saya melihat banyak sekali perbedaan antara jaman itu dengan jaman Suharto dan jaman pemerintahan-pemerintahan sesudahnya, sampai dengan pemerintahan SBY sekarang. Perbandingan antara jaman Bung Karno dengan jaman Suharto adalah seperti siang dan malam.


Jaman Bung Karno adalah jaman « revolusi rakyat belum selesai » di bawah pimpinannya berdasarkan ajaran-ajaran revolusioner Bung Karno, antara lain Manifesto Politik (Manipol), Nasakom, Trisaksi, Panca Azimat Revolusi, TAVIP (Tahun Vivere Pericoloso), yang semuanya berjiwa persatuan bangsa, anti-kapitalisme, anti-kolonialisme, dan anti-imperialisme. Bahkan, Bung Karno juga menganjurkan perjuangan untuk sosialisme di Indonesia.


Sedangkan jaman Suhato adalah kebalikannya sama sekali ( atau bertolak belakang), yang sebagian terbesar masih diteruskan sampai sekarang. Jaman Suharto adalah jaman kontra-revolusi, yang melawan seluruh ajaran-ajaran revolusioner Bung Karno, bersekongkol dengan imperialisme, menentang sosialisme, anti-demokrasi, dan yang sekaligus juga korup, penuh KKN dan pelanggaran HAM berat dan besar.


Kalau jaman Bung Karno terkenal dengan politiknya yang tercakup di dalam Trisakti (bebas-aktif dalam politik, berdikari dalam ekonomi, berkepribadian dalam kebudayaan), maka jaman Suharto yang dilanjutkan oleh berbagai pemerintahan sesudahnya, termasuk pemerintahan SBY sekarang, adalah juga kebalikannya.


Negara Republik Indonesia sekarang ini tidak berdaulat penuh dalam politik, sehingga terasa lemah menghadapi Malaysia dan tekanan-tekanan kekuatan bangsa-bangsa lain, termasuk dalam masalah-masalah ekonomi.  Dalam bidang ekonomi negara kita berada DALAM cengkeraman modal-modal raksasa asing, sehingga praktis Indonesia menjadi negara jajahan kembali.


Apa pesan anda terhadap rakyat Indonesia saat ini?


Saya tidak berani memberi pesan apa-apa kepada rakyat Indonesia, selain  menganjurkan seluruh bangsa untuk mendengarkan  (dan melaksanakan)  pesan Bung Karno yang terdapat dalam berbagai macam ajaran-ajaran revolusionernya. Jiwa atau sari-pati ajaran-ajaran revolusionernya  untuk meneruskan revolusi yang belum selesai, revolusi untuk menjebol dan membangun, adalah justru masih relevan sekali untuk menghadapi berbagai masalah besar bangsa kita sekarang.


Sari pati atau jiwa ajaran-ajaran revolusioner Bung Karno untuk terus-menerus melanjutan revolusi sama sekali bukanlah suatu anjuran yang sudah kedaluwarsa, atau suatu slogan yang hanya bagus dan cocok untuk masa lalu, melainkan suatu jalan yang perlu ditempuh oleh rakyat dalam menghadapi persoalan-persoalan parah dewasa ini, termasuk menghadapi neo-liberalisme.


Seperti sudah ditunjukkan oleh pengalaman berpuluh-puluh tahun sesudah berdirinya Orde Baru sampai sekarang, jalan lain menuju masyarakat adil dan makmur tidak ada, kecuali jalan ajaran-ajaran revolusioner Bung Karno. (Selesai)
