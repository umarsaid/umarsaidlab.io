---
layout: collection
title: Malam Persahabatan di Paris Yang Penuh Arti
---

Atas inisiatif dan undangan sejumlah orang-orang Prancis pada tanggal 5 April 2011 telah diadakan malam persahabatan (soirée amicale) untuk merayakan pemberian Medali Kota Paris kepada Umar Said sebagai warga kehormatan ibukota Prancis. Malam persahabatan itu berlangsung di Restoran koperasi INDONESIA, dengan dihadiri oleh berbagai sahabat Restoran dan tokoh-tokoh organisasi dan partai politik Prancis, termasuk Partai Sosialis dan Partai Komunis Prancis.

Mantan pejabat penting dan tokoh yang dikenal oleh berbagai kalangan kiri di Prancis, Louis JOINET, telah hadir juga dan ambil bagian aktif pada malam yang diliputi suasana persahabatan militan itu. Louis JOINET adalah mantan sekretaris  jenderal  persatuan hakim-hakim Prancis (Sindicat de Magistrature) sebelum menjabat sebagai penasehat hukum berbagai Perdana Menteri Prancis (lima Perdana Menteri berturut-turut), dan juga menjadi penasehat di Elysée (Istana kepresidenan).

Telah bertindak secara bagus sebagai pembawa acara dan pengatur malam persahabatan itu Anita Sobron (putri Sobron Aidit alm), di samping Suyoso, seorang yang telah menjadi oengelelola utama restoran koperasi ini selama sekitar 24 tahun.

Pada malam itu, sebagian besar  para hadirin yang berjumlah sekitar 30 orang itu, yang merupakan sahabat-sahabat lama, telah mengutarakan berbagai hal tentang peran Umar Said sebagai pendiri atau promotor dari usaha kolektif restoran koperasi ini serta banyak kegiatan-kegiatan lainnya di bidang politik, sosial, HAM, dan untuk demokrasi.

Dan mereka umumnya ikut merasa bangga dengan diberikannya Medali Kota Paris kepadanya dan menjadikannya sebagai « citoyen d’honneur » (warga kehormatan), mengingat apa yang telah dilakukannya selama ini, baik ketika masih di Indonesia maupun sesudah hidup di Prancis sejak tahun 1974 sebagai political refugee.

Ketika di Indonesia, sebelum peristiwa 1965, Umar Said menjabat sebagai pemimpin redaksi Harian Ekonomi Nasional di Jakarta (dilarang terbit oleh militer Suharto), merangkap pengurus PWI Pusat, dan bendahara PWAA (Persatuan Wartawan Asia-Afrika, gagasan Bung Karno) dan juga bendahara KIAPMA (Konferensi Internasional Anti Pangkalan Militer Asing) di Jakarta, yang adalah juga gagasan Bung Karno.

Di antara pembicara itu terdapat Pascal LUTZ, seorang Prancis yang karena simpatinya kepada projek restoran koperasi ini maka telah bersedia menjadi manager sukarela (dalam bahasa Prancisnya « gérant bénévole ») selama 20 tahun. Dengan panjang lebar ia mengutarakan tentang hubungannya yang erat dengan Umar Said, sejak ia masih aktif di Amnesty Internasional dalam tahun 1976, dan pengalamannya dalam ikut mengelola restoran ini.

Danielle DESGUEES, seorang wanita Prancis yang menerima bintang Legion d’Honneur (penghormatan yang tinggi di Prancis) karena jasa-jasanya dalam bidang sosial dan ekonomi, telah banyak bicara tentang pentingnya pengalaman restoran INDONESIA. Ia adalah direktris suatu badan yang banyak membantu perusahaan-perusahaan Prancis dengan petunjuk-petunjuk atau konsultasi. Selama ini sudah ada 140 000 perusahaan yang  telah mendapat bantuan dari badan yang dipimpinnya .

Pascal LEDERER, tokoh penting asosiasi « Pasar Malam », assosiasi yang banyak kegiatannya dalam menggalang hubungan antara Prancis dan Indonesia menegaskan peran besar restoran INDONESIA selama ini Demikian juga Isabelle DUQUESNE yang memimpin Centre LEBRET, suatu badan yang membantu projek-projek pembagunan di benua Asia dan Afrika, termasuk di Timor Leste.

Carlos SEMEDO, ketua Assosiasi France-Timor Leste yang juga sahabat karib presiden Timor Leste Ramos Horta dan Perdana Menteri yang sekarang Xanana Gusmao, mengatakan bahwa separo dari menteri-menteri Timor Leste sudah mengenal sejarah  restoran INDONESIA dan makan di dalamnya.
Robert AARSSE, mantan diplomat Belanda mengisahkan  sejarah Komite Indonesia di Paris pada masa dan kegiatan –kegiatannya bersama Umar Said dalam melawan poliyik rejim militer Suharto.

Banyak peserta-peserta lainnya ikut memeriahkan petemuan malam  itu, dan di antaranya adalah Pierre Marcie. Karena pentingnya kehadirannya malam itu bersama istrinya,Christianne, yang dua-duanya adalah mantan tokoh dalam Urusan Luar Negeri di Comite Central Partai Komunis Prancis (PCF), maka akan ada tulisan tersendiri mengenainya. Pierre Marcie pernah ditugaskan oleh Comite Central Partai Komunis Prancis pada akhir 1999 untuk membantu YPKP (Yayasan Penelitian Korban Pembunuhan 65) yang dipimpin Ibu Sulami waktu itu

Dengan hadirnya sahabat-sahabat lama tersebut di atas ditambah dengan berbagai orang lainnya, maka bisalah dimengerti bahwa sebagian besar hadirin menganggap bahwa pertemuan yang diadakan oleh orang-orang Prancis malam itu mempunyai arti yang besar, bukan saja bagi Umar Said dan SCOP Fraternite/Restoran INDONESIA, melainkan juga bagi berbagai golongan di Paris, terutama golongan kiri.

Lahirnya Assosiasi Sahabat SCOP Fraternite dan Restoran INDONESIA.

Dalam malam persahabatan itu tidak saja dibicarakan masalah pemberian Medali Kota Paris untuk Umar Said dan sejarah berdirinya Restoran koperasi INDONESIA, melainkan telah diangkat juga masalah penting lainnya, yaitu rencana sejumlah orang Prancis untuk mendirikan « Asosiasi Sahabat  Scop Fraternite dan Resoran Koperasi INDONESIA » (Association des amis de la SCOP Fraternité et Restaurant INDONESIA)

Lahirnya rencana oleh sahabat-sahabat Prancis ini adalah karena berbagai pertimbangan yang berkaitan dengan sejarah berdirinya Restoran INDONESIA 28 tahun yang lalu, ciri-ciri yang unik restoran koperasi ini, orientasi dan tujuannya dalam ekonomi sosial atau ekonomi solider, dan inspirasi atau contoh yang bisa diberikan oleh Restoran INDONESIA kepada dunia usaha di Paris atau Prancis umumnya.

Dalam menjelaskan rencana untuk berdirinya « Association des amis dela SCOP Fraternité et du Restaurant Cooperatif INDONESIA » itu Danielle DESGUEES mengatakan bahwa sejarah didirikannya Restoran  INDONESIA mempunyai berbagai hal yang menarik untuk diketahui oleh masyarakat Prancis.

Menurutnya, keputusan Umar Said untuk mengudurkan diri dari pekerjaannya sebagai pegawai rendahan  di Kementerian Pertanian Prancis, untuk bisa mendirikan usaha guna memungkinkan teman-temannya (political refugee yang berdatangan di Paris dari berbagai negeri) untuk bekerja dan hidup biasa adalah suatu hal yang patut diketahui oleh banyak orang.

Kesediaannya untuk hanya dibayar dengan gaji minimum dan sama rata dengan seluruh pegawai yang bekerja di restoran sampai bertahun-tahun pada perrmulaan berdirinya restoran menunjukkan aspek yang menarik dari kemauannya untuk mensukseskan usaha kolektif restoran ini.

Orientasi yang dipegang oleh SCOP Fraternité/Restoran koperasi INDONESIA yang bertujuan dalam ekonomi sosial dan solider, yang sudah dilaksanakan sejak berdirinya 28 tahun yang lalu, merupakan ciri-ciri yang patut mendapat penghargaan dari banyak kalangan..

Dengan maksud supaya berbagai hal tentang SCOP Fraternité/Restoran koperasi INDONESIA ini lebih diketahui oleh banyak orang di Prancis dan juga untuk ikut mendorong supaya usaha kolektif ini bisa tetap terus berkembang dengan baik menurut cita-cita semula yang melahirkannya, maka didirikan « Assosiasi sahabat SCOP Fraternite dan Restoran Koperasi INDONESIA ».

Para penggagas rencana didirikannya assosiasi ini berpendapat bahwa jiwa restoran koperasi INDONESIA (berhaluan  kiri, melaksanakan tujuan koperasi, bertujuan ekonomi sosial dan solider, ikut dalam berbagai kegiatan  HAM dan demokrasi) perlu dijadikan inspirasi atau contoh bagi usaha kolektif di Prancis.

Dengan berdirinya assosiasi ini, semua orang, semua kalangan atau grup, semua organisai dalam masyarakat Prancis, yang setuju dengan jiwa atau tujuan SCOP Fraternite/Restoran INDONESIA, dapat menjadi anggota dan ikut memperkenalkannya, menyebarluaskannya dan melestarikannya di kemudian hari..

Menurut gagasan sementara, assosiasi ini merupakan badan yang independen, yang terpisah secara organisasi dari SCOP Fraternité dan Restoran INDONESIA, yang merupakan perkumpulan sukarela, dan bisa  bertemu 3 atau 4 kali setahun.

Assosiasi ini bisa mengambil inisiatif unuk melakukan berbagai kegiatan politik, sosial dan kultural secara independen dalam rangka penyebaran jiwa koperasi SCOP Fraternité/Restoran koperasi INDONESIA.



Malam persahabatan yang penuh arti

Dengan penyelenggaraan malam persahabatan itu  kelihatanlah betapa besar perhatian sahabat-sahabat Prancis terhadap pemberian Medali kota Paris kepada Umar Said sebagai warga kehormatan dan juga kepada rencana didirikannya « Association des amis de la SCOP Fraternite et du Restaurant INDONESIA ».

Perhatian besar ini kelihatan lebih menonjol dengan partisipasinya secara aktif tokoh dan mantan pejabat tinggi Prancis, Louis JOINET, dalam malam yang penuh dengan suasana militan itu. Dengan berbagai cara ia menyatakan kesediaannya untuk membantu terlaksananya tujuan-tujuan  Assosiasi Sahabat SCOP Fraternité/Restoran INDONESIA. Bahkan, ia menyanggupi untuk menjadi pimpinan utamanya.

Juga sangat aktifnya Danielle DESGUEES bersama suaminya (François VESCIA) untuk mengadakan berbagai kegiatan yang berkaitan dengan SCOP Fraternité, termasuk melakukan banyak hal tentang rencana Assosiasi Sahabat Restoran INDONESIA, merupakan tambahan keunikan kepada usaha kolektif ini. Fenomena semacam ini tidak pernah  terjadi di antara ribuan restoran di Paris dan sekitarnya.

Semua hal yang disajikan di atas menunjukkan bahwa berdirinya restoran koperasi INDONESIA merupakan hal yang bisa dianggap positif sekali bagi kehidupan di kota Paris. Karenanya, banyak sahabat Prancis yang mengatakan bahwa jiwa restoran koperasi ini menjadi inspirasi atau contoh bagi usaha kolektif lainnya di Paris atau di Prancis pada umumnya.

Dengan adanya « Assosiasi Sahabat Restoran INDONESIA »  dapat diharapkan bahwa usaha kolektif ini mendapat dorongan dari mitra dekat dan menerima sekedar bantuan sukarela dan bersahabat yang diperlukan dalam perjalanan jauh restoran di kemudian hari.

Dengan demikian jiwa koperasi yang sudah dijalankan sejak 28 tahun dapat dilestarikan untuk jangka yang panjang. Sebab, mereka yang bekerja di restoran ini bisa saja berganti-ganti, dan situasi politik, ekonomi dan sosial di Prancis bisa juga berubah pada waktunya, namun jiwa asli Restoran INDONESIA harus tetap dipertahankan. Selama mungkin !!!
