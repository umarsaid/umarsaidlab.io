---
layout: collection
title: Restoran INDONESIA di Paris, monumen perlawanan terhadap Orde Baru
---

Berikut ini adalah berita lanjutan tentang pertemuan untuk ulangtahun ke-28 restoran koperasi INDONESIA di Paris dan pemberian Medali Kota Paris kepada Umar Said, untuk lebih melengkapi lagi berita yang terdahulu yang sudah juga disiarkan dalam website

Pertemuan itu merupakan peristiwa yang disambut dengan gembira oleh banyak orang. Ucapan-ucapan selamat telah diterima dari banyak negeri, dan dalam sebagian terbesar ucapan-ucapan selamat itu disebutkan bahwa kedua peristiwa ini juga merupakan kebanggaan bagi para pengirimnya masing-masing. Banyak juga yang menyebutkan bahwa pertemuan di Paris tanggal 24 Januari itu ikut menaikkan martabat para korban Orde Baru yang terdapat di Indonesia dan juga di luar negeri.

Bahkan ada yang menegaskan bahwa peringatan ke-28 Restoran INDONESIA dan pemberian Medali Kota Paris kepada Umar Said merupakan tamparan  berat kepada sisa-sisa Orde Baru dan orang-orang yang masih tetap mendukung Suharto. Karenanya, dapat dimengerti bahwa kedua peristiwa ini merupakan kejutan bagi kalangan yang selama puluhan tahun (lebih dari 32 tahun !) sudah memusuhi golongan kiri (antara lain anggota dan simpatisan PKI) dan pendukung  Bung Karno.

 Memang adalah hal yang bisa dianggap cukup « aneh » bahwa orang yang sudah jelas berhaluan kiri dan pendukung politik Bung Karno, yang oleh rejim militer Suharto dinyatakan sebagai orang jahat, anti Indonesia, dan segala macam fitnah lainnya, mendapat penghargaan yang begitu  besar dari kotapraja ibukota negara Prancis, yaitu Paris yang terkenal itu.

Dari segi ini dapat dilihat bahwa pemberian Medali Kota Paris ini kepada Umar Said, salah seorang dari puluhan juta orang yang menjadi korban rejim militer Suharto mempunyai makna politik yang besar. Sebab, melalui peristiwa ini juga bisa dianggap sebagai simbul bahwa kota Paris  menyatakan simpati kepada puluhan juta korban rejim militer Suharto, yang dalam ini diwakili oleh Umar Said. Itulah sebabnya mengapa Umar Said merasa senang dan bangga.

Umar Said sudah terima tiga medali

Bagi Umar Said, penerimaan Medali Kota Paris dari Walikota Paris atas nama kotapraja Paris merupakan peristiwa yang amat besar artinya, dilihat dari berbagai segi. Ini merupakan tambahan kepada dua  medali yang sudah pernah diterimanya di masa-masa yang lalu.

Medali pertama adalah medali mas yang diterimanya dalam tahun 1963 dari Ratu Kosamak, ibu Pangeran Sihanouk, pemimpin Kamboja. Ia dianugerahi medali mas ini sebagai wartawan yang ikut dalam rombongan Presiden Sukarno ketika dalam kunjungan kenegaraan ke Kamboja.

Medali mas dari Kamboja ini sampai sekarang disimpan baik-baik oleh istrinya. Karena terjadinya G30S medali mas ini dirawat dengan hati-hati  selama puluhan tahun, karena tertulis « Long live President Sukarno »

Medali kedua adalah dari Presiden Timor Leste, Xanana Gusmao,  dalam tahun 2002 ketika Umar Said ikut menghadiri perayaan kemerdekaan Timor Leste di Dilli. Umar Said adalah promotor didirikannya Komite Timor Timur di Paris dalam tahun 1976.

Baginya dua medali itu mempunyai arti penting tersendiri yang berkaitan dengan masalah-masalah yang dihadapi waktu itu. Namun, ia menganggap bahwa Medali Kota Paris yang diterimanya tanggal 24 Januari itu lebih penting lagi karena mencakup sejarah hidupnya yang cukup berliku-liku sebelum menjadi warganegara Prancis.

Buku otobiografi « Perjalanan hidup saya »

Kisah hidupnya yang penuh dengan pengalaman-pengalaman yang menarik itu dapat disimak dalam website yang juga menyajikan (dalam format PDF) otobiografinya dalam buku « Perjalanan Hidup Saya » yang diterbitkan di Jakarta dalam tahun 2004 sebagai « Buku Bermutu Program Pustaka IKAPI »

Dengan menyimak isi buku tersebut, maka bisa didapat sekadar gambaran mengapa kota Paris memberikan Medali dengan penyebutan bahwa Umar Said adalah pejuang atau aktivis (militant) untuk Fraternité (persaudaraan) dan Liberté (kebebasan).

Pemberian Medali kota Paris dan penyebutan Umar Said sebagai wargakota kehormatan  ibukota Prancis ini menimbulkan gema yang luas dan jauh, dan bisa berjangka lama. Sebab, jaranglah orang Indonesia yang mendapat penghormatan yang begitu tinggi. Apalagi, kalau orang tersebut adalah berhaluan politik kiri, anti Orde Barunya Suharto dan  pernah menjadi korban sebagai « orang klayaban » berpuluh-puluh tahun, karena mendukung  politik Bung Karno.

Gema ini terutama terdengar di kalangan eks-tapol,  dan golongan kiri yang simpati kepada PKI, atau para korban Orde Baru pada umumnya, yang jumlahnya puluhan juta orang dan tersebar di seluruh Indonesia. Bagi mereka gema ini bisa membangkitkan kembali perasaan bangga menjadi orang kiri dan bahwa haluan politik kiri dianggap baik oleh banyak orang.

Dari itu semua dapat ditarik kesimpulan bahwa pemberian Medali Kota Paris kepada Umar Said adalah keputusan yang berdasarkan sikap politik, atau berlatar belakang politik. Dan politik ini adalah politik, yang dalam kasus ini,  bisa digolongkan kiri. Itulah sebabnya mengapa peristiwa ini mendapat sambutan atau menimbulkan gema terutama di kalangan kiri, baik yang di Indonesia maupun yang di luar negeri. Ini kelihatan dari banyaknya ucapan selamat yang diterima.

Teman-teman lama restoran koperasi INDONESIA

Peringatan ulangtahun ke 28 Restoran koperasi INDONESIA yang tambah dimeriahkan dengan pemberian Medali Kota Paris kepada Umar Said kali ini menunjukkan bahwa restoran ini memang mempunyai ciri-ciri yang unik. Ciri-ciri yang unik itu tidak hanya kelihatan dari banyaknya buku tamu (24 jilid ) melainkan juga dari banyaknya teman-teman atau langganan yang selama puluhan tahun tetap mempertahankan hubungan erat.

Salah satu contohnya yalah seorang pegawai pos yang bernama Jean Claude, yang selama 28 tahun setiap minggunya datang untuk makan di restoran ini. Entah berapa jumlah uang yang sudah dibayarkannya untuk itu. Sudah jelas bahwa sikapnya itu tidak hanya karena menyukai makanan Indonesia saja, melainkan juga karena sebab-sebab lainnya. Seperti banyak orang lainnya, ia menaruh simpati kepada restoran yang dikelola dengan jiwa atau orientasi koperasi dan secara kolektif ini, dan yang menjadi tempat persahabatan dan persaudaraan.

Contoh lainnnya adalah sikap yang ditunjukkan Madame Danielle MITTERRAND, istri almarhum Presiden François MITTERRAND, yang belasan tahun yang lalu sering datang dengan teman-temannya untuk mengadakan pertemuan sambil makan, kali ini juga memerlukan datang. Sudah jelas bahwa kedatangannya  itu bukan hanya karena ingin makan enak, melainkan karena simpatinya yang besar kepada restoran yang mempunyai corak dan sejarah tersendiri ini di Paris.

Monumen perlawanan terhadap Orde Barunya Suharto

Mengingat sejarah kelahiran Restoran INDONESIA dan kegiatan-kegiatan yang dilakukan selama 28 tahun ini, nyatalah bahwa restoran ini walaupun merupakan usaha komersial seperti restoran-restoran lainnya di Prancis, namun juga memainkan peran lainnya atau mempunyai fungsi lainnya.

Restoran INDONESIA di Paris, walaupun tidak besar  (70 kursi), namun kumandangnya cukup terdengar di kota besar ini. Letaknya juga strategis di pusat kota Paris, yaitu di daerah Latin Quarter, dan tidak jauh dari Universitas Sorbonne yang terkenal itu.

Dari pengalaman selama ini sudah diketahui oleh banyak orang bahwa Restoran INDONESIA didirikan oleh orang-orang yang bersikap menentang rejim militernya Suharto. Karenanya, ada orang-orang yang mengatakan bahwa restoran ini berupa seperti monumen perlawanan terhadap Orde Baru  dan menjadi simbul untuk fraternité (persaudaraan) dan liberté (kebebasan) bagi banyak orang, baik yang  Indonesia maupun yang lain.

Karena itu diharapkan oleh banyak orang supaya restoran koperasi ini hidup terus dan bisa berfungsi seperti selama ini, dan tetap menjadi kebanggaan bagi para korban Orde Baru,  maupun yang bukan. Yang menarik dan sangat menggembirakan adalah bahwa  ada sejumlah sahabat-sahabat Prancis yang menyatakan kesediaan mereka akan tetap ikut menjaga kelangsungan dan kemajuan usaha kolektif ini, dengan berbagai cara.
