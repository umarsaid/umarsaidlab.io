---
layout: collection
title: 'Radio Nederland : Umar Said warga kehormatan kota Paris'
---

Pemberian Medali Kota Paris kepada Umar Said, dan menjadikannya warga kehormatan ibukota Prancis (citoyen d’honneur) yang dilakukan pada tanggal 24 Januari 2011 masih terus ada gemanya.

Radio Nedeland Siaran Indonesia (RANESI) memberitahukan bahwa tiga hari berturut-turut (tanggal 13, 14, 15 Februari 2011) telah dibuat siaran mengenai masalah ini, melalui gelombang pendek. Untuk memberi kesempatan kepada siapa saja yang masih ingin mendengarkan siaran tersebut, maka RANESI menyajikan sebagiannya di website www.ranesi.nl., yang dapat dibuka setiap waktu.

Dalam website tersebut dapat dibaca « Umar Said Warga Kehormatan Paris » yang teksnya adalah sebagai berikut :



Tahun 2011 berawal baik bagi Umar Said yang sudah 37 tahun tinggal di Paris. Paris menganugerahinya medali berukirkan "Paris à Umar Said, 2011" Dengan demikian Umar Said menjadi citoyen d'honneur, warga kehormatan kota Paris. Terlebih lagi medali itu dianugerahkan pada Umar Said, bukan Andre Aumars, nama yang disandangnya semenjak ia menerima kewarganegaraan Prancis.

Mantan wartawan antara lain Indonesia Raya, Harian Rakyat, Harian Penerangan dan Ekonomi Nasional ini lahir pada 1928 di Pakis, Jawa Timur. Ia adalah salah satu diantara sekian banyak warga Indonesia yang terhalang pulang ke tanah air setelah Peristiwa 30September 1965. Umar Said mendapat suaka di Prancis yang disebutnya France Terre D'Asile, dan di sana ia membangun kehidupan baru.

Tak Mau Bebani Tuan Rumah
Sebagai ucapan terimakasih Umar Said dan teman-teman senasib, bertekad tidak mau membebani negara tuan rumah. Pada 1982 mereka mendirikan rumah makan, Restoran Indonesia Paris. Di tempat ini mereka juga menyelenggarakan berbagai kegiatan kegiatan social dan budaya. Selain menjadi promotor tempat pelepas rindu tanah air, Umar Said juga aktif memperjuangkan HAM.

Medali Kota Paris dianugerahkan pada Umar Said untuk seluruh upayanya. Usaha pria berusia 82 tahun ini banyak: memperjuangkan kebebasan, persahabatan , solidaritas, demokrasi, mendirikan komite Timor Timur, membela ex tapol.

Medali Kota Paris

![]({{ site.baseurl }}/assets/images/myphoto8.jpg)

Penyerahan medali dilakukan oleh Christian Sautter yang mewakili walikota Bertrand Delanoe. Hadir pula pada kesempatan itu Madame Danielle Mitterrand, yang sudah lama mengenal pak Umar Said.

Bagaimana cendekiawan mengelola rumah makan di negeri orang? Penasaran? Klik tanda panah ini.

(Dikutip dari website www.ranesi.nl ).

[![](https://w.soundcloud.com/icon/assets/images/orange_white_32-94fc761.png)](https://soundcloud.com/pradana-aumars/id_umar-said-medali-paris)

Tulisan-tulisan tentang pemberian Medali.

Tulisan-tulisan tentang pemberian Medali Kota Paris kepada Umar Said, mengenai sejarah singkatnya dan mengapa ia diberi penghormatan oleh kota Paris sebagai pejuang persaudaraan (militant de fraternité) dan pejuang kebebasan (militant de liberté) masih bisa terus disimak dalam website ini.

Sampai sekarang masih diterima ucapan-ucapan selamat atau berbagai reaksi dari banyak fihak mengenai pemberian penghormatan yang berupa Medali Kota Paris ini.
