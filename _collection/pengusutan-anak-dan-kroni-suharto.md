---
layout: collection
title: Pengusutan anak dan kroni Suharto
---

Oleh karena banyaknya persoalan-persoalan besar dan parah yang dihadapi negara dan rakyat dewasa ini (antara lain : banjir besar dimana-mana, gempa di Sumatera, listrik yang digilir, penderitaan yang menyedihkan bagi korban lumpur panas Lapindo, penyelewengan di Bank Indonesia, tersangkutnya anggota-anggota DPR dalam soal BLBI, diadilinya mantan Kapolri Rusdihardjo karena korupsi dll dll dll), maka persoalan besar mengenai tindakan hukum terhadap Suharto, beserta anak-anaknya dan kroni-kroninya, akhir-akhir ini kurang menjadi pembicaraan dan perhatian banyak orang.

Padahal, dalam konteks situasi dewasa ini, penyelesaian masalah Suharto beserta anak-anak dan para kroninya, adalah masalah yang tetap penting, karena ada hubungannya yang erat dengan banyak soal bangsa dan negara kita. Hal-hal inilah yang diangkat secara baik dalam tulisan wartawan Siprianus Edi Hardum yang dimuat oleh Suara Pembaruan ‘(25 Februari 2008). Mengingat pentingnya tulisan ini untuk diteruskannya gugatan atau desakan masyarakat untuk menuntut keluarga Suharto beserta kroni-kroninya, maka disajikan kembali tulisan ini, bagi mereka yang tidak sempat membacanya di Suara Pembaruan.

A. Umar Said
= = =

Tulisan dalam Suara Pembaruan itu selengkapnya adalah sebagai berikut :

Pengusutan Anak dan Kroni Soeharto, Utopis?

Pengantar

Setelah mantan Presiden Soeharto meninggal dunia, kasus hukum yang membelitnya tetap saja menarik perhatian publik. Hal itu terjadi karena Tap MPR XI /1998 tentang Penyelenggara Negara yang Bebas Kolusi, Korupsi, dan Nepotisme (KKN), masih tetap berlaku. Akankah putra-putri dan kroni Soeharto diseret ke pengadilan untuk mempertanggungjawabkan berbagai kasus KKN pada masa pemerintahan Orde Baru? Wartawan Suara Pembaruan Siprianus Edi Hardum menuliskan laporannya.

Tuntutan sebagian masyarakat agar pemerintah, dalam hal ini Kejaksaan Agung (Kejagung) menyeret mantan Presiden Soeharto secara pidana atas dugaan tindak pidana korupsi yang dilakukannya selama ia menjadi Presiden, berhenti sejak sang "Jenderal Besar" itu menghadap Tuhan pada 27 Januari 2008.

Pasalnya, menurut hukum, seseorang yang diduga melakukan tindak pidana dan meninggal dunia, maka proses hukum secara pidana terhadapnya gugur dengan sendirinya. Yang didesak sebagian masyarakat terhadap pemerintah, terkait tindak pidana korupsi yang dilakukan Soeharto adalah menuntut Soeharto melalui ahli warisnya (anak-anaknya) secara perdata. Sedangkan kasus korupsi yang dilakukan anak-anak dan kroni-kroni Soeharto tetap dilakukan secara pidana dan perdata.

Tuntutan pengusutan dugaan tindak pidana korupsi yang dilakukan Soeharto, anak-anak dan kroni-kroninya merupakan amanat reformasi 1998, sebagaimana tertuang dalam Tap MPR XI /1998 tentang Penyelenggara Negara yang Bebas Kolusi, Korupsi dan Nepotisme (KKN).

Pasal 4 Tap MPR XI/1998 berbunyi, "Upaya pemberantasan KKN harus dilakukan secara tegas terhadap siapapun juga, baik pejabat negara, mantan pejabat negara, keluarga dan kroninya maupun pihak swasta / konglomerat termasuk mantan Presiden Soeharto dengan tetap memperhatikan prinsip praduga tak bersalah dan HAM".

Dugaan Soeharto melakukan tindak pidana korupsi sehingga sampai dirumuskan dalam Tap MPR seperti tersebut di atas, rupanya tidaklah berlebihan. Betapa tidak pada 20 September 2007, United Nations Office on Drugs and Crime (UNODC) dan World Bank Group (WBG), menyatakan, mantan Presiden Soeharto menduduki peringkat pertama sebagai pencuri aset negara, yaitu sebesar US$ 15 miliar - US$ 35 miliar dari kurun waktu 1967 - 1998.

Pertanyaannya adalah bisakah pemerintah sekarang dalam hal ini Kejagung mengusut dugaan tindak pidana yang dilakukan kroni-kroni Soeharto, baik secara pidana maupun perdata, sebagaimana diamanatkan Tap MPR tersebut?

Masih Rezim Soeharto

Sudah 10 tahun reformasi, amanat Pasal 4 Tap MPR tersebut belum ada hasilnya. Jaksa Agung berganti, tetapi tidak ada terobosan untuk melaksanakan amanat itu. Saat ini Kejagung tengah menggugat Soeharto secara perdata melalui keenam anaknya atas dugaan penyimpangan dana Yayasan Supersemar, yang sidangnya masih berlangsung di Pengadilan Negeri (PN) Jakarta Selatan (Jaksel). Namun, sebagian masyarakat kurang berharap Kejagung bisa menang karena Kejagung tidak terlalu progesif.

Pada 31 Agustus 2000, pemerintahan BJ Habibie dalam hal ini Kejagung mendudukkan Soeharto di kursi pesakitan, yakni di PN Jaksel.

Jaksa Penuntut Umum (JPU), Muchtar Arifin, yang sekarang menjabat sebagai Wakil Jaksa Agung, mendakwa Soeharto melakukan korupsi atas tujuh yayasan yang dimiliki Soeharto, yang merugikan negara senilai Rp 1,7 triliun.

Namun, tuntutan terhadap Soeharto berhenti karena Soeharto mengalami sakit permanen. Berdasarkan itulah Jaksa Agung, Abdul Rahman Saleh, pada 12 Mei 2006, mengeluarkan Surat Ketetapan Penghentian Penuntutan Perkara (SKP3) Soeharto, walaupun banyak masyarakat memprotesnya.

Banyak kalangan berpendapat, kalau Kejagung mau kasus dugaan korupsi yang dilakukan Soeharto diusut tuntas, maka pengadilan terhadap Soeharto bisa dilakukan secara in absentia. Seperti advokat kondang, Adnan Buyung Nasution, mengatakan, pengadilan Soeharto bisa dilakukan secara in absentia. Setelah Soeharto dinyatakan bersalah, kata pendiri Yayasan Lembaga Bantuan Hukum Indonesia (YLBHI) itu, maka negara dan masyarakat mengampuninya; dan uang negara yang dikorupsinya diambil untuk negara.

Direktur Eksekutif The Indonesian Legal Resource Center, Uli Parulian Sihombing, mengatakan, kalau Soeharto tidak diadili secara pidana maka pemerintah sejak awal menggugatnya secara perdata. Hal ini bertujuan untuk mengembalikan harta negara yang dicuri Soeharto.

Sedangkan menurut Ketua Program Studi Magister Hukum Fakultas Hukum Universitas Airlangga, Surabaya, Peter Mahmud Marzuki, diperlukan inisiatif untuk mengembalikan aset yang telah dicuri rezim yang memerintah secara otoriter dan marak korupsi. Biasanya setelah rezim yang otoriter dan korup itu tumbang, penggantinya segera mengeluarkan suatu dekrit apa pun namanya untuk membentuk suatu lembaga tertentu atau melakukan tindakan tertentu yang ditujukan untuk mengusut aset negara yang diperkirakan telah dicuri rezim otoriter. Hal seperti ini, kata Peter, sudah dilakukan Filipina,tidak lama setelah Presiden Marcos tumbang.

Lalu mengapa pemerintah reformasi yang dipimpin Habibie, Megawati dan Susilo Bambang Yudhoyono tidak melakukan itu? Banyak analis politik dan ekonomi berpendapat pengusutan kasus dugaan korupsi oleh Soeharto tidak dilakukan karena pemerintahan reformasi adalah pemerintah Soeharto jilid II. "Pemerintahan reformasi, kecuali pemerintahan Gus Dur, adalah pemerintah Orde Baru jilid II," kata ekonom Faisal Basri.

Dalam pengertian ilmiahnya sebagaimana dirumuskan Stephen D Krasner, seperti dikutip Arief Budiman, rezim lebih dikaitkan dengan prinsip-prinsip, norma-norma, aturan-aturan dan prosedur pengambilan keputusan yang dianut oleh penguasa negara (Arief Budiman, 1997: 86 - 87).

Mengacu pada dua pengertian rezim itu, maka tidak berlebihan kalau kita mengatakan, pemerintahan reformasi, terutama pemerintahan sekarang, adalah pemerintahan yang masih dikuasai rezim Soeharto.

Pada kabinet Habibie, Megawati Soekarnoputri, dan terutama kabinet Yudhoyono banyak sekali kroni Soeharto.

Oligarki Berkaki Tiga

Rezim Soeharto begitu kuat, dan mungkin masih untuk beberapa periode ke depan, karena sistem ekonomi-politik yang dibangun Soeharto selama 32 tahun adalah sistem oligarki. Sistem ini melahirkan pengikut setia yang turun-temurun.

Sistem ekonomi-politik oligarki menurut filsuf Yunani, Plato, sebagaimana dikutip Aditjondro, adalah suatu bentuk masyarakat dimana kekayaan menentukan kekuasaan, dimana kekuasaan politik berada di tangan orang-orang kaya, sementara orang miskin tidak mempunyai kekuasaan apa-apa (George Junus Aditjondro dalam bukunya Korupsi Kepresidenan, Reproduksi Oligarki Berkaki Tiga : Istana, Tangsi, dan Partai Penguasa, LKiS Yogyakarta, 2006, hal 4).

Menurut Aditjondro, oligarki yang dibangun Soeharto adalah oligarki berkaki tiga. Oligarki pertama adalah "istana", yang merupakan lingkaran dalam oligarki ini. "Istana" bukanlah berarti sebuah gedung yang merupakan tempat tinggal resmi Presiden, melainkan keluarga besar Presiden yang meliputi kerabat dan keluarga besar yang tinggal di luar istana.

Oligarki kedua adalah "tangsi", yang sekaligus merupakan lingkaran pelindung pertama dari "istana". Menurut Aditjondro, "tangsi" bukanlah mengacu pada tempat tinggal tiga kesatuan dan Polri, melainkan komunitas militer dan polisi dari para purnawirawan, perwira tinggi, sampai pada para prajurit yang bertugas memelihara kepentingan modal besar.

Tugas itu, kata Aditjondro, bukan semata-mata dijalankan karena ketaatan pada perintah atasan, melainkan karena kesatuan TNI telah diikat kesetiaannya pada keluarga batih Soeharto dengan yayasan-yayasan milik satuan-satuan TNI dan Polri.

Selesai menjalankan dinas kemiliteran mereka, para mantan Panglima Daerah Militer (Pangdam), Kepala Staf Angkatan Darat (Kasad), Kepala Staf Angkatan Laut (Kasal), Kepala Staf Angkatan Udara (Kasau) dan Kapolri banyak yang diangkat menjadi komisaris pada berbagai perusahaan keluarga Soeharto.

Selain itu, sebelum dapat diangkat menjadi Panglima TNI, para perwira tinggi itu dibina kesetiaan mereka menjadi ajudan pribadi Soeharto, istri dan keenam anaknya.

Oligarki ketiga adalah partai penguasa, yang ketika Soeharto menjadi Presiden bernama Golongan Karya (Golkar). Kaki ketiga ini, kata Aditjondro, adalah benteng perlindungan kedua untuk berbagai bisnis istana, yang sekaligus berfungsi menyamarkan keberpihakan para serdadu dalam melindungi kepentingan bisnis keluarga istana.

Dengan ketiga jalurnya, yakni jalur A (ABRI), jalur B (birokrasi), dan jalur G (kader Golkar yang asli, yang berasal dari tiga ormas pendiri Golkar, yakni MKGR, Kosgoro, dan SOKSI), partai penguasa ini menjadi benteng yang sakti dalam melindungi bisnis istana, dan sekaligus men-sipil-kan bisnis keluarga Soeharto.

Sampai sekarang, kroni-kroni Soeharto adalah mereka-mereka yang diuntungkan dan dibesarkan dalam tiga oligarki tersebut. Kroni itu mulai dari konglomerat, pemilik media massa, TNI dan Polri, baik yang masih aktif maupun sudah purnawirawan.

Selain itu, ada juga yang duduk di pemerintahan, DPR dan sebagai pejabat penegak hukum, seperti polisi, jaksa dan hakim. Berdasarkan itu, gagasan mengusut dugaan korupsi yang dilakukan anak-anak dan kroni-kroni Soeharto adalah sesuatu yang utopis, mustahil terjadi, terutama oleh pemerintah sekarang.

Coba simak pernyataan Jaksa Agung, Hendarman Supandji di DPR, Rabu (6/2), ketika ditanya mengenai pengusutan tindak pidana korupsi yang dilakukan kroni mantan Presiden Soeharto. Ia mengatakan, sejauh ada alat bukti yang menunjukkan ada tindak pidana yang dilakukan kroni mantan Presiden Soeharto, Kejaksaan akan menangani perkaranya sesuai koridor hukum.

Meski demikian, harus dirumuskan lebih dahulu, apakah dalam perbuatan itu ada tindakan melawan hukum yang merugikan negara. Menurut Hendarman, hingga kini Kejaksaan belum memiliki satu pun alat bukti yang berkaitan dengan dugaan korupsi kroni Soeharto.

Beranikah aparat penegak hukum, mengusut anak-anak Soeharto dan kroni-kroninya?

Uli Parulian Sihombing masih berharap kepada pemerintah sekarang. Mantan Direktur LBH Jakarta ini, mengatakan, kasus hukum Soeharto, anak-anak dan kroni-kroninya, terutama kasus korupsi, merupakan utang yang harus "dilunasi" pemerintah untuk bangsa dan negara. "Yang terpenting Jaksa Agung mempunyai kemauan politik untuk mengusut dugaan tindak pidana korupsi Soeharto," kata dia.

Indonesia, kata Uli, harus belajar dari pengusutan korupsi mantan Presiden Ferdinand Marcos di Filipina dan Alberto Fujimori di Peru membutuhkan waktu dan komitmen politik untuk menghadapi semua rintangan.
