---
layout: collection
title: Peringatan 55 tahun Konferensi Bandung
---

Tidak lama lagi Konferensi Bandung (atau Konferensi Asia-Afrika), yang diselenggarakan tanggal 18-24 April 1955 akan diperingati ulang tahunnya yang ke 55. Untuk memperingati peristiwa besar yang sangat bersejarah bagi berbagai rakyat di Asia dan Afrika ini , berbagai kegiatan sudah dan akan diselenggarakan di berbagai negeri.



Umpamanya, di Prancis saja sudah diadakan berbagai kegiatan oleh banyak fihak. Yang pertama pada tanggal 5 November 2009 diselenggarakan pameran foto tentang konferensi ini di gedung KBRI Paris oleh berbagai organisasi di bawah koordinasi Darwis Khudori, pengajar dan peneliti di Universitas Le Havre (Peranscis).



Pameran foto ini dibuka dengan malam pertemuan, dimana diundang untuk berbicara wakil sejumlah organisasi di Paris. Ikut juga diundang dalam pertemuan ini  A. Umar Said sebagai seorang saksi dari konferensi yang bersejarah ini, ketika waktu itu ia bekerja sebagai wartawan Harian Rakyat (organ  PKI) untuk membuat reportase-reportase sekitar peristiwa ini. Sebagai pembicara utama tentang Konferensi Bandung, ia telah menguraikan pentingnya sumbangan Bung Karno untuk berhasilnya konferensi besar ini. Kuasa usaha KBRI di Paris juga ikut mengucapkan pidato untuk pembukaan pameran foto ini. Untuk memeriahkan pertemuan pada  malam itu  telah disajikan tari-tarian Indonesia yang dibarengi dengan gamelan.



Kegiatan lainnya adalah yang diadakan oleh kota besar Paris di bawah pimpinan walikota Bertrand Delanoe, yang juga seorang pimpinan terkemuka dari Partai Sosialis. Pada tanggal 22 Februari 2010 di ruangan besar Balai Kota Paris telah diselenggarakan pertemuan dengan thema « Semangat Konferensi Bandung. Akhir jaman koloniale ? Pembangunan ekonomi dan sosial ataukah neokolonialisme ? »



Di antara pembicara-pembicara yang terdiri dari tokoh-tokoh terkemuka dan ahli di berbagai bidang politik, ekonomi, sejarah, dan perjuangan untuk pembebasan nasional berbagai negeri ini terdapat Hocin Aït Ahmad, pimpinan Front Pembebasan Aljazair yang ikut menghadiri Konferensi Bandung lebih dari setengah abad yang lalu. Darwis Khudori memberikan uraian tentang « Kemajuan ekonomi Asia sesudah era Bandung »



Pertemuan besar di Balai Kota Paris dengan fokus pembicaraan soal-soal yang berkaitan dengan  Konferensi Bandung ini kemudian disusul oleh pertemuan lainnya di Universitas Le Havre, kota pelabuhan yang besar di Prancis. Pertemuan di Universitas Le Havre ini, yang dilangsungkan pada tanggal 12 Maret 2010 dari pagi sampai sore,  selain dihadiri oleh para mahasiswa jurusan bahasa Indonesia dan hubungan internasional, juga dihadiri oleh sejumlah gurubesar dan dosen.



Dalam pertemuan di Universitas ini juga telah diundang A.Umar Said yang telah menggunakan kesempatan ini  untuk berbicara banyak tentang peran besar dan penting yang telah dimainkan oleh Bung Karno. Bung Karno adalah penggerak utama Konferensi Bandung di samping Nehru dan Nasser. Tanpa kegigihan semangat anti-imperialisme dan anti-kolonialisme yang sudah disandangnya  sejak ia muda belia, tidak akan ada Konferensi Bandung.



A. Umar Said membeberkan juga pengkhianatan Suharto terhadap Bung Karno, yang berarti bahwa Suharto juga  mengkhianati perjuangan rakyat Indonesia melawan imperialisme dan neo-kolonialisme serta perjuangan rakyat Asia-Afrika lainnya.



Diadakannya tiga macam kegiatan untuk publik dalam waktu yang begitu singkat oleh berbagai kalangan menunjukkan bahwa sejarah Konferensi Bandung mendapat perhatian dari banyak orang di Prancis. Ini dapat dimengerti karena Prancis pernah dalam jangka lama menjajah banyak sekali negeri di Afrika. Karena itu, dengan adanya Konferensi Bandung pernah terjadi juga banyak goncangan di opini publik, yang sejak itu menimbulkan banyak pernyataan, tulisan atau komentar, dan penerbitan buku-buku dalam bahasa Prancis.



Bahan-bahan tentang Konferensi Bandung dalam bahasa Prancis ini sekarang dapat didapatkan lewat Internet dengan membuka Google. Kalau mengklik Google dan mengetik kata kunci « Conference de Bandung » maka akan tersedia segala macam bahan tentang konferensi ini sebanyak 1.570.000 halaman. Kalau diketik kata kunci « Sukarno conférence Bandung » tersedia bahan-bahan sebanyak 800.000 halaman.



Menurut informasi, peringatan 55 tahun Konferensi Bandung ini akan dilanjutkan dengan kegiatan-kegiatan yang diadakan bersama UNESCO. Puncak dari serangkaian kegiatan-kegiatan ini akan diselenggarakan di Indonesia dalam bulan Oktober yang akan datang.



(Catatan tambahan : cuplikan dari isi pidato A. Umar Said di Universitas Le Havre tentang Konferensi Bandung disajikan tersendiri, karena agak panjang).





 *  *  *





                 Bung Karno tokoh utama Konferensi Bandung



Pengantar : Berikut di bawah ini adalah cuplikan dari pokok-pokok atau isi uraian yang disajikan  (dalam bahasa Prancis) oleh A. Umar Said, yang diundang oleh Universitas Le Havre (Fakultas Urusan Internasional) pada tanggal 12 Maret 2010, di depan para mahasiswa dan pengajar Universitas tersebut)



"Saya merasa bahagia mendapat  kesempatan untuk berbicara di depan anda semua mengenai Konferensi Bandung hari ini di Universitas Le Havre. Ada beberapa alasan mengapa saya mengatakan bahwa saya bahagia dan juga bangga. Antara lain, bahwa saya  adalah salah satu dari banyak wartawan Indonesia dan wartawan asing yang dapat tugas untuk membuat reportase tentang konferensi besar ini yang  bersejarah bagi perjuangan rakyat dari berbagai negara untuk menentang imperialisme dan kolonialisme. Ketika Konferensi Bandung diadakan di tahun 1955, saya bekerja sebagai wartawan Harian Rakyat, organ resmi Partai Komunis Indonesia (PKI)



Seperti Anda semua tahu, sampai tahun 1965, Partai Komunis Indonesia adalah yang terbesar di antara partai-partai komunis di seluruh dunia, di luar negara sosialis atau komunis, dengan sekitar 3 juta anggota dan 20 juta simpatisan. Partai Komunis Indonesia adalah pendukung yang sangat besar politik atau ajaran-ajaran revolusioner dari Presiden Soekarno.



Jadi dapatlah kiranya dikatakan bahwa saya adalah seorang di antara banyak saksi dari peristiwa besar kebangkitan bangsa-bangsa yang terjajah, yang terutama terdiri dari orang kulit berwarna.  Tetapi saya berbicara hari ini bukan hanya sebagai saksi Konferensi Bandung saja, melainkan juga sebagai orang Indonesia yang bangga menjadi pengagum salah satu tokoh terkemuka dari Konferensi Bandung itu, yaitu Presiden Sukarno.



Saya mendukung berbagai pemikiran atau ajaran-ajaran Presiden Sukarno, karena hingga September 1965, saya adalah pemimpin redaksi sebuah harian di Jakarta yang bernama Ekonomi Nasional. Pada saat yang sama saya juga menjabat sebagai anggota pimpinan PWI Pusat, dan anggota sekretariat  (bendahara) Persatuan Wartawan Asia-Afrika dan juga bendahara Konferensi Internasional Anti Pangkalan Militer Asing (KIAPMA) yang  diadakan di Jakarta pada tahun 1965 di bawah pimpinan Presiden Sukarno.



Dengan latar belakang yang demikian itulah maka saya berbicara tentang Konferensi Bandung hari ini. Dalam uraian saya hari ini, saya fokuskan pada peran Presiden Soekarno karena hampir setengah abad lamanya namanya sudah tidak sering disebut -sebut lagi di Indonesia,  dan juga di luar negeri, berlainan dengan  tahun-tahun sebelum 1965 ketika namanya  sangat disanjung-sanjung oleh banyak orang.



Padahal, ketika orang berbicara tentang Konferensi Bandung seharusnya tidak dapat lupa  atau sengaja menghindari dari menyebutkan  jasa dan peran Presiden Sukarno. Berbicara tentang Konferensi Bandung, tetapi tidak menyinggung peran historis Presiden Sukarno adalah omong kosong besar, yang pada dasarnya, berarti mengkhianati konferensi itu sendiri. Sebab, tanpa peran Bung Karno, tidak akan ada Konferensi Bandung. Presiden Sukarno dan Konferensi Bandung adalah satu.



Konferensi Bandung adalah refleksi dari kepribadian besar Presiden Soekarno. Konferensi Bandung adalah bagian dari inti perjuangan yang ia mulai sejak muda dalam usia 20 tahunan, yang mencerminkan pandangan revolusionernya melawan kolonialisme, imperialisme, atau kapitalisme internasional.  Konferensi Bandung adalah juga kontribusi utama rakyat Indonesia untuk bangsa-bangsa Asia dan Afrika untuk memenangkan kemerdekaan nasional, khususnya di Afrika.



Nama Bandung dan nama baik Presiden Sukarno dikenal oleh berbagai gerakan kiri di dunia, dan terutama di kalangan gerakan-gerakan anti kolonialisme dan neo-kolonialisme di kawasan Asia-Afrika-Amerika Latin. Konferensi Bandung adalah peristiwa internasional penting di dunia setelah kekalahan fasisme, Nazi Jerman dan Jepang dalam Perang Dunia Kedua. Kita dapat mengatakan bahwa Konferensi Bandung adalah tsunami politik internasional yang membuat kekuatan-kekuatan imperialisme dan kolonialisme goncang, yang, setelah Perang Dunia Kedua selesai masih mendominasi dunia dan mengendalikan bangsa kulit berwarna.



Sangat penting untuk dicatat bahwa ketika Konferensi Bandung diselenggarakan dalam tahun 1955 jumlah negara-negara di Afrika yang telah meraih kemerdekaan nasional hanyalah 5. Banyak negara-negara  lainnya masih dijajah oleh  Inggris, Belgia, Perancis, Spanyol, Portugal, juga Italia dan Jerman  sebelumnya. Hari ini di benua Afrika terdapat negara merdeka yang jumlahnya 54 dan 3 teritori. Alangkah besarnya perubahan di Afrika sejak diselenggarakannya Konferensi Bandung dalam tahun 1955! Dari sekitar 5 negara merdeka dalam tahun 1955, pada dewasa ini terdapat lebih dari 50 negara merdeka di satu benua saja !



Jadi, sangat jelas bahwa Konferensi Bandung telah membantu mempercepat proses emansipasi mayoritas negara-negara beserta rakyat-rakyatnya di Afrika antara 1955 dan 1965. Seperti kita semua ingat,  di antara banyak negara Afrika yang memperoleh kemerdekaan setelah Konferensi Bandung adalah :



1955-Tunisia dan Maroko

1957 - Ghana

1958 - Guinea

1960 - Pantai Gading, Madagaskar, Kongo Belgia, Senegal, Mali

1961 - Sierra Leone

1962 - Aljazair

1963 - Kenya

1964 - Zambia

1966 - Botswana

1974 - Guinea-Bissau

1975 - Angola, Mozambik



Konferensi Bandung pada dasarnya adalah untuk mendorong perjuangan pembebasan nasional dan menggalang solidaritas rakyat berbagai negeri melawan imperialisme. Oleh karena itu, kekuatan imperialisme, terutama imperialisme AS, menentang atau memusuhi Presiden Sukarno. Sebaliknya, bagi banyak negara di Afrika, yang dapat meraih  kemerdekaan nasional setelah tahun 1955, jelas bagaimana Konferensi Bandung itu penting bagi sejarah dari berbagai negara .



Seperti diakui oleh banyak sejarawan dan pakar politik di dunia, Konferensi Bandung adalah peristiwa besar dalam sejarah dunia modern. Konferensi Bandung telah memberikan kontribusi besar kepada perjuangan bangsa-bangsa, khususnya bangsa-bangsa di Afrika  untuk membebaskan diri dari penjajahan. Konferensi Bandung juga sering dikutip sebagai pengangkatan martabat atau kedudukan dari bangsa berwarna melawan dominasi dan eksploitasi oleh orang-orang kulit putih.



Di masa lalu, di bawah pimpinan Presiden Sukarno, Konferensi Bandung telah menjadi kebanggaan bangsa Indonesia. Bukan hanya kebanggaan bangsa Indonesia, tetapi juga kebanggaan banyak negara di Asia dan Afrika. Tapi seperti yang kita semua saksikan selama puluhan tahun pemerintahan militer Jenderal Suharto, kecemerlangan Konferensi Bandung telah lama pudar, atau bahkan hampir menghilang. Oleh karena itu, sebagian besar rakyat Indonesia, termasuk generasi barunya,  tidak mengetahui atau kurang mengerti tentang pentingnya Konferensi Bandung bagi bangsa Indonesia dan juga bagi sejarah dunia.



Dalam banyak buku pelajaran sejarah di berbagai negara di seluruh dunia, terutama di Perancis, dimasukkan bahan-bahan tentang  bagian pada Konferensi Bandung. Tetapi, selama puluhan tahun rezim militer Suharto, dalam buku-buku sejarah di sekolah-sekolah  walaupun disebutkan Konferensi Bandung, seringkali sangat singkat-singkat saja. Pada umumnya tidak ada penjelasan yang memadai yang telah diberikan tentang peran penting yang dipegang oleh Presiden Soekarno dalam peristiwa bersejarah yang besar ini. Dilihat dari pengalaman sejarah, jelas bahwa Presiden Sukarno adalah musuh bebuyutan bagi kekuatan reaksioner di Indonesia dan di dunia.



Berbagai kebijakan telah dilakukan terus-menerus dan menyeluruh oleh rezim Suharto dalam usaha "de-Soekarno-isasi "untuk sepenuhnya menghilangkan pengaruh politik atau prestise Presiden Soekarno, termasuk semua hal yang berkaitan dengan jasanya kepada Konferensi Bandung. Ini bisa dimengerti karena rezim militer Suharto sama sekali bertentangan dengan semangat Konferensi Bandung, yang pada dasarnya anti-imperialis dan anti-kolonialisme. Dan justru semangat anti-imperialisme dan anti-kolonialisme itulah identitas utama dan asli Presiden Sukarno.



Kita bisa melihat dengan jelas sekarang bahwa karena faktor-faktor itulah maka Presiden Sukarno dianggap sebagai musuh utama oleh para pimpinan Angkatan Darat di Indonesia (pendukung Suharto) , yang bersekutu dengan kekuatan-kekuatan imperialisme (terutama Amerika) sebelum dan sesudah peristiwa 1965. Kekuatan-kekuatan imperialisme dan kolonialisme Barat memandang Soekarno sangat berbahaya  untuk kepentingan mereka, dengan diselenggarakannya Konferensi Bandung pada tahun 1955.



Kita ingat bahwa setelah selesainya Perang Dunia Kedua, muncul Perang Dingin antara blok Barat dan Blok Timur, yang kemudian menimbulkan berbagai peristiwa penting di dunia, termasuk pecahnya Perang Korea. Kelahiran Republik Indonesia dan Republik Demokratik Vietnam di tahun 1945, diikuti oleh China pada tahun 1949, telah menyebabkan perubahan-perubahan penting dalam peta geopolitik Asia.



Imperialisme AS, yang memiliki rasa takut akan bahaya "komunis" menyebar di seluruh Asia telah memperkuat basisnya  di Korea Selatan, Jepang (Okinawa), Taiwan, Filipina, Thailand dan Indocina. SEATO yang merupakan tumpuan penting bagi Amerika Serikat dalam pengendalian Asia Selatan (khususnya Indonesia) didirikan. Dalam konstelasi politik yang demikian inilah Konferensi Bandung telah dilaksanakan. Konferensi Bandung telah menjadi menjadi duri atau hambatan besar bagi kepentingan AS dan sekutu-sekutunya. Dan Sukarno adalah tokoh yang dianggap sangat berbahaya bagi kepentingan AS.



Sekarang, ketika kita memperingati ulangtahun yang ke 55 Konferensi Bandung , perlu dan penting sekali untuk mengingat kembali dan memberi penghormatan kepada nama dan peran Presiden Sukarno. Jasanya atau kontribusinya yang besar harus mendapat tempat terhormat dalam ingatan bersama kita.



Secara pribadi, saya sangat menghargai kegiatan seperti yang kita adakan di Universitas Le Havre hari ini untuk memperingati Konferensi Bandung dalam tahun 1955 yang merupakan tonggak sejarah besar dalam skala dunia. Baru-baru ini pertemuan dan pameran foto tentang Konferensi Bandung telah diselenggarakan juga di Kedutaan Besar Indonesia di Paris dengan dukungan aktif dari Kedutaan, dan konferensi besar lainnya juga telah diselenggarakan di gedung Balai Kota Paris dengan partisipasi banyak ahli dan tokoh terkemuka.



Setelah 55 tahun sejak diadakannya Konferensi Bandung mungkin ada yang bertanya apakah masih ada kebutuhan untuk memperingatinya? Karena, situasi internasional telah banyak berubah. Konfrontasi yang sengit Perang Dingin sudah lama berakhir dan digantikan oleh ketegangan dan persoalan politik dan ekonomi, yang terjadi dalam skala global, kontinental atau regional.



Imperialisme AS telah kehilangan  dominasi yang hegemonik atas peta dunia dengan kelahiran Uni Eropa, dan munculnya kekuatan ekonomi Asia (termasuk Cina, India, Jepang, dan bahkan walaupun kurang spektakuler, Vietnam dan beberapa negara-negara di Asia lainnya). Kekuatan politik dan pembangunan ekonomi di Amerika Latin yang didorong oleh negara-negara yang dipimpin oleh Venezuela dan Brasil juga menyumbang banyak untuk perubahan dalam situasi internasional.



Perubahan internasional ini mengakibatkan efek atau pengaruh di benua Asia dan Afrika, yang sudah  jauh berubah juga dari situasi pada tahun 1955. Negara-negara imperialis atau kolonialis terpaksa merobah atau menyesuaikan atau memenuhi tuntutan dan tekanan yang semakin meningkat dari negara-negara Selatan melalui kebijakan pemerintah atau lewat perjuangan LSM atau gerakan-gerakan populer (contoh: pertemuan besar Porto Allegre di Brasil dan Mumbai di India dan pertemuan di Ottawa, Kanada dll dll. )



Terinspirasi oleh keputusan-keputusan Konferensi Bandung dan resolusi yang diadopsi dan dikembangkan oleh gerakan-gerakan alter-globalisasi maka negara-negara Selatan saat ini sedang berperang melawan neo-liberalisme yang dijajakan oleh WTO, IMF, Bank Dunia. Orang dapat melihat bahwa tampaknya, Semangat Bandung menjadi lebih lemah di negara-negara Asia dan Afrika setelah digulingkannya Presiden Sukarno oleh pengkhianatan besar Jenderal Soeharto pada tahun 1966, yang didukung oleh imperialisme AS. Presiden Sukarno, bapak bangsa Indonesia yang sebenarnya,  telah dipenjarakan dalam tahanan rumah sampai wafatnya pada tahun 1970, akibat perlakuan yang tidak manusiawi dalam jangka panjang.



Digulingkannya Presiden Sukarno oleh Suharto berikut jenderal-jenderalnya adalah sebuah kehilangan besar bagi perjuangan rakyat Asia-Afrika melawan imperialisme dan neo-kolonialisme, dan juga bagi Dunia Ketiga dan Tricontinental secara khususnya atau bagi Gerakan Non-Blok pada umumnya. Dari sudut ini nyatalah bahwa pengkhianatan besar Suharto terhadap Presiden Sukarno adalah serangan terhadap kemanusiaan dan peradaban.



Adalah sungguh keterlaluan bahwa selama 32 tahun kediktatoran Suharto nama dan peran Presiden Soekarno dalam Konferensi Bandung dilarang untuk didiskusikan secara publik.. Padahal, Presiden Sukarno adalah juru bicara rakyat Indonesia dan rakyat Asia-Afrika.



Oleh karena itu, peringatan semacam yang diselenggarakan di Universitas Le Havre ini merupakan kontribusi penting sekali, sebagai bagian dari serangkaian  peringatan Konferensi Bandung yang akan diadakan dalam waktu dekat di berbagai negara dengan kegiatan puncaknya, sebagai  program utama, di Indonesia, negara kelahiran konferensi yang bersejarah ini..



Memperingati Konferensi Bandung berarti juga menghormati kepribadian tokoh-tokoh besar dunia seperti Soekarno, Jawaharlal Nehru, Zhou En-lai, Gamal Abdul Nasser dan lain-lain. Memperingati Konferensi Bandung adalah cara yang sangat penting untuk menjiwai Semangat Bandung dan menyesuaikannya dengan kebutuhan dan perkembangan politik, ekonomi dan kondisi sosial di masing-masing negara  Asia dan Afrika.



Situasi internasional dan situasi di Asia dan Afrika juga telah berubah, dan akan berubah terus. Meskipun ada perubahan-perubahan besar, namun esensi dari Semangat Bandung tetap valid atau tetap sah, untuk membela aspirasi rakyat di Asia dan Afrika pada khususnya dan di negara-negara Selatan pada umumnya.



Dalam konteks ini, peringatan tentang Konferensi Bandung juga merupakan kesempatan untuk mengingat kembali semua kejahatan besar rezim Soeharto terhadap Presiden Sukarno dan seluruh kekuatan kiri yang mendukungnya." (Cuplikan pokok-pokok uraian selesai)
