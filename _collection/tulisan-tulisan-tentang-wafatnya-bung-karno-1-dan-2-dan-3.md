---
layout: collection
title: Tulisan-tulisan tentang wafatnya Bung Karno (1) dan (2) dan (3)
---

Tulisan-tulisan tentang wafatnya Bung Karno



Menjelang datangnya tanggal 21 Juni, hari wafatnya Bung Karno, berikut di bawah ini disajikan sebagian kecil dari berbagai tulisan mengenai peristiwa besar yang menyedihkan banyak orang ini. Kumpulan kecil ini dimaksudkan sebagai pengantar dari sebuah tulisan mengenai wafatnya Bung Karno yang sedang dioersiapkan dan akan disajikan dalam website  http://umarsaid.free.fr   tidak lama lagi. Penyajian kumpulan tulisan ini diharapkan bisa menyegarkan kembali ingatan banyak orang di antara kita tentang peristiwa yang bersejarah ini.



Beda Pemakaman Pak Harto dan Bung Karno

Oleh Asvi Warman Adam *


Pada 27 Januari 2008 pukul 13.10, mantan Presiden Soeharto wafat. Jenazahnya disemayamkan di kediamannya, Jalan Cendana, dan dilayat pejabat tinggi negara, mulai presiden, wakil presiden, sampai para menteri. Masyarakat umum berjubel di sepanjang Jalan Cendana menonton para tetamu.
Senin pagi, 28 Januari 2008, ini jenazah mantan orang nomor satu RI itu diterbangkan ke pemakaman keluarga di Astana Giribangun. Ketua DPR Agung Laksono akan bertindak secara resmi dalam pelepasan jenazah di Jalan Cendana, Wakil Presiden Jusuf Kalla memimpin pelepasan di Halim Perdanakusumah. Presiden Susilo Bambang Yudhoyono menjadi inspektur upacara di Astana Giribangun.


Astana Giribangun yang diperuntukkan keluarga Nyonya Suhartinah Soeharto didirikan di Gunung Bangun yang tingginya 666 meter di atas permukaan laut. Cangkulan pertama dilakukan Tien Soeharto Rabu Kliwon, 13 Dulkangidah Jemakir 1905, bertepatan dengan 27 November 1974.
Dengan menggunakan 700 pekerja, bangunan yang merupakan gunung yang dipangkas tersebut diselesaikan dan diresmikan pada Jumat Wage, 23 Juli 1976. Jadi 30 tahun sebelum meninggal, Soeharto telah mempersiapkan tempat peristirahatan yang terakhir. Hal itu dilakukan Soeharto agar “tidak menyusahkan orang lain”.
Soeharto memperoleh hak dan fasilitas sebagai seorang mantan kepala negara. Namun, hal yang berbeda dialami mantan Presiden Soekarno. Sewaktu mengalami semacam tahanan rumah di Wisma Yaso (sekarang Gedung Museum Satria Mandala Pusat Sejarah TNI) di Jalan Gatot Subroto, Jakarta, Soekarno tidak boleh dikunjungi masyarakat umum.


Pangdam Siliwangi H.R. Dharsono mengeluarkan perintah melarang rakyat Jawa Barat untuk mengunjungi dan dikunjungi mantan Presiden Soekarno. Kita ketahui, H.R. Dharsono kemudian juga menjadi kelompok Petisi 50 dan meminta maaf kepada keluarga Bung Karno atas perlakuannya pada masa lalu itu.
Putrinya sendiri, Rachmawati, hanya boleh besuk pada jam tertentu. Pada 21 Juni 1970, Bung Karno wafat setelah beberapa hari dirawat di RSPAD Gatot Subroto, Jakarta. Beberapa waktu sebelumnya, Rachmawati menanyakan kepada Brigjen Rubiono Kertapati, dokter kepresidenan, kalau Soekarno menderita gagal ginjal, kenapa tidak dilakukan cuci darah? Jawabannya, alat itu sedang diupayakan untuk dipesan ke Inggris.


Itu jelas sangat ironis. Pada masa revolusi pasca kemerdekaan, Jenderal Sudirman menderita penyakit TBC. Ketika itu, obatnya baru ditemukan di luar negeri, yakni streptomycin. Pemerintah Indonesia dalam keadaan yang sangat terbatas dan berperang menghadapi Belanda berusaha mendapatkan obat tersebut ke mancanegara, tetapi nyawa Panglima Sudirman tidak tertolong lagi. Hal itu tidak dilakukan terhadap Ir Soekarno.
Bung Karno dibaringkan di Wisma Yaso setelah wafat di RSPAD Gatot Subroto dan di situ pula dia dilepas Presiden Soeharto dan Nyonya Tien Soeharto. Situasi saat itu memang sangat tidak kondusif bagi Soekarno dan keluarganya. Beberapa hari sebelumnya, yakni 1 Juni 1970, Pangkopkamtib mengeluarkan larangan peringatan hari lahirnya Pancasila setiap 1 Juni. Soekarno sedang diperiksa atas tuduhan terlibat dalam percobaan kudeta untuk menggulingkan dirinya sendiri. Pemeriksaan tersebut dihentikan setelah sakit Bung Karno semakin parah.


Pada 22 Juni 1970, jenazah sang proklamator dibawa ke Halim Perdanakusumah menuju Malang. Di Malang disediakan mobil jenazah yang sudah tua milik Angkatan Darat, demikian pengamatan Rachmawati Soekarnoputri (di dalam buku Bapakku Ibuku, 1984) yang membawanya ke Blitar.
Sepanjang jalan Malang-Blitar, rakyat melepas kepergian sang proklamator di pinggir jalan. Di sini Soekarno dimakamkan dengan Inspektur Upacara Panglima ABRI Jenderal Panggabean pada sore hari. Sambutan dibacakan sangat singkat.


Soekarno hanya dimakamkan di pemakaman umum di samping ibunya. Seusai acara resmi, rakyat ikut menabur bunga. Karena banyaknya tanaman itu, sampai terbentuk gunung kecil di atas pusara Sang Putra Fajar tersebut. Namun tak lama kemudian, rakyat yang tidak kunjung beranjak dari makam kemudian mengambil bunga-bunga itu sebagai kenangan-kenangan. Dalam tempo singkat, makam Bung Karno kembali rata sama dengan tanah.
Pemakaman di Blitar itu berdasar Keputusan Presiden RI No 44/1970 tertanggal 21 Juni 1970. Keputusan tersebut diambil dengan berkonsultasi bersama pelbagai tokoh masyarakat. Padahal, Masagung dalam buku Wasiat Bung Karno (yang baru terbit pada 1998) mengungkapkan bahwa sebetulnya Soekarno telah menulis semacam wasiat masing-masing dua kali kepada Hartini (16 September 1964 dan 24 Mei 1965) dan Ratna Sari Dewi (20 Maret 1961 dan 6 Juni 1962). Di dalam salah satu wasiat itu dicantumkan tempat makam Bung Karno, yakni di bawah kebun nan rindang di Kebun Raya Bogor.


Di dalam otobiografinya, Soeharto mengatakan bahwa sebelum memutuskan tempat pemakaman Soekarno, dirinya mengundang pemimpin partai. Jelas Soeharto menganggap itu masalah politik yang cukup pelik. Jadi, pemakaman tidak ditentukan keluarga, tetapi melalui pertimbangan elite politik.
Kemudian, Soeharto melalui keputusan presiden menetapkan pemakaman di Blitar konon dengan alasan tidak ada kesepakatan di antara keluarga. Apakah betul demikian? Sebab, pendapat lain mengatakan bahwa hal itu dilakukan Soeharto demi pertimbangan keamanan. Jika dikuburkan di Kebun Raya, pendukung Bung Karno akan berdatangan ke sana dalam rombongan yang sangat banyak, sedangkan jarak Bogor dengan ibu kota Jakarta tidak begitu jauh. Hal tersebut dianggap berbahaya, apalagi saat itu menjelang Pemilu 1971.


Pemugaran makam Bung Karno juga penuh kontroversi. Pemugaran dilakukan pada 1978 dengan memindahkan makam-makam orang lain itu. Menurut Ali Murtopo di depan kader PDI se-Jawa Timur, ide tersebut berasal dari Presiden Soeharto. Masyarakat tentu bisa menduga bahwa itu dilakukan dalam rangka mengambil hati para pendukung Bung Karno menjelang pemilu. Dalam pemugaran tersebut, keluarga tidak diajak ikut serta. Bahkan, dalam peresmian pemugaran itu, putra-putri Soekarno tidak hadir.


Dalam prosesi pemakaman di Blitar, Megawati tidak hadir karena sedang berada di luar negeri. Namun, kabarnya putra tertua Bung Karno, Guntur Sukarno Putra, mewakili keluarga mantan Presiden Soekarno akan datang ke Astana Giribangun. Ketika Soeharto di Rumah Sakit Pertamina, Guruh juga berkunjung. Ini suatu pelajaran sejarah berharga bagi bangsa kita. Jangan lagi kesalahan masa lalu diulang dan marilah kita berjiwa besar.
* Dr Asvi Warman Adam, sejarawan, ahli peneliti utama LIPI


Sumber : Jawa Pos,



====   


Rachmawati: Bung Karno Dibunuh Pelan-pelan



Rakyat Merdeka, Jumat, 12 Mei 2006,
Jakarta, Rakyat Merdeka. Terbongkar sudah. Teka-teki perlakuan Soeharto kepada Soekarno selama sekarat ternyata menyakitkan. Proklamator itu dibiarkan digerogoti penyakit akutnya hingga meninggal secara mengenaskan.

“Yang saya bawa ini adalah catatan medis selama setahun, dari 1967 hingga 1968 saat Bung Karno mulai sakit. Jelas telihat bahwa Bung Karno tidak mendapatkan perawatan yang semestinya. Bung Karno dibiarkan mati karena semua obat yang diberikan harus disetujui Soeharto,” ujar Rachmawati dengan penuh emosi. Rachma membuka catatan medis ini di Yayasan Pendidikan Soekarno, Kampus UBK Cikini, Jumat sore.

Dijelaskan, catatan medis yang dibawa dalam bentuk sebelas buntelan itu telah disimpannya sejak tahun 1969.

“Ketika itu ada upaya menghapus jejak Bung Karno. Dokter perawat Bung Karno kemudian memindahkan catatan medis yang disimpan secara pribadi itu ke rumah saya,” lanjut Rachma. Dokter yang dimaksud adalah dr. Surojo.

Adik Megawati ini yakin, Soeharto telah melakukan tindakan sistematis dan terencana untuk mencabut secara berlahan nyawa Bung Karno.

“Dalam medical record ini Bung Karno jelas sakit gagal ginjal. Seharusnya ada tindakan pencucian darah secara berkala. Tapi itu tidak diberikan. Saat itu semua obat yang masuk harus disetujui Soeharto. Bahkan Soeharto juga menolak keinginan keluarga agar Bung Karno mendapat pengobatan di luar negeri,” lanjutnya.

Akibat tidak diobati secara layak, menjelang wafatnya, tubuh Bung Karno bengkak-bengkak dan penyakit ginjalnya tidak dapat disembuhkan lagi. Nasib bapak bangsa ini berakhir tragis di tangan Soeharto.

Rachma sendiri akan menyerahkan rekaman medis ini kepada pemerintah SBY dalam waktu dekat.

“Tidak ada tujuan lain, agar mereka tahu, betapa sangat buruknya perlakuan Soeharto kepada Bung Karno,” katanya.


====   



Benarkah Soeharto membunuh Soekarno...?


Sedari pagi, suasana mencekam sudah terasa. Kabar yang berhembus mengatakan, mantan Presiden Soekarno akan dibawa ke rumah sakit ini dari rumah tahanannya di Wisma Yaso yang hanya berjarak lima kilometer.

Malam ini desas-desus itu terbukti. Di dalam ruang perawatan yang sangat sederhana untuk ukuran seorang mantan presiden, Soekarno tergolek lemah di pembaringan. Sudah beberapa hari ini kesehatannya sangat mundur. Sepanjang hari, orang yang dulu pernah sangat berkuasa ini terus memejamkan mata. Suhu tubuhnya sangat tinggi. Penyakit ginjal yang tidak dirawat secara semestinya kian menggerogoti kekuatan tubuhnya.

Lelaki yang pernah amat jantan dan berwibawa-dan sebab itu banyak digila-gilai perempuan seantero jagad, sekarang tak ubahnya bagai sesosok mayat hidup. Tiada lagi wajah gantengnya. Kini wajah yang dihiasi gigi gingsulnya telah membengkak, tanda bahwa racun telah menyebar ke mana-mana. Bukan hanya bengkak, tapi bolong-bolong bagaikan permukaan bulan. Mulutnya yang dahulu mampu menyihir jutaan massa dengan pidato-pidatonya yang sangat memukau, kini hanya terkatup rapat dan kering. Sebentar-sebentar bibirnya gemetar. Menahan sakit. Kedua tangannya yang dahulu sanggup meninju langit dan mencakar udara, kini tergolek lemas di sisi tubuhnya yang kian kurus.

Sang Putera Fajar tinggal menunggu waktu.

Dua hari kemudian, Megawati, anak sulungnya dari Fatmawati diizinkan tentara untuk mengunjungi ayahnya. Menyaksikan ayahnya yang tergolek lemah dan tidak mampu membuka matanya, kedua mata Mega menitikkan airmata. Bibirnya secara perlahan didekatkan ke telinga manusia yang paling dicintainya ini.

"Pak, Pak, ini Ega..."

Senyap.

Ayahnya tak bergerak. Kedua matanya juga tidak membuka. Namun kedua bibir Soekarno yang telah pecah-pecah bergerak-gerak kecil, gemetar, seolah ingin mengatakan sesuatu pada puteri sulungnya itu. Soekarno tampak mengetahui kehadiran Megawati. Tapi dia tidak mampu membuka matanya. Tangan kanannya bergetar seolah ingin menuliskan sesuatu untuk puteri sulungnya, tapi tubuhnya terlampau lemah untuk sekadar menulis. Tangannya kembali terkulai. Soekarno terdiam lagi.

Melihat kenyataan itu, perasaan Megawati amat terpukul. Air matanya yang sedari tadi ditahan kini menitik jatuh. Kian deras. Perempuan muda itu menutupi hidungnya dengan sapu tangan. Tak kuat menerima kenyataan, Megawati menjauh dan limbung. Mega segera dipapah keluar.

Jarum jam terus bergerak. Di luar kamar, sepasukan tentara terus berjaga lengkap dengan senjata.

Malam harinya ketahanan tubuh seorang Soekarno ambrol. Dia coma. Antara hidup dan mati. Tim dokter segera memberikan bantuan seperlunya.

Keesokan hari, mantan wakil presiden Muhammad Hatta diizinkan mengunjungi kolega lamanya ini. Hatta yang ditemani sekretarisnya menghampiri pembaringan Soekarno dengan sangat hati-hati. Dengan segenap kekuatan yang berhasil dihimpunnya, Soekarno berhasil membuka matanya. Menahan rasa sakit yang tak terperi, Soekarno berkata lemah.

"Hatta.., kau di sini..?"

Yang disapa tidak bisa menyembunyikan kesedihannya. Namun Hatta tidak mau kawannya ini mengetahui jika dirinya bersedih. Dengan sekuat tenaga memendam kepedihan yang mencabik hati, Hatta berusaha menjawab Soekarno dengan wajar. Sedikit tersenyum menghibur.

"Ya, bagaimana keadaanmu, No?"

Hatta menyapanya dengan sebutan yang digunakannya di masa lalu. Tangannya memegang lembut tangan Soekarno. Panasnya menjalari jemarinya. Dia ingin memberikan kekuatan pada orang yang sangat dihormatinya ini.

Bibir Soekarno bergetar, tiba-tiba, masih dengan lemah, dia balik bertanya dengan bahasa Belanda. Sesuatu yang biasa mereka berdua lakukan ketika mereka masih bersatu dalam Dwi Tunggal.

"Hoe gaat het met jou...?" Bagaimana keadaanmu?

Hatta memaksakan diri tersenyum. Tangannya masih memegang lengan Soekarno.

Soekarno kemudian terisak bagai anak kecil.

Lelaki perkasa itu menangis di depan kawan seperjuangannya, bagai bayi yang kehilangan mainan. Hatta tidak lagi mampu mengendalikan perasaannya. Pertahanannya bobol. Airmatanya juga tumpah. Hatta ikut menangis.

Kedua teman lama yang sempat berpisah itu saling berpegangan tangan seolah takut berpisah. Hatta tahu, waktu yang tersedia bagi orang yang sangat dikaguminya ini tidak akan lama lagi. Dan Hatta juga tahu, betapa kejamnya siksaan tanpa pukulan yang dialami sahabatnya ini. Sesuatu yang hanya bisa dilakukan oleh manusia yang tidak punya nurani.

"No..."

Hanya itu yang bisa terucap dari bibirnya. Hatta tidak mampu mengucapkan lebih. Bibirnya bergetar menahan kesedihan sekaligus kekecewaannya. Bahunya terguncang-guncang.

Jauh di lubuk hatinya, Hatta sangat marah pada penguasa baru yang sampai hati menyiksa bapak bangsa ini. Walau prinsip politik antara dirinya dengan Soekarno tidak bersesuaian, namun hal itu sama sekali tidak merusak persabatannya yang demikian erat dan tulus.

Hatta masih memegang lengan Soekarno ketika kawannya ini kembali memejamkan matanya.

Jarum jam terus bergerak. Merambati angka demi angka.

Sisa waktu bagi Soekarno kian tipis.

Sehari setelah pertemuan dengan Hatta, kondisi Soekarno yang sudah buruk, terus merosot. Putera Sang Fajar itu tidak mampu lagi membuka kedua matanya. Suhu badannya terus meninggi. Soekarno kini menggigil. Peluh membasahi bantal dan piyamanya. Malamnya Dewi Soekarno dan puterinya yang masih berusia tiga tahun, Karina, hadir di rumah sakit. Soekarno belum pernah sekali pun melihat anaknya.

Minggu pagi, 21 Juni 1970. Dokter Mardjono, salah seorang anggota tim dokter kepresidenan seperti biasa melakukan pemeriksaan rutin. Bersama dua orang paramedis, Dokter Mardjono memeriksa kondisi pasien istimewanya ini. Sebagai seorang dokter yang telah berpengalaman, Mardjono tahu waktunya tidak akan lama lagi. Dengan sangat hati-hati dan penuh hormat, dia memeriksa denyut nadi Soekarno. Dengan sisa kekuatan yang masih ada, Soekarno menggerakkan tangan kanannya, memegang lengan dokternya. Mardjono merasakan panas yang demikian tinggi dari tangan yang amat lemah ini. Tiba-tiba tangan yang panas itu terkulai. Detik itu juga Soekarno menghembuskan nafas terakhirnya. Kedua matanya tidak pernah mampu lagi untuk membuka. Tubuhnya tergolek tak bergerak lagi. Kini untuk selamanya.

Situasi di sekitar ruangan sangat sepi. Udara sesaat terasa berhenti mengalir. Suara burung yang biasa berkicau tiada terdengar. Kehampaan sepersekian detik yang begitu mencekam. Sekaligus menyedihkan.

Dunia melepas salah seorang pembuat sejarah yang penuh kontroversi. Banyak orang menyayanginya, tapi banyak pula yang membencinya. Namun semua sepakat, Soekarno adalah seorang manusia yang tidak biasa. Yang belum tentu dilahirkan kembali dalam waktu satu abad. Manusia itu kini telah tiada.

Dokter Mardjono segera memanggil seluruh rekannya, sesama tim dokter kepresidenan. Tak lama kemudian mereka mengeluarkan pernyataan resmi: Soekarno telah meninggal.

Berita kematian Bung Karno dengan cara yang amat menyedihkan menyebar ke seantero Pertiwi. Banyak orang percaya bahwa Bung Karno sesungguhnya dibunuh secara perlahan oleh rezim penguasa yang baru ini. Bangsa ini benar-benar berkabung. Putera Sang Fajar telah pergi dengan status tahanan rumah. Anwari Doel Arnowo, seorang saksi sejarah yang hadir dari dekat saat prosesi pemakaman Bung Karno di Blitar dalam salah satu milis menulis tentang kesaksiannya. Berikut adalah kesaksian dari Cak Doel Arnowo yang telah kami edit karena cukup panjang:

Pagi-pagi, 21 Juni 1970, saya sudah berada di sebuah lubang yang disiapkan untuk kuburan manusia. Sederhana sekali dan sesederhana semua makam di sekelilingnya. Sudah ada sekitar seratusan manusia hidup berada di situ dan semua hanya berada di situ, tanpa mengetahui apa saja tugas mereka sebenarnya. Yang jelas, semuanya bermuka murung. Ada yang matanya penuh airmata, tetapi bersinar dengan garang. Kelihatan roman muka yang marah. Ya, saya pun marah. Hanya saja saya bisa menahan diri agar tidak terlalu kentara terlihat oleh umum.

Kita semua di kota Malang mendengar tentang almarhum yang diberitakan telah meninggal dunia sejak pagi hari dan sudah menyiapkan diri untuk menunggu keputusan pemakamannya di mana. Sesuai amanat almarhum, seperti sudah menjadi pengetahuan masyarakat umum, Bung Karno meminta agar dimakamkan di sebuah tempat di pinggir kali di bawah sebuah pohon yang rindang di Jawa Barat (asumsi semua orang adalah di rumah Bung Karno di Batu Tulis di Bogor).

Tetapi lain wasiat dan amanah, lain pula rezim Soeharto yang secara sepihak memutuskan jasad Bung Karno dimakamkan di Blitar dengan dalih bahwa Blitar adalah kota kelahirannya. Ini benar-benar ceroboh. Bung Karno lahir di Surabaya di daerah Paras Besar, bukan di Blitar! Bung Karno terlahir dengan nama Koesno, dan ikut orang tuanya yang jabatan ayahnya, Raden Soekemi Sosrodihardjo, adalah seorang guru yang mengajar di sebuah Sekolah di Mojokerto dan kemudian dipindah ke Blitar. Di sinilah ayah Bung Karno, meninggal dunia dan dimakamkan juga di sisinya, isterinya (yang orang Bali ) bernama Ida Ayu Nyoman Rai.

Setelah matahari tinggal sepenggalan sebelum terbenam, rombongan jenazah Bung Karno akhirnya sampai di tempat tujuan. Yang hadir didorong-dorong oleh barisan tentara angkatan darat yang berbaris dengan memaksa kumpulan manusia agar upacara dapat dilaksanakan dengan layak.

Tampak Komandan Upacara jenderal Panggabean memulai upacara dan kebetulan saya berdiri berdesak-desakan di samping Bapak Kapolri Hoegeng Iman Santosa, yang sedang sibuk berbicara dengan suara ditahan agar rendah frekuensinya tidak mengganggu suara aba-aba yang sudah diteriak-teriakkan. Saya berbisik kepada beliau, ujung paling belakang rombongan ini berada di mana? Beliau menjawab singkat di kota Wlingi. Hah?! Sebelas kilometer panjangnya iring-iringan rombongan ini sejak dari lapangan terbang Abdulrachman Saleh di Singosari, Utara kota Malang.

Pak Hoegeng yang sederhana itu kelihatan murung dan sigap melakukan tugasnya. Dia berbisik kepada saya: "There goes a very great man!!" Saya terharu mendengarnya. Apalagi ambulans (mobil jenazah) yang mengangkut Bung Karno terlalu amat sederhana bagi seorang besar seperti beliau. Saya lihat amat banyak manusia mengalir seperti aliran sungai dari pecahan rombongan pengiring. Sempat saya tanyakan, ada yang mengaku dari Madiun, dari Banyuwangi bahkan dari Bali.

Kuburannya Pun Tidak Boleh Dijenguk

Sejarah mencatat, sejak 1971 sampai 1979, makam Bung Karno tidak boleh dikunjungi umum dan dijaga sepasukan tentara. Kalau mau mengunjungi makam harus minta izin terlebih dahulu ke Komando Distrik Militer (KODIM). Apa urusannya KODIM dengan izin mengunjungi makam?

Saya bersama ibu saya dan beberapa saudara datang secara mendadak pergi ke Blitar dengan tujuan utama ziarah ke Makam Bung Karno. Tanpa ragu kita ikuti aturan dan akhirnya sampai ke pimpinannya yang paling tinggi. Saya ikut sampai di meja pemberi izin dan sudah ditentukan oleh kita bersama, bahwa salah satu saudara saya saja yang berbicara. Saya sendiri meragukan emosi saya, bisakah saya bertindak tenang terhadap isolasi kepada sebuah makam oleh Pemerintah atau rezim? Nah, ternyata meskipun tidak terlalu ramah, mereka melayani dengan muka seperti dilipat. Mungkin dengan menunjukkan muka seperti itu merasa bertambah rasa gagahnya terhadap rakyat biasa macam kami. Akhirnya semua beres dan kami mendapat sepucuk surat. Apa yang terjadi?

Sesampainya di makam kami turun dari kendaraan kami dan saya bawa surat izin dari KODIM. Surat itu kami tunjukan ke tentara yang jaga makam. Waktu tentara itu baca surat, saya terdorong untukmenoleh ke belakang. Terkejut saya. Selain rombongan sendiri, Ibu saya dan saudara-saudara, telah mengikuti kami sebanyak lebih dari tiga puluh orang, bergerombol. Mereka, orang-orang yang tidak kami kenal sama sekali, melekat secara rapat dengan rombongan kami. Saya lupa persis bagaimana, akan tetapi saya ingat kami memasuki pagar luar dan kami bisa mendekat sampai ke dinding kaca tembus pandang dan hanya memandang makamnya dari jarak, yang mungkin hanya sekitar tiga meter.

Para pengikut dadakan yang berada di belakang rombongan kami dengan muka berseri-seri, merasa beruntung dapat ikut masuk ke dalam lingkungan pagar luar itu. Ada yang bersila, memejamkan mata dan mengatupkan kedua tangannya, posisi menyembah. Saya tidak memperhatikannya, tetapi jelas dia bukan berdoa cara Islam. Mereka khusyuk sekali dan waktu kami kembali menuju ke kendaraan kami, beberapa di antara mereka menjabat tangan dan malah ada yang menciumnya, membuat saya merasa risih.

Salah seorang dari mereka ini mengatakan bahwa dia sudah dua hari bermalam di sekitar situ di udara terbuka menunggu sebuah kesempatan seperti yang telah terjadi tadi. Tanpa kata-kata, saya merasakan getar hati rakyat, rakyat Marhaen kata Bung Karno! Mereka menganggap Bung Karno bukan sekedar Proklamator, tetapi seorang Pemimpin mereka dan seorang Bapak mereka. Apapun yang disebarluaskan dan berlawanan arti dengan kepercayaan mereka itu semuanya dianggap persetan. Dalam hubungan Bung Karno dengan Rakyat, tidak ada unsur uang berbicara.

Dibunuh Perlahan

Keyakinan orang banyak bahwa Bung Karno dibunuh secara perlahan mungkin bisa dilihat dari cara pengobatan proklamator RI ini yang segalanya diatur secara ketat dan represif oleh Presiden Soeharto. Bung Karno ketika sakit ditahan di Wisma Yasso Jl. Gatot Subroto. Penahanan ini membuatnya amat menderita lahir dan bathin. Anak-anaknya pun tidak dapat bebas mengunjunginya.

Banyak resep tim dokternya, yang dipimpin dr. Mahar Mardjono, yang tidak dapat ditukar dengan obat. Ada tumpukan resep di sebuah sudut di tempat penahanan Bung Karno. Resep-resep untuk mengambil obat di situ tidak pernah ditukarkan dengan obat. Bung Karno memang dibiarkan sakit dan mungkin dengan begitu diharapkan oleh penguasa baru tersebut agar bisa mempercepat kematiannya.

Permintaan dari tim dokter Bung Karno untuk mendatangkan alat-alat kesehatan dari Cina pun dilarang oleh Presiden Soeharto. "Bahkan untuk sekadar menebus obat dan mengobati gigi yang sakit, harus seizin dia, " demikian Rachmawati Soekarnoputeri pernah bercerita.
(Dari website Andi Santoso, 1 Februari 2010)
* * *





 Menjelang Wafatnya Soekarno (1)

Berkas yang Hilang

Oleh Wahyu Dramastuti
Copyright © Sinar Harapan

Diposkan  27/07/2009



JAKARTA – Sembilan buku besar tertumpuk rapih di salah satu ruangan di rumah Rachmawati Soekarnoputri, Jl. Jati Padang Raya No. 54 A, Pejaten, Jakarta Selatan. Buku bertuliskan tangan itu berisi medical record (catatan medis) mantan Presiden Soekarno selama sakit di Wisma Yaso, Jakarta.
Ada pula tujuh lembar kertas tua yang warnanya sudah memudar kecokelatan. Ini juga menjadi bukti riwayat penyakit Bung Karno. Kopnya bertuliskan Institut Pertanian Bogor, Fakultas Kedokteran Hewan Bagian Bakteriologi, Djl. Kartini 14, telpon 354, Bogor. Tapi yang lebih membuat dahi ini berkernyit keras, nama pasien disamarkan. Misalnya, ada yang tertera namanya Taufan (salah seorang putra Soekarno).

Menguak peristiwa yang terjadi tahun 1965-1970 itu memang tidak mudah. Pada masa lalu
membicarakan masalah ini secara terbuka menjadi hal tabu. Maka tak heran jika sekarang
banyak orang, terutama generasi muda, tak mengetahui kebenaran sejarah tersebut. Namun kini, ketika semua mata dan seluruh perhatian tertumpah di Rumah Sakit Pusat Pertamina (RSPP) sehubungan dengan sakitnya mantan Presiden Soeharto sejak 4 Januari 2008, rasa ingin tahu tentang masa lalu pun kembali mengusik. Itu semata-mata karena Soeharto dan Soekarno sama-sama mantan kepala negara.

Adalah Rachmawati Soekarnoputri, putri ketiga Soekarno, yang sangat ingin menyerahkan catatan medis
ayahnya kepada pemerintah. “Ini kalau pemerintah butuh data-data pendukung dan ingin melihat dari segi kebenaran, bukan hanya cerita fiktif,” tutur Rachmawati kepada SH di kediamannya, Sabtu (19/1) sore. Maklum, seorang mantan menteri Orde Baru pernah berkomentar bahwa perlakuan terhadap Soekarno ketika sakit tidak sekejam itu. “Saya tak mau gegabah.
Ini bukan make up story, karena Kartono Mohamad saja (saat itu Ketua Ikatan Dokter Indonesia/IDI-red), mengatakan perawatan terhadap Bung Karno seperti perawatan terhadap keluarga sangat miskin,” kata Rachmawati.

Di sore hari itu, Rachmawati tidak sanggup bercerita banyak. Ia hanya tersedu sedan, hal itu
sudah menggambarkan betapa getir kenangan yang dialaminya. Tetapi sebuah artikel yang pernah
dimuat SH pada 15 Mei 2006, memberikan gambaran lebih lengkap. “Seorang perempuan muncul di
Kantor IDI di Jakarta, awal 1990-an,” demikian kalimat pertama artikel tersebut. Perempuan itu ingin bertemu Kartono Mohamad untuk menyerahkan 10 bundel buku berisi catatan para perawat jaga Soekarno.

Namun jauh sebelum pertemuan itu, Kartono bertemu Wu Jie Ping, dokter yang pernah merawat Soekarno di Hong Kong. Wu mengungkapkan bahwa Soekarno “hanya” mengalami stroke ringan akibat penyempitan sesaat di pembuluh darah otak saat diberitakan sakit pada awal Agustus 1965, dan sama sekali tidak mengalami koma seperti isu yang beredar. Ini menepis spekulasi bahwa Soekarno tidak akan mampu menyampaikan pidato kenegaraan pada peringatan hari proklamasi 17 Agustus 1965. Dan nyatanya, Soekarno tetap hadir pada peringatan detik-detik proklamasi 17 Agustus itu di Istana Merdeka, lengkap dengan tongkat komandonya.

Diperiksa Dokter Hewan

Setelah kembali lagi ke Jakarta, Kartono menemui Mahar Mardjono, dokter yang tahu banyak soal stroke.
Rupanya Kartono tak hanya bercerita soal stroke, tapi juga rentetan kejadian yang dengan sengaja menelantarkan Soekarno. Maka bundel buku yang dibawa perempuan itu semakin menguatkan kegelisahan Kartono.

Namun Indonesia di awal 1990-an, kebenaran hanya boleh ditentukan oleh penguasa. Maka bundel buku itu hanya teronggok di meja kerja Kartono selama bertahun-tahun. Hingga kemudian, krisis moneter meledak.

Rakyat turun ke jalan dan Presiden Soeharto, yang telah berkuasa selama 32 tahun, dipaksa meletakkan jabatan. Indonesia berubah wajah. Kartono pun teringat onggokan buku itu.
Ia bergegas ke RSPAD, rumah sakit yang mempekerjakan empat perawat di Wisma Yaso.
Kartono berharap dapat menemukan mereka, agar bangsa Indonesia mendapat cerita yang lengkap
tentang tahun-tahun terakhir Soekarno. Namun menemukan Dinah, Dasih, J. Sumiati, dan Masnetty ternyata bukan hal mudah. Seorang di antara mereka meninggal, sedangkan yang lain sudah pensiun.
RSPAD pun mendadak tak memiliki file atau berkas dari para perawat ini.

Kartono kehilangan jejak. Upayanya untuk mencari medical record Soekarno gagal. Pihak RSPAD
mengatakan bahwa keluarga Soekarno telah membawanya. Ketika ini ditanyakan kepada
Rachmawati, ia hanya geleng-geleng kepala. “Tidak, tidak,” jawabnya lirih.
Yang membuatnya semakin terenyuh, sebelum dibawa ke Jakarta, Soekarno ditangani oleh dokter
Soerojo yang seorang dokter hewan. Jejak ini terlihat dari berkas berkop Institut Pertanian Bogor, Fakultas Kedokteran Hewan Bagian Bakteriologi.

Bahkan setelah dipindah ke RSPAD karena sakit ginjalnya semakin parah, upaya untuk melakukan cuci darah tidak dapat dilakukan dengan alasan RSPAD tidak mempunyai peralatan. Catatan medis juga menyebutkan obat yang diberikan hanya vitamin (B12, B kompleks, royal jelly) dan Duvadillan, obat untuk mengurangi penyempitan pembuluh darah perifer. Perihal tekanan darah tinggi yang juga disebutkan dalam catatan medis, juga menyisakan tanya pada diri Rachmawati.

Setiap kali menjenguk sang ayah dan mencicipi makanannya, masakan selalu terasa asin. “Saya kecewa dengan semua perawatan itu. Ini sama saja dengan membiarkan orang berlalu,” lanjut Rachmawati.
Seorang mantan pejabat di era Presiden Soekarno membenarkan terjadinya fakta seputar masa sakit Soekarno yang tersia-sia. “Tidak seperti sekarang ini, perawatan terhadap Soeharto. Sangat berbeda, padahal seharusnya semua mantan presiden berhak dirawat secara all out dan diongkosi oleh negara,” katanya.

Purnawirawan perwira tinggi militer itu juga mengungkapkan, perlakuan seragam terhadap Soekarno berasal dari sebuah instruksi. “Yang memberi instruksi ya orang yang sekarang sedang dirawat itu,” katanya.
Namun pria ini enggan dituliskan namanya. “Wah, kalau ditulis di koran saya pasti digangguin…,” tuturnya dengan nada serius.



Menjelang Wafatnya sang Proklamator (2-Habis)

Mengistimewakan Soeharto, Menyia-nyiakan Soekarno


JAKARTA – Selembar foto hitam putih menguak penderitaan Soekarno ketika tergolek sakit di
Wisma Yaso, Jakarta, 15 hari sebelum ia wafat. Kedua pipinya terlihat bengkak, gejala fisik pasien gagal ginjal.

Matanya sedikit terbuka, tapi tanpa ekspresi. Raut wajahnya menampak kepasrahan yang begitu dalam.
Soekarno terlihat berbaring di atas sofa berukuran sempit dengan sebuah bantal.
Kedua tangannya dicoba ditangkupkan. Siapa sangka, pria gagah inilah sang proklamator yang mengantarkan Negara Indonesia ke pintu kemerdekaan hingga detik ini.

Gambar ini dibuat secara diam-diam oleh Rachmawati Soekarnoputri bersama Guruh Soekarnoputra,
6 Juni 1970. Sebuah momentum bertepatan dengan hari ulang tahun Soekarno yang ke-69.
Dik, ikut yuk, saya mau motret bapak,” tutur Rachmawati kepada adiknya, Guruh, kala itu.
Rupanya foto tersebut kemudian dikirimkan Rachmawati ke Kantor Berita AP dan dimuat di Harian Sinar Harapan.

Maka gemparlah, dan Rachmawati diinterogasi oleh Corps Polisi Militer
(CPM). Rachmawati pun bertanya, ”Mengapa dilarang memotret, memangnya status Bung Karno apa?”
Tetapi tak pernah ada jawaban tentang apa status Soekarno, sampai detik ini. ”Jadi semua
serbasumir. Tak ada kejelasan tentang status bapak sampai bapak meninggal,” kata Rachmawati
kepada SH di kediamannya di Jl. Jati Padang Raya No. 54A, Pejaten, Jakarta Selatan, Sabtu (19/1) sore.
Di saat kondisi penyakit ayahnya semakin kritis dan putra-putri ingin membesuknya, mereka
tetap harus melapor dulu ke Pomdam Jaya, sehingga tidak dapat menengok setiap hari. Wisma
Yaso yang terletak di Jl. Gatot Subroto, Jakarta, itu padahal merupakan kediaman istri
Soekarno, yakni Dewi Soekarno. Begitu pula Soekarno sendiri, dalam kondisi sakit masih tetap
harus menjalani pemeriksaan oleh Kopkamtib tiga bulan sekali.

Situasi seperti itu semakin menambah kalut keluarga Soekarno, setelah sebelumnya didera
cobaan bertubi-tubi. Pada pertengahan 1965, sebuah rumor mengabarkan Soekarno mengalami koma
sehingga diperkirakan tidak bisa menyampaikan pidato kenegaraan pada peringatan Proklamasi
17 Agustus 1965. Tetapi nyatanya, Soekarno hadir pada peringatan detik-detik proklamasi
tersebut di Istana Merdeka, lengkap dengan pakaian kebesaran dan tongkat komandonya.
Tahun berikutnya, setelah Surat Perintah Sebelas Maret (Supersemar) diteken 11 Maret 1966, keluarga Soekarno mendapat kiriman radiogram pada tahun 1967, berupa perintah supaya mereka keluar dari Istana Bogor.

Maka kemudian di tahun 1968, Soekarno pindah dari Istana Bogor ke Batu Tulis, masih di wilayah Bogor.
Ternyata hawa dingin di Bogor membuat penyakit rematik Bung Karno semakin parah. Saat itu Soekarno ditangani oleh dokter Soerojo, dokter hewan. Bukti ini dikuatkan dengan dokumen riwayat penyakit Bung Karno di atas tujuh lembar kertas tua yang warnanya sudah memudar kecokelatan.
Kopnya bertuliskan Institut Pertanian Bogor, Fakultas Kedokteran Hewan Bagian Bakteriologi,
Djl. Kartini 14, telpon 354, Bogor. Menurut Rachmawati, penanganan dokter Soerojo saat itu pun hanya bersifat insidental.

Lantaran rasa sakit makin tak tertahankan, akhirnya Soekarno mengutus Rachmawati untuk
menyampaikan surat permohonan kepada Soeharto agar diperbolehkan kembali ke Jakarta.
Saya diterima Pak Harto di Cendana, menyampaikan permintaan agar bapak dipindah ke Jakarta.
Saya harus menunggu jawabannya dua minggu, aduh…,” tutur Rachmawati tak habis pikir.
Akhirnya, Soekarno dipindah ke Wisma Yaso di Jakarta, yang sekarang menjadi Museum Satria Mandala.
Beberapa minggu kemudian dibentuklah tim dokter dengan ketua Mahar Mardjono. Tetapi karena
Wisma Yaso hanya rumah biasa, tentu saja fasilitas medis yang tersedia sangat berbeda dengan jika dirawat di rumah sakit.

Tidak Cuci Darah

Hari-hari berikutnya, kondisi Soekarno menurun drastis. Seperti yang terdokumentasi dalam
foto Rachmawati tersebut, pipi Soekarno sudah membengkak, pertanda mengalami gagal ginjal.
Kondisi makin kritis. Hingga akhirnya pada 11 Juni 1970 mantan Presiden I RI itu dilarikan
ke Rumah Sakit Pusat Angkatan Darat (RSPAD) Gatot Subroto.

Rupanya pindah perawatan ke rumah sakit tidak seluruhnya menyelesaikan masalah. Perawatan
yang diberikan tetap tidak maksimal dan peralatan medis seadanya. Pada saat dokter
memastikan harus dilakukan cuci darah, upaya satu-satunya ini tidak dapat dilaksanakan.
Alasannya, RSPAD tidak memiliki peralatan untuk cuci darah. ”Ini sama saja membiarkan orang
cepat berlalu…,” tutur Rachmawati.

Begitu pula dengan obat-obatan, tidak tersedia di RSPAD sehingga putra-putri Soekarno
membelinya sendiri di apotek di Kebayoran. ”Ada satu periode di mana saya tidak boleh
menengok bapak,” ungkap Rachmawati. Dalam keadaan genting seperti itu, keluarga masih tetap
tidak diizinkan tidur di rumah sakit untuk menunggui sang ayah. Mereka hanya boleh menunggu
di dalam mobil di tempat parkir.

Tak ada pula teman-teman yang bisa menjenguknya, kecuali Mohammad Hatta. Rachmawati
mengakui, selepas tahun 1965 memang terjadi de-Soekarno-isasi. Semua hal yang berbau
Soekarno harus disingkirkan sejauh mungkin.

Hingga akhirnya pada 21 Juni 1970, Soekarno wafat. Jenazahnya kemudian disemayamkan di Wisma
Yaso dan dilepas oleh Presiden Soeharto untuk diterbangkan ke Blitar, Jawa Timur. Itulah
kali pertama Soeharto melihat fisik Soekarno setelah disingkirkan dari Istana Kepresidenan.
Yang menjadi inspektur upacara pada pemakaman itu Jenderal TNI M Panggabean, tanpa kehadiran
Presiden Soeharto di Blitar.

Mengapa pusara mantan Presiden I RI itu berada di kompleks pemakaman Desa Bendogerit,
Blitar? Lokasi itu dipilihkan oleh negara dengan alasan dekat dengan makam kedua orangtua
Soekarno. Pihak keluarga sebenarnya mengajukan permintaan sesuai wasiat Soekarno, yaitu
dimakamkan di Batu Tulis atau di lokasi lain di Bogor.

Juga ketika gaung agar Tap MPR No. 11 Tahun 1998 tentang KKN mantan Presiden Soeharto
dikumandangkan kembali tatkala Soeharto sakit di Rumah Sakit Pusat Pertamina (RSPP),
Rachmawati meminta dicabutnya Tap MPRS No. XXXIII/1967.

Tap MPRS itu intinya adalah mencabut mandat MPRS dari Soekarno dan mengangkat Soeharto
sebagai Pejabat Presiden. ”Secara pribadi saya berharap ada rehabilitasi. Dan Tap MPRS harus
dicabut karena beban politik berbeda dengan beban pidana atau perdata,” lanjut Rachmawati.
Putri ketiga Soekarno itu pun hanya geleng-geleng kepala, ketika mengetahui kondisi kesehatan Soeharto yang sedang dirawat di RSPP mulai membaik. Semua orang juga tahu, betapa
luar biasa dan istimewanya fasilitas untuk Soeharto.
Tetapi Rachmawati sama sekali tidak menaruh rasa dendam. Yang ia inginkan hanyalah,
kebenaran sejarah.

 ===    ===        = ===       
Soekarno - Sejarah yang tak memihak

Oleh : Iman Brotoseno, 13 January 2008
. Hawa panas dan angin seolah diam tak berhembus. Malam ini saya bermalam di rumah ibu saya. Selain rindu masakan sambel goreng ati yang dijanjikan, saya juga ingin ia bercerita mengenai Presiden Soekarno. Ketika semua mata saat ini sibuk tertuju, seolah menunggu saat saat berpulangnya Soeharto, saya justru lebih tertarik mendengar penuturan saat berpulang Sang proklamator. Karena orang tua saya adalah salah satu orang yang pertama tama bisa melihat secara langsung jenasah Soekarno.
Saat itu medio Juni 1970. Ibu yang baru pulang berbelanja, mendapatkan Bapak ( almarhum ) sedang menangis sesenggukan.
“ Pak Karno seda “ ( meninggal )

Dengan menumpang kendaraan militer mereka bisa sampai di Wisma Yaso. Suasana sungguh sepi. Tidak ada penjagaan dari kesatuan lain kecuali 3 truk berisi prajurit Marinir ( dulu KKO ). Saat itu memang Angkatan Laut, khususnya KKO sangat loyal terhadap Bung Karno. Jenderal KKO Hartono - Panglima KKO – pernah berkata ,
“ Hitam kata Bung Karno, hitam kata KKO. Merah kata Bung Karno, merah kata KKO “

Banyak prediksi memperkirakan seandainya saja Bung Karno menolak untuk turun, dia dengan mudah akan melibas Mahasiswa dan Pasukan Jendral Soeharto, karena dia masih didukung oleh KKO, Angkatan Udara, beberapa divisi Angkatan Darat seperti Brawijaya dan terutama Siliwangi dengan panglimanya May.Jend Ibrahim Ajie.

Namun Bung Karno terlalu cinta terhadap negara ini. Sedikitpun ia tidak mau memilih opsi pertumpahan darah sebuah bangsa yang telah dipersatukan dengan susah payah. Ia memilih sukarela turun, dan membiarkan dirinya menjadi tumbal sejarah.

The winner takes it all. Begitulah sang pemenang tak akan sedikitpun menyisakan ruang bagi mereka yang kalah. Soekarno harus meninggalkan istana pindah ke istana Bogor. Tak berapa lama datang surat dari Panglima Kodam Jaya – Mayjend Amir Mahmud – disampaikan jam 8 pagi yang meminta bahwa Istana Bogor harus sudah dikosongkan jam 11 siang. Buru buru Bu Hartini, istri Bung Karno mengumpulkan pakaian dan barang barang yang dibutuhkan serta membungkusnya dengan kain sprei. Barang barang lain semuanya ditinggalkan.

“ Het is niet meer mijn huis “ – sudahlah, ini bukan rumah saya lagi , demikian Bung Karno menenangkan istrinya.
Sejarah kemudian mencatat, Soekarno pindah ke Istana Batu Tulis sebelum akhirnya dimasukan kedalam karantina di Wisma Yaso.
Beberapa panglima dan loyalis dipenjara. Jendral Ibrahim Adjie diasingkan menjadi dubes di London. Jendral KKO Hartono secara misterius mati terbunuh di rumahnya.
. Saat itu belum banyak yang datang, termasuk keluarga Bung Karno sendiri. Tak tahu apa mereka masih di RSPAD sebelumnya. Jenasah dibawa ke Wisma Yaso. Di ruangan kamar yang suram, terbaring sang proklamator yang separuh hidupnya dihabiskan di penjara dan pembuangan kolonial Belanda. Terbujur dan mengenaskan. Hanya ada Bung Hatta dan Ali Sadikin – Gubernur Jakarta – yang juga berasal dari KKO Marinir.

Bung Karno meninggal masih mengenakan sarung lurik warna merah serta baju hem coklat. Wajahnya bengkak bengkak dan rambutnya sudah botak.
Kita tidak membayangkan kamar yang bersih, dingin berAC dan penuh dengan alat alat medis disebelah tempat tidurnya. Yang ada hanya termos dengan gelas kotor, serta sesisir buah pisang yang sudah hitam dipenuhi jentik jentik seperti nyamuk. Kamar itu agak luas, dan jendelanya blong tidak ada gordennya. Dari dalam bisa terlihat halaman belakang yang ditumbuhi rumput alang alang setinggi dada manusia !.
Setelah itu Bung Karno diangkat. Tubuhnya dipindahkan ke atas karpet di lantai di ruang tengah.
Ibu dan Bapak saya serta beberapa orang disana sungkem kepada jenasah, sebelum akhirnya Guntur Soekarnoputra datang, dan juga orang orang lain.

Namun Pemerintah orde baru juga kebingungan kemana hendak dimakamkan jenasah proklamator. Walau dalam Bung Karno berkeingan agar kelak dimakamkan di Istana Batu Tulis, Bogor. Pihak militer tetap tak mau mengambil resiko makam seorang Soekarno yang berdekatan dengan ibu kota.
Maka dipilih Blitar, kota kelahirannya sebagai peristirahatan terakhir. Tentu saja Presiden Soeharto tidak menghadiri pemakaman ini.
Dalam catatan Kolonel Saelan, bekas wakil komandan Cakrabirawa,
“ Bung karno diinterogasi oleh Tim Pemeriksa Pusat di Wisma Yaso. Pemeriksaan dilakukan dengan cara cara yang amat kasar, dengan memukul mukul meja dan memaksakan jawaban. Akibat perlakuan kasar terhadap Bung Karno, penyakitnya makin parah karena memang tidak mendapatkan pengobatan yang seharusnya diberikan. “
( Dari Revolusi 1945 sampai Kudeta 1966 )
Dr. Kartono Muhamad yang pernah mempelajari catatan tiga perawat Bung Karno sejak 7 februari 1969 sampai 9 Juni 1970 serta mewancarai dokter Bung Karno berkesimpulan telah terjadi penelantaran. Obat yang diberikan hanya vitamin B, B12 dan duvadillan untuk mengatasi penyempitan darah. Padahal penyakitnya gangguan fungsi ginjal. Obat yang lebih baik dan mesin cuci darah tidak diberikan.
( Kompas 11 Mei 2006 )
Rachmawati Soekarnoputri, menjelaskan lebih lanjut,
“ Bung Karno justru dirawat oleh dokter hewan saat di Istana Batutulis. Salah satu perawatnya juga bukan perawat. Tetapi dari Kowad “
( Kompas 13 Januari 2008 )
dengan perlakuan terhadap mantan Presiden Soeharto, yang setiap hari tersedia dokter dokter dan peralatan canggih untuk memperpanjang hidupnya, dan masih didampingi tim pembela yang dengan sangat gigih membela kejahatan yang dituduhkan. Sekalipun Soeharto tidak pernah datang berhadapan dengan pemeriksanya, dan ketika tim kejaksaan harus datang ke rumahnya di Cendana. Mereka harus menyesuaikan dengan jadwal tidur siang sang Presiden !
Malam semakin panas. Tiba tiba saja udara dalam dada semakin bertambah sesak. Saya membayangkan sebuah bangsa yang menjadi kerdil dan munafik. Apakah jejak sejarah tak pernah mengajarkan kejujuran ketika justru manusia merasa bisa meniupkan roh roh kebenaran ? Kisah tragis ini tidak banyak diketahui orang. Kesaksian tidak pernah menjadi hakiki karena selalu ada tabir tabir di sekelilingnya yang diam membisu. Selalu saja ada korban dari mereka yang mempertentangkan benar atau salah.
Butuh waktu bagi bangsa ini untuk menjadi arif.
