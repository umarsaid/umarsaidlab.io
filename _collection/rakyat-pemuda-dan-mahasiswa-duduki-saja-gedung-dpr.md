---
layout: collection
title: Rakyat, pemuda dan mahasiswa, duduki saja gedung DPR !!!
---

Kalau rencana pembangunan gedung baru DPR
tetap ngotot mau diteruskan juga



Di bawah berikut ini disajikan sebuah tulisan yang diambil dari Kompasiana.Com (Kompas) tanggal 4 September 2010. Tulisan yang bagus ini secara tajam dan terus-terang – yang diselingi dengan berbagai ungkapan yang sinis tetapi tepat -- mengemukakan fikiran dan perasaan banyak orang mengenai rencana pembangunan gedung baru DPR.


Rencana ini  -- yang merupakan gagasan gila dari orang-orang yang sudah miring otaknya dan mempunyai tujuan yang haram atau bathil --  sudah menjadi sasaran kritik pedas (termasuk cemooh atau hujatan) dari banyak kalangan yang marah.
Namun, ketua DPR yang merangkap sebagai Ketua BURT (Badan Urusan Rumah Tangga), Marzuki Alie, tetap ngotot mau terus melaksanakannya, walaupun banyak suara yang menentang. Ia mengatakan, antara lain, bahwa rencana pembangunan gedung baru DPR ini tidak bisa dihentikan, karena sudah dikeluarkan beaya untuk konsultan dan lain-lain.


Melihat berbagai pernyataan dari para pendukung pembangunan gedung baru DPR ini sampai sekarang, maka ada kemungkinan bahwa mereka akan berusaha sekuat mungkin  - dan dengan segala cara dan jalan – untuk tetap meneruskan  rencana gila-gilaan ini.
Oleh karena itu, seluruh kekuatan demokratis dalam masyarakat, tidak peduli dari golongan apapun juga (nasionalis, agama dan sosialis atau komunis) perlu bersuara dengan lantang, dan juga bersama-sama melancarkan berbagai macam aksi, untuk melawan diteruskanya projek pembangunan gedung baru DPR ini.


Kalau suara dan aksi-aksi oleh berbagai golongan ini tidak digubris juga oleh para pejabat dan tokoh-tokoh yang bertanggungjawab tentang rencana ini, maka seperti ditulis dalam Kompasiana itu, perlu diadakan shock therapy, yang berbentuk cara-cara yang lebih radikal. Umpamanya,  dengan  “massa rakyat/mahasiswa menyerbu dan menduduki gedung DPR itu, sebagaimana pernah dilakukan oleh massa mahasiswa untuk menurunkan Presiden Soeharto pada Mei 1998.


"Karena rasanya tabiat segerombolan orang yang katanya terhormat ini semakin mirip dengan rezim Orde Baru saja,yang rakus kekuasaan, rakus harta, dan rakus kemewahan di atas kemiskinan rakyat. Untuk itu terapinya harus sama dengan terapi ketika berhasil menjatuhkan Orde Baru itu.” tulis artikel panjang dalam Kompasiana tersebut.


Pendudukan gedung DPR oleh massa rakyat/pemuda/mahasiswa ini tidaklah berarti melecehkan simbol-simbol negara, melainkan sebagai tindakan untuk menyelamatkannya, dan bahkan mendudukkannya di tempat yang sesuai dengan jiwa dan fungsi dewan yang betul-betul mewakili rakyat.


Pendudukan  (walaupun untuk sementara waktu) gedung DPR, yang merupakan shock therapy ini, berarti bahwa kepercayaan rakyat terhadap DPR  (yang sekarang ) sudah hancur sama sekali, dan respek publik juga sudah hilang sama sekali, karena adanya rencana gedung baru itu.


Paris, 6 September 2010

Umar Said
=  = =


Berikut adalah teks artikel dalam Kompasiana tanggal 4 September 2010, yang  disajikan secara utuh atau selengkapnya :


“Setelah alasan untuk membangun gedung baru DPR yakni gedung lamanya sudah miring tujuh derajat, dan takut kalau gempa nanti gedungnya roboh, dipatahkan, DPR rupanya tidak kehilangan akal. Sebenarnya sudah kehilangan akal, yakni akal sehatnya.
Yang tidak hilang itu akalnya yang rupanya yang miring tujuh derajat.  Sehingga gedungnya normal dia bilang miring. Berarti yang miring bukan gedungnya, tetapi otak para DPR-wan. Maka, tidak heran yang keluar dari mereka itu rada-rada miring juga.
Dengan demikian kalau pun benar nanti ada gempa bumi, dan gedungnya sampai roboh, barangkali kita harus merelakan mereka yang punya otak miring itu sampai seperti orang kecanduan narkoba, ngeyel tetap mau membangun gedung yang baru itu, tertimbun reruntunhan gedungnya sendiri.


Barangkali perlu setiap anggota DPR itu diperiksa tingkat kewarasannya oleh psikater yang berpengalaman.
Bagaimana tidak dikatakan miring otaknya, misalnya, dari ucapan anggota BURT DPR Michael Wattimena,  yang bilang bahwa salah satu alasan dibangunnya fasilitas kolam renang di lantai 36 (paling atas) tersebut karena kolam renang itu bisa berfungsi ganda, selain dipakai sebagai kolam renang, juga air kolamnya dapat difungsikan sebagai pemadam kebakaran jika terjadi kebakaran di gedung itu! (detik.com, 04/09/2010).


Kalau otaknya tidak miring, tentu para DPR-wan dengan melihat kondisi ekonomi negara yang seperti sekarang ini, pengangguran dan rakyat miskin yang makin banyak, kondisi rawan pangan yang semakin mengkhawatirkan, plus kinerja mereka yang sangat memprihatinkan itu, tidak akan ngeyel terus untuk membangun gedung baru itu.
Bukan hanya baru, tetapi dengan klasifikasi super mewah. Bagaimana tidak super mewah, kalau biaya pembangunannya saja bakal memakan biaya sedikitnya Rp. 1,6 trilun! Itu baru biaya pembanguan gedungnya saja. Belum dengan interior,  perlengkapan kantornya, perabot-perabotnya, dan tak kalah besarnya adalah biaya perawatan rutinnya.
Kompas  menghitung, dengan anggara sebesar itu, berarti untuk setiap satu ruangan kerja anggota DPR bakal menghabiskan minimal Rp. 2,8 miliar! Akan ada 560 ruangan super meah yang seperti ini. (Kompas, Kamis, 02/09/2010)


 Jika ditambahkan dengan interior, perlengkapan kantor, dan perabot-perabotnya yang sudah pasti harus baru dan mewah, entah negara harus menambah berapa rupiah lagi untuk setiap ruangan kerja anggota DPR yang akan didampingi satu orang sekretaris dan lima orang staf ahli, yang berarti juga menambah anggaran untuk menggaji mereka itu.
Semua gambaran bagaimana nanti supermewahnya gedung baru DPR itu yang setara dengan hotel bintang lima berlian itu tentu Anda semua sudah tahu.


Kalau semua itu terwujud, berarti Indonesia yang masih terbelit dengan masalah ekonomi dan rakyat yang susah pangan dan papan ini bakal mengungguli semua negara di dunia ini. Betapa tidak, rasanya negara semaju apapun dan sekaya apapun tidak ada yang punya gedung DPR semewah Republik Indonesia punya.


Demi terwujudnya gagasan gila ini, logika pun dibalik-balik. Tidak perduli publik mau menerimanya atau tidak. Yang penting supaya tidak dikatakan tidak responsif. Rakyat berteriak, orang gila tetap berlalu.
Seperti yang dinyatakan Ketua DPR Marzuki Alie bahwa rencana pembangunan gedung baru DPR itu tidak dapat dihentikan. Sebab, panitia kerja pembangunan juga sudah bekerja. Juga sudah membayar panjar ke pihak arsitek dan kontraktornya.  
“Mau tidak mau, suka tidak suka, apapun kritik dari masyarakat, kami terima saja. Tetapi, pembangunan tetap harus dijalankan,” katanya.


Siapa yang suruh kalian jalan sendiri tanpa mau dengar suara rakyat, tanpa mau perduli dengan anggaran yang pasti sangat membebani negara ini? Kalau semua memang sudah kerja, kenapa tidak bisa distop? Kalau telanjur sudah bayar panjar kepada arsitek dan kontraktor, dan tidak bisa diminta kembali, ya, kalian yang harus menggantikannya ke kas negara.
 Pemerintah yang diharapkan bisa mencegah rencana gila tersebut, ternyata ikut-ikutan tidak waras dengan mengatakan bahwa pemerintah tidak bisa mencegah rencana DPR tersebut dengan alasan sudah dianggarkan dan diputuskan.


Ketika ditanya, kenapa wakil pemerintah tidak menolak rencana pembangunan gedung DPR yang super mewah itu, Menteri Koordinator Perekonomian Hatta Rajasa menjawab bahwa karena waktu itu pemerintah tidak terlalu memperhatikan, tidak terlalu merinci melihat usulan dan rancangan pembangunan gedung tersebut, dan lain-lainnya.  Karena rincian tersebut adalah wewenang DPR.
Bukankah ini suatu jawaban yang tidak waras juga? Bagaimana bisa dengan tidak melihat secara rinci, kok pemerintah sebegitu mudahnya menyetujuinya? Hanya orang tidak waras saja yang menganggap anggaran jumbo sebesar Rp 1,6 triliun sampai Rp. 1,8 tiliun itu merupakan jumlah yang tidak perlu terlalu diperhatikan, sehingga dengan mudah setuju saja ketika diajukan untuk membangun sebuah gedung.


Dalam situasi negara yang serba memprihatinkan ini; krisis ekonomi, angka pengangguran dan kemiskinan yang terus meningkat,  krisis pangan dan papan, dan hutang luar negeri yang hampir mencapai Rp 1.700 triliun. Memberatkan APBN yang harus mencicilnya Rp. 100 triliun setiap tahun. Eh, DPR malah menambah beban negara demi memuaskan nafsu hedonis mereka dengan rencana pembangunan gedung baru DPR  yang super mewah tersebut.


Padahal kinerja DPR  periode ini juga sangat, sangat memprihatinkan, seperti yang datanya saya cantumkan di bagian akhir tulisan ini.
Salah satu logika yang dibalik-balik adalah bahwa karena kinerja DPR selama ini tidak memuaskan, maka perlu dibangun gedung baru DPR yang supermewah tersebut. Dengan demikian, maka akan memotivasi DPR bekerja lebih baik. Singkatnya gedung super mewah ini akan memperbaiki kinerja DPR!


Alasan yang sangat absurd. Seharusnya, tunjukkan dulu prestasi, kinerja kalian semua. Kalau sudah betul-betul sangat bagus dan memuaskan, barulah menuntut macam-macam. Itu pun kalau memang kalian bekerja demi pamrih pribadi. Bukan semata-mata demi bangsa dan negara sebagaimana isi sumpah jabatan anggota DPR kalian. Karena pejabat negara yang sungguh bekerja tanpa pamrih tentu walaupun dia kerja dengan sungguh-sungguh dengan hasil terbaiknya, tidak akan menuntut macam-macam.
Sebaliknya dengan DPR kita ini, sudah dapat gaji dan berbagai tunjangannya yang besar, plus berbagai fasilitas serba mewah yang memanjakan, tetapi  masih saja belum puas; tetap korup, tetap malas, tetap tidak punya prestasi apa-apa, malah sekarang menuntut yang jauh lebih fantastis lagi.


Alasan lain, karena kapasitas gedung DPR yang sekarang ini sudah tidak mencukupi, maka perlu dibangun gedung baru DPR itu.
Pertanyaannya, kalau pun memang betul demikian, apakah perlu dibangun gedung yang super mewah seperti itu? Kenapa tidak dibangun gedung yang jauh lebih sederhana saja?
Seorang pengamat mengatakan bahwa kalau memang alasan kapasitas, sebetulnya bisa dibangun gedung baru yang hanya memerlukan biaya sekitar Rp 400 miliar.


Tapi, menurut saya, kalau memang kapasitas gedung sudah tidak mencukupi untuk memuat 560 anggota DPR, yang dipangkas itu jumlah anggota DPR yang terlalu banyak dan mubazir itu saja.
Untuk apa punya 560 orang wakil rakyat, tetapi sebagian besarnya kerjanya hanya bermalas-malasan, suka tidur di sidang,  dan suka membolos itu? Tinggal dihitung saja, berapa dari 560 orang itu yang suka bermalas-malas, tidur di sidang DPR, atau membolos itu. Mereka itu saja yang dibuang. Karena selama ini hanya membuat sesak gedung DPR, dan membuat negara harus membayar gaji buta mereka.


Sebagai tindak lanjut sebaiknya jumlah partai politik dalam pemilu-pemilu mendatang perlu dikurangi. Jangan sebanyak seperti sekarang ini. Cukup dua, atau paling banyak tiga saja. Supaya lebih efektif dan efesien baik dari aspek ekonomi, maupun politik itu sendiri.
Sikap pemerintah yang begitu kompromis, atau lebih tepat dikatakan kooperatif dengan DPR  sangat patut diduga bahwa ini bagian dari semacam tawar-menawar kompensasi dalam meluluskan berbagai kebijakan masing-masing yang di masa akan datang pasti bakal ada lagi. Khususnya kebijakan-kebijakan yang kontroversial seperti rencana pembangunan gedung DPR ini.
Pemerintah sengaja memenuhi nafsu hedonis DPR ini sebagai bagian dari kompensasi agar kelak DPR juga akan mendukung hal-hal yang serupa dari pemerintah. Juga sebagai bagian dari bargaining power  masing-masing untuk saling menutupi dan melindungi kebobrokan masing-masing.


Contoh: Dalam kelanjutan pengusustan kasus skandal Bank Century,  dan pembahasan RUU tentang Pencegahan dan PemberantasanTindak Pindana Pencucian Uang.
DPR yang kita dikuasai partai pendukung pemerintah tidak akan melanjuti pengusutan lebih dalam kasus skandal Bank Century yang bisa mengarah kepada pucuk kekuasaan. Sebaliknya, sebagai kompensasinya, pemerintah akan (diam-diam) mendukung DPR  dalam pembahasan RUU Tindak Pidana Pencucian Uang.


Dalam pembahasan RUU tersebut, entah apa alasan sebenarnya, DPR keberatan kalau wewenang KPK diperluas sampai pada penyidikan kasus dugaan tindak pidaan pencucian uang. DPR juga keberatan dengan perluasan wewenang KPK untuk mengakses data dari  Pusat Pelaporan Analisa dan Transaksi Keuangan (PPATK), dan perluasan wewenang PPATK untuk melakukan penyelidikan dan pemblokiran rekening bank yang transaksinya mencurigakan. Hanya orang yang berbuat jahat dan yang bermaksud berbuat demikian yang punya alasan penolakan seperti itu.


Indikasinya sudah terlihat di DPR, yakni antara lain arah kesepakatan yang  membatasi wewenang KPK dengan hanya boleh memperoleh kopi Laporan Hasil Akhir  PPATK. PPATK juga tidak diberi kewenangan untuk melakukan penyelidikan dan pemblokiran rekening mencurigakan.
Kalau  fenomena ini benar, Indonesia akan menjadi seolah-olah dipimpin oleh segerombolan orang-orang yang rakus kekuasaan dan harta. Yang hanya menjadikan jabatan dan kekuasaan sebagai prasarana untuk mencapai maksud dan tujuan mereka tersebut.
Untuk memperkuat posisinya mereka memang harus bersatu untuk saling mendukung dalam upaya memperdayai rakyat dengan berbagai alasan dan logika yang terbalik-balik itu.


Ketua DPR Marzuki Alie yang tempo hari mukanya pucat menghadapi anggota DPR yang marah dan menyerbu ke arahnya ketika dia sebagai pimpinan sidang, menutup secara mendadak sidang paripurna DPR tentang kasus Bank Century pada 2 Maret 2010 lalu, sekarang sudah tumbuh nyalinya dan bisa bersuara keras. Ini seiring dengan setelah junjungannya Presiden SBY berkompromi dengan Ketua Umum Golkar, membuat posisi mereka semakin kuat di DPR.
Menghadapi berbagai kritik dari publik, dan kalangan internal DPR sendiri, Marzuki Alie marah. Marahnya ala Orde Baru. Katanya, kritikan-kritikan tersebut dilontarkan oleh pihak-pihak yang berkeinginan melemahkan DPR. Dan, terhadap anggota DPR yang tidak setuju, dia berkata: “Kalau ada anggota DPR yang menolak, itu atas nama siapa? Kalau ada yang mencari panggung untuk bicara, silakan saja!”


Marzuki barangkali lupa bagaimana kalau yang benar-benar marah itu rakyatnya. Setelah diingatkan, dikiritik, dan dikecam terus-menerus, tidak pernah mau mendengar. Malah seolah-olah mau menantang dengan terus memperlihatkan perilaku tak terpuji mereka, puncaknya dengan rencana gila pembangunan gedung baru DPR yang memakan biaya paling sedikit Rp 1,6 triliun itu, barangkali sudah tiba waktunya rakyat menunjukkan kemarahan sesungguhnya langsung kepada DPR. Karena segala cara telah dilakukan, tetapi gagal.
Tidak cukup dengan cara seperti yang pernah dilakukan oleh aktor Pong Harjatmo yang menulisi atap gedung DPR dengan tulisan pada 31 Juli 2010: “Jujur,  Adil, Tegas”,  yang malah ditanggapi salah satu anggota DPR dengan menuding Pong hanya mencari muka saja.


Cara seperti ini bagi anggota-anggota DPR itu hanya seperti melempari kerikil di tengah samudera. Tiada terasa efeknya sama sekali.
Barangkali yang harus dipakai cara yang jauh lebih radikal. Seperti massa rakyat/mahasiswa menyerbu dan menduduki gedung DPR itu, sebagaimana pernah dilakukan oleh massa mahasiswa untuk menurunkan Presiden Soeharto pada Mei 1998. Karena rasanya tabiat segerombolan orang yang katanya terhormat ini semakin mirip dengan rezim Orde Baru saja,yang rakus kekuasaan, rakus harta, dan rakus kemewahan di atas kemiskinan rakyat.

Untuk itu terapinya harus sama dengan terapi ketika berhasil menjatuhkan Orde Baru itu.
Setidaknya, memang diperlukan semacam shock therapy yang keras seperti ini untuk DPR yang tingkat kewarasannya semakin diragukan ini. (artikel dalam Kompasiana selesai)
