---
layout: collection
title: '"Umar Said, cofondateur du restaurant Scop Fraternité Indonesia : un pionnier à part entière" -- un article par François Vescia'
---

**Klik [<u>DISINI</u>]({{ site.baseurl }}/assets/article-françois-vescia.pdf) untuk PDF**

On pourrait dire qu’Umar Said a déjà eu deux vies. Il a dans l’une et l’autre illustré une capacité extraordinaire à inventer l’avenir, mais pas seulement le sien propre. Il l’a fait d’abord en Indonésie, dans le grand mouvement d’émancipation collective de son peuple. Puis dans un second temps lorsqu’il a dû fuir son pays. Devenu citoyen du monde, sa détermination individuelle lui a permis de créer plusieurs collectifs originaux d’information ou de travail en fidélité à son identité d’origine.

En bon innovateur social Umar Said, conjugue son engagement dans des combats collectifs avec une adoption précoce des nouvelles technologies. Elles lui fournissent autonomie et capacité d’intervention,  l’initiative individuelle et le lien au réseau.
De la naissance exaltante de l’Indonésie indépendante, à l’émergence laborieuse du « tiers-monde », de la conférence de Bandung au printemps arabe de 2011, Umar Said est un acteur ou témoin actif de ce monde en évolution.

Aujourd’hui les jeunesses tunisiennes, égyptiennes, libyennes et celles de presque tous les pays arabes rappellent dans leurs combats pour la démocratie et un partage plus équitable des richesses, celle d’une génération qui les a précédées ... Cette génération qui au milieu du vingtième siècle s’était lancée à l’assaut des administrations coloniales avec un courage et une détermination équivalents.  

Le « progrès » technologique et la guerre mondiale avaient alors fonctionné comme un accélérateur de l’Histoire dans toute l’Asie et l’Afrique, démontrant l’immensité des forces de destruction mais aussi la capacité de résistance, le courage de la jeunesse.

Cette accélération prodigieuse avait amené la Chine, l’Inde, le Vietnam, l’Indonésie à s’embraser et à vaincre le colonialisme en quelques années, entre 1945 et 1954.

Il est des périodes qui donnent le vertige, où les engagements des nouvelles générations ouvrent de multiples chemins, permettent les projets les plus ambitieux. Umar Said s’y est engagé passionnément. Il a puisé dans son héritage familial, volonté individuelle et sens de la communauté.

# L’enfance

Umar Said est né le 26 octobre 1928 à Pakis, un village proche de la petite ville de Tumpang à l’Est de Java, à l’époque sous administration coloniale néerlandaise.
Ses parents l’appellent Umar Said car ils admiraient le nationaliste Hadji Oemar Said Tjokroaminoto, personnage important durant la lutte contre le gouvernement colonial. Umar est l’aîné d’une famille de sept enfants. Son père est enseignant.

Pour accéder à une meilleure scolarisation, Umar passe une partie de son enfance auprès de ses grands-parents. C’est une vie simple mais rude. Umar côtoie des adultes exerçant  des métiers variés (le commerce, la peinture, l’éducation). Umar intériorise cet intérêt pour l’éducation avec un grand sens des responsabilités et de bons résultats. Une ou deux fois par mois en train, il retrouve sa famille pour sa plus grande joie.

En 1941 la guerre fait rage entre les Néerlandais affaiblis et les Japonais qui conquièrent l’Indonésie.

Umar adolescent doit revenir auprès de ses parents. Il apprend le japonais en plus du néerlandais qui lui donne accès à des ouvrages de philosophie et d’histoire des religions.

Umar se spécialise et étudie particulièrement les langues, l’art de parler et d’écrire.

Parallèlement, la lutte avec les Japonais contre les Néerlandais s’intensifie. Mais les Japonais luttent avec brutalité pour une grande Asie sous leur autorité… jusqu’au coup d’arrêt fatal d’Hiroshima. Onze jours après le premier bombardement atomique, Soekarno et Hatta, leaders nationalistes indonésiens engagés dans la lutte dès les années vingt, proclament l’indépendance de l’Indonésie le 17 août 1945. Umar n’a que 17 ans.

Au début de la révolution, comme beaucoup de ses camarades d’école, il manque souvent les cours durant sa dernière année au lycée. Son dilemme est de continuer ses études ou de rejoindre la lutte. Umar choisit la deuxième option. La révolution à Java-Est est pour lui une intense période d’apprentissage. C’est la grande « Bataille de Surabaya » en 1945 contre les troupes britanniques et néerlandaises pour l’indépendance.
Umar continue ensuite ses études, se forme politiquement et cherche du travail comme enseignant dans une école primaire. Il participe à la lutte pour l’indépendance qui s’intensifie contre les Néerlandais et ne se termine qu’en 1949.

Comme Umar le dit  « Mon adolescence s’est ainsi passée dans un climat de révolution exaltant ».

# Le journaliste participant de la conférence de Bandung

C’est en tant que correcteur au journal Indonesia Raya, fondé en 1949, qu’Umar débute sa carrière dans le journalisme. Il découvre la fabrication d’un journal et se forme à la rédaction et à la mise en page.

Dans cet immense pays qu’est l’Indonésie encore mal stabilisée, Umar couvre des événements en dehors de l’île de Java, à l’Est de l’Indonésie. Umar accompagne le Président Sukarno avec un groupe de journalistes pour un premier voyage officiel en Indonésie de l’Est. L’accueil de la population est enthousiaste. Ils visitent Macassar, Ambon, Banda, Timor, Florès, Sumba et Bali, ces îles si différentes et si extraordinaires parmi celles qui font l’Indonésie.
Puis Umar commence des voyages à l’étranger, en particulier vers la Chine et l’Europe.

La conférence Afro-asiatique de Bandung (Java-Ouest) se tient  du 18 au 24 avril 1955. Umar est témoin d’un événement historique exceptionnel pour le mouvement d’indépendance de nombreux pays colonisés (notamment en Afrique). Il écrit plusieurs reportages sur cette conférence au retentissement international. Il peut se rappeler encore la conférence de presse tenue par le Premier ministre indien Nehru, de même que le chaleureux accueil réservé au Premier ministre chinois Chou En Lai et au président Nasser.
La conférence de Bandung est pour l’Indonésie un forum important. Dix ans après la fin de la Deuxième Guerre mondiale et après les basculements révolutionnaires qui ont suivi, beaucoup de pays africains et asiatiques qui souhaitent devenir indépendants de l’Occident signent ensemble contre les essais nucléaires, la politique des blocs et le colonialisme.

Pour Umar la conférence de Bandung reste une boussole qui l’aide à agir et à lire les événements ultérieurs, des indépendances africaines aux révolutions de 2011.

Au sein de cette conférence, l’idée germe chez Umar et d’autres journalistes, de créer un réseau de journalistes afro-asiatiques.

En 1956, un journal de Padang (Ouest de Sumatra), cherche un rédacteur en chef. Umar accepte le poste.

En 1960, il devient rédacteur en chef du quotidien Ekonomi Nasional à Jakarta tout en participant à l’association des journalistes indonésiens (il fait partie du comité exécutif du Bureau central). Il y côtoie Joesoef Isak qui jouera un rôle d’éditeur résistant important sous la dictature de Suharto.

Il rejoint également comme trésorier le bureau de l’Association des Journalistes afro-asiatiques PWAA en 1963, créée dans le sillage de Bandung et initialement basée à Jakarta. Ses connaissances de plusieurs langues lui facilitent de très nombreux voyages (l’Egypte, le Soudan, l’Ouganda, la Tanzanie, la Somalie, l’Ethiopie et le Kenya). En 1963, la Conférence internationale de Hanoi lui permet de rencontrer Hô Chi Minh.

En septembre 1965 Umar Said participe à la Conférence de l’Organisation internationale des journalistes à Santiago du Chili.

Cet éloignement de Jakarta le sauve et lui ouvre la voie à une seconde vie.

Le militant indonésien devient citoyen du monde.

# Suharto ou le temps de l’exil forcé

En effet, l’armée intervient à Jakarta après avoir dénoncé un complot communiste des plus obscurs.

“L’Ordre nouveau” du général Suharto est sanglant et impitoyable. Il fait des dizaines de milliers d’emprisonnés sans jugement, d’autres laissés sans vie. 500 000 morts est le chiffre le plus communément avancé, sans doute sous-estimé. Le général Suharto, sa famille, son armée et ses alliés mettent le pays en coupe réglée, construisent des fortunes colossales de 1965 à 1998.

Suharto décime nationalistes, communistes, Indonésiens d’origine chinoise, petits paysans bénéficiaires de la réforme agraire, intellectuels, femmes engagées et syndicalistes. C’est une éradication méthodique appuyée sur des milices paramilitaires, des propriétaires revanchards, et un appareil militaro-policier épuré et soutenu par les Etats-Unis.

Toute une génération des personnes engagées est décapitée ou connaît les prisons de Salemba à Jakarta et de l’île de Buru.

Au plan économique, champ de l’activité d’Umar, l’Indonésie devient l’un des premiers domaines d’application des théories ultralibérales de Milton Friedman et ses Chicago Boys. La dictature pro-occidentale instaure un libéralisme sans entrave. Dérégulation, ouverture incontrôlée aux investissements étrangers, limitation des infrastructures d’état. L’Amérique latine suivra dans les années 70 avant que les mêmes théories économiques ne soient « vendues » aux électeurs américains et britanniques dans les années 80.

Pour Umar retourner à Jakarta et retrouver sa famille – sa femme et ses deux garçons qui ont alors quatre ans et un an- est impossible. De nombreux journaux ont été fermés, y compris celui qu’il dirige Ekonomi Nasional. Umar se fait passer pour mort pour éviter toute difficulté à sa famille.

Il commence un long exil qui va l’amener en Afrique, au Moyen-Orient et surtout en Chine où il va passer sept années dans le cadre de l’Association internationale de journalistes.

Durant cette période Umar Said devient directeur du Bureau du Secrétariat de la PWAA. A ce titre il a l’occasion de se déplacer dans tous les continents. Pour pouvoir circuler, Umar proroge lui-même la validité de son passeport indonésien.

En 1966, il participe à la Conférence Tricontinentale à La Havane (Cuba) et rencontre à cette occasion Fidel Castro.

# L’installation en France

En 1974, Umar choisit de venir en France « connue pour sa générosité » car elle offrait la possibilité d’obtenir l’asile politique. Il est accueilli par Régis Bergeron qui avait participé à Jakarta à la Conférence internationale contre les bases militaires étrangères en 1964. A l’époque, il avait ouvert une librairie, Le Phoenix, spécialisée sur la Chine. Pour ses papiers, Umar est aidé par l’avocat célèbre, Maître Henri Leclerc.

Pour commencer, il rejoint le Comité Vietnam, il aide à repeindre gratuitement les murs de la libraire l’Harmattan qui était sur le point d’ouvrir. Pour démarrer des activités concernant l’Indonésie, il fait connaître la cause des prisonniers politiques (Tahanan Politik : Tapol) et participe aussi au comité d’information sur Timor que les généraux indonésiens veulent annexer par la force.

C’est une période difficile de petits boulots.

Puis, grâce à ses réseaux de solidarité, Umar se voit proposer un poste à la SMAR (Société de Secours Mutuels du personnel du ministère de l’Agriculture et du Ravitaillement).
Dans cette période des rencontres importantes ont lieu pour lui avec la CIMADE (Comité Inter Mouvements Auprès Des Évacués), le CCFD (Comité Catholique Contre la Faim et pour le Développement), Amnesty international, le Parti Socialiste et d’autres organisations politiques ou ONG comme France Terre d’Asile ou, plus tard la Fondation France Libertés de Danielle Mitterrand.

En 1982, après la libération des prisonniers politiques de l’île de Buru et d’autres prisons, le Comité Tapol a moins d’activités et le Conseil d’administration propose alors de le dissoudre. A l’époque, Umar démissionne aussi du Ministère de l’agriculture et commence à préparer la création de la SCOP Fraternité, le collectif du futur Restaurant Indonesia.

Pendant tout ce temps Umar a été séparé de sa famille. Il revoit sa femme en 1978 et ce n’est qu’en 1984 que celle-ci peut le rejoindre à Paris avec leurs deux fils.

# Le restaurant Indonesia, une SCOP pas comme les autres

Umar -journaliste économique, trésorier- fait partie de ces militants pour qui l’économie est un domaine d’intervention et d’engagement. L’idée d'établir un restaurant coopératif fait progressivement son chemin chez lui au sein d’un groupe de réfugiés politiques indonésiens contraints de vivre en France parce qu'ils ne peuvent pas rentrer en Indonésie. En effet, début 1980, une trentaine d’entre eux ont été accueillis et logés par France Terre d'Asile, mais ils veulent être des « hommes debout » grâce à un travail. Ils veulent sortir de l’assistanat, devenir des citoyens productifs et ne pas être une charge pour la société française. Toutefois, par manque de maîtrise de la langue française et de compétences appropriées au marché du travail, il leur est difficile de trouver un emploi.

Est venue l’idée de créer un restaurant servant de la cuisine indonésienne. Cette cuisine était bien connue et appréciée aux Pays-Bas. A cette époque, il n’existait pas de restaurant indonésien en France. Un restaurant permet d’engager beaucoup de personnes. Le groupe a choisi d’ouvrir à Paris un restaurant sous la forme d'une entreprise collective, signifiant que c’était la propriété commune des personnes qui la construisent et qui y travaillent. Il s’agit d’une SCOP, Société Coopérative Ouvrière de Production, un statut bien connu en France dans le domaine de l’économie solidaire et sociale. Ses fondateurs sont quatre personnes représentantes de la communauté des réfugiés politiques indonésiens et quatre français. Son nom est « Scop Fraternité ».

Pour établir la Scop, il a fallu trouver des fonds soit environ 80 000 Euros, somme dont les réfugiés politiques indonésiens ne disposent pas. Les fondateurs de la Scop ont alors déposé des demandes d'assistance auprès d’organisations humanitaires et sollicité des subventions auprès des institutions d’Etat. Le Comité Catholique contre la Faim et pour le Développement (CCFD) a accordé 13.000 Euros de dons, la CIMADE 8.000, le Secours Catholique 1.300. Le gouvernement français a accordé 25.000 Euros sous forme de subvention E.I.L. (Emploi d’Initiative Locale). La mobilisation d’autres amis indonésiens à Paris autour du projet de restaurant permet de compléter sous forme d’aide ou d’emprunt.

L'ouverture du restaurant et la collecte de fonds a mobilisé aussi l'aide de plusieurs amis français. Entre autres, Louis Joinet, conseiller juridique du Premier ministre français de l’époque Pierre Mauroy, Danielle Desguées, fondatrice de la boutique de Gestion de Paris et Paulette Géraud, ont beaucoup aidé à la préparation du projet.

Enfin, après avoir surmonté bien des difficultés, le restaurant coopératif INDONESIA a ouvert le 14 décembre 1982, en présence de nombreux amis, représentants de diverses organisations communautaires à Paris.

Le nouveau restaurant attire les amoureux de l’Indonésie mais aussi des réseaux associatifs et politiques touchés par l’humanité de l’accueil. Pendant plus d’une dizaine d’année, le restaurant est méprisé par l’ambassade indonésienne, représentante du gouvernement de la République d'Indonésie qui le considère comme un foyer de réfugiés politiques. Umar Said en a été l’animateur inlassable mettant au service de ce lieu original ses qualités humaines, son écoute.

L'attitude et les actions d’un des huit fondateurs de la Scop, Pascal Lutz, constituent aussi une expérience intéressante et unique qui traduit concrètement le mot « Fraternité ». Ce Français, a accepté de devenir gérant bénévole de la SCOP pendant 20 ans, sans traitement ni indemnité, accompagnant Umar dans la phase la plus difficile de lancement du restaurant.  

Aujourd’hui, le restaurant coopératif Indonesia, a presque 30 ans. Il représente une initiative unique d‘un collectif d’origine immigrée dans le domaine de l’économie sociale et solidaire. Près de 100  personnes y ont travaillé. Avec le temps, l'esprit coopératif reste toujours bien gardé, défendu et entretenu pour être transmis aux successeurs de la SCOP. Périodiquement, y sont organisées des expositions de photos et des soirées-spectacles avec des danses de Bali, de Java et de Sumatra. D’année en année, les anniversaires du restaurant sont fêtés avec chaleur autour d’Umar, même s’il n’y travaille plus. Les fondateurs et premiers employés sont remplacés peu à peu par de nouveaux employés, plus jeunes. Désormais considéré comme le « fondateur historique», le respecté Pak Umar a quitté la direction du restaurant en 1998 mais il reste membre de la coopérative Fraternité  (restaurant Indonesia) et continue diverses activités sociales et politiques, liées à l’Indonésie. Umar est fier de la poursuite de l’œuvre collective sous la direction de M. Soeyoso puis de Mme Anita Sobron.

# D’autres projets qui mobilisent Pak Umar

Umar devient également français sous le nom de André Aumars.

En 1988, Umar créé une nouvelle entreprise appelée China Documentation & Communication (Centre de documentation et de communication sur la Chine). Il assure le contenu éditorial et la fabrication d’un journal de veille économique - Chine Express - sur la Chine au moment où celle-ci s’ouvre à l’économie occidentale.

Il rencontre alors des personnalités telles que le Premier ministre en 1994, Edouard Balladur, le Président du Sénat, René Monory, le ministre du Commerce Extérieur, l’Ambassadeur de Chine, Cai Fangbo et les responsables de grandes entreprises françaises (comme Aérospatiale, Peugeot, Citroën, Renault, EDF, BNP, Crédit Lyonnais, Lyonnaise des eaux, Gaz de France, etc.).

Durant cette partie de sa vie, jusqu’en 1998, Umar Said peut à nouveau créer son propre travail (même s’il rapporte peu) en restant farouchement indépendant. C’est aussi l’occasion de nouveaux voyages en Chine et en Asie. Surtout Umar retrouve la satisfaction professionnelle d’écrire. Il acquiert également la maîtrise de l’outil de Publication Assistée par Ordinateur.

Nouvelle situation en 1998, la révolte de la jeunesse indonésienne provoque la chute du dictateur Suharto, après 33 ans du régime de l’Ordre Nouveau. Umar peut retourner dans son pays d’origine, retrouver de vieux amis, en particulier l’éditeur courageux Joesoef Isak qui avait fondé à sa sortie de prison la maison d’édition Hasta Mitra  en 1980, qui avait publié les livres du grand écrivain indonésien Pramoedya Ananta Toer.

L’un et l’autre viennent en Europe en 1999 pour Pramoedya, invité par le Ministre de la culture et de la communication pour être nommé Chevalier de l'Ordre des Arts et des Lettres, et à la Journée des dix heures pour la littérature indonésienne en 2004 pour Joesoef, avec le soutien de l’association Pasar Malam dont Pak Umar est membre d’Honneur.

En 2000, il accompagne Danielle Mitterrand et une délégation de France Libertés et d’autres ONG en Indonésie.

Toujours utilisateur avisé des nouvelles technologies, Pak Umar a lancé en 2002 son site internet (http://umarsaid.free.fr), en indonésien pour garder un lien avec son pays d’origine, y défendre les droits de l’homme et la démocratie. En 2011 cette tribune lui permet de suivre l’actualité et de faire connaître son point de vue.

Parallèlement il a entamé la rédaction de ses mémoires « Un chemin de vie », un legs à ses fils, à ses amis d’un parcours au service d’un engagement constant.

# Epilogue : L’hommage de Paris

Le 24 Janvier 2011, dans ce restaurant chaleureux et symbole d’une aventure exceptionnelle, Umar Said a reçu la Médaille de la Ville de Paris pour son parcours d’innovateur social.

Christian Sautter, Maire adjoint au développement économique à la ville de Paris, ancien ministre du budget, a célébré chez Umar la capacité de liberté et d’initiative, la fraternité et enfin, l’esprit d’égalité (la Scop) de cet homme hors du commun.

La remise de cette médaille s’est faite en présence de personnalités et d’un certain nombre d'amis du restaurant d’Indonésie ou d’ailleurs. Venant de France et de la ville de Paris, elle a été ressentie par toute cette communauté et bien au-delà, grâce aux messageries et au réseau Internet, comme une reconnaissance de ses combats collectifs et de sa dignité.

Pour Umar ces témoignages le confirment dans ses choix d‘une humanité debout et fraternelle.

Pour tous, sa personnalité attachante, sa capacité à retourner les obstacles qu’il rencontre, ses liens avec des réseaux très diversifiés, sa modestie, son ouverture, en font un être d’exception, et … un ami précieux.
