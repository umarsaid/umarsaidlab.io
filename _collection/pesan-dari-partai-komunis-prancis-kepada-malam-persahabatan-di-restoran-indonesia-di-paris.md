---
layout: collection
title: Pesan dari Partai Komunis Prancis Kepada malam persahabatan di Restoran INDONESIA di Paris
---

Pada malam persahabatan yang diadakan di Restoran INDONESIA (Paris) tanggal  5 April yang lalu telah hadir dan berbicara banyak sahabat-sahabat lama. Di antara sekitar 30 peserta malam persahabatan itu sebagian terbesar adalah orang-orang Prancis yang menaruh perhatian terhadap peristiwa 65 ketika jutaan orang dibunuh dan dipenjarakan oleh rejim militer Suharto selama puluhan tahun tanpa pengadilan.



Salah satu di antara berbagai pembicara soal kejahatan rejim militer Suharto itu adalah Pierre Marcie, kader urusan internasional Comite Central Partai Komunis Prancis (PCF) yang pernah dikirim partainya untuk membantu YPKP-nya Ibu Sulami dalam tahun 1999. Selain berbicara, Pierre Marcie juga menyampaikan sambutan secara tertulis, yang sebagian isinya disajikan berikut di bawah ini :



« André yang tercinta, saya ditugaskan untuk menyampaikan kepada Anda segala penghormatan dan harapan kepada kesehatan demi  kelanjutan  perjuangan Anda,  dari Pierre LAURENT, sekretaris nasional PCF ( ketr US : sekretaris nasional adalah penyebutan baru sebagai ganti sebutan sekretaris jendral.) dan dari Jacques FATH, penanggungjawab departemen internasional PCF »



(Cher André, je suis chargé de te transmettre toutes leurs salutations et leurs vœux de santé pour la continuation de ton combat, de la part de Pierre Laurent , secrétaire national du PCF, et de Jacques Fath, responsable du département international du PCF)



« Kita berkumpul disini untuk bersama-sama Anda merayakan  penerimaan Medali kota Paris sebagai penghormatan terhadap semua kegiatan Anda untuk hidup dan berjuang, dan selalu terus berjuang, bersama-sama dan untuk kawan-kawan Anda, para korban yang begitu banyak dari bulan Septembernya Suharto.



« Namun, di sini secara simbolik, di restoran Indonesia yang indah ini dan yang telah begitu banyak hubungan terjalin melalui Anda, pertemuan malam ini mempunyai arti yang lebih besar lagi dari pada sebuah pesta untuk kita semua yang banyak sedikitnya telah menemani Anda selama bertahun-tahun sejak kedatangan Anda di Prancis.



« Pertemuan kita ini adalah juga suatu pesan yang mengandung harapan (message d’espoir). Ketika memberi  penghormatan kepada Anda dan di tengah-tengah kehangatan persahabatan kita, maka fikiran kita semua juga tertuju kepada kawan-kawan kita yang hilang di sana,  yang menderita berkepanjangan selama bertahun-tahun di penjara dan pemerasan, kepada mereka yang tidak mau menyerah dan tetap berjuang dengan beragam bentuk dan mempertahankan harapan untuk bisa hidup sederhana.



« Saya menjalin kontak dengan Anda ketika pada akhir 1999 saya telah dipilih oleh Partai Komunis Prancis untuk pergi ke Indonesia menghubungi Ibu Sulami Djojoprawiro. Ia telah datang ke Eropa menemui kalangan diaspora Indonesia dan minta bantuan berbagai organisasi progresif untuk membantu Yayasan yang baru didirikannya untuk mengenang kembali  korban pembunuhan besar-besaran (massacre) tahun 1965/1966 (Yayasan Penelitian Korban Pembunuhan 1965/1966 – YPKP)



« Saya hidup di Tangerang, di daerah besar Jakarta, selama sekitar 3 bulan , dari bulan Februari sampai Mai, di rumah seorang ex-Tapol, teman dan kawan seperjuangan Ibu Sulami. Saya bekerja untuk Yayasan, dengan membuat data base, dengan program File Maker Pro, berdasarkan hasil penelitian yang dilakukan oleh para aktifis.



« Data-data ini mencakup identitas para korban pembunuhan massal, tempat pembunuhan dan cara pembunuhan. Yayasan mengadakan latihan-latihan kepada para sukarelawan untuk itu.



« Walaupun rejim militer mulai runtuh dalam tahun 1998 situasi tetaplah sangat tegang. Saya tidak pernah keluar rumah tanpa ditemani. Sesudah saya kembali (ke Prancis) saya mendengar bahwa fihak intel militer selalu mengawasi kehadiran saya, dan bahwa pada tanggal 14 Mei, beberapa hari sesudah saya kembali pulang (ke Prancis), sekitar 30-an kaum fundamentalis Islam mendatangi rumah yang menjadi kantor Yayasan dan juga tempat tinggal Ibu Sulami dan dimana saya bekerja.



« Mereka mengancam akan membunuhnya beserta lain-lainnya karena dituduh berusaha membatalkan  undang-undang antikomunis. Pada tanggal 31 Agustus satu grombolan golongan Islam fundamentalis datang memaksa Ibu Sulami untuk meninggalkan rumah yang ditinggalinya dengan ancaman mau dibakar.



« Yang terjadi kemudian adalah serangan pada tanggal 14 September tengah malam, ketika tidak ada seorang pun ada dalam rumah itu. Ibu Sulami beserta kawan-kawannya yang lain telah pindah di suatu rumah di Jakarta.



« Suatu hari Ibu Sulami didatangi wartawan Kompas yang mewancarainya tentang soal-soal Yayasannya. Dalam wawancara ini si wartawan mengajukan pertanyaan yang bersifat menjebak «  tetapi ada desas-desus yang beredar bahwa Anda menampung seorang asing ». Kompas kemudian memberitakan bahwa seorang  dari satu organisasi kemanusiaan Eropa memang hadir dalam rumah tempat Yayasan ».



Itulah salah satu dari banyak kenang-kenangan yang diperoleh Pierre Marcie

tentang  cara-cara kerja hati-hati yang dilakukan Ibu Sulami selama memimpîn YPKP.



Tetapi, tekanan selama 32 tahun pemerintahan rejim militer sangatlah  menyeluruh atau dimana-mana (omniprésente), termasuk dalam situasi yang kelihatannya saja tidak penting.



Dalam kalimat-kalimat akhir dari sambutan tertulisnya pada malam persahabatan di restoran koperasi INDONESIA itu Pierre Marcie menyatakan :



« André yang tercinta, kita satu sama lain sudah masuk generasi yang betul-betul tua. Meskipun begitu, dan pastilah itu yang menjadi sumber daya tahan  utama dalam hidup kita bersama, yaitu setiap saat ketika kita  menghadapi kewajiban-kewajiban yang mendesak, maka kepada pembangunan masa depanlah kita memandang, meskipun dalam keadaan yang tidak menentu..



“Saya melihat diri Anda selalu siap untuk bertindak demi kemajuan di negeri Anda. Website yang Anda kelola dengan kegairahan yang terus-menerus tumbuh, untuk membangunkan   semangat di antara kawan-kawan  Anda setanah-air, memberikan contoh yang menarik.



“Ini adalah sumbangan kepada suatu urusan besar (une grande cause), dan saya katakan begitu tanpa perkataan muluk-muluk, karena justru itulah yang kita jalani sebagai tugas yang biasa sehari-hari.



“Dan urusan besar ini,  adalah jiwa solidaritas antara sesama manusia untuk menegakkan suatu masyarakat manusia  (une société humaine), yang menguasai diri-sendiri dan yang membersihkan secara bertahap segala kotoran masa lampau, betapa pun banyaknya dan betapa pun menyakitkannya.



“André yang tercinta, dengan menyampaikan hormat kepada Anda, saya tidak mau mengucapkan kata-kata berikut ini, tetapi seperti dikatakan seorang penyair, Anda adalah “masa muda dunia” (la jeunesse du monde)



Itulah kalimat terakhir dari sambutan tertulis Pierre Marcie, mantan kader urusan internasional Comite Central Partai Komunis Prancis.



= = = =             



PS. Pierre Marcie adalah anggota Partai Komunis Prancis sejak mudanya, dan sudah selama puluhan tahun ia tetap setia kepada partainya.  Untuk jangka cukup lama ia terpaksa dirawat di rumahsakit dan dioperasi berkali-kali



Walaupun belum lama ia meninggalkan rumahsakit ia memerlukan hadir (bersama istrinya yang juga mantan kader dari Comite Central  PCF) dalam malam persahabatan di restoran INDONESIA untuk menyampaikan pesan-pesan partainya.



Pierre Marcie telah bertemu dengan berbagai delegasi gerakan revolusioner Indonesia yang datang ke Paris dan mengunjungi gedung CC PCF
