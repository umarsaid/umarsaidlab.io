---
layout: collection
title: Kepala Ayah Dipenggal di Depan Mataku
---

Kisah yang disajikan oleh Sadewa di bawah ini adalah salah satu saja di antara beberapa ratusan ribu (bahkan, mungkin jutaan !) kisah nyata tentang berbagai kejadian yang menyedihkan atau mengharukan sekitar peristiwa 65. Sebenarnya, kisah yang kurang lebih sejenis atau serupa masih banyak sekali yang bisa ditemukan di seluruh Indonesia. Sejak Suharto turun dari kedudukannya sebagai presiden dan pimpinan tertinggi Orde Baru, sedikit demi sedikit mulai muncul terbuka kisah-kisah tentang kejadian-kejadian di sekitar peristiwa 65 ini.

Tetapi, masih banyak sekali kisah-kisah tentang peristiwa 65 yang terpendam atau tersembunyi sampai sekarang. Padahal, munculnya kisah-kisah nyata itu sehingga diketahui oleh orang banyak adalah mutlak penting sekali, untuk menunjukkan watak sebenarnya atau sifat jahat Suharto beserta para pembangun Orde Baru lainnya. Mengangkat atau menyebarluaskan kisah-kisah nyata tentang kejahatan Orde Baru adalah bagian yang penting dari usaha untuk menjadikannya sebagai pendidikan bangsa.

Peristiwa 65 adalah gudang besar sejarah yang berisi berbagai kisah nyata tentang kebiadaban dan kekejaman yang mengerikan, menyedihkan, menakutkan, (atau juga memuakkan !) yang pernah dilakukan oleh Suharto dkk beserta pendukung-pendukungnya terhadap sejumlah besar sekali golongan kiri dan simpatisan-simpatisan Bung Karno. Sampai sekarang, masih terdapat banyak orang, dimana-mana, yang bisa menceritakan dengan sejelas-jelasnya dan sejujur-jujurnya pengalaman mereka ini.

Sebagai contoh, wawancara singkat Sumarsono (nama samaran) kepada Sadewa seperti yang ditulis di bawah ini merupakan bukti kuat tentang betapa sewenang-wenangnya serta betapa kejamnya aparat (baca : Angkatan Darat) waktu itu terhadap orang-orang yang dianggap kiri atau pengikut PKI. Dan seperti yang diketahui oleh banyak orang, kejadian semacam yang dialami ayah dan ibu Sumarsono di Pekalongan ini juga terjadi di banyak sekali daerah di seluruh Indonesia. Dari Aceh, Sumatera Utara, Riau, Sumatera Barat, Sumatera Selatan, sampai Jawa Barat, Jawa Tengah, Jawa Timur, Bali, Kalimantan, Sulawesi dan tempat-tempat lainnya.

Jadi, sekarang ini masih banyak sekali saksi hidup dari kalangan eks-tapol dan para korban 65 beserta sanak saudaranya yang bisa menceritakan kembali apa yang sudah terjadi dengan sebenarnya sekitar peristiwa 65 itu. Sebagian kecil sekali di antara mereka sudah ada yang mau, dan sudah berani, berbicara tentang perlakuan sewenang-wenang dan tidak bermanusiawi yang mereka alami. Tetapi sebagian terbesar dari mereka masih belum bisa melakukannya, oleh karena berbagai faktor atau sebab.

Padahal, terungkapnya sebanyak-banyaknya berbagai kisah nyata tentang peristiwa 65 itu adalah penting – dan berguna sekali !!! - bagi kehidupan bangsa dan generasi selanjutnya. Perbuatan kejam secara besar-besaran – dan biadab - yang dilakukan terhadap jutaan orang tidak bersalah apa-apa itu adalah aib bangsa kita, dan merupakan pelajaran berharga , yang tidak boleh terulang lagi di kemudian hari.Untuk tidak mengulangi lagi, perlulah kiranya diketahui dengan baik dan jelas apa-apa saja dari kebiadaban yang sudah terjadi.

Dari segi ini kelihatanlah bahwa pengungkapan kisah-kisah mengenai peristiwa 65 adalah tindakan yang mempunyai tujuan luhur bagi kehidupan bangsa, dalam rangka usaha bersama untuk benar-benar menjunjung tinggi-tinggi Pancasila (Pancasila menurut jiwa aslinya, yaitu jiwa Bung Karno, dan bukannya menurut “Pancasila” palsu yang selama lebih dari 32 tahun dikoar-koarkan secara munafik oleh Suharto beserta para pendukungnya).

Menyebarluaskan segala tindakan biadab dan tidak manusiawi oleh Suharto beserta para pendukung setia Orde Baru - dan mengutuknya - adalah perlu sekali dalam usaha untuk membersihkan bangsa kita dari penyakit jiwa yang parah atau iman yang sesat, yang bisa menyebabkan terjadinya kebiadaban-kebiadaban luar biasa di sekitar peristiwa 65 dan sesudahnya. Bangsa kita tidak bisa atau tidak pantas digolongkan sebagai bangsa terhormat selama masih mendiamkan (bahkan menyembunyikan) berbagai kekejaman yang berkaitan dengan peristiwa 65.

A. Umar Said


Berikut adalah wawancara Sadewa (alamat E-mailnya : sadewa48@centrin.net.id) dengan saksi hidup Sumarsono (nama samaran) yang berasal dari daerah Pekalongan

* * *

(Ringkasan transkrip wawancara dengan Sumarsono pada 4 Maret 2001 di Jakarta. Atas permintaan narasumber, semua nama orang diganti tetapi tiga huruf pertama tetap asli. Nama tempat semuanya asli.)

Aku lupa kapan presisnya. Tapi kira-kira dekat dengan akhir tahun 1965. Malam itu sebuah truk warna abu-abu gelap berhenti di rumah orangtuaku, Sukartono, yang tinggal di desa Kesesi tak jauh dari kota Kawedanan Kajen Kabupaten Pekalongan Jawa Tengah. Sebenarnya itu bukan rumah orangtuaku. Ayah, ibu dan aku (anak tunggal) mengungsi di rumah itu dari desa Sebedug. Desa ini terletak dekat jalan pertigaan. Kalau ke Utara sampai Wiradesa, kalau ke Timur sampai Kecamatan Karanganyar dan kalau ke Selatan sampai kota Kawedanan Kajen.

Kami mengungsi ke rumah saudara ayahku di Kesesi karena merasa tidak aman, sebab sejak pecah peristiwa 30 September 1965 itu, banyak anggota BTI (atau dituduh BTI) ditangkapi dan ditahan. Ada juga yang dari desa Sangangjaya, Watubelah, Tambor, Kemploko dan lain-lain.

Malam itu, kami bertiga dinaikkan ke atas truk di mana sudah banyak orang lain di atasnya. Beberapa di antaranya saling kenal dengan ayah ibuku. Truk itu menuju ke kantor Kepolisian Kajen. Beberapa hari kami ditahan di tempat itu yang letaknya tak jauh dari sebuah sungai.

Selanjutnya kami bertiga kembali dinaikkan truk menuju ke Pekalongan. Seperti yang pertama, di atas truk sudah ada beberapa orang dengan tangan terikat. Sebelum masuk kota Pekalongan truk menuju ke arah timur, dan setelah sampai di luar kota membelok ke kiri menuju ke utara, lalu menyusuri jalan kecil ke timur. Semua penumpang diam membisu. Di atas truk ada "aparat" yang tak kuketahui jelas, polisi atau tentara, bercelana loreng tapi bajunya hitam sipil dan pakai ikat kepala merah.

Setelah menempuh perjalanan cukup jauh di tempat sepi itu, si “aparat” berteriak pada sopir sambil memukul-mukul atap truk. "Ini sudah sampai Gamer. Ya, ini Gamer. Coba cari jalan ke utara arah pantai biar urusannya gampang, ha, ha!"

Tentu saja aku tak tahu apa maksud kata-kata itu. Rupanya Gamer nama desa yang terletak antara kota Pekalongan dan Batang, menyusuri daerah pantai. Sepi, gelap, tapi sayup-sayup aku mendengar suara debur ombak. Rupanya truk sudah mendekati pantai.

Truk berhenti, tapi hanya kami bertiga yang diturunkan. Yang lain dibiarkan di atas truk. Pak "aparat" ikut turun. Ada satu "aparat" lagi turun dari samping sopir. Dia membawa benda panjang yang dibungkus kain hitam. Di atas truk masih ada tiga "aparat" lagi yang semua memakai pakaian polisi dan tidak ikut turun. Mereka membawa senjata api. Kami digiring terus ke utara, dan suara debur ombak semakin terdengar nyata.

Kami disuruh berhenti. "Ayo jongkok" perintah Pak "aparat" yang membawa benda panjang dibungkus kain hitam itu. Kami bertiga jongkok. Tapi ayahku diseret untuk jongkok agak terpisah.

"Kamu Sukartono sudah lama dicari-cari. Malam ini kamu harus mati" kata Pak "aparat" dengan kasar. Dia membuka kain yang membungkus benda panjang itu dan aku tahu ternyata benda itu sebuah golok atau pedang. Jantungku berdebar kencang dan ternyata aku terkencing-kencing di celana. Ketika itu usiaku baru 13 tahunan karena baru lulus SD. Aku sadar, ayahku akan dibunuh.

"Hei, lihat sana!" Bentak Pak "aparat" satunya lagi memaksa ibuku dan aku memandangi ayahku yang sedang berjongkok menunggu maut. Ibuku menutupi wajah dengan tangannya, tapi langsung ditendang. Golok itu berkelebat. Dan ibuku jatuh menindih tubuhku yang ikut roboh. Aku masih sempat melihat bagaimana kepala ayahku lepas dari lehernya. Sementara ibuku pingsan.

Peristiwa itu hanya berlangsung beberapa menit dan aku diseret dipaksa berjalan kembali ke atas truk. Tapi aku tak melihat ibuku lagi. Aku ditendang naik ke atas truk. Orang-orang yang tadi di atas truk masih utuh tak ada yang berkurang. Tapi kedua Pak "aparat" tidak ikut naik. Mataku mencari sosok ibuku di kegelapan malam namun tak terlihat. Apakah ikut dipenggal? Lalu di mana kedua mayat orangtuaku? Aku ingat kata-kata Pak "aparat" ketika mengajak si sopir truk mendekati pantai "biar gampang urusannya". Apakah artinya ayahku (dan mungkin juga ibuku) dibuang ke laut sesudah dibunuh agar tanpa repot-repot menguburkan dan meninggalkan bekas?

Aku tak bisa berpikir. Truk berbelok ke arah Barat dan memasuki kota Pekalongan, berhenti di depan sebuah penjara yang terletak berseberangan dengan kali. Tengah malam itu semua penumpang disuruh turun dari truk, langsung digiring masuk ke penjara yang kemudian kuketahui namana Penjara Loji.

* * *

Mungkin ada duabelas tahunan aku menghuni penjara itu dan aku tetap tak mendapat berita di mana gerangan ibuku, yang bernama Sumiarti, kalau masih hidup, sampai saatnya aku dibebaskan. Yang aneh, di penjara itu namaku "disulap" menjadi Sutrisno, anggota Pemuda Rakjat pelarian dari Kediri. Itulah identitas yang harus kuakui setiap kali ada pemeriksaan. Padahal namaku Sumarsono. Aku tahu apa yang disebut Pemuda Rakyat tapi jelas aku tidak pernah menjadi anggotanya karena ketika masuk pejara itu aku baru menginjak usia 13 tahunan. Kediri adalah nama kota di Jawa Timur yang hanya kukenal dalam pelajaran sekolah. Tapi itulah yang harus kuakui sebagai "kota kelahiranku".

Karena "pelarian dari Kediri", maka saat pembebasan aku digabungkan dengan para tahanan yang akan dipulangkan ke Semarang. Mereka kelihatan gembira akan kembali ke kampung halamannya. Kecuali aku yang justru bingung. Dalam perjalanan salah seorang teman berbisik: "Kalau kau memang bukan asal Kediri, nanti ikut turun saja di Semarang, kita coba bertani di kampungku. Aku orang Boja".

Ringkas kata, aku mengikuti ajakannya. Mungkin ada empat tahunan aku di Boja, untuk kemudian kami berdua sepakat untuk sama-sama mengadu nasib ke Jakarta sebagai kuli bangunan. Semula kami tetap berdua tinggal di bedeng, sampai bisa mengontrak kamar dan setelah cukup mapan kami berpisahan untuk membangun rumahtangga sendiri.

Kini kami berkeluarga dengan tiga anak. Usiaku (2001) 49 tahunan, tapi aku tak putus harapan, kiranya pada suatu saat masih bisa bertemu dengan ibuku, Sumiarti yang tentunya sudah berusia lebih dari 75 tahunan dengan ciri-ciri ada bekas luka di kaki kanannya.

Pewawancara: Sadewa48
