---
layout: collection
title: Latar belakang mengapa Umar Said dianugerahi Medali Kota Paris
---

Berhubung dengan banyaknya perhatian terhadap berita tentang ulang tahun ke 28 Restoran koperasi INDONESIA yang sekaligus juga pemberian Medali Kota Paris kepada Umar Said, maka berikut di bawah ini disajikan berita atau tulisan selanjutnya, sebagai pelengkap berita yang sudah disajikan dalam website http://umarsaid.free.fr/ .

Tulisan-tulisan ini akan memungkinkan bagi banyak orang untuk mendapat informasi tambahan guna mempunyai sekadar gambaran tentang latar belakang mengapa Umar Said diberi penghargaan berupa Medali Kota Paris dan menjadikannya wargakota kehormatan kota Paris (citoyen d’honneur)

Karena peristiwa yang agak istimewa ini mungkin menimbulkan keheranan bagi sejumlah orang, dan memancing berbagai pertanyaan, maka adanya informasi tambahan atau berbagai penjelasan akan berguna untuk diketahui.

Umar Said dikenal sejak lama sebagai orang kiri

Sejak Umar Said minta suaka politik di Prancis dalam tahun 1974 tentulah pemerintah yang sudah berganti-ganti itu mengetahui bahwa Umar Said adalah orang yang berhaluan politik kiri, dan pendukung Presiden Sukarno, yang anti-imperialisme dan anti-kolonialisme, dan penentang rejim militer Suharto.

Sebab, dalam tahun 1976 ia menjadi promotor didirikannya Komite Timor Timur di Paris tidak lama sesudah terjadi agresi militer Indonesia untuk mencaplok negara yang diproklamirkan Fretilin ini. Ia juga mendirikan Komite Tapol Prancis yang diketuai Philippe Farine, pimpinan organisasi Katolik yang terkemuka di Prancis. Kedua organisasi ini sering mengadakan pertemuan-pertemuan atau berbagai kegiatan di Paris dan di tempat-tempat lainnya di Prancis.

Ia juga telah berhasil mengumpulkan tanda-tangan dari tokoh-tokoh tinggi Partai Sosialis, yang kemudian menjadi berbagai Perdana Menteri dan Menteri-Menteri  selama kepresidenan François MITTERRAND, untuk petisi mengenai Pramoedya Ananta Toer.

Dalam jangka lama sekali Umar Said menjalin hubungan yang erat dengan organisasi France Libertés, yang dipimpin Madame Danielle MITTERRAND, yang telah mengadakan pertemuan dengan Pramoedya Ananta Toer dan Joesoef Isak, dan ibu Sulami dari YPKP di Paris.

Dalam tahun 2000 Umar Said ikut dalam delegasi Madame Danielle MITTERRAND yang berkunjung ke Indonesia untuk menghadiri pertemuan di Tangerang  dengan banyak eks-tapol yang berdatangan dari Sumatra, Jawa, Sulawesi, dan NTT. Delegasi ini juga menemui para eks-tapol di Jogya.

Selama 37 tahun di Paris Umar Said sering ikut dalam bermacam-macam kegiatan dari berbagai golongan  (Prancis dan bukan Prancis) yang bergerak di bidang HAM, untuk membela demokrasi dan untuk kebebasan. Selama bertahun-tahun ia memelopori ikut sertanya stand Indonesia dalam Fête de l’Humanité, pesta besar tiap tahun yang diselenggarakan oleh Partai Komunis Prancis, untuk menyuarakan perlawanan terhadap rejim militer Suharto.

Tentunya, semuanya ini banyak sedikitnya juga diketahui oleh sejumlah  tokoh di Prancis, termasuk pejabat-pejabat tinggi  yang dari golongan kiri dan sosialis ( yang duduk dalam pemerintahan). Namun, bahwa Umar Said sejak lama berhaluan politik kiri dan penentang rejim militer Suharto tidaklah merupakan halangan baginya (bersama istrinya) untuk diundang untuk malam musik dan jamuan makan  di Istana Elysee yang dihadiri oleh ratusan orang yang dianggap sahabat. Waktu itu hanya Umar Said dan istrinya adalah orang Asia yang kelihatan di antara para undangan.

Bagi Umar Said, sebagai salah satu dari begitu banyak korban rejim militer Suharto,  peristiwa ini merupakan indikasi bahwa,  pada dasarnya, sebenarnya pemerintahan di bawah Presiden MITTERAND bersama tokoh-tokohnya mempunyai simpati terhadap golongan kiri yang menjadi korban dari berbagai politik rejim militer di bawah Suharto. Sikap tokoh-tokoh tinggi golongan sosialis terhadap Umar Said selama puluhan tahun merupakan bukti tentang fenomena ini.

Arti Medali Kota Paris bagi golongan kiri Indonesia

Pemberian Medali Kota Paris kepada Umar Said pada kesempatan ulangtahun Restoran Koperasi INDONESIA memang bisa dipandang sebagai peristiwa luar biasa bagi para korban Orde Baru atau penentang rejim militer Suharto. Sebab, seorang yang lahir di Malang dan besar di Blitar yang di usia lanjutnya dianugerahi Medali Kota Paris dan menjadi wargakota kehormatan ibukota Prancis  memang merupakan hal yang cukup menarik.

Semuanya itu kelihatan lebih jelas lagi dari pidato Christian SAUTTER,  bekas wakil sekjen Istana Kepresidenan Elysée dan Menteri Budget, yang menyerahkan Medali Kota Paris beserta piagamnya kepada Umar Said.

Dalam pidato yang diucapkan tanpa teks itu , tokoh tinggi yang berhaluan sosialis ini menegaskan bahwa Walikota Paris, Bertrand Delanoue beserta kotaprajanya memutuskan memberikan penghargaan kepada Umar Said karena dianggap sebagai militant (pejuang atau aktivis) untuk Fraternité (persaudaraan) dan Liberté (kebebasan).

Sebagai penjelasan ia menceritakan kepada para pengunjung malam itu bahwa Umar Said pada usia mudanya ikut dalam perjuangan untuk kemerdekaan Indonesia, dan ketika kemudian menjadi wartawan ia membikin reportase-reportase tentang konferensi Bandung.  

Christian SAUTTER mengatakan bahwa sebagai amateur sejarah dan peristiwa-peristiwa penting (ia adalah pakar mengenai soal-soal Jepang) bahwa konferensi Bandung (yang Presiden Sukarno adalah tuan rumahnya) merupakan peristiwa yang amat bersejarah bagi perjuangan bangsa-bangsa, yang kemudian menjadi gerakan non-blok.Sayangnya, kata Christian SAUTTER  bahwa perjuangan ini berhenti dalam tahun 1965 dengan terjadinya kudeta (oleh Suharto).

Kepada para peserta pertemuan malam itu, ChristianSAUTTER mengatakan bahwa setelah datang ke Prancis dalam tahun 1974 kemudian Umar Said  telah menolong kawan-kawannya yang lain untuk datang ke Prancis. Waktu itu  ia memiliki gagasan yang ideal dan eksepsionel (istimewa) mendirikan SCOP Fraternité untuk memungkinkan kawan-kawannya itu menjadi otonom dan berdiri di atas kaki sendiri.

Yang dianggap eksepsionel oleh Chrintian SAUTTER, yang menjabat sebagai Wakil Walikota Paris dan bertugas di bidang tenaga kerja (emploi), developement économique (pengembangan ekonomi) dan attractivité internationale (perhatian internasional) adalah bahwa hampir 100 orang pernah bekerja dalam restoran koperasi ini. Ini adalah prestasi yang besar.

Itu semua mendorong Walikota Paris memberikan Medali kepada Umar Said sebagai orang yang dipandang sebagai militant untuk Fraternité dan militant untuk Liberté, kata Christian SAUTTER.  



Pengalaman Louis JOINET soal Timor Timur dan  Indonesia

Dalam pertemuan malam itu Louis JOINET, mantan pejabat tinggi di pemerintahan Prancis di bawah kepresidenan François MITTERRAND (ia adalah penasehat hukum para Perdana Menteri yang berganti-ganti lima kali) sebelum cerita panjang tentang hubungannya dengan Umar Said ia minta kepada Christian SAUTTER untuk menyampaikan terimakasihnya, atas nama sahabat-sahabat lainnya,   kepada Walikota Bertand Delanoue atas keputusannya untuk mengumumkan Umar Said sebagai wargakota kehormatan kota Paris.

Kemudian dengan menelusuri arsip-arsipnya ia menceritakan kisah perkenalannya dengan soal-soal Timor Timur berkat Umar Said, antara lain  dengan pertemuan soal Timor Timur di Lisabon (Portugal) dalam tahun  1976.  Sebelum itu, ia tidak tahu sama sekali masalah-masalah Timor Timur, katanya.

Ia mengatakan bahwa sejak itu ia banyak tahu soal Timor Timur dari Umar Said, dan kemudian dari pekerjaannya sebagai expert yang mewakili Prancis dalam Komisi PBB untuk Orang-orang yang Hilang.

Diceritakannya bahwa ia adalah expert PBB  yang pertama ke Timor Timur untuk mengadakan penyelidikan tentang orang-orang yang dihilangkan. Dalam rangka ini juga ia ke Indonesia untuk soal-soal yang berkaitan dengan pelanggaran HAM, dan bertemu dengan Jenderal Wiranto dan mengunjungi penjara Cipinang untuk bertemu dengan Xanana Gusmao.

Dalam ceritanya mengenai kunjungan ke Indonesia ini ia menguraikan tentang berbagai perlakuan terhadap para korban tindakan penahanan sewenang-wenang  (détention arbitraire) oleh diktaturnya  Suharto.

Tentang Restoran koperasi INDONESIA ia menceritakan beberapa anekdote ketika berkunjung ke Indonesia. Ia pernah ditanya oleh seorang pejabat apakah ia kenal restoran INDONESIA, dan jawabnya adalah « Masakannya enak », dan ketika ia ditanya apa ia juga mengisi buku tamu (livre d’or) restoran semua pendengar pada malam itu ketawa terbahak-bahak.

Ia mengatakan bahwa buku tamu restoran INDONESIA adalah istimewa, karena berbagai pembesar atau tokoh, di antaranya terdapat anggota-anggota parlemen, yang berkunjung ke restoran. Menurutnya, sampai-sampai dinas rahasia Prancis pernah terheran-heran dan menyelidiki mengapa begitu banyak tokoh-tokoh yang mengunjungi restoran ini.ss

Ia mengatakan juga bahwa sebagai pejabat yang bekerja di Matignon (kantor Perdana Menteri) ia mendapat laporan-laporan resmi. Dalam salah satu laporan itu disebutkan bahwa para diplomat Indonesia pernah dianjurkan untuk tidak mendatangi restoran ini. Juga ada laporan bahwa ada usaha KBRI untuk didirikannya restoran Indonesia lainnya, tetapi ternyata tidak jalan, karena orang selalu berdatangan memilih restoran koperasi INDONESIA.

Dari pengalaman yang sudah ditempuh selama ini, Louis JOINET menganggap bahwa projek didirikannya restoran koperasi adalah pengalaman yang hebat (formidable).

Dengan sejumlah kecil cuplikan isi pidato kedua tokoh tingkat tinggi Prancis ini dapat diperoleh sekadar gambaran tentang usaha kolektif yang dijalankan oleh sejumlah korban rejim Orde Baru dengan mendirikan restoran koperasi yang sudah berusia 28 tahun, dan juga sedikit latar belakang pemberian Medali Kota Paris kepada Umar Said.

Dari bahan-bahan yang disajikan di atas ini juga dapat dimengerti bahwa pemberian Medali Kota Paris oleh Walikota dan kotapraja Paris kepada Umar Said adalah didasarkan atas berbagai pertimbangan dan usul banyak orang dan juga pengamatan yang cukup lama tentang apa yang sudah dikerjakannya selama ini.
