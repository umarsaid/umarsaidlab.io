---
layout: collection
title: 'Wasiat Soedjinah : Aku pendukung Bung Karno sampai mati'
---

Menyusul tulisan yang berisi rekaman wawancara HD. Haryo Sasongko dengan Soedjinah (harap baca In memoriam : Soedjinah, pimpinan Gerwani dan pendukung Bung Karno) maka berikut di bawah ini disajikan tambahannya untuk melengkapi tulisan tersebut. Tambahan ini, yang berjudul “Wasiat Soedjinah”, mengandung pesan atau keinginan Soedjinah untuk mengumpulkan dan menerbitkan dokumen-dokumen yang ditulisnya di penjara dengan pensil.

Untuk buku yang berisi kumpulan dokumen-dokumen yang ditulisnya dalam keadaan sulit dan secara sembunyi-sembunyi itu, Soedjinah memilih judul “AKU PENDUKUNG BUNG KARNO SAMPAI MATI”. Dokumen-dokumen yang ditulisnya itu telah diserahkan secara sembunyi-sembunyi kepada seorang wartawan yang berhasil berhubungan dengan Soedjinah di penjara dengan menyamar sebagai kuli bangunan.

Dalam rangka usaha untuk bisa merealisasikan wasiat Soedjinah ini HD Haryo Sasongko menghimbau wartawan yang pernah mengadakan kontak dengan Soedjinah supaya menghubunginya. Isi wasiat Soedjinah dan himbauan kepada wartawan tersebut selengkapnya adalah sebagai berikut :

“Soedjinah, seorang pejuang wanita yang pantang menyerah untuk tetap memberikan dukungannya kepada Bung Karno setelah peristiwa Kudeta berdarah 1965 dan Bung Karno jatuh dari kedudukannya sebagai Presiden RI, telah wafat pada 6 September 2007 di Panti Jompo "Waluyo Sejati", Kramat Jakarta Pusat.

Rekaman suara Soedjinah dalam wawancara dengan saya pada tahun 2000 (baca kembali Soedjinah in Memoriam oleh Umar Said) telah saya serahkan kepada Lontar Foundation. Di luar wawancara yang terekam, ada pembicaraan lisan antara saya dengan Soedjinah. Intinya, dia ingin agar dokumentasi berupa tulisan-tulisan tangan yang dia selundupkan dari kamar tahanan dan disimpan oleh seorang wartawan (tidak dapat saya sebut namanya di sini) dapat dihimpun kembali secara lengkap.

Dia ingin dokumen itu dibukukan dan dia mengusulkan berjudul AKU PENDUKUNG BUNG KARNO SAMPAI MATI. Dokumen itu ditulis dengan pensil di atas kertas kecil sambil bersembunyi di WC kamar tahanannya. Kertas kecil-kecil itu sengaja disobek-sobek, ada yang diremas-remas seperti sampah, agar tidak mencurigakan di mata petugas penjara, dan wartawan tersebut berhasil menemui Soedjinah karena menyamar sebagai kuli bangunan yang harus memperbaiki bangunan di penjara tempat Soedjinah ditahan (ada beberapa naskah yang terpaksa dia kunyah dan ditelan karena takut kepergok petugas penjara yang tiba-tiba lewat).

Dari wawancara saya dengan seorang mantan aktivis Wanita Marhaen, dia menyebutkan kenal baik dengan Soedjinah dan mereka berdua ditambah satu wanita lagi (dari golongan agama) selalu di dekat Bung Karno dan bertugas mencicipi makanan apabila Bung Karno akan mengunjungi suatu daerah.

Sampai kini saya tidak berhasil menghubungi wartawan yang dimaksud Soedjinah. Mohon apabila wartawan yang dimaksud membaca surat ini, kiranya dapat menghubungi saya melalui email. Atas perhatiannya disampaikan terima kasih dan saya bersedia untuk menjaga kerahasiaan saudara. Saya merasa berdosa belum melaksanakan wasiat almarhumah. Selamat jalan Soedjinah, maafkan saya.

HD. Haryo Sasongko

E-mail : sadewa48@centrin.net.id
