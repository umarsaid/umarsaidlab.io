---
layout: collection
title: Gerakan 30 September dan kudeta Suharto
---

Buku penting John Roosa :

Gerakan 30 September dan kudeta Suharto

Diterbitkannya terjemahan dalam bahasa Indonesia buku John Roosa dengan judul “Dalih pembunuhan massal. GERAKAN 30 SEPTEMBER DAN KUDETA SUHARTO”merupakan sumbangan besar sekali bagi semua kalangan dan golongan untuk menambah pengenalan mereka terhadap masalah besar yang sudah membikin sengsaranya puluhan juta warganegara Indonesia oleh Orde Baru selama berpuluh-puluh tahun.

Yang berikut di bawah ini adalah sebagian dari dari kata pendahuluan dari buku tersebut, sedangkan bagian selebihnya dapat dibaca dalam website http://kontak.club.fr/index.htm.

A. Umar Said
=============

Sebagian dari kata pendahuluan tersebut adalah yang berikut :

Bagi sejarawan yang ingin memahami perjalanan sejarah Indonesia modern, hal yang terkadang menimbulkan rasa frustasi ialah justru karena kejadian yang paling misterius ternyata merupakan satu babak kejadian yang terpenting. Pada dinihari 1 Oktober 1965, menteri panglima Angkatan Darat (Menpangad Letnan Jenderal Ahmad Yani dan 5 orang staf umumnya diculik dari rumah-rumah mereka di Jakarta, dan dibawa dengan dengan truk ke sebidang areal perkebunan di selatan kota. Para penculik membunuh Yani dan dua jenderal lainnya pada saat penangkapan berlangsung. Tiba di areal perkebunan beberapa saat kemudian pada pagi hari itu, mereka membunuh tiga jenderal lainnya dan melempar enam jasad mereka ke sebuah sumur mati. Seorang letnan, yang salah tangkap dari rumah jenderal ketujuh yang lolos dari penculikan, menemui nasib dilempar ke dasar sumur yang sama.

Pagi hari itu juga orang-orang di balik peristiwa pembunuhan ini pun menduduki stasiun pusat Radio Repubmik Indonesia (RRI) dan melalui udara menyatakan diri sebagai anggaota pasukan yang setia kepada presiden Sukarno. Adapun tujuan aksi yang mereka umumkan ialah untuk melindungi presiden dari komplotan jenderal kanan yang akan melancarkan kudeta.

Mereka menyebut nama pimpinan mereka, Letnan Kolonel Untung, komandan Batalyon I Kawal Kehormatan Cakrabirawa, yang bertangungjawab mengawal presiden, dan menamai gerakan mereka Gerakan 30 September (selanjutnya disebut sebagai G-30-S).

Dalam sebuah unjuk kekuatan, ratusan prajurit pendukung G-30-S menduduki lapangan Merdeka (sekarang lapangan Monas) di pusat kota. Lalu pada sore dan petang hari 1 Oktober, seperti menanggapi isyarat dari Jakarta, beberapa pasukan di Jawa Tengah menculik 5 perwira pimpinan mereka. Kesuliatn memahami G- 30-S antara lain karena gerakan tersebut sudah kalah sebelum kebanyakan orang Indonesia mengetahui keberadaannya.

Gerakan 30 September tumbang secepat kemunculannya. Dengan tidak adanya Yani, Mayor Jenderal Suharto mengambil alih komando Angkatan Darat pada pagi hari 1 Oktober, dan pada petang hari ia melancarkan serangan balik. Pasukan G-30-S meninggalkan stasion RRI dan lapangan Merdeka yang sempat mereka duduki selama 12 jam. Semua pasukan pembrontak akhirnya ditangkap atau melarikan diri dari Jakarta pada pagi hari 2 Oktober.

Di Jaxwa Tengah, G-30-S hanya bertahan sampai 3 Oktober. Gerakan 30 September lenyap sebelum anggoata-anggotanya sempat menjelaskan tujuan mereka kepada publik. Pimpinan G-30-S bahkan belum sempat mengadakan konferensi pers dan tampil memperlihatkan diri di depan kamera fotografer. Kendati bernafas pendek, G-30-S mempunyai dampak sejarah yang penting. Ia menandai awal berakhirnya masa kepresidenan Sukarno, sekaligus bermulanya kekuasaan Suharto. Sampai saat itu Sukarno merupakan satu-satunya pemimpin nasional yang paling terkemuka selama dua dasa warsa lebih, yaitu dari sejak ia bersma peminpîn nasional lain Mogomad Hatta, pada 1945 mengumumkan kemerdekaan Indonesia.Ia satu-satunya presiden negara-bangsa baru itu.

Dengan karisma, kefasihan lidah, dan patriotismenya yang menggelora, ia tetap sangat populer di tengah-tengah semua kekacauan politik dan salahurus perekonomian pasca-kemerdekaan. Sampai 1965 kedudukannya sebagai presiden tidak tergoyahkan. Sebagai bukti popularitasnya, baik G-30-S maupun Mayor Jenderal Suharto berdalih bahwa segala tindakan yang mereka lakukan merupakan langkah membela Sukarno. Tidak ada fihak manapun yang berani memperlihatkan pembangkannya terhadap Sukarno.

Suharto menggunakan G-30-S sebagai dalih untuk merongrong legitimasi Sukarno, sambil melambungkan dirinya ke kursi kepresidenan. Pengambilalihan kekuasaan negara oleh Suharto secara bertahap, yang dapat disebut sebagai kudeta merangkak, dilakukannya di bawah selubung usaha untuk mencegah kudeta. Kedua belah fihak tidak berani menunjukkan ketidaksetiaaan terhadap presiden. Juga bagi presiden Sukarno aksi G-30-S itu sendiri disebutnya sebagai “riak kecil di tengah samudera besar Revolusi (naional Indonesia), “ sebuah peristiwa kecil yang dapat diselesaikan dengan tenang tanpa menimbulkan guncangan besar terhadap struktur kekuasaan, bagi Suharto peristiwa itu merupakan tsunami pengkhianatan dan kejahatan, yang menyingkapkan adanya kesalahan yang sangat besar pada pemerintahan Sukarno.

Suharto menuduh Partai Komunis Indonesia (PKI) mendalangi G-30-S, dan selanjutnya menyusun rencana pembasmian terhadap orang-orang yang terkait dengan partai itu. Tentara Suharto menangkapi satu setengah juta orang lebih, Semuanya dituduh terlibat dalam G-30-S. Dalam salah satu pertumpahan darah terburuk dalam abad keduapuluh, ratusan ribu orang dibantai Angkatan Darat dan milisi yang berafiliasi dengannya, terutama di Jawa Tengah, Jawa Timur, dan Baki, dari akhir 1965 sampai pertengahan 1966.

Dalam suasana darurat nasional, tahap demi tahap Suharto merebut kekuasaan Sukarno dan menempatkan dirinya sebagai presiden de facto (dengan wewenang memecat dan mengangkat para menteri) sampai Maret 1966.

Geralan 30 September, sebagai titik berangkat kejadian berkait kelindan yang bermuara pembunuhan massal dan 32 tahun kediktatoran, merupakann salah satu di antara kejadian-kekejadian penting dalam sejarah Indonesia, setara dengan pergantian kekuasaan negara yang terjadi sebelum dan sesudahnya : proklamasi kemerdekaan Sukarno-Hatta pada 17 Agustus 1945 dan lengsernya Suharto pada 21 Mei 1998.

Bagi kalangan sejarawan, G-30-S tetap merupakan misrteri. Versi resmi rezim Sujharto – bahwa G-30-S adalah percobann kudeta PKI - tidak cukup meyakinkan. Sukar dipercaya bahwa partai politik yang beranggotakan orang sipil semata-mata dapat memimpin sebuah operasi militer. Bagaimana mungkin orang sipil dapat memerintah personil militer untuk melaksanakan keinginan mereka ? Bagaimana mungkin sebuah partai yang terorganisasi dengan baik, dengan reputasi sebagai partai yang berdisiplin tinggi, merencanakan tindak amatiran semacam itu ? Mengapa partai komunis yang dipimpin prinsip-prinsip revolusi Leninis mau berkomplot dalam pûtsch oleh sepasukan tentara ? Mengapa partai politik yang sedang tumbuh kuat di pentas politik terbuka memilih aksi konspirasi? Agaknya tak ada alasan ke arah sana.

Di lain fihak, sukar dipercaya bahwa G-30-S seperti dinyatakannya dalam siaran radio yang pertama “semata-mata dalam tubuh Angkatan Darat” karena memang ada beberapa tokoh PKI yang jelas ikut memimpin G-30-S bersama beberapa orang perwira militer. Sejak hari-hari Oktober 1965, masalah siapa dalang di belakang peristiwa ini telah menjadi perdebatan yang tak kunjung reda. Apakah perwira militer itu bertindak sendiri sebagaimana mereka nyatakan, dan kemudian mengundang atau bahkan menipu beberapa tokoh PKI agar membantu mereka? Ataukan, justru PKI yang menggunakan sementara perwira militer ini sebagai alat pelakana rencana mereka, sebagaimana yang dikatakan Suharto.? Atau, adakah semacam modus vivendi antara para perwira militer tersebnut dan PKI?

Perdebatan juga timbul sekitar hubungan Suharto dengan G-3O-S. Bukti-bukti tidak langsung memberikan kesan bahwa para perencana G-30-S setidaknya mengharapkan dukungan Suharto; mereka tidak mencantumkan Suharto dalam daftar jenderal yang akan diculik, dan juga tidak menempatkan pasukan di sekeliling markasnya. Dua perwira di antara pimpinan G-30-S adalah sahabat-sahabat pribadi Suharto.. Salah seorang, yaitu Kolonel Abdul Latief, mengaku memberitahu Suharto tentang G-30-S sebelumnya dan mendapat restu darinya secara diam-diam.

Benarkah Suharto sudah diberitahu sebelumnya? Informasi apa yang diberikan G30S kepadanya, apa tanggapan Suharto terhadap informasi itu? Apakah ia menjanjikan dukungan atau melangkah lebih jauh dan membantu merencanakan operasi G30S? Apakah ia dengan licik menelikung G30S agar dapat naik ke tampuk kekuasaan ? Sampai sekarang dokumen utama yang ditinggalkan oleh G30S hanyalah empat pernyataan yang disiarkan RRI Pusat pada pagi dan siang hari 1 Okober 1965. Pernyataan-pernyataan itu menampilkan wajah G30S di depan publik dan tentu saja tidak mengungkap pengorganisasian di balik layar dan tjujuan yang mendasarinya.

Sesudah tertangkap, para pîmpinan kunci G30S tidak mengungkap banyak hal. Kesaksian mereka didepan pengadilan yang dikenal sebagai Mahkamah Militer Luar Biasa (Mahmilub) lebih mencerminkan keterdesakan sangat untuk mernolak segala dakwaan, ketimbang menjelaskan secara rinci tentag bagaimana dan mengapa G30S dilancarkan. Para terdakwa, dapat dimengerti, memilih tutup mulut,berbohong, tidak sepenuhnya berkata benar, dan menghindar demi melindungi diri sendiri dan kawan-kawan mereka, atau melempar kesalahan kepada orang lain.

Baik penuntut umum maupun hakim tidak ambil using untuk mengorek kesaksian-kesaksian merela yang saling bertentang–tentangan ; pengadilan memang tidak dimaksudkan untuk mennyelidiki kebenaran atas peristiwa tersebut. Semua hanyalah pengadilan sandiwara belaka. Tidak satu orang pun yang dibawa ke Mahmilub dibebaskan dari tuntutan. Dari lima orang pimpinan utama G-30-S, kecuali satu orang, semuanya dinyatakan terbukti berkhianat, dijatuhi hukuman mati, dan dieksekusi oleh regu tembak, sehingga dengan demikian menutup setiap kemungkinan mereka muncul kembali dengan keterangan baru yang lebih rinci dan akurat tentang gerakan mereka.

(Keterangan : Tulisan di atas ini disingkat dari kata pendahuluan buku ““Dalih pembunuhan massal. GERAKAN 30 SEPTEMBER DAN KUDETA SUHARTO” Akan diusahakan supaya dalam website http://kontak.club.fr/index.htm bisa disajikan cuplikan dari bagian-bagian lain dari buku yang sangat penting ini).
