---
layout: post
title: Gerakan Konstitusi Pasal 33 untuk Membebaskan Rakyat Indonesia
date: 2011-07-26
---

Berdikari Online, organ PRD (Partai Rakyat Demokratik) tanggal 22 Juli 2011 telah menyajikan sebuah editorial untuk memperingati kelahiran partai tersebut  15 tahun yang lalu. Dalam tulisan tersebut telah diangkat berbagai hal penting tentang perjalanan partai yang terutama terdiri dari kaum muda itu.

Tulisan tersebut selengkapnya adalah sebagai berikut :



15 Tahun Perjalanan PRD

«  Senin sore, 22 Juli 1996, di kantor Yayasan Lembaga Bantuan Hukum
Indonesia (YLBHI). Ratusan orang, sebagian besar berusia 20-an tahun,
menghadiri  pendeklarasian partai baru: Partai Rakyat Demokratik (PRD).

Kelahiran PRD 15 tahun yang lalu adalah sejarah kelahiran kembali
gerakan politik rakyat Indonesia, setelah selama 30-an tahun ditindas
dan diharamkan oleh rejim orde baru. Sejarah perkembangan PRD pun adalah
sejarah perjuangan politik rakyat Indonesia untuk menegakkan
kedaulatannya. Dimana, dalam upaya mencapai tujuan itu, PRD telah
menempatkan kekuatan rakyat sebagai inti gerakan politik. Hal itu sangat
nampak jelas dalam di bagian penutup Manifesto PRD  tahun 1996 yang
berbunyi: "untuk itu Partai Rakyat Demokratik (PRD) percaya dan
yakin bahwa pengorganisiran rakyat adalah satu-satunya cara untuk
menegakkan kedaulatan rakyat."

Dalam perjalanan 15 tahun itu, PRD telah melalui tahap-tahap penting
dalam perjuangan rakyat Indonesia: pada awalnya adalah perjuangan
melawan kediktatoran orde baru, dan sekarang ini adalah perjuangan
melawan imperialisme.

Dalam periode singkat itu, muncul berbagai perdebatan di internal partai
seputar strategi dan taktik menghadapi situasi-situasi baru tersebut,
yakni peralihan dari situasi kediktatoran menjadi situasi liberal, dari
menekankan perjuangan demokrasi menjadi perjuangan pembebasan nasional.

Ada sebagian orang yang mengatakan, tujuan PRD untuk membuka ruang-ruang
demokrasi sudah tercapai dan, karena itu, saatnya untuk memanfaatkan
ruang demokrasi itu sebaik mungkin. Sementara yang lain mengatakan,
perjuangan membuka demokrasi hanyalah salah satu `tahapan'—dari
sekian tahapan—dalam perjuangan PRD untuk membebaskan rakyat dari
penindasan dan penghisapan.

Oleh pendapat pertama, PRD dianjurkan untuk bekerja dalam kerangka
demokrasi dan kerangka politik yang tercipta saat ini. Atau, ringkas
kata, silahkan PRD menikmati ruang demokrasi yang sudah diperjuangkannya
itu.

Sementara pendapat yang kedua mengarah pada begitu banyak sekali tawaran
politik, diantaranya: (1) menolak berpartisipasi dalam ruang politik
yang ada dan tetap konsisten di garis ekstra-parlementer. (2) melihat
ruang politik itu sebagai langkah `taktis' untuk memajukan
gerakan politik rakyat.

Sejak tahun 1999 hingga sekarang, PRD setidaknya tiga kali berusaha
menggunakan momentum pemilu untuk agenda politik memperluas dan
memperbesar daya politik gerakan rakyat: pemilu 1999, pemilu 2004, dan
pemilu 2009. Akan tetapi, dalam proses memanfaatkan ruang politik itu,
dinamika internal partai ditandai dengan munculnya perdebatan-perdebatan
keras dan tidak sedikit yang berakhir dengan perpecahan (split).

Keikutsertaan PRD pada pemilu 2009, sekalipun dengan menggunakan
kendaraan politik partai lain, telah memberikan "pembelajaran"
penting kepada PRD. Bukan saja tentang bagaimana mengenal medan politik
demokrasi liberal, tetapi sekaligus mengerti situasi dan tantangan
perjuangan sekarang ini.

Kita menjadi tahu, bahwa untuk menjadi sebuah kekuatan politik
alternatif, kita tidak bisa sekedar berteriak-teriak bahwa kita adalah
alternatif, tetapi harus ada pengorganisasian rakyat   dari bawah dalam
waktu yang tidak singkat. Kita semakin mengerti bahwa perjuangan
sekarang ini memerlukan "kesabaran revolusioner"—mengutip
istilah Njoto, salah seorang penulis pidato Bung Karno.

Untuk itu, dalam kongres VII PRD, 1-3 Maret 2010 lalu, telah ditegaskan
tugas mendesak partai: pembangunan partai (party building). Kongres itu
juga telah mengubah PRD dari partai kader menjadi partai terbuka atau
partai massa.

Sekarang ini kita sedang berhadapan dengan situasi yang semakin jelas:
penjajahan asing (imperialisme) yang semakin agressif. PRD pun sangat
menyadari situasi baru ini dan memahami tantangan-tantangannya. Karena
itu, PRD telah menyusun sebuah Manifesto Politik yang baru, sebagai
jawaban terhadap problem pokok rakyat Indonesia saat ini: Imperialisme.

Oleh karena itu, jika deklarasi PRD pada 22 Juli 1996 lalu telah
mengumandangkan perjuangan melawan orde baru—dengan tiga tuntutan
pokok: pencabutan 5 paket UU Politik dan Dwi Fungsi ABRI, maka pada
peringatan 15 tahun PRD hari ini telah berkumandang "gerakan
nasional pasal 33" sebagai bentuk perlawanan langsung terhadap
imperialisme.

Akhirnya, PRD—sebagaimana partai-partai revolusioner di masa
perjuangan anti-kolonial—telah berjuang bersama rakyat Indonesia.
Sampai sekarang, banyak kader PRD masih hilang karena diculik rejim orde
baru dan sampai sekarang belum diketahui keberadaannya, diantaranya:
Wiji Thukul, Herman Hendrawan, Suyat, Bimo Petrus, dan lain-lain. Ada
banyak pula kader-kader PRD yang gugur dalam saat-saat perjuangan:
Gilang (Diculik oleh aparat rejim orba dan ditemukan mayatnya di Madiun,
Jawa Timur) Yusuf Rizal, Saddam Husein, Andi Munajat, dan Taufik
(meninggal karena ditabrak lari saat mengadvokasi rakyat di NTB).

Kita berharap, PRD bisa berkembang terus menjadi partai yang kuat dan
kokoh, tahan terhadap panas dan hujan, yang menjadi tempat bernaungnya
kaum revolusioner dan menghimpun sebanyak-banyaknya rakyat Indonesia.

Dirgahayu PRD! Berjuang terus untuk mewujudkan Sosialisme Indonesia! »

 (kutipan editorial selesai).

Komentar :

Editorial « Berdikari Online » di atas patut mendapat perhatian dari banyak kalangan masyarakat, mengingat arti pentingnya kalau dihubungkan dengan situasi negara dan bangsa, baik di masa yang lalu, dewasa ini maupun di kemudian hari.

Apalagi, ketika negara dan bangsa dewasa ini sedang diporak-porandakan oleh kebejatan akhlak atau kerusakan moral para « tokoh-tokohnya » di bidang eksekutif, legislatif, dan judikatif, yang membikin Indonesia sebagai negara di mana korupsi dan pelecehan hukum merajalela dengan hebatnya, dan juga sudah menjadi jajahan kekuatan asing.

Editorial mengingatkan kita semua bahwa PRD, yang didirikan oleh kaum muda dalam masa rejim militer Orde Baru telah menjadi obor dalam perlawanan yang berani – dan penuh pengorbanan – melawan Dwibungsi ABRI dan berbagai politik diktatorial lainnya (antara lain 5 paket UU Politik).

Kiranya, dalam sejarah berbagai perlawanan rakyat terhadap rejim militer Suharto, perjuangan PRD adalah salah satu di antaranya yang termasuk paling tegas, paling menonjol, dan paling terkenal, baik di kalangan para penentang Orde Baru maupun di kalangan « musuhnya », yaitu para pendukung Suharto.

Seperti yang ditulis dalam editorial tersebut, PRD telah mengalami berbagai pengalaman dalam perjalanannya selama 15 tahun ini, dalam usaha untuk membangun partai yang berjuang untuk membebaskan rakyat dari penindasan dan penghisapan.

PRD yang tadinya adalah  partai tertutup dan merupakan partai kader dalam tahun 2010 telah dirobah menjadi partai massa dan partai terbuka. Sejak itu,  partai yang jelas-jelas menganut politik kiri ini dengan terang-terangan dan tegas mengumandangkan ajaran-ajaran revolusioner Bung Karno, yang pro-rakyat kecil (wong cilik), anti-imperialisme dan pro sosialisme à la Indonesia.

Dewasa ini PRD sedang mengobarkan « Gerakan Nasional Pasal 33 », yang merupakan gerakan yang tepat untuk menghadapi situasi negara dan bangsa yang sedang digerogoti oleh bahaya neo-liberalisme dan kolonialisme dalam bentuk baru.

Sebab, banyak sekali bukti bahwa pengkhianatan berat telah dan sedang terjadi terhadap Konstitusi atau UUD 45 pasal 33, yang berbunyi :

(1) Perekonomian disusun sebagai usaha bersama berdasar atas asas kekeluargaan.
(2) Cabang-cabang produksi yang penting bagi negara dan yang menguasai hajat hidup orang banyak dikuasai oleh negara.
(3) Bumi dan air dan kekayaan alam yang terkandung didalamnya dikuasai oleh negara dan dipergunakan untuk sebesar-besar kemakmuran rakyat. »

« Gerakan Nasional Pasal 33 » tersebut dewasa ini sudah dan sedang mendapat dukungan dari banyak kalangan masyarakat : pemuda, mahasiswa, kaum buruh, dan kaum  tani, yang mengadakan macam-macam aksi di banyak kota di daerah-daerah. Dukungan yang begitu antusias dari mana-mana menunjukkan bahwa gerakan ini mencerminkan aspirasi banyak kalangan. (Mengingat pentingnya dukungan terhadap gerakan ini yang mengindikasikan berkembangnya kesedaran politik dan patriotisme, akan diusahakan adanya tulisan khusus tersendiri)

Dalam situasi ketika sikap kalangan atasan atau elite bangsa makin kelihatan membusuk dan bobrok sekali akibat  korupsi dan  berbagai macam kongkalikong dengan kekuatan ekonomi asing, maka perjuangan  PRD serta berbagai macam organisasi kiri lainnya (yang cukup banyak)  merupakan harapan bahwa negara dan bangsa kita masih bisa diselamatkan, demi anak cucu kita di kemudian hari.

Kiranya, selama ini kita  semua sudah menyaksikan sendiri  bahwa kebanyakan partai politik yang tergabung dalam koalisi SBY sudah tidak bisa  diharapkan berbuat banyak bagi rakyat untuk mencapai tujuan-tujuan revolusi 17 Agustus 45 dan Konstitusi 45. Bahkan sebaliknya, partai-partai politik pendukung SBY sudah menghancurkan, atau menjauhkan cita-cita masyarakat adil dan makmur, masyarakat sosialisme à la Indonesia, yang dengan gigih sudah diperjuangkan Bung Karno sepanjang hidupnya, sampai akhir hayatnya.

Situasi negara dan bangsa dewasa ini menunjukkan dengan jelas bahwa di bawah pimpinan SBY pemerintahan  di Indonesia menjadi tambah morat-marit, dan berbagai bidang kehidupan tambah kacau-balau.  Negara kita membutuhkan pimpinan yang baru, namun bukanlah yang sejenis  atau segolongan SBY !!!
