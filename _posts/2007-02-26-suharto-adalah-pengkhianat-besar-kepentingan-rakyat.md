---
layout: post
title: Suharto adalah pengkhianat besar kepentingan rakyat
date: 2007-02-26
---

(Pengantar: Para pembaca dimohon dengan sangat supaya jangan terkejut atau buru-buru marah atau jengkel ketika membaca judul tulisan ini. Marilah, dengan fikiran jernih dan hati bersih, membaca dan merenungkan - secara dalam-dalam - berbagai hal yang diutarakan dalam tulisan berikut. Setelah merenungkan hal-hal yang dipaparkan dalam tulisan ini, maka mungkin sekali banyak orang akan beranggapan bahwa judul “pengkhianat besar kepentingan rakyat” itu memang sudah pada tempatnya atau sudah seharusnya)


Harian Jawapos, Koran Tempo, harian Suara Merdeka (dan banyak suratkabar Indonesia lainnya) pada tanggal 22 Februari 2007 telah menyiarkan satu berita penting yang patut mendapat perhatian dari kita semua. Sebab, dalam berita tersebut terdapat hal-hal yang berkaitan dengan persoalan harta haram Suharto. Dan persoalan harta haram Suharto (beserta keluarganya) itu patut kita jadikan bahan pemikiran atau pembicaraan kita bersama Mengingat pentingnya berita tersebut untuk kita telaah bersama atau kita jadikan bahan renungan, maka berikut di bawah ini disajikan isi berita Jawapos :


“Gagal menjerat pidana, Kejaksaan Agung (Kejagung) mencari celah untuk menyeret mantan Presiden Soeharto ke pengadilan melalui gugatan perdata. Sebelum gugatan diajukan, Kejagung melayangkan somasi. Isinya, meminta agar Soeharto menyerahkan seluruh uang hasil korupsi sekitar Rp 1,7 triliun.
Jaksa Agung Abdul Rahman Saleh menyatakan, somasi tersebut akan dikirimkan pekan ini. Kejagung juga memberikan tenggat kepada Soeharto untuk menjawab somasi tersebut selambatnya sepekan sejak surat somasi diterima. "Yang kami minta adalah penyerahan hasil korupsi tujuh yayasan," kata Arman -panggilan Abdul Rahman Saleh- di Gedung Kejagung, Jakarta, kemarin.


Sesuai surat dakwaan, Soeharto didakwa kasus korupsi tujuh yayasan dengan total kerugian negara Rp 1,7 triliun dan USD 419 juta. Tujuh yayasan yang pernah diketuai Soeharto tersebut adalah Supersemar, Dana Sejahtera Mandiri, Trikora, Dharmais, Dana Abadi Karya Bakti (Dakab), Amal Bhakti Muslim Pancasila, dan Gotong Royong Kemanusiaan.


Menurut Arman, kejaksaan tidak memaksa Soeharto menjawab somasi tersebut. Namun, jika dia mengabaikan somasi, kejaksaan akan memasukkan gugatan ke pengadilan. "Itu sesuai hukum acara (perdata)," tegasnya. Sebaliknya, kejaksaan menjajaki membatalkan gugatan jika Soeharto ternyata mengindahkan isi somasi tersebut.


Dia menambahkan, soal pendaftaran gugatan, kejaksaan tinggal melangkah. Draf gugatan telah selesai dan siap didaftarkan. "Gugatannya sudah selesai kok," ujar mantan aktivis YLBHI tersebut. Pendaftaran gugatan dijadwalkan akhir Februari 2007. (kutipan berita Jawapos selesai).


7 yayasan : baru sebagian kecil harta haram Suharto


Jumlah Rp 1,7 triliun itu banyak sekali, dan karena banyaknya maka sukar untuk bisa dibayangkan. Sebab, kalau ditulis dengan cara lain, maka Rp 1,7 triliun berarti Rp 1,7 dikalikan 1000 000 juta, atau Rp 1.700 000 000 000 (angka nolnya ada sebelas). Ini jumlah uang yang besar sekali !!! (Ingat: uang Rp 1 juta saja sudah banyak buat sebagian terbesar rakyat kita, dan jumlah uang yang akan digugat Kejaksaan Agung adalah Rp 1 juta dikalikan 1,7 000 000, atau sekitar USD 419 juta).


Jumlah uang yang sebesar itu barulah yang berkaitan dengan 7 yayasan yang pernah didirikan oleh Suharto, melalui cara-cara halus dan kasar, dan dengan menyalahgunakan kekuasaannya sebagai pemimpin tertinggi rejim militer Orde Baru. Padahal, yayasan-yayasan Suharto (beserta keluarganya) tidak hanya yang tujuh itu saja. Menurut penelitian George Aditjondro (lihat: Yayasan-yayasan Suharto dalam website http://perso.club-internet.fr/kontak.) ada sekitar 40 buah. Selama ini tidak diketahui dengan pasti berapa besarnya asset-asset yang dimiliki yayasan-yayasan Suharto itu.


Kalau Kejaksaan Agung berusaha supaya Suharto mengembalikan uang hasil korupsi yang sebesar Rp 1,7 triliun, maka jelaslah bahwa jumlah itu hanya merupakan sebagian kecil sekali saja (!!!) dari harta haram yang telah dikumpulkan dengan cara-cara yang bathil pula selama 32 tahun kekuasaan tangan-besinya. Sebab, menurut laporan majalah TIME 24 Mei 1999 kekayaan Suharto (beserta keluarganya) ditaksir USD 15 miliar (harap baca laporan majalah TIME selengkapnya). Jadi, jumlah uang yang akan diusahakan “ditarik kembali” oleh Kejaksaan Agung dari Suharto (yang Rp 1,7 triliun atau USD 419 juta itu) hanyalah sebagian kecil sekali dari keseluruhan harta Suharto beserta keluarganya, yang jumlahnya bermiliar-miliar dollar (artinya : ribuan juta dollar atau ratusan triliun rupiah !!!).


Suharto pengkhianat terbesar kepentingan rakyat


Berita tentang akan digugatnya harta haram yang ditanamkan dalam 7 yayasan oleh Suharto ini merupakan langkah permulaan penting untuk – Alhamdulilah, akhirnya – membongkar atau memblejeti kebusukan politik dan kebobrokan moral Suharto (beserta keluarganya), yang selama puluhan tahun dapat dibungkus atau ditutup-tutupi dengan segala kemegahan dan “keharuman”. Penelanjangan kebusukan politik atau pemblejetan kebobrokan moral Suharto melalui pembongkaran korupsi besar-besaran olehnya adalah masalah yang maha-penting bagi sejarah bangsa Indonesia.


Sebab, korupsi besar-besaran oleh Suharto (dan keluarganya) adalah perwujudan yang gamblang dari kebusukan politik dan kebobrokan moral, yang berdominasi di masa rejim militer Orde Baru. Melihat besarnya harta haram yang telah dikumpulkan oleh Suharto (sekali lagi, beserta keluarganya), maka jelaslah bagi banyak orang bahwa Suharto adalah pengkhianat besar kepentingan rakyat Indonesia. Bahkan, pengkhianat terbesar!


Korupsi besar-besaran oleh Suharto berkaitan erat sekali dengan penyalahgunaan kekuasaan dan pengaruhnya sebagai presiden, sebagai panglima tertinggi ABRI, sebagai pimpinan utama GOLKAR, singkatnya sebagai dedengkot yang paling berkuasa dari rejim militer Orde Baru. Korupsi besar-besaran Suharto dimungkinkan juga karena adanya dukungan dari pembesar-pembesar militer dan Golkar (dan golongan lainnya) yang merupakan tulang-punggung Orde Baru selama puluhan tahun.


Oleh karena itu, pembongkaraan korupsi besar-besaran Suharto (dan keluarganya) bukan hanya merupakan pukulan berat bagi Suharto saja, melainkan juga pukulan berat bagi para-pembantu atau pendukung utamanya. Bahkan lebih jauh lagi! Pembongkaran korupsi besar-besaran yang dilakukan Suharto berarti juga pembongkaran kejahatan, keburukan, atau kebusukan rejim militer Orde Baru. Sebab, Suharto adalah pengejawantahan rejim militer Orde Baru, yang selama 32 tahun mengangkangi bangsa dan negara Indonesia secara paksa dan tangan besi. Dilihat dari berbagai dan banyak segi, bolehlah kiranya dikatakan bahwa Suharto adalah Orde Baru, dan bahwa Orde Baru adalah Suharto.


Suharto adalah presiden terkorup di dunia


Korupsi besar-besaran yang dilakukan Suharto (dan keluarganya) sebagai presiden dan pimpinan tertinggi ABRI selama 32 tahun adalah salah satu dari berbagai pengkhianatan besar-besaran terhadap kepentingan rakyat banyak di bidang politik, sosial, ekonomi, dan HAM. Harta haram yang begitu besar itu telah ditumpuknya di atas bangkai jutaan manusia yang dibunuh tahun 65-66, di atas lautan air mata dan darah puluhan juta keluarga korban peristiwa 65, dan di atas jenazah Bung Karno beserta para pendukungnya


Di antara kita semua banyak yang sudah tahu bahwa sejak lama Suharto sudah dikenal di dunia sebagai presiden yang terkorup di dunia, sebagai diktator yang melakukan berbagai kekejaman serupa fasis Hitler, dan sebagai kaki-tangan imperialisme AS. Buktinya, kalau kita buka Google (bahasa Inggris) di Internet dan kita ketik kata kunci “Suharto corruption” maka akan tersedia berbagai bahan yang berkaitan dengan korupsi Suharto sebanyak 317 000 halaman. Luar biasa “tersohornya” di dunia kebusukan atau kebobrokan “pemimpin” yang satu ini.


Menurut laporan organisasi internasional yang independent dan mengadakan penelitian serius terhadap masalah korupsi berbagai tokoh di dunia, yaitu Transparency International ( 24 Maret 2004), Suharto menduduki tempat paling atas di antara 10 kepala negara yang terkorup di dunia. Menurut laporan itu, korupsi Suharto meliputi jumlah antara US$ 15 sampai US$ 35 miliar (atau, antara US$ 15 000 000 000 dan US$ 35 000 000 000). Kalau dihitung dalam Rupiah maka US$ 15 miliar itu adalah 15 000 000 000 dikalikan Rp 10 000 (dibulatkan, sekitar kurs dewasa ini), menjadi Rp 150 000 000 000 000 , atau Rp 150 triliun.

Itu paling sedikitnya. Sebab, ditaksir harta haram hasil korupsi Suharto (dan keluarganya) adalah antara US$ 15 miliar dan US$ 35 miliar Jumlah uang haram hasil korupsi Suharto (beserta keluarganya) yang diumumkan oleh Transparency International, dan yang hampir sama dengan dengan laporan majalah TIME itu, adalah sangat, sangat, dan sangat besar sekali (!!!).


Nah, sekarang marilah sama-sama kita renungkan dalam-dalam. Apakah harta yang jumlahnya sebesar itu betul-betul hasil jerih payah atau usaha yang “bersih” dari Suharto beserta anak-anaknya, dan bukannya hasil dari berbagai praktek-praktek yang tercela dan hina? Dengan kalimat lain, apakah harta yang sebanyak itu betul-betul merupakan hak-hak yang sah, suci dan adil dari Suharto, Tutut, Sigit, Bambang, Tommy, Mamiek dan Titiek ? Atau, dengan kalimat lainnya lagi, apakah harta sebanyak itu telah bisa mereka peroleh dengan cara-cara yang biasa dan jujur atau fair? Tidak mungkin, alias mustahil !!!


Menumpuk harta haram dengan kebathilan


Sekarang, adalah jelas bagi banyak orang bahwa Suharto bukan saja pengkhianat Bung Karno dan kakitangan imperialisme AS, dan algojo pembantaian jutaan orang-orang tidak bersalah, dan pembungkam demokrasi selama puluhan tahun, melainkan juga bahwa ia adalah maling yang terbesar dalam sejarah bangsa Indonesia sampai sekarang. Harta haram Suharto (dan keluarganya) sebesar antara US$ 15 miliar sampai US$35 miliar itu merupakan bukti yang gamblang sekali bahwa Suharto adalah pengkhianat terbesar kepentingan rakyat Indonesia. Dengan dalih menyelamatkan bangsa dari bahaya Sukarnoisme dan komunisme, Suharto telah menyalahgunakan berbagai kesempatan dan kekuasaan untuk menumpuk harta melalui jalan haram dan cara-cara yang bathil.


Oleh karena itu, adalah sesuatu yang benar, adil, sah, dan mulia, kalau mulai sekarang banyak fihak dalam masyarakat menuntut dikembalikannya dana atau harta yang telah dicuri Suharto (dan keluarganya) dari negara dan rakyat. Adalah sesuatu yang aneh (dan juga memalukan !), atau adalah sesuatu yang tidak beres di Republik kita, kalau kejahatan monumental yang begitu besar dibiarkan saja dan tidak ada tindakan apapun untuk memeriksanya dan mengadilinya. Kiranya bisalah dikatakan bahwa kejahatan Suharto terhadap negara dan rakyat adalah lebih besar dari seluruh kejahatan yang dilakukan oleh penjahat-penjahat kriminal Indonesia dijadikan satu.


Sejarah bangsa Indonesia akan membuktikan selanjutnya bahwa mengadili berbagai kejahatan, dan kesalahan (termasuk KKN) Suharto adalah sesuatu yang benar dan perlu dilakukan, demi kebaikan kehidupan bangsa kita selanjutnya di kemudian hari. Artinya, membiarkan saja, atau mendiamkan saja, atau, bahkan menutup-nutupi saja kejahatan dan kesalahannya adalah suatu sikap yang salah,.adalah pengkhianatan terhadap kepentingan bangsa dan negara.


Membela kejahatan Suharto adalah "bunuh diri"


Adalah jelas sekali bahwa memblejeti kejahatan Suharto dan menuntut diadilinya kesalahan-kesalahannya (yang amat besar dan bertumpuk-tumpuk) merupakan langkah teramat penting sekali untuk bisa mengadakan perubahan dan perbaikan atas berbagai kerusakan dan pembusukan yang sudah ditimbulkan rejim militer Orde Baru. Sebab, mengadili segala kejahatan dan kesalahan Suharto berarti juga mengadili (secara tidak langsung) kejahatan dan kesalahan para pembantunya dan para pendukung-pendukung setianya.


Makin terbongkarnya berbagai borok-borok Suharto beserta keluarganya (antara lain : kasus 7 yayasan, kasus uang simpanan Tommy sebesar 36 juta Euro (atau sekitar Rp 421 miliar) di bank BNP, kasus Sigit dan Bambang) membikin sulitnya para mantan tokoh-tokoh Orde Baru (artinya, sebagian pimpinan militer dan GOLKAR serta sebagian tokoh-tokoh Islam) untuk terang-terangan tetap mendukung dan memuja-muja Suharto. Sekarang, membela kesalahan dan kejahatan Suharto, adalah sama saja dengan "bunuh diri" atau mencoreng muka sendiri.


Sebab, sudah jelas bagi banyak orang, baik di Indonesia maupun di dunia, bahwa Suharto adalah maling yang maha-besar dan, karenanya, adalah juga pengkhianat besar kepentingan negara dan rakyat Indonesia.

---

Jawapos, 19 Juni 2007,

Gugatan Kasus Soeharto

Prajogo Pangestu Juga Dititipi Dokumen Asli



JAKARTA - Kejaksaan Agung (Kejagung) memastikan dokumen-dokumen asli yang akan dijadikan alat bukti kasus korupsi mantan Presiden Soeharto masih tersimpan baik. Saat ini, sebagian dokumen itu disimpan pemilik perusahaan peminjam uang dari yayasan-yayasan yang diketuai Soeharto.

"Semuanya (dokumen) masih ada dan dititipkan agar tidak hilang," kata Sekretaris Jaksa Agung Muda Pidana Khusus (JAM Pidsus) Kemas Yahya Rahman di gedung Kejagung kemarin. Kemas didampingi Kapuspenkum Kejagung Salman Maryadi.

Menurut Kemas, dokumen-dokumen asli dititipkan kepada masing-masing pengelola yayasan milik Soeharto. Sebagian lagi diserahkan kepada pihak-pihak yang diduga terkait kasus Soeharto. "Kami memfotokopi dan melegalisasinya," kata Kemas. Proses penitipan dokumen-dokumen asli itu disertai pembuatan berita acara penitipan yang ditandatangani pihak-pihak yang bertanggung jawab.

Kemas lantas mencontohkan bentuk surat berita acara penitipan ke Yayasan Supersemar. Dalam berita acara tersebut, surat bukti asli dititipkan atas nama Saborono Slamet.

Sedangkan Salman mencontohkan berita acara penitipan dokumen asli bukti pinjaman bos Grup Barito Prajogo Pangestu ke Yayasan Dakab Rp 40 miliar. Dokumen tersebut dititipkan kepada Prajogo di kantornya, Wisma Barito Pacific, Slipi, Jakarta Barat. "Ini salah satu contoh," kata Salman sambil memperlihatkan berita acara penitipan itu kepada wartawan.

Baik Salman maupun Kemas menjamin, dokumen asli tersebut masih tersimpan dengan baik. Sebab, jika ada indikasi kesengajaan menghilangkan, yang dititipi dapat dimintai pertanggungjawaban.

"Secara hukum, itu dapat dijamin. Dan, misalnya, (dokumen asli) itu hilang, maka kami dapat menuntut pihak yang kita titipi dengan tuduhan menghilangkan alat bukti," beber Kemas.

Menurut dia, kejaksaan sengaja menitipkan dokumen-dokumen asli karena dikhawatirkan hilang. "Kami tak mau mengambil risiko sehingga dititipkan ke pihak-pihak tertentu. Apalagi, dokumen-dokumen tersebut jumlahnya sangat banyak," kata mantan kepala Kejati Jambi itu. Langkah kejaksaan menitipkan tersebut sesuai dengan prosedur penitipan dokumen, mengingat hal itu diatur dalam perundang-undangan.

Kemas menegaskan, kejaksaan masih menyimpan dokumen-dokumen fotokopian hasil legalisasi terkait kasus Soeharto. "Kapan pun siap diajukan ke persidangan," jelas Kemas. Jumlah dokumen fotokopian cukup banyak. Dia mengilustrasikan, jika disimpan di sebuah ruangan, dokumen tersebut akan membutuhkan ruangan seluas sekitar 5 x10 meter persegi.

Ditanya kapan berkas didaftarkan ke PN Jakarta Selatan, Kemas menjawab, akan dilaksanakan bulan depan. "Mudah-mudahan segera didaftarkan," ujar jaksa senior yang pernah menjabat Kapuspenkum itu.

Sebelumnya, JAM Perdata dan Tata Usaha Negara (Datun) Alex Sato Bya mengaku terkejut begitu mendapati alat bukti kasus Soeharto yang tersimpan pada sembilan filling cabinet merupakan dokumen fotokopian. Dia tidak tahu apakah dokumen aslinya hilang atau sengaja dihilangkan. Nah, kejaksaan kini berupaya mendapatkan dokumen-dokumen asli sebagai materi gugatan kasus Soeharto. (agm)

* * *


Majalah TEMPO: Edisi 17 Juni 2007

Merayap dengan Mercy Tua

Dokumen berkas perkara Soeharto banyak yang cuma fotokopi. Keterangan
para jaksa simpang-siur. Pada hari ulang tahunnya yang ke-86,
terbukti Soeharto tak mudah dipatahkan.

PADA hari ulang tahunnya yang ke-86, betapa bahagianya Soeharto.
Jumat pekan lalu, sekitar 200 tamu hadir di rumah bekas presiden itu di
Jalan Cendana, Jakarta. Berebutan mereka mengucapkan selamat—bahkan
sampai antre dan saling dorong. Tutut, putri sulung Soeharto,
memberikan sambutan. Anak-cucu hadir, tak terkecuali Bambang
Trihatmodjo dan istrinya, Halimah—meski keduanya tengah bersiap untuk
bercerai. Karangan bunga berdatangan. Para undangan bersuka ria
sambil menyantap nasi kebuli, soto, sate, sup kaki kambing, dan santapan
lain yang disajikan tuan rumah. Bekas Menteri Agama Quraish Shihab
memanjatkan doa.

Tapi tak ada "kado" yang lebih besar yang diterima Soeharto daripada
kabar ini: dokumen asli perkara tujuh yayasan, yang bakal digunakan
Kejaksaan Agung untuk menggugat Soeharto secara perdata, "terselip"
entah ke mana. Tujuh yayasan itu adalah Dakab, Dharmais, Dana
Sejahtera Mandiri, Trikora, Amal Bhakti Muslim Pancasila, Gotong
Royong Kemanusiaan, dan Yayasan Supersemar. Tak seperti pada tuntutan
pidana yang bisa mengandalkan dokumen fotokopi, pada tuntutan perdata
keberadaan dokumen asli sangat vital. Dokumen fotokopi memang masih
bisa digunakan dengan legalisasi. Tapi, seperti kata seorang bekas
petinggi kejaksaan, dengan berkas asli, kejaksaan ibarat ngebut
dengan mobil Mercy baru. Dengan dokumen fotokopi, mereka cuma merayap dengan
Mercy tua.

Jaksa Agung Muda Perdata dan Tata Usaha Negara Alex Sato Bya-lah yang
mula-mula mengabarkan kisah duka ini. Senin dua pekan lalu, dalam
jumpa pers untuk menjelaskan persiapan kejaksaan menggugat Soeharto
secara perdata, ia mengeluh. "Saya hanya menerima sembilan boks
dokumen fotokopi dari Kejaksaan Tinggi DKI Jakarta," katanya. Padahal
rencananya gugatan itu akan didaftarkan bulan depan.

Lebih dari sekadar mengembalikan duit negara yang dipakai tujuh
yayasan itu, keputusan bersalah yang diketuk hakim perdata diyakini
bisa mengembalikan Rp 524 miliar uang putra bungsu Soeharto, Hutomo
Mandala Putra, di Bank Paribas Cabang Guernsey, Inggris.

Seperti telah banyak diberitakan, penyelidik Inggris menyebut uang
Itu terkait dengan Soeharto. Pemerintah mengklaim dana itu dikumpulkan
dari bisnis penuh kolusi dan korupsi yang di antaranya melalui
yayasan Soeharto. Repotnya, belum ada satu pun putusan pengadilan yang
memperkuat klaim itu. Pertengahan Mei lalu, pengadilan Guernsey
membekukan uang itu selama enam bulan. Dalam kurun waktu itu, harus
ada putusan pengadilan yang membuktikan bahwa Soeharto dan
anak-anaknya memang bergelimang harta tak halal.

Salah satu yayasan yang diduga mengalirkan uang ke perusahaan Tommy
Soeharto adalah Supersemar. Yayasan yang tersohor karena rajin
Memberi beasiswa itu berdiri pada 1974. Uang yayasan ini dikeduk dari bank
pemerintah. Saban tahun bank-bank itu harus menyetor 2,5 persen dari
total pendapatan. Walhasil, pundi yayasan ini gampang menjulang.

Pada massa pemerintah B.J. Habibie, kejaksaan menemukan sekitar 84
persen penyimpangan dalam penggunaan dana yayasan ini. Ditemukan pula
bukti cek senilai Rp 450 juta yang ditandatangani Soeharto.
Perusahaan yang kebagian rezeki Supersemar adalah PT Sempati Air, yang didirikan Tommy

dan sahabat Soeharto, Bob Hasan. Perusahaan penerbangan ini
meraup sekitar Rp 40 miliar. Sejumlah perusahaan anak Cendana lainnya
juga menerima duit haram itu.

DALAM proses menuntut Soeharto secara pidana inilah berkas-berkas itu
dikumpulkan. Pada tahun 2000, tim kejaksaan yang diketuai Direktur
Tindak Pidana Korupsi Kejaksaan Agung Chairul Imam memerintahkan
penyitaan setumpuk dokumen di Gedung Granadi di Kuningan, Jakarta
Selatan. Di gedung itulah semua yayasan Soeharto berkantor.

Dokumen yang direbut mahapenting: dari akta pendirian perusahaan,
surat perintah pembayaran, hingga bukti transfer. Bustanil Arifin,
bendahara Amal Bhakti Pancasila, dan petinggi yayasan lainnya
dipanggil. Mereka ditanyai perihal keabsahan dokumen itu. Penyidikan
berbulan-bulan menghasilkan berkas dakwaan lebih dari 2.000 halaman.
Di dalamnya ada hasil pemeriksaan 134 saksi fakta, sembilan saksi
ahli, dan ratusan dokumen.

Chairul cuma sebulan memimpin penyelidikan ini. Dia dicopot dari
jabatan direktur tindak pidana korupsi. Penggantian itu diprotes
sejumlah kalangan karena Chairul dianggap giat mengusut kasus ini.
Ris Pandapotan Sihombing, yang menggantikan Chairul, meneruskan
penyelidikan. Semua dokumen dan berkas perkara diserahkan kepada
jaksa penuntut umum Mukhtar Arifin, yang kini menjadi Wakil Jaksa Agung.
Dari situlah dakwaan disusun.

Tapi berkas dakwaan itu tidak pernah dibacakan di pengadilan karena
pada September 2000 kasus ini dipetieskan. Lalu Mariyun, hakim ketua
kasus ini, menolak mengadili Soeharto karena dia sakit permanen.
Jaksa penuntut umum lalu mengembalikan semua dokumen perkara itu ke
Kejaksaan Negeri Jakarta Selatan. Di sana semua dokumen disimpan di
ruang arsip.

Ruang itu terletak di pojok lantai dasar gedung, tepat di bawah
Tangga menuju lantai dua. Bila ingin masuk ke sana, kita cukup masuk dari
pintu utama, lalu belok kanan hingga mentok di ujung lorong. Tidak
ada penjaga keamanan yang bersiaga. Pintu ruang ini cuma menggunakan
kunci biasa, tidak ada gembok.

Tumpukan dokumen itu kabarnya ngendon di sana hingga akhirnya Jaksa
Agung Abdul Rahman Saleh menerbitkan SKP3, Mei 2006. Sesudah
Keputusan ini terbit, semua berkas itu dikirim ke Kejaksaan Tinggi DKI Jakarta. Dari Kejaksaan Tinggi DKI, tumpukan dokumen itu diteruskan ke
Kejaksaan Agung.

TAPI, dan inilah soalnya, dokumen yang ngendon itu tak semuanya
risalah asli. Perihal asli atau fotokopi saja, aparat kejaksaan
berbeda pendapat. Yoseph Suardi Sabda, Direktur Perdata Kejaksaan
Agung, mengatakan, untuk kasus Yayasan Supersemar, semua dokumen yang
mereka miliki berbentuk fotokopi. Tapi Dachmer Munthe, Direktur
Pemulihan dan Perlindungan Hak Kejaksaan Agung, mengatakan hanya
sebagian kecil dokumen yang mereka miliki adalah berkas asli. Seorang
pejabat Kejaksaan Negeri Jakarta Selatan menyebutkan dokumen itu ada
yang asli dan ada yang fotokopi—baik yang sudah dilegalisasi maupun
yang belum. Tumpukan rupa-rupa dokumen—asli dan fotokopi—itu kini
disimpan di salah satu ruang anak buah Dachmer (lihat "Gado-gado
Berkas Sitaan").

Sebagian dokumen berupa fotokopian karena dokumen asli tidak
ditemukan. Saham asli PT Indocopper Investama Corporation Tbk., anak
perusahaan Nusamba Group, penerima dana yayasan, misalnya, dipegang
Chase Manhattan Bank di New York sebagai agen bank pemberi kredit
sindikasi. Sebagian risalah asli lainnya raib karena belasan
perusahaan penerima dana yayasan pailit dan perlu dijual buat
membayar utang.

Tapi itu sebagian saja. Mantan jaksa penuntut umum kasus Soeharto,
Mukhtar Arifin, mengatakan, dari awal, dokumen yang mereka miliki
sebagian besar berbentuk fotokopi. Mukhtar memastikan dokumen-dokumen
asli itu tidak hilang. Katanya, dokumen asli kasus Soeharto
"dititipkan di manajemen yayasan tempat dokumen itu disita". Besar
kemungkinan dokumen penting itu disimpan di Gedung Granadi. Mukhtar
menggarisbawahi, dulu dokumen-dokumen itu tidak disita karena, dalam
perkara pidana, kejaksaan biasanya tidak menyimpan yang asli. Yang
dipegang penuntut cuma dokumen fotokopi yang sudah dilegalisasi.

Betulkah? Yoseph Suardi Sabda menyangkalnya. Menurut Yoseph, sejumlah
dokumen tentang Yayasan Supersemar yang diterima timnya bukan salinan
yang sudah dilegalisasi. Tim Yoseph sendirilah yang melegalisasi
dokumen-dokumen itu. Dengan kata lain, tanpa dokumen asli, kejaksaan
jadi bekerja dua kali.

Logikanya, daripada berpayah-payah melegalisasi dokumen fotokopi,
bukankah lebih baik mengambil dokumen asli yang dititipkan di Gedung
Granadi? Mestinya begitu. Tapi anehnya, dalam hal mengambil dokumen
titipan itu, kejaksaan terkesan ogah-ogahan. "Tenang sajalah. Kami
sedang memantapkan alat-alat bukti. Kalau ada di Granadi, pasti bisa
dicari. Lagi pula, kalau ada 100 dokumen, kan enggak perlu semuanya
dibutuhkan. Kalau 10 sudah relevan, ya, tidak perlu 100," kata
Dachmer Munthe mantap. Suara yang lebih pesimistis datang dari Yoseph Suardi.
Katanya, "Jangan harap dapat lagi aslinya. Ini saja digunakan. Dapat
saja sudah syukur."

Kejaksaan tampaknya lebih suka mengambil "jalan memutar". Tim Dachmer
Munthe, misalnya, kini sibuk memanggil sejumlah saksi untuk
memverifikasi dokumen fotokopian itu. Yang sudah dipanggil enam
orang. Dachmer memastikan sejumlah anak dan kerabat Soeharto juga bakal
dipanggil.

Keluarga Soeharto tampaknya sudah siaga menghadapi gugatan perdata
ini. Dua bulan lalu, mereka sudah memberikan surat kuasa kepada Otto
Cornelis Kaligis untuk mengurus perkara ini. Kaligis ikut ke Cendana,
Jumat pekan lalu. Selain menghadiri acara ulang tahun Soeharto, di
sana ia mendiskusikan kasus ini.

Menurut Kaligis, akhir Mei lalu Bob Hasan dipanggil kejaksaan menjadi
saksi. Tapi, "Bob Hasan tidak datang karena dia merasa tidak relevan
dengan perkara yayasan itu," kata Kaligis. Pemanggilan para saksi,
kata Kaligis, terkesan asal-asalan. Pertengahan Mei lalu, dua
pengurus yayasan dipanggil kejaksaan. Suratnya dikirim lewat kantor Kaligis.
"Tapi dua orang yang dipanggil itu sudah meninggal dunia," kata
Kaligis.

Itulah sebabnya Kaligis optimistis Cendana bakal menang dalam gugatan
perdata ini. Dokumen yang cuma fotokopian, kata Kaligis, tidak bakal
laku di pengadilan. Katanya, "Ini perkara yang lucu. Mereka menyita
dokumen, lalu membuat salinannya. Setelah itu, bingung sendiri."

AZ/Wenseslaus Manggut, Wahyu Dhyatmika, Arif A. Kuswardono, Sukma N.
Loppies


Kisah Hilangnya Dokumen

CERITA tentang "dokumen hilang" ini bisa dirunut dari awal.
Setelah jatuhnya Soeharto pada 1998, berbekal desakan publik,
Kejaksaan Agung berniat menggugat Soeharto secara pidana. Apalagi
ketika itu MPR telah mengeluarkan ketetapan yang meminta perkara
beraroma korupsi, kolusi, dan nepotisme, termasuk kasus Soeharto,
diusut tuntas (lihat "Bersusah Dahulu, Berpayah Kemudian"). Yang
dianggap paling mudah dibidik adalah penyelewengan tujuh yayasan.
Kejaksaan menduga, oleh yayasan Soeharto, negara dirugikan hingga Rp
5 triliun.

Tapi, Oktober 1999, kejaksaan menerbitkan surat perintah penghentian
penyidikan (SP3) dengan alasan korupsi Soeharto tak bisa dibuktikan.
Abdurrahman Wahid, yang menggantikan Baharuddin Jusuf Habibie sebagai
presiden, lalu menghidupkan lagi perkara ini. Marzuki Darusman, Jaksa
Agung ketika itu, mencabut SP3 tadi. Tapi pengusutan perkara ini
berjalan di tempat. Saat Presiden Megawati Soekarnoputri berkuasa,
kasus ini tetap sunyi senyap.

Belakangan pemerintah Susilo Bambang Yudhoyono malah menguncinya. Mei
2006, Jaksa Agung Abdul Rahman Saleh menerbitkan surat ketetapan
penghentian penuntutan perkara (SKP3). Setelah dihujat kiri-kanan,
Presiden Yudhoyono akhirnya melansir bahwa kasus ini cuma diendapkan.
Yudhoyono menebar janji lain: menuntut Soeharto secara perdata.

Tapi surat kuasa dari Presiden kepada Kejaksaan Agung, sebagaimana
diwajibkan undang-undang, tidak kunjung diterbitkan. Padahal surat
kuasa itu diperlukan Kejaksaan Agung guna mengajukan gugatan.
Walhasil, sejak Mei 2006, rencana gugatan itu cuma meriah di
koran-koran. Surat kuasa itu baru terbit delapan bulan kemudian,
Januari 2007.
Bersusah Dahulu, Berpayah Kemudian

1998:
- 13 November
Ketetapan No. XI/MPR/1998 tentang Pemberantasan Korupsi, Kolusi, dan
Nepotisme, termasuk yang dilakukan bekas presiden Soeharto,
dikeluarkan MPR.

- 2 Desember
Presiden mengeluarkan Instruksi No. 30 Tahun 1998 kepada Jaksa Agung
agar segera mengambil tindakan hukum terhadap kasus korupsi Soeharto.

- 4 Desember
Direktur Tindak Pidana Korupsi Jaksa Agung mengundang Soeharto untuk
didengar keterangannya pada 9 Desember 1998.

- 9 Desember
Soeharto menghadiri undangan pemeriksaan Kejaksaan Agung. Tempat
pemeriksaan yang semula di gedung Kejaksaan Agung dialihkan ke
Kejaksaan Tinggi DKI Jakarta.

1999
- 11 Oktober Penjabat Jaksa Agung Ismudjoko mengeluarkan surat
perintah penghentian penyidikan (SP3). Salah satu pertimbangannya,
korupsi yang disangkakan kepada Soeharto tidak dapat dibuktikan.

- 6 Desember
Jaksa Agung Marzuki Darusman mencabut SP3 itu. Alasannya, cukup dasar
untuk melanjutkan penyidikan terhadap Soeharto.

2000

- 31 Mei
Soeharto mengajukan praperadilan yang mempertanyakan pencabutan SP3.
Permohonan ditolak hakim.

- 21 Agustus
Pengadilan Negeri Jakarta Selatan memindahkan lokasi persidangan dari
Pengadilan Negeri Jakarta Selatan ke Departemen Kesehatan.

- 31 Agustus
Sidang pertama kasus Soeharto, tapi bekas presiden itu tidak datang
dengan alasan sakit.

- 14 September
Hakim memberikan kesempatan kepada tim dokter Soeharto dan tim dokter
Kejaksaan Agung untuk memberikan penjelasan terperinci tentang
kondisi
Soeharto. Hakim menetapkan pembentukan tim dokter untuk memeriksa
kesehatan Soeharto.

- 28 September
Tim dokter menyatakan Soeharto dalam keadaan tidak fit untuk diadili.
Hakim menyatakan tuntutan pidana terhadap Soeharto tidak dapat
diterima. Soeharto juga dibebaskan dari tahanan kota. Jaksa banding.

- 8 November
Pengadilan Tinggi DKI Jakarta menerima banding jaksa dan membatalkan
keputusan pengadilan negeri.

- 17 November
Pengacara Soeharto mengajukan kasasi ke Mahkamah Agung.

2001
- 2 Februari
Mahkamah Agung memutuskan menerima kasasi pengacara Soeharto dan
membatalkan keputusan Pengadilan Tinggi DKI Jakarta.

2006
- 12 Mei
Jaksa Agung Abdul Rahman Saleh mengeluarkan surat ketetapan
penghentian penuntutan perkara (SKP3) Soeharto.

- 22 Mei
Tujuh lembaga swadaya masyarakat mengajukan gugatan praperadilan
terhadap Jaksa Agung karena mengeluarkan SKP3 Soeharto.

- 1 Agustus
Pengadilan Tinggi DKI Jakarta membatalkan putusan praperadilan
Pengadilan Negeri Jakarta Selatan tentang SKP3 Soeharto.

- 28 September
Pengadilan Negeri Jakarta Selatan menolak permohonan kasasi terhadap
putusan banding penerbitan SKP3 Soeharto.

2007
- Mei
Kejaksaan Agung menyampaikan rencana menggugat Soeharto secara
perdata.

buatan Radja|endro

· * * * *
·
Majalah TEMPO: Edisi. - 17 Juni 2007
Opini

Kaburnya Dokumen Asli Soeharto

SOEHARTO genap 86 tahun Jumat pekan lalu. Karangan bunga yang
mengalir ke Jalan Cendana sudah cukup menunjukkan betapa kuat pengaruh bekas
presiden yang berkuasa 32 tahun itu. Pengaruh kuat juga yang membuat
pemerintah seperti berada di posisi "anjing menyalak di ekor gajah"
dalam menuntaskan kasus dugaan korupsi oleh tokoh legendaris ini.
Sedangkan Soeharto sendiri bisa tiba-tiba sakit keras kalau akan
dihadapkan ke meja hijau, tapi bugar kalau punya hajatan keluarga.
Terlepas dari kondisi itu memang keadaan sebenarnya atau sekadar
taktik menghindar, faktanya ia tak pernah bisa dibawa ke pengadilan.
Dan sekarang sebagian dokumen asli—mungkin malah sebagian besar—kasus
tujuh yayasan Soeharto hilang.

Tentu orang masih ingat, Pengadilan Negeri Jakarta Selatan pada 2002
gagal menghadirkan Soeharto ke pengadilan kasus dugaan korupsi di
tujuh yayasan yang ia dirikan. Ketua majelis hakim Lalu Mariyun
percaya Soeharto benar-benar mengidap sakit permanen. Palu diketuk,
sidang ditutup dengan memberi tugas kepada kejaksaan untuk
menyembuhkan terdakwa.

Sulit untuk melacak sejauh mana upaya kejaksaan "mengobati" Soeharto.
Alhasil, Mahkamah Agung kemudian memutuskan tokoh Orde Baru ini tak
bisa dibawa ke depan meja hijau. Kasusnya mulai dilupakan. Publik
kembali ingat justru ketika Jaksa Agung Abdul Rahman Saleh pada Mei
2006 mengeluarkan keputusan menghentikan penuntutan pidana terhadap
Soeharto dan akan memulai gugatan baru, yakni gugatan perdata. Yang
penting, menurut Abdul Rahman, uang negara kembali, bukan
menjebloskan orang yang sudah tua renta ke penjara. Sayangnya, sampai Abdul Rahman
kena reshuffle dan digantikan Hendarman Supandji, langkah penuntutan
itu baru pada tahap wacana.

Hendarman Supandji tentu sulit bergerak maksimal dalam gugatan
perdata dengan raibnya dokumen asli. Tentu saja ini bukan kebetulan belaka.
Sebab, mustahil dokumen sebanyak sembilan filing cabinet itu turun
sendiri dari lantai atas gedung Kejaksaan Agung dan menghilang di
keramaian Jakarta. Layak ditanyakan: ini benar-benar hilang atau
dihilangkan? Siapa yang terlibat?

Patut disayangkan, meski ini kasus yang serius, ternyata kejaksaan
dengan enteng menyebutkan itu tak menjadi masalah. Fotokopi bisa
dilegalisasi dengan pengakuan saksi-saksi dan dengan mendatangkan
saksi ahli. Belakangan, setelah masyarakat dibuat bertanya-tanya,
muncul ralat: "Dokumen itu memang sejak dulu sudah berupa fotokopi,
hanya beberapa yang asli."

Jawaban tersebut membingungkan sekaligus menerbitkan pertanyaan
besar: seriuskah para jaksa itu menggugat Soeharto? Soal dokumen fotokopi
saja sudah melahirkan pro dan kontra di antara para jaksa. Ada jaksa
yang bilang dokumen fotokopi sangat lemah sebagai barang bukti, ada
pula yang bilang tidak jadi masalah asalkan ada saksi yang kuat.
Padahal sebagian ahli hukum yakin, dalam kasus perdata, dokumen lebih
penting sebagai alat bukti daripada kesaksian. Pengacara Soeharto
saja mengatakan alat bukti fotokopi itu gampang dipatahkan di
sidang—misalnya dengan menuduh "dokumen fotokopi itu bisa saja
merupakan hasil rekayasa".

Dengan posisi "hampir pasti kalah" ini, orang bisa saja beranggapan
jangan-jangan gugatan perdata ini bukan lagi "proyek hukum". Siapa
tahu ini semata-mata usaha meyakinkan masyarakat bahwa pemerintah
serius dalam kasus Soeharto. Sebutlah semacam "proyek tebar pesona"
menyongsong Pemilu 2009. Ujung-ujungnya tak ada apa-apa.

Kecurigaan ini bukan tanpa alasan. Di kalangan pejabat sebenarnya tak
ada kesatuan pendapat tentang bagaimana memposisikan Soeharto:
diproses secara hukum atau "dilupakan" sebagai bagian dari masa lalu
yang kelam. Singkat kata, ada yang memang serius ingin menggugat
dengan target kekayaan Soeharto yang didapat dengan cara tak halal
itu bisa kembali ke kas negara, tapi ada yang enggan mengungkit-ungkit
masalah ini. Dalihnya, itu hanya membuang-buang energi dan waktu,
sementara masalah yang dihadapi bangsa ini ke depan sangatlah
kompleks.

Betapapun kompleksnya masalah yang dihadapi bangsa ini, menggugat
Soeharto sesungguhnya merupakan keharusan. Ini soal keadilan, soal
kesetaraan warga negara di muka hukum, dan soal usaha mengoreksi
kesalahan yang terjadi di masa lalu agar tak terulang di masa depan.

Karangan bunga biarlah tetap mengalir ke Cendana saat Soeharto
berulang tahun—pertanda secara kemanusiaan kita menghormati dia.
Namun pemerintah harus punya kemauan politik untuk mengejar triliunan
rupiah yang diduga diselewengkan yayasan yang dipimpin Soeharto. Tanpa
kemauan politik itu, urusan menggugat Soeharto akan selalu diwarnai
hal-hal menggelikan seperti "kabur"-nya dokumen asli itu.

buatan Radja|endro

========================================================

Koran Tempo - Senin, 11 Juni 2007

Gugatan Perdata terhadap Soeharto Tidak Efektif

Guru Besar Hukum Universitas Padjadjaran Romli Atmasasmita pesimistis
terhadap langkah Kejaksaan Agung yang akan menggugat Soeharto secara
perdata. Selain tidak efektif, dia menilai gugatan itu sangat
terlambat. "Saya pesimistis. Seharusnya ini dilakukan dari dulu,"
kata
Romli Atmasasmita, yang dihubungi Tempo melalui sambungan telepon
kemarin.

Rencana pemerintah menggugat Soeharto secara perdata memang santer
diberitakan dalam sepekan terakhir. Tim Jaksa Pengacara Negara terus
mengumpulkan bukti yang akan digunakan dalam gugatan ini. Sayangnya,
semua bukti berupa dokumen yang dimiliki kejaksaan hanya berbentuk
salinan (fotokopi). Sedangkan dokumen asli--yang pernah disita untuk
digunakan dalam penyidikan perkara pidana dugaan korupsi yayasan
Soeharto--dikembalikan kepada pihak yayasan.

Masalah bukti-bukti tersebut mendapat sorotan tajam dari Romli.
Menurut master hukum dari University of California, Berkeley, ini,
bukti otentik sangat penting dalam gugatan perdata. Jika semua bukti
itu berada di tangan tergugat, kejaksaan akan menghadapi persoalan
besar. Sebab, lembaga hukum ini tidak memiliki wewenang meminta
dokumen secara paksa. "Kalau yang aslinya dipegang yang punya, ya,
wassalam," ujarnya.

Menurut Romli, seharusnya gugatan perdata itu diajukan setelah ada
keputusan pidana yang menyatakan Soeharto bersalah. Namun, yang
terjadi, pada masa kepemimpinan Abdul Rahman Saleh, kejaksaan justru
mengeluarkan surat penghentian penyidikan (SP3) terhadap Soeharto.
"SP3 bisa dikeluarkan kalau kurang alat bukti, bukan karena tersangka
sakit," ujarnya.

Praktisi dan ahli hukum perdata Luhut Pangaribuan menyampaikan
pendapat senada. Dia melihat sejumlah kejanggalan. "Gugatan perdata
ini lebih merupakan konsumsi politik daripada suatu perbuatan melawan
hukum yang serius."

Pengacara Soeharto, Elsa Syarief, menyatakan belum mengambil sikap
berkaitan dengan rencana pemerintah yang akan mengajukan gugatan
perdata terhadap kliennya. "Saya belum bisa berkomentar," katanya.

Menurut Elsa, dia baru mengetahui tuntutan itu dari media massa. Tim
pengacara Soeharto pun, kata dia, belum menyiapkan langkah hukum
selanjutnya. TITO SIANIPAR | KURNIASIH BUDI


======================================

Koran Tempo - Senin, 11 Juni 2007

Kejaksaan Siap Buktikan Kerugian Negara

JAKARTA -- Pemerintah akan mengajukan ganti rugi Rp 15 triliun dalam
gugatan perdata terhadap Yayasan Supersemar milik mantan presiden
Soeharto. Perinciannya, kerugian negara Rp 1,5 triliun ditambah bunga
dan ganti rugi imateriil. "Totalnya Rp 15 triliun," kata Direktur
Perdata Kejaksaan Agung Yoseph Suardi Sabda saat dihubungi Tempo dua
hari lalu.

Gugatan perdata ini memang baru difokuskan pada Yayasan Supersemar.
"Yang lainnya masih diproses," kata Yoseph. Surat kuasa khusus untuk
kejaksaan sebagai jaksa pengacara negara sudah diberikan kepada
Presiden sejak Februari lalu. Kejaksaan sendiri mengagendakan paling
lambat gugatan sudah didaftarkan ke pengadilan negeri Jakarta Selatan
pada 22 Juli 2007.

Menurut Yoseph, nilai kerugian negara tersebut dihitung dari dana
negara yang disalahgunakan oleh yayasan. "Pengeluaran dana itu tidak
bisa dipertanggungjawabkan," katanya. Yayasan memang bisa berdalih
menggunakan dana yayasan untuk kegiatan sosial, seperti pemberian
beasiswa. Namun, kenyataannya, uang negara itu justru digunakan untuk
membiayai perusahaan keluarga, seperti Sempati Air, Kiani Kertas, PT
Timor Putra Nasional, dan Goro.

Ganti rugi ini harus diserahkan kepada pemerintah dalam waktu delapan
hari setelah putusan pengadilan yang final dan mengikat (inkracht).
"Jika tidak dipenuhi, kejaksaan akan mengajukan permohonan penyitaan
atau eksekusi kepada pengadilan," ujar Yoseph.

Sebelumnya, Direktur Akuntansi dan Pelaporan Keuangan Departemen
Keuangan Hekinus Manao mengatakan saat ini departemen aktif
berkomunikasi dengan pihak pengelola yayasan Soeharto untuk
pengalihan
aset-aset yayasan. "Karena kami tahu dananya berasal dari penyisihan
sebagian keuntungan perusahaan negara," kata Hekinus.

Berdasarkan Undang-Undang Nomor 17 Tahun 2003 tentang Keuangan
Negara,
aset di sejumlah yayasan itu merupakan hak negara karena dana yayasan
bersumber dari pemerintah, yakni dari penyisihan laba badan usaha
milik negara.

Berkaitan dengan rencana pengambilalihan aset ini, Yoseph mengatakan,
tidak tertutup kemungkinan ada upaya damai dalam proses gugatan
perdata ini. Biasanya majelis hakim selalu memberikan waktu untuk
mediasi atau berdamai dalam waktu dua minggu kepada pihak-pihak yang
beperkara.

Pengacara Soeharto, Otto Cornelis Kaligis, menyatakan siap menghadapi
langkah-langkah hukum yang akan dilakukan kejaksaan, "Kalau mau
mengajukan gugatan perdata, kami siap," ia menegaskan.

Namun, terhadap rencana pengambilalihan aset yayasan yang akan
dilakukan Departemen Keuangan, Kaligis menentang. Menurut dia,
pengurus yayasan tidak memiliki kapasitas menerima atau menolak
pengambilalihan aset tersebut tanpa izin pemiliknya. "Pak Harto itu
pendiri, pemilik, dan ketua yayasan-yayasan itu. Jadi harus ada izin
(dari Soeharto)," ujarnya.

Departemen Keuangan sebagai wakil pemerintah tidak bisa seenaknya
mengambil kebijakan itu. "Tidak ada dasar hukumnya," kata dia.
Pemerintah mesti bisa membuktikan bahwa dana yayasan itu memang
benar-benar milik negara. Bentuk pembuktiannya sendiri harus melalui
jalur hukum, yakni kejaksaan sebagai lembaga negara yang memiliki
kewenangan dalam bidang hukum dan penuntutan. RINI KUSTIANI | AGUS
SUPRIYANTO

Koran Tempo - Senin, 11 Juni 2007
========================================
Jawapos, 11 Juni 2007,

Baru Terkumpul Satu Dokumen Asli

Alat Bukti Kasus Soeharto



JAKARTA - Kejaksaan Agung (Kejagung), tampaknya, sulit
menyiapkan alat bukti terkait dengan gugatan terhadap mantan
Presiden Soeharto. Saat ini, tim jaksa pengacara negara (JPN) baru
mengumpulkan satu dokumen asli sebagai alat bukti kasus korupsi
tujuh yayasan senilai Rp 1,7 triliun.

"Dokumen asli itu diserahkan seorang saksi yang kami periksa
dua pekan lalu," kata Yoseph Suardi Sabda, salah seorang anggota tim
JPN, saat dihubungi koran ini kemarin.

Saat ditanya soal identitas saksi yang menyerahkan dokumen
tersebut, dia menolak menyebutkan. "Kami baru membukanya dalam
persidangan kelak," ujar Yoseph yang menjabat direktur perdata
Kejagung tersebut.

Yang pasti, kata dia, dokumen asli tersebut sangat membantu
langkah kejaksaan menyiapkan gugatan. Sebab, dokumen itu
merupakan "induk" beberapa barang bukti berupa dokumen
fotokopian. "Kami berharap ada saksi lain yang
juga menyerahkan dokumen asli lagi," ungkapnya.

Menurut Yoseph, tim JPN saat ini berharap mendapatkan dokumen
asli surat perintah membayar uang (SPMU) yang ditandatangani
Soeharto selaku ketua yayasan. "Itu akan menunjukkan peran Soeharto
yang memerintahkan mengeluarkan uang yayasan untuk beberapa
perusahaan kroni," jelasnya.

Sayangnya, tim JPN hanya punya dokumen fotokopian sebagaimana
yang disimpan dalam sembilan brankas penyimpanan (filling cabinet)
berkas kasus Soeharto.

Yoseph menyatakan, untuk mengganti SPMU dan dokumen asli lain,
tim JPN melanjutkan pemeriksaan beberapa saksi fakta yang mengetahui
dokumen-dokumen terkait dengan kasus Soeharto. Di antara 43 saksi,
tim JPN baru memeriksa sepuluh saksi. "Sebagian dari pengurus
yayasan (Supersemar)," ujarnya. Mereka bukan pengurus inti, ketua
atau bendahara, melainkan staf pengurus.

Ditanya tentang rencana pemeriksaan Soeharto, Yoseph
menegaskan bahwa tim JPN tidak mengagendakan. "Itu kan kasus
perdata. Lagi pula, Pak Harto kan masih sakit permanen," tegasnya.

Di tempat terpisah, pengacara Soeharto, O.C. Kaligis,
menjelaskan bahwa tim pengacara terus memantau rencana kejaksaan
mengajukan gugatan kliennya dan pengurus yayasan.

Menurut dia, sejauh ini tim pengacara tidak membicarakan
perkembangan gugatan dan kasus Tommy Soeharto saat menghadiri
perayaan HUT Soeharto pekan lalu. "Nggak lah. Kalau soal itu, saya
tidak bicara dengan Pak Harto," ungkapnya.

Pengacara berambut putih itu menyatakan, saat bertemu
Soeharto, dirinya hanya mengucapkan selamat ulang tahun dan
menanyakan soal kesehatannya.

Soal kondisi kesehatan Soeharto, Kaligis menegaskan tidak ada
perbaikan. Dia menuturkan, gangguan kesehatan di bagian otak secara
permanen (permanent brain damage) masih terjadi. "Beliau memang bisa
berkomunikasi, tapi sangat lambat berbicara," katanya.

Sebelumnya, JAM Perdata dan Tata Usaha Negara (Datun) Alex
Sato Bya mengaku terkejut begitu mendapati alat bukti kasus Soeharto
yang disimpan di sembilan filling cabinet merupakan dokumen
fotokopian. Dia tidak tahu apakah dokumen aslinya hilang atau
sengaja dihilangkan. Nah, kejaksaan kini berupaya mendapatkan
dokumen-dokumen asli sebagai materi gugatan kasus
Soeharto.

Muchtar Arifin, koordinator tim jaksa penuntut umum (JPU),
mengungkapkan, dokumen asli kasus Soeharto dititipkan kepada
manajemen yayasan saat terjadi penggeledahan di gedung Garanadi,
Jakarta Selatan. Hal itu dilakukan untuk mencegah kemungkinan
hilangnya dokumen tersebut. (agm)

* * *

Koran Tempo, 4 Juni 2007

Dokumen Asli Soeharto Belum Ditemukan etc

JAKARTA -- Kejaksaan Agung hingga saat ini masih mencari dokumen asli
terkait dengan yayasan Soeharto yang sedianya dijadikan barang bukti
dalam penuntutan perdata terhadap mantan presiden tersebut. "Saya
tidak tahu apakah dokumen itu hilang, disimpan, atau bagaimana. Yang
jelas, sedang dicari," kata Kepala Pusat Penerangan Hukum Kejaksaan
Agung Salman Maryadi kepada Tempo kemarin.

Dokumen itu disimpan oleh Kejaksaan Tinggi Jakarta setelah kasus
tindak pidana korupsi Soeharto tak dapat disidangkan. Namun, saat tim
jaksa pengacara negara pimpinan Dachmer Munthe akan memakainya dalam
tuntutan perdata, yang diserahkan hanya fotokopiannya.

Jaksa Agung Muda Perdata dan Tata Usaha Negara Alex Sato Bya pun
mengaku tak tahu di mana persisnya berkas asli itu berada. ''Saat
saya terima sembilan filing cabinet berkas dalam keadaan fotokopian,''
katanya.

Agak susah melacaknya karena sudah enam tahun dokumen itu "tak
diperhatikan". Pada September 2000 ketua majelis hakim Pengadilan
Negeri Jakarta Selatan, Lalu Mariyun, menolak mengadili perkara
karena Soeharto sakit permanen. Pada Mei 2006 Jaksa Agung Abdul Rahman Saleh
menghentikan penuntutan perkara. Di antara dua waktu itu, berkas
tersebut cuma teronggok.

Tujuh tahun lalu, Chairul Imam, mantan Direktur Tindak Pidana Korupsi
Kejaksaan Agung, telah mendapatkan dokumen-dokumen asli. Bahkan ia
sempat mengecek keabsahan dokumen tersebut ke sejumlah saksi kunci,
seperti Bustanil Arifin, mantan Menteri Koperasi yang juga Bendahara
Yayasan Amal Bhakti Pancasila. "Ah, masak sih hilang? Kalau benar,
pasti ada sabotase," kata Chairul Imam berang.

Meski berkas belum ditemukan aslinya, proses penuntutan perdata
terhadap Soeharto jalan terus. Menurut Munthe, dokumen fotokopian
yang diperlukan nantinya akan dilegalisasi dan dikonfirmasi ke beberapa
sumber yang terkait. Timnya, kata dia, akan meminta konfirmasi kepada
43 saksi yang bisa membenarkan keabsahan surat itu.

Menurut Munthe, walau tidak mau mengungkap siapa saja saksi yang akan
diperiksa, saksi yang akan dimintai keterangan oleh tim jaksa adalah
saksi yang pernah dipanggil dan terkait dengan kasus pidana Soeharto.
''Juga tim jaksa yang kala itu menangani,'' katanya.

''Selain itu, akan diminta keterangan saksi ahli,'' katanya. Dengan
legalisasi dan konfirmasi itu, dokumen fotokopian pun akan sesakti
aslinya. "Legalisasi dan pembenaran saksi bisa menjadikan dokumen
menjadi alat bukti,'' ujarnya.

Hal yang sama dikatakan oleh Direktur Perdata Kejaksaan Agung Yoseph
Suardi Sabda. Dia mengatakan, untuk memperkuat gugatan perdata,
memang harus dilampirkan bukti dokumen asli.

''Jika memang tidak ada, legalisasi pun bisa dikuatkan dengan
pengesahan penyidik, notaris, atau pejabat yang berwenang,'' katanya.

Namun, seorang jaksa yang menolak namanya dikutip meragukan dokumen
itu hilang. Dia khawatir berkas penting itu disimpan di tempat lain
karena khawatir "dihilangkan". Ia menunjuk sejumlah insiden
mencurigakan sepanjang penyidikan kasus korupsi Soeharto: ledakan bom
di Gedung Bundar pada Juli 2000 dan kebakaran di gedung Badan
Pengawasan Keuangan dan Pembangunan tiga bulan sesudahnya.
Setidaknya, "Salinan berkas perkara disimpan di dua tempat itu," katanya. Hingga
kini tak jelas di mana berkas perkara Soeharto itu disimpan. SANDY
INDRA PRATAMA | WAHYU DHYATMIKA



* * *

Kompas, 2 Juni 2007
Tiga Saksi Kasus Soeharto Sudah Dipastikan


Kejaksaan Agung mulai memastikan keterangan sejumlah saksi yang pernah
memberikan keterangan dalam penyidikan perkara dugaan korupsi mantan
Presiden Soeharto. Keterangan saksi itu digunakan untuk menguatkan
barang bukti berupa fotokopi dokumen-dokumen, yang akan digunakan
dalam menggugat perdata Soeharto dan Yayasan Supersemar.

Direktur Perdata pada Bagian Perdata dan Tata Usaha Negara Kejaksaan
Agung Yoseph Suardi Sabda, Kamis (31/5), menyampaikan, tiga saksi
sudah didatangi dan dipastikan keterangannya. Namun, Yoseph menolak
menyebutkan siapa saja saksi yang dimintai keterangan itu.

"Keterangan mereka menguatkan barang bukti dokumen yang kami miliki,"
kata Yoseph.

Dengan demikian, meskipun bukti berupa fotokopi dokumen, diyakini hal
itu dapat mendukung gugatan perdata atas perbuatan melawan hukum
Soeharto dan Yayasan Supersemar. Rencananya, dalam gugatan tersebut,
Kejaksaan selaku jaksa pengacara negara akan mengajukan ganti rugi
materiil Rp 1,5 triliun dan imateriil Rp 10 triliun.

Secara terpisah, Kepala Pusat Penerangan Hukum Kejaksaan Agung Salman
Maryadi mengatakan, Bagian Perdata dan Tata Usaha Negara Kejaksaan
Agung akan berkoordinasi dengan Kejaksaan Agung dalam mencari
saksi-saksi kasus Soeharto.

Yoseph Suardi Sabda pernah menyampaikan, sebanyak 43 saksi yang pernah
bersaksi saat pemeriksaan perkara Soeharto akan disortir lagi.

"Dicari, siapa yang relevan untuk perkara perdata. Harus dipastikan
juga mereka mau bersaksi dalam gugatan perdata," ujar Yoseph.

Kasus PT Timor disidik

Dugaan kerugian negara, sesuai informasi tim penyidik, masih dihitung
dengan meminta bantuan Badan Pengawasan Keuangan dan Pembangunan,"
kata Salman Maryadi pada Kamis siang.

Dihubungi Kamis malam, Elza Syarief, salah seorang pengacara Hutomo
Mandala Putra atau Tommy Soeharto—pemilik PT TPN—mengaku sudah
mendengar perihal penyidikan jaksa itu. Begitu pula kliennya. Namun,
Elza menolak menanggapi dimulainya penyidikan perkara yang diduga
melibatkan kliennya di PT TPN, yang tak lama dilakukan setelah
penyidikan dugaan korupsi di Badan Penyangga dan Pemasaran Cengkeh.
"Kita lihat saja nanti," kata Elza. (idr)

* * *


Kompas, 2 Juni 2007

Tiga Saksi Kasus Soeharto Sudah Dipastikan &

Benarkan Dokumen

Jakarta, Kompas - Kejaksaan Agung mulai memastikan keterangan
sejumlah saksi yang pernah memberikan keterangan dalam penyidikan
perkara dugaan korupsi mantan Presiden Soeharto. Keterangan saksi
itu digunakan untuk menguatkan barang bukti berupa fotokopi dokumen-
dokumen, yang akan digunakan dalam menggugat perdata Soeharto dan
Yayasan Supersemar.

Direktur Perdata pada Bagian Perdata dan Tata Usaha Negara
Kejaksaan Agung Yoseph Suardi Sabda, Kamis (31/5), menyampaikan,
tiga saksi sudah didatangi dan dipastikan keterangannya. Namun,
Yoseph menolak menyebutkan siapa saja saksi yang dimintai keterangan
itu. "Keterangan mereka menguatkan barang bukti dokumen yang kami
miliki," kata Yoseph.

Dengan demikian, meskipun bukti berupa fotokopi dokumen,
diyakini hal itu dapat mendukung gugatan perdata atas perbuatan
melawan hukum Soeharto dan Yayasan Supersemar. Rencananya, dalam
gugatan tersebut, Kejaksaan selaku jaksa pengacara negara akan
mengajukan ganti rugi materiil Rp 1,5 triliun dan imateriil Rp 10
triliun.

Secara terpisah, Kepala Pusat Penerangan Hukum Kejaksaan Agung
Salman Maryadi mengatakan, Bagian Perdata dan Tata Usaha Negara
Kejaksaan Agung akan berkoordinasi dengan Kejaksaan Agung dalam
mencari saksi-saksi kasus Soeharto.

Yoseph Suardi Sabda pernah menyampaikan, sebanyak 43 saksi
yang pernah bersaksi saat pemeriksaan perkara Soeharto akan disortir
lagi.

"Dicari, siapa yang relevan untuk perkara perdata. Harus
dipastikan juga mereka mau bersaksi dalam gugatan perdata," ujar
Yoseph.

Kasus PT Timor disidik

"Dugaan kerugian negara, sesuai informasi tim penyidik, masih
dihitung dengan meminta bantuan Badan Pengawasan Keuangan dan
Pembangunan," kata Salman Maryadi pada Kamis siang.

Dihubungi Kamis malam, Elza Syarief, salah seorang pengacara
Hutomo Mandala Putra atau Tommy Soeharto—pemilik PT TPN—mengaku
sudah mendengar perihal penyidikan jaksa itu. Begitu pula kliennya.
Namun, Elza menolak menanggapi dimulainya penyidikan perkara yang
diduga melibatkan kliennya di PT TPN, yang tak lama dilakukan
setelah penyidikan dugaan korupsi di Badan Penyangga dan Pemasaran
Cengkeh. "Kita lihat saja nanti," kata Elza. (idr)


================================


Jawapos, 2 Juni 2007,

Tiga Saksi Bank Benarkan Dokumen

Rencana Gugatan Perdata Mantan Presiden Soeharto



JAKARTA - Kejaksaan Agung (Kejagung) mulai memeriksa para saksi
fakta atas rencana gugatan terhadap mantan Presiden Soeharto. Tiga
saksi dari perbankan, misalnya, sudah memberikan keterangan yang
menguatkan pernah mengetahui dokumen asli berisi aliran dana dari
Yayasan Supersemar ke beberapa perusahaan kroni Soeharto, termasuk
milik Tommy Soeharto.

"Saya sudah memeriksa tiga saksi. Mereka menyatakan confirm atas
dokumen fotokopi yang saya tunjukkan. Artinya, mereka menyatakan
fotokopian itu benar asli adanya," kata Dachmer Munthe, ketua tim
jaksa pengacara negara (JPN) gugatan Soeharto, kepada Jawa Pos
kemarin.

Ditanya soal siapa dan dari mana tiga saksi tersebut, dia menolak
menjelaskan. "Saya nggak bisa menyebutkan. Itu terkait dengan
strategi kami," ujar direktur Pemulihan dan Perlindungan HAM di JAM
Datun tersebut.

Yang pasti, kata dia, tiga saksi dari perbankan tersebut sangat
kooperatif selama memberikan keterangan di gedung JAM Perdata dan
Tata Usaha (Datun), kompleks Kejagung, Kamis lalu.

Menurut Dachmer, tiga saksi itu akan dihadirkan dalam persidangan
gugatan Soeharto. Keterangan mereka nanti dikuatkan dengan alat
bukti lain, termasuk saksi-saksi lain. "Fotokopian itu juga menjadi
alat bukti," ungkap jaksa senior tersebut.

Dia menambahkan, selain tiga saksi itu, tim jaksa melanjutkan
memanggil dan memeriksa beberapa pihak yang mengetahui aliran dana
dari Yayasan Supersemar ke perusahaan kroni Soeharto, termasuk dari
pengurus yayasan tersebut.(agm)

* * *


Sinar Harapan, 1 Juni 2007


Kejaksaan Yakini Bukti Dokumen

Soal Soeharto Cukup Kuat

Jakarta- Kejaksaan Agung (Kejagung) menegaskan legalisasi fotokopi
dokumen untuk berkas gugatan perdata terhadap Yayasan Supersemar bisa
dikuatkan dengan pengesahan penyidik, notaris, atau pejabat yang
berwenang. Dengan demikian, langkah gugatan terhadap Soeharto
diyakini tak akan "bermasalah" dari barang bukti dokumen yang dimiliki
Kejaksaan. "Jika dokumennya (asli) tidak ada, maka legalisasinya bisa
dikuatkan dengan pengesahan penyidik, notaris, maupun pejabat yang
berwenang," kata Direktur Perdata Kejaksaan Agung (Kejagung) Yoseph

Suardi Sabda kepada SH ,Kamis (31/5).

Yoseph menambahkan, hingga saat ini Kejagung memang masih berusaha
mencari dokumen asli guna melengkapi berkas gugatan perdata dalam
kasus kosupsi pada Yayasan Supersemar yang pernah dipimpin mantan
Presiden Soeharto. Akan tetapi, Yoseph menegaskan ketiadaan dokumen
asli tersebut bukan menjadi halangan bagi Kejagung untuk tetap
memperkarakan secara perdata.

Sementara itu, Kapuspenkum Kejagung Salman Maryadi mengatakan
Pihaknya sedang menelusuri dokumen kasus mantan Presiden Soeharto.

Kejagung,menurutnya, perlu mengklarifikasi apakah dokumen itu memang

sebagianbesar fotokopi atau hanya sebagian kecil saja. Menurut Salman,

jaksa pengacara negara yang diketuai Bachmer Munte sedang mencari data-data
dan bukti asli untuk pembuktian gugatan perdata.

Secara terpisah, Kabid Penum Mabes Polri Kombes Pol Bambang Kuncoko
meminta perbankan tidak diam saja dalam membantu menelusuri dugaan
pencucian uang sebesar 10 juta dolar AS dari BNP Paribas ke
Indonesia. Perbankan, menurutnya, harus proaktif mengungkap kasus dana

milik Tommy Soeharto. (rafael sebayang)



* * *

Balipost, 24 Mei 2007

26 Kali Gagal Usut Kasus KKN Soeharto

Jaksa Agung targetkan sebelum 22 Juli


JAKARTA, BPOST - Salah satu amanat reformasi adalah membongkar kasus
dugaan kolusi, korupsi dan nepotisme (KKN) mantan Presiden Soeharto.
Namun, upaya ini selalu gagal. Bahkan, pada 11 Mei 2006, Jaksa Agung
(saat itu) Abdul Rahman Saleh mengeluarkan Surat Perintah Penghentian
Penyidikan (SP3) dengan alasan Soeharto sakit permanen.

Kini posisi jaksa agung dipegang Hendarman Supandji. Dia bertekad
terus mengusut kasus-kasus yang melibatkan Soeharto. Berkasnya
ditargetkan selesai sebelum HUT ke-47 Kejaksaan pada 22 Juli untuk
selanjutnya diserahkan ke Pengadilan Negeri Jakarta Pusat.

"Kita punya satu target, itu semua bisa dilengkapi sebelum HUT
Kejaksaan," kata Hendarman di Jakarta, Rabu (23/5). Dia mengatakan,
ekspose alias gelar perkara untuk kasus itu sudah dilakukan Kejagung.
Dari ekspose itu, ada hal-hal yang perlu dipenuhi, yaitu alat bukti.
"Ada dokumen-dokumen seperti fotokopi yang tentunya perlu proses
legalisasi, supaya fotokopi itu menjadi alat bukti," ujarnya.

Hendarman pun menegaskan Presiden Susilo Bambang Yudhoyono sudah
mengeluarkan surat kuasa khusus untuk gugatan Soeharto. Dalam gugatan
ini, kejaksaan melihat adanya perbuatan melawan hukum

Tekad kejaksaan ini ditanggapi dengan sinis oleh salah satu kuasa
keluarga Cendana, OC Kaligis. "Mereka sudah 26 kali gagal. Mana
allegation (dakwaan)? Mana charge (tuntutan)? Mana final judgment
(putusan final yang mengikat)?" tegas Kaligis yang kini mendampingi
Tommy Soeharto dalam kasus dugaan pencucian uang (money laundering)
dalam persidangan di Royal Court Guernsey, Inggris.

Karena itu, Kaligis mengaku heran dengan sikap pemerintah melalui
kejaksaan yang ingin menyita harta Tommy di luar negri dengan alasan
harta itu adalah uang korupsi bapaknya.

"Pada kasus Marcos (mantan Presiden Filipina Ferdinand Marcos)
misalnya, sudah ada putusan pengadilan di dalam negeri yang memutuskan
Marcos bersalah korupsi. Soeharto mana?" tandas Kaligis.

Dukungan

Upaya Kejagung boleh dicibir Kaligis. Tetapi dukungan mengalir dari
Gedung DPR. Sejumlah politisi mengingatkan pemerintah supaya tetap
menjalankan Ketetapan MPR Nomor XI tahun 1998 tentang Penyelenggara
Negara yang Bersih dan Bebas KKN yang isinya antara lain tentang
pemberantasan korupsi Soeharto.

"Tap MPR itu harus tetap dijalankan karena belum dicabut," kata Ketua
Fraksi Partai Persatuan Pembangunan, Lukman Hakim.

Dia menilai, pengusutan kasus Soeharto sendiri sebenarnya sudah
berusaha dijalankan pemerintah, namun masih terhambat masalah
kesehatan, serta gerak kejaksaan sendiri. "Karena itu, kita berharap
jaksa agung yang baru bisa serius menyelesaikannya," tegasnya.

Anggota Fraksi Partai Golkar, Yuddy Chrisnandi mengemukakan, selama
Tap MPR itu belum dicabut, maka pemerintah wajib melaksanakannya.
"Pemerintah seharusnya menyampaikan agenda dan target penyelesaian
masalah mantan Presiden Soeharto kepada masyarakat," katanya.

Desakan serupa juga disuarakan oleh Sekjen PDI Perjuangan Pramono Anung.

"Tapi harus jujur, persoalan itu tidak bisa hanya ditimpakan
sepenuhnya pada pemerintahan yang ada sekarang," tegasnya.

Menurut Pramono, pengusutan kasus Soeharto, harus menjadi persoalan
bersama seluruh bangsa, jika ingin dituntaskan. "Ini menjadi
pelajaran, bahwa tidak bisa menyerahkan cek kosong pada siapa pun,"
ucapnya.

Pun dengan ahli ilmu hukum tata negara universitas Andalas, Saldi
Isra. "Soeharto harus diadili. Kalau tidak, pemerintah akan sulit
membongkar korupsi di Indonesia, terutama yang melibatkan anak-anak
Soeharto dan kroni-kroninya. Peradilan terhadap Soeharto merupakan
titik pembuka untuk membongkar kasus korupsi anak-anak dan
kroni-kroninya," ujarnya.

Dia menjelaskan, Tap MPR memang tidak lagi sebagai sumber hukum.
Namun, ada Tap MPR yang substansinya masih ada dalam aturan peralihan,
yang harus tetap dilaksanakan oleh pemerintah.

"Saya pikir, waktu untuk Yudhoyono masih panjang untuk menyeret
Soeharto ke pengadilan," ujarnya. dtc/spc


* * *

Kompas, 21 Mei 2007

Gugatan Perdata Soeharto Jadi Prioritas

Jakarta, Kompas - Mantan Jaksa Agung Abdul Rahman Saleh
menegaskan ada tiga pesan yang disampaikannya kepada Jaksa Agung
Hendarman Supandji berkaitan dengan penanganan perkara yang belum
tuntas. Tiga perkara itu adalah perkara pembunuhan aktivis hak asasi
manusia Munir, penuntasan korupsi Bantuan Likuiditas Bank Indonesia,
dan gugatan perdata mantan Presiden Soeharto.

Hal itu disampaikan Abdul Rahman Saleh kepada Kompas, Minggu
(20/5), menanggapi pernyataan anggota Komisi III DPR Benny K Harman
dan Koordinator Advokasi Imparsial Donny Ardiyanto yang menegaskan
Jaksa Agung Hendarman Supandji harus meneruskan kasus pidana mantan
Presiden Soeharto. Bahkan, Donny mengatakan, "Hendarman harus
mengabaikan pesan mantan Jaksa Agung Abdul Rahman Saleh, yaitu
menghentikan penuntutan kasus Soeharto," (Kompas,
19/5).

Menanggapi pernyataan Donny, Abdul Rahman mengatakan, putusan
bahwa mantan Presiden Soeharto tidak bisa diajukan ke pengadilan
adalah putusan pengadilan yang telah mempunyai kekuatan hukum tetap.
Kejaksaan Agung telah membawa kasus pidana mantan Presiden Soeharto
ke pengadilan, tetapi jawaban pengadilan sama. Terakhir, Ketua
Pengadilan Negeri Jakarta Selatan memberikan jawaban: pemeriksaan
perkara Soeharto an sich telah final. Soeharto dapat dihadapkan ke
persidangan apabila sembuh. Namun, tim dokter menyatakan tak dapat
disembuhkan, maka tidak dapat diajukan ke persidangan.

Karena status Soeharto terkatung-katung, Kejaksaan Negeri
Jakarta Selatan menerbitkan surat ketetapan penghentian penuntutan
perkara (SKP3).

"Saya tidak menggunakan model penghentian penyidikan karena
saya yakin ada cukup bukti. Saya juga tidak mendeponir dengan
menggunakan hak oportunitas Jaksa Agung," kata Abdul Rahman.

SKP3 itu kemudian diuji melalui praperadilan. Hakim Pengadilan
Negeri Jakarta Selatan menyatakan SKP3 tidak sah, tetapi Pengadilan
Tinggi Jakarta menyatakan SKP3 sah. "Itu putusan terakhir dan
final," katanya.

Abdul Rahman menegaskan, soal gugatan perdata tinggal menunggu
laporan tim jaksa dari London, Inggris. "Gugatan sudah disusun dan
tinggal dimasukkan. Itu pesan saya pada Hendarman," kata Abdul
Rahman. (BDM)

* * *

Kompas, 19 Mei 2007

Kejaksaan Harus Tetap Limpahkan Perkaranya

Meski Kejaksaan Agung mengeluarkan surat ketetapan penghentian
penuntutan perkara atau SKP3 untuk mantan Presiden Soeharto, bukan
berarti perkara itu berhenti. Bahkan, untuk merealisasikan
kredibilitas hukum, Jaksa Agung Hendarman Supandji harus berani
menghadirkan Soeharto ke pengadilan.

Kepastian hukum atas perkara mantan presiden itu kini penting karena
menjadi simbol penegakan hukum di Indonesia. "Apakah Soeharto
bersalahatau tidak, biar pengadilan yang memutuskannya," kata anggota DPR
Benny K Harman (Fraksi Partai Demokrat, Nusa Tenggara Timur I), Kamis
(17/5) di Jakarta.

Benny menjelaskan, SKP3 kala itu dikeluarkan Kejagung karena kondisi
kesehatan Soeharto. Disebutkan, karena sakit permanen, Soeharto tak
dapat dihadirkan ke pengadilan. "Namun, status Soeharto tak berubah,
masih tetap tersangka. Selain itu, kala itu kan alasannya
kondisional. Jika Soeharto sehat, tentu alasan itu gugur," kata dia lagi.

Bahkan, Benny menandaskan, sehat atau sakit seharusnya Kejagung
berani menghadirkan Soeharto ke pengadilan. "Ini batu ujian bagi
pemerintahan Presiden Susilo Bambang Yudhoyono dan Hendarman Supandji yang dinilai
memiliki integritas tinggi," tutur dia.

Sebelumnya, Koordinator Advokasi Imparsial Donny Ardyanto mengatakan,
Hendarman harus berani mengabaikan pesan mantan Jaksa Agung Abdul
Rahman Saleh saat serah terima jabatan, yaitu menghentikan penuntutan
kasus korupsi yang diduga melibatkan Soeharto. Jaksa Agung harus
bersikap independen dan tegas dalam perkara itu.

"Apalagi selama ini Soeharto terkesan tidak pernah mau hadir dengan
alasan sakit. Itu artinya ia tak menghormati pengadilan, tidak
menghormati hukum," kata Donny.

Menurut Direktur Pelaksana Imparsial Rusdi Marpaung, pengadilan bagi
Soeharto bukan untuk mendiskreditkan mantan penguasa Orde Baru itu.
Sebaliknya, pengadilan justru dipakai untuk membangun penghormatan
bagi Soeharto. (jos)
