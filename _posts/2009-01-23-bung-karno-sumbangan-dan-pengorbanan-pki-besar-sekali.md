---
layout: post
title: 'Bung Karno : “Sumbangan dan pengorbanan PKI besar sekali!”'
date: 2009-01-23
---

Berikut di bawah ini disajikan cuplikan dari sebagian pidato Presiden Sukarno di depan rapat umum Front Nasional di Istora Senayan Jakarta, tanggal 13 Februari 1966.. Pidatonya ini diucapkannya 4 bulan sesudah terjadinya G30S, ketika Angkatan Darat di bawah pimpinan Suharto sudah mulai secara besar-besaran membunuhi, atau menangkapi, atau menyiksa para pemimpin PKI dan tokoh-tokoh berbagai organisasi masa (antara lain : buruh, tani, nelayan, pegawai negeri, wanita, mahasiswa, pelajar, intelektual, seniman) di seluruh Indonesia.

Agaknya, patut dicatat bahwa pidato Bung Karno di depan rapat umum Front Nasional ini diucapkannya ketika golongan militer di bawah pimpinan Suharto-Nasution sudah terang-terangan mulai melakukan “kudeta merangkak” secara bertahap dan juga merongrong atau merusak kewibawaannya.

Cuplikan sebagian pidatonya ini, diambil dari buku “Revolusi Belum Selesai” halaman 422, 423 , 424, dan 425 Buku “Revolusi Belum Selesai” tersebut terdiri dari 2 jilid, dan berisi lebih dari 100 pidato-pidato Bung Karno, yang diucapkannya di berbagai kesempatan sesudah terjadinya G30S sampai pidatonya tentang Nawaksara 10 Januari 1967. Karena sesudah terjadinya G30S, boleh dikatakan bahwa semua media massa (pers, majalah, TV dan radio ) dikuasai atau dikontrol keras Angkatan Darat, maka banyak sekali (atau hampir semua) pidato-pidato Bung Karno di-black out atau diselewengkan atau dimanipulasi., sehingga tidak diketahui oleh umum secara selayaknya.

Isi buku “Revolusi belum selesai “ ini menyajikan berbagai hal penting yang berkaitan dengan fikiran atau pandangan Bung Karno tentang perlunya persatuan revolusioner bangsa Indonesia dalam mencapai masyarakat adil dan makmur atau sosialisme à la Indonesia, menentang imperialisme AS, melawan neo-kolonialisme dalam segala bentuknya, menjaga persatuan bangsa dan kesatuan Republik Indonesia dan juga mengenai G30S. Karena itu, di samping buku “Di bawah Bendera Revolusi” yang juga merupakan kumpulan tulisan dan pidato-pidatonya selama perjuangannya sejak muda, maka buku “Revolusi Belum Selesai” merupakan dokumen sejarah Indonesia yang amat penting untuk dijadikan khasanah bangsa Indonesia.

Mengingat pentingnya berbagai isi buku “Revolusi belum selesai” ini untuk mengenal lebih jauh dan lebih dalam lagi gagasan atau ajaran Bung Karno, maka website http://kontak.club.fr/index.htm akan sesering mungkin menyajikan cuplikan-cuplikannya. Kali ini disajikan pendapat Bung Karno mengenai sumbangan atau jasa-jasa PKI dalam perjuangan bangsa Indonesia untuk mencapai kemerdekaan. Apa yang diungkapkannya secara tegas, jujur, dan terang-terangan tentang PKI, merupakan hal-hal yang patut menjadi renungan kita bersama.

Penghargaan Bung Karno terhadap perjuangan PKI mempunyai bobot penting dan besar sekali. dalam sejarah perjuangan bangsa. Karena, penghargaan ini datang dari seorang bapak besar bangsa, yang dalam sepanjang hidupnya telah membuktikan diri dengan jelas sebagai seorang pemimpin nasionalis, yang juga muslim dan sekaligus marxis. Sangatlah besar artinya, ketika ia mengatakan bahwa sumbangan PKI dalam perjuangan untuk kemerdekaan adalah paling besar dibandingkan dengan partai-partai atau golongan yang mana pun, bahkan termasuk PNI yang telah ia dirikan sendiri.

Apa yang dikatakan Bung Karno ini amat penting untuk diketahui oleh rakyat Indonesia berikut generasi yang akan datang. Karena, selama lebih dari 40 tahun masalah PKI ini dipakai oleh Suharto bersama jenderal-jenderalnya sebagai alat untuk menjatuhkan kekuasaan dan kewibawaan Bung Karno dan menghancurkan kekuatan kiri atau revolusioner yang mendukung politiknya. Racun yang disebarkan oleh rejim militer Orde Baru secara terus-menerus, intensif, luas, dan menyeluruh ini, sampai sekarang masih bisa mempengaruhi fikiran sebagian masyarakat kita.Salah satu buktinya ialah apa yang disiarkan oleh koran Duta Masyarakat tanggal 18 dan 19 Januari 2009. (Harap para pembaca menyimak ucapan-ucapan Asisten Intelijen Kasdam I/Bukit Barisan, Kolonel (Inf) Arminson, dalam tulisan di harian tersebut yang berjudul “Lewat kaos, parpol hingga film).

Cuplikan sebagian pidato Bung Karno mengenai PKI ini menunjukkan betapa besar dan jauhnya gagasan atau idam-idamannya tentang persatuan revolusioner yang dirumuskannya dalam konsep Nasakom. Ini terasa lebih penting dan menonjol sekali, kalau kita ingat bahwa pidatonya ini diucapkannya (dalam bulan Februari 1966) ketika Suharto bersama jenderal-jenderalnya sudah melakukan berbagai langkah besar-besaran untuk menghancurkan PKI.

Cuplikan dari pidato Bung Karno :

(Catatan : teks cuplikan pidato ini diambil oleh penyusun buku “Revolusi belum selesai” dari Arsip Negara, dan disajikan seperti aslinya. Kelihatannya, pidato Bung Karno ini diucapkannya tanpa teks tertulis, seperti halnya banyak pidato-pidatonya yang lain yang juga tanpa teks tertulis).

“Nah ini saudara-saudara, sejak dari saya umur 25 tahun, saya sudah bekerja mati-matian untuk samenbundeling (penggabungan) ) semua revolutionaire krachten (kekuatan revolusioner) buat Indonesia ini. Untuk menggabungkan menjadi satu semua aliran-aliran, golongan-golongan, tenaga-tenaga revolusioner di dalam kalangan bangsa Indonesia. Dan sekarang pun usaha ini masih terus saya jalankan dengan karunia Allah S W T. Saya sebagai Pemimpin Besar Revolusi, sebagai Kepala Negara, sebagai Panglima Tertinggi Angkatan Bersenjata, saya harus berdiri bukan saja di atas semua golongan, tetapi sebagai ku katakan tadi, berikhtiar untuk mempersatuan semua golongan.

“Ya golongan Nas, ya golongan A, ya golongan Kom. Kita punya kemerdekaan sekarang ini, Saudara-saudara, hasil daripada keringat dan darah, ya Nas, ya A, ya Kom. Jangan ada satu golongan berkata, ooh, ini kemerdekaan hanya hasil perjuangan kami Nas saja. Jangan ada satu golongan berkata, ooh, ini kemerdekaan adalah hasil daripada perjuangan-perjuangan kami A saja. Jangan pula ada golongan yang berkata, kemerdekaan ini adalah hasil daripada perjuangan kami, golongan Kom saja.

“Tidak .Sejak aku masih muda belia, Saudara-saudara, aku melihat bahwa golongan-golongan ini semuanya, semuanya membanting tulang, berjuang, bahkan berkorban untuk kemerdekaan Indonesia. Saya sendiri adalah Nas, tapi aku, demi Allah, tidak akan berkata kemerdekaan ini hanya hasil dari pada perjuangan Nas. Aku pun orang agama, bisa dimasukkan dalam golonban A, ya pak Saifuddin Zuhri, saya ini ? Malahan, saya ini oleh dunia Islam internasional diproklamir menjadi Pahlawan Islam dan Kemerdekaan. Tetapi demi Allah, demi Allah, demi Allah SWT, tidak akan saya berkata bahwa perjuangan kita ini, hasil perjuangan kita, kemerdekaan ini adalah hasil perjuangan daripada A saja.

“Demikian pula aku tidak akan mau menutup mata bahwa golongan Kom, masya Allah, Saudara-saudara, urunannya, sumbangannya, bahkan korbannya untuk kemerdekaan bukan main besarnya. Bukan main besarnya !

“Karena itu, kadang-kadang sebagai Kepala Negara saya bisa akui, kalau ada orang berkata, Kom itu tidak ada jasanya dalam perjuangan kemerdekaan, aku telah berkata pula berulang-ulang, malahan di hadapan partai-partai yang lain, di hadapan parpol yang lain, dan aku berkata, barangkali di antara semua parpol-parpol, di antara semua parpol-parpol, ya baik dari Nas maupun dari A tidak ada yang telah begitu besar korbannya untuk kemerdekaan Indonesia daripada golongan Kom ini, katakanlah PKI, Saudara-saudara.

“Saya pernah mengalami. Saya sendiri lho mengalami, Saudara-saudara, mengantar 2000 pemimpin PKI dikirim oleh Belanda ke Boven Digul. Hayo, partai lain mana ada sampai ada 2000 pimpinannya sekaligus diinternir, tidak ada. Saya pernah sendiri mengalami dan melihat dengan mata kepala sendiri, pada satu saat 10 000 pimpinan daripada PKI dimasukkan di dalam penjara. Dan menderita dan meringkuk di dalam penjara yang bertahun-tahun.

“Saya tanya, ya tanya dengan terang-terangan, mana ada parpol lain, bahkan bukan parpolku, aku pemimpin PNI, ya aku dipenjarakan, ya diasingkan, tetapi PNI pun tidak sebesar itu sumbangannya kepada kemerdekaan Indonesia daripada apa yang telah dibuktikan oleh PKI. Ini harus saya katakan dengan tegas.

“Kita harus adil, Saudara-saudara, adil, adil, adil, sekali adil. Aku, aku sendiri menerima surat, kataku beberapa kali di dalam pidato, surat daripada pimpinan PKI yang hendak keesokan harinya digantung mati oleh Belanda, yaitu di Ciamis. Ya, dengan cara rahasia mereka itu, empat orang mengirim surat kepada saya, keesokan harinya akan digantung di Ciamis. Mengirim surat kepada saya bunyinya apa ? Bung Karno, besok pagi kami akan dihukum di tiang penggantungan. Tapi kami akan jalani hukuman itu dengan ikhlas, oleh karena kami berjuang untuk kemerdekaan Indonesia. Kami berpesan kepada Bung Karno, lanjutkan perjuangan kami ini, yaitu perjuangan mengejar kemerdekaan Indonesia.

“Jadi aku melihat 2000 sekaligus ke Boven Digul. Berpuluh ribu sekaligus masuk di dalam penjara. Dan bukan penjara satu dua tahun, tetapi ada yang sampai 20 tahun, Saudara-saudara. Aku pernah mengalami seseorang di Sukamiskin, saya tanya : Bung, hukumanmu berapa? 54 tahun. Lho bagaimana bisa 54 tahun itu ? Menurut pengetahuanku kitab hukum pidana tidak ada menyebutkan lebih daripada 20 tahun. 20 tahun atau seumur hidup atau hukuman mati, itu tertulis di dalam Wetboek van Strafrecht (kitab hukum pidana). Kenapa kok Bung itu 54 tahun? Ya. Pertama kami ini dihukum 20 tahun, kemudian di dalam penjara, kami masih mempropaganda-kan kemerdekaan Indonesia antara kawan-kawan pesakitan, hukuman. Itu konangan, konangan, ketahuan, saya ditangkap, dipukuli, dan si penjaga yang memukuli saya itu saya tikam mati. Sekali lagi aku diseret di muka hakim, dapat tambahan lagi 20 tahun. Menjadi 40 tahun.

“Sesudah saya mendapat vonnis total 40 tahun ini, sudah, saya tidak ada lagi harapan untuk bisa keluar dari penjara. Sudah hilang-hilangan hidup saya di dalam penjara ini, saya tidak akan menaati segala aturan-aturan di dalam penjara. Saya di dalam penjara ini terus memperjuangkan kemerdekaan Indonesia. Pada satu waktu saya ketangkap lagi, oleh karena saya berbuat sebagai yang dulu, saya menikam lagi, tapi ini kali tidak mati, tambah 14 tahun, 20 tambah 20 tambah 14 sama dengan 54 tahhun.

“Ini orang dari Minangkabau, Saudara-saudara. Dia itu tiap pagi subuh-subuh sudah sembahyang. Dan selnya itu dekat saya, saya mendengar dia punya doa kepada Allah SWT ; Ya Allah, ya Robbi, aku akan mati di dalam penjara ini. Tetapi sebagaimana sembahyangku ini, shalatku ini, maka hidup dan matiku adalah untuk Engkau.

“Coba; coba, coba, coba ! Lha kok ada sekarang ini golongan-golongan yang berkata bahwa komunis atau PKI tidak ada jasa di dalam kemerdekaan Indonesia ini.

“Sama sekali tidak benar ! Aku bisa menyaksikan bahwa di antara parpol-parpol malahan mereka itu yang telah berjuang dan berkorban paling besar.”

***

Demikian kutipan sebagian kecil dari amanat Presiden Sukarno di depan rapat umum Front Nasional di Istora Senayan Jakarta, tanggal 13 Februari 1966.

Seperti yang sama-sama kita lihat, amanat tersebut adalah luar biasa! Di dalamnya terkandung pesan (message) yang besar sekali kepada seluruh nasion, dan sekaligus juga peringatan keras kepada semua golongan (terutama kalangan jenderal-jenderal pendukung Suharto) yang bersikap anti-komunis.

Adalah jelas bahwa pernyataan Bung Karno tentang PKI di depan Front Nasional dalam tahun 1966 itu berdasarkan kebenaran sejarah, dan juga bahwa itu lahir dari ketulusan hatinya yang sedalam-dalamnya. Pernyataannya yang demikian itu adalah cermin dari isi atau jiwa perjuangan revolusionernya sejak muda.

Pendapat Bung Karno tentang sumbangan atau pengorbanan PKI untuk kerdekaan Indonesia menunjukkan bahwa ia adalah betul-betul pemersatu rakyat Indonesia, guru besar dan bapak bangsa, yang tidak ada bandingannya di Indonesia.

Catatan tambahan :

Buku “Revolusi belum selesai” terdiri dari dua jilid. Jilid pertama berisi 443 halaman, sedangkan jilid kedua 456 halaman.
Buku ini diterbitkan oleh. Masyarakat Indonesia Sadar Sejarah (MESIASS), Semarang
Penyunting/Editor :Budi Setiyono dan Bonnie Triyana
Kata pengantar :Asvi Warman Adam
E-mail : mesiass 2001@yahoo.com
