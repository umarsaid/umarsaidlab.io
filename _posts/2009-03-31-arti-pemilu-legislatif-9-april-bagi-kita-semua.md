---
layout: post
title: Arti pemilu legislatif 9 April bagi kita semua
date: 2009-03-31
---

Sebentar lagi (tanggal 9 April) akan dilangsungkan Pemilu 2009 guna memilih wakil partai-partai untuk menduduki kursi badan-badan legislatif. Dalam pemilu yang akan datang ini ikut serta 44 partai politik. Menjelang pemilu ini sudah banyak tulisan-tulisan, atau prediksi-prediksi, atau berbagai survei-survei (jajak pendapat) yang sudah beredar.

Seperti biasa, dan di mana-mana di dunia, di antara hasil-hasil survei yang diadakan di Indonesia ini juga ada perbedaan-perbedaan, sehingga kita semua tidak perlu memandangnya sebagai kebenaran yang mutlak bisa dipakai sebagai pegangan.

Dengan pandangan yang seperti itulah disajikan berikut di bawah ini hasil survei Lembaga Survei Nasional (LSN). Informasi yang dipaparkan oleh LSN ini perlu diuji atau dibandingkan dengan hasil survei-survei lainnya, untuk bisa membuat analisa atau pendekatan yang lain, yang relatif lebih cocok dengan kenyataan nantinya. Sebab, akhirnya toh hasil pemilihan tanggal 9 April itulah yang akan merupakan bukti apakah berbagai tulisan, analisa, prediksi , atau survei itu ternyata mengandung kebenaran atau tidak.

Sebagai tanggapan terhadap survei LSM di bawah ini, disajikan komentar, yang berusaha melihat masalah pemilu legislatif 2009 ini dari berbagai segi, yang sebaiknya harap dianggap hanya sebagai bahan pemikiran bersama mengenai persoalan bangsa dan negara yang amat penting ini. Mohon ma’af terlebih dulu, kalau dalam komentar di bawah ini terdapat ungkapan-ungkapan yang terasa terlalu polos atau kurang “senonoh”. Itu semua hanya dengan maksud untuk menekankan atau menggarisbawahi persoalan-persoalan yang dikemukakan.

Tulisan soal hasil survei LSM yang disiarkan INILAH. COM (tanggal 26 Maret 2009) itu antara lain berbunyi sebai berikut (disingkat sedikit) :

Siapa Bisa Kalahkan SBY? (judul asli)

INILAH.COM, Jakarta – Popularitas Susilo Bambang Yudhoyono, oleh lembaga survei, mendekati Pemilu kian naik saja. Hampir semua menempatkan SBY sebagai kandidat pemenang. Hanya satu kandidat calon presiden yang bisa mengalahkannya. Siapa dia?

Survei Lembaga Survei Nasional (LSN) seperti mengkonfirmasikan hasil survei lembaga-lembaga sebelumnya, yaitu menempatkan SBY sebagai pemenang jika pemilu dilaksanakan saat survei. Terungkap, SBY meraih hampir separuh dukungan di republik ini, yakni 45,1%.

Kandidat capres lainnya berada cukup jauh di bawah. Megawati Soekarnoputri hanya memperoleh 15,3%. Prabowo Subianto 10,2%, Sri Sultan Hamengkubuwono X 5,8%, M. Jusuf Kalla 3,3%, Rizal Ramli 3,2%, dan Wiranto 1,5%. Hidayat Nur Wahid? Mantan Presiden Partai Keadilan Sejahtera ini menduduki posisi buncit, hanya disukai 1,2% responden.

Survei LSN berlangsung pada 5-15 Maret 2009 di 33 provinsi di seluruh Indonesia. Jumlah sampel 1.230 responden yang diperoleh melalui teknik pengambilan sampel acak bertingkat (multistage random sampling). Margin of error 2,8 % dan pada tingkat kepercayaan 95%. Pengumpulan data dilakukan melalui teknik wawancara tatap muka dengan bantuan kuesioner.

Menurut Direktur Eksekutif LSN, Umar S Bakry, menjelang pemilu ini elektabilitas SBY melesat cukup fantastis. Isu ekonomi seperti kenaikan harga BBM dan krisis ekonomi global, tak menyurutkan popularitas SBY. “Survei saat ini cukup fantastis hingga 45,1%. Padahal Desember 2008, SBY hanya mendapat 32%,” jelasnya kepada INILAH.COM, Rabu (25/3) di Jakarta.

Menurut dia, capres lainnya belum bisa menandingi popularitas dan elektabilitas SBY. Lawan terdekat SBY seperti Megawati hanya meraih 15,3%. Umar menyatakan sulit bagi Mega untuk mengalahkan SBY. “Karena sejak 2007, elektabilitas Mega hanya stagnan di kisaran 15-17%,” katanya. Jika pun Megawati mampu hingga putaran kedua, takkan mampu mengalahkan SBY.

Sedangkan calon presiden lainnya seperti Rizal Ramli, Prabowo Subianto dan Sultan HB X, Umar menjelaskan sebenarnya ketiga capres tersebut berpotensi menjadi capres alternatif. “Namun sayang, ketiganya memiliki persoalan tentang kendaraan politik,” jelasnya.

Lalu siapa yang berpotensi mengalahkan figur SBY? Umar menegaskan tokoh yang belum terlibat dalam Pemilu 2004 dan belum terlibat dalam kontestasi 2004. Menurut dia, figur tersebut harus memberi inspirasi ke banyak orang. “Figur yang inspiring dan kedatangannya ditunggu-tunggu banyak orang,” jelasnya.

Siapa? Itu dia persoalannya. Rasanya mustahil menemukan figur semacam ‘Ratu Adil’ dalam waktu sesingkat ini. Kalaupun ada dan inspiratif, maka dia akan tenggelam dalam pertarungan. Pasalnya, figur tersebut jelas kalah start. (Kutipan dari INILAH. COM selesai)

Komentar (dilihat dari berbagai segi) :

Membaca hasil survei LSM seperti tersebut di atas, orang bisa saja mengomel atau bergumam (atau nyeletuk) : “Mungkin saja SBY akan bisa terpilih kembali karena tidak ada yang bisa menyainginya, tetapi, apa SBY akan bisa dan juga berani mengadakan terobosan atau gebrakan baru. dari pada yang sudah-sudah ?” Rasanya, tidak! Pemerintahan SBY (tidak peduli ia akan didampingi oleh wakil presiden yang mana saja, atau didukung oleh koalisi dengan partai yang manapun juga) tidak akan bisa melakukan hal-hal besar untuk mengadakan perubahan drastis dan fundamental, karena terikat oleh keterbatasan-keterbatasan yang melilitnya. Jadi tidak akan ada hal-hal besar dan spektakuler yang baru. Yang akan ada hanyalah yang itu itu juga.

Pemerintahan SBY-JK sudah “mendingan” dari pada pemerintahan-pemerintahan Habibi, Abdurahman Wahid, dan Megawati, dalam hal pemberantasan korupsi, meskipun korupsi masih tetap terus merajalela. Tetapi, persoalan-oersoalan parah lainnya, masih banyak sekali yang tidak bisa atau belum bisa ditangani. Di antaranya : masalah pengangguran yang hampir 40 juta orang, jumlah orang miskin, menurut BPS, ada sekitar 52 juta orang (sedangkan angka-angka yang sebenarnya jauh melebihi itu).

Hari kemudian tetap tidak akan cerah

Di samping itu, selama 5 tahun pemerintahan SBY-JK yang telah lewat banyak persoalan-persoalan besar dan parah tidak dapat diatasi, antara lain : persoalan Munir, penanganan korban Lapindo, soal pelanggaran HAM yang sedang ditangani IKOHI, tak berani membongkar kebusukan-kebusukan di kalangan pimpinan militer, masalah keluarga korban 65 yang jumlahnya jutaan orang, masalah rehabilitasi nama dan kehormatan sepenuhnya untuk Bung Karno.

Selama 5 tahun yang lalu, tidak ada atau tidak banyak perubahan drastis dan besar-besaran yang dilakukan oleh pemerintahan SBY-JK. Mungkin saja, Golkar (dan golongan tertentu di kalangan pimpinan militer yang masih setia kepada garis politik Suharto) merupakan ganjalan yang utama bagi SBY untuk bertindak, sehingga SBY kelihatan lemah, ragu-ragu, atau lambat. Lalu, apakah kalau seandainya (sekali lagi,seandainya, terpilih lagi !) akan bisa ada terobosan-terobosan baru, gebrakan-gebrakan yang lain dari pada yang sudah-sudah,? Inilah yang patut sekali disangsikan atau perlu sekali diragukan.

Jadi, apapun hasil pemilu legislatif tanggal 9 April nanti, dengan adanya 44 partai-partai yang bertanding itu, perspektif situasi negara dan bangsa kita tetap tidak akan cerah.Sebab, dari kampanye partai-partai sebelum pemilu 9 April (termasuk pernyataan-pernyataan berbagai tokoh) sudah kelihatan sekali betapa rendahnya kualitas isi dan cara kerja kebanyakan sebagian besar partai-partai tersebut.

Isi banyak kampanye itu umumnya hanya berupa janji-janji kosong, atau uraian yang muluk-muluk tentang perbaikan hidup rakyat banyak, atau kebohongan-kebohongan tentang kesejahteraan bagi masyarakat luas, atau tentang cerahnya hari kemudian bagi banyak orang, atau omong kosong dan bualan lainnya (termasuk kampanye capres Prabowo lewat partainya GERiNDRA, yang menyajikan program-program atau gagasan-gagasan atau “mimpi” yang serba hebat, besar, dan galak, yang sulit dilaksanakan dalam praktek).

Kerusakan moral di kalangan partai-partai
Tidak lama lagi, yaitu tanggal 9 April nanti, akan nampak dengan nyata sekali betapa parahnya dan luasnya kebohongan atau pembodohan oleh partai-partai ini, yang sudah mengeluarkan dana bermilyar-milyar (atau bahkan ada yang puluhan milyar atau ratusan milyar) rupiah untuk mempengaruhi (atau menipu ?) fikiran orang banyak. Hasil 9 April akan menunjukkan bahwa perubahan besar atau fundamental di Indonesia tidak akan bisa dilakukan oleh partai-partai yang ikut dalam pemilu itu nantinya.

Akibat pengkhianatan Suharto beserta para jenderalnya terhadap Bung Karno (beserta golongan kiri – terutama PKI – yang merupakan pendukung utamanya) maka kehidupan kepartaian di Indonesia sudah kehilangan sama sekali jiwa revolusionernya, dan sudah rusak sekali semangat mengabdi dengan sungguh-sungguh kepada kepentingan rakyat dan negara. Jadi, kerusakan mental atau pembusukan moral tidaklah hanya terjadi di kalangan pejabat-pejabat pemerintahan, melainkan juga sudah sangat parah di kalangan partai-partai politik. Banyak partai dewasa ini menjadi alat kotor dan busuk untuk mengejar kursi-kursi dalam badan legislatif, hanya untuk mendapat gaji yang tinggi (sampai Rp 50 juta, dengan segala macam tambahan atau “tunjangan” ) tanpa peduli kepada kepentingan bangsa dan negara.

Patutlah kiranya kita sadari bersama-sama bahwa kerusakan mental dan pembusukan moral di kalangan partai-partai adalah sumber utama dari rusaknya atau bobroknya DPR atau DPRD kita (juga DPD). DPR kita sering mempertontonkan banyak hal-hal yang mengecewakan atau memalukan (ingat, antara lain ; studi banding ke luarnegeri yang tidak perlu, korupsi dan jual beli mengenai pengesahan dan pembuatan undang-undang, dan sering mbolos atau tidur dalam sidang-sidang).

Dengan rendahnya kualitas wakil partai-partai dalam badan-badan legislatif, maka tercermin juga rendahnya kualitas DPR atau DPRD kita. Dengan DPR yang banyak boroknya semcam itu akan sulitlah kiranya diharapkan adanya peran untuk membela sungguh-sungguh kepentingan rakyat banyak, atau mengontrol pemerintah, atau menjadi juru bicara rakyat yang sesungguhnya.

Tugas pemerintah dan DPR akan berat sekal
i

Karena banyaknya masalah-masalah parah yang tidak bisa ditangani oleh pemerintah dan DPR dengan tuntas atau secara baik selama ini, ditambah dengan adanya krisis keuangan dan ekonomi di skala dunia yang juga parah dan berkepanjangan akhir-akhir ini dan di masa depan, maka tugas yang dihadapi oleh DPR dan pemerintahan hasil pemilu 2009 (kalau mau sungguh-sungguh bekerja untuk rakyat) akan berat dan besar sekali !!! Krisis ekonomi di berbagai negeri sekarang sudah terasa dampaknya yang hebat di negeri kita, yang menyebabkan makin memperparah penderitaan sebagian terbesar rakyat kita, karena makin meluasnya kemiskinan, membengkaknya pengangguran, dan banyaknya penutupan perusahaan-perusahaan.

Dalam menghadapi situasi yang akan makin parah bagi sebagian terbesar rakyat kita itu, wajarlah kalau perlawanan rakyat juga akan makin menggelora, yang dilakukan terutama sekali oleh gerakan buruh, tani, pemuda, perempuan, pegawai negeri, mahasiswa, rakyat miskin dan lain-lain golongan dalam masyarakat. Oleh karena krisis ekonomi dan keuangan di negeri kita akan berkepanjangan lama sekali, dan akan menimpa banyak sekali kalangan, maka perlawanan untuk membela nasib sebagian terbesar rakyat kita itu akan merupakan dorongan untuk mempertinggi kesedaran politik kalangan luas di masyarakat atau merevolusionerkan jiwa rakyat umumnya.

Pentingnya gerakan ekstra parlementer
Dalam rangka merevolusionerkan jiwa rakyat ini, sumbangan dan peran gerakan ekstra parlementer yang perkasa dan meluas -- dalam segala cara dan bentuknya -- adalah mutlak perlu sekali. Ini dapat dilakukan oleh semua golongan dalam masyarakat sesuai dengan kebutuhan dan kemampuan masing-masing, dengan tujuan bersama-sama untuk mengadakan perubahan radikal dan fundamental. Gerakan ekstra parlementer yang betul-betul kuat dan menjadi satu dengan rakyat, dan dengan kerjasama dengan kekuatan-kekuatan pro-rakyat dalam parlemen, akan bisa mendorong lahirnya kekuatan revolusioner untuk mengadakan perubahan besar dan fundamental, demi kepentingan sebagian terbesar rakyat kita.

Seperti sudah ditunjukkan oleh pengalaman selama 32 tahun Orde Baru ditambah dengan 10 tahun pasca-Suharto, gerakan ekstra-parlementer untuk menyiapkan situasi revolusioner guna mengadakan perubahan-perubahan besar dan fundamental ini tidak bisa dilahirkan atau tidak mungkin dikembangkan oleh kalangan atau golongan yang pro Orde Baru, atau yang anti ajaran-ajaran Bung Karno. Situasi revolusioner yang bisa mendatangkan perubahan besar dan fundamental hanya bisa diciptakan bersama-sama dengan arahan atau bimbingan ajaran-ajaran revolusioner Bung Karno (ingat , antara lain : “samenbundeling van alle revolutionaire krachten”, Pancasila, Nasakom, Dekon, Berdikari, Tavip, Manipol-Usdek, Revolusi Belum Selesai).

Setelah mengamati praktek berbagai partai yang selama ini sudah ikut pemilu atau sudah ikut memerintah di negeri kita , maka kiranya banyak di antara kita yang yakin bahwa partai-partai itu kebanyakan adalah partai-partai yang tidak patut mendapat kepercayaan rakyat banyak atau tidak layak menjadi wakil rakyat. Dan ketika partai-partai atau DPR (dan DPRD) beserta pemerintah sudah tidak dapat dipercayai lagi, maka rakyat berhak dan terpaksa harus mencari jalan sendiri, melalui berbagai cara dan bentuk extra-parlementer, untuk membela diri dan mendatangkan perubahan. Dengan memegang teguh dan mentrapkan ajaran-ajaran revolusioner Bung Karno, kita semua dapat mempersiapkan kedatangan situasi revolusioner yang memungkinkan perubahan besar di negeri kita.. Jalan lain tidak ada!
