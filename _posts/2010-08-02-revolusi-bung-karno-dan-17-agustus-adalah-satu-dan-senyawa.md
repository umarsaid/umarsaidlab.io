---
layout: post
title: Revolusi, Bung Karno dan 17 Agustus adalah satu dan senyawa
date: 2010-08-02
---

Membikin Rakyat Indonesia kranjingan kepada
masyarakat adil dan makmur




Tidak lama lagi kita semua akan memperingati Hari Kemerdekaan 17 Agustus. Untuk menyambut hari proklamasi kemerdekaan bangsa kita itu, maka di bawah berikut ini disajikan lanjutan tulisan-tulisan yang lalu mengenai  Revolusi, satu gagasan besar  (dan amat bersejarah!)  bagi bangsa Indonesia, yang sudah disampaikan oleh Bung Karno.

Memperingati 17 Agustus dengan mengangkat kembali ajaran-ajaran Bung Karno tentang Revolusi adalah cara baik untuk menyatukan hari yang keramat ini dengan satu-satunya tokoh agung dan yang paling besar jasanya bagi bangsa dan negara Republik Indonesia.

Sebab, pada hakekatnya, sosok Bung Karno adalah pengejawantahan Revolusi Indonesia, yang mencerminkan juga gagasan besar Revolusi bagi ummat manusia, di Asia-Afrika, atau di  bagian dunia lainnya, yang tertindas dalam penghisapan dan pemerasan.

Oleh karena itu, memperingati 17 Agustus tanpa memperingati  kebesaran Bung Karno dalam sejarah bangsa adalah suatu kekurangan, atau suatu kesalahan, bahkan, pada hakekatnya,  suatu pengkhianatan. Memperingati 17 Agustus dengan sengaja melupakan Bung Karno adalah sikap politik yang keliru dan sikap moral yang nista.

Revolusi dan Bung Karno dan 17 Agustus adalah satu dan senyawa serta  tidak bisa dipisahkan. Bung Karno adalah jiwa paling utama dari 17 Agustus, di samping adanya tokoh lainnya, yaitu Bung Hatta.  Seperti yang dirumuskan oleh alm Subadio Sastrosatomo (pendiri dan tokoh PSI) “Bung Karno adalah Indonesia dan Indonesia adalah Bung Karno”.



Revolusi tidak boleh mandeg dan menyeleweng

Dalam pidatonya “Vivere Pericoloso” dalam tahun 1964 Bung Karno dengan pajang lebar, dan berkali-kali, mengatakan bahwa bangsa Indonesia perlu meneruskan Revolusi, yang pada hakekatnya adalah satu proses, satu perjalanan, satu gerak, yang tidak boleh mandeg dan tidak boleh menyeleweng.

Ketika kita semua akan memperingati Hari 17 Agustus dalam tahun 2010 ini, maka kita bisa merasakan atau menyaksikan bahwa Revolusi Rakyat Indonesia yang sudah pernah berkobar begitu hebat dan menggelora begitu dahsyat di bawah pimpinan  Bung Karno bersama-sama rakyat sekarang telah mandeg atau kelihatan mati.

Kita bisa melihat sendiri di sekeliling kita sendiri bahwa tidak sedikit orang yang tidak mengerti lagi apa arti sebenarnya Revolusi, tidak memahami tujuan proklamasi 17 Agustus, tidak mengenal kebesaran Bung Karno bagi bangsa, acuh-tak-acuh  terhadap kesengsaraan orang lain.

Kita mengetahui  bahwa situasi yang demikian itu disebabkan karena Revolusi Rakyat Indonesia, yang telah menjadikan bangsa Indonesia pernah mempunyai sikap revolusioner, patriotik, nasionalis, dan internasionalis sesuai dengan ajaran-ajaran Bung Karno telah dibunuh oleh rejim militer Suharto.

Revolusi Rakyat Indonesia telah dimatikan oleh Orde Baru, dengan lebih dulu menghancurkan kekuatan kiri, dengan membunuh jutaan manusia tidak bersalah (di antaranya terutama golongan PKI) dan memenjarakan ratusan ribu lainnya selama jangka waktu yang lama sekali.

Dengan dilumpuhkannya kekuatan kiri dengan senjata secara luar biasa kejamnya,  kemudian disusul dengan digulingkannya Bung Karno secara khianat oleh Suharto, maka Revolusi Rakyat jadinya ikut mandeg. Sebab, kekuatan kirilah yang menjadi kekuatan utama, menjadi pendorong, dan menjadi sokoguru Revolusi Rakyat Indonesia bersama-sama Bung Karno.

Dengan mandegnya Revolusi rakyat itu  maka tujuan revolusi 17 Agustus telah nyeleweng  atau diselewengkan, sehingga keadaan negara dan bangsa dalam situasi yang serba menyedihkan sekarang ini.



Situasi sekarang berbeda sekali dengan di bawah Bung Karno

Banyak hal yang buruk dan busuk yang kelihatan merajalela sekarang ini  --  antara lain : kerusakan moral yang parah di  kalangan tokoh-tokoh masyarakat, korupsi besar-besaran yang merajalela di seluruh negeri, bermacam-macam kejahatan di banyak kalangan – tidak terdapat di jaman pemerintahan di bawah pimpinan Bung Karno.

Kalau sekarang kita melihat adanya apatisme politik di sebagian besar masyarakat, atau tipisnya perasaan kerakyatan, dan lunturnya semangat kegotongroyongan, atau hilangnya semangat pengabdian kepada rakyat, maka berlainan sekali dengan situasi di bawah pimpinan Bung Karno. Sejarah telah berbicara sendiri, dan dengan jelas pula.

Karena adanya Revolusi di bawah pimpinan Bung Karno maka moral revolusioner  dan semangat mengabdi kepada kepentingan rakyat pernah mendominasi seluruh kehidupan berbangsa dan bernegara. Pada waktu itu, tidak ada orang-orang yang berkelakuan seperti jenderal-jenderal polisi yang berrekening gendut, atau jaksa-jaksa yang terima suap sampai bermilyar-milyar, atau hakim-hakim yang memperjual-belikan hukum, dan pejabat-pejabat lainnya yang ramai-ramai menjadi penjahat.

Sedangkan sekarang kerusakan moral kelihatan dimana-mana, penyalahgunaan  kekuasaan meluas, korupsi merajalela di seluruh negeri, pertentangan  horizontal meletup di banyak tempat, ormas-ormas “agama” dan etnik dan preman bikin onar, rumah-rumah ibadah dirusak atau dibakar.

Suatu waktu akan tiba saatnya di kemudian hari ketika segala kebusukan moral, kebejatan akhlak, dan kesesatan iman yang sekarang merusak tubuh bangsa kita itu akan disapu bersih dan habis-habisan oleh Revolusi Rakyat menurut ajaran-ajaran revolusioner Bung Karno. Hanya Revolusi Rakyat yang dijalankan menurut ajaran-ajaran Bung Karno-lah yang bisa membersihkan segala penyakit dan kebusukan yang disandang bangsa kita sekarang ini.

Revolusi Rakyat yang dijalankan menurut petunjuk ajaran-ajaran revolusioner Bung Karno, -- yang bisa dilengkapi dengan ajaran-ajaran revolusioner lainnya  -- merupakan jalan untuk mewujudkan cita-cita Proklamasi 17 Agustus 45.

Sebagian kecil dari gagasan-gagasan Bung Karno tentang Revolusi Indonesia tercermin dalam beberapa kutipan pidatonya berikut ini.



Revolusi adalah satu perjalanan, satu proses, satu gerak.

Kata Bung Karno ;”Sebab, jangan lupa ; Revolusi kita masih terus berjalan, dan bukan saja berjalan, tetapi harus bertumbuh, dalam arti pengluasan, bertumbuh dalam arti pemekaran konsepsi-konsepsi, sesuai dengan tuntutan jaman, sesuai dengan tuntutan Amanat Penderitaan Rakyat, sesuai dengan tuntutan The Universal Revolution of Man. Karena itulah, maka tiap kali saya berdiri di atas Podium 17 Agustus ini, saya bukan saja berdialoog dengan Rakyat Indonesia yang ber-Revolusi, tetapi juga berdialoog dengan seluruh Ummat Manusia yang juga dalam Revolusi.

Bagaimana jalannya Revolusi kita ini ? Bagaimana maju-mundurnya Revolusi kita ini ? Bagaimana « gatuknya » derap-irama Revolusi kita ini dengan derapmu, hai Umat Manusia di seluruh muka bumi ini ? Dan selalu, dalam memberikan « stock-opname » yang demikian itu , hati saya  berganti-ganti terharu-gembira dan terharu-sedih, berganti-ganti mongkok-senang dan mengkeret-kecewa, ---mongkok-kagum dalam melihat titik-titik- gemilang dalam jalannya Revolusi kita ini, mengkeret-kecewa dan kadang-kadang mengkeret-cemas kalau melihat penyelewengan-penyelewengan yang dapat membahayakan jalannya Revolusi kita itu.

Pendek-kata, saya selalu memberikan balans dari Revolusi kita itu,  -- pasang-surutnya dan pasang-naiknya, dentam-majunya dan geram-deritanya Revolusi kita itu.

Pada tiap 17 Agustus saya mengajak saudara-saudara menoleh ke belakang sejenak. Lihat ! Hai saudara-saudara ! Lihat ! Peristiwa-peristiwa di belakang kita ini, peristiwa-peristiwa di masa yang lampau, merupakan pelajaran bagi kita semua, pelajaran agar jalannya Revolusi dapat dipercepat, pelajaran agar yang pahit-getir tidak diulang lagi. Dan selanjutnya juga selalu saya  lantas mengajak rakyat untuk melihat ke muka : selalu saya lantas memberikan jurusan, memberikan arah, memberikan direction  selanjutnya, dalam menghadapi masalah-masalah yang akan datang.

Pelajaran dari pengalaman yang sudah,dan jurusan untuk yang di muka, dua hal itu adalah penting-maha-penting dalam Revolusi yang sedang berjalan, --- Revolusi yang pada hakekatnya adalah satu perjalanan, satu proses, satu gerak.

Apalagi bagi satu Revolusi yang sedang dikepung seperti Revolusi kita sekarang ini, satu Revolusi yang hendak dihancurkan orang, satu Revolusi yang harus mempertahakan  kepalanya di atas samudera subversi dan intervensi dari fihak imperialis dan kolonialis, -- satu Revolusi  yang harus menyelamatkan badannya dan jiwanya dari serangan-serangan yang  maha-dahsyat dari segala jurusan,  -- dari luar, dari dalam, dari kanan, dari kiri, dari atas dari bawah. (Dikutip dari Dibawah Bendera Revolusi - DBR, halaman    560-561)



Revolusi kita harus bergerak terus, kata Bung Karno

Kata Bung Karno:”Saya tandaskan sekarang sekali lagi : dus.. Revolusi minta tiga syarat-mutlak : romantik, dinamik, dialektik.  Romantik, dinamik, dan dialektik yang bukan saja  bersarang di dada pemimpin, tetapi romantik, dinamik, dialektik yang menggelora di seluruh hatinya Rakyat, -- romanatik, dinamik, dan dialektik yang mengelektrisir sekujur badannya Rakyat dari Sabang sampai Merauke.

Tanpa romantik yang mengelektriksir seluruh Rakyat tu, Revolusi tak akan tahan. Tanpa dinamik yang laksana mengkranjingankan seluruh Rakyat itu, Revolusi akan mandeg di tengah jalan. Tanpa dialektik yang bersambung kepada angan-angan seluruh Rakyat, Rakyat tak akan bersatu dengan raising demands-nya Revolusi, dan Revolusi akan pelan-pelan ambles dalam padang-pasirnya kemasabodohan, seperti kadang-kadang ada sungai ambles–hilang dalam gurun-gurun pasir sebelum ia mencapai samudera lautan.

Karena itu maka kita harus memasukkan romantik, dinamik, dan dialektik Revolusi itu dalam dada kita semua, kita pertumbuhkan, kita gerakkan, kita gemblengkan dalam dada kita semua, sampai ke puncak-puncaknya kemampuan kita, agar Revolusi kita dan Revolusi Ummat Manusia dapat bergerak-terus, menghantam dan membangun terus, mendobrak segala rintangan yang direncanakan dan dipasangkan oleh fihak imperialis dan kolonialis.



Revolusi bukan karena sakt hati pribadi

Dari kutipan yang berikut ini, Bung Karno mengungkapkan pandangannya tentang Revolusi yang hanya didorong oleh impuls (rangsangan atau gerak hati yang timbul tiba-tiba, atau mengikuti dorongan hati)  perseorangan, ambisi dari seorang-orang, atau rasa sakit-hati-pribadi.

Artinya, menurut  Bung Karno Revolusi haruslah bersifat massal, mendapat dukungan rakyat, dan sesuai dengan harapan atau kemauan rakyat, dan bukan hanya karena kemauan  atau ambisi perseorangan.

Kata Bung Karno :”Adakah Revolusi tanpa tiga syarat-mutlak itu tadi ? Ada!  Tetapi Revolusi yang tanpa romantik, dinamik,dialektik massal, Revolusi yang hanya didorong oleh impuls perseorangan, ambisi pribadi dari seorang-orang atau rasa–sakit-hati-pribadi sebagai dinamik dari kekuatan , -- revolusi yang demikian itu hanyalah merupakan “revolusi istana” saja,  -- satu “ palace revolution”, yang sekarang muncul,  besok sudah hilang kembali.

Revolusi yang demikian itulah yang sering ditunggangi oleh kaum imperialis ! Revolusi yang demikian itulah yang sering dibuat oleh kaum imperialis, dengan mengadakan “coup”, pembunuhan pemimpin, dan lain sebagainya.

Juga di Indonesia kaum imperialis kadang-kadang mencoba mengadakan revolusi yang demikian itu, dengan maksud hendak mematikan Revolusi kita! Tetapi kita selalu waspada! Rakyat Indonesia, Alhamdulillah selalu wapada ! Rakyat Indonesia telah mengganyang berkali-kali percobaan-percobaan kaum imperialis itu!. (Kutipan dari DBR halaman 564-565)



Kranjingan untuk selalu sibuk dalam aksi

Kutipan yang di bawah ini dengan jelas sekali menunjukkan bahwa Bung Karno senang sekali mengamati (pada waktu itu, artinya sebelum rejim militer Suharto membunuh Revolusi Indonesia ) bangsa Indonesia mempunyai sikap yang kranjingan (tergila-gila) kepada segala yang berkaitan dengan perjuangan untuk masyarakat adil dan makmur. Bung Karno menginginkan bahwa bangsa Indonesia tidak masa-bodoh saja, dan mengharapkan supaya kalbunya selalu bergelora dan selalu sibuk dalam aksi.

Kata Bung Karno:“Syukur Alhamdullillah ! Demikan itulah memang Bangsa Indonesia ! Bewust! Bewust! Sadar!  Ia tidak boleh masa-bodoh. Ia tidak seperti rumput. Ia selalu “gito-gito lir gabah den interi”. Kalbunya senantiasa bergelora. Fikirannya selalu bergerak. Jiwanya senantiasa “kranjingan”.

Kranjingan seperti ditiup Malaikat! Kranjingan dengan cita-cita. Kranjingan dengan idee. Kranjingan dengan tujuan perjoangan. Kranjingan dengan kemerdekaan. Kranjingan dengan idee masyarakat adil dan makmur. Kranjingan dengan hapusnya “exploitation de l’homme par l’homme”. Kranjingan dengan lenyapnya “exploitation de nation par nation”. Kranjingan dengan benci mati-matian kepada imperialisme dan kolonialisme. Kranjingan dengan hidup berjoang.  Kranjingan, ya kranjingan, maka karena itulah ia selalu sibuk dalam aksi”. (Kutipan dari DBR, halaman 592)



Mengingat itu semua, kiranya jelas bahwa untuk mengatasi keterpurukan dan kerusakan atau pembusukan yang terjadi di banyak bidang di negara kita dewasa ini, perlu diteruskan Revolusi Rakyat yang sudah dijalankan di bawah pimpinan Bung Karno, dengan mentrapkan ajaran-ajaran revolusionernya, sesuai dengan kondisi kongkrit atau perkembangan situasi sekarang.
