---
layout: post
title: Jiwa Agung Bung Karno Masih Terus Hidup Di hati Banyak Orang
date: 2011-06-17
---

Ketika negara dan bangsa kita sedang dalam keadaan yang sangat membusuk, amat bobrok, atau  rusak sekali  dalam banyak bidang kehidupan, yang seperti sama-sama kita saksikan dimana-mana
dewasa ini, maka banyak orang akan memperingati Hari Wafatnya Bung Karno pada tanggal 21 Juni yang akan datang.

Kebejatan moral atau kerusakan akhlak di banyak kalangan,(terutama di kalangan atas), yang sekarang tercermin dengan gamblang dalam merajalelanya korupsi, pelanggaran dan pelecehan hukum, penyalahgunaan kekuasaan dalam lembaga eksekutif, legislatif, dan judikatif, sudah

sedemikian hebatnya, sehingga banyak orang mulai bertanya-tanya apakah negara kita masih bisa diselamatkan dari kehancuran.

Keadaan yang serba semrawut dan kerusakan moral publik yang meluas dewasa ini , yang sudah berlangsung sejak lama, membuktikan bahwa sejak wafatnya Bung Karno dalam tahun 1970, negara dan bangsa kita kehilangan panutan yang berwibawa atau pimpinan  nasional yang mempunyai integritas yang tinggi.

Dengan wafatnya Bung Karno akibat serentetan tindakan yang tidak manusiawi dan perlakuan yang biadab oleh rejim militer Suharto, maka negara dan bangsa Indonesia sejak waktu itu sudah kehilangan arah, dan terjerembab dalam jurang kehancuran dan kebusukan. Kalau direnungkan dalam-dalam maka kelihatan nyatalah  bahwa wafatnya Bung Karno adalah, pada hakekatnya , akibat politik anti-Bung Karno yang dianut pimpinan Angkatan Darat (waktu itu), yang mendapat dukungan dari berbagai golongan reaksioner dalam negeri dan luar negeri

Apakah negara dan bangsa masih bisa diperbaiki ?

Wafatnya Bung Karno merupakan kehilangan besar atau kerugian yang luar biasa hebatnya bagi bangsa, termasuk bagi anak-cucu kita di kemudian hari. Oleh karena itu, dosa-dosa rejim militer Suharto beserta para pendukung setianya adalah juga amat besar, yang sama sekali tidak boleh ( !!!)   dan juga tidak bisa dilupakan oleh berbagai golongan bangsa dan generasi-generasi yang akan datang.

Selalu mengingat kepada pengkhianatan  Suharto dan konco-konconya terhadap Bung Karno, dan mengenang kembali penyiksaan biadab terhadapnya sehingga wafat dalam tahanan adalah suatu kewajiban semua golongan bangsa Indonesia yang mencintai Bung Karno dan menjunjung tinggi-tinggi perasaan keadilan dan kemanusiaan.  Dan mengutuk pengkhianatan Suharto (beserta konco-konconya) adalah perbuatan yang benar, sikap moral yang terpuji, dan tindakan yang sah dan juga mulia.

Memperingati Hari Wafatnya Bung Karno merupakan kesempatan yang baik sekali untuk mengingat-ingat kembali segala tindakan rejim militer Orde Baru selama 32 tahun yang penuh pelanggaran HAM, pembelengguan hak-hak demokrasi rakyat, merajalelanya KKN di semua tingkat dan daerah, dan kerusakan moral dimana-mana. Sebagian besar sekali dari politik yang anti kepentingan rakyat itu masih terus berlangsung sampai dewasa ini.

Merajalelanya korupsi di segala bidang sekarang ini, dan kebejatan moral yang dipertontonkan dengan jelas oleh banyaknya perusakan hukum dan keadilan oleh mafia hukum (ingat, antara lain :  kasus Andi Nurpati, Nunun Nurbaeti, Nazaruddin, istri Nazaruddin hakim Syarifuddin, , Gayus Tambunan, kasus Bank Century, dan banyak kasus lainnya lagi) bertolak belakang sama sekali dengan situasi ketika pemerintah dan negara ada di bawah pimpinan Bung Karno.

Kebejatan moral atau kerusakan akhlak (terutama di kalangan atas) yang sudah membikin busuk kehidupan negara dan bangsa dewasa ini sudah sedemikian parahnya sehingga sulit diramalkan apakah masih bisa diperbaiki, dalam tempo singkat, dan di bawah pengelolaan  para elite yang sekarang memegang kekuasaan..

Indonesia di bawah Bung Karno dikagumi banyak rakyat di dunia

Tidak mngkin !!! Sebab, seperti yang sama-sama kita saksikan selama ini, justru para elit yang seharusnya dapat secara baik memimpin atau mengelola atau mengatur urusan-urusan di bidang eksekutif, legislatif, dan judikatif   itulah yang malahan menjadi sumber kerusakan, kebusukan, dan berbagai penyakit parah bangsa. Dan sejarah bangsa sudah menunjukkan dengan jelas bahwa  kerusakan atau pembusukan yang demikian luas dan demikian besar  itu tidak pernah terjadi di bawah pimpinan Presiden Sukarno.

Kalau kita renungkan dengan dalam-dalam maka nyatalah bahwa digulingkannya Bung Karno oleh pimpinan Angkatan Darat (waktu itu) dan disusul dengan wafatnya dalam keadaan tersiksa sekali adalah peristiwa yang merobah bangsa Indonesia yang terkenal di dunia sebagai bangsa yang mempunyai patriotisme yang tinggi, nasionalisme yang luhur, solidaritas internasional yang besar, menjadi bangsa yang melempem, atau loyo, dalam menghadapi musuh-musuh rakyat dan bangsa.

Kalau Indonesia di bawah pimpinan Bung Karno pernah dikagumi oleh banyak rakyat di dunia berkat digelorakannya  Pancasila dan Bhinneka Tunggal Ika, Konferensi Bandung, Tripanji revolusi, Lima Azimat Revolusi, Nasakom, Berdikari, Manipol, To build the World Anew, Go to Hell With your Aid, Ganefo dan berbagai kegiatan solidaritas Asia-Afrika, maka Indonesia di bawah Suharto terkenal di dunia sebagai pembantai jutaan manusia yang tidak bersalah apa-apa dan banyaknya KKN  dan pelanggaran HAM yang berlangsung lama sekali.

Bung Karno wafat tanpa peninggalan harta-benda yang banyak

Bung Karno wafat dengan tidak  meninggalkan banyak harta-benda, atau kekayaan yang berasal dari korupsi atau sumber-sumber haram lainnya. Ini berlainan sekali dengan Suharto, yang selama hidupnya, dan melalui jaring-jaringan KKN yang luas, telah mengumpulkan   -- secara besar-besaran --  harta haram, juga untuk anak-anaknya.

Bung Karno wafat dengan mewariskan berbagai hal yang besar artinya bagi bangsa atau rakyat Indonesia untuk selama-lamanya.. Di samping Pancasila, dan Bhinneka Tunggal Ika, dan berbagai ajaran-ajarannya yang antara lain terdapat dalam buku « Dibawah Bendera Revolusi » dan « Revolusi Belum Selesai », maka ada sejumlah peninggalan-peninggalannya yang lain, yang juga amat penting. Di antara peninggalan-peninggalan itu adalah Gelora Bung Karno, Monumen Nasional (Monas), mesjid besar Istiqlal, Jembatan Semanggi.

Hal itu semua adalah bertolak belakang dengan wafatnya Suharto, yang tidak meninggalkan hal-hal yang besar dan berharga bagi rakyat serta anak-cucu bangsa, melainkan kerusakan jiwa dan dosa-dosa berat yang berupa harta haram dan banyak pelangggaran HAM dan perusakan demokrasi.
.
Dengan wafatnya Bung Karno, seorang pemimpin bangsa Indonesia yang terbesar selama ini – dan mungkin juga terbesar dalam sejarah bangsa di kemudian hari ! – maka hilang pulalah  tokoh besar rakyat  dalam perjuangan untuk masyarakat adil dan makmur, untuk sosialisme. Rakyat Indonesia kehilangan seorang pemimpin yang dalam hidupnya sudah memperlihatkan sikap gigih dan berani dalam melawan kolonialisme dan neo-kolonialisme atau imperialisme.

Seandainya Bung Karno belum wafat dan melihat situasi negara dan bangsa seperti yang kita saksikan dewasa ini (dan sejak pemerintahan Orde Baru) maka ia akan marah sekali dan berteriak lantang « Bukan negara dan bangsa semacam ini yang saya cita-citakan, dan telah saya perjuangkan dengan seluruh jiwa dan tenaga saya !!! ».

Bung Karno, dalam memimpin negara dan bangsa sebagai presiden selama lebih 20 tahun (sejak 1945 sampai 1966) tidak bergelimang dalam kekayaan yang diperolehnya dari korupsi dan perbuatan-perbuatan haram lainnya. Walaupun ia mempunyai banyak istri, namun tidak terdengar adanya banyak korupsi, seperti yang dilakukan oleh Suharto atau presiden-presiden lainnya.

Indonesia, negaranya para maling berdasi

Bagi mereka yang masih hidup dan pernah menyaksikan keadaan negara dan bangsa sebelum 1965 nyatalah dengan jelas bahwa di bawah pimpinan Bung Karno bangsa dan negara Indonesia tidak pernah mengalami kerusakan akhlak dan kebejatan moral seperti yang terjadi di jaman Orde Baru dan seterusnya sampai sekarang.

Pengalaman selama ini sudah membuktikan  bahwa walaupun sampai dewasa ini sudah banyak sistem yang dicoba untuk dipraktekkan dalam berbagai bidang, dan  banyak komisi-komisi atau pansus dan panja telah dibentuk dimana-mana, namun keadaan masih tetap serba rusak atau serba busuk, disebabkan oleh kebejatan atau kebusukan  moral orang-orang  yang ditugaskan.

Sistem yang bagaimanapun bagusnya sistem atau perundang-undangan yang bagaimanapun « njlimetnya » dan muluk-muluknya, toh masih bisa dilanggar, disalahgunakan, direkayasa oleh orang-orang yang memang bermoral busuk atau bobrok. Hal yang demikian itu pulalah yang sekarang ini kita saksikan sedang terjadi dalam kasus besar Bank Century, kasus mafia hukum, kasus mafia peradilan, kasus rekening gendut Polri, kasus Nazaruddin dan Neneng Sriwahyuni, kasus Nunun Nurbaeti dan Andi Nurpati dan banyak lagi lainnya.

Kita semua tahu bahwa setelah Bung Karno digulingkan secara khianat dan kemudian sengaja dibiarkan (lebih tepatnya : dibikin ) wafat dalam keadaan sangat tersiksa berat sekali, maka negara kita telah menjadi negaranya para maling berbintang, negaranya para perampok berdasi, negaranya para penipu yang suka menghambur-hamburkan sumpah palsu, negaranya para tokoh-tokoh « gadungan » yang mulutnya sampai berbusa-busa berkoar-koar soal pentingnya ayat-ayat suci dan agama, negaranya para koruptor yang justru bicara lantang tentang penegakan hukum dan pemberantasan korupsi (ma’af, sudah cukup itu saja, supaya jangan kebablasan lebih jauh).

Perubahan besar dan fundamental hanya dengan revolusi

Sekarang ini, kerusakan moral atau kebejatan akhlak sudah sedemikian hebatnya dan sebegitu luasnya sehingga pemimpin yang mana pun atau sistem pemerintahan yang bagaimana pun sudah tidak mampu lagi memperbaikinya atau tidak bisa mengobatinya selama belum terjadi perubahan-perubahan yang besar dan fundamentaL.

Namun, adalah jelas sekali bahwa perubahan fundamental di Indonesia tidaklah mungkin dilaksanakan  --  kapan pun juga ! – oleh orang-orang korup dan bermoral bejat sejenis yang sekarang ini memegang kekuasaan  (atau pengaruh besar) di bidang eksekutif, legislatif , judikatif,  dan di bidang ekonomi, termasuk tokoh-tokoh usaha swasta.

Mereka ini pada umumnya terdiri dari orang-orang yang dididik atau dibesarkan dengan kebudayaan rejim militer Suharto yang serba reaksioner, korup, anti rakyat, anti ajaran-ajaran revolusioner Bung Karno.

Karenanya, perubahan besar-besaran dan fundamental di Indonesia hanyalah bisa diadakan dengan jalan dan cara-cara revolusioner, dan bukan dengan jalan dan cara-cara tambal sulam.  Jalan dan cara-cara revolusioner ini bisa dinamakan revolusi. Dan untuk mengadakan  revolusi itu Bung Karno sudah menunjukkan jalan yang harus ditempuh. Ajaran-ajaran revolusioner Bung Karno adalah pada pokoknya, atau sebagian terbesar di antaranya, merupakan senjata atau alat di tangan rakyat untuk mengadakan revolusi.

Dan menurut Bung Karno,  revolusi haruslah  dijalankan terus-menerus, untuk bisa selalu mengadakan penjebolan dan pembangunan, terhadap apa yang sudah lapuk dan busuk untuk diganti dengan yang bersih, segar dan baru. Ketika bangsa dan negara kita sedang dibusukkan atau dirusak oleh berbagai kekuatan korup dan reaksioner dalam negeri dan dijajah oleh neo-liberalisme yang mendominasi sektor-sektor vital ekonomi seperti sekarang ini, maka makin kelihatan jelaslah kebenaran ajaran-ajaran revolusioner Bung Karno.

Untuk menghadapi situasi dalam negeri yang sudah remuk atau bubrah akibat menghebatnya korupsi dan kebejatan moral kalangan elite maka tidak ada jalan lain yang bisa ditempuh oleh bangsa kita kecuali jalan yang sudah ditunjukkan dengan gamblang oleh ajaran-ajaran revolusioner Bung Karno.

Dengan mengenang kembali segala ajaran-ajaran revolusionernya dan juga memberikan penghormatan setinggi-tingginya kepada seluruh perjuangannya untuk kepentingan bangsa dan negara, maka kita peringati Hari Wafat Bung Karno tanggal  21 Juni ini.

Bung Karno telah wafat, sesudah mengalami penyiksaan berat  dan biadab dalam tahanan yang penuh penderitaan. Jasadnya telah ditelan bumi di Blitar, namun jiwa agungnya masih bersemayam di hati nurani banyak orang, dan gema ajaran-ajaran revolusionernya masih terus terdengar, sampai sekarang !!!
