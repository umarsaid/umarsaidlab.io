---
layout: post
title: Generasi muda Wajib Meneruskan Revolusi yang Belum Selesai
date: 2011-08-27
---

Mohon perhatian para pembaca kepada sebuah artikel dalam harian Sinar Harapan tanggal 24 Agustus 2011 yang berjudul « Dari Pemuda Proklamator ke Koruptor », yang ditulis oleh Web Warouw, sebagai bahan renungan pada akhir bulan Ramadan kali ini.



Artikel tersebut adalah sebagai berikut :

« Pemuda macam apakah yang diharapkan bangsa kita? Apakah Anas Urbaningrum, Edi Baskoro Yudhoyono (Ibas), ataukah Sri Mulyani Indrawati? Sebagai anak bangsa, pemuda ditantang untuk bisa menjawab tuntutan zamannya.

Oleh karena itu, penting untuk mengupas peran pemuda dari zaman ke zaman agar bisa jadi pelajaran dan ukuran kepantasan.

Soe Hok Gie dalam bukunya Di bawah Lentera Merah menjelaskan, di masa kolonial Belanda kaum muda punya dua pilihan, mengabdi dalam struktur kekuasaan penjajah atau mengorganisasi diri melawan penjajah. Untuk itu, lahirlah generasi awal pergerakan Indonesia dalam Sarikat Islam pada 1912 yang sebelumnya bernama Serikat Dagang Islam.

Di dalamnya kaum muda menjawab penderitaan rakyat Indonesia dengan berbagai pemogokan dan perlawanan yang dipimpin HOS Tjokroaminoto (30 tahun), Alimin (23), Agus Salim (28), Tan Malaka (16), dan Semaun (13)  

Meski demikian, politik saat itu tidak serta merta hitam putih. Luas dan tingginya perlawanan berupa pemogokan dan demonstrasi membuat Belanda memenuhi tuntutan Indonesia untuk berparlemen.

Pada 1918 sebagian pemuda di antaranya Tjokroaminoto (38), MH Thamrin (24), Agus Salim (34), dan Abdul Muis (35) bergabung dengan Volksraad yang sejatinya bukanlah parlemen, namun sebagai penasihat Gubernur Jenderal Belanda.  

Badan ini dibentuk Belanda dengan tujuan agar dapat mengendalikan radikalisasi di kalangan pemuda. Saat itu, beberapa pemuda juga sudah mulai dibina menjadi intel dan kaki tangan Belanda.

Parlemen ciptaan itu gagal membendung perlawanan. Rakyat terus melawan sampai saat pemberontakan nasional pertama oleh rakyat Indonesia pada November 1926 yang dipimpin Semaun, Dharsono, dan Alimin.

Perjuangan pemuda Indonesia bergelora kembali di masa fasisme Jepang. Kehidupan rakyat semakin berat, mendorong pemuda memilih antara ikut Jepang atau Belanda. Dalam metode perjuangan saat itu, ada yang melakukan agitasi propaganda mengobarkan semangat perlawanan seperti Soekarno (41).  

Ada yang menyusup di Heiho dan PETA, seperti Supriadi (19) dan kawan-kawan. Ada juga yang melancarkan perang gerilya melawan Jepang seperti Amir Sjarifuddin (35), Sutan Sjahrir (33), dan Tan Malaka (47).

Puncak dari perjuangan pemuda dan rakyat melawan Jepang dan kolonialisme Belanda adalah proklamasi kemerdekaan 17 Agustus 1945. Proklamasi dilakukan Soekarno (44) dan Moh Hatta (43).



Menjatuhkan Soekarno

Situasi politik setelah proklamasi kemerdekaan berubah-ubah sampai 1965. Namun, gelora antipenjajahan di dalam Preambule UUD’45 terus membakar sikap anti neokolonialisme dan imperialisme (nekolim).

Kebijakan luar negeri Presiden Soekarno berhasil menggalang Konferensi Asia Afrika (KAA) pada 1955 dan mendorong terbentuknya Gerakan Nonblok yang antineokolonialisme dan imperialisme. Di dalam negeri Soekarno menggalang front nasional Nasakom (Nasional Agama dan Komunis).

Tentu saja musuh-musuh Soekarno di luar negeri merasa terganggu sehingga berkali-kali mencoba menjatuhkan Soekarno. Beberapa persiapan dilakukan, termasuk di kalangan pemuda dan mahasiwa. Pater Beek yang pada saat itu sudah menjalin hubungan dengan TNI mendapat tugas mengumpulkan para pemuda untuk menggalang gerakan anti-Soekarno.

Lewat kudeta militer 1965 Soekarno berhasil dicongkel dan Soeharto merebut kekuasaan yang didukung pemuda dan mahasiswa. Para pemuda tersebut di antaranya adalah Akbar Tanjung (20), Cosmas Batubara (27), Hari Tjan Silalahi (31), Arief Budiman (24), dan Soe Hok Gie (23).

Namun Soeharto yang didukung angkatan 66 itu harus berhadapan dengan gelombang perlawanan pemuda yang bersatu dengan rakyat. Selama 30 tahun Soeharto berkuasa dengan militerisme telah melahirkan perlawanan rakyat yang yang dipelopori pemuda mahasiswa sejak zaman Hariman Siregar (36) pada peristiwa Malari 197

Diikuti perlawanan Mahasiswa yang dipimpin Indro Tjahjono (28) pada 1978. Kemudian Orde Baru Soeharto berhasil diakhiri di masa Budiman Sujadmiko (26) pada 1996-1998.

Politik Uang

Lain lagi di masa Reformasi yang dipenuhi kepentingan partai politik (parpol), setiap pemuda dituntut bisa mendapat simpati pemilihnya. Tujuannya agar bisa menjadi anggota parlemen atau berebutan posisi elite partai. Untuk mencapai tujuan itu tidak mudah, karena membutuhkan modal dana yang tidak sedikit.

Perjuangan pemuda yang tadinya demi kepentingan rakyat telah didominasi politik elite dengan modus mengeruk sebanyak-banyaknya dana untuk memperkaya diri dan partai.  

Saat ini, alat perjuangan pemuda adalah mengumpulkan dukungan dengan politik uang dan dari politik pencitraan. Sasarannya adalah kursi di DPR, kepala daerah, menteri, dan mungkin presiden.

Memang masih ada segelintir pemuda yang percaya bahwa perjuangan harus dilakukan dengan menumbangkan penguasa. Segelintir lagi berjuang secara sukarela melayani rakyat yang hidupnya semakin susah.

Tentu saja buat pemuda seperti Anas Urbaningrum, Ibas, atau Sri Mulyani sudah tidak membutuhkan perjuangan keras seperti para pemuda pendahulunya. Dengan bermodal uang saat ini semua posisi bisa dibeli, termasuk kursi kepresidenan 2014. Yang penting tidak ketahuan seperti Nazaruddin kalau uang tersebut dari hasil korupsi anggaran negara. (Kutipan tulisan Web Warouw selesai)



Komentar:



Walaupun dalam tulisan bagus di atas terdapat hal-hal yang kurang akurat (umpamanya, antara lain  : tahun-tahun), namun isi atau semangat yang menjiwainya adalah menarik sekali untuk diperhatikan oleh kita semua, karena pentingnya.

Dalam tulisan ini telah diangkat betapa besar arti peran perjuangan orang-orang muda seperti yang telah ditunjukkan oleh HOS Tjokroaminoto, Alimin, Agus Salim, Tan Malaka , dan Semaun untuk menggerakkan semangat perlawanan terhadap penjajahan Belanda., semasa didirikan Serikat Islam.

Juga diangkat peran tokoh-tokoh komunis seperti Semaun, Dharsono, Alimin dalam pembrontakan rakyat terhadap pemerintah kolonial Belanda dalam tahun 1926. Semangat  perlawanan para pemuda itu diteruskan selama pendudukan fasisme Jepang, oleh (antara lain) Amir Syarifudin, Sutan Syahrir, Tan Malaka, Supriyadi.

Peran pemuda ini menjadi memuncak ketika menjelang dan sesudah proklamasi 17 Agustus oleh Soekarno-Hatta. Dan, kemudian,  di bawah pimpinan Bung Karno  semangat pemuda pada umumnya masih tetap berkobar-kobar berkat politiknya yang anti-imperialis (terutama AS) , yang tercermin dalam konferensi Bandung, gerakan solidaritas Asia-Afrika, menyokong perjuangan rakyat Palestina dan Vietnam, poros Jakarta-Pnompenh-Hanoi-Peking-Pyongyang, pembebasan Irian Barat.

Sayangnya, peran dan semangat perjuangan para pemuda ini  kemudian merosot atau jauh mundur sekali sejak Bung Karno digulingkan secara khianat oleh Suharto dengan menggerakkan sebagian kalangan muda (antara lain : KAMI, KAPPI, HMI) dalam macam-macam aksi-aksi dan demonstrasi.

Sejak waktu itulah, di Indonesia  muncul kalangan muda yang anti ajaran-ajaran bung Karno, yang anti-revolusi rakyat, yang reaksioner, yang pro-Barat (terutama AS), anti-sosialisme atau anti-komunisme.  Sejak itu pulalah hilang peran kalangan muda Indonesia di berbagai bidang kehidupan bangsa Indonesia. Sejak itulah pembusukan atau degenerasi dan degradasi kalangan muda mulai tampil secara besar-besaran dan kontinyu. (Sampai sekarang !!!).

Dalam jangka lama sekali (lebih dari 32 tahun) justru kalangan muda yang semacam itulah yang kemudian direkrut oleh Orde Baru untuk menduduki birokrasi dan kehidupan politik, sosial, ekonomi di seluruh Indonesia (di samping tokoh-tokoh militer), dan menduduki tempat-tempat penting juga di Golkar atau di partai-partai politik lainnya.

Oleh karena itu, kiranya  kita bisa melihat terjadinya kerusakan atau kebobrokan di banyak bidang kehidupan bernegara dan berbangsa dewasa ini juga dari sudut ini. Bahwa kebejatan moral pimpinan (atau tokoh-tokoh)  di berbagai lembaga dan aparat negara (dan partai-partai politik) dewasa ini adalah produk atau akibat dari dihancurkannya jiwa dan ajaran-ajaran revolusioner Bung Karno oleh pimpinan Angkatan Darat (waktu itu)  beserta golongan-golongan reaksioner lainnya,  dengan kerjasama dan bantuan imperialisme negara-negara Barat.

Ini semua dapat kita sakskan selama puluhan tahun sejak merajalelanya  korupsi dan penyalahgunaan kekuasaan (dan pelangggaran berat HAM)  selama Orde Baru, yang diteruskan oleh kasus BLBI, Bank Century, kasus Gayus Tambunan, dan kasus Nazaruddin-Anas Urbaningrum-Partai Demokrat dewasa ini.

Sekarang kita bisa menyaksikan sama-sama bahwa kejahatan dan dosa-dosa besar yang dilakukan oleh Orde Baru (beserta para pendukungnya)  bukan hanya  penggulingan (sekali lagi : secara khianat !!!) terhadap Bung Karno dan pembunuhan secara massal dan biadab jutaan warganegara --  yang tidak bersalah atau tidak berdosa apa-apa --  , melainkan juga terutama sekali pengrusakan dan pembusukan generasi muda.

Sejak Orde Barunya Suharto generasi muda bangsa kita dididik mulai dari Sekolah Dasar sampai perguruan tinggi dengan cekokan segala macam doktrin yang jauh dari patriotisme kerakyatan,  terlepas dari nasionalisme revolusioner, berlawanan dengan semangat mengabdi kepada bangsa, dan bertentangan dengan jiwa asli Pancasila dan Bhinneka

Sejak itu, generasi muda  – yang sekarang ini banyak yang memperoleh kedudukan penting dalam pemerintahan maupun dalam masyarakat,  pada umumnya tidak mendapat pendidikan politik dan moral sebagai bagian penting dari nation and character building. Kebanyakan di antara mereka hanya menjadi pengejar kedudukan yang serba enak, mendapat gaji yang baik, dan mengumpulkan kekayaan (walaupun dengan cara-cara haram dan melalui korupsi). Mereka tidak peduli lagi kepada penderitaan puluhan juta rakyat miskin yang sekarang masih tetap terdapat di seluruh daerah negara kita.

Gejala semacam ini tercermin dalam hidup dan tingkah laku kalangan  elite bangsa, yang menjadi pembesar-pembesar dan anggota DPR atau DPRD, yang tersangkut dalam banyak korupsi, dan diselidiki oleh KPK dewasa ini.

Mengingat betapa besarnya kerusakan mental dan seriusnya kebobrokan moral di kalangan  elite (yang sebagian terbesar terdiri kalangan muda seusia Nazaruddin, Anas Urbaningrum, dan Ibas) yang sudah membusukkan kehidupan bangsa selama puluhan tahun, maka wajarlah kalau ada orang-orang yang menjadi pesimis apakah mungkin ada perubahan atau perbaikan dalam waktu dekat yang akan datang.

Banyaknya mafia hukum dan peradilan  (di Jakarta dan di seluruh Indonesia), kebejatan di kalangan pimpinan Polri dan Kejaksaan, keruwetan yang terjadi di KPK, tingkah-laku wakil-wakil partai politik di DPR dan DPRD semuanya mengindikasikan bahwa negara dan bangsa kita akan tetap terus mengalami pembusukan yang berjangka panjang.

Apalagi, dengan  pemerintahan sejenis yang dipimpin  SBY sekarang ini, yang menurut surat-surat rahasia Kedutaan Besar AS yang dibocorkan Wikileaks adalah terdiri dari orang-orang yang dekat dengan Amerika, maka sedikit sekali kemungkinan adanya politik negara dan bangsa kita, yang sungguh-sungguh anti neo-liberalisme dan menjunjung tinggi kepentingan rakyat banyak.

 Tetapi, banyaknya ketidakadilan dan luasnya penderitaan orang banyak di Indonesia, dan merajalelanya kejahatan yang berbentuk korupsi, semuanya itu menimbulkan perlawanan, dan menciptakan kemauan bersama untuk terjadinya perubahan menuju perbaikan.

Tanda-tanda ke arah itu sudah dan sedang ditunjukkan oleh berbagai kalangan generasi muda yang anti Orde Baru, dan yang melihat tak ada jalan lain, kecuali meneruskan revolusi yang belum selesai,  menurut ajaran-ajaran revolusioner Bung Karno.
