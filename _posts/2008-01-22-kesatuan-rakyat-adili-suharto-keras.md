---
layout: post
title: Kesatuan Rakyat Adili Suharto (KERAS)
date: 2008-01-22
---

khirnya, gencarnya suara-suara yang minta kepada rakyat untuk memberi ma’af kepada mantan presiden Suharto, dan bertubi-tubinya desakan atau tuntutan kepada pemerintah (antara lain : kepada presiden SBY, Jaksa Agung, Mahkamah Agung dll) untuk menghentikan proses hukum atau memberi ma’af atau ampun kepada Suharto atas segala kesalahan-kesalahannya memimpin Orde Baru selama 32 tahun, telah mendapat reaksi keras dari berbagai kalangan dalam masyarakat. Di antara reaksi masyarakat yang tidak setuju kalau rakyat memberi ma’af kepada Suharto dan menolak pemerintah men-deponir atau menghentikan proses hukum Suharto datang dari 20 organisasi massa (Ormas) yang tergabung dalam KERAS (Kesatuan Rakyat Adili Soeharto).

Menurut berita Antara (19 Januari 2008) 20 ormas yang tergabung dalam KERAS ini telah menggelar aksi dalam bentuk demonstrasi di depan Rumah Sakit Pusat Pertamina untuk menuntut agar mantan presiden Soeharto tetap diadili sesuai atas kejahatan yang telah dilakukannya selama 32 tahun memimpin Orde Baru. Aksi yang dilakukan oleh 20 ormas ini merupakan indikasi yang penting tentang sikap sebagian dari rakyat mengenai kasus Suharto. Berita Antara yang berjudul “20 Ormas Tuntut Soeharto Diadili Secara Tegas” itu berbunyi selengkapnya sebagai berikut :

“ Sebanyak 20 organisasi massa (Ormas) yang tergabung dalam Kesatuan Rakyat Adili Soeharto (KERAS) menuntut agar mantan Presiden Soeharto tetap diadili sesuai atas "kejahatan" yang telah dilakukannya selama 32 tahun memimpin Indonesia.

"Kita tahu bahwa selama 32 tahun, Indonesia berada di bawah kekangan kekuasaan Soeharto dimana rakyat Indonesia selalu menderita dan sengsara," kata koordinator lapangan KERAS, Totok dalam orasinya dalam unjuk rasa yang dilakukan di depan Rumah Sakit Pusat Pertamina (RSPP), tempat Soeharto dirawat, Sabtu.

KERAS menilai, Indonesia sebagai negara yang demokratis harus bersikap tegas untuk mampu mengadili Soeharto sesuai dengan kesalahannya selama memimpin.

Ia mengatakan, Tragedi 65 adalah awal kekuasaan Soeharto yang dibangun dengan darah serta berlanjut pada kekuasaan yang diwarnai berbagai tragedi berdarah, seperti peristiwa Malari, Kedung Ombo, kasus Marsinah, kasus wartawan Udin, kasus Tanjung Priok, kasus Trisakti, kasus Semanggi I dan II, dan lainnya.

"Berbagai kejahatan kemanusiaan tersebut membuktikan bahwa Soeharto adalah jenderal pelanggar HAM dan orang yang paling bertanggung jawab terhadap kematian tiga juta sampai lima juta rakyat yang tidak berdosa," kata Totok.

Kini, tambahnya, setelah 10 tahun reformasi, Soeharto dapat bernapas dengan lega dan tenang. Tidak ada satu pasal pun yang dapat menyeret Soeharto ke dalam jeruji penjara, bahkan tidak ada satu hakim pun yang berani memvonis Soeharto bersalah, dan tidak ada satu jaksa pun yang berani menuntut pertanggungjawaban atas dosa-dosa yang dilakukan.

Totok menambahkan, pemberian SKP3 kepada Soeharto merupakan kebijakan yang mengingkari suara rakyat dan cita-cita reformasi. Karena itu, atas nama apapun, SKP3 harus dibatalkan dan dicabut demi keadilan.

Selain itu Totok menegaskan, sakitnya Soeharto tidak serta merta dapat dijadikan senjata untuk memaafkan Soeharto, karena bagaimanapun rakyat masih ingat kekejaman Soeharto saat memimpin Orde Baru.

Karena itu, demi keadilan dan kesejahteraan rakyat, Soeharto harus tetap diadili.

Aksi tersebut diikuti oleh sejumlah aktivis dari sejumlah Ormas, antara lain Kontras, Lembaga Bantuan Hukum (LBH), Imparsial, dan Gerakan Mahasiswa. Mereka menggunakan mobil bak terbuka yang dilengkapi pengeras suara untuk menggelar orasi.

Wakapolres Jakarta Selatan, AKBP Mulyatno mengatakan berdasarkan kesepakatan, aksi tersebut hanya dapat dilakukan di luar rumah sakit selama 20 menit.

"Bagaimanapun, ini adalah rumah sakit, tempat orang dirawat, sehingga tidak pantas ada demo. Tapi untuk menampung aspirasi para pengunjuk rasa, maka kami berikan waktu 20 menit untuk menyampaikan aspirasi itu," katanya.(Berita Antara selesai).



Kekuasaan politik sekarang masih dikuasai Orba

Menggeloranya suara berbagai golongan dalam masyarakat (termasuk ormas-ormas yang bermacam-macam) adalah amat penting, ketika rakyat sudah tidak bisa - dan tidak boleh !!! - punya harapan terlalu banyak atas peran DPR (atau lembaga-lembaga lainnya) terhadap penyelesaian yang adil dan tuntas mengenai kesalahan dan kejahatan Suharto. Sebab, dengan berduyun-duyunnya banyak “pembesar” dan “tokoh” ke rumah sakit Pertamina untuk menyampaikan simpati dan respek mereka kepada Suharto maka jelas sekali (!) bahwa sebagian besar kekuasaan politik di negeri kita masih dikuasai oleh “kaum elite” yang bersimpati kepada Orde Baru.

Bermacam-macam aksi atau gerakan yang dilakukan oleh berbagai kalangan masyarakat untuk menyuarakan isi hati dan fikiran mereka mengenai kejahatan Suharto sangatlah perlu untuk menunjukkan kepada umum bahwa sikap para “elite” yang masih memihak Suharto adalah sikap yang tidak menguntungkan bagi rakyat banyak, dan karenanya perlu ditentang. Sikap para “elite” yang memihak Suharto adalah berlawanan dengan aspirasi sebagian besar dari rakyat negeri kita yang menginginkan adanya tindakan tegas terhadap pencuri terbesar di dunia dan pembunuh jutaan orang-orang tidak bersalah.

Apa yang dilakukan oleh 20 ormas yang tergabung dalam KERAS (Kesatuan Rakyat Adili Suharto) dengan demonstrasi di dekat rumah sakit Pertamina merupakan aksi yang amat penting, karena didukung oleh macam-macam golongan dalam masyarakat, termasuk dari golongan muda (pemuda dan mahasiswa). Aksi KERAS di Jakarta baru-baru ini akan merupakan dorongan atau inspirasi bagi terbentuknya organisasi semacam KERAS di daerah-daerah, yang akan melakukan aksi-aksi searah atau gerakan serupa selanjutnya di kemudian hari.



Diadilinya Suharto penting untuk menghancurkan sisa-sisa Orba

Berkembangnya berbagai kegiatan di sebanyak mungkin kota atau daerah untuk menyuarakan hati dan fikiran rakyat mengenai perlunya Suharto diadili adalah sangat penting, karena kegiatan ini bisa merupakan pukulan berat langsung ke arah “jantung” Orde Baru. Dari segi ini dapat diartikan bahwa menuntut diadilinya Suharto adalah bagian yang amat penting dari perjuangan bersama untuk menghancurkan sisa-sisa Orde Baru, yang masih menguasai banyak kekuasaan politik negeri kita, walaupun Suharto sudah “turun dari tahtanya”. Menuntut diadilinya Suharto dengan fair adalah tujuan politik yang benar, adil, dan luhur, demi kepentingan bangsa dan negara.

Menuntut diadilinya Suharto sama sekali bukanlah karena balas dendam, atau karena sakit hati, atau sebab cemburu ( karena Suharto bisa mencuri uang begitu banyak), atau hanya karena senang melihat ia menderita. Menuntut diadilinya Suharto adalah ibarat usaha menghilangkan salah satu penyakit bangsa yang parah. Artinya, selama Suharto belum diadili, maka selama itu pula bangsa kita masih akan tetap mengidap penyakit yang parah.

Sebab, kesalahan atau kejahatan Suharto adalah juga kesalahan atau kejahatan rejim militer Orde Baru. Dan, rejim militer ini didukung sepenuhnya oleh para simpatisan Golkar dan kalangan-kalangan lainnya, terutama dari kalangan militer dan sebagian kalangan Islam. Dari segi ini dapatlah kiranya diartikan bahwa mengadili Suharto adalah juga mengadili Golkar dan para simpatisannya. Karenanya, nyata sekalilah bahwa aksi-aksi menuntut diadilinya Suharto adalah aksi politik yang amat besar artinya bagi kehidupan bangsa dan negara. Jadi, bukannya hanya aksi hura-hura semata-mata atau kegiatan yang hanya mengumbar kata-kata kebencian dan kemarahan, yang tidak karuan artinya. (Meskipun benci atau marah kepada Suharto dapatlah kiranya dimengerti atau dibenarkan, terutama bagi para korban Orde Baru, yang kebanyakan sudah menderita selama lebih dari 40 tahun !)



Mengadili Suharto berarti mengadili GOLKAR dll

Mengadili Suharto berarti mengadili Golkar dan pendukung-pendukung Orde Baru lainnya. Karenanya, diadilinya Suharto adalah sesuatu yang tidak menguntungkan kepentingan para pendukungnya dan kroni-kroninya, yang kebanyakan terdiri dari orang-orang yang perlu disangsikan sikap mereka terhadap kepentigan rakyat. Itulah sebabnya, mereka berusaha dengan keras - dan dengan segala cara - untuk mencegah diadilinya Suharto, dengan berbagai alasan dan dalih (sakit permanen, sudah berjasa, usia lanjut, dalih hukum dll dll).

Sakit kerasnya Suharto, yang memancing rasa “simpati dan iba” banyak pendukungnya (ingat : berkat “kampanye” media massa dan televisi yang banyak dikuasai kelompok-kelompok pro-Suharto) telah merupakan kesempatan yang baik bagi mereka untuk “menyelamatkan” Suharto dari penuntutan di depan pengadilan. Karenanya, sebagai reaksinya, kekuatan-kekuatan dalam masyarakat yang anti-Suharto telah bangkit untuk memblejeti kesalahan dan kejahatan Suharto beserta pendukung-pendukungnya, dan membongkar dosa-dosanya.

Dalam usaha membongkar dosa-dosa Suharto ini berbagai golongan masyarakat kelihatan telah muncul sebagai penggugat yang offensif. Dan kelihatanlah, sekarang, bahwa Suharto beserta seluruh kubunya berada dalam posisi defensif. Betapa tidak! Sebab, sudah terlalu banyak kesalahan atau kejahatan yang telah dibikinnnya selama 32 tahun lebih. Kelemahan Suharto (beserta pendukung-pendukung setianya) adalah justru banyaknya kejahatan yang telah dilakukannya. Sampai sekarang masih bisa ditemukan banyak saksi-saksi hidup dari pelanggaran HAM yang dilakukan rejim militer Orde Baru di bawah pimpinan Suharto. Itulah sebabnya, mengapa kelihatan sekali bahwa fihak kubu Suharto (beserta pendukung-pendukungnya) sekarang ini bersikap defensif, dan tidak bisa segalak di masa-masa Orde Baru.



Menuntut diadilinya Suharto adalah benar secara moral

Dalam sejarah bangsa kita, di kemudian hari akan tercatat bahwa menuntut diadilinya Suharto adalah sikap yang benar secara moral dan juga sikap patriotik, karena bertujuan untuk membela kepentingan orang banyak dan demi kebaikan negeri. Oleh karena itu, sudah sepatutnyalah bahwa ormas-ormas yang tergabung dalam KERAS (dan ormas-ormas lainnya yang juga anti-Orde Baru) mempunyai kebanggaan bisa berbuat untuk menuntut diadilinya Suharto.

Sebab, tuntutan untuk mengadili Suharto adalah sesuai dengan isi atau semangat TAP MPR no 11/1999 yang memerintahkan adanya tindakan terhadap para koruptor, termasuk di antaranya Suharto. Tuntutan mengadili Suharto juga merupakan dorongan (atau desakan) kepada pemerintah Indonesia untuk bersikap aktif dan positif dalam melaksanakan program Star Initiative yang dilancarkan PBB dan Bank Dunia untuk dikembalikannya dana-dana yang dicuri oleh para korupor. Tuntutan mengadili Suharto juga merupakan hal yang baik dengan akan diselenggarakannya pertemuan internasional yang diselenggarakan PBB di Bali (tanggal 28 Januari - 1 Februari) tentang pemberantasan korupsi di dunia.

Mengingat hal-hal itu semua, maka jelaslah bahwa kekuatan dalam masyarakat yang tergabung dalam bermacam-macam ormas atau gerakan untuk menuntut diadilinya Suharto menyandang tugas nasional yang penting. Dan tuntutan diadilinya Suharto bukanlah hanya penting sekarang ini, melainkan juga seterusnya di masa datang. Karena persoalan diadilinya Suharto atau tidak adalah masalah yang menyangkut berbagai masalah politik negeri kita

Memang, perjuangan untuk diadilinya Suharto (dalam segala bentuk dan cara) bisa makan waktu panjang, dan melalui jalan yang berliku-liku. Tetapi, demi kebaikan negara dan bangsa kita dewasa ini, dan demi anak-cucu kita di kemudian hari, tugas inilah yang perlu sama-sama kita tuntaskan.
