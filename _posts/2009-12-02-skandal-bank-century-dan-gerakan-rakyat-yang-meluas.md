---
layout: post
title: Skandal Bank Century dan gerakan rakyat yang meluas
date: 2009-12-02
---

Sudah berhari-hari televisi dan pers Indonesia dibanjiri oleh berita atau tulisan-tulisan yang mencerminkan bahwa negeri kita memang sedang dirundung berbagai masalah yang parah, dan rumit, dan penuh misteri atau rekayasa yang berlatar belakang korupsi besar-besaran dalam kasus KPK dan Bank Century. Selain itu  juga banyak diberitakan  cerita menyedihkan tentang hukuman bagi bu Minah yang mencuri 3 kakao, atau pencurian satu semangka (di Kediri) yang diancam hukuman 5 tahun,  dan pemenjaraan  keluarga miskin (di Jawa Tengah) karena mencuri dua kilo kapas.

Banyak orang juga telah dibikin marah atau trenyuh oleh  pemandangan yang memilukan hati di layar televisi tentang banyaknya orang-orang yang berdesak-desakan sampai ada yang terinjak-injak dan jatuh pingsan ketika antri untuk mendapat pembagian daging kurban di Mabes Polri atau di mesjid besar Istiqlal. Semuanya ini  menambah lagi banyaknya indikasi atau bukti bahwa negeri kita memang betul-betul sedang sakit parah akibat kebejatan moral kalangan elitnya

Tetapi, di samping itu ada juga banyak indikasi lainnya, yang menunjukkan bahwa negeri kita sedang bergolak dengan penuh gejolak besar oleh banyaknya aksi-aksi di banyak kota dan daerah, oleh perlawanan dan perjuangan berbagai kalangan di masyarakat luas, untuk mendesak dibongkarnya skandal besar Bank Century dan tuntutan dihukumnya Anggodo dan pejabat-pekjabat tinggi yang korup.

Menyangkut lapisan atas negeri kita

Ini semua adalah perkembangan situasi yang sangat menggembirakan. Sebab, gemuruhnya gerakan yang diadakan secara luas sekarang ini merupakan pertanda bahwa banyak kalangan rakyat tidak rela  negeri ini  dirusak terus  oleh bedebah-bedebah yang terdiri dari pejabat-pejabat tinggi yang tersangkut dengan kasus kriminalisasi KPK atau perampokan Bank Century secara besar-besaran sebanyak Rp 6,7 triliun, dan kejahatan-kejahatan lainnya oleh para mafia hukum, mafia peradilan, mafia kekuasaan, mafia ekonomi, mafia pengacara dan mafia markus.

Hiruk-pikuk kasus kriminalisasi KPK (Bibit-Chandra) mulai kelihatan « mengendor » dengan dihentikannya pengajuan perkara ini ke pengadilan, yang diumumkan hari Selasa tanggal 1 Desember 2009 ini. Sudah tentu, berita tentang dibebaskannya Bibit-Chandra dari tuduhan menerima suap atau memeras Anggoro telah diterima dengan gembira dan kelegaan oleh banyak kalangan.

 Tetapi sebenarnya masih banyak persoalan yang sama sekali belum selesai. Karena, bagaimanapun juga, persoalan KPK (Bibit-Chandra) ada kaitannya dengan skandal raksasa berupa perampokan besar-besaran Bank Century, yang diduga menyangkut pertanggungan jawab banyak pejabat-pejabat tertinggi di negeri kita (antara lain Wakil Presiden Budiono, Menteri Keuangan Sri Mulyani, dan bahkan ada desas-desus bahwa mungkin termasuk juga presiden SBY sendiri).

Seperti yang kita saksikan bersama, persoalan kasus perampokan Bank Century sebesar  Rp 6,7 triliun sekarang ini sudah menjadi heboh besar di banyak kalangan masyarakat luas, dan juga di lapisan atas bidang eksekutif, legislatif, yudikatif negeri kita.  Bisalah kiranya dikatakan bahwa heboh besar ini sudah menggoncang negeri kita, lebih hebat dari pada banyak goncangan-goncangan sebelumnya, walaupun tidak sehebat peristiwa G30S yang memungkinkan jenderal Suharto akhirnya mengkhianati dan meggulingkan Bung Karno, atau peristiwa 1998 yang menyebabkan turunnnya Suharto dari kekuasaannya.

Munculnya  hak angket DPR

Hebatnya heboh sekitar Bank Century ini, yang sudah menyulut kegeraman atau  mengobarkan kemarahan kalangan muda di seluruh negeri, dan  menimbulkan kemuakan di kalangan intelektual, serta menimbulkan berbagai reaksi di kalangan  partai-partai politik, menjadi lebih heboh lagi di DPR dengan munculnya soal hak angket DPR untuk menyelidiki kasus Bank Century, yang mulai dibicarakan tanggal 1 Desember ini.

Tanpa menaruh harapan (atau ilusi) yang terlalu besar terhadap keberhasilan DPR dalam menjalankan hak angket mengenai Bank Century -- berdasarkan kegagalan-kegagalan yang sudah dihadapi berkali-kali oleh DPR di masa-masa yang lalu – kita perlu bersama-sama seluruh kekuatan demokratis di negeri ini untuk mendorong, dan mengawasi atau mengawal supaya upaya-upaya (dari fihak manapun juga !)  untuk  menggagalkan hak angket ini, atau memacetkannya, dengan berbagai rekayasa atau manuvre, tidak akan berhasil.

Kita sudah bisa meramalkan bahwa masalah hak angket DPR soal Bank Century akan makan waktu berbulan-bulan (paling sedikit 3 bulan) untuk diselesaikan, dan itu pun kalau berjalan mulus dan tanpa halangan atau sabotase. Sebab, walaupun ada berita-berita bahwa usul hak angket ini sekarang (sebelum tanggal 1 Desember) sudah disetujui oleh semua partai di DPR, tetapi masih bisa saja terjadi surprise-surprise yang baru. Maklum, persoalan Bank Century ini menyangkut persoalan penggelapan atau perampokan dana yang besar sekali (Rp 6,7 triliun), dan melibatkan banyak orang-orang penting di berbagai bidang.

Karena adanya kemungkinan-kemungkinan bahwa pembongkaran kasus Bank Century ini akan dihalang-halangi atau dipersulit oleh oknum-oknum korup yang bersekongkol dengan  mafia hukum, mafia peradilan, mafia kekuasaan, mafia ekonomi, maka aksi-aksi massa atau gerakan rakyat untuk memberantas korupsi yang sudah marak dimana-mana akhir-akhir ini perlu didukung bersama-sama atau didorong terus supaya lebih berkembang lebih luas dan lebih menggelora lagi.

Kesedaran politik rakyat makin meninggi

Adalah gejala-gejala yang sangat menggembirakan dengan adanya aksi-aksi yang diadakan oleh KOMPAK (Komisi Masyarakat Sipil Antikorupsi) di Bunderan Hotel Indonesia, dan aksi berkemah dan mogok makan oleh kalangan mahasiswa/pemuda di pelataran gedung KPK selama hampir sebulan, serta demo-demo yang digelar di Bandung, Jogya, Solo, Makassar, Padang, Medan, Palangkaraya, dan berbagai  kota lainnya.

Televisi sering menyiarkan tayangan aksi-aksi kalangan muda ibukota di depan Istana, gedung DPR, gedung PPATK dll. Yang juga merupakan perkembangan yang penting dan menarik adalah  terjunnya HMI dan PMII dalam berbagai aksi ini, dan pernyataan Din Syamsudin, Syafii Maarif  dan Amien Rais (tokoh-tokoh Muhammadiah) yang mendukung aksi-aksi mengenai Bank Century . Semua aksi atau berbagai kegiatan masyarakat luas ini merupakan gerakan extra-parlementer yang sangat penting yang akan ikut mengawasi jalannya hak angket DPR.

Patut diamati bersama bahwa berbagai siaran televisi (terutama Metro TV dan TV One) tiap hari menyajikan berita, reportase, perdebatan, interview tentang persoalan kriminalisasi KPK (Biibit-Chandra) dan Bank Century. Itu semua merupakan pendidikan politik yang sangat penting dan besar tiap harinya bagi puluhan juta orang di seluruh Indonesia. Dari banyaknya partisipasi para pemirsa televisi dalam berbagai siaran mengenai masalah-masalah politik tercerminlah ukuran betapa tingginya kesedaran politik banyak pemirsa dewasa ini. Ini merupakan perkembangan penting yang sangat menggembirakan, yang menunjukkan bahwa banyak orang sudah tidak seperti di jaman Orde Baru lagi, dan berani mengemukakan fikiran mereka secara bebas dan berani.

Dengan mengamati berkembangnya gerakan berbagai kalangan masyarakat sekitar heboh besar tentang kasus kriminalisasi KPK dan kasus perampokan Bank Century, maka nyatalah bahwa situasi sekarang ini sudah makin tidak menguntungkan lagi bagi sisa-sisa kekuatan politik yang pro-Orde Barunya Suharto. Dengan kalimat lain, bolehlah kiranya dikatakan bahwa perkembangan persoalan kriminalisasi KPK dan perampokan Bank Century menunjukkan bahwa « angin tidak menguntungkan lagi» kubu golongan pendukung pola berfikir Suharto lagi.

Jalan Orde Baru adalah jalan buntu

Sebab, makin banyak orang yang melihat bahwa kekisruhan di berbagai bidang yang bersumber dari kerusakan moral yang sama-sama kita saksikan  sekarang ini, adalah akibat ulah dari orang-orang atau oknum-oknum yang pada dasarnya (dan pada umumnya)  menganut politik atau pola berfikir yang dianut oleh para pendukung Orde Baru, yang anti rakyat, yang anti Bung Karno atau anti kiri. Di antara mereka ini masih banyak yang terus menganggap Suharto adalah pemimpin yang patut dihormati, dan yang juga masih terus anti Bung Karno dan anti ajaran-ajaran revolusionernya.

Kiranya, patutlah kita amati bahwa dengan adanya korupsi besar-besaran dan kerusakan moral  kalangan elite yang meluas, yang hanya sebagian kecilnya saja telah dan sedang dipertontonkan oleh heboh Polri-Kejaksaan-KPK dan oleh kasus Bank Century, maka berbagai kalangan mulai mempertanyakan kemampuan dan keabsahan pemerintahan SBY-Budiono untuk bisa membawa negeri kita ini ke arah perbaikan fundamental yang betul-betul mementingkan kepentingan rakyat banyak.

Sebab, politik yang dianut oleh pemerintahan SBY-Budiono pada dasarnya adalah pro-neoliberal dan anti-rakyat yang  terlalu menguntungkan kaum koruptor atau elite yang tidak bermoral. Kalau politik yang demikian ini terus ditempuh pemerintahan SBY-Budiono maka merupakan jalan sesat yang akan mencelakakan negara dan  bangsa. Dan perkembangan situasi di Indonesia di kemudian hari akan membuktikan bahwa jalan yang ditempuh dengan arah yang demikian itu adalah jalan buntu bagi cita-cita masyarakat adil dan makmur, seperti buntunya jalan yang ditempuh Orde Baru selama puluhan tahun.

Kekosongan pimpinan nasional yang seperti Bung Karno



Dalam situasi yang demikian ini, makin kelihatan jelas  bagi banyak orang bahwa bangsa kita memerlukan adanya pimpinan yang bisa membawa negeri kita ke arah yang benar-benar pro rakyat banyak, yang anti-neoliberalisme, yang bisa mendorong rakyat seluruh negeri untuk bersama-sama berjuang menciptakan masyarakat adil dan makmur, seperti yang dicita-citakan oleh Bung Karno beserta para pendukungnya.

Kekosongan adanya pimpinan nasional yang betul-betul pro-rakyat ini tidak hanya terasa sekarang saja, melainkan sejak digulingkannya  -- secara khianat !!! -- presiden Sukarno oleh jenderal Suharto. Jelas sekalilah bagi banyak kalangan dan golongan di Indonesia bahwa tidak ada satu pun tokoh Indonesia yang bisa menjadi pemimpin yang kewibawaan politik dan moralnya seagung Bung Karno.
Bung Karno merupakan sumber inspirasi besar bagi semua orang yang mau sungguh-sungguh berjuang untuk kepentingan rakyat banyak. Bung Karno adalah satu-satunya pemimpin bangsa yang telah menjadi pedoman moral (moral guidance) bagi banyak kalangan dan golongan. Bung Karno adalah tokoh agung satu-satunya yang telah menciptakan pedoman politik dan pedoman perjuangan yang bisa mempersatukan seluruh bangsa (ingat, antara lain : Pancasila, Bhinneka Tunggal Ika, Mesjid Istiqlal, Monas, Berdikari, Di bawah Bendera Revolusi, Revolusi Belum Selesai, dll dll dll).

Mengingat itu semuanya, kiranya kita bisa melihat bahwa persoalan besar mengenai KPK dan Bank Century telah membuka mata dan fikiran banyak orang bahwa rakyat atau bangsa kita sudah harus membuang  jauh-jauh sekali ilusi terhadap  sistem politik yang selama ini sudah ditrapkan sejak jaman Orde Baru dan yang juga terbukti sekarang gagal dalam banyak bidang.

Artinya, kita bisa juga melihat bahwa kebejatan moral atau kerusakan akhlak yang dipertontonkan sejak lama sampai sekarang (dengan kasus KPK dan Bank Century) ini merupakan cambuk bagi seluruh kekuatan demokratis dalam masyarakat – tidak peduli dari golongan atau aliran politik yang mana pun juga ! -- untuk menyokong berkembangnya gerakan ekstra-parlementer dalam membela kepentingan rakyat, terutama rakyat miskin. Gerakan extra-parlementer yang luas dan kuat adalah senjata sekaligus pelindung bagi perjuangan rakyat, ketika dari DPR, dari DPD, dari pemerintah, dan dari partai-partai politik, sudah tidak bisa diharapkan lagi adanya tindakan-tindakan yang sungguh-sungguh memperjuangkan kepentingan rakyat banyak.

Gerakan extra-parlementer untuk perubahan besar

Gerakan extra-parlementer yang dewasa ini kelihatan mulai muncul dimana-mana adalah investasi penting untuk munculnya di kemudian hari kekuatan politik yang bisa mendorong terjadinya perubahan-perubahan fundamental bagi kepentingan rakyat banyak di negeri kita yang berpenduduk lebih dari 230 juta ini. Perubahan besar dan fundamental tidak bisa lagi diharapkan, atau tidak boleh terus-menerus digantungkan, dari kekuatan-kekuatan politik yang selama 32 tahun (dan juga sesudahnya) sudah terbukti membikin rusaknya bangsa dan negara kita.

Dan, ikut sertanya secara aktif dan secara besar-besaran generasi muda kita dalam berbagai macam gerakan extra parlementer di seluruh negeri dewasa ini  menimbulkan harapan bahwa hari kemudian  negara dan bangsa kita akan ada di tangan orang-orang yang tidak bermental korup, atau berakhlak bejat atau bermoral busuk, seperti yang kita saksikan sekarang di kalangan DPR dan di kalangan aparat negara atau badan-badan pemerintahan kita, termasuk di kalangan pemuka-pemuka masyarakat.

Berbagai macam aksi dan kegiatan yang dilancarkan oleh kalangan muda di seluruh negeri dewasa ini merupakan darah segar bagi kehidupan bangsa kita, yang sudah terlalu lama diracuni atau dibikin busuk oleh Orde Baru beserta para pendukung setianya. Gerakan yang meluas oleh kalangan muda dewasa ini merupakan pertanda penting yang menunjukkan adanya perpecahan antara generasi muda dengan partai-partai politik, dan juga mencerminkan ketidakpercayaan mereka terhadap pemerintahan SBY. Kelihatannya, generasi  muda kita mulai melihat lebih jelas bahwa jalan yang harus mereka tempuh untuk menyongsong hari kemudian bangsa bukanlah jalan buntu (jalan mati) yang sudah ditempuh oleh Suharto beserta pendukung-pendukungnya,

Dan, rupanya, kasus skandal raksasa Bank Century (dan heboh soal KPK) telah mempertinggi kesadaran politik tidak saja kalangan generasi muda kita, melainkan juga sebagian besar rakyat kita.
Sungguh, ini semua adalah perkembangan situasi yang menggembirakan kita semua !
