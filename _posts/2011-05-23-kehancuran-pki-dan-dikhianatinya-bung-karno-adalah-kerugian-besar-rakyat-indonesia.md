---
layout: post
title: Kehancuran PKI dan Dikhianatinya Bung Karno Adalah Kerugian Besar Rakyat Indonesia
date: 2011-05-23
---

Mungkin ada orang-orang yang setelah membaca judul tulisan kali ini geleng-geleng kepala dan bertanya-tanya mengapakah masih ada saja sekarang ini ( !)  orang yang menulis tentang ulangtahun PKI 23 Mei dan menyatakan juga bahwa  kehancuran PKI dan pengkhianatan terhadap Bung Karno adalah kerugian yang besar sekali bagi rakyat Indonesia. Bukankah sampai sekarang ini masih berlaku TAP (Ketetapan)  MPRS /66 nomor 25, yang melarang kegiatan PKI dan disebarluaskannya marxisme  dan leninisme atau komunisme oleh rejim militer Suharto ?

Memang ! Tulisan ini dibuat dengan berbagai maksud atau tujuan. Antara lain untuk menunjukkan bahwa TAP MPRS/66 nomor 25 itu, dalam kenyataannya  sehari-hari,  sekarang ini sebenarnya sudah kedaluwarsa, tidak ada gunanya sama sekali bagi dan negara kita, karena sudah tidak diindahkan atau tidak ditakuti seperti halnya di masa pemerintahan Orde Baru.

Sudah sama-sama kita ketahui, bahwa (resminya saja)  PKI memang sudah tidak ada lagi sebagai organisasi terbuka. Tetapi siapakah bisa mencegah atau melarang jika ada kalangan atau golongan di Indonesia yang melakukan berbagai kegiatan yang jiwanya, semangatnya, atau cita-citanya sebenarnya mengandung marxisme, komunisme atau sosialisme, walaupun tidak terang-terangan memakai nama PKI atau komunis ?

« Generasi baru PKI » sudah bangkit lagi

Kiranya sudah bisalah diduga, atau diperkirakan,  bahwa sejak lama sebenarnya sudah ada banyak orang dari berbagai kalangan di Indonesia yang menempuh jalan  ini dalam macam-macam bentuk dan cara. Ini dapat dirasakan di sana-sini, atau « dicium baunya » dalam berbagai hal dan peristiwa, Sehingga kaum reaksioner dari berbagai kalangan menyuarakan  « Awas PKI gaya baru », atau « Generasi baru PKI sudah bangkit ». Dan ini adalah hal yang wajar,

Sebab, seperti yang sudah ditunjukkan oleh sejarah berbagai bangsa di dunia, lahirnya marxisme atau komunisme adalah karena adanya penindasan, penghisapan, ketidak adilan yang dilakukan satu golongan masyarakat (atau kelas) atas golongan (kelas)  lainnya. Lahirnya marxisme atau komunisme di dunia adalah untuk memberikan senjata atau alat bagi mereka yang tertindas, guna mengadakan perlawanan.

Demikian jugalah halnya  di Indonesia, baik yang terjadi disekitar tahun 1920  ketika PKI muncul secara terbuka untuk pertama kalinya, maupun sekarang dan juga di masa datang. Sebab ;  selama ada penindasan, penghisapan dan ketidak-adilan terhadap rakyat banyak di Indonesia, selama itu pula marxisme atau komunisme, yang dimanifestasikan dengan nama PKI  (atau dengan nama-nama yang lain) dan dalam macam-macam cara dan bentuk  akan tetap ada !  

Itulah sebabnya, mengapa meskipun Suharto beserta konco-konconya  (di dalam negeri dan luar negeri ) sudah berusaha menghancur-luluhkan PKI dan membunuhi, menyiksa, mempersekusi dan menyengsarakan jutaan anggota, dan  simpatisan atau pendukung PKI dengan cara-cara yang luar biasa biadabnya, namun jiwa, semangat, dan cita-citanya masih tetap dipelihara di sana-sini oleh banyak orang. Sampai sekarang

Situasi bangsa membutuhkan senjata untuk perlawanan

Hal yang begini inilah yang  sulit dicegah oleh TAP MPRS/66 nomor 25, atau oleh simpatisan rejim militer Suharto, atau oleh pendukung Golkar, atau oleh FPI dan macam-macam kalangan Islam sejenisnya. Sebab, situasi negara dan bangsa yang menimbulkan penindasan, penghisapan dan ketidakadilan bagi rakyat banyak di Indonesia  justru membutuhkan adanya perlawanan dan pembelaan, untuk terjadinya  perubahan-perubahan dan perbaikan. Dan di antara berbagai  kekuatan dalam masyarakat yang bersedia untuk berjuang mengadakan perlawanan terhadap penindasan dan ketidakadilan ini adalah golongan kiri yang dipelopori oleh PKI.

Sikap perlawanan untuk menentang penindasan, penghisapan dan ketidak-adilan yang demikian inilah yang telah ditunjukkan oleh orang-orang komunis anggota dan simpatisan PKI, sejak lahirnya di tahun 1920. PKI dengan anggota dan simpatisannya telah merupakan kekuatan utama dalam melawan kolonialisme Belanda. dengan adanya pembrontakan di Jawa  (1926) dan Samatera  (Silungkang, tahun 1927), sehingga belasan ribu orang ditangkapi secara besar-besaran , dan ribuan di antaranya dibuang ke Digul.

Setelah pembrontakan melawan kolonialisme Belanda ini ditumpas, maka PKI dinyatakan dilarang oleh pemerintahan kolonial dan terpaksa bergerak di bawah tanah. Perjuangan di bawah tanah ini diteruskan selama pendudukan militerisme fasis Jepang, dan  baru muncul lagi secara terbuka sejak proklamasi 17 Agustus 1945

Sesudah proklamasi kemerdekaan, perjuangan PKI bersama golongan-golongan lainnya diteruskan melawan  kolonialisme Belanda yang ingin kembali menjajah Indonesia, dan juga melawan imperialisme negara-negara Barat yang dikepalai Amerika Serikat setelah selesainya Perang Dunia ke-II. Hanya beberapa tahun saja setelah terjadinya  peristiwa Madiun dalam tahun 1948 yang menggegerkan itu PKI dapat bangkit kembali untuk meneruskan perjuangannya.

PKI sudah sejak lama diincer oleh kekuatan reaksioner

Sejarah bangsa kita menunjukkan bahwa  perjuangan PKI ternyata mendapat dukungan dari rakyat banyak. Hal yang demikian ini  kelihatan dari  kenyataan bahwa dari jumlah anggota dan simpatisan  yang hanya beberapa puluhan ribu orang saja dalam tahun 1950, menjadi ratusan ribu dalam beberapa tahun saja. Puncak dukungan atau simpati rakyat banyak ini kemudian termanifestasikan dalam hasil pemilu demokratis yang diselenggarakan secara baik sekali dalam tahun 1955.

Membesarnya PKI sejak 1950 sudah menarik perhatian kekuatan imperialis Barat (terutama AS), yang mulai mengadakan hubungan dengan tokoh-tokoh pimpinan Angkatan Darat dengan kedok « training » (latihan militer) dll. Melalui berbagai saluran dalam negeri, dan dengan menggunakan persoalan-persoalan yang timbul waktu itu, kalangan Barat telah mendukung (secara terang-terangan dan juga sembunyi-sembunyi) gerakan-gerakan separatis yang dilancarkan oleh sebagian dari Angkatan Darat waktu itu di berbagai daerah.

Sejarah telah mencatat adanya Peristiwa Tiga Selatan (Sumatera Selatan, Kalimantan Selatan dan Sulawesi Selatan), dan juga lahirnya Dewan Garuda, Dewan Banteng, Dewan Gajah, Dewan Manguni, Permesta, yang kemudian memuncak dengan diproklamasikannya PRRI di Bukittinggi pada tanggal 15 Februari tahun 1958. Lahirnya gerakan-gerakan daerah yang dicetuskan oleh unsur-unsur Angkatan Darat ini sebenarnya, atau pada hakekatnya, adalah gerakan anti Bung Karno dan anti-PKI.

Dalam berbagai peristiwa yang ditimbulkan oleh sejumlah pimpinan Angkatan Darat di berbagai daerah waktu itu PKI (sejak 1953) sudah selalu dijadikan sasaran penangkapan dan pembunuhan. Pembunuhan besar-besaran adalah  yang dilakukan oleh   PRRI di kamp maut Situjuh (1958). Baik PRRI maupun Permesta telah mendapat bantuan secara rahasia dari Barat, terutama oleh Amerika Serikat (ingat peristiwa pendropan senjata di Pakanbaru dan penembakan pilot CIA, Allen Pope, di Morotai dalam tahun 1958)

Kekuatiran terhadap PKI menanjak sesudah pemilu 1955

Kekuatiran imperialisme Barat (terutama AS) terhadap makin membesarnya PKI menjadi lebih-lebih lagi dengan juga makin luasnya  dukungan rakyat terhadap Bung Karno. Dengan diselenggarakannya Konferensi Bandung dalam tahun 1955 popularitas Bung Karno menanjak sekali, bukan saja di dalam negeri, bahkan juga di luar negeri, sebagai pejuang anti imperialisme  dan anti-kolonialisme.

 Di luar dugaan banyak orang (termasuk di luar negeri) dalam pemilu 1955 itu PKI mendapat suara yang banyak sekali, dan menduduki nomor ke 4 (16,34 % suara, 6 juta lebih pemilih), sesudah PNI (22,32% suara, 8 juta pemilih), Masyumi 20,92 % suara, 7,9 juta pemilih).Nahdatul Ulama (18,47 % suara, 6,9 juta pemilih)

Sejak itu, dukungan kepada PKI makin membesar, dan menjadi kekuatan politik dan sosial yang makin membikin kuatir kekuatan reaksioner dalam negeri (Angkatan Darat, Masyumi, PSI dan berbagai kalangan anti Bung Karno dan anti-PKI lainnya) dan juga di luar negeri. Kemudian, karena AS makin terlibat dengan perang di Indo-Cina, maka kedekatan sikap politik PKI dengan politik anti-imperialismenya Bung Karno menjadi bahaya yang makin besar bagi kepentingan AS.

Kekuatiran fihak Barat (terutama AS) terhadap sikap Bung Karno dan PKI  lebih membesar lagi disebabkan karena makin besarnya persekutuan (atau persahabatan) antara Indonesia yang diwakili Bung Karno (dengan dukungan PKI) dan RRT, Vietnam, Kamboja, Korea Utara waktu itu. Garis persahabatan  ini dikenal dengan nama « poros Jakarta - Pnompenh – Hanoi – Peking – Pyongyang ».  Amerika Serikat berusaha dengan segala jalan dan cara untuk mencegah terjadinya   « effek domino » sebagai akibat dari perkembangan di Vietnam.

PKI dan Bung Karno adalah musuh yang harus dimusnahkan

Dengan makin besarnya pengaruh politik anti-imperialis Bung Karno di kalangan internasional (terutama di Asia-Afrika dan Amerika Latin) maka Bung Karno menjadi musuh besar dan utama bagi imperialisme AS. Dan di belakang politik Bung Karno yang pernah menggegerkan dunia internasional waktu itu (ingat : Konferensi Bandung, Indonesia keluar dari PBB, dukungan Indonesia kepada Tiongkok, persoalan Vietnam, sikap Bung Karno « Go to hell with your aid » dll dll) berdiri kekuatan PKI, yang pernah merupakan partai komunis yang terbesar di dunia sesudah Tiongkok dan Uni Soviet.

Dalam berbagai bahan dapat dibaca  bahwa PKI pernah mempunyai anggota 3,5 juta, ditambah dengan 3 juta dari gerakan pemudanya, dari kalangan serikat buruh 3,5 juta, dari kalangan tani 9 juta. Kalau digabungkan dengan  anggota dan simpatisan dari golongan wanita, seniman, sastrawan, sarjana dan lain-lainnya,  maka diperkirakan bahwa PKI mempunyai pendukung sekitar  20 juta orang, atau kurang lebih seperlima penduduk Indonesia pada waktu itu.

Terlepas dari apakah angka-angka itu semuanya mendekati kebenaran atau tidak, namun bisalah  dikatakan dengan pasti bahwa PKI memang pernah menjadi partai yang mempunyai pendukung yang besar sekali. Karenanya,  ada yang meramalkan bahwa seandainya jadi dilaksanakan pemilu waktu itu maka PKI pastilah akan mendapat suara  yang terbesar. Dan perkembangan yang demikian ini adalah hasil dari berbagai politik Bung Karno yang mendapat dukungan PKI. Karena, dalam banyak hal visi Bung Karno adalah sama atau sejiwa dengan visi PKI.

Di bidang dalam negeri politik Bung Karno yang didukung PKI adalah Pancasila, Bhinneka Tunggal Ika, Manipol, USDEK, Berdikari, NASAKOM, dan berbagai ajaran-ajaran revolusioner lainnya. Sedangkan di bidang luar negeri atau internasional politik Bung Karno yang didukung PKI adalah :  Konferensi Bandung, berbagai gerakan solidaritas Asia-Afrika (antara lain : Persatuan Wartawan Asia-Afrika, Konferensi Pengarang Asia-Afrika, Konferensi Juris Asia-Afrika)  GANEFO, Konferensi Internasional Anti Pangkalan Militer Asing, pidato di PBB « To build the world anew ».

Jelaslah bahwa gejala atau perkembangan yang demikian itu tentu saja membikin makin kuatirnya kekuatan reaksioner dalam negeri dan luar negeri. Karena itu, Bung Karno dan PKI menjadilah satu musuh yang perlu dimusnahkan atau dihancurkan. Dan untuk bisa menghancurkan Bung Karno perlu lebih dulu menghancurkan PKI, yang merupakan kekuatan utama yang mendukungnya. Dengan kalimat lain, penghancuran PKI akan memungkinkan penghancuran Bung Karno.

Tindakan Angkatan Darat terhadap PKI dan Bung Karno ;
lembaran hitam sejarah bangsa

Dengan terjadinya peristiwa G30S – yang masih mengandung berbagai persoalan dan banyak sekali pertanyaan yang belum terjawab sampai sekarang – maka kekuatan reaksioner dalam negeri (Angkatan Darat dengan  dibantu oleh kekuatan-kekuatan  lainnya yang anti Bung Karno dan anti-PKI), dan kekuatan imperialis mendapat dalih untuk menghancurkan PKI.

Tindakan pimpinan Angkatan Darat (waktu itu) dalam usaha menghancurkan kekuatan PKI merupakan lembaran hitam sejarah bangsa kita, yang perlu dicatat oleh generasi yang sekarang dan harus diketahui dengan jelas oleh generasi-generasi yang akan datang. Karena, untuk menghancurkan kekuatan PKI telah dibunuh jutaan anggota dan simpatisan atau pendukungnya, dengan cara-cara yang biadab dan betul-betul tidak manusiawi sama sekali. Ratusan ribu keluarga kehilangan sang-bapak atau sang-ibu, dan jutaan orang  juga ditahan atau dipenjarakan tanpa diadili (contohnya :di pulau Buru dan Nusakambangan).

Mengingat itu semua, maka sekarang makin jelaslah bagi banyak orang bahwa hancurnya kekuatan PKI dan bisanya Bung Karno digulingkan secara khianat oleh pimpinan Angkatan Darat  adalah sebenarnya sudah dipersiapkan (atau bahkan dimulai) oleh kekuatan-kekuatan reaksioner dalam negeri sejak lama (ingat :  peristiwa tiga Selatan dll) dengan kerjasama kekuatan asing ( ingat : PRRI, Permesta)

Dari sudut ini kita bisa melihat juga bahwa kehancuran PKI dan tergulingnya Bung Karno adalah karena akibat pengkhianatan besar-besaran pimpinan  Angkatan Darat (waktu itu) terhadap cita-cita para perintis kemerdekaan bangsa, terhadap Pancasila dan Bhinneka Tunggal Ika

Situasi negara dan bangsa di segala bidang yang serba busuk atau rusak yang kita saksikan dewasa ini adalah bukti yang jelas bahwa itu semua adalah akibat karena  dihilangkannya kedudukan  Bung Karno dari  kepemimpinan negara dan bangsa dan dihancurkannya PKI sebagai pendukung utama berbagai politik revolusionernya.

Dengan adanya kepemimpinan Bung Karno (dan dengan PKI sebagai pendukung utamanya) maka di masa-masa lalu  patriotisme dan nasionalisme kerakyatan bisa senantiasa dikobarkan, dan dihidupkan  terus-menerus semangat untuk menjalankan revolusi, serta digelorakan rasa cinta kepada rakyat dan gotong-royong, sebagai usaha dalam nation and character building. Justru itu semualah yang sekarang ini tidak ada ( !!!, tanda seru tiga kali) , dan karenanya negara dan bangsa kita menghadapi  kekosongan kepemimpinan nasional yang betul-betul berwibawa.

Mengingat itu semuanya, dan melihat pula kebejatan moral yang melanda seluruh kehidupan negara dan bangsa dewasa ini maka terasa sekali bahwa kehancuran PKI dan pengkhianatan terhadap Bung Karno adalah kerugian yang amat besar sekali bagi Republik Indonesia dan rakyat kita.

Perkembangan  situasi selanjutnya di kemudian hari akan membuktikannya lebih jelas lagi !!!
