---
layout: post
title: Pollycarpus dihukum tetapi kasus Munir belum selesai
date: 2005-12-21
---

Kiranya sudah dapat diduga sejak sekarang, bahwa persoalan sekitar konspirasi pembunuhan terhadap tokoh Hak Azasi Manusia, Munir, tidak selesai begitu saja dengan dijatuhkannya hukuman penjara 14 tahun terhadap Pollycarpus Budihari Prianto oleh Pengadilan Negeri Jakarta Pusat pada tanggal 20 Desember 2005. Bahkan, vonis yang dijatuhkan oleh majelis hakim itu malahan akan bisa membawa buntut yang masih panjang.

Sebab, banyak sekali orang dari berbagai kalangan yang menduga bahwa Pollycarpus hanyalah pelaku atau pelaksana saja dari suatu operasi gelap yang telah direncanakan oleh kalangan militer, khususnya Badan Intelijen Negara (BIN). Hal ini tercermin dari banyaknya berita atau komentar baik dalam pers nasional maupun mancanegara. Reaksi dari berbagai kalangan pun marak (Bagi mereka yang ingin menelusuri kembali berbagai hal yang berkaitan dengan kasus yang membikin heboh ini harap baca rubrik “Kumpulan berita soal kasus Munir” dalam website http://perso.club-internet.fr/kontak)

Rasa-rasanya, adalah sulit sekali bagi pimpinan militer untuk menutup-nutupi keterlibatan aparat-aparatnya yang tertentu (umpamanya BIN) dalam konspirasi pembunuhan Munir ini. Dengan tersiarnya berita-berita dalam pers dan televisi tentang proses pemeriksaan Pollycarpus sebelum diajukan di depan pengadilan dan selama di pengadilan, orang mudah mendapat kesan bahwa ia memang hanya jadi orang suruhan.

Kalau dipertanyakan secara sederhana saja, memang ada hal-hal yang aneh mengenai tindakan Pollycarpus dalam kasus konspirasi pembunuhan ini. Apakah Pollycarpus mempunyai kepentingan pribadi untuk membunuh Munir kalau tidak disuruh orang lain ?
Apakah Pollycarpus bisa memalsu surat perintah jalan kalau tidak bekerjasama dengan orang lain? Apakah racun arsenic bisa disediakan olehnya sendirian ? Mengapa ia menilpun rumah Munir untuk menanyakan kapan Munir berangkat ke Holland? Mengapa ia sering tilpun kepada orang-orang di BIN setelah terjadinya pembunuhan?


DILEMMA BESAR BAGI PIMPINAN TNI-AD

Memang, bisa dibayangkan bahwa kasus pembunuhan Munir ini merupakan dilemma besar bagi pimpinan militer (dalam hal ini : TNI-AD). Mengakui terus terang kesalahan besar (untuk tidak mengatakan kejahatan besar) dengan adanya pembunuhan terhadap tokoh HAM ini bisa berarti mengambil risiko rusaknya citra TNI-AD.

Tetapi, tidak mengakui kesalahan besar (atau kejahatan besar) yang berkaitan dengan konspirasi pembunuhan Munir ini pun bisa tambah makin memburuknya citra TNI-AD (setidak-tidaknya citra aparat intelijen), yang selama ini memang sudah buruk, akibat berbagai kejahatan yang dilakukan di masa rejim militer Orde Baru berkuasa selama 32 tahun.

Pastilah pimpinan TNI-AD (mungkin tidak seluruhnya) juga sudah menyadari bahwa, sebenarnya, reputasi golongan militer di kalangan bangsa Indonesia dewasa ini tidaklah lagi setinggi ketika masih disanjung-sanjung semasa Orde Baru. Sebab, sekarang banyak bukti bahwa pimpinan TNI-AD di masa lalu telah melakukan berbagai kejahatan, terutama pengkhianatan terhadap pemimpin besar bangsa, Bung Karno. Bukan saja Bung Karno telah digulingkan kekuasaannya sebagai presiden, tetapi kemudian ia pun dikurung sebagai tapol sampai wafat di tahanan.

Dan sesudah menghacurkan kekuatan kiri yang dipelopori oleh PKI dalam tahun 1965 dan 1966, di bawah pimpinan Suharto – dan dengan dukungan imperalisme AS - golongan militer (terutama TNI-AD) di masa lalu telah menguasai Republik kita dengan cara-cara yang tidak menguntungkan kehidupan demokratis bangsa, sama sekali tidak menghargai HAM, repressif, dan banyak menyalahgunakan kekuasaan. Selama puluhan tahun Orde Baru banyak pembunuhan, penculikan, dan terror, telah dilakukan secara illegal oleh aparat-aparat militer atau oleh kalangan-kalangan yang dekat dengan militer (ingat, antara lain : hilangnya belasan anak-anak muda PRD)


KASUS MUNIR : KESALAHAN ATAU KEBODOHAN

Kiranya, dapat dimengertilah bahwa sisa-sisa pendukung rejim militer Orde Baru (terutama kalangan TNI-AD) masih berusaha terus dengan segala cara dan jalan mempertahankan kepentingannya, walaupun Suharto sudah dipaksa turun dari kekuasaannya sejak 1998. Mereka ini merupakan kekuatan yang sebenarnya (!!!) menentang reformasi, tidak suka dengan demokrasi dan HAM. Mereka pulalah yang merupakan kekuatan reaksioner di tengah-tengah bangsa, yang tidak senang dengan bangkitnya rakyat lewat kehidupan yang betul-betul demokratis, lewat organisasi-organisasi massa, lewat LSM, dan lewat berbagai gerakan.

Dan dalam hal ini, Munir adalah satu tokoh besar yang menonjol sekali dalam kebangkitan melawan kekuatan reaksioner. Dengan kegigihan dan keberanian yang menakjubkan banyak orang ia telah melakukan berbagai kegiatan untuk membongkar pelanggaran HAM atau kejahatan terhadap kemanusiaan, antara lain orang-orang yang diculik atau ditangkap secara sewenang-wenang. Mungkin, oleh karena sebagian kalangan militer (TNI-AD, termasuk BIN) menganggap kegiatan Munir ini sudah merupakan bahaya bagi mereka, maka diciptakanlah “scenario” untuk menghilangkannya dari muka bumi.

Tetapi, mereka yang telah mengambil keputusan untuk membunuh Munir ini, walaupun sudah direncanakan, dipersiapkan dan dilaksanakan secara “njlimet”, kali ini telah melakukan kesalahan atau kebodohan yang besar sekali. Sebab, berlainan dengan kasus-kasus pembunuhan atau penculikan yang banyak dilakukan di masa-masa yang lalu, kali ini mereka kesandung batu yang amat besar sekali. Munir adalah tokoh yang sudah menjadi besar sekali bagi banyak kalangan, terutama kalangan yang memperjuangkan demokrasi dan HAM. Bukan saja di dalamnegeri, juga di luarnegeri.

Itu sebabnya, maka kasus pembunuhan Munir menjadi topik hangat di Indonesia, yang menjadi perhatian dari banyak organisasi dan gerakan. Juga di luarnegeri, banyak organisasi-organisasi internasional menaruh perhatian besar terhadap pembunuhan yang dikaitkan dengan kemungkinan besar terlibatnya aparat intelijen militer Indonesia ini.


SIAPA SEBENARNYA DALANG PEMBUNUHAN ?

Walaupun Pollycarpus sudah dijatuhi hukuman penjara 14 tahun, tetapi masih ada pertanyaan penting yang perlu mendapat jawaban: siapakah sebenarnya dalang yang bersalah karena menyuruh atau memberikan instruksi ? Berikut di bawah ini disajikan sejumlah kutipan dari pernyataan berbagai orang yang menarik untuk diperhatikan :


« Pollycarpus hanyalah pelaku pembunuhan di lapangan," kata Suciwati, istri alm. Munir, seusai sidang pembacaan putusan perkara pembunuhan Munir di Pengadilan Negeri Jakarta. Suciwati menyatakan bahwa keinginan utamanya adalah terungkapnya dalang pembunuh Munir. Karena itu, ia meminta aparat melanjutkan penyelidikan kasus ini. ( dari Tempo Interaktif, 20/12/2005)


Ungkapan lainnya yang juga amat menarik untuk diperhatikan :"Saya tidak melakukannya (pembunuhan)," kata Pollycarpus dengan keras saat menjawab pertanyaan Ketua Majelis Hakim Tjitjut Sutiyarso, di Pengadilan Negeri Jakarta. Di tempat yang sama, pengacara Pollycarpus, M. Assegaf, menuduh majelis hakim melindungi pembunuh Munir yang sebenarnya.
Istri Pollycarpus, Ny. Herawati Swandari, yang menangis sepanjang sidang tampak marah. Ia berulangkali menyebutkan bahwa putusan hakim sebagai dongeng. "Vonisnya panjang sekali, tapi semuanya hanya omong kosong," ia menyatakan. ( menurut Tempo Interaktif, 20/12/2005)


Lebih-lebih menarik lagi ialah pernyataan Kepala Badan Intelijen Negara Syamsir Siregar yang menilai « aktor utama pembunuhan aktivis Munir belum terungkap meskipun Pollycarpus Budihari Priyanto dijatuhi hukuman 14 tahun penjara.

Menurutnya, « polisi penyidik hanya menyeret Polycarpus ke pengadilan padahal telah diberikan kesempatan seluas-luasnya untuk membongkar kasus ini hingga tuntas. Badan Intelijen Negara, kata dia, juga telah berkerja sama untuk menyelesaikan kasus ini. Dia (penyidik) belum bisa mengungkap siapa di balik Pollycarpus itu," kata Syamsir kepada pers di kompleks istana,


Sejumlah kalangan mencurigai kaitan Pollycarpus, pilot senior Garuda Indonesia, dengan aparat intelijen. Penyidik pun beberapa kali memanggil mantan pejabat BIN. Namun, hingga kini, Pollycarpus menjadi satu-satunya terdakwa dalam kasus pembunuhan Munir. (juga dari Tempo Interaktif, 20/12/2005)



TNI-AD HARUS AKTIF IKUT BONGKAR


Jadi, kasus konspirasi pembunuhan Munir, tidak berarti sudah selesai dengan dijatuhkannya hukuman penjara 14 tahun terhadap Pollycarpus. Sebab, dalang yang sebenarnya dari kasus pembunuhan ini masih belum terungkap dengan jelas dan terang-terangan, meskipun sudah banyak tanda-tanda yang mengarah bahwa aparat intelijen militer (BIN) terlibat di dalamnya.


Oleh karena itu, adalah untuk kepentingan citra baik atau nama baik TNI-AD pada khususnya, dan golongan militer pada umumnya, maka seyogyanya pimpinan TNI-AD ikut campur tangan aktif, dengan menginstruksikan kepada BIN untuk ikut aktif sekuat-kuatnya dan sejujur-jujurnya membongkar siapa yang di belakang pembunuhan ini. Adalah fikiran yang keliru kalau menganggap bahwa dengan menutup-nutupi kesalahan besar - yang sebenarnya merupakan kejahatan besar ini –citra TNI-AD dapat dijaga. Bahkan, sebaliknya!


Dengan menutup-nutupi kesalahan atau kejahatan yang termanifestasikan dengan pembunuhan Munir ini, atau mempersulit atau menghalangi pembongkarannya secara tuntas, TNI-AD akan membikin tambah buruknya citra yang dalam waktu puluhan tahun sudah buruk di mata banyak orang. Keburukan-keburukan ini sudah banyak - dan sudah sejak lama ! - diketahui oleh banyak orang, baik di dalamnegeri maupun di luarnegeri (umpamanya : oleh kedutaan-kedutaan banyak negara di Jakarta, dan perwakilan oranisasi-organisasi internasional).


Dalam hal ini, peran Presiden SBY adalah amat penting. Sebagai kepala negara dan pemimpin tertinggi angkatan bersenjata, ia perlu ikut berusaha supaya TNI-AD jangan terus-menerus memperpanjang dosa-dosanya yang sudah banyak dilakukan di masa lampau. Presiden SBY sudah pernah menyatakan bahwa kasus Munir akan diselesaikan secara tuntas. TPF (Tim Pencari Fakta) kasus Munir (yang dibentuk dengan keputusan presiden) juga sudah menyampaikan laporan tertulis kepada presiden tentang hasil penyelidikannya. Dalam laporannya ini TPF menyajikan bahan-bahan yang memberikan indikasi kuat bahwa di belakang kasus Munir ini ada kejahatan konspiratif.


Sekarang menjadi makin kuat dugaan bahwa ada kekuatan-kekuatan militer yang berdiri di belakang usaha-usaha untuk men-torpedo hasil-hasil temuan TPF dan melakukan “tekanan” ke kiri dan ke kanan – termasuk tekanan terhadap Presiden SBY – untuk mencegah terbongkarnya dalang yang sesungguhnya dari komplotan yang konspiratif ini.


Kasus pembunuhan Munir dan tuduhan terlibatnya intelijen militer (BIN) ini sekarang sudah menjadi sorotan banyak orang di dunia, bukan hanya di Indonesia saja. Karenanya, apakah kasus ini akhirnya dapat diselesaikan secara adil, jujur, transparan dan menurut hukum, merupakan salah satu barometer penting (antara berbagai barometer lainnya) tentang dapat tidaknya hukum benar-benar ditegakkan dan hak azasi manusia betul-betul dihargai di negeri kita.

---

Jawapos, 23 Desember 2005,

BIN Diminta Ikut Tuntaskan Kasus Munir

JAKARTA - Selain Kapolri Jenderal Pol Sutanto, Kepala BIN Syamsir Siregar diinstruksikan presiden untuk membantu penuntasan kasus Munir. BIN akan berkoordinasi dengan kejaksaan dan Polri untuk menemukan dalang pembunuh aktivis HAM tersebut.

Adanya perintah SBY itu diakui Kepala BIN Syamsir Siregar kepada wartawan saat menghadiri rapat konsultasi dengan tim pemantau Poso di Gedung DPR kemarin.

Untuk kepentingan itu, BIN membutuhkan akses ke semua dokumen penyidikan polisi serta semua hasil penyelidikan dan rekomendasi tim pencari fakta (TPF) Munir. BIN juga akan mempelajari salinan putusan majelis hakim PN Jakpus yang menghukum Pollycarpus Budihari Prianto.

Kasus Munir itu memang sudah diajukan ke pengadilan. Namun, sejauh ini baru Polly yang dijatuhi hukuman. Dalam sidang di PN Jakur Selasa lalu, pilot senior Garuda itu dihukum 14 tahun penjara. Dia terbukti melakukan pembunuhan berencana.

"Selain Polly, ada pihak lain yang menginginkan kematian Munir," kata Ketua Majelis Hakim Cicut Sutiarso saat itu. Namun, tidak disebutkan secara jelas siapa mereka.

Majelis hakim hanya mengutip kesaksian mantan Deputi V BIN Muchdi P.R. yang menyatakan Munir kurang disukai karena selalu mengkritik pemerintah, terutama soal TNI dan BIN. Alasan itu dinilai sebagai motivasi pembunuhan.

Kapolri yang juga hadir di DPR mengatakan, pihaknya tetap berharap agar Polly berkata jujur dan menceritakan siapa aktor di balik pembunuhan Munir. Polri akan memberikan perlindungan kepadanya jika dia takut. "Kami pasti akan memberikan perlindungan pada semua pihak yang membantu polisi," janji Sutanto.

Sementara itu, Kejaksaan Agung (Kejagung) akan berkonsentrasi pada pengadilan banding nanti. Kejaksaan tidak akan mengajukan bukti-bukti baru atau saksi baru. Jaksa akan memfokuskan vonis majelis hakim yang lebih ringan daripada tuntutan jaksa penuntut umum (JPU).

"Kita hanya banding masalah hukuman yang terlalu ringan dan minta supaya tinggi," kata Kepala Pusat Penerangan dan Hukum Kejaksaan Agung Masyhudi Ridwan. Sebelumnya, jaksa menuntut hukuman seumur hidup terhadap polly, tapi hakim memutus 14 tahun.

Meski begitu, Kejaksaan Agung tetap membantu penyidik Mabes Polri untuk mengungkap dalang pembunuhan tersebut. Untuk itu, pihaknya akan terus berkoordinasi dengan kepolisian.

Dia menjelaskan, yang berwenang melakukan penyidikan tetap Mabes Polri. Pasalnya, kasus Munir merupakan tindak pidana umum.

Mengenai tidak dihadirkannya Sekretaris Umum BIN Nurhadi Djazuli, Masyhudi mengatakan, kesaksian itu tidak terlalu penting. Jaksa merasa cukup dengan saksi-saksi yang dihadirkan. Justru, pemeriksaan saksi BIN dapat mempengaruhi pencapaian-pencapaian dalam persidangan Polly. "Karena sudah dituntut dan diputus, jadi jangan mundur kembali," ujar pria berkacamata minus ini. (naz/yes/yog)

* * *

Tempo Interaktif,, 23 Desember 2005

Presiden Tidak Akan Bentuk Tim Baru Kasus Munir

TEMPO Interaktif, Jakarta: Presiden Yudhoyono tidak akan membentuk tim baru untuk menyelidiki kasus pembunuhan aktivis Munir. Menurut Presiden, lebih baik lembaga yang ada menjalankan fungsinya.

"Kepolisian sebagai penyidik, kejaksaan sebagai penuntut, Mahkamah Agung dan lembaga peradilan di bawahnya sebagai pemutus tuntutan, itu yang benar," kata Yudhoyono dalam keterangan pers seusai pertemuan konsultasi informal dengan Ketua DPR Agung Laksono di kantor presiden, Jumat (23/12).

Jika terlalu banyak dibentuk organisasi atau komisi yang bersifat ad hoc dan tidak diatur dalam undang-undang, menurut Presiden, dapat menunjukkan kesan bahwa lembaga penegakan hukum belum bekerja optimal. "Tetapi karena kebutuhan waktu itu, saya putuskan untuk membentuk tim pencari fakta," kata dia menunjuk pembentukan tim pencari fakta kematian Munir.

Ia juga telah menyampaikan kepada Kepala Kepolisian RI dan Jaksa Agung untuk terus melaksanakan proses penegakan hukum dengan benar dan adil. "Dan benar-benar menyelesaikan secara tuntas kasus meninggalnya Munir," katanya.

Yudhoyono mengatakan tidak akan menanggapi proses hukum yang telah berjalan. "Siapa yang salah, siapa yang tidak salah, siapa yang lebih bertanggung jawab dalam kasus itu, apakah di samping saudara Pollycarpus ada (pelaku) yang lain, saya tidak punya kewenangan untuk bicara itu," katanya.

Jika memang ada pelaku yang lain, menurutnya, ada mekanisme penyidikan yang bisa dilakukan. Ia menyerahkannya kembali soal itu kepada mekanisme penegakan hukum. Dimas Adityo

* * *

Suara Merdeka, 23 Desember 2005 NASIONAL

Menguak Ulang Pembunuhan Munir (1)

Hakim pun Sebut Mantan Pejabat BINKemunculan nama Muchdi PR, salah seorang mantan petinggi Badan Intelijen Negara (BIN) yang terkait dengan pembunuhan Munir dalam vonis hakim Pengadilan Negeri Jakarta Pusat semakin menguatkan dugaan bahwa BIN berada di balik pembunuhan aktivis HAM itu. Pembunuhnya, Pollycarpus Budihari Prijanto, sudah divonis 14 tahun penjara. Namun dugaan tersebut masih harus dibuktikan dengan penyelidikan oleh pihak yang berwenang. Bagaimana motivasi awal sehingga pembunuhan itu terjadi?

Tulisan ini mencoba menguraikannya.KEGELISHAN Suciwati akan terjadinya sebuah firasat yang tidak baik sebenarnya sudah ada sejak Munir menceritakan kepada dirinya bahwa pada saat berangkat ke Amsterdam, suaminya itu menelepon Hendropriyono yang saat itu menjabat Kepala BIN soal pencekalan dirinya. Saat itu, Hendro membantah bahwa Munir dicekal.

''Kegelisahan makin menjadi saat Pollycarpus yang tidak pernah saya kenal menelepon ke ponsel saya untuk memastikan keberangkatan Munir ke Amsterdam. Selain itu, Munir di Bandara Cengkareng pernah bercerita bahwa saat pergi ke Swiss dirinya pernah dicekal,'' kata Suciwati dalam BAP-nya.Dari pengakuan Suciwati tersebut, majelis hakim kemudian mendapatkan petunjuk bahwa ada pihak-pihak yang tidak senang terhadap Munir atas kevokalnnya kepada pemerintah, terkait dengan beberapa kasus HAM sehingga menimbulkan motivasi untuk menghilangkan nyawa Munir.

alu siapa yang tidak senang terhadap Munir? Sejauh ini hakim menyebutkan dengan jelas bahwa orang yang memiliki rasa tidak senang terhadap Munir adalah orang yang memiliki motivasi untuk membunuh Munir. Hakim pun dengan mantap mengatakan bahwa orang yang dimaksud adalah mantan petinggi BIN, Muchdi Prawiroprandjono atau yang lebih dikenal dengan nama Muchdi PR. Dia memiliki motivasi untuk membunuh Munir karena pernah mengingatkan Munir melalui pengacara Adnan Buyung Nasution tentang aktivitas Munir yang dianggap membahayakan.

Dari pengakuan tersebut, orang yang berbicara di nomor telepon 0811900978 adalah orang yang menghendaki Munir dibunuh.Jadi, berdasarkan tingkat komunikasi frekuensi yang sering terjadi antara nomor 021-7407459 dan 0815843083 yang tidak lain adalah nomor Pollycarpus yang tercatat 41 kali dengan nomor 0811900978 kepunyaan Muchdi PR, majelis hakim menyimpulkan, mereka berdua bersepakat dan memiliki kegiatan yang sama.

''Meskipun tidak diketahui isi pembicaraan di telepon tersebut, tetapi bisa disambungkan dengan sikap Pollycarpus yang mondar-mandir di sekitar kelas bisnis, bar premium, dan toilet saat berada di pesawat pada penerbangan GA 974 Jakarta-Singapura. Artinya, antara terdakwa dan pemilik nomor 0811900978 ini terjadi kesepakatan tentang tata cara pelaksanaan keinginan mereka menghilangkan nyawa Munir,'' kata ketua majelis hakim Cicut Sutiarso.

Berdasarkan petunjuk di atas, layak kiranya jika Cicut kemudian menegaskan bahwa masih ada pihak lain yang ingin menghilangkan nyawa Munir dan hanya bisa dibuktikan dengan penyelidikan yang lebih akurat oleh pihak yang berwenang. ''Semua harus dibuktikan kembali,'' katanya.Dari kesepakatan tersebut, dengan inisiatif sendiri, Pollycarpus kemudian memutuskan untuk berangkat ke Singapura setelah pada tanggal 2 September 2004 menelepon Suciwati untuk mengetahui keberangkatan Munir ke Amsterdam.

''Polly ke Singapura tanpa surat perintah, itu menjadi petunjuk bahwa tujuan terdakwa ke Singapura untuk membunuh Munir yang sudah dibicarakan dengan Muchdi,'' kata Cicut.Keberangkatan Pollycarpus atas niat sendiri itu dapat dibuktikan setelah ada pengakuan dari Ramelgia Anwar selaku VP Corporate Security yang mengatakan surat dari dirut bukan ke Singapura, melainkan surat perbantuan di Corporate Security.

''Bukti lain, Polly tidak berani melakukan check on the spot (kasus dumping fuel) di Bandara Changi karena tidak memiliki surat resmi,'' kata Cicut menambahkan.Kemudian, dua buah surat bertanggal 4 September 2004 dan 15 September 2004 dibuat setelah Pollycarpus pulang dari Singapura. Surat tersebut dibuat untuk penentuan akomodasi sehingga perbuatan yang tidak halal menjadi halal. ''Ramelgia juga tidak mengetahui keberangkatan terdakwa, tetapi dibebani biaya perjalanan terdakwa.

Tanggal 15 September dan 4 September tidak sesuai dengan isi surat dan pembuatan surat palsu membebankan biaya pada unit Corporate Security,'' paparnya.Dengan demikian, Polly dan Ramelgia telah membuat dan menggunakan surat yang tidak benar dan seolah-olah asli. ''Surat dibuat untuk membebankan biaya akomodasi pada unit Corporate Security (CS). Padahal unit CS tidak mengetahui kepergian terdakwa,'' katanya.

Keterlibatan PollycarpusYang jelas, selain keterlibatan Muchdi, Uli Parulian Sihombing, salah seorang mantan anggota TPF Munir menegaskan, tim kepolisian harus bisa membuktikan keterlibatan Pollycarpus dan hubungan perintah kerja yang terjadi antara Pollycarpus dan Muchdi. Yang terpenting adalah bagaimana polisi dapat menemukan hubungan antara Sekertaris Utama BIN Nurhadi Jazuli, Muchdi PR selaku mantan Deputi V BIN, dan Bambang Irawan yang diduga sebagai penumpang ke-15 dengan menemukan mastermind dari pembunuhan terhadap Munir dan menjelaskan peran mantan Kepala BIN AM Hendropriyono,'' katanya.

Bagi Uli, keberhasilan pengungkapan kasus Munir tidak lagi berada di tangan kepolisian, tetapi di tangan Presiden selaku pemegang kekuasaan politik. Sebab, kekuasaan hukum terbukti tidak mampu mengungkap dalang pembunuhan terhadap aktivis HAM itu.''Kasus ini masih gelap di masyarakat karena kita masih perlu mengetahui jawaban siapa yang memiliki ide untuk membunuh Munir. Apakah berasal dari kalangan individu di dalam BIN atau memang berasal dari institusi BIN itu sendiri,'' tanyanya.

Tudingan keras tentang kesanggupan pihak kepolisian untuk mengungkap aktor intelektual dalam kasus pembunuhan Munir memang masih perlu dipertanyakan kepada angin yang menggoyangkan rumput savana. Artinya, polisi masih ragu untuk memulai penyelidikan kembali. Sebab hingga kini kepastian Yetty Susmiarti, Ramelgia Anwar, dan Oedi Irianto untuk dijadikan tersangka masih dalam impian semusim belaka.Jika demikian, sanggup dan beranikah Polri menyeret aktor utama yang tidak lain adalah BIN yang berada di balik pembunuhan Munir? ''Jika ini berhasil dilakukan maka Presiden Susilo Bambang Yudhoyono tentu akan membuat prestasi bersejarah di bidang penegakan HAM,'' kata Suciwati harap-harap cemas. (Ali Imron Hamid-48n)
* * *

Media Indonesia,, 23 Desember 2005

Mabes Polri Kawal Pollycarpus

Jika Ungkap Pembunuhan Munir


JAKARTA--MIOL: Mabes Polri akan memberikan perlindungan kepada Pollycarpus Budihari Priyanto, termasuk mengawal secara pribadi, jika terpidana 14 tahun atas kasus pembunuhan aktivis HAM Munir itu bersedia mengungkap dalang pembunuh Munir.

"Perlindungan saksi dari polisi itu bentuknya tergantung situasi mulai dari dijaga rumahnya, dikawal secara pribadi hingga dipasangi alat komunikasi yang terhubung langsung dengan polisi," kata Wakil Kepala Divisi Humas Mabes Polri, Kombes Pol Anton Bachrul Alam di Jakarta, Jumat.

Sebelumnya Kapolri Jenderal Pol Sutanto menyebutkan bahwa pihaknya bersedia memberikan perlindungan jika Pollycarpus bersedia menjadi saksi untuk mengungkap dalang pembunuhan Munir.PN Jakarta Pusat memvonis Pollycarpus 14 tahun penjara setelah dinyatakan bersalah berperan membunuh Munir dengan cara memasukkan racun arsenik ke dalam mie goreng yang dimakan Munir sewaktu dalam pesawat Garuda yang terbang dari Jakarta menuju Belanda.

Munir akhirnya tewas ketika pesawat yang ia tumpangi melintas di atas Hungaria. Setelah diotopsi oleh tim medis Belanda, ditemukan bahwa di jasad Munir terdapat kandungan arsenik yang jumlahnya berlebihan namun hingga kini hanya Pollycarpus yang telah dinyatakan bersalah.

Mengenai kelanjutan penyidikan kasus Munir, Anton Bachrul Alam mengatakan penyidikan tidak dihentikan kendati sebelumnya kepala tim penyidik, Brigjen Pol Marsudi Hanafi telah dimutasi sebagai staf ahli Kapolri."Pak Marsudi masih terlibat penyidikan kasus Munir karena koordinasi tim itu di bawah Bareskrim. Kendati sudah menjadi staf ahli, dia kan masih bisa menjadi penyidik," kata Anton. (Ant/OL-03)
* * *


Media Indonesia, 22 Desember 2005


Kepala BIN Tegaskan Akan Ungkap

Dalang Pembunuhan Munir
JAKARTA--MIOL: Kepala Badan Intelijen Negara (BIN) Syamsir Siregar mengatakan institusi yang dipimpinnya akan berupaya mengungkap siapa dalang di balik pembunuhan aktivis HAM Munir.Upaya pengungkapan itu dilakukan dengan jalan mengintensifkan penyelidikan yang bekerjasama dengan Polri.

"Kami akan melaksanakan instruksi Presiden dengan berkoordinasi bersama Polri," kata Syamsir kepada wartawan di DPR, Kamis.Pernyataan itu disampaikannya untuk menanggapi pernyataan Presiden Susilo Bambang Yudhoyono yang memerintahkan aparat hukum untuk mengungkap secara tuntas kasus pembunuhan Munir yang diduga mengandung konspirasi.

Melalui juru bicaranya Andi Mallarangeng, Presiden Yudhoyono mengatakan kasus yang terkait dengan adanya konspirasi ada kalanya tidak mudah diungkap aparat penegak hukum.Namun Presiden berkomitmen untuk terus mengungkap kasus kematian Munir.

Syamsir mengatakan karena kasus Munir ini sudah ditangani oleh Pengadilan, maka BIN akan mempelajari lebih detil lagi mengenai berkas-berkas yang sudah ada di pengadilan.Menurut dia, instruksi Presiden untuk mengungkap secara tuntas kasus pembunuhan Munir ini, juga akan dilakukan dengan berkoordinasi bersama aparat lain yang terkait selain dengan Polri.

Majelis Hakim Pengadilan Negeri Jakarta Pusat, Selasa (20/12), menjatuhkan vonis 14 tahun penjara terhadap terdakwa kasus pembunuhan aktivis HAM Munir, Pollycarpus Budihari Priyanto.Putusan itu disampaikan dalam sidangnya yang dipimpin Ketua Majelis Hakim Cicut Sutiarso, di PN Jakarta Pusat, Selasa.Vonis tersebut dijatuhkan karena menurut Majelis Hakim, Pollycarpus terbukti turut serta melakukan perbuatan pembunuhan berencana terhadap Munir.(Ant/OL-03)
* * *

Kompas, 21 Desember 2005

Presiden: Dalam Kasus Munir Ada Konspirasi

Kepala Polri Diperintahkan Ungkap Tuntas Masalah Ini



Jakarta, Kompas - Presiden Susilo Bambang Yudhoyono mengakui kasus yang terkait dengan adanya konspirasi adakalanya tidak mudah diungkap aparat penegak hukum. Namun, Presiden Yudhoyono tetap berkomitmen untuk terus mengungkap kasus kematian Munir.


Juru Bicara Kepresidenan Andi Mallarangeng menjawab pertanyaan pers di Kompleks Istana, Jakarta, Rabu (21/12), mengatakan, Presiden Yudhoyono—di sela-sela kunjungan kerjanya di Jawa Timur, Selasa malam—telah menginstruksikan Kepala Kepolisian Negara RI Jenderal (Pol) Sutanto untuk mengungkap tuntas kasus kematian Munir.


Munir tewas di atas pesawat GA 974, yang membawanya dari Singapura ke Amsterdam pada 7 September 2004. Sekitar satu tahun tiga bulan (470 hari) setelah peristiwa itu, majelis hakim Pengadilan Negeri Jakarta Pusat menghukum Pollycarpus Budihari Priyanto (Polly), seorang pilot Garuda, dengan hukuman 14 tahun penjara. Pollycarpus dinyatakan terbukti turut melakukan pembunuhan berencana dan turut menggunakan surat palsu. Majelis juga mengemukakan, Pollycarpus bukanlah pelaku tunggal sehingga keterlibatan pelaku lain juga harus diusut.


Presiden Yudhoyono, yang pernah mengatakan pengungkapan kasus pembunuhan Munir sebagai test of our history, meminta Polri bekerja sama dengan aparat negara lainnya—yang selama ini terkait dengan penyidikan kasus pembunuhan Munir, seperti Badan Intelijen Negara (BIN) dan Kejaksaan Agung.
Andi mengatakan, setelah mendengar putusan pengadilan berikut pertimbangan hukumnya, Presiden langsung membulatkan tekad untuk terus mengungkap kasus tewasnya Munir.


”Sistem harus bekerja, Polri, kejaksaan, bahkan BIN harus jalan bersama memperkuat kinerja mengungkap kasus Munir. Memang kasus konspirasi tidak gampang diungkapkan, tetapi Presiden telah meminta setiap lembaga dan aparat negara untuk terus mengungkapkannya,” ujar Andi.


Menurut Presiden, kata Andi menambahkan, semua yang terlibat dalam kasus Munir di luar Pollycarpus harus diproses dan dinyatakan bersalah jika memang bukti hukum memperkuatnya. ”Mereka harus mendapat hukuman. Pertimbangan pengadilan dan bukti yang terungkap selama proses pengadilan itu hendaknya menjadi rujukan bagi Polri, kejaksaan, termasuk BIN, untuk menelusurinya kembali,” ucapnya.


Di Surabaya, Kepala Polri Jenderal (Pol) Sutanto meminta Pollycarpus terbuka pada penyidik Polri. ”Kami mohon kepada masyarakat dan Pollycarpus untuk terbuka kepada kami, supaya kami tahu mengenai hal itu yang sesungguhnya,” kata Sutanto seperti dikutip Antara.


Kuasa hukum Pollycarpus, Mohammad Assegaf, yang dihubungi terpisah mengatakan, pernyataan Kepala Polri agar Polly berterus terang adalah pernyataan yang didengar untuk ke sekian kalinya. ”Apa maksud pernyataan itu, apakah mau memberikan kesan bahwa Polly menutupi sesuatu,” katanya.

”Mau disuruh terbuka apanya. Wong tak ada yang ditutupi dari Polly,” kata Assegaf lagi.
Dia juga mengatakan, pengadilan telah membuka wacana adanya keterlibatan orang lain yang berkomunikasi dengan Polly melalui telepon. ”Kenapa enggak orang itu dulu yang dijadikan fokus,” ucap Assegaf.


Ketua Komisi III DPR Trimedya Panjaitan (F-PDIP, Sumatera Utara II) mengatakan, putusan hakim itu sebenarnya terlalu dipaksakan. Meski demikian, apa yang diungkapkan pengadilan, bahwa ada pelaku lain yang harus dimintai pertanggungjawaban, meskipun tak mempunyai kekuatan mengikat, harus ditindaklanjuti aparat kepolisian. ”Ini tantangan bagi Presiden dan khususnya kepolisian,” ujarnya.
Jaksa ajukan banding


Menyusul sikap Pollycarpus dan penasihat hukumnya untuk menyatakan banding atas putusan hakim, jaksa juga ikut menyatakan banding. Sikap jaksa itu disampaikan Kepala Pusat Penerangan Hukum Kejaksaan Agung Masyhudi Ridwan di Kejaksaan Agung, Jakarta, Rabu. Masyhudi menjelaskan, dasar sikap banding yang diambil jaksa adalah kurang sepadannya putusan majelis hakim dengan tuntutan jaksa.


Dalam kesempatan terpisah, Komite Aksi Solidaritas Untuk Munir (KASUM) mendesak Presiden Yudhoyono segera membentuk tim kepresidenan untuk meneruskan temuan Tim Pencari Fakta (TPF) Munir. Hal tersebut penting agar dalang pembunuhan Munir dapat diungkap. Demikian penjelasan Hendardi, Uli Parulian Sihombing, dan Pungky Indrawati yang tergabung dalam KASUM, kemarin.


Menurut Hendardi, pembentukan tim kepresidenan—yang memiliki wewenang jauh lebih kuat dibandingkan dengan TPF—sangat diperlukan. Dengan wewenang kuat, tim kepresidenan dapat menyentuh institusi negara semacam BIN yang tidak dapat dijangkau oleh TPF maupun aparat kepolisian.
Vonis 14 tahun terhadap Polly, katanya, bukan gema dari lonceng keadilan. Polly hanyalah pelaku lapangan dan besar kemungkinan dia akan dibebaskan di tingkat banding atau kasasi. (har/idr/ana/bdm)
