---
layout: post
title: Deklarasi « 7 Cita-cita Perubahan Indonesia » oleh angkatan muda
date: 2011-04-21
---

Persatuan antara berbagai golongan lintas aliran politik

dan agama menuju Kongres Pemuda

Satu perkembangan penting sedang terjadi di kalangan angkatan muda Indonesia !!! Persatuan atau kesatuan tekad sedang digalang untuk berjuang bersama-sama  demi adanya perubahan besar-besaran dan fundamental di negeri kita yang sedang dilanda korupsi yang parah di seluruh bidang, kebejatan moral di « kalangan atas », pelecehan hukum yang keterlaluan di kalangan penegak hukum, kebusukan di kalangan politisi dan para tokoh masyarakat.
Berbagai gerakan di kalangan muda Indonesia, yang lintas aliran politik dan lintas agama telah mendeklarasikan  «  7 Cita-cita Perubahan Indonesia » yang merupakan program besar dan penting untuk bernegara dan berbangsa, yang sekarang sedang morat-marit atau ambur-adul di banyak bidang.
Perkembangan penting yang mencerminkan adanya kesamaan atau kesepakatan tujjuan ini dapat dilihat dari tergabungnya banyak organisasi-organisasi dari kalangan Islam, Katolik, Protestan, Hindu, Budha, dan sosialis (atau kiri), dalam deklarasi « 7 Cita-cita Perubahan Indonesia ».
Kalau semua berjalan lancar, maka bisa diharapkan bahwa usaha besar ini akan merupakan dorongan untuk terbentuknya front nasional di kalangan angkatan muda Indonesia yang akhirnya bisa bersama-sama melahirkan Kongres Pemuda Indonesia di kemudian hari.
Perkembangan yang demikian ini akan membikin angkatan muda Indonesia tidak ketinggalan dari perkembangan di negeri-negeri Arab (antara lain di Aljazair, Libia, Tunisia, Mesir, Siria, Jordania, Bahrain, Yaman, Oman, bahkan juga di Saudi Arabia dan Iran yang bukan Arab) dimana angkatan mudanya sudah dan sedang berjuang untuk perubahan.
Deklarasi bersama « 7 Cita-cita Perubahan Indonesia » oleh angkatan muda Indonesia ini sangat penting dan bersejarah, mengingat bahwa sekarang ini sudah tidak bisa diharapkan lagi adanya perubahan-perubahan besar dan fundamental yang bisa dibikin oleh para « tokoh-tokoh » yang sekarang dan oleh sisa-sisa Orde Baru.
Hari kemudian bangsa dan negara kita adalah di tangan angkatan muda Indonesia !!!



A.Umar Said

Paris, 21 April  2011       


* * *


Persatuan Gerakan Mahasiswa dan Pemuda

Siap Turunkan SBY-Boed


Selasa, 19 Apr 2011

JAKARTA, RIMANEWS - Gerakan Pemuda dan Mahasiswa yang terdiri dari elemen PII, PMKRI, HMI, PMII, LMND, GMKI, KAMTRI, HIMMAH dan IMM kemarin (18/04) mendeklarasikan 7 Cita-Cita Perubahan Indonesia dengan tujuan memberikan kontribusi positif bagi negara.

Pertemuan yang di lakukan di gedung Joeang 45 ini menggagas 7 Cita-Cita Perubahan, yang berisi antara lain Indonesia merdeka dari penjajahan gaya baru demi mewujudkan kedaulatan dan kemandirian bangsa, supremasi hukum tanpa diskriminasi, tangkap adili dan sita kekayaan para perampok uang rakyat, persatuan Indonesia yang berlandaskan keadilan sosial dan semangat kebhinekaan, Indonesia bebas dari kemiskinan melalui redistribusi tanah untuk rakyat dan industrialisasi yang kuat dan mandiri, Indonesia memiliki pemimpin nasional yang mandiri, berani, demokratis dan bermental kerakyatan serta demokrasi Indonesia tanpa oligarki.

Lamen Hendra Saputra ketua umum Liga Mahasiswa Nasional Untuk Demokrasi (LMND) mengatakan kaum pergerakan mahasiwa dan pemuda menilai ada sistem yang salah dalam konsep pembangunan di Indonesia.

“Perkumpulan kita hari ini ingin memberitahukan kepada rakyat bahwa Indonesia diselimuti gurita hitam, yang berlindung di dalam istana hitam Cikeas. Dari awal SBY-Boediono naik sebagai Presiden dan Wapres saja sudah criminal, dengan menggunakan segala cara untuk memenangkan pemilu termasuk menggunakan biaya bailout Century untuk kampanye, kemudian hari ini dengan bangganya melakukan kebohongan public dengan mengeluarkan data pertumbuhan ekonomi, tapi ironisnya itu tidak di ikuti dengan peningkatan kesejahteraan masyarakat. Solusinya adalah pembangunan front persatuan anti boneka imperialis(SBY-Boediono) melawan kebohongan public ini.

Poin yang sudah kami susun adalah hasil kajian kami selama ini. Kita mencari benang merah dan akhirnya ada solusi yang kami namakan 7 cita-cita perubahan. Kami ingin menyatakan kepada publik bahwa gerakan mahasiswa tidak sekedar aksi reaktif, hanya bertumpu pada isu tertentu. Kita ingin punya gagasan dan kami share kepada publik,” kata Ton Abdillah dari Ikatan Mahasiswa Muhammadiyah (IMM) dalam acara Konsolidasi Pelajar dan Mahasiswa Indonesia "Perubahan Sudah Tidak Bisa Ditunda" dan launching "7 Cita-Cita Perubahan" di Jakarta, Senin 18 April 2011.

Jika memang ketika perubahan yang harus segera dipercepat ini memerlukan pergantian rezim (SBY-Boediono) maka secepatnya harus kita rumuskan metodenya, tidak perlu menunggu 2014, lanjut M.Chozin ketum HMI MPO.(ian)

* * *

 Konsolidasi HMI, GMNI,PII,IMM, PMKRI,GMKI,

Hindu,Budha dan Kaum Muda

Senin, 18 Apr 2011

JAkARTA-Konsolidasi pelajar, pemuda dan mahasiswa karena perubahan tak
bisa lagi ditunda, berlangsung Senin di Jakarta. SBY-Boediono dinilai
gagal, dan rakyat makin marginal. Feodalisme dan neoliberalisme makin
kental.

Aksi konsolidasi yang digelar mahasiswa dan pelajar di Jakarta Senin
tadi berakhir dengan simpulan bahwa aksi mendesak perubahan akan terus
dilakukan bersama semua elemen perubahan. DPP IMM, PMKRI, GMKI, PB PII,
PB HMI, KMHDI, GMNI, LMNB, mahasiswa Budha, Hindu, Dan semua elemen
gerakan mengutuk kegagalan Negara dan pemerintah mengatasi kemiskinan dan
ketidakadilan.

Mereka menggelar aksi anti kebohongan. ‘’Rezim SBY-Boed lembek, korup
dan bohong. Aparat bertindak represif dan bengis, suatu kutukan sejarah
yang terus berulang,'' kata Ton Abdillah Haz, Ktua Umum IMM.

Pekan lalu, seratusan orang telah berkumpul di dalam ruangan yang tidak
begitu besar. Mereka sebagian besar adalah perwakilan berbagai
organisasi dan komite aksi yang ada di Jakarta.

Masinton Pasaribu, Ketua Umum Repdem dan sekaligus tuan rumah pertemuan
ini, membuka pertemuan dengan sedikit kata sambutan. “Ini merupakan
pertemuan awal untuk menggagas sebuah kongres pemuda Indonesia,”
katanya.

Karena rencana hajatan kongres pemuda itulah Masinton dan sejumlah
inisiator mengundang berbagai organisasi untuk berkumpul. Sekitar 30-an
organisasi terdaftar di absensi panitia. Lagu kebangsaan “Indonesia
Raya” pun berkumandang saat pertemuan ini baru dimulai.

Agus Jabo, ketua Umum Partai Rakyat Demokratik, menjelaskan soal kenapa
persatuan sangat diperlukan saat ini. “Sekarang ini, kita berhadapan
dengan apa yang disebut imperialisme dan neo-kolonialisme. Alat untuk
melawan imperialisme dan nekolim itu adalah persatuan nasional,”
tegasnya.

Inisiator lain dari pertemuan ini adalah Haris Rusli Moti, aktivis
Petisi-28. Ia menuturkan bahwa semangat komunike Bandung 1955 dapat
dijadikan sebagai landasan persatuan pemuda ini. “Dalam komunike Bandung
1955, sudah jelas bahwa Indonesia berwatak anti-nekolim dan
anti-imperialisme.”

Merumuskan Persoalan

Seorang aktivis perempuan dengan bersemangat mengabarkan soal rencana
pertemuan di kalangan organisasi perempuan. Meski banyak berbicara soal
perempuan, ia tetap menyatakan dukungan terhadap rencana kongres pemuda
ini.

Beberapa saat kemudian, Agus Jabo menjelaskan panjang lebar mengenai
kesesuaian antara ciri-ciri imperialisme yang pernah disebutkan Bung
Karno dalam pidato “Indonesia Menggugat” dengan praktek kebijakan
pemerintahan SBY-Budiono saat ini.

Keempat ciri itu adalah sebagai berikut: : (1) Indonesia tetap menjadi
negeri pengambilan bekal hidup, (2) Indonesia menjadi negeri pengambilan
bekal-bekal untuk pabrik-pabrik di eropa, (3) Indonesia menjadi negeri
pasar penjualan barang-barang hasil dari macam2 industri asing, (4)
Indonesia menjadi lapang usaha bagi modal yang ratusan-ribuan-jutaan
rupiah jumlahnya.

Keempat ciri itu, kata Agus Jabo, sudah sesuaian dengan apa yang
dijalankan oleh SBY-Budiono sekarang ini. “Dengan fakta ini, maka kita
tidak ragu lagi untuk menyebut SBY-Budiono sebagai agen nekolim dan
imperialisme,” katanya.


* * *

Alumni HMI: Indonesia Berada dalam Genggaman Mafia


Kamis, 21 April 2011
RMOL. Indonesia dalam genggaman mafia. Betapa tidak, praktek mafia menjadi pemandangan nyata sehari-hari. Praktek dilakukan dari hulu hingga hilir, tanpa terkecuali pemegang amanah kekuasaan rakyat.

Hal itu dikatakan Ta’zim Jurubicara Gerakan Alumni Himpunan Mahasiswa Isalam (GAHMI) dalam keterangan pers yang diterima redaksi, Rabu malam (20/4).

“Kalau dahulu ada istilah mafia Istana Cendana, sekarang mafia Istana Cikeas," kata Ta'zim.

Apalagi, Wikilieks kemudian melansir kawat diplomatik tentang praktek mafia yang melibatkan keluarga SBY.

"Istana Cikeas menjadi tempat transaksi segelintir kepentingan utk membangun konspirasi merampok Negara," lanjutnya.

Oleh karenanya, Ta'zim mendesak KPK untuk membuka skandal korupsi yang melibatkan Istana Cikeas. GAHMI juga meminta revisi Undang-undang pemberantasan korupsi ditolak, sebab mengkebiri pemberantasan korupsi.

"Jika tidak juga dilakukan, maka SBY-Boediono harus turun," tegasnya.

Sebelumnya, harian The Age mengutip tulisan WikiLeaks soal kebocoran kawat diplomatik Kedubes AS di Jakarta. Dalam tulisan pada tanggal 11 Maret 2011 itu, The Age membeberkan dosa keluarga Presiden Susilo Bambang Yudhyono yang dinilai menyalahi kewenangannya. SBY dituding terlibat kasus korupsi dan penyalahgunaan kekuasaan. Dalam kawat yang tidak diketahui nomor dokumennya itu, SBY dikatakan secara personal terlibat untuk mempengaruhi jaksa penuntut dan hakim untuk melindungi sejumlah tokoh politik
