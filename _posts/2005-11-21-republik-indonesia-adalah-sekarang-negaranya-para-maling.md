---
layout: post
title: Republik Indonesia adalah sekarang negaranya para maling
date: 2005-11-21
---

Barangkali, bagi banyak orang, pernyataan Presiden SBY bahwa Indonesia memerlukan waktu sekitar 15 tahun untuk mewujudkan kehidupan masyarakat yang benar-benar bersih dari praktik korupsi (Media Indonesia, 19 November 2005) adalah bukti yang gamblang sekali bahwa korupsi betul-betul sudah merupakan penyakit amat parah bagi negara dan bangsa. Keterlaluan, 15 tahun lagi korupsi baru bisa dihabisi. Dan itu pun masih belum tentu ! Jadi, berapa banyak lagi uang rakyat yang akan dicuri oleh para maling atau penjahat itu?

Pernyataan tersebut, yang disampaikan Presiden di hadapan sekitar 500 warga masyarakat Indonesia di Busan, Korea Selatan, menggarisbawahi ucapannya beberapa bulan yang lalu, yang berbunyi : « Alangkah malunya bila Indonesia sebagai bangsa yang mayoritas beragama Islam dan merupakan negara muslim terbesar di dunia tetapi angka korupsinya juga tertinggi di dunia » (Jawapos 4 Juli 2005)

Presiden mengatakan saat ini pemerintah benar-benar serius dalam memberantas praktik-praktik korupsi dan sudah banyak pejabat seperti gubernur, bupati, maupun walikota, dan anggota DPR yang diperiksa sehubungan dengan dugaan tindak pidana korupsi (masih menurut Media Indonesia, 19 November 2005)

## Koruptor Adalah Penjahat Dan Pengkhianat

Penyataan Presiden bahwa korupsi baru bisa dihabisi 15 tahun, dan juga bahwa kita malu sebagai negara yang penduduk muslimnya terbesar di dunia tetapi angka korupsinya juga tertinggi di dunia, bisa menyebabkan banyak orang jadi bingung dan bertanya-tanya : Apakah penyebab utama maka di negara kita terjadi korupsi yang begitu parah? Apakah agama Islam sama sekali sudah tidak bisa diandalkan untuk mencegah korupsi? Apa sajakah yang harus ditempuh untuk bisa menghabisi korupsi? Apakah korupsi memang tidak mungkin diberantas? Apakah bangsa Indonesia memang punya kecenderungan untuk korup? Apakah memang korupsi adalah termasuk kelemahan yang normal bagi setiap manusia? Apakah “ketidakcukupan” bukannya pendorong utama untuk korupsi?

Apa pun pertanyaan yang diajukan dan apa pun pula jawaban yang bisa diberikan, satu hal yang sudah jelas bagi semua orang yalah bahwa negara kita sudah keterlaluan busuknya karena penyakit parah yang bernama korupsi ini. Bahkan, ada orang yang mengatakan - dengan kesal dan geram - bahwa Republik Indonesia sekarang ini negara maling. Mungkin saja, didengar sepintas lalu, ungkapan yang demkian itu terasa agak keterlaluan atau “kebablasan”. Tetapi, kalau diingat betapa sangat banyaknya korupsi yang sudah merajalela secara ganas atau melanda secara besar-beasaran di negeri kita, maka adalah pantas sekali kiranya kalau orang mengatakan bahwa Republik Indonesia adalah sekarang ini negaranya para maling, artinya, negaranya para penjahat.

Supaya lebih jelas dalam pemikiran kita semua, patutlah ditegaskan bahwa kata “korupsi” adalah dalam bahasa lainnya MALING, dan adalah kejahatan. Koruptor adalah dalam bahasa lugunya PENJAHAT. Karenanya, boleh dikatakan bahwa negara kita sekarang ini pada hakekatnya dikuasai oleh banyak penjahat. Penjahat-penjahat ini menduduki jabatan-jabatan penting dan terhormat di (antara lain!) : Mahkamah Agung, di Sekretariat Negara, di Kementerian Agama, di Kejaksan Agung dan Kejaksaan Tinggi, di DPR dan DPRD, di Mabes Polri dan di Mabes TNI, di banyak kantor Gubernur dan Bupati, di bank-bank dan BUMN, dan instansi-instansi atau badan-badan pemerintahan sipil dan militer lainnya.

Pada hakekatnya, para penjahat besar atau maling besar (atau koruptor besar itu) semunya adalah PENGKHIANAT besar rakyat dan negara. Tegasnya, dan jelasnya, mereka adalah juga pengkhianat ummat.

## Kasus Besar Probosutedjo

Sejak beberapa waktu yang lalu kita semua sudah disuguhi oleh media pers (dan televisi) Indonesia sebuah cerita bersambung sekitar kasus besar Probosutedjo, adik tiri mantan presiden Suharto, yang berkaitan dengan korupsi hutan tanaman industri di Kalimantan Selatan yang merugikan negara Rp 100,9 miliar. Cerita bersambung ini menarik dan sekaligus memuakkan atau membikin kita geleng kepala, karena menyangkut uang suapan yang jumlahnya amat besar dan juga menyeret nama orang-orang besar. Menurut Probosutedjo sendiri ia sudah menghabiskan uang suapan sebanyak Rp 16 miliar untuk “mengurusi” perkaranya (Jawapos, 12 Oktober 2005). Di antara nama-nama yang disebut-sebut tersangkut dalam kasus besar ini terdapat Ketua Mahkamah Agung Bagir Manan. Sejumlah hakim agung dari mahkamah yang tertinggi di negara ini telah diperiksa atau ditindak.

Pada 30 September lalu KPK telah menangkap Harini Wiyoso (pengacara Probosutedjo) dan lima pegawai MA. Dari keenam tersangka kasus suap tersebut, KPK menyita uang US$ 400.000 dan Rp 800 juta. Uang itu diberikan Harini untuk mengurus perkara di tingkat kasasi MA.
Di tingkat pertama Probosutedjo dihukum empat tahun penjara, namun pengadilan tingkat banding memvonis lebih ringan dua tahun penjara. Di tingkat kasasi, perkara korupsi Probosutedjo itu ditangani tiga hakim agung, Bagir Manan, Parman Suparman dan Usman Karim. (Suara Pembaruan, 14 November 2005)

Terlepas dari persoalan bagaimana selanjutnya kasus besar Probosutedjo ini nantinya akan berakhir, tetapi perkara ini telah menunjukkan sebagian wajah yang amat buruk dan praktek-praktek busuk yang telah menjangkiti – sejak lama sekali ! – dunia hukum dan peradilan negara kita. Kasus ini juga telah menunjukkan betapa besar kekuasaan uang (harap catat: uang haram!) dalam pengambilan keputusan hukum. Pengacara Probosutedjo mengklaim bahwa uang sebesar Rp 6,5 miliar telah dibagi-bagikan kepada hakim !

## Korupsi Di Sekretariat Negara

Hal lain yang juga menunjukkan bahwa korupsi telah berkecamuk sejak lama di kalangan atas adalah tersiarnya berita bahwa Tim Koordinasi Pemberantasan Tindak Pidana Korupsi (Timtas Tipikor) memutuskan mendatangi Kantor Badan Pemeriksa Keuangan (BPK) untuk mengusut dugaan korupsi di Sekretariat Negara (Setneg).Langkah itu dilakukan setelah Timtas Tipikor menunggu hasil audit BPK tentang korupsi di Setneg, tetapi tak kunjung datang.

Dari hasil sementara pengusutan korupsi di Setneg, ditemukan sejumlah petunjuk adanya tindak pidana korupsi. Antara lain soal pengelolaan lahan di Gelora Bung Karno dan Kemayoran yang menyalahi prosedur. Misalnya, ada pengalihan lahan menjadi hak milik seseorang. Selain itu, ditemukan pula adanya penyelewengan dana penyelenggaraan Konferensi Asia Afrika (KAA) ke-50 di Bandung 2004.

Pada September silam, audit BPK tentang dugaan korupsi di Setneg menyangkut tiga komponen. Yakni yang berkaitan dengan pengelolaan aset, pengelolaan APBN, dan pengelolaan dana rekanan Setneg. Belum ada kepastian mengenai besar kerugian negara yang ditimbulkan oleh penyelewengan penggunaan dana di lembaga itu.

Tetapi, Timtas Tipikor telah memperolah data sementara mengenai dugaan penyelewengan dana penyelenggaraan KAA ke-50 di Bandung mencapai lebih dari Rp500 juta. (Media Indonesia, 17 November 2005).

Kita tunggu saja kelanjutan pengusutan korupsi di Setneg ini. Seperti diketahui oleh banyak orang, pada masa-masa jayanya Suharto, kekuasaan Setneg adalah besar sekali, dan karenanya, penyalahgunaannya juga luar biasa besar sekali. Mabes ABRI dan Setneg (dan Jalan Cendana) adalah pusat syaraf rejim militer Orde Baru. Dan itu berlangsung selama 32 tahun. Jadi, tidak bisa dibayangkan lagi berapa jumlah uang yang telah diselewengkan lewat Setneg ini, yang memungkinkan banyak jenderal dan tokoh-tokoh Golkar ( !!!) untuk menumpuk kekayaan haram dengan cara-cara najis.

## Orde Baru Perusak Moral Bangsa

Kalau kita teliti merajalelanya korupsi yang melanda banyak sekali bidang dan badan di negara kita, maka kita akan melihat dengan jelas bahwa faktor utama penyebabnya adalah, sekali lagi, kemerosotan moral atau kebejatan iman. Terutama sekali kemerosotan moral atau kebejatan iman (atau pembusukan akhlak) di kalangan “atas” , atau di kalangan tokoh-tokoh pemerintahan, pimpinan partai politik, tokoh-tokoh agama dan masyarakat.

Dan kemerosotan moral, atau kebejatan iman, atau pembusukan akhlak ini adalah akibat atau produk rejim militer Orde Baru. Rejim militer yang dikendalikan secara ketat oleh TNI-AD dan Golkar ini selama lebih dari 32 tahun telah merusak secara besar-besaran moral bangsa dalam banyak bidang. Sekarang dapat dilihat dengan jelas (karena buktinya dapat disaksikan di mana-mana dewasa ini) bahwa, pada dasarnya, rejim militer Suharto dkk tidak mendidik orang banyak untuk bermoral baik. Rejim militer Orde Baru telah merusak moral banyak orang, terutama di kalangan tokoh-tokoh pendukungnya.

Banyak sekali tokoh-tokoh Orde Baru (terutama pimpinan TNI-AD dan Golkar) telah menunjukkan contoh-contoh yang buruk atau negatif sekali bagi bangsa. Bukan hanya dalam masalah Hak Asasi Manusia (ingat, antara lain, pembantaian besar-besaran tahun 65), dan kehidupan demokratis, dan toleransi saja mereka telah memberikan contoh yang jelek, melainkan juga dalam masalah korupsi. Yang dipertontonkan oleh keluarga Suharto, sebagai tokoh besar Orde Baru, dan pimpinan utama Golkar ( dan pemimpin tertinggi ABRI) dalam menumpuk kekayaan haram dengan menyalahgunakan atau menyelewengkan kekuasaan, adalah contoh buruk sekali bagi bangsa seluruhnya.

## Perlukah Revolusi Rakyat ?

Korupsi ada hubungannya yang erat sekali dengan masalah moral. Biasanya, sistem pemerintahan atau sistem politik yang baik hanya bisa diciptakan oleh - dan dengan – orang-orang yang bermoral baik pula. Rejim militer Orde Barunya Suharto dkk selama 32 tahun lebih telah secara parah sekali mengabaikan pendidikan dan pemupukan moral bangsa ini. Sebagai akibatnya : semangat gotong royong ditinggalkan, kejujuran dalam bekerja menjadi hilang, rasa pengabdian kepada kepentingan umum menjadi barang aneh, berbakti kepada kepentingan rakyat menjadi tertawaan orang, amanat penderitaan rakyat jadi slogan kosong saja, bekerja secara tulus menjadi cemoohan banyak orang, hidup sederhana dan bersih dari korupsi malahan dianggap sinting atau aneh.

Melihat banyaknya kasus korupsi yang sudah menyerang begitu parah bidang eksekutif, legislatif dan judikatif ini, maka timbul pertanyaan : apakah korupsi bisa dibrantas? Apa bisa 15 tahun lagi, seperti yang dibayangkan oleh Presiden SBY? Mungkin, jawabannya yang paling masuk akal adalah : sulit sekali!

Sebab kerusakan moral dan kebejatan iman yang ditimbulkan oleh rejim militer-nya Suharto dkk selama lebih dari 32 tahun sudah terlalu luas dan sudah terlalu parah pula. Sekarang ini, boleh dikatakan bahwa seluruh birokrasi pemerintahan dan berbagai lembaga sipil dan militer adalah produk Orde Baru. Tidak banyak - atau belum banyak - di antara mereka yang benar-benar tersentuh oleh reformasi.

Dari orang-orang produk Orde Baru ini, terutama dari mereka yang masih terus mendukung berbagai pola berfikir rejim militer Suharto dkk (meskipun bekerja di bawah SBY atau presiden lainnya), tidak bisa diharapkan sikap yang betul-betul mementingkan kepentingan rakyat dan juga sikap yang benar-benar anti-korupsi.

Oleh sebab itu, ada orang-orang yang sudah begitu pesimisnya tentang pembrantasan korupsi ini, sehingga menurut mereka hanya bisa ditempuh lewat revolusi rakyat. Menurut mereka, segala kejahatan, segala kebusukan, dan segala keburukan yang kita saksikan dewasa ini, yang sebagian besar adalah produk Orde Baru, tidak mungkin - atau akan lama sekali - bisa dihilangkan dengan cara apa pun, dan oleh siapa pun, kecuali oleh revolusi rakyat.

Kepada orang-orang yang berpendapat demikian, bisa saja dikatakan : mungkin, mungkin, kalian benar. Tetapi sabar-sabarlah, dan jangan buru-buru atau jangan keburu-nafsu, sebab jalan masih panjang..............!
