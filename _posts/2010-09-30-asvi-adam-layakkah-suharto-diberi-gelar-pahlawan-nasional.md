---
layout: post
title: 'Asvi Adam : layakkah Suharto diberi gelar pahlawan nasional ?'
date: 2010-09-30
---

Dalam rangka mengenang peristiwa 30 September 1965 akhir-akhir ini banyak sekali hal-hal menarik yang sudah dilakukan oleh berbagai ragam kalangan, baik di Indonesia maupun di luar negeri. Di dalam negeri banyak tulisan sudah muncul di suratkabar dan majalah-majalah, dan TV pun menyajikan tayangan-tayangan yang berkaitan dengan peristiwa yang terjadi 45 tahun yang lalu itu.

 Di Jakarta diselenggarakan Pekan Melawan Lupa, yang merupakan serentetan berbagai kegiatan yang penting-penting dalam rangka kampanye strategis yang massif atas berbagai pelanggaran HAM berat yang terjadi di Indonesia pada masa lalu.

Pekan Melawan Lupa ini diselenggarakan dengan partisipasi banyak organisasi, dan diisi dengan berbagai acara yang bervariasi dan sarat dengan isi tentang politik dan banyak pelanggaran HAM, yang dilakukan terutama sekali oleh Orde Baru. (Berhubung pentingnya Pekan Melawan Lupa ini, akan diusahakan adanya berita dan tulisan-tulisan tersendiri di kemudian hari).

Dalam rangka mengenang 30  peristiwa 30 September 1965 berikut di bawah ini disajikan kutipan tulisan  sejarawan dan ahli peneliti utama LIPI, Asvi Warman Adam, yang disiarkan dalam Tempo 2 Oktober 2010.

Dalam tulisannya ini ia dengan tajam sekali mempersoalkan kemungkinannya Suharto dicalonkan untuk mendapat gelar pahlawan nasional.  Dengan sinis, tetapi tepat sekali, ia menulis apakah seseorang dapat dianggap « menyelamatkan bangsa » bila korban yang jatuh demikian luar biasa jumlahnya itu.

Ia melihat dari komposisi Dewan Gelar dan Tanda Jasa yang dibentuk oleh Kementerian Sosial dan diurus oleh Sekretaris Negara terdiri dari orang-orang yang majoritasnya dekat dengan kelompok Suharto. Dewan yang terdiri dari 7 orang inilah yang akan menilai siapa-siapa sajakah di antara calon-calon itu pantas diberi gelar pahlawan nasional.

Mengingat komposisi Dewan Gelar dan Tanda Jasa yang kebanyakan adalah orang-orang yang pernah mengabdi kepada Orde Baru, «, maka bisa diprediksi pemberian gelar  pahlawan nasional kepada Jenderal Suharto tinggal selangkah lagi » tulis Asvi Warman Adam.
« Pertanyaannya apakah Suharto layak ? Gelar pahlawan nasional merupakan gelar tertinggi di negara ini yang abadi (dalam prakteknya tidak pernah dicabut). » tandasnya.

Inilah yang gawat bagi bangsa dan anak-cucu kita di kemudian hari !!! Suharto yang dosa-dosanya sarat dengan darah dan nyawa jutaan korban  yang tidak bersalah, dan terkenal di dalam negeri dan luarnegeri sebagai pelanggar berat HAM dan koruptor raksasa di sepanjang sejarah bangsa kita, akan dijadikan pahlawan nasional. Sungguh-sungguh keterlaluan.

Kalau idee yang sinting ini menjadi kenyataan lalu apakah para « tokoh » yang menjadi anggota Dewan Gelar dan Tanda Jasa  itu bisa disebutkan « tokoh-tokoh » yang berjiwa waras ? Gagasan yang hanya  akan membikin sakit hatinya banyak korban Orde Baru ini, dan juga membikin marahnya orang-orang lainnya  yang mempunyai rasa keadilan dan perikemanusiaan ini, harus dilawan sekeras-kerasnya oleh sebanyak mungkin kalangan dalam masyarakat.

Para anggota Dewan Gelar dan Tanda Jasa yang memutuskan untuk memberi gelar Pahlawan Nasional kepada Suharto itu akan dikenang dan dihujat oleh banyak orang, termasuk generasi bangsa di kemudian hari, sebagai orang-orang yang mengotori sejarah bangsa untuk selama-lamanya.

Sebab, jelaslah bahwa semua kejahatan besar Suharto di bidang HAM  selama 32  tahun, dan segala tindakannya yang membuktikan dosa-dosa beratnya  lewat KKN (korupsi, kolusi dan nepotisme) membuatnya  pantas sekali disebutkan sebagai sampah bangsa, dan bukannya sebagai pahlawan nasional.

Umar Said
= = =



Diambil dari Tempo, 2 Oktober 2010

SUHARTO, G30S & PAHLAWAN NASIONAL



Oleh Asvi Warman Adam

Setahun sebelum lengser, tahun 1997, Suharto sempat mengangkat tiga orang
jenderal termasuk dirinya sendirinya menjadi jenderal besar (bintang lima).
Sudirman berjasa sebagai bapak perang kemerdekaan, Nasution peletak dwifungsi
ABRI, sedangkan Suharto “berhasilmenyelamatkan negara dan bangsa dari
ancaman G30S/PKI”.

Setelah tanggal 1 Oktober 1965
dalam tempo beberapa bulan terjadi pembunuhan massal yang mencapai 500.000
jiwa. Jumlah sebanyak sebanyak itu tidak mungkin hanya disebabkan oleh konflik
horizontal di tengah masyarakat. Yang jelas semuanya diawali dengan operasi
militer. Lantas siapa yang memerintahkan ?

Apakah seseorang dapat dianggap menyelamatkan bangsa bila korban yang
jatuh demikian luar biasa jumlahnya ? Presiden Sukarno marah sekali mendengar
berita ini, ia memerintahkan supaya pembantaian ini dihentikan. Tetapi siapa
yang mendengar suara seorang pemimpin tua yang sudah digerus kekuasaannya.
Kalau bukan Sukarno lalu siapa yang bertanggungjawab terhadap tragedi nasional ini ?

“Kudeta merangkak” yang merupakan uraian post factum terhadap rangkaian
peristiwa sejak meletus G30S sampai keluarnya Supersemar (Surat Perintah 11
Maret) 1966 bertentangan dengan klausul “menyelamatkan negara dan bangsa”.
Karena ini jelas kepentingan untuk meraih kekuasaan tertinggi, walaupun itu
dikerjakan secara seksama dengan penuh perhitungan.

Tulisan ini tidak menggugat gelar Jenderal Besar yang telah dianugerahkan Suharto
kepada dirinya sendiri. Itu einmalig dan bintang itu silakan dibawa
ke tempat peristirahatan yang terakhir di puncak sebuah bukit. Yang menjadi
persoalan adalah ketika nama Suharto lolos seleksi administratif pemberian
gelar pahlawan nasional. Dari 30 orang calon, 19 lolos termasuk putra Kemusuk
ini. Sebanyak 13 orang anggota tim yang dibentuk oleh Departemen Sosial termasuk
sejarawan dari Pusat Sejarah TNI akan mengkaji. Selepas ini berkasnya
dikirimkan kepada sekretariat Negara dan dinilai oleh Dewan Gelar dan Tanda Jasa.

Dewan ini terdiri dari tujuh orang yakni Menkopolkam Djoko Sujanto selaku ketua,
Hayono Sujono sebagai wakil ketua, dengan anggota TB Silalahi, Juwono
Sudarsono, Quraish Shihab, Edi Sedyawati dan Jimly Asshidiqie. Dari tujuh
anggota dewan, mayoritas sebanyak empat orang pernah menjadi duduk di kabinet
sebagai pembantu Presiden Suharto. Hajono Sujono pernah menjadi Menko Kesra
semasa Orde Baru dan sampai sekarang masih memimpin sebuah yayasan yang
didirikan Suharto. Melihat kondisi di atas, maka bisa diprediksi pemberian gelar
pahlawan nasional kepada Jenderal Suharto tinggal selangkah lagi.

Pertanyaannya apakah Suharto
layak ? Gelar pahlawan nasional merupakan gelar tertinggi di negara ini yang abadi
(dalam prakteknya tidak pernah dicabut). Tokoh yang berjasa sangat besar dalam
membela bangsa dan negara, mengorbankan jiwa raganya demi tanah air serta
menjadi contoh teladan bagi masyarakat, berhak memperoleh status terhormat itu.

Suharto memang berkuasa sangat
lama, karena itu ia sempat membangun secara fisik negara ini. Entah berapa
gedung yang telah dibuat, berapa panjang
jalan yang dibangun, terlepas dari berapa persen proyek itu dikorupsi, namun
hasil pembangunan itu memang kongkret. Namun berapa pula kerugian yang telah
diderita masyarakat yang tergusur, dirampas tanahnya atau dibeli paksa dengan
murah, demi pembangunan.

Politik regional memang berhasil,
ASEAN adalah model kerjasama wilayah yang dapat meredam konflik dan memberi
peluang kepada masing-masing negara untuk menyelenggarakan pemerintahan dengan aman.
Walaupun rasa “aman” justeru diperoleh dengan melakukan represi terhadap
kekuatan kritis yang ada di tengah rakyat. Kebebasan pers dipasung dan hak
asasi manusia dilanggar. Mengenai yang terakhir ini sejarah mencatat setelah
pembantaian massal pasca G30S, terjadi pembuangan paksa ke pulau Buru yang
memakan korban lebih dari 10 ribu selama sekitar 10 tahun (1969-1979), pembunuhan
misterius tahun 1980-an, kasus Tanjung Priok, Talangsari Lampung, penyerbuan
kantor PDIP di Jalan Diponegero tanggal 27 Juli 1996, belum lagi operasi
militer yang terjadi di Aceh, Papua (dan Timor Timur sebelum lepas dari
pangkuan ibu pertiwi). Semuanya terjadi ketika berjalan rezim yang dipimpin
Jenderal Suharto.

Sang proklamator Sukarno diangkat sebagai pahlawan nasional tahun 1986,
16 tahun setelah wafat tahun 1970. Suharto meninggal tahun 2008. Apakah ia perlu
segera dinobatkan menjadi pahlawan nasional ? Kalau pengadilan HAM berat tidak
dapat lagi dilakukan, maka minimal Komisi Kebenaran dan Rekonsiliasi harus
bekerja. Proses ini berjalan lamban karena ketidakseriusan pemerintah dan DPR.
Akhirnya Undang-Undang KKR yang sudah jadi itu pun dirubuhkan oleh Mahkamah
Konstitusi semasa dipimpin Jimly Asshidiqie. Kini RUU itu sudah dibuat kembali
oleh Kementerian Hukum dan HAM dan diserahkan kepada Sekretariat Negara.

Hendaknya pemerintah tidak menunda lagi dan segera meneruskan kepada DPR agar
diproses kembali menjadi Undang-Undang. Sebuah perjalanan yang panjang dan
teramat panjang bagi sebuah pengungkapan kebenaran. Menurut hemat saya,
pemberian gelar kepada Suharto seyogianya baru diproses setelah dilakukan
upaya pencarian kebenaran oleh KKR. Sebab sekali terlanjur menghadiahi
seseorang gelar pahlawan nasional itu tidak bisa dicabut. Ironis bila pahlawan nasional
adalah sekaligus pelaku pelanggaran HAM Berat. (Dr Asvi Warman Adam,
ahli peneliti utama LIPI)
