---
layout: post
title: Harap baca Google tentang Tommy Soeharto
date: 2009-08-24
---

Mohon para pembaca untuk menyimak berita dari Kompas tentang Tommy Suharto seperti di bawah ini, di samping juga diharapkan sekali membaca kembali kumpulan berita mengenai persoalan yang sama yang tercantum dalam rubrik “Aneka berita utama terkini” dalam website  http://umarsaid.free.fr
Dari berita-berita itu semuanya makin jelaslah bagi kita semua bahwa Tommy Soeharto memang sedang mempersiapkan diri untuk berusaha menjadi Ketua Umum Partai Golkar dalam Munas Oktober yang akan datang di Riau, untuk kemudian berusaha merebut kedudukan Presiden/Kepala Negara dalam tahun 2014.


Berita dalam Kompas (22/8/09) tersebut singkatnya adalah sebagai berikut :
“Keinginan putra mantan Presiden Soeharto, Hutomo Mandala Putra atau Tommy Soeharto untuk menggantikan Jusuf Kalla sebagai Ketua Umum Golkar, ternyata  sudah dengan persiapan matang. Salah satu orang dekat Tommy Soeharto, Yusysafri Syafei  menyatakan posisi Ketum Golkar adalah target awal untuk mencapai target utama, menjadi calon Presiden  di tahun 2014 mendatang. Kini, Tommy Soeharto, diakuinya sedang melakukan operasi senyap, menggalang dukungan di berbagai daerah untuk bersaing sehat bersama para kandidat ketum Golkar lain, termasuk Aburizal Bakrie, atau Ical.

Tommy, ujarnya, sudah mempersiapkan tim pemenangan yang memang tidak diungkap kepada publik. Ia memberikan alasan, hal ini karena  masing-masing kandidat memiliki cara serta taktik sendiri-sendiri dalam bersaing sehat di Munas Golkar 4-7 Oktober mendatang di Pekanbaru, Riau.   

"Mas Tommy  sebagai warga negara ingin berkiprah di dunia politik melalui partai politik. Yaitu, ingin merebut kursi Golkar 1.  Soal dukungan,  berarti harus ada pernyataan di atas kertas, berupa surat dukungan. Mengenai ini, memang sudah masuk beberapa surat dukungan dari beberapa daerah.
Ingat, Mas Tommy punya team work dibidang bisnis yang sudah running well," Syafei menandaskan.

Syafei kemudian menjelaskan merebut kursi Golkar 1 adalah keinginan Tommy Soeharto awal saja. Ke depannya, Tommy Soeharto diakuinya memang ingin menjadi salah satu calon presiden di 2014 nanti, bisa menjadi pengisi Istana Negara.

"Apapun ceritanya, Tommy adalah anak Soeharto. Yang jenius di dalam strategi yang secara genetik diturunkan kepada Tommy Soeharto. Jadi, target maksimalnya adalah menuju Istana, bertarung menjadi capres di 2014. Memperebutkan kursi Golkar 1, adalah target jangka pendeknya. Istilah orang Minang, bajanjang naik, batangga turun," ungkapnya lagi. (Kutipan yang disingkat selesai).


Persoalan Tommy adalah masalah besar dan serius


Persoalan Tommy Soeharto ingin merebut kedudukan Ketua Umum Golkar adalah persoalan besar dan serius, yang pasti akan banyak sekali dibicarakan, ditulis, atau dianalisa oleh berbagai  kalangan. Apalagi (!), ia ingin juga  jadi presiden atau kepala negara RI di tahun 2014. Ini adalah persoalan besar yang gawat sekali (!!!), yang menandakan bahwa bangsa dan negara kita akan  menjurus ke ”jaman édan”.


Terbetiknya berita itu saja sudah menimbulkan reaksi yang macam-macam, seperti yang bisa disimak oleh siapa saja dalam kumpulan berita di website tersebut di atas. Dengan makin dekatnya Munas Golkar, maka akan makin sangat ramai jugalah persoalan Tommy Soeharto. Sebab, keinginan Tommy menjadi ketua Golkar (apalagi jadi presiden !) adalah bukti bahwa ia tidak bercermin diri, dan mengira bahwa banyak orang masih bisa ditipu dengan macam-macam cara dan jalan, atau bahwa ia selalu bisa membagi-bagikan  uang (yang haram itu !!!) untuk melakukan terus-menerus banyak kejahatan di berbagai bidang.


Dengan mencalonkan diri sebagai Ketua Umum Golkar dan juga menginginkan jadi Presiden RI, Tommy mengira bahwa banyak orang sudah lupa bahwa dalam masa yang lalu seluruh koran, majalah,  televisi Indonesia pernah menyoroti berbagai peristiwa kejahatan yang berkaitan dengannya. Sejarah Tommy Soeharto sudah terlalu banyak dikotori oleh segala hal yang berbau busuk, dan mengandung najis, sebagai sikap moral yang tidak luhur.


Tommy ingin jadi presiden : sungguh keterlaluan (!!!!!)


Bahwa Tommy ingin terjun dalam dunia politik itu adalah haknya yang sepenuhnya. Juga, bahwa ia ingin jadi ketua umum Golkar adalah satu hal yang bisa dimengerti. Sebab, Golkar adalah alat golongan militer yang pernah dibesarkan oleh bapaknya Tommy selama lebih dari 30 tahun.. Golkar adalah alat pemukul dan penindas selama pemerintahan Orde Baru, bersama-sama golongan militer, dalam melindungi kepentingan-kepentingan Suharto dan konco-konconya. Jadi, keinginan Tommy untuk membangun kembali Golkar (yang sudah mulai ambruk sekarang ini) adalah sesuai dengan  jati dirinya; karena Golkar adalah habitatnya
Namun, bahwa ia menginginkan jadi presiden kepala negara RI dalam tahun 2014  adalah betul-betul keterlaluan !!!!! (lima tanda seru). Mungkin ada orang yang menganggapnya kelewatan  “tidak tahu diri” sehingga bisa disangsikan kesehatan jiwanya atau dipertanyakan “kenormalan” cara berfikirnya.


Tommy Soeharto mungkin memang banyak bakatnya (termasuk “bakatnya” yang lihay  dalam hal-hal yang remeng-remeng, abu-abu atau atau bahkan gelap – di bidang “bisnis” maupun hal-hal lain yang berkaitan dengan urusan pribadi - yang tidak perlu dan tidak patut dibongkar, karena menyinggung soal moral pribadinya, yang tidaklah bisa dikatakan tinggi atau mulia).


Untuk tidak terjerumus dalam gossip yang bukan-bukan, atau penilaian yang sembarangan, atau pendapat yang awur-awuran, kita perlu melihat kasus  Tommy Soeharto –sebisa mungkin – dari banyak segi atau sudut pandang yang berbeda-beda.  Makin banyak informasi mengenainya, baik yang negatif maupun yang positif, sangatlah diperlukan oleh kita semua untuk mengetahui apa sajakah dan siapakah sebenarnya itu Tommy Soeharto, yang ingin menjadi presiden RI melalui kedudukannya sebagai ketua umum Golkar.


Isi Google tentang Tommy Soeharto


Salah satu di antara banyak cara untuk mendapat informasi bagi pertimbangan kita, yang relatif mudah, murah, cepat, dan bersegi banyak  adalah dengan membaca bahan-bahan yang disajikan Google lewat Internet. Dengan membaca bahan-bahan mengenai Tommmy Soeharto yang bisa didapat dengan Google, maka kita akan mendapat gambaran sekadarnya apakah Tommy Soeharto memang pantas untuk menjadi Ketua Umum Golkar dan kemudian juga patut menjadi presiden kepala negara Republik Indonesia kita..


Setelah membaca Google (dalam versi bahasa Indonesia) semua orang akan dapat menyimpulkan sendiri bahwa Tommy Soeharto memang “hebat”sekali, karena pernah berurusan dalam masalah-masalah yang pelik, rumit, ruwet, semrawut, yang berkaitan dengan dugaan korupsi, penggelapan pajak, penyuapan, penyimpanan uang “haram”, kongkalikong dengan pejabat-pejabat penting, termasuk pembunuhan. Tidak salahlah kalau dikatakan bahwa Tommy Soeharto adalah salah satu di antara berbagai nama (atau “kata kunci”) yang paling banyak disebut oleh Google, yaitu dalam 176.000 halaman. (Dengan sendirinya, namaTommy tidak disebutkan sebanyak nama ayahnya, Soeharto,  yang mengangkangi negara dan bangsa selama 32 tahun, yaitu sebanyak  1.320.000 halaman  !).


Kalau kita buka Google di computer (Internet)  dan kita ketik kata kunci :
Tommy Soeharto – kejahatan,  maka ada 12 000 halaman bahasa Indonesia
Tommy Soeharto – kriminal , maka ada 18.400 halaman bahasa Indonesia          
Tommy Soeharto – pelanggaran hukum, maka ada  13 000 halaman bahasa Indonesia                      
Tommy Soeharto – hukuman pidana, maka ada 15.500 halaman bahasa Indonesia          
Tommy Soeharto – korupsi, maka ada 50.400 halaman bahasa Indonesia   


Jadi, bacaan tentang Tommy Soeharto di Google adalah banyak sekali dan mengenai macam-macam soal yang pernah diberitakan dalam berbagai media atau ditulis. Karena banyaknya jumlah halaman, dan juga karena isinya yang macam-macam, maka siapapun yang ingin membacanya agak teliti, memerlukan kesabaran atau ketekunan, waktu, dan tenaga untuk memilah-milahnya. Silakan mencobanya!


Namun,  begitu banyaknya halaman di Google itu tidak hanya berisi yang bersifat negatif saja tentang Tommy Soeharto, melainkan juga hal-hal yang positif. Ini disebabkan karena sebagian terbesar dari media cetak dan televisi (tidak semuanya, memang !) terlalu lama ada dalam kekuasaan atau pengaruh kubu Soeharto dan simpatisan-simpatisannya. Sejak lama, dan sampai sekarang. Karena itu,  walaupun citra rumah jalan Cendana sudah rusak oleh banyaknya korupsi, penyalahgunaan kekuasaan, dan berbagai pelanggaran hukum (dan moral !!!) yang begitu banyak, maka masih saja ada pengagum-pengagumnya


Kekuatan uang haram Tommy Soeharto


Uang haram yang berjumlah besar sekali atau kekuatan dana (yang mungkin beratus-ratus triliun Rupiah) yang bisa dimainkan oleh Tommy Soeharto adalah senjata utamanya untuk “membeli” pengaruh di kalangan Golkar, merekayasa perpecahan dan pertentangan, memojokkan atau melumpuhkan lawan-lawan atau saingannya. Kemenangan Tommy untuk merebut kedudukan Ketua Umum Golkar adalah syarat utama yang mutlak untuk merebut jabatan presiden dan penghuni Istana Negara.


Justru inilah yang harus dicegah oleh semua kalangan dan golongan dari sebagian terbesar dari rakyat Indonesia, tidak peduli dari aliran politik yang mana pun juga, (termasuk oleh kalangan Golkar sendiri), dan tidak peduli juga dari suku dan agama yang apa pun juga. Mencegah Tommy Soeharto (dengan berbagai cara, jalan dan bentuk) menjadi ketua umum Golkar (untuk kemudian menjadi presiden) adalah urusan semua orang yang tidak menginginkan negara kita dipimpin orang yang dalam hidupnya selama ini sudah begitu banyak cacadnya yang serius sekali. Tommy Soeharto sudah terkenal sekali dengan tudingan korupsi besar-besaran, pelanggaran hukum (dan moral), dan pembunuhan Hakim Agung Syafiuddin Kartasasmita.


Kita tidak bisa  (dan tidak boleh sama sekali  !!!) membiarkan seorang pembunuh yang sudah pernah dihukum berat di Nusakambangan (dan yang dituding sebagai maling harta negara dan rakyat lewat berbagai bentuk korupsi dan penyalahgunaan) merebut kedudukan sebagai kepala negara kita. Bangsa kita membutuhkan kepala negara yang bisa menjadi panutan, yang bisa mengangkat derajat atau martabat bangsa, bukannya kepala negara sejenis Soeharto yang justru malahan sudah memerosotkan martabat dan kehormatan  bangsa.


Memblejedi Tommy Soeharto adalah  tugas besar dan mulia


Negara dan bangsa kita  yang sebanyak 240 juta jiwa, tidak boleh dihina, atau diaduk-aduk dan diobok-obok oleh seorang Tommy Soeharto (beserta konco-konco dan para pendukungnya, terutama di kalangan bisnis). Pencalonan dirinya sebagai Ketua Umum Golkar sebagai jalan menuju Istana Negara adalah penghinaan terhadap nalar sehat bangsa, Tommy Soeharto mengira bahwa dengan uang haramnya yang melimpah-limpah itu, ia bisa terus-menerus menipu kesedaran sebagian terbesar rakyat, dan menutupi cacad-cacadnya atau dosa-dosanya yang banyak, untuk selama-lamanya.


Karena itu, segala macam kegiatan untuk memblejedi Tommy Soeharto, dalam segala bentuknya ataupun caranya, merupakan sumbangan atau partisipasi dalam mencegah kemungkinan dijerumuskannya negara dan bangsa lebih lanjut ke dalam kehinaan. Memblejedi Tommy Soeharto bukanlah hanya sekadar menyajikan sensasi, atau mengumbar gossip rendahan, atau melampiaskan dengki dan dendam, atau melancarkan fitnah terhadap pribadinya dan bukan pula hanya untuk semata-mata menyakiti hatinya (beserta para pendukungnya).


Memblejedi  Tommy Soeharto adalah demi kepentingan ratusan juta warganegara RI, demi menyelamatkan negara RI dari kerusakan dan pembusukan yang lebih parah lagi dari pada sekarang ini. Jadi, menggagalkan usaha-usaha Tommy Soeharto dalam melaksanakan rencana-rencananya yang bisa merugikan kepentingan orang banyak adalah hal yang benar, sah, dan mulia. Segala perjuangan terhadap Tommy Soeharto adalah bagian yang penting dari tugas bersama kita untuk mengikis habis sisa-sisa busuk rejim militer Orde Baru, dan juga punya peran penting dalam perlawanan kepada segala yang berbau Soeharto, pengkhianat besar terhadap tokoh terluhur bangsa kita, yaitu Bung Karno.
