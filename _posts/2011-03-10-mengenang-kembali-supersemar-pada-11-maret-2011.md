---
layout: post
title: Mengenang Kembali Supersemar Pada 11 Maret 2011
date: 2011-03-10
---

Di bawah berikut ini disajikan sebuah tulisan/makalah tentang Supersemar 11 Maret, yang layak menjadi bahan renungan kembali bagi semua orang yang ingin mengetahui tentang berbagai hal tentang pengkhianatan Suharto bersama sejumlah jenderal-jenderal Angkatan Darat terhadap Bung Karno, dan terhadap revolusi 45.

Makalah yang bagus dan penting ini, meskipun agak panjang, disajikan selengkapnya sekaligus,  supaya para pembaca tidak terpotong-potong membacanya. Dengan menyimak tulisan ini, maka kelihatan sebagian kecil dari dosa-dosa Suharto bersama jenderal-jenderalnya, kepada bangsa dan negara, termasuk kepada generasi-generasi yang silih berganti di masa-masayang akan datang.

Supersemar yang selama 32 tahun pernah diuar-uarkan oleh pendukung-pendukung Orde Baru (terutama pimpinan TNI-AD dan Golkar !!!)  setinggi langit terus-menerus, dan secara luas serta sistematis, ternyata adalah hasil konspirasi jahat besar-besaran  untuk menggulingkan kekuasaan Bung Karno dan melumpuhkan kekuatan PKI, yang merupakan pendukung politik Bung Karno yang utama.

Supermar adalah sumber segala kejahatan, segala dosa, segala tindakan pelanggaran HAM, yang pernah dilakukan sekelompok pengkhianat bangsa, yang mendatangkan kesengsaraan, penderitaan, dan kesedihan bagi banyak sekali orang-orang yang tidak bersalah.

Marilah sama-sama kita renungkan berbagai hal yang berkaitan dengan Supersemar, yang pernah merupakan  noda hitam dalam sejarah bangsa kita.



Paris, 10 Maret 2011

Umar Said

= = = =

MENGENANG KEMBALI SUPERSEMAR
PADA 11 MARET 2011


TIDAK ADA SALAHNYA PADA 11 MARET 2011 KALI INI SAYA MENURUNKAN KEMBALI MAKALAH SAYA TENTANG SUPERSEMAR. MENURUT SAYA MASIH RELEVAN KARENA SUPERSEMAR ASLI BELUM DITEMUKAN.

Makalah Seminar Nasional dan Diskusi Interaktif "Impilkasi Wafatnya Soeharto terhadap Kebenaran Sejarah Supersemar."

Makalah Seminar Nasional dan Diskusi Interaktif "Implikasi Wafatnya Soeharto terhadap Kebenaran Sejarah Supersemar" diselenggarakan oleh Fakultas Hukum Universitas YARSI di Ruang Auditorium Universitas YARSI, Jakarta, Selasa, 25 Maret 2008. Penulis adalah Direktur Eksekutif Lembaga Pengkajian Sejarah Supersemar (LPSS) dan Penulis Buku: Jenderal TNI Anumerta Basoeki Rachmat dan Supersemar. Nara sumber lain adalah: Dr.Anhar Gonggong (Sejarawan), Atmadji Sumarkidjo (Wapemred RCTI dan Penulis Buku: Jenderal M.Jusuf Panglima para Prajurit dan Abdul Kadir Besar (Sekretaris Umum MPRS 1966).


Supersemar, Sumber Sejarah yang Hilang

Oleh : Dasman Djamaluddin

Mantan Presiden RI kedua Soeharto yang wafat pada hari Ahad, 27 Januari 2008, memunculkan berbagai polemik sekitar pemerintahannya selama 32 tahun. Banyak di antaranya mendukung, tetapi banyak pula yang mengecam. kita berharap kepergian Soeharto merupakan awal dari terbukanya tabir kegelapan sejarah bangsa Indonesia yang selama ini ditutup-tutupi.

Kepergian orang nomor satu di Indonesia tersebut mendapat tempat yang istimewa di berbagai media massa. Ini membuktikan, lengsernya Soeharto sebagai seorang Presiden pada 21 Mei 1998 tidak memudarkan pengaruh yang telah dibinanya selama ini, karena hingga menghembuskan nafas terakhirnya, masih banyak para pejabat atau mantan pejabat dalam dan luar negeri berkunjung ke rumahnya di Jalan Cendana.

Selain itu, keterkaitan Soeharto dengan hilangnya Supersemar tidak dapat diragukan, karena naskah asli tersebut dipegang oleh beliau :

"Naskah asli Surat Perintah Sebelas Maret (Supersemar) yang tidak diketahui keberadaannya selama 40 tahun saat ini berada di tangan mantan Presiden Soeharto," ujar Wakil Presiden (Wapres) Jusuf Kalla dalam sambutannya pada peluncuran buku memoir Jenderal M.Jusuf Panglima para Prajurit di Jakarta, Jumat malam (10 Maret) 2006. foot note : "Naskah asli Supersemar Dipegang Soeharto," Media Indonesia (Sabtu, 11 Maret 2006): 12

Kepergian Soeharto seakan-akan telah diatur sebelumnya, karena hari wafatnya berselang tidak lama dengan tahun peringatan ke-42 lahirnya Surat Perintah 11 Maret (Supersemar/ 11 Maret 1966-11 Maret 2008) yang merupakan titik tolak seorang anak dari Desa Kemusuk (Jawa Tengah) itu menapak langkahnya menjadi Presiden.

Hari ini dengan diselenggarakannya Seminar Nasional bertema: "Implikasi Wafatnya Soeharto terhadap Kebenaran Sejarah Supersemar," juga merupakan langkah terpuji buat seorang ilmuwan untuk mengkaji lebih mendalam tentang kebenaran sejarah 42 tahun lalu. Tujuannya antara lain untuk kepentingan umum. Ini pula yang dimaksud oleh Vaclav Havel (foot note: Vaclav Havel adalah Presiden Pertama dan terakhir Republik Cekoslovakia sesudah tahun 1989. Dia bukan berasal dari politikus, tetapi dari seorang seniman, penulis dan lakon teater yang berhasil mengantar rakyatnya membuang sistem komunis. Tetapi tidak lama kemudian dia sangat kecewa dengan hasil referendum di mana rakyatnya terpecah menjadi dua, bergabung dengan Ceko dan Slovia) bahwa seorang ilmuwan tugasnya adalah membaktikan hidupnya untuk berpikir demi kepentingan umum dan bahkan menafikan kepentingan pribadinya. Jika seseorang sudah lebih banyak memikirkan dirinya sendiri dan mengabaikan kepentingan
umum, maka boleh jadi keilmuawan seseorang tersebut perlu diragukan, karena biasanya seorang ilmuwan atau juga disebut dengan intelektual, cendekiawan atau cerdik pandai mempraktikan hidup dalam kesederhanaan, jujur dan rela berkorban. Sehingga seorang cendekiawan, agamawan dan seniman di dalam menjalankan fungsinya masing-masing punya getaran gelombang yang sama.

Mempelajari dan menggali sejarah masa lalu buat seorang ilmuwan tidak mudah. Dia dihadapkan dengan berbagai kemungkinan-kemungkinan dari mana sumber bahan penulisannya diperoleh. Kalau sumbernya telah ditemukan, maka masih ada sederet pertanyaan yang harus diajukan. Apakah sumber yang diperolehnya itu otentik (asli), apakah sumbernya shahih, sah dan benar (validity), terpercaya, sungguh-sungguh benar (realibility) dan kuat (perihal dapat dipercaya tadi /kredebility) ? Jika hl ini telah terpenuhi, barulah ilmuwan tersebut memenuhi kreteria sebagaimana diungkapkan oleh W.S.Rendra bahwa seorang cendekiawan harus "berumah di angin", tidak mau terikat oleh suatu sistem yang menghalangi kebebasannya.

"Ia harus bebas pula dari ikatan bathin sehingga konsekuen menurut keyakinan intelektualnya dan jangan terjadi sebagaimana Julien Benda katakan sebagai 'pengkhianatan kaum cendekiawan'," ujar W.S.Rendra yang baru saja memperoleh Doktor Kehormatan dari Universitas Gajah Mada (foot note: Dick Hartoko (ed), Golongan Cendekiawan (Jakarta: Yayasan Obor Indonesia dan PT.Gramedia, 1980), hal.ix)

Penghilangan Sumber

Sumber sangat penting bagi kemajuan sebuah bangsa, bahkan maju atau tidaknya peradaban sebuah bangsa ditentukan oleh sumber-sumbernya. Itulah sebabnya ketika sebuah bangsa berkeinginan menghancurkan bangsa lainnya, yang dilkukannya pertama kali adalah menghancurkan kebudayaannya dengan menghancurkan perpustakaan, museum dan pusat-pusat kesenian lainnya. Atau dengan istilah lain," menghancurkan peradabannya."

Jika hal tersebut telah dilakukan, maka bangsa tersebut tidak lagi punya harga diri dan tidak punya lagi sumber-sumber kebudayaan yang patut dibanggakannya, sehingga masuklah kebudayaan bangsa si penyerang sebagai pengganti budaya bangsa yang diserang.

Apa yang terjadi di Indonesia di masa Orde Baru--sebuah istilah yang sejak awal tidak saya setujui, karena istilah tersebut berkonotasi merendahkan apa yang telah dihasilkan pemerintahan sebelumnya, Orde Lama--sangat mirip dengan "penghilangan sumber" yang pernah dilakukan bangsa-bangsa terdahulu. Bedanya, di masa Orde Baru "penghilangan sebuah sumber" dilakukan oleh bangsanya sendiri, bukan oleh bangsa lain.

Berbagai penghilangan sumber di masa Orde Baru, salah satunya apa yang pernah diungkapkan oleh Majalah TEMPO, 28 Juli 2002. Majalah tersebut mengungkap penghilangan dokumentasi tentang aktivitas parlemen Indonesia sejak era Konstituante hingga MPRS. Dokumen ini dimusnahkan pemerintah Orde Baru. Buku yang diterbitkan sebnyak 120 jilid, berjudul Laporan MPRS 1966-1972 tidak lagi kita temukan di perpustakaan-perpustakaan sebagai pusat kebudayaan bangsa untuk mencerdaskan generasi penerus, begitu pula di Arsip Nasional. Pada tahun 1972, pemerintah melarang peredaran buku tersebut dan buku yang berhasil ditemukan dibakar.

"Abdul Kadir Besar, sejumlah pengurus MPRS, dan pengelola Percetakan Siliwangi malah sempat diinterogasi. Tudingannya seram, membocorkan rahasia negara. Pak Nasution dicekal dari tahun 1972 sampai 1993, sejak itu, penjagaan dan fasilitas ditarik. Bahkan air PAM di rumah dicabut. Setiap hari intelijen mengamati kami. Semua yang dekat-dekat Pak Nas akan dibikin susah, sehingga orang takut datang. Yang setia adalah Surono dan Wiyogo Admodarminto, Soepardjo Roestam juga pernah datang, tapi cuma sebentar, lalu pergi. Yang lain menjauh. Seolah-olah kami ini penderita lepra. Sewaktu Adam Malik meninggal, Bapak datang. Tiba-tiba ada tentara yang menarik Bapak dan menyuruh mundur dengan cara yang tidak sopan. Kurang ajar sekali mereka," ujar isteri Pak Nasution, Yohana Sunarti Nasution.

Selanjutnya Bu Nas mengatakan:

"Pak Nas bilang sudah memaafkan Pak Harto. Cuma ada satu orang yang tidak mau dia maafkan. Tapi saya tak akan bilang siapa. Bapak bilang TNI sudah meninggalkan tugas seharusnya. TNI harusnya dekat dengan rakyat, bukan ikut memeras rakyat. Mengapa sekarang jadi begini ? TNI sekarang masuk ke segala peran. Orang jadi mempermasalahkan Pak Nas sebagai penggagas dwi fungsi. Padahal dwifungsi yang dilakukan Pak Harto bukan yang dimaksud Pak Nas. Prinsipnya TNI punya hak juga di negeri ini. Tapi jangan di semua tempat."

Selain itu, tidak ditemukannya naskah pidato Presiden Soekarno pada bulan Mei 1966 di Sekretariat Negara, juga menjadi tanda tanya. Hal ini terungkap dari pernyataan Asvi Warman Adam, Sejarawan dan Peneliti Senior Lembaga Ilmu Pengetahuan Indoneisa (LIPI) di dalam kata pengantar buku: Revolusi Belum Selesai.

Buku yang diterbitkan dalam dua jilid itu berisi kumpulan sebagian pidato Presiden Soekarno, yang dimulai sejak pasca peristiwa Gerakan 30 September. Mengherankan, pidato politik yang disampaikan sebanyak 103 kali tersebut tidak memperoleh tanggapan dari rakyatnya. Majalah TEMPO memberi perhatian besar terhadap terbitnya buku tersebut dan berkomentar:

"...pidato itu juga melukiskan kesunyian seorang Bung Besar. Perintahnya tak dituruti, pidatonya hanya menjadi kembang api, membuncah lalu hilang bersama malam. Hampir dua tahun suara Bung Karno nyaris tidak terdengar. Ia seperti tokoh dalam novel Gabriel Garcia Marquez, lelaki yang melewati waktunya dalam 100 tahun kesendirian."

Sepertinya penghilangan dokumen masih saja terjadi hingga sekarang. Baru-baru ini, hilangnya berkas penyelidikan pro justia kasus pelanggaran hak asasi manusia berat dalam tragedi Trisakti tahun 1998, Semanggi I dan Semanggi II, serta peristiwa penghilangan orang yang ada di Kejaksaan Agung, sungguh disesalkan.

Peranan TNI

Orde Baru juga disebut kemenangan TNI (dulu istilahnya ABRI) terhadap Partai Komunis Indonesia (PKI), karena pada Masa Sistem Politik Demokrasi Terpimpin, golongan militer tidak mendapat kesempatan lebih besar dalam kegiatan politik. Peranan militer semakin jelas terlihat ketika sedang membahas konsep Orde Baru di dalam Seminar Angkatan darat (AD) II 1966 yang berlangsung di Graha Wiyata Yudha, Seskoad Bandung, 25 - 31 Agustus 1966. Wlaupun terdapat kalangan sipil, tetapi jumlahnya sangat sedikit. Tujuan seminar pada waktu itu adalah untuk mengoreksi total serbagai ketimpangan-ketimpangan di masa Orde Lama. Jadi koreksi total adalah semangat Orde Baru.

Sangat jelas, bahwa di saat-saat mendirikan Pemerintahan Orde Baru, peranan TNI khususnya Angkatan Darat sangat besar. Itulah sebabnya mengapa para pelaku sejarah Supersemar adalah dari kalangan militer, termasuk Jenderal TNI Anumerta Basoeki Rachmat yang bukunya telah diterbitkan thun 1998 dan sebelumnya telah memperoleh persetujuan keluarga untuk ditulis pada tanggal 7 November 1996.

Pada masa Orde Baru, TNI tidak hanya berpengaruh terhadap salah satu partai politik saat itu (Golkar), walaupun saat itu enggan disebut sebuah partai, tetapi juga terhadap posisi TNI yang secara terang-terangan berada di lingkaran kekuasaan. TNI tidak hanya menunjukkan pengaruhnya di pemerintahan, tetapi di parlemen berlaku aturan-aturan khusus untuk TNI. Hal tersebut dinyatakan dengan tegas dalam Sidang Pleno Dewan Perwakilan Rakyat Gotong Royong (DPR-GR) pada bulan Februari 1967, di mana Fraksi ABRI mengemukakan, bahwa ikut sertanya ABRI dalam politik adalah suatu keharusan dalam kehidupan sistem politik di Indonesia. Dengan dalih menjaga kekompakan, maka masuknya wakil-wakil ABRI dalam badan perwakilan haruslah dengan pengangkatan. Sehingga Soeharto di masa Orde Baru memegang tiga wewenang sekaligus. Dia adalah Panglima Tertinggi ABRI, Kepala Eksekutif dan Ketua Dewan Pembina Golkar.

Tetapi bagaimana selanjutnya ? Pada satu sisi Pemerintahan Orde Baru berkeinginan mengoreksi total berbagai ketimpangan-ketimpangan di masa Orde Lama, tetapi pada saat bersamaan pula pemerintah Orde Baru juga membuat kekeliruan di dalam menjalankan roda pemerintahan selama 32 tahun.

Pertama, di dalam hal membuabarkan sebuah partai, yaitu PKI. Soeharto tidak mengacu kepada penetapan Presiden Republik Indonesia No.7 tahun 1959 yang masih berlaku saat itu. Di mana di dalam pasal 9 ayat 1 nya dinyatakan bahwa :
Presiden, sesudah mendengar MA dpat melarang dan/atau membubarkan partai yang:
1. Bertentangan dengan azas dan tujuan negara
2. Programnya bertentangan dengan azas dan tujuan Negara
3. Dan melakukan pemberontakan
4. Tidak memenuhi syarat-syarat lain yang ditentukan.

Dengan ketentuan ini jelaslah bahwa tindakan Soeharto membubarkan PKI telah menyalahi aturan hukum. Hanya dengan berbekal Supersemar, Soeharto yang mengatasnamakan Presiden Soekarno dapat membubarkan sebuah partai, yaitu PKI.

Kedua, Supersemar meningkat menjadi Tap MPRS sebenarnya menjadi tinggi sekali dan menjadi Sumber Hukum Tata Negara, tetapi pada dasarnya tidak layak. Supersemar tidak lebih hanyalah merupakan surat perintah, tidak termasuk di dalam perundang-undangan dan bukan pula suatu peraturan kebijakan. Jika dinyatakan bahwa Supersemar tersebut tidak perlu dipersoalkan lagi karena setelah dikeluarkan dan suratnya berlaku selesai, permasalahannya bukan terletak pada manfaat surat pada saat itu, tetapi kembali kepada uraian di atas, bahwa bangsa ini setelah masa reformasi tidak hanya mereformasi peraturan-peraturan atau lembaga-lembaga peraturan, tetapi yang lebih penting adalah mereformasi budaya hukumnya.

Supersemar muncul dalam ketetapan MPRS No.XIX/MPRS/1966 tentang Sumber, Tertib Hukum dan Tata Urutan Perundangan RI. Urutan Sumber Tertib Hukum pada waktu itu adalah: Pancasila, Proklamasi Kemerdekaan 17 Agustus 1945, Dekrit Presiden 5 Juli 1959, Undang-Undang dasar Proklamasi 1945 dan Surat Perintah 11 Maret 1966.

Selanjutnya terlihat sangat jelas bagaimana Soeharto sudah dipersiapkan menjadi Presiden. Hal ini dapat dibaca dalam Ketetapan MPRS No.XV/MPRS/1966 tentang Pemilihan/Penunjukan Wakil Presiden dan Tata Cara Pengangkatan Pejabat Presiden. Pada dasarnya dinyatakan bahwa berdasarkan pertimbangan-pertimbangan politik psychologis, MPRS menganggap lebih baik tidak mengisi lowongan jabatan Wakil Presiden. Selanjutnya ditegaskan, oleh karena itu dipandang perlu untuk membuat ketentuan mengenai pejabat Presiden, apabila Presiden sewaktu-waktu berhalangan, mangkat, berhenti, atau tidak dapat melakukan kewajibannya guna menghindarkan kekosongan dalam Pimpinan Negara dan Pemerintahan.

Ketiga, adanya Surat Perintah Presiden Soekarno, tanggal 13 Maret 1966 yang tidak diindahkan Soeharto. Surat tersebut dikeluarkan sebagai koreksi terhadap penyelewengan pelaksanaan Supersemar. Surat sepanjang satu pragraf itu baru disebarluaskan pada hari Senin, 14 Maret 1966. Isi surat tersebut diungkapkan kembali dalam pidato Bung Karno pada 17 Agustus 1966. Nasib surat ini sama dengan Supersemar, yaitu "hilang."

Keempat, telah terjadi penangkapan-penangkapan tanpa prosedur, termasuk penangkapan-penangkapan di jajaran eksekutif (para menteri Kabinet Dwikora yang tanpa sepengetahuan Bung Karno) dan anggota legislatif, sehingga memuluskan jalan bagi Soeharto mengemukakan gagasan-gagasannya. Pembersihan oknum-oknum partai di Parlemen bertujuan untuk membersihkan orang-orang yang dekat dengan sikap Bung Karno. Praktis di MPRS, Bung Karno tidak punya kekuatan. Pada komposisi MPRS yng demikian, Sidang istimewa berlangsung, maka menjadi hal yang sangat wajar bila Pidato Pertanggungjawaban Bung Karno, berjudul: Nawaksara ditolak.

Pada Sidang Istimewa yang berlangsung di Jakarta pada tanggal 7-12 Maret 1967 melalui Ketetapan No.XXXIII/MPRS/67, MPRS menetapkan mencabut kekuasaan pemerintah negara dari Presiden Soekarno dan dengan ketetapan yang sama, MPRS mengangkat Pengemban Ketetapan MPRS No.IX/MPRS/66, Jenderal Soeharto sebagai Pejabat Presiden hingga dipilihnya Presiden oleh MPR hasil pemilihan umum.

"untuk sementara waktu kami akan memberlakukan beliau sebagai Presiden yang tidak berkuasa lagi, sebagai Presiden yang tidak mempunyai kewenangan apa pun di bidang politik,kenegaraan dan pemerintahan." /foot note: Robert Edward Elson, Suharto Sebuah Biografi Politik (Suharto: a Political Biography), diterjemahkan oleh Satrio Wahono dan IG Harimoerti Bagoesoka (Jakarta: Pustaka Minda Utama, 2005), hal.298-299.

Pertanyaan yang muncul, siapa yang menandatangani surat keputusan pembentukan Kabinet Ampera yang Disempurnakan, karena pembentukan kabinet tersebut disebutkan berdasarkan Keputusan Presiden No.171 tahun 1967, apakah di dalam berbagai peraturan diperbolehkan seorang pejabat Presiden mengatasnamakan Presiden untuk menandatangani sebuah surat keputusan atau sejauh manakah sahnya sebuah surat keputusan bila ditandatangani oleh seorang presiden yang kekuasaannya telah dicopot ?

Jika kita kembali sejenak ke belakang, sejarah mencatat,bahwa telah terjadi pula berbagai penangkapan anggota PKI dan anggota organisasi yang terkait. Sampai Oktober 1965, sudah 1.334 orang di wilayah Jakarta yang ditahan dengan alasan terlibat PKI./foot note: Ibid, hal.239. Banyak sumber yang memberitakan jumlah pembantaian tahun 1965/1966 terutama di Jawa, Sumatera dan Bali, merupakan jumlah terbesar. Jumlah pembantaian itu tidak mudah diketahui persis, tetapi dari 39 artikel yang dikumpulkan Robert Cribb (1990:12), jumlah korban berkisar 78.000 jiwa hingga dua juta jiwa /foot note: Asvi Warman Adam,"Pembantaian 1965, Kekerasan Terbesar dan Rekonsiliasi," Kompas (4 Desember 2000): 40.

Selanjutnya Robert Cribb mengatakan bahwa pembantaian tahun 1965 dilakukan dengan memakai alat sederhana, seperti pisau, golok dan senjata api. Caranya juga sederhana, tanpa harus terlebih dulu dimasukkan ke kamar gas seperti dilakukan Nazi. Orang yang akan dieksekusi juga tidak dibawa jauh-jauh sebelum dibantai,biasanya mereka bunuh di dekat rumahnya. Ciri lain dari kejadian itu biasanya dilakukan pada malam hari.Pembunuhan berlangsung relatif cepat, hanya beberapa bulan. Nazi memerlukan waktu bertahun-tahun dan Khmer Merah melakukannya dalam empat tahun. Bahkan kasus kekejian tahun 1965 tersebut sempat menjadi perhatian mantan Ibu Negara Perancis, nyonya Francois Mitterand, yang bersama rombongan menyempatkan diri mengunjungi Klaten dan menengok lokasi "killing field" (ajang pembantaian) di daerah sekitar Pandansimping./footnote:Lihat Bernas, 4 September 2000,'Mantan Ibu Negara Perancis ke Klaten Tertarik Cerita "Killing Field" di Pandansimping. Lihat
juga cerpen GM Sudarta, Kompas, 10 Februari 2005.

Pembersihan sisa-sisa PKI tidak hanya di Jawa, tetapi juga di Sumatera Utara.Pembersihan sisa-sisa PKI dilakukan tanpa mengindahkan rasa kemanusiaan. Ada nuansa balas dendam politik yang memang tak terelakkan.Ia menjadi soal, lantaran korban dendam itu tidak hanya dirasakan oleh mereka yang diduga terlibat G.30.S, teapi juga keturunan mereka. Tidak cuma antek, tetapi juga keturunan mereka. Tidak cuma anak, tetapi cucu dan menantu. Stigma PKI khas Orde Baru benar-benar membuat berjuta anak Indonesia mengalami kematian perdata./foot note: Tim Investigasi,"Sejarah Hitam Pasca G-30-S, Majalah Tajuk No.23, TH.II (3 Januari 2000):68-71.

Kelima, pers mengalami apa yang dinamakan "pemasungan", sehingga peristiwa-peristiwa pada saat peralihan kekuasaan dari Soekarno kepada Soeharto banyak yang tidak diketahui dengan pasti oleh masyarakat. Hal ini terkait dengan peristiwa sebelumnya, di mana pada bulan Februari 1965, Menteri Penerangan Achmadi melarang terbit 21 surat kabar di Jakarta dan Medan karena mendukung Badan Pendukung Soekarno (BPS). Hanya yang leluasa memberitakan peristiwa-peristiwa itu adalah media-media yang sangat dekat dengan kekuasaan militer, seperti Harian Angkatan Bersenjata dan Berita Yudha. Kedua surat kabar ini pula yang memberitakan masalah Supersemar. Harian Angkatan Bersenjata memberitakan tanggal 12 Maret 1966 pagi dan sore, anehnya harian itu sudah tahu lebih dahulu tentang Supersemar. Sedangkan Berita Yudha memberitakan tanggal 13 Maret 1966.

Keenam, Soeharto sebagai Menpangad pada waktu itu tidak lagi patuh kepada Presiden Soekarno, sebagaimana diungkapkannya :

"Beliau mempunyai satu pendirian, saya punya pendirian lain. Tetapi saya tidak menentang begitu saja. Saya sebagai bawahan, sebenarnya harus taat. Apa yang diperintahkannya seharusnya saya patuhi. Tetapi saya sebagai pejuang tidak mungkin patuh begitu saja." /foot note: G.Dwipayana dan Ramadhan, K.H., Soeharto, Pikiran, Ucapan, dan Tindakan Saya (Jakarta:PT.Citra Lamtoro Gung Persada, 1989), hal.167.

Sebaliknya Presiden Soekarno, sejak awal telah menganggap Soeharto sebagai perwira yang "begudul" (kepala batu). Hal tersebut diungkapkan beliau pada pidato 20 November 1965 di depan ke empat panglima angkatan di Istana Bogor.

Bung Karno mengatakan bahwa ada perwira yang begudul. "Begudul! Itu apa ? Hei, Bung, apa itu begudul? ya, Kepala Batu. Saya yang ditunjuk MPRS menjadi Panglima Besar Revolusi. Terus terang bukan Soebandrio. Bukan Leimena. Bukan engkau Soeharto, bukan engkau Soeharto..." berbeda dari nama lain, Soeharto disebut dua kali secara berturut./foot note: Setiyono, loc.cit.

Supersemar dan Perang Dingin

Faktor penting munculnya Supersemar tidak dapat dilepaskan dari terjadinya "Perang Dingin" antara Amerika Serikat dengan Uni Soviet. Dimulai dengan konsep pembendungan komunisme melalui Doktrin Truman (Truman Doctrin) yang diumumkan pada tahun 1947 sebagai reaksi Amerika serikat terhadap ekspansi komunisme Uni Soviet setelah Perang Dunia II di Eropa Tengah dan Eropa Timur, dan yang kelihatan akan menjalar ke wilayah Laut Tengah, termasuk Turki dan Yunani./foot note: Lie Tek Tjeng, Percaturan Politik Di Kawasan Asia Pasifik Dilihat dari Jakarta (Jakarta: PT.Karya Unipress, 1983), hal.37

Konsep Doktrin Truman disusul oleh Marshall Plan yang bertujuan membantu rehabilitasi Negara-negara Eropa yang telah hancur sebagai akibat Perang Dunia II. Kemudian disusul dengan pembentukan Pakta Pertahanan Atlantik Utara (NATO) yang merupakan persekutuan militer raksasa Barat untuk membendung komunisme mulai dari Samudera Atlantik sampai ke Laut Tengah. sasaran utamanya adalah Uni Soviet.

Pada tanggal 1 Oktober 1949 negara Republik Rakyat Tiongkok (RRT) berdiri sebagai akibat kemenangan kaum komunis terhadap Koumintang. Tugas Amerika Serikat dan sekutunya dalam hal membendung komunisme bertambah berat. Untuk membendung komunisme dari RRT ini, Amerika Serikat dan sekutunya menganggap perlu membentuk Pakta Pertahanan di Timur. Pada tahun 1954 dibentuklah SEATO (Southeast Asia Treaty Organization), sebuah kekuatan pertahanan militer di Asia Tenggara beranggotakan Amerika Serikat, Inggeris, Perancis, Australia dan Selandia Baru serta negara-negara Asia: Filipina, Thailand dan Pakistan. Dalam hal ini, Indonesia menolak pembentukan pakta militer tersebut dan sejak saat ini hubungan Indonesia-Amerika Serikat mendingin, meskipun tidak bermusuhan. Pada saat bersamaan pertentangan dua kekuatan antara Barat dan Timur semakin memuncak, hal tersebut terlihat dari pecahnya Perang Korea (1950-1953), Pemisahan Berlin dengan tembok (Agustus 1961), Krisis
Kuba (1961-1962).

Di dalam situasi Dunia yang tidak menentu ini, Indonesia lalu memprakarsai Konferensi Asia Afrika pada tanggal 18 April 1955 di Bandung dan pada akhirnya menjadi landasan buat negara-negara yang tidak memihak ke Barat atau ke Timur, mendirikan sebuah gerakan bernama: Gerakan Non-Blok di Beograd tahun 1961. Pada saat bersamaan, bulan Mei 1955, Uni Soviet dan sekutunya membentuk pula sebuah pertahanan militer bernama Pakta Warsawa.

Perkembangan di luar negeri berpengaruh besar terhadap perkembangan di dalam negeri Indonesia, begitu pula sebaliknya. Munculnya PKI, setelah Hatta sebagai Wakil Presiden RI mengeluarkan Maklumat Wakil Presiden No.X (huruf eks, bukan angka 10 hitungan Romawi, tetapi abjad ke-24) dan ditindaklanjuti dengan Maklumat Pemerintah tanggal 3 November 1945 menjadi perhatian Amerika Serikat dan sekutunya. Apalagi secara tak terduga. di dalam Pemilihan Umum pertama di Indonesia pada tanggal 29 September 1955, yang hasilnya baru diumumkan tanggal 1 Maret 1956, PKI berhasil menduduki posisi ke-empat dalam jumlah pengumpulan suara untuk Parlemen/DPR (16,3 persen), setelah PNI (22,1 persen), Masyumi (20,9 persen) dan NU (18,4 persen).

Ketika Pemerintah Revolusioner Republik Indonesia (PRRI) menyatakan berpisah dari Negara Kesatuan Republik Indonesia tanggal 15 Februari 1958, Amerika Serikat mulai ikut membantu PRRI dengan persenjataan. Pada saat ini dikerahkan Armada Ketujuh Pasifik Amerika Serikat dengan membentuk Satgas 75 yang ditempatkan di Singapura. Dalam hal ini Menteri Luar Negeri PRRI Kolonel Maludin Simbolon pernah disarankan pihak Amerika Serikat untuk meledakkan instalasi tambang minyak Caltex di Riau agar ada alasan Armada VII Amerika Serikat mendaratkan pasukan marinirnya. Tetapi ditolak Maludin Simbolon karena dia tidak menghendaki Indonesia mengalami seperti yang terjadi di Korea Utara dan Korea Selatan atau "balkanisasi" negara dan bangsanya./foot note: Payung Bangun, Kolonel Maludin Simbolon, Liku-liku Perjuangannya dalam Pembangunan Bangsa (Jakarta: Pustaka Sinar Harapan, 1996), hal.257.

Pemisahan PRRI ini tidak terlepas dari mundurnya Wakil Presiden Mohammad Hatta pada tanggal 1 Desember 1956. Sejak saat ini PKI semakin leluasa mempengaruhi Bung Karno. Buktinya, ketika pada 21 Februari 1957, pukul 20.05, Bung Karno berpidato di Istana Merdeka dan secara terus terang mengatakan bahwa dirinya menginginkan agar kaum komunis ikut serta di dalam menyelenggarakan pemerintahan. Pidato Soekarno tersebut sangat mengejutkan berbagai pihak, terutama kelompok-kelompok yang sejak awal tidak menghendaki kehadiran PKI di Indonesia, terutama kelompok militer. Soekarno mengatakan :

"Yah. Saya tahu saudara-saudara misalnya terhadap PKI ada beberapa saudara-saudara atau pihak yang keberatan dia duduk dalam kabinet. Saya bertanya dengan setenang-tenangnya, saudara, apakah kita dapat terus menerus mengabaikan satu golongan yang di dalam pemilu mempunyai suara enam juta manusia ? Sungguh. saudara-saudara, saya tidak memihak. Saya sekedar menghendaki adanya perdamaian nasional. Saya sekedar ingin mengadakan cara pemerintah gotong royong dengan tidak memihak sesuatu pihak."/foot note: Presiden Soekarno, Menyelamatkan Republik Proklamasi Konsepsi Bung Karno. Catatan Stenografis dari Pidato Presiden Soekarno tanggal 21 Februari 1957 jam 20.05 di Istana Merdeka (Jakarta: Kementerian Penerangan RI, 1957), hal.13

Oleh karena itu, adalah hal wajar apabila Amerika Serikat dan sekutunya sejak awal mengamati perkembangan di Indonesia. Kegigihan Amerika Serikat membendung komunisme di Asia Tenggara dengan menggelorakan bahkan memperluas Perang Vietnam sampai ke Laos dan Kamboja, yang ditentang keras oleh Indonesia, menjadi salah satu bukti ada keterkaitan pihak Amerika Serikat dengan Indonesia di era "Perang Dingin" tersebut. Apalagi setelah Indonesia membentuk poros Jakarta-Phnom Penh- Hanoi-Beijing-Pyongyang yang sudah dianggap oleh Amerika Serikat bahwa Indonesia semakin masuk ke kubu Komunisme yang justru adalah musuh utama negara adidaya itu. Tetapi bagaimana pun juga kita kembali ke konsep semula. Sebagai seorang ilmuwan yang berpikir untuk bangsanya, perlu adanya sebuah kejujuran dibalik peristiwa apa pun yang melatar belakanginya. Konsep seorang ilmuwan sangat jelas, yaitu berkeinginan melihat sejarah sebuah bangsa tidak dimanipulasi oleh
kepentingan-kepentingan sesaat, termasuk di dalam hal melihat Sejarah Supersemar. Oleh Karena itu perlu kiranya kita menemukan sumber asli Supersemar tersebut.

DAFTAR BACAAN

A.BUKU

Amirmachmud. Surat Perintah 11 Maret 1966 (Supersemar) Tonggak Sejarah Perjuangan Orde Baru (tanpa penerbit dan tahun. Ditulis ketika menjadi Ketua MPR/DPR-RI).

Bangun, Payung. Kolonel Maludin Simbolon. Liku-liku Perjuangannya dalam Pembangunan Bangsa. Jakarta:Pustaka Sinar Harapan, 1996.

Budiawan. Mematahkan Pewarisan Ingatan. Jakarta: Elsam, 2004.

Djamaluddin, Dasman. Jenderal TNI Anumerta Basoeki Rachmat dan Supersemar. Jakarta: PT.Grasindo. 1998.

Dwipayana, G dan Ramadhan,K.H. Soeharto, Pikiran, Ucapan dan Tindakan Saya. Jakarta: PT.Citra Lamtoro Gung Persada, 1989.

Elson, Robert Edward. Suharto Sebuah Biografi Politik (Suharto: a Political Biography), diterjemahkan oleh Satrio Wahono dan IG Harimoerti Bagoesoka. Jakarta: Pustaka Minda Utama, 2005.

Feith, Herberth. Soekarno-Militer dalam Demokrasi Terpimpin. Jakarta: Pustaka Sinar Harapan, 1995.

Green, Marshal. Dari Sukarno ke Suharto. Jakarta: Pustaka Utama Grafiti, 1995.

Hari-Hari Terakhir Bung Karno (tanpa penerbit dan tahun. Seri Dokumentar).

Hartoko, Dick (ed). Golongan Cendekiawan. Jakarta: Yayasan Obor Indonesia dan PT.Gramedia, 1980.

May, Brian. The Indonesia Tragedy. Singapore: Graham Brash Ltd.1981.

Nasution,A.H. Kepemimpinan. Jakarta: tanpa penerbit, 1977.

Nawaksara. Jakarta: Central Klender Plaza, tanpa tahun.

Nishihara, Masashi. Sukarno, Ratna Sari Dewi dan Pampasan Perang. Jakarta: Grafiti, 1994).

Scott, Peter Dale. CIA dan Penggulingan Sukarno. Yogyakarta: Lembaga Analisis Informasi, tanpa tahun.

Sd, Subhan. Langkah Merah. Yogyakarta: Yayasan Bentang Budaya, 1996.

Setiyono, Budi dan Bonnie Triyana. Revolusi Belum Selesai. Jilid I. Jakarta: Messias, 2003.

Shill, Edward. The Intellectuals and the powers and other Essay. Chicago & London: The University of Chicago Press, 1972.

Soebandrio. Kesaksianku tentang G30S. Jakarta: Forum Pendukung Reformasi Total, 2001.

Soekarno, Presiden. Menyelamatkan Republik Proklamasi Konsepsi Bung Karno. Catatan Stenografis dari Pidato Presiden Soekarno tanggal 21 Februari 1957 jam 20.05 di Istana Merdeka. Jakarta: Kementerian penerangan RI, 1952.

Supersemar. Jakarta: Badan penerbit Almanak RI/B.P.Alda. 1977.

Suryohadiprojo, Sayidiman. Kepemimpinan ABRI Dalam Sejarah dan Perjuangannya. Jakarta: Penerbit Intermasa, 1996.

Tjeng, Lie Tek. Percaturan Politik Di Kawasan Asia Pasifik Dilihat dari Jakarta. Jakarta: PT.Karya Unipress, 1983.

B. Surat Kabar:

Adam, Asvi Warman."Pembantaian1965, Kekerasan Terbesar dan Rekonsiliasi", Kompas (4 Desember 2000):40.
."Seabad Kontroversi Sejarah," Kompas (17 Januari 2000) : 4

Luhulima, James."Peristiwa G30S Titik Balik Soekarno,"Kompas ( 1 Juni 2001): 69.

"Naskah Asli Supersemar Dipegang Soeharto", Media Indonesia (Sabtu, 11 Maret 2006): 12

C. Majalah

Anom, Andari Karina."Setumpuk Buku 30 Tahun yang lalu" dan"Kami Dijauhi Seperti Penderita Lepra,"Majalah TEMPO ( 28 Juli 2002) : 64 dan 69.

Iqra."Bung Karno 100 Tahun dalam Sunyi", Majalah TEMPO (26 Oktober 2003) : 71.

Tim Investigasi."Sejarah Hitam Pasca G-30-S."Majalah Tajuk No.23, Th.II (3 Januari 2000) : 68-71.
