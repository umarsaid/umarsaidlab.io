---
layout: post
title: Njoto, Wakil Ketua CC PKI yang mati dalam sunyi
date: 2005-08-05
---

Ulasan buku Saleh Abdullah cs, Usaha Untuk Tetap Mengenang, Kisah-Kisah Anak-Anak Korban 65, Jendela Budaya & Yappika & Hidup Baru, 2003, dengan pengantar MM Billah (Anggota Komnas HAM). Tebal: xlix + 132, harga Rp19.000,-

Oleh: Harsutejo

Anak-anak Sebagai Korban

Buku kecil ukuran 12 x 19 cm ini sudah terbit hampir dua tahun yang lalu, tetapi saya baru saja mendapatkannya menyempil di toko buku di pertokoan Sarinah, Jl Thamrin, Jakarta. Di dalamnya dimuat sejumlah kisah anak korban tragedi 1965, ya korban rezim Orde Baru. Pendokumentasian dan penerbitan kisah semacam ini penting untuk melawan amnesia sejarah yang menurut pengantar penerbitnya sang rezim telah mengkonstruksikan kenangan yang sama tapi dengan versi sama sekali lain secara massif, terorganisir dan sistimatis. Seperti kita ketahui apa yang disebut monumen Pancasila Sakti di Lubang Buaya secara kasat mata telah membakukan hal semacam itu, sejarah yang digelapkan. Dalam suatu forum santai saya pernah menyampaikan gagasan kelak monumen itu kita tempel dengan plakat ‘Monumen Kebohongan’.


Begitu banyak keluarga yang disebut kaum kiri, utamanya para anggota organisasi massa dan PKI yang telah dibantai, ditahan, dan anak-anak mereka berpencaran tak tentu tujuan. Begitu hebatnya penciptaan suasana yang mengancam sehingga banyak orang biasa termasuk sanak keluarga yang tidak berani bersentuhan dengan keluarga semacam itu. Bukan itu saja bahkan juga diciptakan kebencian terhadap mereka ini sebagai ‘anak-anak setan pki’. Sudah sejak dini anak-anak ini membentuk pertahanan diri tanpa sadar antara lain dengan menyembunyikan identitas dirinya rapat-rapat, sesuatu yang sering amat menyiksa karena dilakukan hampir tanpa batas.

Bagi mereka yang tidak memiliki tempat berlindung dalam alam perlakuan sebagai warga kelas kambing, terbuka ke dunia gelandangan. Bahkan seorang kakak menceritakan bagaimana dua orang adiknya yang telah lulus SLTA ditolak bekerja karena diketahui anak tapol mengalami depresi berat dan menjadi gelandangan. Isteri saya yang membaca bab ini diburu bayangan setiap bertemu gelandangan sebagai anak tapol..... Cukup mengiris. Di sela-sela kesedihan semacam itu, seorang anak yang telah digembleng pahit getir pengalaman hidupnya muncul sebagai pemenang dan menyimpulkan bahwa ‘mendidik penguasa itu mesti dengan perlawanan, sedang mendidik rakyat harus dengan gerakan’. Yang lolos semacam ini bahkan mampu mendidik sekitarnya termasuk luar biasa, sedang kebanyakan manusia itu biasa-biasa saja.



Keluarga Nyoto


Sejumlah anak masuk ke dalam penjara bertahun-tahun bersama ibu mereka yang menjadi tapol karena tiada tempat lain untuk berlindung, ya berlindung ke dalam penjara! Betapa absurdnya. Bahkan juga perempuan tapol yang sedang hamil melahirkan di penjara. Seperti pernah kita dengar, Nyoto yang Menteri Negara dan Wakil Ketua CC PKI sampai meninggalnya, istri dan 7 anaknya (yang tertua 9 tahun, si bungsu yang lahir 1966 sedang menyusu) dijebloskan ke dalam tahanan di Kodim Jl Setiabudi, Jakarta.

Anak-anak ini pula selama dalam tahanan bersama ibu mereka melihat dan mendengarkan jerit tangis tahanan laki dan perempuan yang sedang disiksa! Dapat kita bayangkan dampak psikologis macam apa yang mungkin mencengkeram seluruh jiwa raga mereka untuk seumur hidupnya. Anak pertama Nyoto yang bernama indah Svetlana Dayani, bertahun-tahun lamanya tidak berani menyandang nama depannya hanya karena berbau Rusia.


Seperti kita ketahui isteri Nyoto, Sutarni, berasal dari keluarga ningrat Mangkunegaran, Sala. Dia tidak memiliki kegiatan politik apa pun, sudah terlalu sibuk dengan anak-anaknya sampai tragedi 1965 meletus. Beruntung anak pertama mereka, Svetlana, baru berumur 9 tahun bersama ibunya di tahanan. Saya ngeri membayangkan, andai saja dia sudah gadis remaja, entah apa yang terjadi terhadap dirinya. Bukan rahasia lagi bahwa telah terjadi pelecehan seksual dan perkosaan, sering beramai-ramai terhadap para perempuan korban. Untuk membungkam saksi tak jarang kemudian mereka dihabisi.
Bukti sejarah berdasarkan pengakuan si pelaku menunjukkan bahwa Nyoto yang Menteri Negara dibunuh atas perintah Jenderal Sumitro sebagai pembantu Jenderal Suharto dengan jabatan Aisten Operasi Menpangad pada 1965. Tipikal jenderal Orba yang dengan bangga mengakui telah melakukan pembunuhan terhadap lawan politiknya ketika kekuasaannya sedang berkibar.


“Nyoto, gembong PKI, pernah saya lihat sewaktu ia ikut dalam Sidang Kabinet di Bogor. Dia kelihatan sangat sombong, kurang ajar terhadap Pak Hidayat, hingga saya memberi tanda dengan sikut kepada Jenderal Moersjid dan berkata: ‘Sjid, ik krijg hem wel.’ (Aku akan dapatkan dia). Benar, saya sakit hati melihatnya. Saya perintahkan khusus untuk mengejarnya supaya ia terpegang. Selang beberapa waktu ia dikabarkan mati sudah”. [i]

Itulah yang diakui dengan bangga oleh Jenderal Sumitro. Kalau saja Pak Jenderal ini masih hidup, ia dapat diseret ke depan pengadilan kriminal karena memerintahkan pembunuhan seorang warga negara sekaligus seorang menteri yang sedang menjabat.


Svetlana melukiskan ayahnya sebagai ayah yang baik kepada siapa saja, halus, sopan, pandai. Sesibuk apa pun dia masih memberikan waktunya untuk keluarga. Svetlana dan adiknya terkadang diajak ke kantor ayahnya di koran Harian Rakjat, juga ketika menerima undangan sarapan ke Istana Negara bersama Bung Karno, bahkan nonton pertandingan sepakbola.

Selama hantaman badai, setelah terpisah dari ibunya, dia dan adik-adiknya terpencar-pencar mengikuti sanak keluarga yang berbeda-beda di Jawa dan Sumatra, sementara ibu mereka mendekam sebagai tapol selama 14 tahun. Rumahnya di Kebayoran Baru telah dijarah dan diobrak-abrik, buku-buku dan dokumen dibakar termasuk segala macam dokumen keluarga dan foto-foto. Belakangan rumah itu ditinggali oleh Jenderal Mattalata (ayah penyanyi Andie Meriem). Setelah mengalami segala macam pahit getir bersama semua adik-adiknya, mereka cenderung bersikap apatis terhadap politik karena trauma. Akhirnya Svetlana Dayani berseru, “Hentikan diskriminasi kepada kami”.
