---
layout: post
title: 'Hasan Raid, tokoh PKI :" G30/SOEHARTO, bukan G30/PKI"'
date: 2011-09-07
---

Menjelang tanggal 30 September 2011  dalam website http://umarsaid.free.fr/ akan diusahakan penyajian secara berturut-turut tulisan atau artikel mengenai berbagai soal yang berkaitan dengan G30S, yang merupakan peristiwa besar dan sangat bersejarah bagi negara dan bangsa Indonesia.

Penyajian berbagai bahan bacaan  tentang G30S ini dimaksudkan sebagai ajakan kepada sebanyak mungkin orang untuk mengenang kembali atau merenungkan macam-macam akibat dari tindakan para jenderal Angkatan Darat di bawah pimpinan Suharto sebagai kelanjutan peristiwa G30S itu.

Sebab, seperti yang sama-sama kita ketahui,  dengan dalih untuk bertindak terhadap pelaku-pelaku pembunuhan 6 jenderal-jenderal Angkatan Darat, Suharto bersama pimpinan militer lainnya telah melakukan pelanggaran HAM secara besar-besaran yang tiada taranya dalam sejarah bangsa.  Pelanggaran HAM ini dimulai sejak Oktober 1965, berturut-turut dan terus-menerus selama puluhan tahun, sampai jatuhnya Orde Baru dalam tahun 1998.

Mula-mula atau pada tingkat awal, para pemimpin PKI dari tingkat yang paling atas sampai yang paling  bawah banyak yang telah dibunuhi, disiksa dengan berbagai cara, dipenjarakan atau dihilangkan. Itu terjadi di seluruh Indonesia. Boleh dikatakan  bahwa  seluruh pimpinan dan kader-kader PKI  di berbagai tingkat  telah dihancur-luluhkan dengan cara-cara  yang biadab sekali.

Kemudian,  dilakukan pembunuhan massal di banyak tempat di seluruh Indonesia, terutama Jawa Tngah, Jawa Timur, Bali, Sumatera, Kalimantan dan Sulawesi, serta dibarengi dengan penangkapan dan pemenjaraan ratusan ribu orang-orang tidak bersalah apa-apa, yang menjadi anggota atau simpatisan PKI dan organisasi massa seperti SOBSI, BTI, Pemuda Rakyat, Gerwani, IPPI, CGMI, PGRI Non-vaksentral, HSI, Lekra.

Sebagian dari mereka ada yang ditahan (harap catat : tanpa pengadilan !!!) di Pulau Buru atau Nusa Kambangan, atau Plantungan,  sampai belasan tahun. Di antara mereka terdapat intelektual atau sarjana (insinyur, dokter, dosen, wartawan, penulis, seniman, sastrawan).



Nasib yang menyedihkan puluhan juta keluarga korban Orba

Sebagian terbesar sekali di antara ratusan ribu eks-tahanan politik (eks-tapol) ini hidup sengsara sesudah keluar dari penjara atau tahanan, karena sulit sekali mencari pekerjaan dan hidup normal, atau mendapat berbagai macam perlakuan yang tidak layak dari masyarakat atau aparat-aparat pemerintahan.

Banyak sekali dari begitu banyak eks-tapol ini sudah meninggal karena usia tua, dan karena kesulitan hidup, atau karena sakit. Mereka tidak mendapat bantuan yang layak dari pemerintah. Juga tidak dari sanak saudara atau kerabat, yang umumnya juga sudah sulit untuk hidup mereka sendiri, atau karena takut memberi bantuan.

Jadi, jumlah keluarga atau sanak-saudara  (dekat maupun jauh) para korban pembunuhan dan pemenjaraan adalah besar sekali. Mereka ini banyak yang menjauhi atau putus hubungan dengan keluarga para korban Orde Baru ini. Dan keadaan  demikian ini berlangsung selama 32 tahun pemerintahan Suharto. Bahkan, walaupun rejim militer sudah tumbang dalam tahun 1998, namun nasib sebagian terbesar para korban Orde Baru masih  belum banyak berobah. Sampai sekarang !!!

Sampai sekarang, masih saja ada banyak orang yang menyembunyikan hubungan kekeluargaan mereka dengan para korban Orde Baru (anggota PKI, korban pembunuhan massal atau para eks-tapol). Padahal, mereka sama-sama tidak bersalah apa-apa sama sekali !.



Pelanggaran HAM selama puluhan tahun

Dari itu semua nyatalah dengan jelas sekali bahwa Suharto dengan Orde Baru-nya  telah melakukan pelanggaran HAM secara besar-besaran, terhadap puluhan juta orang, lagi pula dalam jangka lama sampai puluhan tahun. Kejahatan terhadap kemanusiaan yang begitu besar dan begitu luas dan juga begitu lama  (dan begitu biadab pula !)  ini tidaklah boleh dilupakan sama sekali oleh bangsa kita, termasuk oleh generasi-generasi yang akan datang.

Melupakan kejahatan besar  Orde Baru terhadap HAM adalah sikap politik yang salah dan sikap moral yang sesat. Dan cuwèk saja atau selalu bersikap masa bodoh terhadap penderitaan begitu lama dari jutaan para korban rejim Suharto mencerminkan hati nurani yang rusak atau akhlak yang tidak sehat.

Sebaliknya, mengutuk banyak dosa-dosa berat rejim militer Suharto di berbagai bidang  adalah tindakan yang sah atau  menghujatnya sebesar mungkin adalah  cermin hati nurani yang benar dan mulia. Orang-orang yang bernalar sehat dan menjujung tinggi-tinggi perasaan kemanusiaan tentunya akan tidak menyukai pelanggaran-pelanggaran HAM yang begitu banyak dan begitu serius oleh Orde Baru.

Sebab, tidak bisa dibayangkan, sudah berapa banyak darah yang ditumpahkan dalam pembantaian jutaan orang-orang tidak bersalah, atau berapa banyak air mata yang sudah dicucurkan oleh jutaan keluarga para korban Orde Baru selama puluhan tahun itu di seluruh Indonesia.

Tidak pernah ada dalam sejarah bangsa Indonesia kejadian yang begitu merusak persatuan bangsa,  yang begitu menyayat-nyayat hati rakyat banyak, yang begitu mengotori fikiran banyak orang, seperti yang dilakukan oleh Orde Baru  (dengan para pendukungnya terutama pimpinan Angkatan Darat dan Golkar waktu itu )  sebagai kelanjutan G30S.



Dosa besar Orba lainnya : pengkhianatan terhadap Bung Karno

Dosa besar  rejim Suharto tidak hanya berupa  pelanggaran HAM secara besar-besaran dan biadab terhadap seluruh golongan kiri di seluruh Indonesia, melainkan dibarengi dengan penggulingan  secara khianat terhadap Pemimpin Besar Revolusi Bung Karno. Pengkhianatan pimpinan Angkatan Darat (waktu itu) terhadap Bung Karno adalah dosa besar lainnya dari Orde Baru terhadap bangsa dan negara kita.

Dosa besar pengkhianatan Suharto dkk terhadap Bung Karno adalah dosa raksasa yang tidak bisa, dan tidak boleh, dan juga  tidak layak untuk dilupakan  (apalagi,  tidak bisa dima’afkan !) oleh bangsa kita, termasuk generasi bangsa di kemudian hari.

Sebab, seperti dibuktikan oleh sejarah, dan juga berdasarkan kesaksian kita masing-masing selama ini,  pengkhianatan Suharto dkk dengan menggulingkan Bung Karno sama sekali bukanlah merupakan penyelamatan bangsa dan negara, melainkan sebaliknya :  perusakan dan pembusukan di segala bidang, yang dampaknya atau akibatnya kelihatan nyata sekali dalam situasi sekarang !!!

Dengan menggulingkan Bung Karno secara khianat  -- melalui « kudeta merangkak »-nya -- Suharto dkk telah merusak jiwa asli proklamasi Republik Indonesia, memelintir atau memalsu Pancasila dan Bhinneka Tunggal Ika, membunuh elan revolusioner rakyat Indonesia, memadamkan ajaran-ajaran revolusioner Bung Karno yang sudah dikobarkannya puluhan tahun  sejak muda  di tahun 1926.

Dengan menyebarkan dalih bahwa Bung Karno tidak mau bertindak tegas terhadap para pelaku G30S, dan bahkan menguar-uarkan isapan jempol bahwa Bung Karno « tersangkut » peristiwa ini atau menjadi « dalang » yang sebenarnya, maka akhirnya ia digulingkan oleh Suharto dkk melalui sidang MPRS yang sudah dipreteli dan di-« vermaak » untuk kepentingan golongan militer waktu itu.



Latar belakang mengapa Bung Karno digulingkan Suharto

Suharto dkk menggulingkan Bung Karno karena Bung Karno merupakan satu-satunya  (sekali lagi : satu-satunya !) tokoh besar yang bisa mempersatukan seluruh bangsa Indonesia dengan ajaran-ajaran revolusionernya, antara lain : Pancasila, Manifesto Politik (Manipol), NASAKOM, Berdikari, Trisakti, yang pada pokoknya adalah berorientasi pro-wong cilik, pro-masyarakat adil dan makmur dan anti-imperialis.

Bung Karno, pemimpin nasionalis kiri revolusioner yang merupakan tokoh tinggi dunia ketiga (Asia-Afrika-Amerika Latin ) dan gerakan non-blok, dan penggagas utama lahirnya « poros » Jakarta-Pnompenh-Hanoi-Peking-Pyongyang (yang pada pokoknya adalah poros anti-AS) merupakan musuh besar imperialis AS.

Sudah sejak permulaan tahun-tahun 50-an, imperialis AS dan sekutu-sekutunya berusaha  melemahkan, atau melumpuhkan, bahkan menghilangkan kepemimpinan Bung Karno, karena politiknya yang tidak menguntungkan imperialis AS (ingat, antara lain : persahabatan dengan  RRT, peristiwa Korea Utara dan Selatan, persoalan Taiwan, konferensi Bandung, Ganefo dll dll).

Imperialisme AS (dan sekutu-sekutunya)  sejak lama « menggarap » sebagian pimpinan Angkatan Darat untuk dijadikan « our local friends » (teman-teman lokal)., melalui training militer dalam institut-institut di AS dan lewat saluran-saluran partai Masyumi dan PSI  (ingat antara lain : peristiwa 3 Selatan, dan  kemudian terbentuknya dewan-dewan militer di berbagai daerah yang contohnya : Dewan Gajah di Sumatra Utara, Dewan Banteng di Sumatra Barat, Dewan Garuda di Sumatera Selatan, Dewan Manguni di Sulawesi)

Pada umumnya, Dewan-dewan militer ini mempunyai sikap yang anti Bung Karno (atau mempersoalkan kepemimpinannya) dan juga jelas-jelas anti-PKI. Puncak dari sikap anti Bung Karno dan anti-PKI ini kemudian kelihatan lebih jelas sekali  dengan meletusnya pembrontakan PRRI-Permesta dalam tahun 1958, yang seperti dibuktikan oleh sejarah , mendapat bantuan besar dari imperialisme AS.

Jadi, mengingat itu semua, maka kita bisa melihat bahwa penghancuran kekuatan kiri di Indonesia dan sekaligus juga penggulingan secara khianat terhadap Bung Karno akibat peristiwa G30S ada hubungannya dengan sikap sebagian pimpinan Angkatan Darat yang sejak lama sudah anti Bung Karno dan anti PKI, dan yang sejalan atau searah dengan kepentingan imperialisme AS.



Menghancurkan PKI lebih dulu untuk melumpuhkan Bung Karno

Peristiwa G30S merupakan  peluang besar bagi sebagian pimpinan Angkatan Darat dan kekuatan imperialisme AS untuk mewujudkan rencana yang sudah sejak lama  mereka impi-impikan, yaitu : mengakhiri  kepemimpinan Bung Karno dengan menghancurkan terlebih dulu kekuatan kiri di Indonesia yang dipelopori oleh PKI.

Dan tergulingnya Bung Karno yang disusul dengan penahanannya serta dibiarkannya menderita sakit parah sampai wafatnya merupakan dosa besar rejim Suharto dkk yang tidak bisa   - dan tidak boleh !!! -  dima’afkan oleh orang-orang yang berhati nurani yang bersih  dan berfikiran waras.

Sebab, sejak hilangnya Bung Karno sebagai  pemimpin dan guru bangsa, atau sebagai pedoman moral dan kehidupan politik  revolusioner, atau sebagai pengayom kepentingan  seluruh rakyat, maka sekarang ini situasi negara dan bangsa menjadi makin rusak atau membusuk di segala bidang.

Korupsi besar-besaran yang merajalela dengan ganas sekali di semua bidang dewasa ini , kerusakan moral di kalangan elite, kebejatan akhlak yang membikin runyamnya dunia hukum dan peradilan, kebobrokan di kalangan partai-partai politik, kebusukan sikap para anggota DPR dan DPRD, adalah akibat  -- langsung maupun tidak langsung  -- dari tindakan khianat Suharto dkk  terhadap Bung Karno dan kejahatan-kejahatan besar  lainnya terhadap golongan kiri.

Itu semua layak untuk menjadi renungan kita bersama dalam menyongsong 30 September yang akan datang.
