---
layout: post
title: Pancasila Menurut Ajaran-ajaran Revolusioner Bung Karno
date: 2011-06-04
---

Hari kelahiran Bung Karno (6 Juni 1901)  kali ini kita peringati dalam situasi yang jauh lebih baik dari pada selama ini sejak Suharto dan konco-konconya (kaum reaksioner di dalam dan luar negeri)  menggulingkannya dalam tahun 1966-1967. Dengan diselenggarakannya peringatan Hari Lahir Pancasila 1 Juni baru-baru ini kedudukan Bung Karno sebagai bapak bangsa dan penggali Pancasila kembali diakui dan dihormati oleh sebagian terbesar bangsa kita.



Dengan dilancarkannya gerakan besar-besaran secara nasional untuk penguatan Pancasila (atau re-vitalisasi dan applikasi Pancasila), yang sudah disepakati oleh 9 lembaga tertinggi negara, maka dengan sendirinya sosok agung  Bung Karno sebagai tokoh tinggi pemersatu rakyat dan inspirator besar perjuangan bangsa makin menjulang tinggi untuk selanjutnya.



Perkembangan baik seperti itu sangat penting untuk hari kemudian bangsa dan negara, yang dewasa ini sedang dalam keadaan yang serba membusuk, atau rusak, atau bobrok, dan kehilangan arah, akibat kebejatan moral para pejabat-pejabat dan tokoh-tokoh terkemuka masyarakat pada umumnya, di seluruh negeri dan yang sudah berlangsung lama.



Di tengah-tengah kerusakan moral atau membusuknya akhlak yang tercermin di seluruh kehidupan bangsa dan negara (terutama di kalangan atas) itulah peringatan Hari Lahir Bung Karno sekarang ini makin terasa penting sekali.



Sebab, begitu hebatnya keparahan penyakit-penyakit mental dan begitu banyaknya masalah-masalah berat yang dihadapi bangsa kita sehingga  - sekarang ini  -  tidak ada seorang pun yang bisa memastikan apakah situasi yang serba semrawut dan bejat sekarang ini bisa diatasi, baik  dalam waktu dekat maupun dalam jangka panjang.



Orde Baru adalah perusak jiwa Pancasila



Sudah sebegitu besarnya  kebobrokan budi dan sebegitu luasnya pembusukan rochani di banyak kalangan masyarakat sehingga segala macam teori muluk-muluk tidak bisa dipakai lagi untuk mengatasinya, dan segala macam ayat-ayat suci juga tidak mempan untuk mencegahnya atau mengobatinya. Segala mesjid atau mushollah dan gereja serta klenteng dan kuil juga seperti dikentuti atau diludahi saja oleh para koruptor dan berbagai macam penjahat lainnya.



Karenanya, di bawah Suharto dan penerus-penerusnya,  NKRI sudah berubah menjadi negaranya para  maling berdasi, pejabat-pejabat yang bertindak sebagai penjahat,,perampok-perampok uang negara dan rakyat, dan penipu-penipu besar yang suka mengucapkan sumpah palsu di depan Tuhan atau sembahyang dengan tujuan-tujuan yang haram.



NKRI yang dikelola   -- dari yang paling atas sampai yang  paling bawah  -- oleh orang-orang yang sehaluan dengan Suharto dan anti Bung Karno itulah  yang merupkan sumber dari segala macam penyakit parah bangsa sekarang ini.



Kalau sama-sama kita telaah dalam-dalam, dan kalau kita renungkan akibat-akibatnya, maka jelaslah bahwa rejim militer Orde Baru adalah perusak besar NKRI, dan pengkhianat cita-cita para perintis kemerdekaan, dan pengotor Pancasila, dan penjegal revolusi rakyat, dan pembunuh Bung Karno, dan penginjak-injak ajaran-ajaran revolusionernya.



Karenanya, ketika kita semua memperingati Hari Lahirnya Bung Karno perlulah kiranya kita ingat juga kepada dosa-dosa besar dan pengkhianatan Suharto beserta pendukung-pendukung setianya . Sebab, pengkhianatan Suharto (beserta konco-konconya) terhadap Bung Karno  adalah -- pada hakekatnya --  pengkhianatan terhadap seluruh kekuatan yang mendukungnya. Pengkhianatan Suharto (bersama sekutu-sekutunya dalam dan luar negeri)  terhadap Bung Karno merupakan kerugian besar sekali bagi rakyat Indonesia, dan kelumpuhan bagi kekuatan-kekuatan revolusionernya, serta kelemahan bagi perjuangan menentang imperialisme dalam segala bentuknya.



Lahirnya Bung Karno adalah anugerah agung bagi bangsa

Dengan lahirnya Bung Karno dalam tahun 1901 di Surabaya (bukan di Blitar seperti yang sering dikatakan selama ini) maka bangsa Indonesia dianugerahi dengan seorang putera yang paling agung dalam sejarah Indonesia. Sejak umur belasan tahun (16 tahun) ia sudah belajar dalam perjuangan politik di bawah bimbingan HOS Tjokroaminoto, tokoh raksasa Sarekat Islam. Ketika baru umur 26 tahun, ia sudah mengeluarkan gagasan besarnya dalam tulisannya « Nasionalisme, Islamisme dan Marxisme », suatu gagasan yang kemudian dikembangkannya menjadi konsepsinya yang terkenal dengan NASAKOM.



Tanpa menelusuri kembali secara terperinci sejarah hidup Bung Karno sudah dapat dikatakan dengan tegas dan gamblang bahwa hidup Bung Karno adalah terutama sekali untuk perjuangan rakyat Indonesia, baik untuk melawan kolonialisme dan merebut kemerdekaan, dan kemudian diteruskan dengan perjuangan untuk melawan imperialisme dan neo-kolonialisme dan memperjuangkan masyarakat adil dan makmur, yaitu sosialisme à la Indonesia



Untuk itu, Bung Karno sudah menyumbangkan jasa-jasanya yang besar dan berharga sekali, bukan saja dengan meringkuk dalam penjara dan hidup di pembuangan di Ende (Flores) dan Bengkulu, tetapi juga dengan melahirkan gagasan-gagasan besarnya untuk bangsa, antara lain : Pancasila,Bhinneka Tunggal Ika, NASAKOM, Panca Azimat Revolusi, Manipol, USDEK, BERDIKARI, RESOPIM,TAVIP.



Gagasan-gagasan  besarnya atau pemikiran-pemikiran agungnya untuk bangsa ini masih dapat terus disimak kembali oleh setiap orang dengan membuka halaman buku-bukunya yang amat penting dan bersejarah seperti « Dibawah Bendera Revolusi » dan « Revolusi Belum Selesai » atau buku-bukunya yang lain.



Bung Karno tokoh besar di dunia internasional

Dengan melihat kembali sejarah perjuangan Bung Karno sebelum menjadi presiden dan juga selama jadi presiden (sebelum dikhianatinya oleh Suharto) maka jelaslah bahwa tidak ada pemimpin Indonesia lainnya yang mempunyai sosok yang sebesar dan  setinggi dirinya. Bung Karno adalah satu-satunya pemimpin bangsa yang mumpuni dalam banyak hal yang berkaitan dengan kepentingan rakyat, negara dan bangsa. Tidak ada orang lain yang bisa menandinginya apalagi melebihinya !!!



Kebesaran atau keagungan Bung Karno justru terletak di sini, yaitu cintanya yang tak terbatas (dan sunugguh-sungguh !!!), kepada Indonesia, kepada rakyatnya, kepada negaranya dan kepada bangsanya. Untuk itu pulalah ia telah selalu mencurahkan segala-galanya dalam mendorong semua kekuatan bangsanya untuk menjalankan revolusi terus-menerus dengan memegang teguh ajaran-ajaran revolusionernya.



Tetapi, kebesaran dan keagungan Bung Karno tidaklah terbatas di Indonesia saja, melainkan juga menjangkau negara-negara lain di dunia, terutama di Asia-Afrika dan Amerika Latin, dalam perjuangan melawan imperialisme (terutama AS). Ini tercermin dalam prestasi besarnya untuk menyelenggarakan konferensi AA di Bandung, kemudian GANEFO dan KIAPMA (Konferensi Internasional Anti Pangkalan Militer Asing) di Jakarta, dan dukungannya kepada berbagai gerakan-gerakan solidaritas Asia-Afrika (contoh : PWAA –Persatuan Wartawan Asia-Afrika, OSRAA –Organisasi Setiakawan Rakyat Asia-Afrika, KPAA –Konferensi Pengarang Asia-Afrika, KJAA –Konferensi Jurist Asia-Afrika)



Itu semualah yang  membuat Bung Kano menjadi sosok internasional yang sejajar  (bahkan juga melebihi) tokoh-tokoh termashur seperti Kwame Nkrumah dari Ghana, Nasser dari Mesir, Tito dari Yugoslavia, Nehru dari India, Ho Chi Min dari Vietnam, dan Mao Tse-tung , Chou En-lai dari Tiongkok. Tidak ada pemimpin Indonesia lainnya yang bisa mencapai kedudukan begitu tinggi dan terhormat di bidang internasional. Suharto tidak, dan lain-lainnya pun tidak ! Dan itu semua jugalah yang menjadikan Bung Karno sebagai musuh nomor yang berbahaya  sekali bagi kekuatan imperialis di dunia, termasuk agen-agennya di Indonesia.





             Pancasila adalah penjelmaan jiwa besar Bung Karno

Dalam berbagai kegiatan untuk memperingati Hari Lahir Bung Karno kali ini pastilah banyak orang akan ingat kepada Pancasila. Hal yang demikian adalah wajar-wajar saja, bahkan sudah semestinya harus begitu, atau tidak bisa lain. Sebab, Pancasila dan  Bung Karno adalah satu, dan tidak bisa dipisahkan. Pancasila adalah Bung Karno dan Bung Karno adalah juga Pancasila. Pancasila adalah penjelmaan semua gagasan luhur dan agung Bung Karno untuk rakyat, bangsa dan negara, Indonesia.



Oleh karena itu, seseorang yang mau mengerti betul-betul isi atau jiwa Pancasila sudah seharusnyalah  mengerti betul-betul jiwa dan gagasan-gagasan agung Bung Karno. Orang yang tidak mengerti, atau tidak mau mengerti, gagasan-gagasan agung dan ajaran-ajaran revolusioner Bung Karno, tidak akan mengerti isi atau jiwa Pancasila. Dan karena itu pulalah kita katakan bahwa orang-orang yang anti Bung Karno serta anti ajaran-ajaran revolusionernya adalah dengan sendirinya anti Pancasila pula.



Dalam kaitan ini perlu sama-sama kita ingatkan  (dan juga amati )  bahwa gerakan besar-besaran secara nasional untuk me-revitalisasi dan applikasi Pancasila yang sedang dilancarkan dewasa ini haruslah dijalankan  dengan isi atau  jiwa Pancasila yang asli, yaitu yang menurut gagasan agung dan ajaran-ajaran revolusioner Bung Karno sendiri. Revitalisasi Pancasila tidak akan bisa berhasil kalau dijalankan dengan cara-cara dan isi yang bertentangan dengan ajaran-ajaran revolusioner Bung Karno.



Ajaran-ajaran Bung Karno masih relevan sekarang ini

Ajaran-ajaran revolusioner Bung Karno adalah khazanah yang kaya sekali dengan petunjuk atau bimbingan dalam perjuangan rakyat di bidang politik, ekonomi, sosial, kebudayaan, moral, yang meliputi masalah-masalah pemerintahan, pendidikan, militer, perjuangan melawan imperialisme, dan usaha untuk menciptakan sosialisme à la Indonesia, di bawah bendera Pancasila dan Bhinneka Tunggal Ika.



Banyak sekali dari berbagai ajaran-ajaran revolusioner Bung Karno itu yang masih relevan, atau masih sesuai dengan kebutuhan situasi dewasa sini, baik yang mengenai masalah dalam negeri maupun luarnegeri. Umpamanya, nation building and character building, patriotisme kerakyatan, semangat gotong royong, pemupukan rasa pengabdian kepada kepentingan rakyat, perjuangan menentang imperialisme atau neo-liberalisme, perjuangan untuk sosialisme à la Indonesia.



Di antara ajaran-ajaran atau gagasan Bung Karno yang sudah mendapat pengakuan, penghormatan, dan dukungan dari sebagian terbesar sekali rakyat Indonesia adalah Pancasila. Tetapi bukanlah Pancasila model P4-nya rejim Orde Baru, yang diselewengkan atau dipalsukan dan dipaksakan secara memuakkan  selama puluhan tahun. Pancasila-nya rejim militer Suharto adalah pada hakekatnya anti-Pancasila yang asli, karena digunakan untuk mengkhianati Bung Karno dan menghancurkan kekuatan revolusioner pendukung berbagai politiknya, dan  melumpuhkan golongan kiri yang dipelopori PKI.



Seluruh bangsa Indonesia, mulai dari generasi para pejuang perintis kemerdekaan (termasuk yang dipenjara pemerintahan kolonial Belanda dan dibuang ke Digul) sampai kepada generasi yang berjuang untuk  revolusi Agustus 45  -- dan  juga semua generasi-generasi yang akan datang !  --  mendapat karunia besar dengan lahirnya Bung Karno, yang di kemudian harinya menyumbangkan Pancasila (dan berbagai ajaran-ajaran revolusionernya yang lain) kepada seluruh rakyat Indonesia.



Pancasila adalah kiri dan anti neo-liberalisme

Pancasila yang lahir dari penggalian atau penggagasan Bung Karno adalah satu ideologi pembimbing untuk mempersatukan bangsa Indonesia yang majemuk,dan terdiri dari ratusan suku bangsa, adat, agama dan aliran kepercayaan, dan berjumlah 240 juta jiwa yang hidup di 17 000 pulau-pulau.



Ketika negara dan bangsa kita sedang menghadapi kebobrokan dimana-mana dan berbagai penyakit parah akibat ulah para maling berdasi dan  penjahat (pejabat !!!) yang menyalahgunakan hukum dan menginjak-injak rasa keadilan dan bersekongkol dengan kepentingan asing (neo-liberalisme) maka ajaran-ajaran revolusioner Bung Karno menghadapi kekuatan asing terasalah makin diperlukan dewasa ini.



Untuk menghadapi itu semualah Pancasila-nya Bung Karno merupakan senjata di tangan bangsa kita. Sebab, menurut ajaran-ajaran Bung Karno Pancasila adalah kiri, adalah progresif, adalah anti-imperialis, adalah senjata untuk menciptakan masyarakat adil dan makmur, yaitu sosialisme à la Indonesia.



(Mengingat pentingnya arti ungkapan Bung Karno bahwa Pancasila adalah kiri atau berjiwa anti-imperialis dan pro-sosialisme, maka akan diusahakan adanya tulisan tersendiri).



Untuk memperingati Hari Lahir Bung Karno kali ini, tulisan ini ditutup dengan kutipan bait-bait lagu Franky Sahilatua « Pancasila rumah kita » yang mempunyai arti yang dalam dan juga mengandung pesan yang besar, yang antara lain berbunyi sebagai berikut :

« Pancasila rumah kita, rumah untuk kita semua. Nilai dasar Indonesia, rumah kita selamanya. Untuk semua keluarga saling menyatu, untuk semua saling membagi. Pada setiap insan sama dapat, sama rasa, oh Indonesiaku, oh Indonesiaku,” (kutipan selesai)



Akhir kata, Bung Karno telah lahir dan berjuang sepanjang hidupnya, dan sampai wafatnya ia tetap setia dan memegang  teguh kepada ajaran-ajarannya, antara lain Pancasila. Bung Karno telah tiada, namun Pancasila tetap ada, dan untuk selama-lamanya !
