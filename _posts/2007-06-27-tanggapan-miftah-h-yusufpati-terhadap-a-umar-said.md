---
layout: post
title: Tanggapan Miftah H. Yusufpati terhadap A. Umar Said
date: 2007-06-27
---

Buku yang saya tulis "HM Soeharto, Membangun Citra Islam" mendapat
tanggapan khusus A. Umar Said tanpa merasa perlu membaca buku
tersebut, padahal saya sangat berharap Tuan Umar membaca dulu baru
berkomentar agar tanggapan yang diberikan menjadi jernih dan layak
dikonsumsi untuk kemaslahatan orang banyak. Buku tersebut
diluncurkan menjelang hari ulang tahun mantan presiden Soeharto yang
ke-86, yang jatuh pada tanggal 8 Juni 2007 dan Tuan Umar
mengomentarinya pada 12 Juni 2007 setelah membaca berita dari Kantor
Berita Nasional Antara.

Saya mencoba menunggu komentar lain dari kalangan netter karena Tuan
Umar telah mengirim tulisannya tersebut ke sejumlah mailing list dan
blog. Akan tetapi tanggapan lain yang saya tunggu tak juga muncul
kecuali hanya beberapa, itupun tak begitu penting karena hanya
memberikan suport kepada Tuan Umar dengan slogan "salam kiri".

Saya ingin menanggapi tulisan Tuan Umar dengan pertimbangan agar
para pembaca juga mengetahui pendapat saya tentang apa yang sudah
ditulis Tuan Umar tersebut. Saya juga tidak bisa menutup mata atau
merasa cuek dengan tanggapan yang begitu serius dari Tuan Umar.

Tuan Umar menulis bahwa judul "HM Soeharto Membangun Citra Islam"
sebagai menyesatkan. Ia mengutip pernyataan saya yang dimuat Antara
bahwa "Banyak perjuangan yang sudah dilakukan Soeharto terhadap
Islam. Pak Harto terbukti telah menegakkan prinsip-prinsip ajaran
Islam dan memperjuangkan toleransi beragama."

Lalu Tuan Umar beropini; "Mungkin saja di antara kita ada yang
tercengang, atau mungkin juga ada yang tertawa terkekeh-kekeh, atau,
ada pula yang jengkel dan marah-marah. Sebab, mungkin saja, ada
orang yang menganggapnya sebagai lelucon, atau sebagai bualan alias
omong-kosong, atau bahkan sebagai hinaan yang amat besar kepada
Islam."

Saya baca berkali-kali kalimat tersebut; sangat terasa menghinakan,
pernyataan tendensius dan apriori, tapi biarlah. Tuan Umar boleh
beropini apa saja. Mungkin juga karena Tuan Umar jarang ke Indonesia
dan terhanyut oleh provokasi kalangan pendendam Soeharto atau boleh
jadi Tuan Umar adalah bagian dari para pendendam itu sehingga
menganggap rakyat Indonesia ini sebagai sosok-sosok pembenci
Soeharto sehingga perlu tertawa terkekeh-kekeh karena menganggap
judul buku dan pernyataan saya sebagai lelucon, bualan atau omong
kosong.

Akan tetapi begitu membaca tentang cv Tuan Umar serta beberapa
tulisannya dalam situs pribadinya saya jadi maklum. Sebagaimana
dikatakan KH. Dr. dr. Tarmizi Taher dalam bedah buku dimaksud saat
mengomentari bahwa AM Fatwa memaaafkan Soeharto; "Hanya orang-orang
PKI yang tidak memaafkan Soeharto." Lalu, saya pun mencoba menerka
mungkin Tuan Umar adalah bagian dari tokoh yang disebut Tarmizi
tersebut.

Selanjutnya mari kita baca ulang tulisan Tuan Umar. Ia mengajak para
pembaca untuk merenungkan secara dalam-dalam apakah sebutan "HM
Soeharto membangun citra Islam" itu memang mengandung kebenaran, dan
apakah sebutan itu akan mengangkat Islam, ataukah sebaliknya,
malahan memperburuk atau merusak citra Islam. Sebab, terbitnya buku
dengan judul yang demikian itu untuk memperingati hari ulangtahun
Soeharto (Umar menulisnya Suharto-- sesuai EYD, katanya)
mencerminkan bahwa di kalangan sebagian golongan Islam memang
terdapat anggapan bahwa Soeharto adalah tokoh pembangun citra Islam
dan "penegak prinsip-prinsip ajaran Islam". Hal yang demikian ini
adalah sangat serius sekali. Dan menyesatkan sekali!

"Oleh karena itu, alangkah baiknya, kalau masalah ini dibicarakan
dan ditelaah dengan teliti oleh para tokoh agama Islam di Indonesia,
dan dijadikan bahan studi atau riset di universitas-universitas dan
lembaga-lembaga ilmiah Islam. Singkatnya, masalah ini perlu sekali
menjadi kajian secara luas oleh ummat Islam di Indonesia. Sebab,
masalah apakah Soeharto telah "membangun citra Islam" atau malahan
merusak citra Islam, adalah masalah penting yang bisa dipakai untuk
menilai berbagai politik rejim militer Orde Baru, dan juga untuk
menelaah sikap berbagai golongan Islam terhadap Soeharto dan Orde
Baru. Sikap berbagai golongan Islam terhadap Soeharto dan Orde Baru
adalah satu hal yang mempunyai pengaruh penting dalam persoalan
politik dan sosial di negara kita, yang buntutnya masih sama-sama
kita saksikan sampai sekarang ini."

Sungguh, saya sepakat bahwa judul itu serius untuk direnungkan. Akan
tetapi kalau judul itu dianggap menyesatkan tentulah kesimpulan yang
terlalu dini, tanpa melalui analisa yang matang. Ini juga sangat
berlebihan, bombastis—lebih sebagai pernyataan yang dilandasi sikap
apriori dari sosok pendendam. Tuan Umar menggambarkan dirinya seakan-
akan sangat peduli dengan Islam (semoga saja seperti itu) dan bukan
sedang menghasut umat Islam untuk ikut-ikutan menghujat Soeharto.
Teknik yang cukup cerdas dan matang sebagai seorang aktivis
pergerakan "kiri".

Sayangnya, tulisan-tulisan Tuan Umar adalah hujatan yang tak
berkesudahan; suatu sikap yang jauh dari nilai-nilai Islam. Nama
Umar diambil dari nama salah seorang khulafaurrasyidin yang
memberikan penegasan bahwa Umar seorang muslim. Itu sebabnya saya
berharap sedang berdialog dengan sesama muslim, sehingga saya patut
menanggapi apa yang ditulis Umar tersebut. Dan saya tidak ingin
mempertanyakan kadar keislaman Umar, sebab bukankah itu urusan Allah
semata. Sebagaimana saya juga tidak perlu mempertanyakan kadar
keislaman Soeharto.
Tuan Umar selanjutnya menulis; "Soeharto memang pernah memberikan
bantuan kepada sejumlah pesantren, memberikan sumbangan untuk
membangun mesjid-mesjid, dan memberikan dana kepada sejumlah kyai-
kyai dan para pemuka agama Islam. Soeharto juga sering memberikan
sedekah kepada anak-anak yatim piatu, atau memberi beasiswa kepada
pelajar-pelajar.

Tetapi, perlulah sama-sama kita renungkan dalam-dalam, apakah itu
semua telah dilakukan Suharto oleh karena memang ingin melaksanakan
ajaran-ajaran Islam dengan sungguh-sungguh dan secara tulus, ataukah
karena sebab-sebab dan pertimbangan lainnya. Selama pemerintahan
rejim militer Orde Baru, memang Soeharto berusaha merangkul, menina-
bobokkan, merekayasa, menggunakan, mensalahgunakan, bahkan menipu
atau menjerumuskan sebagian golongan Islam."

Sejauh mana atau sebesar apa, dan juga bagaimana saja cara-caranya
Soeharto telah "merangkul" dan "menggunakan" sebagian golongan Islam
ini, barangkali berbagai kalangan Islam sendiri dapat menganalisanya
atau menelitinya berdasarkan pengalaman-pengalaman pahit mereka
masing-masing. Yang sudah jelas yalah adanya kebutuhan atau
keharusan bagi Soeharto (artinya rejim militer Orde Baru)
untuk "merangkul" sebanyak mungkin golongan Islam, demi kelanggengan
diktatur militernya, dan untuk melawan lawan-lawan politiknya.

Pandangan Tuan Umar begitu tendensius, penuh fitnah dan menghasut
dengan kelincahan goresan penanya tanpa data yang memadai. Kendati
demikian, analisa seperti itu layak dikomentari. Menurut hemat saya,
dalam kadar tertentu, tak ada salahnya menilai secara obyektif
motivasi Soeharto menjalankan nilai-nilai Islam seperti itu. Lagi
pula, Tuan Umar juga tidak sendirian dalam menilai ritual keagamaan
Soeharto sebagai memiliki dimensi politis di samping tentu saja tak
sedikit pula yang menilai secara obyektif, tak membabi buta seperti
itu.

Donnald Emmerson, misalnya, menilai tidak semua aktivitas ritual
Soeharto sebagai siasat politik melulu. Apalagi bila dihubungkan
bahwa Soeharto pernah mengenyam pendidikan di Muhammadiyah. Dan
lebih penting lagi adalah keputusan Soeharto untuk mengambil KH
Qosim Nurzeha, seorang da'i dari Bintal Angkatakan Darat, sebagai
pembimbing kehidupan keagamaan Soeharto dan keluarganya.

Peranan KH Qosim Nurzeha dalam melakukan "Islamisasi" keluarga
Presiden Soeharto hampir menjadi persepsi umum terutama
dikumandangkannya tahlil berkali-kali pada prosesi pemakanan Ibu
Tien Soeharto pada 1996 yang jelas menampakkan kultur Islam.

Mengenai semakin meningkatnya penghayatan keagamaan Pak Harto, Ketua
MUI, KH Hasan Basri mengatakan: "Sebagai orang Jawa, saya lihat
nilai-nilai spiritualnya sangat dominan. Mungkin pengaruh pendidikan
beliau yang sejak kecil sekolah Muhammadiyah. Sedangkan di rumah, ia
belajar agama dengan gurunya, KH Qosim Nurzeha. Dan perubahan baru
yang saya lihat justru setelah beliau pulang ibadah haji dengan
membawa nama baru... Sejak itu keberagamaan Presiden lebih intens,
imannya tambah kuat. Sekarang segala kebijakan yang diambilnya
dikembalikan pada nilai-nilai agama. Pola pikir beliau tidak hanya
berdasarkan akal semata."

Pendapat KH Hasan Basri patut dijadikan rujukan karena selain beliau
dekat dengan Soeharto, sebagai ulama, pendapatnya tentu tulus dan
jujur. Tapi bila Tuan Umar juga memasukkan kiai yang sangat
terhormat itu ke dalam golongan tokoh Islam yang "dibeli" maka patut
dipertanyakan tentang kejiwaan Tuan Umar.

Saat Soeharto mengakomodasikan kepentingan umat Islam, sampai
kemudian berdirinya ICMI (Ikatan Cendekiawan Muslim se-Indonesia)
juga muncul tafsir bermacam-macam dari kalangan umat Islam sendiri.
Sebagaimana ditulis Tuan Umar tadi, maka sebagian kalangan juga ada
yang menganggap sikap presiden pada waktu itu sebagai bagian untuk
mencari sandaran politik baru pasca renggangnya hubungan dirinya
dengan ABRI terutama dengan LB Moerdani. Mengomentari pendapat
seperti itu Dr. Imaduddin Abdurrahim menyatakan: "Kini yang terjadi
bukan hanya sebuah upaya presiden untuk mengamankan dukungan
sehingga bisa meredam pesaing-pesaingnya di militer. Tentu saja,
terdapat ukuran-ukuran politik seperti itu, namun saya tak terlalu
sinis. Saya kira Presiden Soeharto mempunyai mata dan ia menyadari
bahwa 90% rakyatnya adalah muslim dan mereka harus diberikan peran
dalam kehidupan nasional. Ini sebuah awal yang murni. Untuk pertama
kali dalam 27 tahun kami diajak dalam kehidupan poltik negeri ini,
dan kami harus mengambil keuntungan darinya. Kenyataan ini mungkin
tak memuaskan harapan semua orang, namun sebuah kesempatan nyata."

Imadudin Abdurahim adalah intelektual muslim yang bukan termasuk
penikmat jabatan di zaman Orde Baru sehingga pendapatnya menjadi
sangat penting untuk dikutip.

Tokoh lainnya, yakni Prof. Dr. Ismail Sunny menyatakan, "Tak perlu
terlalu menyederhanakan motivasi Presiden. Presiden menyadari bahwa
bila terdapat kelompok besar yang menentangnya, hal itu tak baik
bagi kepentingan sendiri. Maka, tidak, ini bukan taktik sesaat di
pihaknya, karena ia menaruh perhatian tak hanya pada pemilu tetapi
juga dengan proses Islamisasi yang bakal memberikan pengaruh jangka
panjang pada masyarakat Indonesia."

Sebagai catatan, Ismail Sunny adalah Guru Besar Hukum Tata Negara
Universitas Indonesia, yang punya pengalaman tidak menyenangkan saat
berpolitik di zaman Orde Baru. Apakah Tuan Umar juga ingin
mengatakan intelektual Islam sekaliber Imaduddin dan Ismail Sunny
juga "dibeli" Soeharto?

Selanjutnya, Tuan Umar mungkin merasa kurang pas dengan pendapat
para tokoh tersebut. Okelah, saya mencoba menerima pendapat Tuan
Umar, andaikata tindakan Soeharto mengadobsi kepentingan Islam dan
umat Islam itu berdimensi politik sekalipun sesungguhnya siapa yang
diuntungkan dan siapa yang dirugikan? Ataukah Tuan Umar menghendaki
agar Soeharto tidak perlu melakukan itu dan menindas saja umat Islam
supaya apa yang dipersepsikan Tuan tentang Soeharto menjadi
kenyataan?

"Soeharto sebagai musuh umat Islam atau merusak citra Islam?"
Tuan Umar menulis; "(Soeharto) Merangkul golongan Islam untuk
bersekutu dengan AS." Ah, Tuan Umar memfitnah lagi. Dan lagi-lagi,
Tuan Umar tidak menunjukkan data yang kongkrit tentang persekutuan
itu sehingga kita juga sulit menilai apakah persekutuan dimaksud
merugikan umat Islam atau menguntungkan? Atau mungkin, Tuan Umar
menganggap bersekutu dengan AS dalam bentuk apa pun sebagai tindakan
haram, sebagaimana dikatakan pula oleh Osama bin Ladin.

Tentang apa yang disebut dengan "persekutuan" dengan AS (baca Barat)
hendaknya kita perlu berpikir jernih, tidak hantam kromo. Posisi ini
pula yang telah diambil oleh Soeharto dan Orde Baru; bergaul dengan
Barat dilakukan tanpa mengimpor secara mentah-mentah faham Barat,
apalagi membebek dan mengabdi pada mereka. Satu pergaulan yang win
win solution. Membenci Barat tak akan menyelesaikan masalah.

Kita juga tahu bahwa benturan kepentingan yang berlangsung ratusan
tahun antara Islam, Kristen dan Barat telah menimbulkan reaksi umat
Islam yang cukup beragam. Barat mengusung berbagai ideologi yang
sedikit banyak mempengaruhi pola berpikir dan bertindak umat Islam.
Di antara ideologi yang diusung oleh Barat adalah ide sekularisasi
dan kapitalisme. Selain itu, keberagaman respon juga dipengaruhi
oleh situasi lokal, kultur dan faham keagamaan. Secara kategoris,
bentuk respon umat Islam tersebut terbagi ke dalam tiga kategori
utama.

Pertama, sebagian kalangan menerima ide sekularisasi Barat tanpa ada
reserve. Respon ini diperlihatkan terutama oleh gerakan Mustafa
Kamal Attaturk ketika melakukan sekularisasi di Turki. Mustafa Kamal
berhasil menghapus sistem kekhalifahan Islam yang terakhir.
Sekularisasi yang dilakukan Mustafa juga berhasil mengubah praktik
keberagamaan, misalnya praktik azan yang dibaca dengan bahasa Arab
diubah dengan bahasa Turki.
Kedua, sebagian kalangan lagi ada yang mengambil sekularisasi tetapi
dengan batasan dan berbagai revisi.

Respon ini diperlihatkan dalam pembentukan negara-negara Muslim yang
dibangun dengan kerangka demokrasi, seperti pembentukan Republik
Indonesia dan pembentukan negara Malaysia. Format kedua negara ini
sebenarnya dibangun atas ideologi nasionalis, namun tetap
mengupayakan okomodasi nilai-nilai religius dan kultur setempat.
Hanya saja sedikit perbedaan, Malaysia menempatkan Islam sebagai
agama resmi negara, tetapi Indonesia memposisikan Islam sebagai
agama yang dilindungi negara. Meskipun Islam dijadikan agama resmi
negara, akan tetapi Malaysia tetap menggunakan konstitusi sekuler
(artinya tidak menempatkan Al-Qur'an dan Hadist sebagai konstitusi
formal, sebagaimana layaknya negara Islam yang memposisikan al-
Qur'an han Hadist sebagai konstitusi tertinggi).

Ketiga, kelompok yang menolak segala bentuk modernitas yang berasal
dari Barat, lebih khusus lagi sekulerisme. Respon ini dapat dilihat
dalam format pembentukan negara Pakistan dan sejumlah negara Arab
yang kemudian diteruskan Iran ketika dipimpin Khomeini.

Pilihan ini, selain mendasarkan pendiriannya pada perkembangan
rasional politik, mereka sebenarnya tengah melakukan ijtihad untuk
tetap dapat survive dalam menghadapi perubahan zaman. Namun demikian
sejarah membuktikan dari ketiga respon tersebut, hanya umat Islam
yang moderat saja yang dapat dianggap bertahan, paling ideal dan
paling mempunyai masa depan cerah.

Latar pemikiran muslim moderat itu berbeda sebagaimana respon yang
pertama. Kelompok yang menerima secara total ide sekularisasi Barat
ternyata telah membawa pada situasi yang mengisolasikan diri dalam
siklus teologi Islam dan tradisinya. Kebijakan yang dilakukan oleh
Mustafa telah membawa pada situasi yang tidak kondusif. Karena
kebijakan radikal yang dilakukan Mustafa telah menimbulkan benturan
yang sangat dahsyat, antara kebijakan strukturalnya dengan kultural
dan tradisi Islam yang telah lama hidup dalam tradisi keagamaan umat
Islam.

Paradigma pertama ini tidak jauh berbeda dengan respon ketiga, yang
ekstrim. Mereka menolak segala bentuk modernitas dari Barat. Sebagai
akibatnya, kelompok ini menjadi terisolasi Islam dalam lintasan
global dan bahkan menjadi bahan atau obyek permainan negara Barat.

Sebagaimana negara-negara yang dibangun dengan semangat Islam
fundamentalis sebagai negara-negara di Timur Tengah, Pakistan dan
Afghanistan dalam perkembangan dinamika sosial-politik tidak
menunjukkan prestasi yang dibanggakan. Negara-negara tersebut tetap
menjadi negara yang terbelakang, meskipun memiliki sumber daya alam
yang berlimpah, kondisi sosial dan ekonominya tidak mengalami
kemajuan yang berarti.

Ini berbeda dengan negara-negara Islam yang mengambil sikap moderat,
negara-negara ini telah mencapai kemajuan di berbagai bidang.
Malaysia, misalnya, kini menjadi negara Islam modern yang telah
sukses membangun berbagai sektor keagamaan baik yang menyangkut
ekonomi, teknologi, pendidikan dan lembaga-lembaga keuangan Islam.

Demikian juga dengan Indonesia. Prestasi Malaysia dan Indonesia yang
membanggakan itu, kini peta kekuatan Islam seolah telah mengalami
pergeseran, semula di Timur Tengah beralih ke kawasan Asia Tenggara.
Bukti ini juga diperkuat dengan dipercayai Malaysia sebagai pemimpin
organisasi Islam yang bertaraf internasional itu. (hal 255-256).

Jadi, tidak ada yang salah dengan para pendiri bangsa ini dan
penerusnya dalam merumuskan dan menerapkan sistem bagi bangsa ini.
Begitu juga tentang persekutuan itu, sepanjang bermaslahat bagi
umat, bangsa dan negara tidak ada salahnya. Meminjam istilah Ruslan
Abdulgani; "jangan gebyah uyah".
Lalu apakah kita harus menyesali 'bersekutu" dengan AS lantaran
negeri itu di kemudian hari ternyata menyerang negeri-negeri Islam.
Fakta seperti itu tidak ada hubungannya dengan kerja sama yang
dibangun Indonesia dengan AS maupun negara lainnya selama ini.

Selanjutnya Umar menulis; "Seperti kita ketahui, stabilitas politik
dan sosial telah dapat diciptakan rejim militer Orde Baru, dengan
menggunakan tangan besi atau berbagai praktek otoriter, di samping
merangkul, membujuk, menyiasati sebagian terbesar golongan Islam.
Ada yang mengatakan bahwa Orde Baru telah berhasil "membeli"
berbagai golongan Islam, dan menjadikan mereka sebagai pendukung
berbagai politik rejim militer Soeharto, yang dalam jangka lama
adalah pro-Barat dan mengabdi kepada kepentingan politik AS...dst"

Lagi-lagi hujatan dan fitnah itu datang dari goresan tangan Tuan
Umar. Sebagaimana Tuan Umar juga ketahui bahwa masyarakat Indonesia
dalam visi Soeharto adalah masyarakat yang berdasar Pancasila, di
mana warga negara Indonesia hidup selaras dan berdampingan tanpa
mempedulikan perbedaan di antara mereka. "Orde Baru adalah orde
Demokrasi Pancasila yang mengutamakan kepentingan rakyat dan bukan
kepentingan golongan atau pribadi." Dalam sebuah kesempatan Pak
Harto mengatakan Orde Baru "mengejar institusional dan menolak
individualisasi". (hal. 57)

Tuan Umar menyebut dalam kekuasaannya Soeharto menggunakan tangan
besi. Bila yang Tuan maksud adalah terhadap kelompok ekstem kanan
atau ekstrem kiri, Soeharto memang keras terhadap mereka. Jadi
memang begitu adanya. Tuan mungkin juga telah mencatat bagaimana
negara lain dalam menghadapi kelompok seperti itu.

Lagi pula kebijakan tersebut juga dilakukan di awal-awal
pemerintahan Orde Baru yang memang "harus" diambil karena tuntutan
keadaan. Sebagaimana Tuan tahu bahwa Orde Baru dikonsepsikan sebagai
koreksi total atas pemerintahan Orde Lama yang lebih berpihak pada
kalangan kiri (komunis) dan Presiden Soekarno tidak mampu
mempertanggungjawabkan kepemimpinan berkait dengan sejumlah
persoalan seperti terbunuhnya sejumlah perwira tinggi Angkatan Darat
yang dikenal dengan G30S/PKI, kemudian runtuhnya ekonomi bangsa
dengan inflasi yang sangat tinggi serta pendapatan per kapita di
tahun 1966 yang hanya 120 dollar Amerika.

Masa awal pemerintahan Orde Baru ini ditandai dengan integrasi yang
kokoh antara umat Islam dengan militer serta kekuatan sosial lainnya
ketika mereka menumpas pemberontakan Partai Komunis Indonesia (PKI).

Pilihan stratejik Orde Baru adalah pembangunan yang berorientasi
kepada modernisasi dengan antara lain rekayasa politik melalui
restrukturisasi partai-partai politik. Soeharto tidak menjadikan
politik sebagai panglima, ia lebih menitik-beratkan pada pembangunan
untuk kesejahteraan rakyat. Pelajaran berharga tentang demokrasi
yang dikembangkan Presiden Soekarno dengan Orde Lama-nya adalah
carut marutnya tatanan politik. Itu sebabnya, pada masa Orde Baru
pengalaman buruk itu tak perlu diulangi. Terlepas Tuan setuju atau
tidak.

Tuan juga mungkin tahu bahwa sangat sulit memisahkan diri Soeharto
dengan ideologinya yang anti-komunis dan menjunjung tinggi
Pancasila. Saat menjadi prajurit ia terlibat langsung dalam
memerangi pemberontakan Partai Komunis Indonesia pada 1948 dan
menempatkan garda paling depan menumpas pemberontakan G- 30 S/PKI.
Bahkan dari sinilah jasa Pak Harto sampai sekarang dikenang.

Kisah tentang bagaimana Pak Harto menumpas PKI sudah banyak ditulis
dan diceritakan dari mulut ke mulut oleh banyak orang dengan penuh
kekaguman. Tuan mungkin tak suka dengan itu, tapi Tuan sebaiknya
masuk ke pesantren-pesantren NU atau Muhammadiyah supaya Tuan tidak
terkaget-kaget lagi bila ternyata nama Soeharto amat dipuja di sana.

Selama kepemimpinannya, Soeharto tampak sangat curiga dengan gerakan
kaum komunis. Peringatan akan bahaya komunis ini muncul dari waktu
ke waktu. Soeharto tak bosan-bosannya mengingatkan bahaya laten
komunis tersebut. "Bila kita mempelajari doktrin dan taktik-taktik
PKI .. (kita akan menyadari) bahwa partai itu akan terus melancarkan
gerakan-gerakan ilegalnya untuk berusaha muncul kembali," kata
Soeharto pada 11 Maret 1969.

Pada akhir tahun itu juga dan awal 1970, Soeharto berbicara tentang
perlunya untuk terus menindak sisa-sisa Gerakan 30 September yang
masih ada di tengah-tengah masyarakat. Tujuan PKI pada 1965 sama
dengan ketika pada 1948, kata Soeharto pada peringatan tahun kelima
Supersemar pada 1971. Lalu beliau mengatakan, "Mengubah Pancasila
dan Undang-Undang Dasar 1945, untuk diganti dengan sistem yang
lain."

Kini bahaya laten PKI masih disuarakan sejumlah tokoh masyarakat
termasuk TNI, menyusul dibukanya kran bagi mereka untuk bergerak
lebih leluasa. Bukannya tidak mungkin di kemudian hari apa yang
dikhawatirkan Soeharto tentang bahaya laten komunis itu bakal
terjadi. Jadi, Soeharto memang benci PKI terus menumpas gerakan
mereka. Tuan Umar juga tentu tahu, bahwa gerakan itu didukung kuat
umat Islam Indonesia.

Selain kepada PKI, Soeharto juga sangat keras terhadap gerakan
radikal Islam yang ingin menegakkan negara Islam di Indonesia. Tapi
Tuan Umar mungkin juga harus bersikap jujur bahwa kebijakan Soeharto
itu wajar adanya dan "harus" dilakukan untuk membangun bangsa yang
plural seperti Indonesia.

Mungkin tepat bila kita mengaca pada kebijakan Gamal Abdul Naseer
atau Anwar Sadat dalam menangani Ikhwanul Muslimin. Sadat yang lunak
akhirnya tewas di ujung senapan pasukan khusus Al-
Ikhwan. "Pertanyaannya, apakah ada jaminan bahwa bila Soeharto
membebaskan begitu saja kelompok ekstrem kanan untuk tumbuh dan
berkembang tidak akan bernasib sama seperti Anwar Sadat?"

Tuan Umar menyebut Soeharto membeli umat Islam dan dalam jangka lama
untuk mengabdi pada AS, satu pernyataan yang bisa jadi gegabah dan
fitnah. Saya tidak mau berkomentar tentang asumsi bernada menghujat
tanpa dasar seperti itu. Begitu juga Tulisan Tuan Umar selanjutnya;
asumsi-asumsi, praduga tanpa bukti, hujatan dan fitnah.

Selanjutnya, Tuan Umar, tak ada seorang pun di dunia ini yang
sempurna, termasuk Soeharto. Hanya saja, menafikkan jasa para
pemimpin bangsa ini adalah sikap yang tidak ada tempatnya di bumi
Indonesia.

Boleh jadi kita sama-sama tidak sepakat terjadinya kekerasan dalam
politik akan tetapi adalah suatu keniscayaan bahwa agama dan politik
selalu diwarnai kekerasan bahkan teror. Secara konsepsional, doktrin
Islam yang utama adalah penekanannya pada ajaran perdamaian. Kata
Islam sendiri bermakna damai, aman, selamat dan penyerahan diri.

Kendati demikian, dalam sejarahnya, konsepsi di atas tidak selalu
seiring dengan perjalanan umat Islam. Pada awal-awal lahirnya Islam,
tidak kurang dari enam kali peperangan diikuti oleh Nabi, kendati
dengan tujuan untuk menegakkan keadilan ekonomi, kesetaraan manusia
dan bertahan dari penyerangan.

Demikian pula pada masa Abu Bakar, Umar bin Khattab, Ustman Bin
Affan dan Ali bin Abi Thalib. Peperangan yang dilakukan oleh mereka
umumnya bermotifkan ekonomi, seperti penumpasan terhadap kelompok
pembangkang yang enggan membayar zakat di masa Abu Bakar; pelebaran
wilayah Islam, seperti yang terjadi pada masa Umar bin Khattab; dan
perebutan kekuasaan seperti yang terjadi pada Ustman dan Ali.

Disebutkan dalam sejarah, sepeninggal Nabi Muhammad, tak kurang dari
70.000 orang Islam mati terbunuh dalam medan peperangan. Bahkan dua
khalifah yang terakhir, yakni Usman dan Ali terbunuh secara
mengenaskan dalam suatu peristiwa peperangan. Ahmad Amin menyebut
peperangan besar dalam sejarah umat Islam ini sebagai fitnah
kehidupan yang paling besar (al-fitnah al-Kubra).

Berbagai bencana kekerasan berlangsung sepanjang kekuasaan dinasti
Islam, yakni pada masa Dinasti Umayyah dan Dinasti Abbasiyah.
Kekerasan pada masa ini tidak hanya dalam bentuk fisik, tetapi juga
kekerasan intelektual yang diwujudkan dalam wujud pembunuhan
terhadap beberapa pemikir yang dianggap dapat merugikan kekuasaan,
seperti yang terjadi pada Ghaylan al-Dimasyqi dan Al-Ja'd bin Dirham
yang berpandangan bahwa kekuasaan dalam masyarakat Islam bukanlah
monopoli keturunan Arab (bangsa Quraisy) sebagaimana yang menjadi
pandangan kelompok Sunni, atau dari keturunan Ali bin Abi Thalib
sebagaimana yang menjadi garis politik Syiah, melainkan siapa saja
dari umat Islam yang disetujui berdasarkan musyawarah (kesepakatan).

Kekerasan pada aspek politik lebih mengerikan lagi. Pada masa
Dinasti Umayyah, tepatnya dimasa Yazid bin Muawiyah, kepala Husain
bin Ali dipenggal dari tubuhnya, dan kepala tersebut dibawa ke
istana untuk dipermainkan. Seorang tua yang tahu masa kecil Husain
dengan gusar berkata: "Saya pernah melihat wajah itu diciumi oleh
Rasulullah". Peristiwa ini disebut sebagai "tragedi Karbala", sebuah
momentum perpecahan antara kelompok Islam Sunni dan Syiah pada tahun
64 Hijriah.

Demikian halnya pada masa Dinasti Abbasiyah. Akbar S Ahmed
menggambarkan upaya kudeta militer Dinasti Abbasiyah terhadap
Dinasti Umayyah sebagai berikut: "Sekali waktu Abdullah, seorang
Jendral Abbasiyah mengundang 8 orang pemimpin Umayyah untuk makan
malam pada musim panas bulan Juni tahun 750 M di rumahnya yang
terletak di Jaffa. Ketika para tamu sedang makan, mereka ditangkap
oleh para tentara. Setelah para tentara menikam semua pimpinan
Umayyah tersebut, para pelayan menggelar tikar di atas tubuh mereka
yang masih menggeliat-geliat dan para tamu lainnya melanjutkan makan
malam sambil bersuka ria." (Discovering of Islam).

Menjelang keruntuhan kekuasaan Umayyah di Spanyol dan Abbasiyah yang
terbentang dari wilayah Afrika dan seluruh jazirah Arab itu, tentara
Kristen dari Eropa Barat menyerang dua dinasti Islam tersebut.
Dengan misi perang suci untuk menaklukkan orang-orang "kafir", umat
Islam saat itu dihadapkan pada dua pilihan: beralih ke agama Kristen
atau keluar dari wilayah kekuasaannya (migrasi).

Perang ini kemudian diikuti oleh sebuah perang lain yang dalam
sejarah dunia disebut perang salib, sebuah perang yang menghabiskan
waktu tidak kurang dari seratus tahun dan menyisakan trauma pahit
bagi Kristen dan Islam.
Pada awal-awal abad 20-an, beberapa wilayah yang berbasiskan umat
Islam pun terdesak oleh negara-negara kolonial dari Eropa. Misalnya,
Indonesia oleh Belanda; Mesir, Maroko, Aljazair oleh Perancis,
Malaysia, Nigeria oleh Inggris dan Libia oleh Italia. Dan panggung
sejarah umat Islam yang sempat penuh gemerlap itu terbenam ke bawah
dasar permukaan, bagai sebuah cerita yang ditutup secara tiba-tiba.

Lalu bagaimana dengan terorisme? Syaikh Zaid bin Muhammad dalam
kitabnya al-Irhab wa asaruhu ala al-afrad wa al-Umam menjelaskan
bahwa sejarah terorisme dalam Islam telah berlangsung lama sejak
masa awal Islam. Ia menyebut kasus-kasus pembunuhan khalifah Umar,
Khalifah Ustman, khalifah Ali dan berbagai pemikiran Qodariyah dan
Mu'tazilah merupakan tindakan yang dapat dikategorikan terorisme.

Pada Abad XX, umat Islam dikejutkan serentetan teror yang melanda
dunia Islam yaitu dimulai dengan terbunuhnya Abdul Aziz al-Faisal al-
Su'ud ketika tengah melakukan tawaf ifadah di putaran keempat.
Selain peristiwa pembunuhan, masih di tempat yang sama di Masjidil
Haram pada tahun 1400 H, pada awal bulan Muharam tiba-tiba
sekelompok orang yang dipimpin oleh Muhammad bin Abdul al-Qathami
dengan juru bicara Juhaiman bin Salf al-`Utaibi melakukan intimidasi 
dan teror mengajak umat Islam untuk membaiat al-Mahdi. Dari dua
kasus tersebut, fenomena makin berkembangnya terorisme mempengaruhi
diskursus politik Islam.

Peristiwa lainnya adalah ketika dunia Islam dikejutkan dengan
keganasan muslim fundamentalis membunuh Presiden Anwar Sadat dan
Farag Faudah. Teror itu dilakukan oleh kaum fundamentalis yang tidak
setuju atas kebijakan Anwar Sadat. Faktor utama pembunuhan terhadap
Presiden Mesir ini lebih bermuatan politis dan mendapat legitimasi
teologis yang mereka yakini.

Selain itu juga terjadi teror yang dialami oleh umat Islam di
berbagai belahan dunia. Ketika umat Islam dijajah, seperti di
Palestina, umat Islam mendapat teror dari kaum Yahudi. Di Bosnia
telah terjadi pembersihan etnis, di Kasmir umat Islam terus mendapat
teror dan mendapat perlakuan kekerasan, di Thailand dan di sejumlah
negara yang penduduknya terdiri minoritas Muslim seperti di Rusia
dan Cina di mana-mana partai komunis masih berkuasa umat Islam
mendapatkan teror.

Setelah beratus-ratus tahun umat Islam menjadi sasaran gerakan
teroris, muncul fenomena baru bahwa dunia luar membangun opini yang
menuduh umat Islam sebagai dalang teroris. Opini ini didasarkan
berbagai peristiwa seperti peledakan pesawat Okland, Peledakan
kedubes AS di Nairobi, Kenya Darus Salam, Tanzania. Di Nairobi
menewaskan 212 orang dan melukai 4000 di Darus Salam, menewaskan 11
orang dan melukai 72 orang lebih.

Selanjutnya juga teror yang dilakukan Usamah bin Ladin dengan
pernyataannya; "Tuhan telah memberkati satu kelompok prajurit
Muslim, yang menjadi baris terdepan Islam, untuk menghancurkan
Amerika Serikat." Serta fatwanya, "Kami telah mengeluarkan fatwa
untuk seluruh umat Muslim: Perintah untuk membunuh warga AS dan
sekutunya –baik sipil atau militer—adalah kewajiban pribadi bagi
tiap Muslim yang bisa melakukannya di negara mana saja yang
memungkinkan... Kami –dengan pertolongan Tuhan—berseru kepada setiap
Muslim yang percaya pada Tuhan dan mengharapkan pahala agar mematuhi
perintah Tuhan untuk membunuh rakyat Amerika dan merampas uang
mereka di mana dan kapan saja."

Dunia akhirnya mafhum bahwa gerakan Al-Qaidah pimpinan Usamah bin
Ladin adalah gerakan yang dibangun di atas semangat Islam, pelakunya
Muslim dan mempunyai jaringan dengan berbagai gerakan Islam lainnya,
seperti gerakan Jihad Islam di Mesir, gerakan Islam Uzbekistan dan
memiliki hubungan dengan Jamaah Islamiyah di ASEAN.

Pertanyaan kita selanjutnya, bila itu terjadi di Indonesia, apa yang
ditempuh pemerintah yang berkuasa; menghidupkan atau membasmi
gerakan seperti itu? Tuan mungkin tidak tahu, bahwa kini telah
terjadi krisis moral dan etika begitu bangsa ini memasuki masa
reformasi. Dan Tuan tentu sudah tahu bahwa kekerasan atas nama agama
mulai merambah negeri kita. Apakah Tuan ingin pula menyalahkan
Soeharto yang di masanya hal seperti itu tidak terjadi? Apakah Tuan
juga ingin mengatakan itu adalah buah kebijakan Soeharto di masa
lalu?
