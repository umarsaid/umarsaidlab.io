---
layout: post
title: Tampilnya kaum nasionalis untuk menyelamatkan bangsa
date: 2006-03-30
---

Sambutan presiden SBY di depan Kongres Persatuan dan Kesatuan Akumni GMNI pada tanggal 24 Maret 2006 patut mendapat perhatian dari banyak orang, karena berisi hal-hal menarik untuk sama-sama kita renungkan dan kita jabarkan dari berbagai segi yang berkaitan dengan situasi bangsa dan negara di masa-masa yang lalu, masa kini dan masa depan.

Menurut Kompas (25 Maret 2006) dalam pidatonya itu ia mengucapkan kalimat-kalimat sebagai berikut :."Hari ini kita membangun tonggak sejarah baru. Alhamdulillah kaum nasionalis sudah mulai tampil kembali untuk menyelamatkan bangsa kita. Mari dengan Pancasila dan rasa kebangsaan yang tinggi kita bangun negara kita menuju masa depan yang lebih baik. Selamat berjuang. Merdeka!"

“Selain menanggapi sejumlah isu aktual nasional, Presiden dalam sambutannya juga menekankan empat konsensus dasar yang dibangun para pendiri bangsa, yang menurut Yudhoyono tidak boleh tercabut sampai kapan pun. Empat konsensus dasar itu adalah Pancasila, Undang-Undang Dasar 1945, Negara Kesatuan Republik Indonesia, dan Bhinneka Tunggal Ika.

"Indonesia akan rontok kalau empat konsensus dasar ini tercabut. Saya kira kaum nasionalis tidak boleh dan tidak akan membiarkan negaranya rontok. Karena itu, konsensus dasar tersebut harus dipertahankan. Itu adalah amanah para pendiri bangsa," ujarnya disambut tepuk tangan.

“Terkait dengan globalisasi yang tak terhindarkan, yang menantang tiga kemandirian bangsa seperti dikemukakan Bung Karno dalam Trisakti: di bidang politik, ekonomi, dan budaya, Presiden mengatakan, dengan jati diri dan semangat kebangsaan, Indonesia berupaya mengatasi semua tantangan itu demi kemandirian dan kemajuan bangsa. (kutipan selesai)

Apakah gerangan maksud ucapan presiden SBY ?
Terhadap apa yang dikatakan presiden SBY seperti di atas ini mungkin ada macam-macam pendapat atau reaksi orang-orang, baik yang tidak percaya akan ketulusannya, atau menyangsikan kesungguhannya ( atau yang meragukan kejujurannya ) maupun yang memuji keberaniannya atau yang menganggap bahwa presiden SBY telah mengucapkan hal-hal yang cukup positif dan menarik. Mungkin juga ada orang yang bertanya-tanya apakah gerangan maksud sesungguhnya presiden SBY dengan mengatakan itu semuanya? Apa tidak ada tujuan tertentu sebagai latar-belakangnya ?

Mengingat bahwa presiden SBY adalah mantan jenderal Angkatan Darat, dan sejak mudanya aktif sebagai militernya rejim Suharto, adalah wajar bahwa ada orang-orang yang mempunyai pertanyaan-pertanyaan semacam itu, atau yang meragukan ketulusannya ketika ia mengucapkan hal-hal yang sudah lama tidak terdengar di kalangan pendukung Orde Baru, dan terutama sekali di kalangan Angkatan Darat.

Sebab kalimatnya yang berbunyi : “Hari ini kita membangun tonggak sejarah baru. Alhamdulillah kaum nasionalis sudah mulai tampil kembali untuk menyelamatkan bangsa kita” merupakan ungkapan yang penting dan bisa mengandung arti yang jauh. Tetapi, kita masih patut bertanya-tanya : apakah yang dimaksudkan “membangun tonggak sejarah yang baru” itu berarti kita meningggalkan era Orde Baru dan memulai sejarah baru? Kalau betul-betul itu yang dimaksudkan maka ini merupakan ungkapan yang positif.

Juga ucapannya dengan kalimat :” .Alhamdulillah kaum nasionalis sudah mulai tampil kembali untuk menyelamatkan bangsa kita” bisa mempuyai arti yang penting sekali. Sebab, bisa saja ini diartikan bahwa presiden SBY merasa senang dengan tampîlnya kembali kaum nasionalis, dan yang lebih penting lagi “untuk menyelamatkan bangsa”. Sepintas lalu, ucapannya yang demikian ini memang pantas dianggap positif juga. Marilah sama-sama kita coba teliti lebih jauh arti ucapannya itu dari berbagai segi.

Bung Karno adalah simbul nasionalisme Indonesia
Kita bisa bertanya-tanya siapa sajakah yang dimaksudkan dengan sebutan nasionalis itu? Apakah mereka yang terdiri dari para alumni GMNI dan simpatisan-simpatisannya saja, atau apakah orang-orang yang juga menjadi anggota dan simpatisan PDIP, atau orang-orang yang tergabung dalam berbagai partai yang menyatakan mendukung gagasan-gagasan besar dan politik Bung Karno?

Kalimatnya yang berbunyi : “:.Alhamdulillah kaum nasionalis sudah mulai tampil kembali untuk menyelamatkan bangsa kita” juga bisa diartikan bahwa ia mengakui bahwa selama ini (artinya, selama Orde Baru) kaum nasionalis tidak muncul dan tidak dibolehkan punya peran penting dalam banyak hal. Nah, sekarang ia senang bahwa “kaum nasionalis sudah tampil kembali untuk menyelamatkan bangsa.” Sungguh, ini merupakan ungkapan yang tidak tanggung-tanggung dari seorang mantan jenderal Angkatan Darat, kalau kita renungkan hal-hal sebagai berikut :

Dalam sejarah bangsa Indonesia kontemporer, nasionalisme mengandung pengertian revolusioner atau kiri, karena bercorak anti-imperialisme dan anti-kolonialisme. Dan dengan pengertian ini bisalah kiranya dikatakan bahwa Bung Karno adalah contoh tipikal dari seorang nasionalis revolusioner dan kiri Di Indonesia, orang-orang yang dengan jujur atau serius bicara tentang nasionalisme (atau kaum nasionalis) maka dengan sendirinya akan ingat kepada peran dan ketokohan Bung Karno sebagai nasionalis yang besar.

Nasionalisme kita temukan dengan gamblang dan jernih pada diri, ketokohan, perjuangan, pandangan dan ajaran-ajaran Bung Karno. Pikiran-pikiran besar, gagasan-gagasan monumental, dan ajaran-ajaran Bung Karno yang tercermin dalam “Indonesia Menggugat”, atau dalam “Lahirnya Panca Sila”, atau dalam banyak pidato-pidatonya yang terkumpul dalam buku “Di bawah Bendera Revolusi”, dan “Nawaksara” adalah personifikasi atau pengejawantahan nasionalismenya Bung Karno. Bisa ditamsilkan bahwa Bung Karno adalah simbul atau perwakilan nasionalisme Indonesia.

Sebab, Bung Karno adalah nasionalis terbesar dalam sejarah perjuangan bangsa Indonesia dalam menentang imperialisme, kolonialisme, dan mengantar bangsa Indonesia yang terdiri dari berbagai suku ini menuju ke kemerdekaan. Di antara banyak tokoh-tokoh besar atau pemimpin dalam sejarah bangsa Indonesia jelaslah kiranya bagi banyak orang bahwa Bung Karno adalah merupakan tokoh nasionalis revolusioner yang banyak sekali memberikan sumbangan dan pengorbanan bagi kepentingan rakyat Indonesia.

Sayangnya, justru tokoh nasionalis yang besar inilah yang telah dikhianati oleh Suharto dan konco-konconya di Angkatan Darat (waktu itu), dan bersekongkol dengan kekuatan-kekuatan nekolim, terutama dengan imperialisme AS.

Rejim militer Suharto mengubur nasionalisme Indonesia
Mereka yang membenci Bung Karno ( karena berbagai sebab, umpamanya : kurang tahu sejarahnya dan perjuangannya, atau terpengaruh oleh propaganda rejim militer Orde Baru) maka tidak akan pernah mengerti atau tidak bisa menjiwai nasionalisme secara sungguh-sungguh.

Boleh dikatakan bahwa selama puluhan tahun Orde Baru, nasionalisme telah dikubur, atau setidak-tidaknya disingkirkan oleh rejim militer Suharto, karena rejim ini tidak menyukai kaum nasionalis yang personifikasinya adalah sosok Bung Karno. Rejim militer Suharto yang memusuhi nasionalis besar Bung Karno, dengan sendirinya bukanlah suatu rejim yang bisa dikategorikan menghargai nasionalisme, apalagi menjunjungnya tinggi-tinggi.

Oleh karena menentang berbagai fikiran, pandangan, sikap, dan politik Bung Karno, sebagai nasionalis revolusioner, maka di bawah pimpinan Suharto Angkatan Darat negeri kita telah memushi nasionalisme juga. Disebabkan oleh kebencian mereka kepada gagasan besar Bung Karno tentang NASAKOM, maka Suharto dan konco-konconya di Angkatan Darat juga menaruh kebencian terhadap siapa saja yang menampakkan diri sebagai nasionalis pendukung NASAKOM.

Oleh sebab itulah, kiranya kita tidak bisa dan tidak patut menggolongkan orang-orang sejenis Suharto (dan kawan-kawan terdekatnya) sebagai orang-orang nasionalis, karena sejak terjadinya G30S mereka telah bersikap menentang atau memusuhi kaum nasionalis yang mendukung politik Bung Karno. Dengan apa yang terjadi di akhir tahun 1965, Supersemar, dan Orde Baru, kiranya bagi banyak orang sudah jelas bahwa orang-orang sejenis Suharto tidak bisa dipandang sebagai nasionalis, bukan pula demokrat, bukan juga patriot, dan juga bukan humanis.

Suharto , dan pendukung-pendukung setianya ( yang terdiri dari tokoh-tokoh Angkatan Darat, Golkar, dan kalangan reaksioner lainnya), yang sudah mengkhianati Bung Karno dan membantai serta memenjarakan jutaan orang tidak bersalah, jelas bukanlah orang-orang yang pantas disebut sebagai pencinta demokrasi dan pengemban Pancasila yang sesungguhnya.

Selama puluhan tahun Orde Baru, Suharto dkk banyak bicara tentang Pancasila. Tetapi, dalam prakteknya Pancasila telah diinjak-injak, dipalsu, atau dilecehkan. Perlakuan terhadap para korban peristiwa 65 yang jutaaan orang dan selama puluhan tahun adalah bukti yang kongkrit bahwa mereka tidak memahami sama sekali arti Pancasila. Dengan mengkhianati Bung Karno, satu-satunya tokoh bangsa yang justru melahirkan Pancasila, orang-orang Orde Baru tidak berhak mengatakan dirinya sebagai Pancasilais.

TRISAKTI-nya Bung Karno dan TNI-AD

Yang juga termasuk hal yang menarik dari sambutan presiden SBY ialah ketika ia berbicara tentang tiga kemandirian bangsa seperti yang dikemukakan Bung Karno dalam Trisakti : di bidang politik, ekonomi, dan budaya. “Dengan jati diri dan semangat kebangsaan, Indonesia berupaya mengatasi semua tantangan demi kemandirian dan kemajuan bangsa, “ kata presiden SBY.

Sudah puluhan tahun selama rejim militer Suharto dkk tidak pernah terdengar (kalau ada pun jarang sekali!) ajaran atau gagasan Bung Karno, seperti Trisakti, dikutip oleh tokoh-tokoh utama Orde Baru, apalagi tokoh-tokoh militer. Karena, justru oleh karena ajaran atau gagasan-gagasannyalah maka Bung Karno dikhianati, digulingkan, kemudian ditapolkan (oleh Angkatan Darat) sampai wafatnya dalam tahanan.

Hal lain yang juga menarik adalah ketika presiden SBY juga menekankan empat konsensus dasar yang dibangun para pendiri bangsa, yang tidak boleh tercabut sampai kapan pun. Empat konsensus dasar itu adalah Pancasila, Undang-Undang Dasar 1945, Negara Kesatuan Republik Indonesia, dan Bhinneka Tunggal Ika. "Indonesia akan rontok kalau empat konsensus dasar ini tercabut. Saya kira kaum nasionalis tidak boleh dan tidak akan membiarkan negaranya rontok. Karena itu, konsensus dasar tersebut harus dipertahankan. Itu adalah amanah para pendiri bangsa," ujarnya.

Membaca hal-hal tersebut di atas maka orang bisa saja bertanya-tanya apakah itu semua pertanda bahwa sudah ada “perubahan” di kalangan militer, khususnya Angkatan Darat? Belum tentu !!! Sebab, Angkatan Darat khususnya, dan golongan militer pada umumnya, sudah terlalu lama (sekitar 40 tahunan) dirusak oleh Suharto dkk, dengan menjadikannya sebagai bagian bangsa yang merupakan tulang punggung rejim militer yang otoriter atau despotik, Mereka, yang jumlahnya sekitar 500.000 orang itu telah menjadi golongan yang berklas “istimewa” dalam kehidupan bangsa selama puluhan tahun, dan telah mengangkangi bangsa Indonesia yang jumlahnya 200 juta orang.

Jadi, ketika presiden SBY mengucapkan hal-hal yang begitu positif terhadap kaum nasionalis dan bahkan juga mengutip sebagian ajaran -ajaran Bung Karno, apakah berarti bahwa ia “meninggalkan” garis atau doktrin militer Orde Baru? Tidak jelas, atau tidak pasti. Mungkin-mungkin saja. Sebab, meskipun ia mantan jenderal TNI-AD ia menjadi presiden RI bukan sebagai wakil militer. Ia dipilih secara langsung oleh sebagian rakyat Indonesia (lebih dari 60% suara). Dan lagi pula, tentunya, ia pun menyadari bahwa TNI-AD (juga Suharto) sudah lama sangat buruk citranya di mata banyak orang.

Hingga kini, masih belum jelas benar apakah sosok SBY bisa betul-betul meninggalkan keterikatannya - secara ideologi, politik maupun mental atau moral - dengan rejim militer Orde Baru yang sudah dikutuk oleh banyak orang, dan sejauh apa ia memisahkan diri dari segala yang buruk yang telah dilakukan oleh Suharto dkk.

Namun, ucapannya di depan kongres alumni GMNI telah memercikkan harapan bahwa ia sebagai presiden terpilih merupakan tokoh mantan jenderal TNI-AD yang mungkin mampu mendorong terjadinya perubahan-perubahan fundamental di kalangan militer, dan kalangan-kalangan reaksioner lainnya (termasuk sebagian kalangan Islam) yang selama puluhan tahun memusuhi Bung Karno, beserta para pendukungnya yang setia, yaitu golongan nasionalis dan komunis.

Apakah harapan ini akan hanya merupakan ilusi atau mimpi di siang hari bolong saja, atau betul-betul bisa terlaksana dan menjadi kenyataan, kita semua akan menyaksikannya di kemudian hari. Sebab, pepatah Jawa “Becik ketitik, olo ketoro” (apa yang baik akan ketahuan, dan apa yang jelek akan kelihatan) tetap masih ada kebenarannya dalam banyak hal.
