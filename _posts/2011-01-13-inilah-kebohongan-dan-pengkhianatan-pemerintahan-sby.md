---
layout: post
title: Inilah kebohongan dan pengkhianatan pemerintahan SBY
date: 2011-01-13
---

Perkembangan situasi di negeri kita makin menarik, karena menunjukkan bahwa ada kebangkitan kesedaran politik yang  makin meluas dan meninggi, yang memberikan indikasi kuat bahwa kebutuhan akan adanya perubahan besar-besaran dan fundamental  di bidang politik atau pemerintahan sudah kelihatan lahir dimana-mana.

Yang juga menarik untuk diperhatikan bersama-sama oleh kita semua adalah gejala bahwa kesadaran politik yang meningkat sekarang ini bukan hanya di kalangan orang-orang kiri (antara lain golongan PKI dan pendukung Bung Karno) melainkan juga di kalangan orang-orang yang tidak termasuk golongan kiri namun ikut juga  mendambakan juga perubahan besar. Untuk memberikan sekadar sedikit gambaran tentang perkembangan semacam itu disajikan di bawah ini sejumlah bahan-bahan untuk pemikiran bersama.

Dari bahan-bahan  ini, ditambah dengan banyaknya berita-berita di media dan siaran-siaran televisi, dan diperkuat oleh berbagai demo (terutama oleh kalangan muda dan mahasiswa) atau pernyataan oleh banyak LSM atau  organisasi di seluruh Indonesia nyatalah bahwa kemarahan rakyat terhadap pemerintahan SBY sudah memuncak tinggi sekali. Kita saksikan bahwa kemarahan atau ketidak-puasan itu sudah banyak sekali ditujukan kepada presiden SBY sendiri, selain kepada para pejabat-pejabat tinggi pemerintahan.

Akibat kepemimpinan presiden  SBY yang sangat mengecewakan

Kemarahan atau ketidak-puasan terhadap kepemimpinan presiden SBY ini sudah begitu besarnyya sehingga sudah disalurkan dengan macam-macam cara dan dengan berbagai ungkapan, termasuk yang paling kasar, paling keras, paling tajam, bahkan sampai kepada yang kedengaran paling tidak senonoh atau tidak sopan.

Dalam berbagai kesempatan, presiden SBY sudah diperolok-olokkan dengan simbul kerbau (yang gemuk dan lamban), fotonya diinjak-injak, patungnya dibakar atau diseret-seret dalam demo, dan namanya dihujat. Presiden SBY dikatakan sebagai musuh publik (public enemy), pengkhianat kepentingan rakyat, pelindung koruptor, bukan pemimpin yang bisa dipercaya, presiden yang hanya repot mengutamakan citranya, selalu ragu-ragu untuk berani bertindak, munafik, penipu rakyat, Kiranya, dalam sejarah Republik Indonesia barulah presiden SBY yang mendapat begitu banyak kecaman atau hujatan dari kalangan yang begitu luas..

Kecaman amat pedas dan tajam, bahkan hujatan kepada presiden SBY adalah akibat dari kepemimpinannya sebagai presiden/kepala negara yang dianggap sangat mengecewakan, dan sudah menunjukkan kegagalan di banyak bidang. Banyak tokoh-tokoh yang mengatakan di televisi dan media pers bahwa akibat kepemimpinan SBY sebagai presiden-lah maka negara kita mengalami kemerosotan, kemunduran, kerusakan, kebobrokan yang luar biasa parahnya, seperti yang bisa disaksikan oleh banyak orang dewasa ini.

Begitu besarnya ketidakpuasan dan kemarahan publik terhadap kepemimpinan presiden SBY dan pemerintahannya, sehingga sulit untuk diramalkan peristiwa-peristiwa besar macam apa sajakah yang bisa terjadi di kemudian hari. Namun, bagaimanapun juga,  sudah dapat diduga, bahwa akan terjadi sesuatu yang penting atau situasi yang  luar biasa, mengingat panasnya suhu politik dan opini publik yang makin panas dan makin semrawutnya kehidupan bangsa dan negara.

Krisis moral dan berbagai krisis parah lainnya


Kiranya, bagi kita semua sudah jelas bahwa  negara dan bangsa sedang menghadapi krisis-krisis  berat dan besar, yang jarang terjadi sebelumnya.  Krisis itu adalah krisis kepercayaan terhadap pemerintahan SBY, dan krisis moral yang tercermin dalam besarnya dan parahnya kerusakan di bidang penegakan hukum dan  tetap merajalelanya korupsi. Persoalan kasus Gayus  Tambunan adalah salah satu contoh saja dari ribuan, bahkan puluhan ribu kasus ( !!!)  yang sebenarnya terjadi di seluruh tanah air kita selama ini.


Berbagai krisis besar dan berat ini membikin banyak golongan dalam masyarakat menjadi putus asa dan kuatir tentang hari kemudian bangsa. Bermacam-macam tokoh sudah mengatakan bahwa  bangsa kita  sedang menuju kehancuran, dan bahwa negara kita  akan karam karena tidak adanya keadilan dan rusaknya penegakan hukum (antara lain,  pernyataan Mahfud.MD , Ketua Mahkamah Konstitusi).
Ketidak-tegasan, atau tidak adanya nyali presiden SBY untuk mengambil tindakan  terhadap tingkah-laku jahat para pejabat kepolisian, kejaksaan, imigrasi, dan instansi perpajakan atau keuangan telah menjadi pembicaraan sehari-hari dari orang-orang yang merasa gemas karena terkatung-katungnya pembongkaran secara tuntas terhadap kasus Gayus Tambunan dan kasus Bank Century. Sikap yang demikian ini jugalah yang membikin Adnan Buyung Nasution mengeluarkan ungkapan-ungkapan yang keras sekali dan mengajukan protes keras terhadap presiden SBY. Ia menegaskan bahwa dengan sikap yang demikian ini, maka sampai kiamat pun banyak soal tidak akan bisa selesai.


Pernyataan keras terhadap presiden SBY juga dikeluarkan Syafii Ma’arif, mantan Ketua Umum PP Mumammadiyah. Ia mengatakan bahwa di bawah pemerintahan SBY  tidak ada perubahan yang fundamental. Misalnya soal kemiskinan,  ekonomi, hukum tidak bisa ditegakkan," ujar Syafii di Gedung Stovia, Museum  Kebangkitan Nasional, Jakarta (Selasa, 11/1/2011). Meski begitu, dia tidak sepakat, bila SBY lengser dengan cara dimakzulkan.  "Jangan dimakzulkan. Pergantiannya jangan pakai kudeta, jangan berdarah-darah, harus konstitusional," imbuhnya.

Saat ditanya lagi dengan cara apa SBY lengser bila tidak dimakzulkam, dia menjawab SBY nyerah atau lempar handuk yang menandakan tidak mampu lagi menjalankan amanat rakyat. "Bilang saja, 'Saya (SBY) tidak mampu," tandasnya. (sumber Rakyat Merdeka, 11/1/2011)

Pernyataan terbuka tokoh-tokoh lintas agama dan LSM

Krisis kepercayaan kepada kepemimpinan presiden SBY dan pemerintahannya ini nampak juga dari pernyataan terbuka, --  yang penting dan mengandung arti yang besar  -- yang dikeluarkan oleh para tokoh lintas agama di kantor PP Muhammadiyah tanggal 10 Januari 2011.
Para tokoh lintas agama yang hadir dalam pernyataan terbuka tersebut antara lain Ketua Umum PP Muhammadiyah Din Syamsudin, Ketua Konferensi Waligereja Indonesia (KWI) Mgr Martinus Situmorang, Ketua Persatuan Gereja-Gereja Indonesia (PGI) Pendeta Andreas Yewangoe, Buya Syafii Maarif, Frans Magnis Suseno, Shalahudin Wahid, dan Bhikku Sri Pannyavaro. Turut hadir sederet aktivis seperti Haris Azhar dari KontraS, Anis Hidayah dari Migrant Care, Berry Furqan dari Walhi dan Tama Satrya Langkun.


Dalam pernyataan terbuka itu para tokoh lintas agama tersebut menegaskan bahwa hingga detik ini, kantong-kantong kemiskinan sangat mudah ditemukan di tanah air. Maraknya pengrusakan lingkungan dan pelanggaran hak azasi manusia (HAM) menyebabkan kemiskinan tersebut kian bertambah akut.
Kenyataan itu merupakan sebuah penghianatan pemerintah yang harus segera dihentikan.
"Kami mengimbau kepada elemen bangsa, khususnya pemerintah, untuk menghentikan segala bentuk kebohongan publik," ujar Romo Benny Susetyo saat membacakan pernyataan bersama tersebut.


Selain itu, para tokoh lintas agama sepakat, bahwa sistem ekonomi neo liberalisme yang dijalankan pemerintah telah gagal meskipun pertumbuhan ekonomi Indonesia tercatat 5,8 persen. "Rakyat kecil tidak pernah merasakan keadilan dari pertumbuhan ekonomi semu itu. Ini berlawanan dengan tuntutan Pasal 33 UUD 1945," lanjut Romo Benny.


Ekonomi Indonesia, kata Romo Benny, sudah keluar dari jalur Undang-Undang Dasar (UUD). Kecenderungan pasar bebas dalam sistem ekonomi Indonesia dinilai sebagai penghianatan terhadap pembukaan UUD 1945. Kondisi tersebut, lanjutnya, diperburuk oleh sikap pemerintah yang masih mengedepankan pencitraan.


"Dan, terindikasi berpura-pura, tidak satu antara kata dan perbuatan," katanya. Pemerintah, selama ini dinilai hanya berpura-pura dalam menegakkan hukum dan Hak Asasi Manusia (HAM), memberantasa korupsi, serta menjaga lingkungan hidup, dan kekayaan Indonesia. "Marilah kita canangkan tahun 2011 ini sebagai tahun perlawanan kebohongan," pungkas Romo Benny.


Inilah sembilan kebohongan baru pemerintah


Pada kesempatan itu, sembilan orang aktivis dari berbagai lembaga swadaya masyarakat, juga mengeluarkan pernyataan bersama menyikapi pemerintahan Presiden Susilo Bambang Yudhoyono. Menurut mereka, pemerintahan Presiden SBY telah melakukan setidaknya sembilan kebohongan, baik yang dilakukan sejak lama maupun kebohongan baru.

Selain menyampaikan pernyataan terkait sembilan kebohongan lama pemerintah, tokoh-tokoh lintas agama dan pemuda itu juga membacakan sembilan kebohongan baru pemerintah yang terjadi sepanjang 2010.

Sembilan kebohongan baru pemerintah itu berkenaan dengan kebebasan beragama; kebebasan pers; perlindungan terhadap TKI-pekerja migran; transparansi pemerintahan, pemberantasan korupsi; pengusutan rekening mencurigakan (gendut) perwira Polisi; politik yang bersih, santun, beretika; kasus mafia hukum yang salah satunya adalah kasus Gayus H Tambunan; dan terkait kedaulatan NKRI.

Salah seorang pemuda, Riza Damanik, menyampaikan, kebohongan pertama pemerintah adalah saat presiden berpidato pada 17 Agustus 2010 yang isinya menjunjung tinggi pluralisme, toleransi, dan kebebasan beragama. Padahal kenyataannya, janji tersebut tidak terpenuhi.
Sepanjang 2010 terjadi 33 penyerangan fisik atas nama agama. "Mantan Kapolri Bambang Hendarso Danuri mengatakan 2009 terjadi 40 kasus kekerasan ormas, 2010 menjadi 49 kasus," katanya.

Kebohongan kedua, terkait kebebasan pers. Presiden menjanjikan jaminan terhadap kebebasan pers dan kepolisian berjanji akan menindak tegas setiap kasus kekerasan terhadap insan pers. "Namun, Lembaga Bantuan Hukum (LBH) Pers mencatat selama 2010 kasus kekerasan pers sebanyak 66 kasus meningkat dari 2009 yang 56 kasus," kata Riza.

Ketiga, kebohongan terkait perlindungan terhadap TKI atau pekerja migran. Presiden berjanji akan melengkapi TKI dengan telepon genggam agar tidak terjadi ketertutupan informasi, namun nyatanya, telpon genggam tidak juga diberikan dan memorandum untuk melindungi para TKI tidak juga dilakukan.

Keempat, terkait transparansi pemerintahan. Aktivis pemuda, Stefanus Gusma, membacakan, Presiden SBY menyatakan bahwa kepindahan mantan menteri keuangan Sri Mulyani ke Bank Dunia adalah atas dasar permintaan Bank Dunia. Namun, di sebuah media nasional diungkapkan bahwa kepindahan Sri Mulyani sesungguhnya merupakan paksaan dari presiden. Seorang pejabat Kementerian Keuangan mengatakan bahwa Sri Mulyani tidak pernah berniat mengundurkan diri.

Kelima, lanjut Gusma, terkait pemberantasan korupsi. Presiden berkali-kali berjanji akan memimpin sendiri pemberantasan korupsi di Indonesia. "Namun, riset ICW, dari pernyataan SBY yang mendukung korupsi hanya 24 persen yang terlaksana," katanya.

Koordinator Komisi untuk Orang Hilang dan Korban Tindak Kekerasan (Kontras), Haris Azhar melanjutkan, kebohongan keenam pemerintah adalah tentang pengusutan rekening gendut para pewira Polri. Presiden menginstruksikan jika ada pelanggaran hukum, yang terkait harus diberikan sanksi. Jika tidak, Kapolri harus menjelaskan kepada masyarakat. Namun kenyataannya, kata Haris, sampai saat ini baik masalah rekening gendut maupun pelaku penganiayaan aktivis ICW, Tama S Langkan masih misterius. "Bahkan 7 Agustus 2010 dan 29 Desember 2010 dua Kapolri mengatakan kasus ini ditutup," katanya.

Kebohongan ketujuh, Presiden menjanjikan politik yang bersih, santun, dan beretika. Padahal kenyataannya, lanjut Haris, hingga kini, Andi Nurpati masih menjadi pengurus Partai Demokrat meskipun sudah diberhentikan tidak hormat oleh Dewan Kehormatan Komisi Pemilihan Umum (KPU). "Andi Nurpati melanggar peraturan KPU," imbuhnya.

Kedelapan, lanjut aktivis ICW, Tama S Langkun, terkait kasus mafia hukum. Kapolri Jenderal Timur Pradopo berjanji menyelesaikan kasus pelesiran terdakwa mafia pajak Gayus H Tambunan dalam 10 hari. Tapi kenyataannya tidak ada keterangan pers tentang hal tersebut. "Kapan Gayus keluar, pergi naik apa, dengan siapa, aktivitasnya, sekarang malah mencuat kasus baru, Gayus pelesir ke luar negeri," kata Tama.

Dan kesembilan, kebohongan pemerintah menyangkut kedaulatan NKRI. Pada 1 September di Mabes TNI Cilangkap Presiden menyampaikan bahwa perlakuan tidak patut terhadap tiga petugas KKP sedang diusut. Pemerintah Malaysia sedang menginvestigasi masalah tersebut. "Tapi sampai saat ini tidak pernah diumumkan penjelasan atau hasil investigas apapun," pungkas Tama. ((sumber harian Kompas 10/1/11),

Penutup

Dari sedikit bahan-bahan yang disajikan dalam tulisan di atas sudah nampak bahwa kepercayaan publik terhadap pemerintahan SBY, dan terutama terhadap pribadi presiden SBY sendiri, sudah merosot atau anjlog dalam sekali. Padahal, masih banyak lagi dan juga masih beraneka ragam lagi kebohongan lainnya yang dilakukan pemerintahan (dan presiden SBY !!!) selama ini.

Karena itu, kita sama-sama melihat bahwa integritas moral, integritas politik, dan berbagai integritas lainnya, sudah makin mengecil dan bahkan sudah menjurus musnah. Karena itu, kita jadinya mengerti lebih terang lagi, mengapa akhir-akhir ini terdengar  suara-suara yang menginginkan dilakukannya pemakzulan (impeachment) terhadap presiden SBY.

Kita tunggu saja, perkembangan situasi yang makin menarik, dan yang akan menimbulkan berbagai gejolak, yang pada akhirnya akan merupakan peningkatan pendidikan politik bagi banyak orang, dan juga sumbangan penting bagi terjadinya perubahan-perubahan besar, sesuai dengan ajaran-ajaran revolusioner Bung Karno.
