---
layout: post
title: Pengalaman korban perisriwa 65 adalah guru besar bangsa
date: 2005-08-14
---

Ketika kita memperingati “40 tahun peristiwa 65” apa sajakah yang perlu kita kenang kembali atau kita tarik sebagai pelajaran penting bagi bangsa kita dewasa ini dan juga untuk anak-cucu kita di kemudian hari ? Boleh dikatakan, semuanya! Semua soal yang berkaitan dengan peristiwa 65 adalah penting. Karena itu, masalah peristiwa 65 adalah rumit, dan bersegi banyak. Dalam persoalan besar yang sangat bersejarah bagi bangsa dan Republik Indonesia ini ada aspek PKI, ada aspek Bung Karno, aspek TNI-AD, aspek Suharto, aspek golongan Islam, aspek CIA. Di dalamnya terdapat juga faktor sejarah, faktor politik, ekonomi, sosial dan kebudayaan. Dan semuanya itu ada sangkut-pautnya - secara langsung atau tidak langsung - dengan banyak persoalan dalamnegeri dan internasional pada waktu itu.

Mengingat begitu besar dampak peristiwa 65 untuk kehidupan bangsa kita, maka sebaiknya (atau sepatutnya ) makin banyak orang bisa menulis tentang itu semua. Sehingga berbagai masalah peristiwa 65 bisa dilihat secara betul-betul jernih dan juga secara menyeluruh. Karena, seperti kita saksikan sendiri masing-masing selama ini, banyak sekali soal yang berkaitan dengan peristiwa 65 telah diputarbalikkan, direkayasa, dipalsukan, dibohongkan, disulap dan “divermaak” oleh Orde Baru. Dan dalam jangka waktu yang lama sekali pula, yaitu lebih dari 32 tahun !!! Jadi, tidak tanggung-tanggung.

Dalam tulisan yang kali ini titik berat diletakkan pada ajakan kepada semua untuk merenungkan bersama masalah penganiayaan dan penyiksaan yang dilakukan oleh para pembangun rejim militer Orde Baru. Karena, penganiayaan biadab dan penyiksaan sadis adalah salah satu di antara banyak “senjata ampuh” yang dipakai oleh Orde Baru dalam melumpuhkan kekuasaan Bung Karno dan dalam memukul PKI beserta pendukung-pendukungnya. Penganiayaan dan penyiksaan (yang dilakukan dalam berbagai bentuk) adalah suatu cara rejim militer Orde Baru untuk kemudian melakukan terror permanen di seluruh negeri, guna memperkokoh cengkeraman kekuatan militernya. Dalam arti tertentu, bisalah kiranya disimpulkan bahwa penganiayaan dan terror adalah satu dan senyawa dengan Orde Baru.

## Puncak Kebiadaban Dalam Sejarah Bangsa

Barangkali, penganiayaan dan penyiksaan oleh kesatuan-kesatuan TNI-AD (dan kalangan kecil dari golongan Islam) terhadap anggota, simpatisan dan kader-kader PKI dan pendukung Bung Karno, merupakan puncak kebiadaban yang pernah dibikin oleh segolongan kecil bangsa kita.terhadap sesama warganegara. Dan, mungkin juga, puncak kebiadaban besa-besaran yang mengerikan ini adalah satu-satunya ( dan yang terakhir! ) yang muncul dalam sejarah bangsa Indonesia. Mudah-mudahan, Insya Allah!

Kalau mengingat betapa hebatnya penganiayaan atau sadisnya penyiksaan terhadap begitu banyak orang yang ditangkap dan diinterogasi oleh aparat militer maka kita bisa bertanya-tanya apakah bangsa kita ini masih pantas dinamakan bangsa yang beradab? Apakah kita bisa membanggakan diri sebagai bangsa yang majoritasnya pemeluk agama?

Selama 32 tahun pemerintahan rejim militer Suharto dkk orang tidak bisa dan juga tidak berani buka suara tentang segala kebiadaban, kebuasan, dan kebengisan yang terjadi dalam tahun-tahun pertama ketika Suharto dkk menyerobot kekuasaan dari tangan Bung Karno. Sebab, berani buka suara waktu itu berarti pasti menghadapi penangkapan dan penganiayaan.

Baru setelah Suharto dengan Orde Barunya dipaksa turun dari kekuasaan dalam tahun 1998, sedikit demi sedikit muncullah beraneka ragam cerita dan kesaksian tentang betapa hebatnya penganiayaan dan penyiksaan terhadap para korban. Sebagian kecil sekali dari cerita dan kesaksian ini sudah mulai diketahui oleh publik melalui, antara lain : wawancara, artikel atau tulisan dalam majalah, memoire dalam bentuk buku.

Mengingat banyaknya kasus penganiayaan dan hebatnya penyiksaan, dan mengingat juga besarnya jumlah orang yang telah menderita perlakuan yang tidak berperikemanusiaan ini, maka kiranya kita semua perlu mendorong sebanyak mungkin orang untuk terus menulis tentang itu semua lebih banyak lagi. Menulis (atau menceritakan) tentang kebiadaban penganiayaan dan penyiksaan yang telah dilakukan oleh sebagian golongan militer (dan sebagian kecil golongan Islam pada waktu itu) merupakan tugas penting generasi bangsa kita dewasa ini. Sebab, kalau tugas penting ini tidak dikerjakan sekarang, maka saksi-saksi hidupnya akan makin berkurang atau banyak pelaku-pelaku sejarahnya yang sudah keburu meninggal dunia.

## Besarnya Dan Luasnya Penganiayaan

Sekadar untuk menyegarkan kembali ingatan kita bersama, pada akhir tahun 1965, dan dalam tahun-tahun 1966 dan 1967, hampir seluruh pemimpin dan kader PKI dari berbagai tingkat (propinsi, kabupaten, kota besar dan kota madya, kecamatan, bahkan kelurahan) di seluruh Indonesia ditangkapi secara besar-besaran dan ditahan secara sewenang-wenang dalam jangka waktu yang lama. Sebagian besar di antara mereka dibunuh begitu saja dengan cara-cara yang tidak berperikemanusiaan.

Banyak diceritakan sekarang bagaimana kejamnya siksaan terhadap mereka yang ditangkap itu selama diinterogasi. Sebagian besar di antara mereka itu terdiri dari para pemimpin atau kader bermacam-macam organisasi buruh, tani, nelayan, wanita, pemuda, pelajar, tentara, pegawai negeri, dan golongan lainnya dalam masyarakat. Banyak di antara mereka yang disetrum listrik, disundut dengan puntung rokok, dipukuli dengan kawat berduri, digantung sampai berhari-hari, dipukuli beramai-ramai, ditusuk-tusuk dengan pisau atau bayonet, di-sel di ruangan gelap (tanpa sinar matahari sedikitpun) sampai berbulan-bulan, bahkan bertahun-tahun.

Sebagian di antara mereka disiksa di hadapan istri dan anak. Banyak yang sesudah disiksa tidak diberi pengobatan dan dibiarkan kesakitan sampai jangka lama. Banyak sekali orang tahanan yang tidak boleh berhubungan sama sekali dengan keluarganya. Tidak sedikit di antara tahanan wanita yang diperkosa atau dilecehkan kesusilaan mereka dengan berbagai cara. Semuanya ini sulit dibantah, karena saksi-saksinya masih ada di seluruh Indonesia, dan korban-korbannya pun banyak sekali yang masih bisa memberikan kesaksian.

Dampak siksaan atau penganiayaan ini besar sekali. Dari mulut ke mulut bocor juga berita tentang kekejaman yang diperlakukan terhadap para tahanan itu. Berita-berita inilah yang kemudian juga menimbulkan terror yang hebat sekal di kalangan keluarga (atau di kalangan teman-teman) yang ditahan. Ketakutan yang besar sekali terhadap militer menghinggapi kalangan luas dalam masyarakat di seluruh negeri waktu itu.

Dalam jangka lama semasa Orde Baru, tempat-tempat seperti Kodam, Korem dan Kodim di seluruh Indonesia merupakan sesuatu yang sangat ditakuti oleh banyak orang biasa. (Ditakuti, tetapi tidak berarti juga otomatis dihormati). Sebab, sering sekali, orang-orang yang ditahan oleh Kodim adalah orang-orang yang dianggap mempunyai masalah. Masalah yang berkaitan dengan “keamanan”, yang arti polosnya yalah kekuasaan rezim militer.

Entah sudah berapalah jumlah orang-orang yang pernah ditahan oleh Kodim (atau instansi militer bawahannya) di seluruh Indonesia. Seandainya tembok-tembok gedung Kodim di banyak tempat bisa bicara akan bisa menggigillah orang mendengar kisah-kisah tentang hebatnya dan juga banyaknya siksaan biadab yang telah dilakukan oleh para petugas militer waktu itu.

## Kesaksian Para Korban Adalah Milik Berharga Bangsa

Kisah-kisah tentang penyiksaan atau penganiayaan oleh petugas-petugas militer ini sebenarnya sekarang ini dapat didengar dari banyak para korban peristiwa 65, para eks-tapol, dan juga keluarga mereka. Kesaksian mereka (beserta sanak-saudara mereka, dekat maupun jauh) adalah MEMOIRE KOLEKTIF bangsa dan merupakan milik berharga bangsa, baik bagi generasi yang sekarang maupun bagi anak-cucu kita.

Dari kisah mereka itu bangsa kita sekarang dan anak-cucu kita akan mengetahui dengan jelas bahwa TNI-AD pernah melakukan kejahatan besar-besaran dalam bentuk siksaan biadab atau penganiayaan sadis terhadap sesama warganegara. Kisah para korban peristiwa 65 adalah sarana pendidikan moral yang sangat ampuh, atau alat pemupukan rasa perikemanusiaan yang sangat ideal. Kisah para korban peristiwa 65 tentang penyiksaan dan penganiayaan merupakan juga contoh kongkrit atau bukti nyata tentang pelanggaran perikemanusiaan.

Dari segi inilah kita bisa melihat pentingnya mendorong atau menganjurkan kepada para korban peristiwa 65 (termasuk sanak-saudara, dan juga teman-teman terdekat mereka) untuk berusaha mengangkat kisah-kisah sebenarnya tentang pengalaman mereka mengenai penyiksaan dan penganiayaan oleh militer, terutama sekali dalam tahun-tahun pertama sesudah terjadinya G30S.

Dalam kaitan ini kiranya perlu sekali kita renungkan bersama -- secara dalam-dalam -- soal berikut : pengangkatan kisah-kisah ini tidak dimaksudkan untuk tujuan nista, yaitu : mengungkit-ungkit dendam, atau membuka luka-luka lama, atau sekadar melampiaskan kemarahan dan mengumbar hujatan, menyebar kebencian atau mengipasi permusuhan Melainkan untuk tujuan luhur, yaitu : mengajak orang banyak untuk menjunjung tinggi-tinggi perikemanusiaan, mematuhi peradaban, memupuk rasa persaudaraan, dan menghormati perasaan keadilan, demi kebaikan bersama seluruh bangsa .

Di samping itu, melalui kisah-kisah tentang penyiksaan dan penganiayaan ini kita bisa berusaha memberi sumbangan kepada reformasi di bidang jiwa dan moral di kalangan militer (terutama kalangan TNI-AD) , supaya tidak mengulangi lagi kejahatan dan pelanggaran-pelanggaran berat yang sudah banyak dan sering sekali dilakukan semasa Orde Baru.

Sebab, seperti sudah kita saksikan sendiri selama ini, maka jelaslah kiranya bahwa penganiayaan, dan penyiksaan, dan penindasan, dan pemerkosaan, dan pemerasan, dan persekusi, dan intimidasi, adalah praktek-praktek yang banyak dilakukan oleh sebagian kalangan militer dalam jangka waktu puluhan tahun. Ini dialami sendiri dengan bukti-bukti yang nyata oleh banyak orang dari berbagai kalangan dan golongan di seluruh tanah air kita. Jadi, sekali lagi, fakta-fakta sejarah ini sulit sekali dibantah atau diungkiri.

Kalau dalam tahun-tahun pertama sesudah G30S tindakan yang biadab itu terutama sekali ditujukan kepada kader atau anggota dan simpatisan PKI dan pendukung Bung Karno, maka kemudian juga golongan-golongan lainnya yang dianggap berbahaya bagi rejim militer mendapat gilirannya. Itu sebabnya maka terjadi peristiwa-peristiwa berdarah di Tanjung Priok, Lampung, Aceh, Madura. Yang termasuk agak baru atau agak akhir adalah peristiwa di Timor Timur, kerusuhan rasial bulan Mei 1998, penculikan 14 anak muda PRD, penyerbuan gedung PDI di jalan Diponegoro, dan peristiwa Semanggi. Pembunuhan pejoang HAM, Munir, dalam tahun 2004, adalah bentuk yang lain lagi dari praktek biadab ini.

## Guru Besar Soal Penyiksaan Dan Perikemanusiaan

Jadi, dalam rangka memperingati 40 tahun peristiwa 65 mengungkap kembali kebiadaban penyiksaan dan membeberkan kebuasan penganiayaan aparat militer terhadap para korban adalah perlu atau berguna dan juga tepat waktunya. Membeberkan kebiadaban penganiayaan dan penyiksaan terhadap korban peristiwa 65 tidaklah bermaksud untuk “memojokkan” salah satu golongan, bukan pula untuk “menghina” salah satu aparat Negara. Apalagi, bukan juga untuk sekadar “menjelek-jelekkan" atau “menodai” kehormatannya.

Justru kebalikannya!!! Kita, sebagai bangsa, tidak ingin kalau kejahatan-kejahatan monumental yang sudah dilakukan oleh golongan militer (terutama TNI-AD) selama masa Orde Baru bisa terulang lagi, baik sekarang maupun di masa-masa yang akan datang. Sebagai bangsa, kita butuhkan militer yang tidak berjiwa Orde Baru. Kita akan hormati militer yang anti-Orde Baru.

Kejahatan di masa yang lalu sudah makan terlalu banyak korban jiwa manusia. Dan penderitaan yang mengucurkan banyak airmata dan darah pun sudah berlangsung terlalu lama.

Tulisan ini diakhiri dengan himbauan kepada para korban peristiwa 65 ( termasuk juga para sanak-saudaranya dan teman-teman terdekatnya) : Pengalaman kalian adalah guru besar bangsa dalam soal-soal penderitaan dan perikemanusiaan! Berikanlah tanpa ragu-ragu pengalaman kalian sebagai sumbangan kepada pembangunan jiwa dan moral baru bangsa. Demi tercapainya masyarakat adil dan makmur, yang juga dijiwai sungguh-sungguh Pancasila dan Bhinneka Tunggal Ika!
