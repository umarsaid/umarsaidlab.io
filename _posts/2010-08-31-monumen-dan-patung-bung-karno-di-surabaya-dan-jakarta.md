---
layout: post
title: Monumen dan patung Bung Karno di Surabaya dan Jakarta
date: 2010-08-31
---

Berikut di bawah ini disajikan berita-berita tentang didirikannya Monumen Kelahiran Bung Karno di Surabaya, yang dikutip dari beberapa sumber. Peristiwa yang diungkap dalam berita itu merupakan hal yang penting bagi pelurusan sejarah Bung Karno. Juga mempunyai arti tersendiri, ketika  negara dan bangsa kita sedang dalam situasi yang tidak menentu, dalam keadaan terpuruk, dan ruwet atau kacau di berbagai bidang seperti sekarang ini.



Berita-berita tersebut adalah sebagai berikut :



Surabaya Bangun Monumen Kelahiran Bung Karno

Minggu, 29 Agustus 2010

TEMPO Interaktif, Surabaya - Pemerintah Kota Surabaya membangun monumen kelahiran Bung Karno di Jalan Pandean IV/40, Kelurahan Peneleh, Kecamatan Genteng, Surabaya. Peletakan batu pertama pembangunan monumen tersebut dilakukan oleh Wali Kota Surabaya, Bambang Dwi Hartono, Minggu (29/8).

Ketua panitia acara pembangunan monumen Bung Karno, Peter A Rohi mengatakan monumen itu untuk menandakan tempat kelahiran Soekarno. Rencananya, monumen tersebut akan diresmikan pada 6 Juni 2012 mendatang. "Kami hanya membuat tugu kecil sebagai penanda, sebab tempatnya sempit dan tak memungkinkan untuk membuat monumen yang besar," kata Peter.

Peter menambahkan, pembangunan monumen Bung Karno itu juga dimaksudkan untuk meluruskan anggapan yang menyebutkan bahwa presiden pertama Indonesia tersebut lahir di Blitar, Jawa Timur. Padahal dalam penelusuran sejarah, kata Peter, Bung Karno dilahirkan di kampung Pandean. "Kami ingin meluruskan sejarah," ujar Peter.

Berdasarkan penelusuran yang dia lakukan, ayah Sokarno, R Soekeni Sosrodiharjo yang juga seorang guru, dipindah dari Singaraja, Bali ke Sekolah Sulung Surabaya pada akhir 1900. Pada saat itu istri Soekeni, Nyoman Rai Srimben tengah hamil tua. "Dipastikan Bung Karno lahir di Surabaya," imbuh Peter.

Selain itu, ujar Peter, putra-putri Bung Karno juga membenarkan bahwa ayahnya lahir di Kota Pahlawan. Sebab suatu ketika Guruh Soekarnoputra pernah menyatakan pada Bambang Dwi Hartono bahwa ayahnya lahir di kampung di tepi Kali Mas itu. "Banyak referensi yang menyatakan Bapak lahir di Blitar, itu keliru," kata Bambang menirukan Guruh.

Menurut Peter, biaya pembangunan monumen itu berasal dari urunan para pecinta dan pengagum Bung Karno. Peter sebenarnya sempat meminta bantuan dana kepada sejumlah pengusaha namun tidak mendapat respon. "Akhirnya kami urunan, dan terwujudlah monumen ini," kata Peter yang juga wartawan senior.

Sebelum peletakan batu pertama, acara itu didahului dengan seminar bertema "Soekarno dan Surabaya" di Balai Pemuda, Sabtu sore kemarin. Seminar itu mendatangkan pembicara Dr Yuke Ardhiati, Dr Nurinwa Hendrowinoto, Dr Tjuk Kasturi Sukiadi dan Bambang Budjono.

* * *

Luruskan Sejarah Kelahiran Soekarno


Minggu, 29 Agustus 2010
SURABAYA, KOMPAS.com--Wali Kota Surabaya Bambang Dwi Hartono akan mengirim surat ke Sekretaris Negara (Sekneg) terkait pelurusan sejarah tempat kelahiran proklamator RI Soekarno (Bung Karno) yang berasal dari Surabaya dan bukan dari Blitar.
"Kami akan mengirim surat ke Sekneg terkait itu," kata Bambang di acara Seminar Pelurusan Sejarah Tempat Kelahiran Bung Karno yang digelar di Balai Pemuda Surabaya, Sabtu.


Bahkan dalam pidato Bung Karno, lanjut dia, dikatakan Bung Karno sendiri mengaku sebagai warga Surabaya. Namun, lanjut dia, sejarah tersebut diputarbalikkan, sehingga seolah-olah Bung Karno lahir di Blitar.
Menurut dia, gambaran menghargai kepahlawan di Indonesia hingga saat ini masih kurang.

* * *

"Bung Karno Arek Suroboyo"

Sabtu, 28 Agustus 2010

Surabaya (ANTARA News) - "Banyak di antara kita yang tidak mengetahui bahwa semangat perjuangan dan jiwa seni Sang Pemimpin Besar Revolusi Indonesia, Soekarno (Bung Karno) adalah juga `Arek Suroboyo` (pemuda asal Kota Surabaya)," kata Peneliti dan pengajar di Universitas Trisakti Jakarta, Yuke Ardhiati.

Yuke mengatakan bahwa setiap bulan Juni dan Agustus, hampir dapat dipastikan "ruh Soekarno" hadir di bumi Indonesia.

"Bulan Juni menjadi bulan khusus baginya (hari kelahirannya), bulan Agustus adalah bulan diproklamasikan kemerdekaan bagi bangsa Indonesia," katanya di acara Seminar Pelurusan Sejarah Tempat Kelahiran Bung Karno yang digelar di Balai Pemuda Surabaya, Sabtu.

Menurut dia, si Arek Suroboyo itu telah lama "Kondur Sowan ing Ngarsaning Gusti Allah" (berpulang ke Rahmatullah atau wafat, red), namun suaranya yang menggelegar di setiap kesempatan menyapa rakyatnya, masih selalu bergema menggaungkan resonansi di setiap sudut hati.

Eksplorasi Yuke untuk mengungkapkan semangat "Arek Suroboyo" yang terpantul dari jiwa Soekarno, diterapkan dalam teori arketipe tentang konsep diri dari gagasan Carl Gustav Jung.

Arketipe, sebagai refleksi sifat dominan dari karakteristik manusia. Dalam diri Soekarno tertanam gabung dari berbagai arketipe yaitu "mother", "hero" dan "mona" berpadu sekaligus.

"Soekarno memiliki sifat menyerupai rahim ibu. Sebagai penyedia sebuah kehadiran, ada semangat patriotik. Namun sekaligus memiliki daya pesona yang luar biasa dari dirinya," paparnya.

Pengalaman dan kebiasaan Soekarno sejak usia muda, kata dia, dijelaskan dalam lima hal, yakni timangan (kekudangan orang tua), kecintaan terhadap unsur air, menolak nuansa kolonialisme, cinta romantisme terhadap negara, citra kemegahan budaya Jawa Kuno.

"Dan terakhir pemuda berjiwa patriot," katanya.

Dalam semua penulisan biografi Soekarno sebelum tahun 1970, semuanya menulis Bung Karno lahir di Surabaya.

Akhir tahun 1900, R.Soekani Sosrodiharjo (ayahanda Soekarno) dipindahtugaskan dari Singaraja Bali sebagai guru sekolah rakyat Sulung, Surabaya.

Di Surabaya itulah istrinya, Nyoman Rai Srimben melahirkan seorang putera yang diberi nama Kusno yang kemudian menjadi Soekarno pada 6 Juni 1901.





Demikianlah berita-berita yang diambil dari beberapa  sumber.

Di bawah berikut ini adalah sekadar berbagai tanggapan mengenai berita-berita tersebut di atas :



--  Dilihat dari berbagai segi dan sudut pandang,  pembangunan monumen kelahiran Bung Karno di Surabaya ini merupakan peristiwa yang penting. Sebab, selama ini masih sedikit sekali monumen atau peninggalan-peninggalan bersejarah tentang kehidupan dan perjuangan Bung Karno yang bisa dijadikan oleh rakyat untuk mengenang pemimpin besar bangsa kita itu beserta gagasan agungnya dan ajaran-ajaran revolusionernya.



Memang, sudah banyak kalangan dalam masyarakat yang mengenal  museum dan makam Bung Karno di Blitar,  yang tiap harinya sepanjang tahun dikunjungi banyak orang, bahkan seringkali sampai ribuan orang.



Walaupun tempat pembuangannya di Endeh (Flores) , dan bekas tempat tinggalnya di Bengkulu juga menjadi perhatian banyak orang, namun masih banyak peninggalan sejarah hidup Bung Karno lainnya yang  patut sekali dikenang dan dihormati oleh para pencintanya atau pengagumnya, termasuk masyarakat luas lainnya.



Didirikannya monumen kelahiran Bung Karno di Surabaya adalah penting bukan saja karena kota ini merupakan tempat lahir Bung Karno melainkan juga karena di kota inilah ia mulai dalam usia muda belia belajar politik dan mengenal Marxisme atau sosialisme dari pemimpin besar gerakan Islam Haji Oemar Said Tjokroaminoto.



Jadi, Surabaya adalah tempat beriwayat bagi Bung Karno selagi masih muda belia, seperti halnya kota Bandung, sebelum ia dibuang oleh pemerintah kolonial Belanda ke Endeh dan Bengkulu.



Didirikannya monumen kelahiran Bung Karno di Surabaya baru-baru ini menambah sarana bagi banyak orang untuk mengenang kembali jasa-jasa besar tokoh agung  Pemimpin Besar Revolusi (PBR) Bung Karno, yang merupakan  salah satu dari tokoh-tokoh yang menjadikan Surabaya sebagai Kota Pahlawan



Karena itu, didirikannya monumen kelahiran Bung Karno di Kota Pahlawan  (Surabaya) ini bisa mempunyai arti simbolis, yang  menjadikan satunya tokoh Pemimpin Besar Revolusi ini (Bung Karno) dengan kota yang melahirkan Hari Pahlawan 10 November.



Pembangunan monumen di Surabaya ini menyusul peristiwa penting lainnya beberapa minggu sebelumnya, yaitu didirikannya patung besar Bung Karno (lebih dari 9 meter)  di kampus Universitas Bung Karno di Jakarta, yang menarik perhatian banyak orang.



Pembangunan monumen di Surabaya dan didirikannya patung besar di Universitas Bung Karno mengindikasikan bahwa opini publik terhadap Bung Karno, yang pernah lebih dari 32 tahun diracuni oleh Orde Baru, sudah mulai berobah sedikit demi sedikit.



Mengingat situasi di Indonesia dewasa ini, yang betul-betul sudah terlalu sakit parah dan payah dengan segala macam kerusakan moral yang menimbulkan bermacam-ragam kejahatan (antara lain korupsi dan pelanggaran hukum, kongkalikong dengan neo-liberalisme) maka opini yang pro-ajaran-ajaran Bung Karno adalah penting untuk bangsa kita.



Untuk itu, banyaknya sarana untuk mengenal kebesaran sejarah perjuangan Bung Karno  dan ajaran-ajaran revolusionernya – yang bisa berupa buku-buku, kaset-kaset berisi pidato-pidatonya, lagu-lagu, foto-foto bersejarahnya, monumen atau patung-patungnya  --  perlu diperbanyak di mana-mana oleh inisiatif masyarakat, seperti yang dilakukan  oleh berbagai kalangan di Surabaya atau di Universitas Bung Karno di Jakarta.



Tersebarnya  secara seluas mungkin ajaran-ajaran revolusioner Bung Karno adalah salah satu di antara unsur-unsur penting dan juga investasi utama dalam usaha bersama untuk membangun kekuatan besar yang bisa mendorong revolusi rakyat sesuai dengan gagasan-gagasan Pemimpin Besar Revolusi Bung Karno, dalam situasi dan kondisi sekarang.



Penyebaran ajaran-ajaran revolusioner Bung Karno seluas mungkin dan sebanyak mungkin adalah tugas utama bagi seluruh kekuatan demokratis di Indonesia, yang menginginkan adanya perubahan besar-besaran dan fundamental di negeri kita, demi kepentingan rakyat, terutama rakyat miskin.
