---
layout: post
title: Masalah Bung Karno dan rehabilitasi korban Orde Baru
date: 2003-02-13
---

Sekarang makin banyak orang yang yakin bahwa Orde Baru memang satu rezim militer yang sudah membikin kerusakan besar-besaran terhadap bangsa dan negara. Makin banyak bukti bahwa politik rezim militer yang berintikan Golkar dan TNI-AD ini telah menyebar racun pembusukan moral dan menumbuhkan kebudayaan korupsi dan kekerasan, yang tidak ada taranya dalam sejarah Republik. Politik yang anti-demokrasi ini telah dilaksanakan secara menyeluruh dan intensif dalam jangka waktu yang lama sekali, yaitu lebih dari 30 tahun. Karena itulah dampaknya besar sekali, sampai sekarang.

Pengrusakan yang paling parah yang dilakukan oleh Orde Baru adalah di bidang mental atau moral. Mental dua generasi bangsa kita telah dirusak olehnya. Yang pertama dirusak atau dihancurkan adalah generasi 45. Generasi ini, yang ikut dalam perjuangan untuk kemerdekaan RI, telah dibunuhi secara fisik atau secara politik. Mereka itu terdiri dari para pejuang revolusioner, yang mendukung politik Presiden Sukarno. Sebagian lainnya dari angkatan 45 ini telah dirusak mentalnya atau moralnya dengan merekrutnya untuk mendukung Orde Baru dan dengan membagi-bagikan fasilitas dan kedudukan. Tidak sedikit di antara generasi 45 ini yang menjadi korup, reaksioner, dan bermental kontra-revolusioner.

Yang kedua dihancurkan atau dirusak mentalnya adalah generasi yang dididik untuk menjadi birokrasi pelaksana politik Orde Baru di bidang militer, eksekutif, legislatif, judikatif, dan lembaga-lembaga resmi (dan swasta). Aparat pemerintahan semasa pemerintahan Habibi, Gus Dur dan Megawati-Hamzah sekarang ini adalah produk didikan Orde Baru.

## INDOKTRINASI YANG MERUSAK FIKIRAN

Kerusakan mental atau kebobrokan moral di kalangan eksekutif, legislatif, judikatif, dan kalangan tokoh-tokoh masyarakat lainnya ini menyebabkan reformasi tidak jalan, korupsi tidak bisa dibrantas, dan hukum tidak bisa ditegakkan. Politik anti-demokrasi Orde Baru yang dibarengi oleh indoktrinasi anti-Sukarno dan anti-PKI secara besar-besaran dan sistematis selama puluhan tahun itu telah merusak fikiran banyak orang, terutama di kalangan elite bangsa. Dampak indoktrinasi yang menyesatkan ini masih kuat sampai sekarang.

Itulah sebabnya mengapa masalah penggulingan Bung Karno oleh Suharto dkk, masalah pembunuhan besar-besaran tahun 1965, masalah pemenjaraan ratusan ribu orang tidak bersalah, masalah ex-Tapol beserta keluarga mereka, sampai sekarang belum dapat diselesaikan. Padahal, itu semua adalah masalah besar bangsa.

## Pëngkhianatan Terhadap Bung Karno

Dalam konteks sejarah pada waktu itu, penggulingan kekuasaan Presiden Sukarno oleh Suharto dkk adalah kontra-revolusi, adalah pengkhianatan. Penggulingan Presiden Sukarno oleh sebagian TNI-AD yang berkomplot dengan kekuatan reaksioner dalamnegeri dan luarnegeri (terutama AS dan Inggris, waktu itu) adalah kelanjutan dari pembrontakan kontra-revolusioner PRRI-Permesta. Dan semuanya ini ada hubungannya, langsung atau tidak langsung, sedikit atau banyak, dengan faktor-faktor internasional yang bersangkutan dengan Perang Dingin.

Sekarang, setelah Suharto bisa dipaksa turun dari tahta bersama Orde Barunya, sebenarnya terbuka kemungkinan untuk memeriksa kembali pengkhianatan kontra-revolusioner Suharto dkk terhadap Bung Karno. Juga penyelidikan terhadap pembantaian besar-besaran terhadap jutaan orang tidak bersalah, yang kebanyakan adalah orang-orang “ kiri” pendukung politik Bung Karno. Pemenjaraan sewenang-wenang, selama puluhan atau belasan tahun, terhadap anggota-anggota PKI atau orang-orang yang dituduh “komunis”, perlu dibongkar kembali. Masalah nasib para ex-Tapol, yang kebanyakan sampai sekarang masih mendapat perlakuan tidak selayaknya sebagai warganegara, perlu diangkat terus-menerus, dengan berbagai cara dan bentuk.

Orde Barunya Suharto dkk sudah melakukan banyak kejahatan dan beraneka-ragam kesalahan serius terhadap bangsa dan negara. Kita semua menyaksikan sendiri sekarang ini - atau bahkan mengalami sendiri – segala akibat buruk dari kejahatan dan kesalahan politik ini. Segala persoalan rumit dan kasus-kasus yang berbau busuk itu pada intinya, atau pada hakekatnya, adalah produk kebudayaan dan cara berfikir Orde Baru.

## Lebih Lama Dari Fasisme Hitler

Mengangkat kembali persoalan pengkhianatan sebagian TNI-AD (di bawah pimpinan Suharto dkk) terhadap Bung Karno dan pembantaian atau pemenjaraan begitu banyak orang-orang “kiri” adalah salah satu di antara tugas sejarah bangsa kita. Sebab, peristiwa ini merupakan pembuka-jalan bagi berlangsungnya diktatur militer selama jangka yang lama sekali, yaitu 32 tahun. Seperti halnya diktatur fasis Hitler bagi bangsa Jerman, diktatur militer Suharto dkk ini merupakan noda sejarah bagi bangsa Indonesia. Bedanya, kalau periode fasisme Hitler berlangsung 12 tahun (antara tahun 1933 sampai 1945), maka diktatur militer Suharto dkk merajalela di negeri hampir tiga kali lebih lama.

Bagi penulisan sejarah bangsa secara akurat atau lebih objektif, amat penting membongkar kembali penggulingan kekuasaan Bung Karno yang didahului oleh penghancuran kekuatan politik yang mendukungnya, terutama golongan “kiri”. Penulisan kembali sejarah ini penting untuk menyelesaikan berbagai persoalan yang kita hadapi dewasa ini, dan juga untuk pendidikan bagi generasi bangsa yang akan datang. Generasi yang akan datang kita harus - dan berhak untuk - mengetahui noda bangsa yang dibikin oleh rezim Orde Baru, untuk tidak akan terulang lagi.

Tetapi, seperti kita saksikan sendiri selama ini, pekerjaan membongkar dan menggugat kejahatan dan kesalahan Suharto dkk beserta Orde Barunya ini tidak mudah. Mesin birokrasi yang telah dibangun dengan indoktrinasi anti-Sukarno dan anti-PKI selama 32 tahun masih dipakai sampai sekarang. Kalau sedikit demi sedikit, kebusukan politik Orde Baru di berbagai bidang sudah mulai ditelanjangi dan dinajiskan orang, tetapi kejahatannya terhadap Bung Karno dan golongan “kiri” belum cukup terungkap. Oleh karena hebatnya tekanan propaganda dan indoktrinasi jang dijalankan selama puluhan tahun, maka di mata banyak orang Sukarno adalah orang yang bersalah karena melindungi atau bersimpati kepada PKI. Karena itu, tuntutan untuk merehabilitasi nama baik Bung Karno masih terus mengalami kesulitan., sampai sekarang.

## Aib Dan Dosa Besar Bangsa

Kejahatan Orde Baru terhadap jutaan golongan “kiri” yang telah dibantai secara besar-besaran dan pemenjaraan sewenang-wenang ratusan ribu tapol juga belum diangkat sepenuhnya. Apa yang telah disiarkan dan diketahui umum selama ini adalah hanya sekelumit saja dari kekejaman dan penderitaan jang dialami oleh para korban Orde Baru beserta keluarga mereka selama puluhan tahun. Bangsa kita perlu menyadari bersama-sama bahwa pembunuhan massal tahun 1965 dan pemenjaraan ratusan ribu orang tidak bersalah itu adalah dosa besar atau kesalahan monumental yang tidak boleh terulang lagi. Adalah kewajiban nasional kita bersama sebagai bangsa untuk mengungkap seluas mungkin masalah penggulingan Bung Karno dan pembunuhan massal dan pemenjaraan sewenang-wenang terhadap golongan “kiri”.

Untuk itu kita semua harus berusaha, dengan berbagai cara dan bentuk, dan melalui sarana dan wahana yang mungkin ditempuh, untuk mengangkat kembali masalah besar bangsa ini. Mengangkat kembali soal penggulingan Bung Karno dan penghancuran golongan “kiri” tidaklah mendatangkan kerugian apa-apa bagi bangsa dan negara. Juga tidak merugikan siapa-siapa. Dan juga tidak membahayakan siapa-siapa. Termasuk mereka yang anti-Sukarno dan anti-kiri (atau anti-PKI). Bahkan sebaliknya, akan mendatangkan kebaikan bagi bangsa dan negara. Sebab, itu berarti memperbaiki kesalahan, dan menebus dosa, menghilangkan aib bangsa. Jadi, tindakan ini adalah benar. Dan tidak melakukannya adalah salah besar.

## Tugas Kita Bersama

Artinya, rehabilitasi nama baik Bung Karno dan rehabilitasi para korban Orde Baru adalah suatu agenda yang harus dilaksanakan oleh bangsa kita. Karena sebagian besar dari aparat negara atau birokrasi masih didominasi oleh mereka yang berfikiran cara Orde Baru, maka kita tidak boleh menaroh ilusi bahwa mereka akan dengan sukarela atau senang hati melakukannya. Indoktrinasi anti “kiri” yang dijalankan secara intensif dan sistematis selama lebih dari 32 tahun telah mendatangkan kerusakan yang dahsyat sekali dalam cara berfikir banyak orang.

Oleh karena rehabilitasi nama Bung Karno dan rehabilitasi para korban Orde Baru adalah sesuatu yang hanya mendatangkan kebaikan bagi bangsa - dan bukan sebaliknya - maka adalah kewajiban nasional kita semuanya untuk melaksanakannya. Apa yang telah mulai dilakukan oleh Gus Dur (permintaan ma’af kepada korban Orde Baru, peresmian kembali nama stadion Gelora Bung Karno, pernyataannya tentang orang-orang yang terpaksa “klayaban” di luarnegeri) perlu sama-sama kita kembangkan lebih lanjut dengan langkah-langkah yang lebih kongkrit lainnya.

Untuk itu, masalah rehabilitasi nama baik Bung Karno sebagai sebagai bapak bangsa dan pendiri Republik Indonesia dan rehabilitasi para korban Orde Baru harus lebih digalakkan dari pada yang sudah-sudah. Mengenai korban Orde Baru, kegiatan-kegiatan berbagai organisasi seperti Pakorba, LPR-KROB, LPKP, YPKP, Solidaritas Nusa Bangsa dan lain-lainnya perlu mendapat dukungan dari berbagai fihak, termasuk dari Komnas Ham. Kerjasama yang telah digalang dengan sejumlah organisasi yang bernaung di bawah NU dalam kegiatan-kegiatan mengenai soal ex-Tapol dan keluarga korban Orde Baru adalah contoh menggembirakan yang perlu ditiru dan dikembangkan.

Masalah korban Orde Baru adalah masalah besar bangsa. Kita tidak bisa dan tidak boleh melupakan hal ini.
