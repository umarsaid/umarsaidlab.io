---
layout: post
title: Tsunami politik di Amerika Latin kalau AS menyerang Venezuela
date: 2006-02-12
---

Banyak tanda-tanda yang menunjukkan bahwa prahara politik yang terjadi antara pemerintahan Venezuela dan AS tidak mereda, dan bahkan makin menjadi-jadi, sehingga besar kemungkinan akhirnya menjadi “tsunami” yang bisa menggoncangkan negeri-negeri di benua Amerika Latin. Pertentangan antara Caracas dan Washington kelihatannnya sudah begitu tajamnya sehingga sulit diramalkan adanya kemungkinan perdamaian atau kerujukan antara kedua fihak. Di antara tanda-tanda akan terjadinya sesuatu yang lebih serius antara kedua pemerintahan itu adalah yang sebagai berikut:

Dalam suatu siaran televisi baru-baru ini, Presiden Hugo Chavez mengatakan bahwa “presiden AS George W. Bush adalah orang gila. Ia menganggap dirinya sebagai pemilik dunia, dan sekarang ia bersama Inggris merencanakan penyerangan terhadap Iran, dan sedang mengadakan komplotan terhadap Venezuela.” (Associated Press 9/2/06)

Dengan mengenakan baju dan baret merah pasukan para, ia mengatakan bahwa senjata Kalashnikov sebanyak 100.000 pucuk yang sudah dipesan dari Rusia tidak mencukupi. Venezuela memerlukan satu juta laki-laki dan perempuan yang dipersenjatai dengan baik, karena Washington sedang mempersiapkan penyerangan terhadap Venezuela . “Saya minta persetujuan untuk membeli persenjataan lainnya, karena orang-orang gringo (orang Amerika) menginginkan kita tidak mempunyai senjata” katanya.

Tahun yang lalu, pemerintah AS berusaha untuk menghalangi Spanyol dalam penjualan 12 pesawat terbang militer kepada Venezuela, yang dibikin dengan teknologi AS. Tetapi Madrid akhirnya memutuskan untuk tetap akan meneruskan penjualannya kepada Venezuela, dengan memakai teknologi Eropa, meskipun harganya lebih mahal.

Venezuela telah memberitahu kepada Spanyol bahwa pemerintahannya akan membeli pesawat-pesawat militer dan kapal-kapal patroli dari negeri-negeri lainnya, kalau tidak bisa mengatasi veto AS yang melarang penjualan perlengkapan perang yang dibikin dengan teknologi AS. “Kita akan membeli kapal-kapal patroli dari RRT, Rusia, Iran, India atau bahkan dari Brasilia. Seperti yang kalian tahu, Mr Danger (maksudnya presiden Bush) mengadakan ancaman dan tekanan di seluruh dunia sehingga tidak ada orang mau menjual kepada kita barang-barang yang kita butuhkan untuk membela negeri kita.” katanya.

PERANG PERLAWANAN 100 TAHUN !

Ia mengatakan bahwa negaranya sudah menghubungi negara-negara yang tidak bisa ditakut-takuti AS untuk menjual perlengkapan militer kepada Venezuela. Venezuela sedang menunggu pengapalan sejumlah peluncur-peluncur roket yang modern, tetapi Hugo Chavez tidak mau mengatakan dari negeri mana dan jumlahya berapa. Dikatakannya bahwa Venezuela tidak lama lagi akan mempunyai satu juta pasukan militer cadangan untuk perang perlawanan.

Menurutnya, kalau pemerintah AS melancarkan serangan terhadap Venezuela, maka akan mulailah perang 100 tahun. Dan perang ini tidak hanya dengan Venezuela saja, karena ia yakin bahwa bangsa-bangsa Amerika Latin lainnya juga akan bangkit melawan musuh yang sama ini.

Pemerintah AS sudah menyatakan bahwa tidak mau mensupply lagi suku-cadang (spare parts) untuk 22 pesawat tempur-pembom (fighter bomber) F-16 yang merupakan tulang-punggung Angkatan Udara Venezuela. Presiden Hugo Chavez mengatakan bahwa karena usangnya perlengkapan pesawat-pesawat ini, maka banyak yang tidak bisa beroperasi lagi, dan karenanya ia sedang memikirkan untuk membeli MIG-29s dari Rusia.

Ia juga menegaskan bahwa kalau pemerintahan Bush ingin memutuskan hubungan diplomatik dengan Venezuela, maka ia tidak akan berpikir dua kali untuk menutup seluruh instalasi penyulingan minyak yang dimiliki Venezuela di AS. “Kita akan lihat nanti bagaimana jadinya dengan harga minyak”, katanya.

Menurutnya, kalau pemeritah AS memutuskan hubungan diplomatik dengan Venezuela, maka ekspor sebanyak 1,5 juta barrel sehari ke AS akan dialihkan ke pasaran dunia lainnya. Sejak lama negara-negara seperti RRT, India dan negara-negara Eropa ingin membeli minyak dari Venezuela, kata Hugo Chavez.

“Sikap yang imperialis, suka membunuh besar-besaran dan fasis dari presiden AS tidak ada batasnya. Saya kira Hitler menyerupai anak bayi yang sedang menyusu saja dibandingkan George W. Bush. », kata Hugo Chavez (The imperialist, genocidal, fascist attitude of the US president has no limits. I think Hitler would be like a suckling baby next to George W. Bush. – BBC 5/2/06)

REVOLUSI BOLIVARIAN MUSUH IMPERIALISME AS

Sikap Hugo Chavez yang anti-imperialisme AS sejak ia jadi presiden Venezuela tahun 1998 makin lama kelihatan makin mengeras, sehingga ia mengucapkan berkali-kali kata-kata yang pedas sekali terhadap presiden AS George W. Bush dalam berbagai kesempatan. Terutama sekali sejak terjadinya kudeta, dalam tahun 2002, oleh sebagian tentara Venezuela yang berkomplot dengan para kapitalis dan kalangan reaksioner lainnya di dalamnegeri, dengan bantuan dari CIA.

Berkat aksi besar-besaran yang dilancarkan oleh rakyat, yang sebagian terbesar terdiri dari buruh dan tani dan penduduk miskin dalam kota Caracas dan sekitarnya, kudeta oleh kaum reaksioner ini hanya berumur dua hari. Presiden Hugo Chavez yang dipecat oleh kekuatan kudeta dari kedudukannya sebagai presiden terpilih dapat dikembalikan menjabat presiden berkat dukungan penduduk dari kalangan bawah. Kegagalan kudeta yang disokong oleh CIA ini juga berkat sikap sebagian tentara yang setia kepada Hugo Chavez untuk melaksanakan revolusi Bolivarian. Kudeta yang hanya berumur dua hari ini menunjukkan bahwa usaha Washington untuk menghalangi kebangkitan rakyat Venezuela (dan negeri-negeri Amerika Latin lainnya) sampai sekarang tidak berhasil.

Kegagalan Washington dalam mendukung kudeta tahun 2002 membikin makin galaknya Hugo Chavez beserta para pendukungnya. Bukan saja makin galak dalam menghadapi pemerintah AS, tetapi juga makin menggebu-gebu dan bersemangat dalam memperdalam lebih jauh Revolusi Bolivarian. Revolusi Bolivarian yang dilancarkan oleh Hugo Chavez beserta para pendukungnya merupakan perkembangan penting yang tidak ada taranya dalam sejarah Amerika Latin.

Dalam tingkat sekarang, nampak sekali bahwa Revolusi Bolivarian Venezuela di bawah pimpinan presiden kiri dan revolusioner Hugo Chavez merupakan promotor dan akselerator perubahan politik, sosial, ekonomi, dan kebudayaan, bukan saja di Venezuela, melainkan juga di berbagai negeri Amerika Latin lainnya, secara langsung atau tidak langsung. Itulah sebabnya maka Revolusi Bolivarian dianggap musuh oleh imperialisme AS, dan “populisme” Hugo Chavez dianggap sebagai gangguan besar bagi stabilitas pengaruh AS di kawasan Amerika Latin.

TINDAKAN UNTUK PERBAIKI HIDUP RAKYAT

Sejak terjadi kudeta dalam tahun 2002, presiden Hugo Chavez makin mempercepat atau memperkuat usaha-usaha untuk mengadakan perombakan dan perubahan besar di Venezuela, terutama untuk memperbaiki kehidupan rakyat banyak, khususnya di kalangan rakyat miskin, yang selama ratusan tahun hidup dalam kesengsaraan. Banyak hal-hal baru dan revolusioner telah diciptakan di bawah pimpinan Hugo Chavez.

Karena partai yang dipimpinnya menguasai secara mutlak hampir seluruh kursi dalam parlemen, maka banyak undang-undang yang menguntungkan perbaikan hidup rakyat banyak yang telah bisa dibuat. Termasuk adanya Undang-undang dasar (Konstitusi) yang mencerminkan semangat revolusioner dari sosialisme Bolivarian. Hugo Chavez sendiri merupakan pemimpin negara yang sosoknya termasuk langka di Amerika Latin.

Ia adalah mantan pimpinan militer, yang kiri dan anti-imperialis AS, tetapi yang berhasil meraih terbesar suara dalam berbagai pemilihan yang berkali-kali diadakan secara demokratis (untuk pemilihan presiden secara langsung , untuk referendum dll). Tidak salahlah kalau ada orang-orang yang mengatakan bahwa ia memiliki karisme yang besar, kecakapan bicara yang luar biasa, mempunyai sosok populis yang kuat, dan mempunyai program politik, ekonomi dan sosial yang radikal revolusioner. Partai yang dipimpinnya telah memenangkan berbagai pemilihan demokratis, sehingga menguasai 20 daerah dari seluruh daerah yang berjumlah 23.

Untuk memperingati 7 tahun kedudukannya sebagai presiden, permulaan Februari tahun 2006 ini ia mengumumkan berlakunya kenaikan gaji atau upah minimum dengan 15 % sehingga menjadi $220 sebulannya. Ini merupakan kenaikan yang lumayan besarnya, kalau diingat bahwa gaji atau upah minimum di Venezuela dalam tahun 1999 adalah $183 sebulannya, yang kemudian naik menjadi $188 sebulan dalam tahun 2005.

Tingkat hidup sebagian terbesar rakyat di Venezuela sejak pemerintahan dipimpin oleh Hugo Chavez makin meningkat, kalau dibandingkan dengan kehidupan rakyat negeri-negeri Amerika Latin lainnya. Menurut angka-angka PBB, sekitar 225 juta orang (yang merupakan 43% penduduk Amerika Latin) adalah orang-orang miskin, dan dari jumlah itu ada sekitar 96 juta orang yang harus hidup hanya kurang dari $ 1 seharinya.

Yang barangkali menarik banyak orang di luar Venezuela ialah diumumkannya oleh Hugo Chavez serentetan peraturan atau ketentuan bagi kaum perempuan pada umumnya, dan kaum perempuan dari kalangan yang kurang mampu pada khususnya. Untuk ibu-ibu rumahtangga yang miskin, diumumkan adanya gaji setiap bulan. Sebab, dalam konstitusi Venezuela diakui bahwa pekerjaan seorang perempuan dalam rumah-tangga dan mengasuh anak-anak adalah pekerjaan yang mempunyai nilai ekonomi, dan karenanya harus mendapat gaji dan penghargaan dari bangsa. Untuk ibu-ibu yang demikian ini diberikan gaji $ 200 sebulan. Ada 200.000 perempuan ibu rumahtangga yang segera menerima gaji $200 sebulan ini, dan yang jumlahnya mungkin bertambah sampai setengah juta orang. Ini merupakan konsepsi baru dan sangat menarik di dunia.

TUJUAN PROGRAM MISSI KRISTUS : ZERO KEMISKINAN

Berbagai tindakan telah dilakukan oleh pemerintah Venezuela untuk memperbaiki tingkat hidup rakyat Venezuela. Menurut angka-angka resmi pemerintah, dalam tahun 1998 sekitar 50% dari penduduk Venezuela hidup dalam keadaan yang payah sekali. Untuk menghapuskan kemiskinan ini telah dilancarkan program yang dinamakan Missi Kristus, yang tujuannya ialah terciptanya “zero kemiskinan” dalam tahun 2021.

Untuk meringankan penderitaan rakyat, pemerintah Venezuela juga memberikan makan gratis kepada 1. 270 000 orang dua kali sehari. Ada sekitar 6 000 “gedung makan” (meal homes) yang setiap harinya menyediakan makanan untuk 200 orang tiap gedungnya, yang terdapat di seluruh negeri. Juga ada program sosial-ekonomi yang diberi nama Mercal, yang memungkinkan sebanyak 500.000 penduduk Venezuela untuk membeli bahan makanan dan minuman dengan potongan harga 50% di 14.000 toko Mercal di seluruh negeri. Bahan makanan dan minuman ini mendapat subsidi dari negara.

Tindakan yang luar biasa besarnya juga telah diadakan dalam bidang kesehatan dan pendidikan. Banyak tindakan-tindakan yang telah dilakukan untuk - setahap demi setahap – adanya perubahan dalam politik, ekonomi, sosial dan kebudayaan di Venezuela. Ini semua dilakukan dalam rangka sosialisme Bolivarian, yang menjadi bidang perjuangan Hugo Chavez dan pendukung-pendukungnya.

Revolusi sosialisme demokratiknya Hugo Chavez juga sedang berusaha mengubah kenyataan pahit yang berikut ini : 77 % tanah pertanian negeri ini dimiliki oleh 3% penduduk, yang kebanyakan dibiarkan menjadi tanah kosong dan tidak diolah, sedangkan banyak petani-petani yang tidak bertanah terpaksa hidup sengsara, sejak ratusan tahun !

Revolusi Bolivarian yang dilancarkan di Venezuela adalah prahara anti-imperialisme AS dan anti-neo liberalisme, yang gemuruhnya sudah berkumandang di seluruh benua Amerika Latin, dan mendapat simpati dari banyak kalangan di berbagai penjuru dunia. Bukan hanya dari negara-negara yang berdekatan seperti Kuba, Bolivia, Argentina, Brasilia, Cili, Ekuador, dan Peru saja, tetapi juga dari berbagai kalangan di negara-negara yang jauh letaknya, seperti Iran, Siria, India, Rusia, RRT, dan beberapa negara di Eropa.

Dalam bulan-bulan yang akan datang, atau tahun-tahun yang akan datang, kita semua mungkin akan menyaksikan terjadinya hal-hal yang lebih menarik, atau mengagumkan, atau bahkan mengejutkan dari perkembangan Revolusi Bolivarian, yang juga disebut-sebut sebagai sosialisme demokratik, atau sosialisme abad ke-21, di benua Amerika Latin ini. Sebab, prahara yang sedang mulai muncul dewasa ini mungkin akan menggoncangkan berbagai negeri di “halaman belakang” AS (backyard) , dan juga menimbulkan dampak yang tidak kecil untuk negeri-negeri lainnya.

Bagi banyak orang di Indonesia, yang sudah berpuluh-puluh tahun dicekoki oleh rejim militer Orde Baru dengan berbagai racun anti-sosialisme atau anti-komunisme, dan disuapi hal-hal yang serba baik melulu tentang Amerika Serikat, perlu merenungkan tentang pergolakan yang terjadi di dunia sekarang ini, antara lain, baik yang di Timur Tengah maupun di Amerika Latin.
