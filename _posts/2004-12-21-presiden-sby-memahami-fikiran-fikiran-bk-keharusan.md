---
layout: post
title: 'Presiden SBY : Memahami fikiran-fikiran BK keharusan'
date: 2004-12-21
---

Kepada para pembaca diharapkan perhatian mereka terhadap berita yang disiarkan oleh Media Indonesia Online 17 Desember 2004 mengenai pidato presiden SBY di depan 250 wisudawan universitas Bung Karno di Istana Negara tanggal 17 Desember. Yang menarik dari isi pidatonya itu ialah bahwa presiden SBY mengatakan bahwa memahami pikiran-pikiran Bung Karno merupakan keharusan, tetapi yang lebih penting adalah mengaktualisasikan pikiran-pikiran beliau, untuk menjawab tantangan masa depan.

Presiden SBY juga mengatakan bahwa "Bung Karno adalah pejuang besar dan bukan sekedar proklamator karena perjuangannya sudah lama dan mencapai puncaknya pada proklamasi ». Menurutnya, sekalipun Indonesia mempunyai kekayaan alam yang melimpah tapi yang lebih penting adalah membangun sumber daya manusia yang tidak hanya tinggi kemampuannya tapi juga mempunyai mental yang baik. Dan karenanya, ia betul-betul memohon kepada Universitas Bung Karno untuk ikut membangun 'national and character building’. (lihat berita selengkapnya pada akhir tulisan ini)

## Diucapkan Seorang Mantan Jenderal

Sayang sekali bahwa berita yang disiarkan oleh Media Indonesia Online itu terlalu singkat atau secara pokok-pokok saja. Padahal, pidato itu merupakan peristiwa yang penting, kalau dilihat dari berbagai segi atau sudut pandang. Oleh karena itu, adalah penting sekali kalau ada usaha - dari fihak mana pun juga - untuk mendapatkan teks yang lebih lengkap dan akurat tentang pidato presiden SBY tentang Bung Karno itu. Sebab pidato penting presiden SBY ini rupanya tidak disiarkan oleh sebagian besar pers nasional.

Sedangkan, isi yang amat positif tentang Bung Karno seperti yang disajikan oleh berita Media Indonesia Online itu, jarang sekali (kalau tidak dikatakan « tidak pernah » sama sekali) dinyatakan oleh para presiden yang terdahulu, termasuk Habibi, Abdurrahman Wahid, dan bahkan oleh Megawati sekali pun. Yang lebih menarik, ialah bahwa penilaian yang sangat positif tentang Bung Karno itu (kalau penyajian berita itu benar-benar akurat) diucapkan oleh seorang mantan jenderal TNI-AD. Sebab, sejarah lahirnya Orde Baru oleh TNI-AD yang bersekutu dengan Golkar – dan mendapat dukungan dari blok Barat yang didalangi imperialis Amerika - adalah sejarah pembangkangan dan pengkhianatan para petinggi militer (terutama dari kalangan TNI-AD) terhadap Bung Karno. Seperti kita ketahui, pengkhianatan kalangan petinggi TNI-AD terhadap Bung Karno adalah pada dasarnya pengkhianatan terhadap kepentingan revolusi rakyat Indonesia.

## Isi Pidato Yang Luar Biasa

Mengingat sikap terhadap Bung Karno yang selama ini dianut sebagian besar golongan militer (terutama TNI-AD), maka kelihatan sekali bahwa isi pidato presiden SBY ini termasuk « luar biasa ». Ia menegaskan bahwa « memahami fikiran-fikiran Bung Karno adalah keharusan », ketika selama lebih dari 32 tahun (sampai sekarang !!!) ajaran atau fikiran Bung Karno dimusuhi, disingkirkan, dikerdilkan, atau digelapkan oleh Orde Baru beserta para pendukungnya, terutama oleh TNI-AD. Presiden SBY juga menganjurkan supaya fikiran-fikiran Bung Karno itu diaktualisasikan dengan kondisi sekarang untuk menjawab tantangan masa depan. Anjurannya ini bisa diartikan bahwa menurutnya ajaran-ajaran atau fikiran-fikiran Bung Karno itu tetap mempunyai nilai berharga yang dapat dipakai atau masih relevan.

Esensi fikiran-fikiran atau ajaran-ajaran Bung Karno adalah pada pokoknya sikap anti-imperialis, gandrung kepada persatuan nasional, perjuangan untuk masyarat adil makmur, yang dirumuskannya dalam berbagai pidato dan tulisan, yang dapat dibaca dalam buku « Di Bawah Bendera Revolusi ».

Kalau presiden SBY betul-betul menganjurkan kepada semua orang supaya fikiran-fikiran Bung Karno « difahami dan diaktualisasikan guna menghadapi tantangan masa depan » maka seruan ini bisa merupakan peristiwa penting yang bisa membawa dampak besar sekali dalam masyarakat. Kita semua merasakan bahwa sejak lahirnya Orde Baru, terasa sekali bahwa kehidupan moral (dan politik) bangsa Indonesia sudah kehilangan pedoman. Karena, Bung Karno yang sejak tahun 1920-an sudah menjadi pedoman atau obor perjuangan bangsa telah dikhianati, dan « dibunuh » secara politik dan fisik oleh pimpinan TNI-AD yang bersekongkol dengan kekuatan imperialis, terutama Amerika.

## Reaksi Terhadap Pidato Presiden SBY

Dengan mengatakan bahwa Bung Karno « adalah pejuang besar dan bukan sekedar proklamator karena perjuangannya sudah lama » rupanya presiden SBY bermaksud untuk mengingatkan mereka yang anti-Bung Karno tentang betapa besar jasa Bung Karno bagi bangsa. Dapat diduga bahwa berbagai ucapannya dalam pidatonya di depan wisudawan Universitas Bung Karno tidak saja mengejutkan banyak orang, tetapi juga membikin tidak senangnya sebagian pimpinan TNI-AD, dan juga tokoh-tokoh pendukung Orde Baru dalam partai Golkar (dan kalangan-kalangan lainnya yang pro-Orde Baru).

Sekarang masih sulit diramalkan apakah sikap atau pandangan presiden SBY yang begitu positif tentang Bung Karno itu akan dipertahankannya di kemudian hari ataukah akan dirobahnya. Sebab, pastilah berbagai kalangan TNI-AD yang tetap anti-Bung Karno akan bereaksi dengan macam-macam kegiatan, bersama-sama dengan kekuatan dari berbagai kalangan lainnya yang pro-Orde Baru.

Yang perlu dicatat adalah bahwa pidato presiden SBY itu diucapkan beberapa hari sebelum Wakil Presiden Jusuf Kalla dipilih (oleh Munas Golkar di Bali) menjadi ketua umum partai Golkar. Mengingat sikap tokoh-tokoh Golkar yang anti-Bung Karno sejak beliau masih menjadi presiden, maka wajar pulalah bahwa Jusuf Kalla pun mempunyai sikap yang juga tidak positif tentang Bung Karno. Apakah presiden SBY akan mengucapkan pidato yang begitu positif tentang Bung Karno seandainya Jusuf Kalla sudah menjadi Ketua Umum partai Golkar ? Ini adalah suatu tanda-tanya yang besar.

## Cacad Terus Bagi TNI-AD Dan Golkar

Sejarah Orde Baru sudah menunjukkan dengan gamblang sekali bahwa Golkar dan TNI-AD adalah satu dan senyawa. Rezim militer Suharto dkk yang telah mengangkangi Indonesia dan melakukan berbagai kejahatan - termasuk kejahatan terhadap Bung Karno - telah mendapat dukungan yang mutlak dari Golkar. Karena itu Golkar wajib ikut bertanggungjawab atas berbagai kejahatan yang pernah dilakukan oleh Orde Baru itu.

Selama para petinggi militer (terutama TNI-AD) dan Golkar tidak mau terang-terangan dan jelas-jelas mengakui kesalahan mereka atau dosa mereka terhadap Bung Karno dan para pendukungnya yang paling setia (termasuk anggota dan simpatisan PKI yang dibunuhi dan dipenjarakan secara sewenang-wenang), maka cacad ini akan tetap terus disandang oleh TNI-AD dan Golkar.

Apakah pidato presiden SBY yang positif mengenai Bung Karno itu bisa dilihat sebagai usaha untuk memperbaiki citra TNI-AD, atau dalam rangka usaha untuk memperbaiki kesalahan-kesalahan masa lalu, ataukah hanya sebagai lamis bibir saja, ini akan bisa sama-sama kita lihat di kemudian hari.

---

Lampiran : Berikut di bawah ini disajikan selengkapnya berita pendek tentang pidato presiden SBY yang disiarkan Media Indonesia Online tanggal 17 Desember 2004

Presiden: Aktualisasikan Pikiran-Pikiran Bung Karno

JAKARTA—MIOL: Presiden Susilo Bambang Yudhoyono mengajak seluruh lapisan masyarakat untuk mengaktualisasikan pikiran-pikiran Bung Karno guna menjawab tantangan masa depan.

"Memahami pikiran-pikiran beliau merupakan keharusan, tetapi yang lebih penting adalah mengaktualisasikan pikiran-pikiran beliau," kata Presiden di Istana Negara, Jakarta, Jumat, ketika menerima 250 wisudawan Universitas Bung Karno.

Yudhoyono yang berpidato tanpa teks mengatakan Bung Karno bukan hanya merupakan proklamator tapi juga adalah pejuang besar di tanah air.

"Bung Karno adalah pejuang besar dan bukan sekedar proklamataor karena perjuangannya sudah lama dan mencapaui puncaknya pada proklamasi," katanya yang didampingi Mendiknas Bambang Sudibyo serta Sekretaris Kabinet Sudi Silalahi.

Yudhoyono mengatakan pula, sekalipun Indonesia mempunyai kekayaan alam yang melimpah tapi yang lebih penting adalah membangun sumber daya manusia yang tidak hanya tinggi kemampuannya tapi juga mempunyai mental yang baik.

"Saya betul-betul memohon kepada Universitas Bung Karno untuk ikut membangun 'national and character building," katanya. (Ant/O-2)
