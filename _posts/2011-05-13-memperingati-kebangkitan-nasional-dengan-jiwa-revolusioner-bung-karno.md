---
layout: post
title: Memperingati Kebangkitan Nasional Dengan Jiwa Revolusioner Bung Karno
date: 2011-05-03
---

Sebentar lagi bangsa kita akan merayakan Hari Kebangkitan Nasional 20 Mei. Peringatan iini untuk mengenang jasa para intelektual dan pemuda, yang ikut didorong Dr Sutomo mendirikan organisasi Boedi Oetomo pada tanggal 8 Mei 1908. Organisasi inilah yang merupakan cikal bakal lahirnya, di kemudian harinya,  berbagai organisasi pemuda, yang mencerminkan mulainya kebangkitan  bangsa, seperti  (antara lain) Jong Java, Jong Sumatra,  Jong Celebes, Jong Ambon.



Lahirnya Boedi Oetomo telah menumbuhkan inspirasi -- langsung dan tidak langsung  -- bagi banyak kalangan untuk memperkuat gerakan melawan penjajahan Belanda, antara lain dengan lahirnya  Sarekat Dagang Islam (tahun 1911) yang kemudian menjadi Sarekat Islam (1913).  Jadi, meskipun Boedi Oetomo bukan organisasi politik, dan lebih bersifat sosial, ekonomi dan kebudayaan, namun telah menggugah kesedaran politik berbagai kalangan waktu itu.



Ini kelihatan dari lahirnya Partai Komuns Indonesia (PKI) pada tanggal 23 Mei 1920, setahun lebih dahulu dari pada Partai Komunis Tiongkok. Pembrontakan PKI dalam tahun 1926 melawan kolonialisme Belanda adalah lanjutan atau pengembangan jiwa kebangkitan bangsa yang telah dicetuskan oleh Boedi Oetomo ;



Kebangkitan bangsa yang dirintis oleh Boedi Oetomo ini lebih menonjol lagi dengan berlangsungnya Kongres Pemuda dalam tahun 1928 yang melahirkan Sumpah Pemuda (Satu bangsa, satu tanahair, satu bahasa) yang menjadi kebanggaan sebagai pemersatu bangsa Indonesia, sampai sekarang ini. Entah berapa puluh ribu jiwa yang telah dikorbankan untuk itu semua, sehinggga bangsa kita bisa merdeka.



Keadaan negara yang serba rusak dan membusuk



Oleh karena itu,  memperingati Hari Kebangkitan Nasional adalah tugas atau kewajiban penting bagi bangsa kita. Ini dalam rangka pendidikan bangsa, untuk melaksanakan nation building and character building yang selalu digandrungkan Bung Karno. Sebab, kita semua harus mengakui terus terang terang,dan jujur ( !!!) , bahwa bangsa dan negara kita  -- sudah sejak era Orde Baru  – dalam keadaan rusak, membusuk, ambur adul, brengsek, kacau, kehilangan arah, kehilangan kepemimpinan nasional, kehilangan harga diri.



Melihat segala hal yang negatif tentang negara dan bangsa kita dewasa ini kita bisa bertanya-tanya apakah negara dan bangsa semacam yang kita saksikan dewasa inikah yang dulu dicita-citakan bangsa sejak lahirnya Boedi Oetomo, Sarekat Islam, Partai Komunis Indonesia, Partai Nasional Indonesia, Sumpah Pemuda, lahirnya Pancasila dan Bhinneka Tunggal Ika dan revolusi 17 Agustus 45 ?



Kerusakan moral di kalangan pejabat-pejabat tinggi (eksekutif, legislatif, dan judikatif) dan kebejatan rohani di kalangan para tokoh dan pemuka di berbagai bidang (organisasi kemasyarakatan dan agama) sudah  sedemikian luasnya dan parahnya, sehingga sulit diperkirakan bisakah dan kapankah  dan bagaimanakah kerusakan  yang begitu hebat ini diperbaiki ???



Barangkali sulit kita temukan di Indonesia sekarang ini adanya pakar-pakar segala ilmu politik, sosial, ekonomi dan kebudayaan (termasuk agama) atau « tokoh-tokoh » yang bisa menjawab pertanyaan yang begitu besar dan penting ini. Yang jelas adalah bahwa kerusakan atau pembusukan sudah begitu meluas dan begitu besar, sehingga tidak bisa diatasi atau diperbaiki dalam tempo 5 atau 10 atau 15 tahun, dengan cara-cara biasa dan oleh orang-orang sejenis yang memegang kekuasaan dan pengaruh seperti sekarang ini.



Perjalanan bangsa yang panjang dan berliku-liku



Dengan memperingati  Hari Kebangkitan Nasional ini kita melihat betapa panjangnya dan sangat berliku-likunya perjalanan bangsa Indonesia untuk mencapai kemerdekaan nasional. Begitu banyak pengorbanan yang telah diberikan oleh banyak pejuang dari berbagai golongan, suku, agama, sejak didirikannya Boedi Oetomo, sampai proklamasi 17 Agustus 45.



Peringatan Kebangkitan Nasional kali ini adalah kesempatan yang baik sekali untuk merenungkan itu semua, dan mencoba memandang masa depan bangsa dan negara kita, di tengah-tengah situasi dunia yang mengalami banyak pergolakan dan menyongsong adanya perubahan, baik di negara-negara Arab atau  di Timur Tengah, Eropa, Asia, Afrika dan Amerika Latin.



Kita semua melihat bahwa Tiongkok yang pernah dimusuhi oleh rejim militer Suharto puluhan tahun sekarang menjadi kekuatan polilik, ekonomi dan militer yang tidak bisa diremehkan di dunia. India yang pernah juga dianggap negara yang miskin dan terbelakang sekarang sudah bangkit juga. Amerika Latin yang dalam jangka lama  menjadi daerah « setengah jajahan » AS sekarang menjadi kekuatan yang tidak bisa dipermainkan lagi oleh AS (ingat kasus Kuba, Venezuela, Bolivia, Brasilia, , Nicaragua).



Ketika di Indonesia ada kalangan-kalangan yang ingin merobah NKRI dan menggantikannya dengan Negara Islam Indonesia yang  menjalankan Syariah Islam, maka jelaslah cita-cita semacam itu bertentangan sama sekali atau merupakan pengkhianatan terhadap isi dan tujuan perjuangan bangsa, yang terdiri dari macam-macam suku, golongan, agama, yang dimulai sejak berdirinya Boedi Oetomo, dan dipimpin oleh Bung Karno sampai wafatnya.



Bung Karno adalah lambang raksasa kebangkitan nasional



Bung Karno adalah cermin yang gamblang sekali dari jiwa atau isi seluruh perjuangan bangsa sejak Boedi Oetomo. Jiwa Boedi Utomo terdapat dalam kalbu Bung Karno , seperti halnya jiwa Sarekat Islam, Partai Komunis Indonesia, Partai Nasional Indonesia. Bung Karno adalah lambang kebangkitan bangsa yang tidak bisa dibantah. Bung Karno dan kebangkitan nasional adalah satu Karenanya, memperingati Hari Kebangkitan Nasional tetapi tanpa mengangkat peran besar dan kedudukan penting Bung Karno bagi bangsa, adalah salah secara politik dan nista secara moral serta khianat secara historis !!!



Dalam memperingati Hari Kebangkitan Nasional adalah penting untuk mengingat kembali dan menjiwai segala ajaran-ajaran revolusioner Bung Karno. Sebab ajaran-ajaran revolusioner Bung Karno adalah kelanjutan dan pelaksanaan isi atau jiwa kebangkitan nasional. Dengan memegang teguh ajaran-ajaran revolusisoner Bung Karno berarti juga bahwa kita semua menolak segala isi dan tujuan rejim militer Suharto dengan Orde Barunya (dan Golkarnya !).



Sebab, sekarang makin banyak bukti yang menunjukkan dengan jelas bahwa isi, tujuan, dan praktek rejim militer Suharto adalah betul-betul bertentangan atau pengkhianatan terhadap jiwa dan tujuan Kebangkitan Nasional, terhadap jiwa revolusi 17 Agustus, terhadap Pancasila dan Bhinneka Tunggal Ika. Dan pengkhianatan yang paling besar adalah sikap Suharto dkk terhadap Bung Karno, serta dosa yang paling berat adalah dihancurkannya kekuatan pendukung politik Bung Karno, terutama golongan kiri yang wakil utamanya adalah PKI.



Hari Kebangkitan Nasional dan ajaran-ajaran revolusioner Bung Karno



 Peringatan Hari Kebangkitan Nasional akan menjadi ritual yang hampa atau mati yang hanya berisi pidato-pidato yang kosong artinya atau rethorika yang serba munafik dan upacara hura-hura yang penuh kepalsuan, kalau tidak  diisi dengan penanaman ajaran-ajaran revolusioner dalam rangka nation building and character building dan pemupukan semangat perjuangan untuk kepentingan rakyat banyak.



Di antara ajaran-ajaran revolusioner itu adalah berbagai gagasan besar  Bung Karno untuk terus-menerus menjebol dan membangun dalam rangka meneruskan revolusi yang belum selesai menuju tercapainya masyarakat adil dan makmur atau masyarakat sosialis à la Indonesia. Ajaran-ajaran revolusioner Bung Karno ini dapat ditemukan dalam buku Dibawah Bendera Revolusi atau Revolusi Belum Selesai, atau dalam berbagai buku yang banyak beredar sekarang ini.



Berlainan dengan masa era Orde Baru ketika ajaran-ajaran revolusioner Bung Karno dilarang beredar dan  sulit didapat oleh umum, maka sekarang berbagai kemungkinan sudah terbuka untuk mendapatkannya.



Ajaran Bung Karno : mengobarkan revolusi terus-menerus

Dengan makin rusaknya secara parah sekali berbagai bidang kehidupan  bangsa dan negara seperti yang kita saksikan dewasa ini, maka kelihatan sekali bahwa arah jalan yang sudah ditunjukkan Bung Karno dengan berbagai ajaran-ajaran revolusionernya itu adalah satu-satunya arah yang benar. Dengan ajaran-ajaran revolusioner Bung Karno untuk terus-menerus mengobarkan revolusi, maka kebangkitan nasional dapat digerakkan bersama-sama untuk mengentaskan negara dan bangsa dari keterpurukan dan pembusukan sekarang ini.



Sebab, kerusakan moral dan kebejatan akhlak yang melanda tanahair kita sekarang ini sudah tidak mungkin lagi (sekali lagi : tidak mungkin !) diperbaiki dengan segala macam apa yang dinamakan « reformasi », yang hanya berupa tambal sulam dan palsu pula. Penyakit yang sudah begitu parah dan begitu luas  itu hanya dapat diberantas dengan cara-cara revolusioner dan oleh orang-orang atau golongan revolusioner dengan dipimpin oleh tokoh revolusioner pula. Singkatnya : hanya dengan dengan revolusi.



Hebatnya kerusakan dan pembusukan negara dan  bangsa kita nyata sekali dari banyaknya korupsi yang melanda dengan ganasnya di berbagai bidang kehidupan. Beragam kasus menunjukkan dengan jelas keadaan yang menyedihkan dan sekaligus memalukan bangsa, antara lain : kasus-kasus Bank Bali, Bank Century, kasus Gayus Tambunan, rekening gendut pimpinan POLRI, banyaknya jaksa dan hakim yang dihukum, daftar gubernur, bupati dan walikota yang korup, persoalan gedung baru DPR, « studi banding » anggota-anggota DPR ke luarnegeri, masalah pembelian pesawat udara oleh Merpati, korupsi di sekitar SEA Games di Palembang, dan seabreg-abreg masalah besar lainnya.



Apalagi, keadaan yang sudah begitu busuk dan rusak itu diperburuk lagi dengan ancaman bahaya dari bermacam-macam kalangan Islam radikal yang ingin menggantikan NKRI beserta Pancasilanya dan Bhinneka Tunggal Ikanya  dengan Negara Islam Indonesia dan Syariah Islam.



Padahal,  NKRI dengan Pancasila dan Bhinneka Tunggal Ika-nya,  sudah terbukti sejak lama merupakan bentuk dan isi yang paling cocok untuk Indonesia, yang terdiri dari 17.000 pulau dan ratusan suku bangsa dan bahasa-daerah, serta berbagai agama, kepercayaan, dan adat-istiadat ini.



NKRI dengan Pancasila dan Bhinneka Tunggal Ika adalah satu-satunya wadah persatuan bangsa Indonesia, sedangkan kita sudah melihat bahwa usaha untuk mendirikan NII sudah menimbulkan perpecahan dan permusuhan di berbagai kalangan bangsa.



Dalam memperingati Hari Kebangkitan Nasional tanggal 20 Mei, itu semua perlu kita renungkan dalam-dalam, dengan berpedoman kepada ajaran-ajaran revolusioner Bung Karno sebagai penunjuk jalan untuk meneruskan revolusi. Jalan lain tidak ada bagi bangsa kita.
