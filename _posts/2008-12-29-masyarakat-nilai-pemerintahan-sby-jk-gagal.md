---
layout: post
title: Masyarakat nilai pemerintahan SBY-JK gagal
date: 2008-12-29
---

Menjelang akhir tahun 2008 dan datangnya tahun baru 2009, banyak kalangan menyatakan pendapat mereka tentang sitiuasi negara dan bangsa kita dalam tahun yang sudah dilewati bersama, dan harapan atau perkiraan tentang tahun 2009.

Dalam kaitan ini, adalah menarik untuk sama-sama kita perhatikan dua berita yang disajikan berikut ini. Berita yang pertama adalah dari Kompas tanggal 27 Desember 2008 yang berjudul “Masyarakat nilai pemerintahan SBY-JK gagal”, dan berita yang kedua dari Detik News (24 Des 2008) yang menyebutkan “Rapor merah SBY-JK, saatnya ganti rezim” dan “4 tahun kepemimpinan SBY-JK Indeks kesengsaraan meningkat".

Isi dua berita tersebut selengkapnya adalah sebagai berikut :



Masyarakat nilai pemerintahan SBY-JK gagal

Kompas 27/12/08 menulis sebagai berikut : “Pemerintahan Presiden Susilo Bambang Yudhoyono (SBY) dan Wakil Presiden Jusuf Kalla (JK) dinilai gagal dalam menjalankan kinerja selama 4 tahun memegang tampuk pimpinan. Berdasarkan hasil survei Lembaga Riset Indonesia (LRI) sebanyak 60,3 persen masyarakat menyatakan tidak puas atas kinerja pemerintahan SBY.

"SBY dinilai gagal di mata masyarakat selama pemerintahannya," kata Direktur Eksekutif LRI Johan O Silalahi saat jumpa pers Evaluasi Akhir Tahun SBY-JK di hotel The Sultan, Jakarta, Sabtu (27/12).

Johan mengatakan, pemerintahan SBY dinilai gagal terutama dilihat dari sisi ekonomi, seperti dalam hal mengatasi kemiskinan dan pengangguran. Menurutnya, proyeksi angka, baik kemiskinan maupun pengangguran yang setiap tahun dipaparkan oleh SBY tidak sesuai dengan realita.

Hal senada juga disampaikan Pengamat Politik Universitas Indonesia Boni Hargend. Boni menuturkan, tingkat pengangguran semakin meningkat seiring dengan maraknya Pemutusan Hubungan Kerja (PHK) yang terjadi hingga 400.000 orang.

Selain itu, Boni menilai 10 langkah untuk mengatasi krisis yang dikeluarkan oleh SBY juga tidak memberi dampak yang signifikan. "10 langkah untuk antisipasi krisis itu tidak ada bedanya dengan khotbah," ujarnya.

Dalam hasil poling tersebut, juga terlihat bahwa masyarakat menilai sosok pemimpin yang ideal, pertama dilihat dari sikap yang peduli rakyat sebanyak 31,5 persen, jujur sebanyak 27,7 persen, bijaksana sebanyak 11,9 persen, tegas sebanyak 7,4 persen, dan dapat dipercaya sebanyak 6,7 persen.

Adapun dari 5 capres 2009 yang menjadi pilihan adalah sosok yang jujur sebanyak 84 persen, tegas 71 persen, dan dapat dipercaya 62 persen.

Poling diadakan di 33 provinsi dengan 2.400 responden yang disebar melalui kuisioner. Poling diadakan mulai tanggal 25 Agustus 2008 sampai 7 Desember 2008 lalu.


Rapor merah SBY-JK, saatnya ganti rezim

Sedangkan menurut Detik News 24 Desember 2008, “kebijakan ekonomi SBY-JK selama empat tahun dinilai menambah indeks kesengsaraan rakyat. Rapor merah untuk SBY-JK.

"Indeks kesengsaraan tahun 2008 mencapai angka 20,3 persen. Padahal, di awal kepemimpinan SBY-JK tahun 2004 angkanya cuma 16,5 persen," ungkap pengamat ekonomi Iman Sugema dalam diskusi 'Rapor Merah SBY-JK, saatnya ganti rezim?' di Gedung Intiland Tower, Jalan Jend Sudirman, Jakarta, Rabu (24/12/2008).

Iman menambahkan, angka indeks kemiskinan tersebut tidak jauh berbeda dengan angka yang dirilis lembaga-lembaga survei. Indikator riil kesengsaraan yang makin meningkat, lanjutnya, adalah harga-harga yang kian melambung, dan tingkat pengangguran meningkat.

"Sama seperti lembaga survei, banyak orang mengaku tidak puas soal kinerja SBY-JK," tambah Iman lagi.Selain soal indeks kemiskinan, Iman juga menyoroti kebijakan-kebijakan ekonomi yang dihasilkan tim ekonomi SBY-JK. Menurutnya, kebijakan yang dihasilkan belum pro rakyat kecil.

"Kebijakannya anti rakyat dan anti domestik. Bayangkan, LNG dijual murah ke asing, tapi rakyat dipaksa membeli BBM harga mahal dan elpiji yang notabene sebagiannya ekspor," tukas Iman. (kutipan selesai)

* * *

Kedua berita tersebut di atas menggambarkan bahwa sebagian terbesar rakyat Indonesia sekarang ini merasa tidak puas atas hasil kerja pemerintahan SBY-JK terutama karena buruknya situasi ekonomi dan banyaknya pengangguran serta meningkatnya kemiskinan atau kesengsaraan. Kalau kita perhatikan kehidupan sehari-hari rakyat kita, baik di kota-kota maupun di desa-desa dewasa ini, maka kita bisa mengatakan bahwa hasil pengamatan dari lembaga survey seperti tersebut di atas, pada garis besarnya, adalah berdasarkan kenyataan,

Padahal, SBY sebagai tokoh pensiunan militer (yang mendapat dukungan dari sebagian dari kalangan militer) sebelum menjadi presiden sudah meraih kepercayaan ( atau, ilusi ?) yang besar dari banyak kalangan, bahwa ia akan bisa mengadakan perubahan-perusahan besar terhadap situasi negara dan bangsa yang mengalami berbagai masalah yang parah.

Tetapi, koalisi yang terutama didominasi oleh Partai Demokrat dan Golkar, selama 4 tahun sudah memperlihatkan keterbatasan kapasitas dan berbagai kelemahan politiknya, sehingga negara dan bangsa menghadapi keterpurukan di berbagai bidang seperti sekarang ini.

Karena pemilu akan diselenggarakan tidak lama lagi, maka timbul pertanyaan apakah pemerintahan hasil pemilu 2009 akan bisa menyelesaikan berbagai keterbengkalaian dan persoalan besar yang dibikin pemerintah SBY-JK ?

Partai Golkar yang diwakili Jusuf Kalla (ketua umum Golkar) sebagai Wakil Presiden untuk mendampingi presiden SBY, sudah mengeluarkan segala macam daya, usaha, dana, taktik, dan 1001 cara lainnya, untuk menunjukkan bahwa mereka bisa menyelenggarakan pemerintahan dengan baik, untuk meningkatkan kesejahteraan rakyat.

Menggantikan pemerintahan dengan yang bagaimana ?

Namun, ternyata bahwa persekutuan Partai Demokrat dan Partai Golkar selama 4 tahun hanya menghasilkan berbagai kegagalan, dan bahkan ada kalangan-kalangan yang menganggap bahwa sudah saatnya sekarang mengganti rezim (pemerintahan).

Berbagai indikasi menunjukkan bahwa persekutuan SBY-JK tidak akan bisa dipertahankan, apa pun hasil pemilu 2009 nanti. Usaha SBY untuk terpilih kembali dan tetap terus menduduki jabatan presiden akan menemui banyak sekali halangan atau kesulitan. Dukungan dari berbagai kalangan terhadap SBY sudah merosot sekali, kalau dibandingkan sebelum pemilu 2004. Namun, ia tetap masih mempunyai dukungan yang cukup, sehingga merupakan saingan berat bagi Megawati.

Banyak perkembangan akhir-akhir ini menunjukkan bahwa partai Golkar yang dalam pemilu 2004 mendapat suara cukup besar ( 21,6 % ) juga akan mengalami kemerosotan yang tidak sedikit. Makin besarnya kemerosotan Partai Golkar ini menunjukkkan perkembangan yang penting sekali dalam kehidupan politik atau keparpolan di Indonesia.Karena, sejarah Indonesia sudah membuktikan dengan jelas bahwa Golkar adalah salah satu unsur perusak berbagai kehidupan bangsa dan negara.

Bahwa banyak kalangan yang menginginkan adanya penggantian rezim adalah sesuatu yang wajar, baik, logis dan berdasarkan nalar yang sehat, demi kepentingan rakyat banyak. Namun, rezim yang bagaimanakah yang mesti menggantikan rezim seperti yang sekarang ini?

Diperlukan : pemerintahan yang pro-rakyat kecil
Dari pengamatan selama 32 tahun rejim militer Orde Baru ditambah lebih dari 10 tahun pemerintahan pasca-Suharto dapatlah kiranya ditarik kesimpulan bahwa negara kita memerlukan adanya rezim (atau pemerintahan) tipe baru, rezim yang pro-rakyat kecil, rezim yang anti-imperialis atau anti-neoliberalisme, rezim yang betul-betul menjalankan Pancasila dan menjunjung tinggi-tinggi Bhinneka Tunggal Ika.

Selama lebih dari 40 tahun (sejak rezim militer Suharto yang diteruskan oleh pemerintahan-pemerintahan lainnya) Indonesia telah menjalankan berbagai politik ekonomi dan keuangan yang pro-ekonomi liberal, yang dianjurkan oleh IMF, Bank Dunia, dengan bantuan atau persekongkolan ekonom-ekonom Indonesia (terutama dengan Berkely Maffia). Keadaan negara dan bangsa kita yang serba parah sekarang ini adalah akibat dari banyak kesalahan dan berbagai kejahatan oleh Orde Baru , yang dibikin lebih parah lagi atau diteruskan oleh berbagai pemerintahan yang menggantikannya.

Karenanya, perubahan yang mendasar hanya bisa dilakukan dengan menggantikan rezim, yang berlainan sama sekali dengan rejim militer Suharto atau dengan pemerintahan-pemerintahan pasca-Orde Baru. Sebab, sudah lebih dari 40 tahun berbagai politik anti-Sukarno (di bidang politik, ekonomi, sosial, kebudayaan, dan juga agama) telah menimbulkan kerusakan-kerusakan dan pembusukan di kalangan bangsa yang akibatnya dapat sama-sama kita saksikan dewasa ini.

Munculnya Wiranto, Prabowo, Sutiyoso dll
Adanya banyak mantan jenderal-jenderal TNI yang muncul sebagai calon presiden seperti (antara lain) Wiranto,Prabowo, Sutiyoso (di samping SBY) yang dengan segala macam cara, dan segala jalan, sedang berusaha untuk merebut kedudukan presiden/kepala negara, makin memperkuat keyakinan berbagai golongan untuk mencegah terkabulnya keinginan (atau ilusi?) mereka dalam merebut pimpinan negara kali ini.

Sebab, sudah cukup banyak bukti selama 32 tahun rezim militer Suharto betapa besar kerusakan-kerusakan yang telah dilakukan oleh pimpinan militer (terutama TNI AD, tetapi tidak semuanya) terhadap bangsa dan negara, terhadap Bung Karno, terhadap Pancasila, terhadap Bhinneka Tunggal Ika, terhadap revolusi bangsa Indonesia di bawah pimpinan Bung Karno.

Wiranto, Prabowo, Sutiyoso ( atau, bahkan SBY sekalipun,) serta para pensiunan jenderal-jenderal lainnya pada umumnya adalah orang-orang yang telah mengenyam kedudukan penting dengan fasilitas-fasilitas yang luar biasa, sehingga merupakan pendukung setia Suharto. Oleh karena itu, bolehlah dikatakan bahwa mereka itu pada umumnya adalah orang-orang yang anti Sukarno, yang berarti sekaligus juga anti ajaran-ajaran Bung Karno. Dan, untuk lebih jelasnya lagi, anti Bung Karno adalah pada dasarnya anti-Pancasila.

Patut diragukan : apakah mereka sudah berubah ?
Mungkin saja (sekali lagi mungkin, meskipun kemungkinan ini kecil sekali !) bahwa sikap politik mereka terhadap Suharto sekarang sudah berubah. Namun, apakah mereka sudah betul-betul berubah, adalah masih perlu diragukan sekali. Sebab, perubahan sikap mereka berarti mengakui bahwa rezim militer Orde Barunya Suharto telah melakukan kesalahan-kesalahan berat terhadap Bung Karno dan terhadap pendukungnya yang terdiri dari golongan kiri (terutama PKI), dan juga telah melakukan pelanggaran HAM secara besar-besaran dan dalam jangka lama sekali.

Kesalahan besar Orde Baru lainnya adalah dengan pengkhianatannya terhadap Bung Karno dan golongan kiri pada umumnya mereka telah membawa negara dan bangsa Indonesia selama puluhan tahun menjadi sekutu imperialisme AS, dan menjadikan Indonesia sebagai daerah jajahan baru bagi modal besar asing. Perkembangan di dunia menunjukkan bahwa imperialisme AS adalah tetap musuh dari berbagai rakyat di Asia, Amerika Latin, Timur Tengah, Afrika, Eropa, bahkan di Amerika Serikat sendiri (Pelemparan sepatu terhadap presiden Bush oleh seorang wartawan Irak adalah salah satu di antara banyak sekali manifestasi anti-Amerika di dunia)

Mengingat itu semuanya, sangatlah mutlak adaya pernyataan terbuka dan terus-terang (dan yang dilakukan dengan tulus !) dari orang-orang semacam Wiranto, Prabowo, Sutiyoso (dan juga SBY serta jenderal-jenderal lainnya ) bahwa mereka sekarang melihat berbagai kesalahan berat Suharto dengan Orde Barunya. Hanya sikap yang demikianlah merupakan ukuran dan tanda bagi banyak orang bahwa mereka sudah berubah. Dan pernyataan semacam yang demikian ini perlulah disertai dengan tindakan kongkrit atau praktek yang nyata.

Sikap tegas dan jelas tentang Suharto

Tanpa bersikap yang begitu tegas terhadap Suharto dan Orde Barunya, dan juga bersikap yang jelas terhadap ajaran-ajaran Bung Karno, maka banyak orang akan melihat bahwa ucapan-ucapan mereka mengenai Pancasila, Bhinneka Tunggal Ika, persatuan bangsa, pengabdian kepada rakyat, dan 1001 ucapan yang muluk-muluk lainnya, adalah hanya omong kosong atau penipuan saja.

Sekarang lebih jelas lagi bagi banyak orang bahwa negara dan bangsa Indonesia tidak akan bisa meraih perbaikan dan kemajuan yang besar bagi rakyat banyak, dengan terus mempertahankan pemerintahan yang tipenya seperti yang sama-sama kita saksikan selama 40 tahun sejak lahirnya Orde Baru. Untuk itu, rezim atau pemerintahan tipe baru perlu diciptakan bersama-sama. Dan pemerintahan tipe baru ini (yang berbeda sama sekali dengan pemerintahan Orde Baru) memerlukan orang-orang yang bukan pendukung politik Suharto.

Situasi nasional dan internasional sudah berubah

Sekarang ini situasi nasional dan juga internasional sudah berubah dari yang 40 atau 30 tahun yang lalu, dan akan terus berubah di masa yang akan datang. Perang dingin (dalam bentuknya yang lama ) sudah lewat. Imperialisme AS yang dulu pernah menjadi kekuatan yang maha dahsyat, sekarang sudah merosot dan loyo. Kapitalisme yang berwajah neo-liberalisme sekarang sedang sekarat. Dalam situasi yang demikian ini, nampak dengan jelas sekali bahwa politik Suharto dengan Orde Barunya adalah salah dan merusak negara dan bangsa.

Sebaliknya, perkembangan situasi di dalam negeri dan di dunia menunjukkan bahwa berbagai visi atau ajaran Bung Karno mengenai Trisakti (bebas aktif dalam politik, berdikari dalam bidang ekonomi, berkepribadian dalam kebudayaan), mengenai Pancasila dan Bhinneka Tunggal Ika, mengenai perjuangan terhadap imperialisme adalah pokoknya benar, dan karenanya masih relevan untuk masa sekarang.

Oleh karena itu, agaknya pemerintahan atau rezim yang menjalankan ajaran-ajaran Bung Karnolah yang bisa diharapkan mampu untuk mengangkat kembali harga diri bangsa, memperkokoh persatuan rakyat, dan menggelorakan perjuangan bersama untuk menciptakan masyarakat adil dan makmur.
