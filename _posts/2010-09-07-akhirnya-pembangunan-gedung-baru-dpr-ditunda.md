---
layout: post
title: Akhirnya, pembangunan gedung baru DPR ditunda !
date: 2010-09-07
---

Karena mendapat reaksi negatif yang keras dari banyak kalangan yang berupa kritik, ungkapan kemarahan, cemooh dan hujatan, maka rencana pembangunan gedung baru DPR akhirnya ditunda, dan akan dikaji lebih mendalam lagi oleh tim teknis yang dibentuk oleh pimpinan DPR.



Pernyataan tentang penundaan oleh Ketua DPR, Marzuki Alie  ini disambut oleh berbagai kalangan dengan bermacam-macam sikap. Masih belum jelas sampai kapan akan ditunda, dan apakah rencana pembangunan gedung baru yang megah dan mewah (dan yang akan membutuhkan pembeayaan sebesar Rp 1, 8 triliun ini)  akan dirubah, dan menjadi berapa akhirnya.



Bagaimanapun juga, masalah pembangunan gedung baru DPR ini harus tetap diikuti dengan teliti dan kewaspadaan oleh semua kalangan yang tidak menginginkan bahwa lembaga legislatif yang tertinggi negara kita ini dikotori oleh berbagai oknum yang moralnya rusak dan sikap politiknya juga busuk terhadap kepentingan rakyat dan pembayar pajak.



Sebab, masih perlu tetap dicurigai apakah di belakang « penundaan » ini tidak ada siasat untuk menipu opini umum, dan hanya cara untuk memadamkan kemarahan opini publik, yang  ternyata kemudian sebetulnya tetap meneruskan pembangunan gedung baru itu.



Jelaslah bahwa projek pembangunan gedung baru DPR ini  -- seperti halnya projek-projek besar lainnya  -- adalah makanan empuk atau kesempatan emas bagi berbagai macam maling berdasi, dan para pencoleng tingkat tinggi, untuk mencari keuntungan yang haram, dengan macam-macam cara.



Kerusakan atau kebobrokan di kalangan DPR kita merupakan bukti yang makin jelas dan makin banyak bahwa moral di kalangan elite bangsa kita dewasa ini sudah kelewatan parahnya. Kebusukan moral separah dewasa ini tidak pernah terjadi sebelum pemerintahan Orde Baru.



Banyak di antara kita yang telah menyaksikan bahwa moral bangsa Indonesia di bawah pimpinan Bung Karno kelihatan penuh dengan ciri-cri revolusioner, patriotisme yang berkobar-kobar, dan semangat perjoangan yang tinggi untuk mengabdi kepada kepentingan rakyat banyak, terutama rakyat kecil.



Untuk memberikan gambaran agak lebih jelas mengenai masalah gedung baru DPR itu, di bawah berikut ini disajikan kumpulan berita mengenai persoalan-persoalan itu.



Paris, 7 September 2010



A. Umar Said



* * *

 DPR Tunda Pembangunan Gedung Baru



Senin, 6 September 2010



Jakarta (ANTARA News) - DPR RI akhirnya menunda pembangunan gedung baru berlantai 36 yang diperkirakan menelan biaya Rp1,8 triliun setelah publik menolak rencana itu.



Ketua DPR RI Marzuki Alie kepada pers usai rapat pimpinan di Gedung DPR/MPR Jakarta Senin mengemukakan bahwa pembangunan gadung itu masih akan dikaji lebih mendalam. Kajian akan dilakukan oleh tim teknis yang dibentuk pimpinan DPR.



Kajian ulang dilakukan untuk lebih memastikan mengenai desain dan biaya pembangunannya. "Ini menjadi citra buruk bagi DPR. karena itu, disepakati untuk ditunda dulu, dilakukan kajian ulang," kata Marzuki.



Selain besarnya biaya dan desain gedung yang dikaji ulang, kata Marzuki, tender yang sudah dibuka dihentikan sementara dan ditunda sampai ada kajian yang tuntas dilakukan tim teknis.



Menurut Marzuki, desain gedung baru harus menjauhkan kesan mewah agar tidak menimbulkan pro-kontra di masyarakat. Jika desainnya mewah, maka publik akan menolaknya.



Marzuki menyayangkan rencana pembangunan gedung baru itu yang sudah memasuki tahap sosialisasi dan tender "diwarnai" hal-hal yang sebenarnya di luar rencana dan desain gedung. Misalnya, ada informasi bahwa di gedung baru akan ada fasilitas kebugaran dan relaksasi, seperti spa. "Padahal hal tidak ada," katanya.



Pimpinan DPR-RI menggelar rapat khusus membahas kelanjutan rencana pembangunan gedung baru DPR yang menimbulkan kontroversi di tengah masyarakat.



Rapat juga mengundang para konsultan yang terlibat dalam rencana pembangunan gedung itu. Ketua DPR Marzuki Alie mengatakan, konsultan ditanya apakah benar ada rencana membangunan fasilitas-fasilitas khusus seperti tempat spa dan lain-lain.



"Jika memang tidak ada rencana itu, lalu siapa yang selama ini menyebarkan kabar bahwa di gedung baru itu akan ada fasilitas spa," kata Marzuki.



Mengenai kolam renang Marzuki menyatakan rencana itu memang ada. Ide teknis awalnya adalah membuat penampungan air untuk kepentingan pemadam kebakaran. Karena untuk memompa air dari bawah ke lantai teratas, yakni lantai 36 tidak memungkinkan, sehingga disarankan membuat bak penampungan air di lantai tengah gedung.



Rencana pembangunan gedung baru DPR RI yang diumumkan tepat pada HUT ke-65 DPR RI pada 30 Agustus menggemparkan masyarakat. Bukan saja karena berlantai 36, tetapi juga karena diumumkan tiba-tiba dan langsung diadakan tender serta biayanya yang ditaksir mencapai Rp1,8 triliun.



Namun Marzuki mengemukakan, pimpinan DPR berusaha agar dilakukan penekanan terhadap biaya sehingga biaya lebih sedikit dari perkiraan semula.



* * *
Penundaan Jangan Cuma Membodohi Rakyat


Rakyat Merdeka, 06 September 2010,

RMOL. Penundaaan pembangunan gedung baru DPR hanya untuk meredam amarah publik. Setelah emosi masyarakat normal, pembangunan akan dilanjutkan kembali.

“Ya, penundaan ini hanya untuk menunggu emosi masyarakat reda. Penundaannya juga hanya sementara, tidak tegas seberapa lama akan ditunda,” kata Direktur Eksekutif Lingkar Madani Indonesia (Lima) Ray Rangkuti, kepada Rakyat Merdeka Online, Senin malam (6/9).

Penundaan serupa, lanjut Ray, pernah dilakukan pada 2008. Saat itu, DPR terpaksa menunda keinginannya membangun gedung baru lantaran masyarakat menolak keras. Namun, setelah masyarakat mulai lupa, DPR kembali merancang gedung baru dengan bentuk dan fasilitas yang lebih wah.

Karena itu, Ray meminta agar Ketua DPR mempertegas seberapa lama rencana penundaan itu. Jangan sampai penyataan penundaan dan peninjauan kembali hanya untuk membodoh-bodohi rakyat agar tidak marah.

“Ke depan, rencana pembangunan ini juga jangan hanya diputuskan di BURT dan Setjen DPR. Tapi harus melalui mekanisme paripurna dan disetujui oleh semua pihak. Agar prosesnya transparan dan pembangunan benar-benar sesuai kebutuhan,” jelasnya.[ald]

*      * *     


DPR Lepas Tangan Soal Pembangunan Gedung Baru

Media Indonesia, 06 September 2010

JAKARTA--MI: DPR lepas tangan soal pembangunan gedung baru DPR. Pengkajian ulang yang dilakukan terhadap pembangunan gedung hanya akan dilakukan di tataran tim teknis pembangunan gedung baru DPR.

Ketua Badan Urusan Rumah Tangga (BURT) DPR Marzuki Ali menyatakan bahwa DPR tidak mengetahui fungsi teknis atas pembangunan gedung baru DPR. DPR hanya menjadi user dalam pembangunan gedung baru tersebut.

"Kami adalah orang politik. Tidak tahu fungsi teknis. Kami hanya menyatakan ingin tambahan gedung untuk memenuhi daya tampung. Yang tahu mendesign adalah tim teknis dan konsultan," ungkapnya ketika ditemui usai Rapat Pimpinan di DPR, Jakarta, Senin (6/9).

Ia menegaskan bahwa Sekretariat Jenderal (Setjen) DPR memiliki kewenangan atas pembangunan gedung baru DPR. Mereka merupakan pemegang anggaran. Di dalam Setjen juga terdapat perwakilan Kementerian Pekerjaan Umum (PU).

"Saya tunjukkan siapa yang sebenarnya bertanggungjawab, tim teknis dan konsultan pembangunan gedung baru DPR, bukan DPR-nya," tegasnya.

Marzuki mengaku DPR telah menjadi korban atas polemik pembangunan gedung baru DPR. Masyarakat memandang bahwa DPR yang berambisi untuk melakukan pembangunan gedung mewah. Padahal, DPR hanya menjadi user atas gedung yang disetujui oleh tim teknis dan konsultan.

Ia kaji ulang atas pembangunan gedung baru ini. Proses kaji ulang ini sepenuhnya dilakukan oleh tim teknis dan konsultas. "Kalau ada pertanyaan langsung saja ke mereka. Kami tidak mau tahu," tegasnya.

Namun kajian ini hanya hanya bersitfat terbatas. Kepala Biro Pemeliharaan Bangunan Instalasi Sekjen DPR Mardiyan Umar masih menyatakan belumd apat menjelaskan secara rinci kajian yang akan dilakukan. Pimpinan DPR dan BURT hanya mengamanatkan transparansi anggaran, efisiensi, dan penghematan.

"Nanti soal kajian ulangnya seperti apa, kami masih tunggu masukan," jelasnya.

Koordinator Forum Masyarakat Peduli Parlemen (Formappi) Sebastian Salang mengungkapkan bahwa DPR telah cuci tangan dari kritik masyarakat. Menurutnya DPR tidak mungkin tak mengetahui proses pembangunan gedung baru.

"Kewenangan BURT sudah jelas. Tidak mungkin mereka hanya menutup mata melihat daftar kebutuhan yang disodorkan oleh DPR. Mereka tahu dan mereka melihat bagaimana prosesnya berjalan," tegasnya.

Menurutnya, sikap pimpinan DPR ini menunjukkan bahwa DPR sendiri memiliki andil dalam pembangunan gedung baru DPR. Mereka tidak memiliki keinginan sepenuhnya untuk melakukan kajian ulang.

Harusnya DPR membawa permasalahan ini ke paripurna. Karena pembangunan gedung baru DPR tidak dapat dilepaskan dari BURT.

"Masing-masing perwakilan fraksi di BURT harus bertanggung jawab kepada seluruh anggota DPR dan menawarkan seluruh konsep pembangunan gedung baru. Kaji ulang yang disodorkan oleh Marzuki Alie ini hanya langkah untuk menutupi keinginan DPR agar pembangunan terus berlanjut," cetusnya. (AO/OL-3)



* * *

Marzuki Cs Jangan Lepas Tangan


Senin, 06 September 2010

RMOL. Kebijakan untuk menunda pembangunan gedung baru DPR yang disepakati Badan Urusan Rumah Tangga (BURT) semata-mata karena ada tekanan
keras dari publik.

Menurut Ketua Forum Pemantau Parlemen Indonesia (Formapi) Sebastian Salang, seandainya publik tidak melakukan kritik dan protes, pembangunan gedung mirip hotel bintang lima itu akan berjalan terus. Sebastian meminta DPR konsisten melakukan penundaan ini, tidak hanya formalitas atau sekadar untuk meredam kemarahan publik.

“Pimpinan DPR harus bertanggung jawab atas penundaan ini. Kalau hanya formalitas, saya yakin kemarahan masyarakat akan lebih massif dari sekarang,” katanya kepada Rakyat Merdeka Online, Senin malam (6/9).

Sebastian juga meminta pimpinan DPR dan BURT tidak menyerahkan sepenuhnya  rencana pembangunan itu pada tim teknis. Pimpinan DPR harus selalu mengontrol  apa yang direncanakan tim teknis.

“Jangan lepas tangan begitu saja dan mengaku tidak tahu-menahu masalah teknis. Tapi harus dikontrol dong agar tidak terlalu  mewah dan mahal. Ini penting, sebab yang kena imbas buruknya bukan tim teknis, tapi DPR. Jadi, nggak bisa lempar tanggung jawab
