---
layout: post
title: Suharto dan keluarganya adalah sampah bangsa
date: 2007-02-14
---

Akan digugatnya yayasan-yayasan Suharto oleh Kejaksaan Agung RI, dan disidangkannya di pengadilan di Inggris kasus penyimpanan uang haram Tommy Suharto tidak lama lagi, akan merupakan kesempatan yang baik sekali bagi banyak orang di Indonesia untuk mengetahui lebih jauh betapa bobroknya moral dan besarnya kejahatan-kejahatan yang telah dilakukan oleh “keluarga Cendana”.

Dilihat dari berbagai sudut, peristiwa pembongkaran kebobrokan keluarga Cendana ini mempunyai arti yang berjangkau jauh bagi sejarah bangsa Indonesia. Sebab, Suharto (beserta keluarganya) ini pernah memegang peran yang maha-penting dan maha-utama selama 32 tahun rejim militer Orde Baru. Suharto pernah dalam jangka yang lama sekali disanjung-sanjung sebagai “penyelamat bangsa”, “bapak pembangunan”, “pangayom rakyat banyak”, “pemimpin tertinggi Abri”, dan “pembina Golkar”.

Tetapi, setelah ia dipaksa mundur dari jabatannya oleh gerakan besar-besaran mahasiswa dan rakyat dalam tahun 1998, sedikit demi sedikit mulai terbongkarlah bahwa segala “kegemilangan » dan « gebyar » yang pernah menyelimutinya (beserta keluarganya) adalah sebenarnya selimut yang penuh dengan kotoran, dosa, dan kejahatan. Boleh dikatakan, bahwa kebobrokan “keluarga Cendana” di bidang moral, politik, dan kebudayaan, adalah perwujudan yang terpusat (concentrated expression) dari segala kebusukan, dosa, kesalahan, dan kejahatan yang dilakukan oleh Orde Baru.

Kerusakan-kerusakan besar oleh Suharto dan Orde Baru

Pembongkaran kebobrokan atau kebusukan Suharto beserta keluarganya adalah langkah penting bagi usaha bersama seluruh bangsa untuk mengadakan pembaruan, atau perombakan segala sistem pemerintahan dan penyembuhan penyakit moral bangsa yang sudah ditanamkan rejim militer Orde Baru selama puluhan tahun. Sebab, kita semua sudah menyaksikan sendiri bahwa kebejatan moral bangsa (antara lain : kebudayaan korupsi yang merajalela, pola hidup serba mewah di kalangan elite, ketidakpedulian terhadap penderitaan rakyat banyak) yang sudah berlangsung sejak lama di Indonesia adalah akibat dari praktek-praktek Orde Baru. Seperti sama-sama kita saksikan sendiri, kebejatan moral ini masih terus berlangsung sampai hari ini.

Dalam sejarah bangsa Indonesia penting sekali untuk dicatat bahwa Suharto beserta Orde Barunya sudah mendatangkan kerusakan-kerusakan besar sekali terhadap negara dan bangsa, baik yang berupa KKN maupun pelanggaran HAM. Generasi-generasi bangsa yang akan datang perlu mengetahui dengan jelas bahwa sejarah bangsa punya noda-noda hitam yang besar sekali, yang diwujudkan oleh kehadiran Suharto (dan keluarganya) yang disokong oleh sebagian pimpinan militer dan tokoh-tokoh Golkar (beserta simpatisan-simpatisan terdekatnya), yang bersekutu dengan nekolim, terutama imperialisme AS.

Karena itulah, kiranya, merupakan tugas atau kewajiban para intelektual, para sejarawan, para sastrawan, para wartawan, para tokoh dari berbagai golongan yang anti Orde Baru untuk terus-menerus mengangkat masalah kebobrokan dan kejahatan Suharto beserta keluarganya di bidang politik, pemerintahan, militer, yang antara lain meliputi masalah KKN dan pelanggaran berat di bidang HAM. Sejarah bangsa kita akan membuktikan bahwa memblejeti kejahatan atau mengutuk kebobrokan Suharto dan Orde Baru adalah perbuatan yang benar, sah, adil dan mulia.

Sumbangan George Aditjondro

Sebaliknya, sejarah akan juga menunjukkan bahwa memuji-muji Suharto (beserta keluarganya), atau mendiamkan kesalahan dan kejahatannya, atau melindungi kebobrokan politik dan kebusukan moralnya, adalah sikap yang salah sama sekali dan tidak menguntungkan kepentingan bangsa dan negara, termasuk kepentingan generasi kita yang akan datang.

Dari segi inilah kita lihat betapa besar sumbangan tulisan George Junus Aditjondro, yang sudah dibuat dalam tahun 1998, dan disiarkan kembali oleh Tempo Interaktif 14 Mei 2004.
Dengan membaca tulisan ini, kita akan mendapat gambaran – sedikit banyaknya – betapa luas dan bermacam-macamnya “yayasan-yayasan” yang didirikan oleh Suharto beserta keluarganya. Seluruhnya, apa yang dinamakan “yayasan-yasasan” itu berjumlah 40, yang meliputi bermacam-macam bidang, termasuk bidang yang “aneh-aneh” (harap baca lengkapnya “Yayasan-yayasan Suharto” .dalam website http://perso.club-internet.fr/kontak.)

Sejak Suharto sudah jatuh dalam tahun 1998, makin lama makin sedikit terdengar tentang “yayasan-yayasan” keluarga Suharto (sengaja di sini digunakan tanda kutip pada kata yayasan, karena pada hakekatnya, yayasan-yayasan yang didirikan oleh keluarga Suharto itu, bertujuan untuk hal-hal yang tidak saleh atau tidak berbudi luhur). Sejumlah yayasan-yayasan itu sudah hilang tak tentu rimbanya (termasuk aset-aset atau kekayaannya), sedangkan sebagian masih beroperasi dengan cara-cara yang patut diragukan kebersihannya, dan sebagian lagi tinggal namanya saja. Tetapi, bagaimana pun juga, dapat diperkirakan bahwa jumlah uang yang masih dikuasai oleh keluarga Suharto (beserta orang-orang terdekatnya) masih cukup besar, walaupun sebagian dana sudah “disingkirkan” atau “disimpan” di tempat-tempat yang tidak gampang diketahui.

Laporan majalah mingguan TIME.

Untuk mendapat gambaran lain - yang juga amat menarik ! - tentang kekayaan keluarga Suharto yang diperoleh dengan cara-cara tidak halal, dapat dibaca laporan majalah minguan
TIME Asia tanggal 24 Mei 1999. Laporan panjang sekali ini, yang merupakan “investigative reporting” (laporan yang bersifat investigasi –penyelidikan) diberi judul Suharto Incorporated. Karya ini adalah hasil penelitian yang lama sekali (empat bulan) yang dilakukan di 11 negeri di dunia, dengan dibantu oleh 5 ahli independen, untuk melacak kekayaan keluarga Suharto.

Laporan jurnalistik yang besar ini telah mendapat award (penghargaan) sebagai “best reporting” dalam tahun 2000.oleh Society of Publishers in Asia. Terjemahannya dalam bahasa Indonesia adalah, antara lain, yang disiarkan oleh situs Edi Cahyono (yang disajikan juga dalam website http://perso.club-internet.fr/kontak.). Penyajian kembali terjemahan laporan majalah TIME ini adalah perlu sekali, ketika pengadilan Indonesia akan mulai menyidangkan gugatan-gugatan terhadap yayasan-yayasan Suharto tidak lama lagi

Sebab, dengan membaca laporan majalah TIME tanggal 24 Mei 1999 ini, merupakan salah satu di antara banyak tulisan yang dengan gamblang menyoroti masalah usaha-usaha Suharto beserta keluarganya untuk “melarikan” atau “menyembunyikan” kekayaannya yang dikumpulkannya melalui cara-cara yang tidak luhur selama puluhan tahun.

Dalam laporan itu disebutkan bahwa Suharto telah memindahkan uang sebanyak 9 miliar dollar AS dari bank Switzerland ke bank Austria. Juga bahwa di Indonesia, enam anak Soeharto memiliki saham dalam jumlah signifikan sekurang-kurangnya di 564 perusahaan, dan kekayaan luar negeri mereka mencakup ratusan perusahaan-perusahaan lainnya.


Keluarga Soeharto juga memiliki sejumlah besar hiasan-hiasan berharga. Selain itu, sebuah ranch untuk berburu seharga $ 4 juta di Selandia Baru and memiliki setengah dari kapal pesiar (yatch) seharga $ 4 juta yang ditambatkan di luar Darwin, Australia. Hutomo Mandala Putra (nama kecilnya Tommy) putra terkecil, mempunyai 75 persen saham di sebuah lapangan golf 18 lubang dengan 22 apartemen mewahnya di Ascot, Inggris. Bambang Trihatmojo, putra kedua Soeharto, memiliki sebuah penthouse seharga $ 8 juta di Singapura dan sebuah rumah besar seharga $ 12 juta di sebuah lingkungan esklusif di Los Angeles, letaknya dua rumah dari rumah bintang rock Rod Steward dan hanya beda satu jalan dari rumah saudaranya Sigit Harjoyudanto yang bernilai $ 9 juta.


Setelah melakukan ratusan wawancara dengan teman-teman Soeharto, baik yang dahulu mau pun sekarang, pegawai pemerintah, kalangan bisnis, ahli-ahli hukum, akuntan-akuntan, bankir-bankir dan keluarga mereka, serta meneliti berlusin-lusin dokumen (termasuk catatan-catatan Bank tentang pinjaman luar negeri terbesar), para koresponden TIME menemukan indikasi bahwa sekurang-kurangnya $ 73 milyar melewati tangan-tangan keluarga Soeharto antara tahun 1996 hingga tahun lalu (1998) Sebagian besar berasal dari pertambangan, perkayuan, komoditi-komoditi dan industri-industri perminyakan. Investasi-investasi buruk dan krisis keuangan di Indonesia telah menurunkan jumlah kekayaan tersebut. Tetapi bukti mengindikasikan bahwa Soeharto dan enam anaknya tetap memiliki kekayaan $ 15 milyar tunai, saham.saham, modal-modal perusahaan, real estate, perhiasan dan benda-benda seni (Harap baca bahan selengkapnya dalam artikel majalah TIME 24 Mei 1999).


Suharto beserta keluarganya adalah sampah bangsa


Meskipun laporan majalah TIME ini sudah dibuat 8 tahun yang lalu, dan juga meskipun mungkin sekali berbagai data atau fakta yang tercantum di dalamnya sekarang sudah mengalami perubahan, tetapi toh laporan besar ini tetap menarik untuk disimak kembali. Ini penting untuk mendapat gambaran yang jelas bahwa Suharto (beserta keluarganya) memang benar-benar orang-orang yang tidak patut dihormati, tidak pantas disegani, apalagi disajung-sanjung, seperti yang terjadi selama puluhan tahun Orde Baru. Dari tulisan George Aditjondro dan laporan besar majalah TIME itu saja banyak orang sudah dapat menyimpulkan bahwa Suharto beserta keluarganya adalah sampah bangsa, yang tidak saja menimbulkan bau busuk, melainkan juga menyebarkan penyakit, sehingga bangsa dan negara menjadi rusak seperti sekarang ini.


Setelah ia dipaksa untuk meletakkan jabatan sebagai presiden RI, dan tidak lagi maha-kuasa seperti selama 32 tahun Orde Baru, makin banyak orang yang melihat berbagai dosa dan kesalahan-kesalahannya atau kejahatannya. Tidak saja bahwa ia sudah mengkhianati Bung Karno dan revolusi Indonesia dan bersekutu dengan imperialisme AS, melainkan juga bahwa ia ternyata sudah menjadi maling yang terbesar dalam sejarah bangsa.


Karena itu, meskipun sekarang masih saja ada sisa-sisa benggolan-benggolan Orde Baru yang tetap memuja-muja Suharto, tetapi makin banyak juga yang menjauhinya atau malu berhubungan secara terang-terangan dengannya atau anak-anaknya. Suharto dan keluarganya sudah makin terkucil dari masyarakat luas, dan hanya hidup (dengan mewah sekali !) dalam lingkungan-lingkungan yang tertutup dan terbatas.


Perkembangan sudah menunjukkan bahwa nasib Suharto beserta keluarganya akhirnya akan mirip dengan nasib penguasa-penguasa korup di dunia, seperti presiden Filipina Ferdinand Marcos, kaisar dari Ethiopia Haila Selassi, presiden dari Conggo Mobutu Sese Seko, dan diktator dari Haiti yang bernama Duvallier.


Sejarah bangsa Indonesia akan menjadi saksinya !!!

---

Berkas Perdata Soeharto Dilimpahkan

ke Pengadilan Jakarta Selatan



JAKARTA -- Kejaksaan Agung menyatakan berkas gugatan perdata terhadap
yayasan yang dipimpin bekas presiden Soeharto akan dilimpahkan ke
Pengadilan Negeri Jakarta Selatan. "Tunggu sekitar satu bulan lagi
(akan dilimpahkan ke Pengadilan Negeri Jakarta Selatan)," kata
Direktur Perdata Kejaksaan Agung Yoseph Suardi Sabda di sela-sela
sidang permohonan hak uji di Mahkamah Konstitusi kemarin.

Menurut Yoseph, pelimpahan berkas perkara tersebut berdasarkan surat
kuasa dari presiden yang disampaikan ke kejaksaan pada akhir Februari
lalu. Adapun gugatan perdata yang diajukan adalah terhadap Yayasan
Supersemar. Menurut Yoseph, yayasan yang didirikan Soeharto itu
diduga menyalahgunakan dana donasi dari pemerintah yang besarnya mencapai Rp
1,5 triliun.

Di tempat terpisah, Jaksa Agung Hendarman Supandji mengatakan berkas
gugatan perdata kasus yayasan Soeharto masih perlu dilengkapi.
Menurut dia, ada beberapa dokumen bukti yang masih berbentuk kopian (salinan)
yang harus dilegalisasi. "Perlu dokumen yang resmi untuk dijadikan
alat bukti," ujar Hendarman kepada wartawan di kantornya kemarin.

Menurut dia, kejaksaan akan segera menyelesaikan berkas perdata
gugatan tersebut. Kejaksaan menargetkan penyelesaian berkas tersebut
sebelum Hari Bhakti Adhyaksa--hari ulang tahun kejaksaan--pada 22
Juli mendatang. "Satu-satunya faktor adalah legalisasi dokumen itu," kata
Hendarman.

Gugatan perdata kasus yayasan Soeharto terkait dengan adanya aliran
dana pemerintah di tujuh yayasan yang dipimpin Soeharto. Nilai dana
pemerintah yang dipermasalahkan oleh kejaksaan dan dipakai oleh
yayasan diduga lebih dari Rp 1 triliun. Hendarman menjelaskan dalam
gugatan perdata tersebut kejaksaan ingin membuktikan adanya perbuatan
melawan hukum dan wanprestasi. "Kecenderungan kejaksaan adalah ingin
membuktikan adanya perbuatan melawan hukum," ujarnya.RINI KUSTIANI |
SANDY INDRA PRATAMA
