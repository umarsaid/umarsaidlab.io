---
layout: post
title: Sikap Bung Karno tentang PKI dan Marxisme adalah benar !
date: 2010-05-22
---

Seperti sudah ditulis terlebih dahulu, selama hampir setengah abad (sejak 1966 sampai sekarang ) PKI dinyatakan sebagai partai terlarang berikut segala ajaran-ajaran Marxisme, Leninisme, oleh penguasa militer di bawah Suharto dan pemerintahan-pemerintahan berikutnya. Karena adanya larangan itu, tidak banyak orang tahu tentang sejarah PKI yang sebenarnya, dan apa itu sebenarnya Marxisme.

Sekali lagi, selama hampir setengah abad ( !!!) sebagian terbesar dari rakyat hanya mendengar dari satu fihak atau dari satu kubu saja yang serba buruk, serba jahat, serba negatif saja tentang PKI.  Di suratkabar, majalah, radio, televisi orang sering membaca dan melihat berbagai pembesar militer dan sipil  -- dan pemuka-pemuka masyarakat reaksioner -- berbicara tentang  bahaya laten komunis, « pengkhianatan PKI » lewat peristiwa G30S,  percobaan  perebutan kekuasaan  oleh PKI, kudeta PKI terhadap pemerintahan Sukarno.

Bukan itu saja !  Seperti yang masih bisa diingat-ingat oleh banyak orang, bertahun-tahun sebelum dan sesudah digulingkannya pemerintahan Bung Karno, boleh dikatakan tiap hari ada berita-berita (bohong) di suratkabar,  atau majalah, radio, televisi tentang banjir dan jebolnya tanggul-tanggul karena sabotase PKI, atau robohnya tiang listrik dan tiang tilpun oleh aksi gelap PKI, atau jembatan rusak oleh gerakan PKI di bawah tanah, dan segala macam berita lainnya yang aneh-aneh, termasuk yang « gila » dan tidak masuk akal sama sekali.

Di sekolah-sekolah di seluruh Indonesia, dari yang paling rendah sampai yang paling tinggi para anak didik diharuskan membaca buku-buku pelajaran yang berisi tentang kejahatan yang dilakukan oleh PKI atau orang-orang komunis yang tidak bermoral baik.

Dalam dakwah  di banyak mesjid-mesjid sering disebut-sebut bahwa orang-orang komunis pengikut PKI adalah anti-Tuhan, anti-agama, kafir, iblis, yang  (bahkan !) darahnya halal untuk dibunuh
Seluruh pegawai negeri diharuskan secara paksa untuk mengikuti  kursus Pancasila (yang palsu), di mana sering disebutkan bahwa PKI anti Pancasila dan orang-orang komunis adalah iblis.

Kampanye fitnahan selama puluhan tahun

Semua itu dilakukan oleh rejim militer Suharto dengan bermacam-macam paksaan dan melalui beraneka ragam cara, dalam waktu puluhan tahun, terus-menerus, secara intensif, dan besar-besaran. Dan selama puluhan tahun itu tidak ada orang atau kalangan yang bisa (atau boleh)  memberikan balasan atau jawaban terhadap suara-suara yang hanya serba negatif saja tentang PKI, apalagi membela PKI secara terbuka dan terangan-terangan. Selama puluhan tahun, yang boleh beredar hanyalah informasi yang merugikan, yang menjelek-jelekkan, yang memfitnah PKI dan Bung Karno.

Begitu hebatnya kampanye fitnahan yang dilakukan Orde Baru terhadap PKI sehingga tidak sedikit  orang di Indonesia yang kemudian percaya dan menjadi yakin bahwa orang-orang komunis atau anggota PKI adalah orang-orang jahat, tidak bermoral, berjiwa licik, « menghalalkan segala cara » , munafik, tidak manusiawi, sehingga perlu dibasmi « sampai ke akar-akarnya »

Kampanye besar-besaran, menyeluruh,  terus-menerus, dan secara intensif  (sekali lagi : selama puluhan tahun)  melalui segala cara, bentuk, dan jalan  untuk memfitnah PKI  adalah salah satu dari begitu banyak politik rejim militer Orde Baru yang paling kotor, bengis, biadab, di samping pembunuhan massal dan pemenjaraan jutaaan orang-orang tidak bersalah, dan juga perlakuan tidak manusiawi terhadap Bung Karno. Karena, sebagai akibat dari kampanye negatif terhadap PKI ini, puluhan juta orang warganegara  Republik Indonesia menanggung berbagai macam penderitaan, sejak jaman Orde Baru sampai sekarang.

Apakah PKI atau orang komunis seburuk itu ?

Mengingat hebatnya kampanye  berpuluh-puluh tahun jang mengandung beraneka ragam fitnah terhadap PKI atau anggota-anggotanya yang komunis, maka bisa saja ada banyak orang yang bertanya-tanya apakah  semuanya itu benar ?

Kiranya, terhadap orang-orang atau kalangan yang bertanya demikian , atau  bahkan kalau ada  yang  percaya bahwa segala yang negatif itu ada benarnya, maka seyogianya dianjurkan untuk ber-sama-sama memeriksa hal-hal sebagai berikut ini sekadar untuk bahan pertimbangan dan renungan :

PKI telah didirikan dalam tahun 1920 di Semarang, mula-mula dengan nama Partai Komunis di Hindia, dan kemudian dirubah dengan  nama  Partai Komunis INDONESIA dalam tahun 1924. Di antara para anggota PKI pada waktu itu tadinya adalah anggota-anggota Sarekat Islam. Partai Komunis di Indonesia adalah partai komunis yang tertua di benua Asia, lebih tua satu tahun dari pada Partai Komunis Tiongkok yang didirikan di Shanghai dalam tahun 1921.

PKI yang didirikan untuk menentang penjajahan Belanda yang sudah berlangsung lebih dari 3 abad, telah melancarkan pembrontakan di berbagai tempat di Jawa dan Sumatera Barat dalam tahun 1926. Seperti yang sudah ditulis berkali-kali, karena pembrontakan itu maka ribuan anggota            dan simpatisan komunis dibunuh oleh pemerintahan kolonial Belanda, 13.000 orang dipenjarakan, dan 1308 orang dibuang ke Digul. Dalam tahun 1927 PKI dinyatakan sebagai organisasi terlarang . Sejak itu, PKI terpaksa melakukan kegiatan-kegiatannya di bawah tanah, sampai terjadinya proklamasi 17 Agutus 45.

Jadi, sudah sejak dari permulaan didirikannya dalam tahun 1920, PKI terdiri dari orang-orang yang mau berjuang  dengan sukarela melawan kolonialisme dan imperialisme, dan membebaskan rakyat dari segala macam  penindasan dan ketidakadilan. Para pengamat sejarah dan semua orang yang jujur dan objektif bisa melihat bahwa PKI merupakan satu organisasi yang anggota-anggota atau pengikutnya pada umumnya betul-betul mempunyai semangat yang tinggi untuk berjuang demi kepentingan rakyat banyak, bahkan dengan berbagai pengorbanan yang besar, seperti yang sudah sering ditunjukkan  di masa-masa yang lalu.

Pejabat-pejabat anggota PKI umumnya jauh dari korupsi

Oleh karena itu semualah maka kita bisa melihat bahwa kebanyakan orang-orang komunis  atau sebagian terbesar anggota-anggota PKI bukanlah  seperti sudah dikampanyekan berpuluh-puluh tahun oleh Orde Baru (beserta para pendukung setianya)  sebagai orang-orang jahat, anti-Tuhan, kafir, tidak bermoral, munafik, suka menipu rakyat, selalu mengobarkan pertentangan berbagai golongan, sering mangadu-domba,  atau mengganggu ketertiban masyarakat dan selalu menimbulkan keonaran.

Padahal, sejarah sudah membuktikan bahwa ketika pemerintahan  ada di bawah pimpinan Presiden Sukarno, kelihatan jelas bahwa anggota-anggota atau simpatisan-simpatisan PKI waktu itu dapat menjalankan  jabatan-jabatan penting dalam berbagai lembaga negara atau badan pemerintahan secara baik, dengan moral bersih, jauh dari  korupsi, dan disenangi rakyat.

Di antara generasi tua atau kalangan lanjut usia di negeri kita mungkin masih banyak sekali yang ingat dan bisa menceritakan bahwa kebanyakan atau sebagian terbesar sekali  para anggota dan simpatisan PKI yang menjabat gubernur, bupati, walikota, camat, lurah atau orang-orang penting dalam banyak lembaga dan jawatan pemerintahan adalah orang-orang yang bersih dari korupsi atau penyalahgunaan kekuasaan.

Boleh dikatakan bahwa suratkabar, majalah, televisi pada kurun waktu itu jarang atau sedikit  sekali  menyiarkan adanya korupsi atau berbagai kejahatan lainnya yang dilakukan para pejabat yang komunis atau simpatisan PKI.



Perbedaan seperti siang dan malam

Hal yang demikian itu adalah bertentangan sama sekali, seperti siang dan malam ( !), dengan sikap para pejabat (dan pembesar-pembesar militer) dalam pemerintahan Orde Baru dan pemerintahan-pemerintahan berikutnya, termasuk pemerintahan SBY sekarang ini. Seperti yang sudah disaksikan sendiri oleh banyak orang, para pejabat pendukung setia pemerintahan Orde Baru dan penerusnya sampai sekarang,  kebanyakan terdiri dari orang-orang yang motivasinya  untuk mengabdi kepada kepentingan rakyat adalah tipis sekali  atau nihil sama sekali, dan sikap moralnya juga jelek sekali.

Kebejatan moral, dan kerusakan rohani atau kebobrokan mental  yang sudah dipamerkan dewasa ini dengan gamblang oleh berbagai kasus besar dan parah sekitar masalah korupsi dan berbagai kejahatan sekitar BLBI, Bank Century, Anggodo, Gayus Tambunan, jenderal-jenderal  Polri, dinas perpajakan, adalah ibarat hanya secuwil kecil saja dari gunung Himalaya, dari segala keburukan dan kebusukan yang sudah terjadi sejak puluhan tahun Orde Baru sampai sekarang.

Sudah dibuktikan di masa lalu bahwa selama pemerintahan Bung Karno tidak pernah kelihatan terjadinya kebejatan moral, atau kerusakan rochani dan  kebobrokan mental yang skalanya sebesar itu dan keparahannya seluas itu, baik di kalangan  para pendukung politik Bung Karno secara umum, apalagi oleh kalangan orang-orang komunis anggota PKI  



Bung Karno membela PKI sampai wafatnya

Seperti kita ketahui, PKI adalah satu-satunya partai politik di Indonesia yang sudah sejak pembrontakan melawan  pemerintahan kolonial Belanda dalam tahun 1926 paling besar menyumbangkan pengorbanan demi kepentingan rakyat banyak. Sikap mengabdi rakyat ini kemudian diteruskan selama pemerintahan di bawah pimpinan Bung Karno, dengan mendukung berbagai politik revolusionernya, baik yang mengenai bidang nasional maupun internasional.

Kiranya, karena sejak mudanya Bung Karno sudah tertarik kepada Marxisme dan juga bukti-bukti yang sudah ditunjukkan dengan jelas oleh PKI dalam sejarah itu pulalah maka Bung Karno, sebagai pemimpin dan pemersatu bangsa  ia  mengeluarkan konsepsinya yang terkenal, yaitu  NASAKOM.

Seandainya PKI atau orang-orang komunis itu memang betul-betul mempunyai ciri-ciri seburuk atau watak-watak sejahat seperti yang dikampanyekan oleh kalangan  reaksioner dalam dan luar negeri sebelum dan sesudah peristiwa G30S, maka tentunya Bung Karno tidak akan mau (atau tidak berani) menyajikan gagasan besarnya NASAKOM kepada seluruh bangsa kita

Bahkan sebaliknya. Ketika jutaan kader-kader, anggota dan simpatisan PKI  (dan juga orang-orang yang tidak ada hubungan sama sekali dengan PKI), sudah dibunuhi atau dipenjarakan di seluruh Indonesia oleh militer bersama kalangan reaksioner, Bung Karno dengan gigih masih membela PKI dan NASAKOM-nya, sampai akhir hayatnya.

Dalam rangka mengenang hari lahir PKI tanggal 23 Mei tahun 1920 maka tulisan ini berusaha menyajikan  sebagian kecil sekali dari sejarah  PKI  dan mencoba menyoroti berbagai fitnahan terhadap orang-orang  komunis yang  --  sebagai akibatnya -- selama hampir setengah abad mengalami perlakuan tidak adil, atau disengsarakan dengan berbagai macam penderitaan.



Sikap Bung Karno terhadap PKI adalah benar

Sebab, di Indonesia perlulah kiranya difahami oleh sebagian terbesar rakyat bahwa orang-orang komunis yang tergabung dalam gerakan kiri atau organisasi semacam PKI pada umumnya merupakan unsur-unsur penting dalam  perjuangan  rakyat untuk melawan berbagai ketidakadilan dan  untuk membangun masyarakat adil dan makmur, dan dalam dewasa ini juga untuk menentang neo-liberalisme.

Di skala dunia pun, dalam perjuangan melawan neo-liberalisme yang berlangsung di Eropa, Afrika, Asia, Australia, Amerika Serikat dan Amerika Latin, orang-orang komunis atau berbagai macam aliran kiri lainnya menjadi pendorong atau bagian aktif dari banyak gerakan. Sudah dicatat dalam sejarah dunia bahwa berbagai macam aliran kiri atau unsur-unsur revolusioner adalah pelopor dari adanya banyak perubahan-perubahan politik, ekonomi dan sosial di berbagai negeri.

Karenanya, adalah jelas sekali bahwa terus-menerus melarang PKI beserta Marxisme di Indonesia adalah sama sekali tidak menguntungkan bagi perjuangan rakyat melawan neo-liberalisme beserta agen-agennya di dalam negeri dan perjuangan untuk mengadakan perubahan-perubahan besar dan fundamental.

Perkembangan situasi di Indonesia dewasa ini dan juga di kemudian hari akan menunjukkan dengan jelas kepada siapa pun bahwa terus-menerus melakukan kampanya fitnah terhadap PKI dan menjelek-jelekkan orang-orang komunis juga tidak adanya gunanya sama sekali bagi kepentingan rakyat banyak.

Oleh karena itu, kita bisa melihat bahwa sikap Bung Karno  -- dalam banyak hal  -- mengenai PKI atau Marxisme adalah benar !
