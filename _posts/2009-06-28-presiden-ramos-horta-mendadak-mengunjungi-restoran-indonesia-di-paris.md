---
layout: post
title: Presiden Ramos Horta mendadak mengunjungi Restoran INDONESIA di Paris
date: 2009-06-28
---

Di luar dugaan banyak orang, Restoran koperasi INDONESIA di Paris secara mendadak sekali mendapat kunjungan tamu terhormat, yaitu Presiden Jose Ramos Horta dari  Republik Demokratik Timor Leste. Peristiwa ini terjadi pada hari Sabtu malam tanggal 27 Juni 2009. Banyak hal-hal yang menarik  (dan juga sangat penting) yang bisa diangkat atau diceritakan tentang kunjungan mendadak ini, sebab mempunyai arti atau nilai sejarah yang tidak kecil.

Bahwa kunjungan Presiden Ramos Horta ini mendadak sekali bagi para pekerja restoran (termasuk bagi managernya Bung Suyoso) adalah bahwa baru satu jam sebelum kedatangan Presiden Ramos Horta di restoran  diketahui dengan pasti bahwa ia bersama stafnya sudah memesan kamar  di Hotel  Senat yang letaknya berdampingan (terpisah satu tembok) dengan restoran INDONESIA.  Presiden Ramos Horta datang ke Paris hari Sabtu siang, untuk kunjungan yang bersifat setengah privé, dan besoknya (hari Minggu) sudah meninggalkan Paris lagi.

Satu jam sebelum kedatangan presiden Ramos Horta ke restoran, kami mendapat keterangan dari stafnya (3 orang) bahwa ia merencanakan bertemu pada jam 8 malam dengan 3 sahabat lamanya (A. Umar Said, Antonio Diaz dan Carlos Semedo).

Adalah menarik untuk diketahui mengapa presiden Ramos Horta, sebagai kepala suatu negara,  memilih Hotel Senat, suatu hotel kelas menengah bintang tiga). Mungkin sekali karena hotel ini terletak berdampingan dengan Restoran  INDONESIA, atau karena pertimbangan-pertimbangan lainnya, yang berkaitan dengan kunjungannya satu malam yang bersifat setengah prive di Prancis. Namun, walaupun kunjungan ini bersifat setengah prive dan hanya satu malam, pemerintah Prancis menyediakan 4 orang dari Dinas Securité untuk selalu menjaga atau mengikutinya.

Pejuang Ramos Horta tidur di kursi restoran

Keputusan presiden Ramos Horta untuk berkunjung lagi ke Restoran INDONESIA dan kali ini juga bertemu khusus dengan sahabat-sahabat lamanya mengandung arti yang dalam.  Restoran INDONESIA memang mempunyai sejarah tersendiri bagi perjuangan rakyat Timor Timur dan perjuangan rakyat Indonesia dalam perlawanan bersama terhadap rejim militer Suharto.

Setelah Restoran  koperasi ini dibuka dalam bulan Desember 1982 (jadi sudah lebih dari 26 tahun yang lalu) sering sekali diadakan pertemuan-pertermuan antara berbagai orang (Prancis dll) dengan anggota-anggota Komite Setiakawan dengan Timor Timur. Restoran INDONESIA dalam jangka lama sekali dianggap oleh berbagai kalangan sebagai salah satu di  antara pusat-pusat kegiatan perlawanan rakyat Timor Timur terhadap agresi rejim militer Suharto.

Bahkan pada suatu waktu ketika Ramos Horta berkunjung ke Paris untuk kegiatan-kegiatan perjuangan rakyat Timor Timur,  ia pernah tidur di kursi-kursi yang dijejer-jejerkan, dan mandi di bawah douche sederhana yang terletak di ruangan bawah restoran. Hal ini diceritakan oleh presiden Ramos Horta sambil makan malam itu di depan 3 stafnya dan 3 sahabat lamanya beserta seorang tamunya dari Spanyol.

Cerita presiden Ramos Horta tentang tidurnya di atas kursi restoran dan mandi di bawah douche (yang sebenarnya tidak digunakan sebagai kamar mandi), dan cerita tentang kegiatan-kegiatan lainnya semasa ia masih sebagai pejuang, mengingatkan kami semua kepada masa-masa silam ketika kami berjuang bersama-sama untuk rakyat Timor Timur.

Tukang cat, sahabat lama Ramos Horta

Dalam pembicaraan santai antara sahabat-sahabat lama sambil makan itu presiden Ramos Horta juga menceritakan di depan kami semua bagaimana pada suatu saat ia pernah menginap di apartemen Antonio Diaz, dan terpaksa tidur di lantai (tetapi pakai alas kasur) karena tidak bisa tidur di hotel. Antonia Diaz adalah seorang Portugis, pernah bekerja sebagai tentara Portugis di Timor Timur, dan sudah lama bekerja di Paris sebagai tukang cat dan bangunan.

Carlos Semedo, seorang Prancis yang sudah lama sekali memimpin berbagai kegiatan mengenai Timor Timur (dan khususnya soal-soal yang berkaitan dengan Sanana Gusmao dan Ramos Horta), adalah sahabat karib Antonio Diaz.

Keinginan presiden Ramos Horta untuk bertemu dan makan bersama dengan sahabat-sahabat lamanya (sekali lagi, antara lain yang bekerja sebagai tukang cat) menunjukkan bahwa walaupun ia sekarang menjabat sebagai presiden, tetapi tidak lupa kepada orang-orang yang di masa-masa yang lalu telah melakukan perjuangan bersama-samanya. Sungguh, suatu hal yang indah !.

Begitu santainya, dan begitu pula hangatnya suasana dalam pertemuan sambil makan itu, yang diselingi oleh acara tarian topeng diiringi gamelan dan suling kecapi, sehingga Antonio tidak segan-segan selalu menyapa presiden Ramos Horta dengan « kau » (dalam bahasa Prancis « tu »). Jadi, dalam pertemuan antara sahabat lama  itu terutama sekali banyak dibicarakan soal-soal masa lalu.

Perjuangan komite Timor Timur di berbagai negeri

Di antara pembicaraan itu kami tinjau bagaimana besar sumbangan kegiatan-kegiatan untuk membantu perjuangan rakyat Timor Timur  yang diadakan secara luas dan selama puluhan tahun , serta berskala internasional, telah merupakan sumbangan penting untuk terisolasinya rejim militer Suharto di hadapan opini internasional. Tidak salahlah kiranya kalau dikatakan bahwa komite-komite Timor Timur yang melakukan berbagai kegiatan di banyak sekali negeri di dunia sudah membantu jatuhnya rejim militer Suharto.

Dari segi ini bisa dilihat bahwa membantu perjuangan rakyat Timor Timur adalah satu dan senyawa dengan perjuangan menentang rejim militer Orde Baru. Hal ini pulalah yang telah dilakukan melalui sebagian kegiatan-kegiatan berbagai orang dengan  Restoran INDONESIA. Dalam kaitan ini telah disinggung dekatnya hubungan berbagai tokoh Prancis dengan restoran, umpamanya istri presiden Prancis François MITTERRAND (alm), Madame Danielle MITERRAND (yang pernah berkunjung ke Timor Timur) dan Louis JOINET, ahli hukum yang menjadi pembantu 5 Perdana Menteri Prancis berturut-turut dan merangkap wakil Prancis di Komisi HAM di PBB. Louis JOINET adalah sahabat dekat Ramos Horta dan juga sahabat dekat Restoran INDONESIA.

Kedatangan Ramos Horta ke Paris dalam tahun 1976

Yang juga banyak dikenang bersama adalah kunjungan pertama kali Ramos Horta dalam tahun 1976 ke Paris, beberapa waktu setelah militer Indonesia di bawah perintah Suharto melakukan agresi mencaplok Timor Timur. Setelah mengetahui bahwa Ramos Horta ada di Holland dan bertemu dengan orang-orang dari Komite Indonesia di Amsterdam (antara lain Prof. Wertheim dan  Go Gin Tjwan) maka A. Umar Said bersama sejumlah sahabat-sahabat Prancis mengusahakan kedatangan Ramos Horta  ke Paris.

Dalam tahun 1976 itu (lebih dari 32 tahun yang lalu), untuk pertama kalinya diadakan rapat besar mengenai Timor Timur dengan pembicara utama Ramos Horta dan sejumlah tokoh-tokoh terkemuka Prancis. Sebagai kelanjutan rapat besar ini, maka terbentuklah untuk pertama kalinya Komite Setiakawan dengan Timor Timur. Komite Timor Timor di Paris adalah salah satu di antara komite yang tertua di dunia waktu itu. Semua ini rupanya merupakan kenangan bagi presiden Ramos Horta, dan juga bagi kami semua.

Sudah tentu, dalam  pembicaraan santai antara sahabat-sahabat lama itu, telah disinggung macam-macam soal. Antara lain tentang jalannya restoran, yang menurut pendengaran presiden Ramos Horta tetap berjalan baik. Juga telah dibicarakan Sobron Aidit (almarhum) dan Emil Kusni, yang kebetulan tidak ada di Paris dan sedang berada di Kalimantan. Presiden Ramos Horta sempat menanyakan beberapa hal mengenainya kepada Didien (istri Emil yang bekerja di restoran juga).

Bahasa Indonesia menjadi bahasa resmi di Timtim

Dalam pertemuan sambil makan yang sering diselingi dengan gelak-tawa itu ada juga sesuatu yang bisa dianggap besar dan serius. Presiden Ramos Horta mengatakan bahwa ia merencanakan untuk mengumumkan dalam bulan Agustus yad, dalam rangka memperingati 10 tahun referendum Timor Timur, ditingkatkannya bahasa Indonesia sebagai bahasa resmi (official language) di Timor Timur.

Bagi kami, yang mendengar rencana ini dari presiden Ramos Horta, merupakan hal yang baru dan penting sekali. Sebab, selama ini bahasa yang dipakai sebagai bahasa resmi adalah bahasa Portugis dan Tetum, sedangkan bahasa Indonesia  dan Inggris dipakai sebagai "working language" dalam pemerintahan dan bisnis. Kalau rencana presiden Ramos Horta ini betul-betul dilaksanakan mulai Agustus, maka akan merupakan tindakan yang realis,   berani, dan juga bisa membuka dimensi-dimensi baru dalam hubungan Indonesia dan Timor Timur untuk masa depan.

Sebab, sekarang ini, bahasa Indonesia sudah banyak dipakai oleh rakyat Timor Timur, baik di kalangan penduduk untuk pergaulan dan dagang, maupun di kalangan pemerintahan atau untuk urusan-urusan resmi dengan jawatan-jawatan. Dengan makin meningkatnya lalu lintas orang dan perdagangan dengan Indonesia, dan makin lancarnya komunikasi, maka peran bahasa Portugis makin terasa berkurang.

Dengan dibeberkannya rencana pengumuman pemakaian bahasa Indonesia sebagai bahasa resmi (official language), maka presiden Ramos Horta menunjukkan kemauan politiknya yang lebih besar dan lebih maju lagi dalam menggalang hubungan persahabatan dengan Indonesia. Dan bahwa rencananya itu diutarakan di depan sahabat-sahabat seperjuangannya yang lama dan juga di restoran INDONESIA  mempunyai arti tersendiri yang penting juga.

Presiden Ramos Horta adalah salah satu di antara tokoh-tokoh Timor Timur yang selama puluhan tahun berjuang terus-menerus, dan sekeras-kerasnya, menentang agresi rejim militer Suharto, sehingga ia menjadi tokoh internasional dan mendapat hadiah Nobel untuk perdamaian. Ia juga mendapat gelar doktor dalam ilmu hukum dari 6 universitas terkenal di berbagai negeri dan meraih beberapa  hadiah (award) internasional.  Sekarang, sebagai presiden Republik Demokratik Timor Leste ia berusaha membuka halaman-halaman baru, demi kepentingan rakyat Indonesia dan Timor Timur.

Dengan perspektif yang seindah inilah kami melihat atau mengartikan kunjungannya yang mendadak di restoran INDONESIA di Paris. Untuk itulah telah dibuat banyak sekali foto-foto dengan sahabat-sahabat seperjuangan lama dan juga dengan para anggota koperasi yang bekerja di restoran.
