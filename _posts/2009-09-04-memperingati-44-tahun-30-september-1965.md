---
layout: post
title: Memperingati 44 tahun 30 September 1965
date: 2009-09-04
---

Dalam rangka memperingati  44 tahun 30 September 1965, yang merupakan bagian sejarah bangsa kita yang penuh darah dan tragedi yang mengerikan, akibat kontra-revolusi tindakan militer di bawah pimpinan Suharto (dengan dukungan negara-negara Barat terutama AS)  untuk menghancurkan kekuatan kiri pendukung Bung Karno, maka akan diusahakan adanya  tulisan-tulisan yang berkaitan dengan peristiwa itu.

Untuk mengenang kembali 44 tahun 30 September 1965 dan mencoba menelaahnya atau merenungkannya dari berbagai segi atau sudut pandang maka  website http://umarsaid.free.fr akan mengusahakan adanya rubrik khusus yang menyajikan berbagai tulisan atau bahan yang diambil dari macam-macam sumber.

Rubrik « Memperingati 44 tahun 30 September 1965 » ini dimaksudkan sebagai sekadar sumbangan (walaupun sekecil-kecilnya) untuk mengingatkan kembali banyak orang dari berbagai kalangan dan golongan –- terutama dari golongan kiri atau para korban penindasan rejim militer Orde Baru --  akan besarnya  kesengsaraan, atau luasnya penderitaan, atau dalamnya kepedihan hati puluhan (bahkan ratusan) juta dari sebagian bangsa kita.

Sebab, mengingat kembali (dan sesering mungkin ) segala kejahatan dan pelanggaran HAM yang dilakukan oleh Suharto dan konco-konconya adalah sangat penting sekali sebagai bagian dari pendidikan bangsa. Berbagai macam kejahatan Suharto (dan Golkar) yang berpuluh-puluh tahun, dan yang diteruskan sekarang ini oleh sisa-sisa Orde Baru, perlu terus-menerus diangkat kembali sebanyak mungkin oleh berbagai kalangan masyarakat.

Pemblejedan kejahatan Orde Baru adalah tugas penting

Perjuangan seluruh gerakan demokratis melawan sisa-sisa kekuatan Suharto, baik di kalangan militer atau sipil (antara lain Golkar) ternyata masih perlu terus dikembangkan atau digalakkan, Sebab, meskipun usaha Tommy Soeharto, atau Tutut (atau siapa pun lainnya) untuk membangkitkan kembali « kejayaan » Golkar  akhirnya toh akan gagal, namun pemblejedan dosa-dosa Jalan Cendana tetap merupakan tugas penting bagi semua yang menginginkan tegaknya demokrasi, pembebasan, dan kesejahteraan bagi orang banyak.

Memperingati kekejaman atau kebiadaban rejim militer (yang mendapat dukungan sepenuhnya dari Golkar atau segala jenis simpatisan-simpatisan Suharto umumnya) selama puluhan tahun tidaklah untuk sekadar membuka luka-luka lama, atau semata-mata mengumbar dendam, dan tidak pula hanya untuk membongkar hal-hal lama yang sudah perlu dilupakan. Tidak!

Memperingati segala kebiadaban yang dilakukan oleh fihak militer di bawah pimpinan Suharto  berkaitan dengan peristiwa 30 September adalah justru dengan maksud mulia dan luhur bagi seluruh bangsa (termasuk untuk generasi yang akan datang atau anak-cucu kita), yaitu menegakkan keadilan bagi semuanya berdasarkan kebenaran, mengutuk segala macam kebiadaban yang bertentangan dengan HAM, mencegah terulangnya kembali  - oleh siapa pun !!! - segala macam kejahatan yang telah dilakukan oleh pemerintahan Suharto selama 32 tahun.

 Pentingnya mengembangkan kekuatan progresif.

Semoga dalam rangka memperingati 44 tahun 30 September  1965 ini segenap kekuatan demokratis atau kekuatan kiri dan pendukung gagasan-gagasan revolusioner Bung Karno pada umumnya kiranya bisa menyumbangkan fikiran-fikiran atau melakukan berbagai kegiatan untuk melanjutkan dan mengembangkan kekuatan progresif. Sebab, pengalaman lebih dari 10 tahun sejak jatuhnya Suharto menunjukkan dengan jelas sekali bahwa masih terpecah-belahnya kekuatan progresif atau ter-fragmentasinya kekuatan kiri  (walaupun hanya bersifat sementara dan pastilah tidak akan selama-lamanya) menghambat terwujudnya politik alternatif yang solid untuk mengadakan perubahan-perubahan besar dan fundamental bagi kepentingan rakyat banyak.

Sejarah perkembangan dunia sudah menunjukkan bahwa fikiran-fikiran progresif atau gagasan-gagasan revolusionerlah yang sudah bisa mendorong terjadinya perubahan-perubahan besar dan fundamental di berbagai negeri, dan bukannya fikiran atau gagasan yang reaksioner, yang pro-kapitalisme atau neo-liberalisme sekarang ini. Demikian jugalah kiranya di Indonesia. Tidak akan ada perubahan-perubahan besar dan fundamental di negeri ini selama fikiran-fikiran progresif atau ajaran-ajaran revolusioner yang membela kepentingan orang banyak dijauhi bahkan dimusuhi, seperti yang terjadi selama beberapa puluh tahun ini.

Mengingat itu semua, adalah sangat penting bagi kekuatan demokratis di Indonesia, terutama kaum kiri dan para pengikut ajaran-ajaran Bung Karno untuk menjadikan peringatan 30 September 1965 setiap tahun sebagai kesempatan yang baik sekali untuk bersama-sama menggalang atau mendorong berkembangnya kekuatan progresif atau kiri atau revolusioner.

Peringatan 30 September 1965 bisa kita jadikan sebagai sumber inspirasi untuk melanjutkan gagasan-gagasan revolusioner Bung Karno., dan sekaligus juga menjadikan kesempatan yang baik untuk meng-ekpose lebih lanjut segala kejahatan yang telah dibikin rejim militer Suharto beserta sisa-sisa Orde Baru yang masih terus membikin kerusakan-kerusakan sampai sekarang ini.
