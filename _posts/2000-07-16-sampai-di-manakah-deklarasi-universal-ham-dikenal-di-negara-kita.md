---
layout: post
title: Sampai di manakah Deklarasi Universal HAM dikenal di negara kita?
date: 2000-07-16
---

Pertanyaan yang terkandung dalam judul tulisan ini memang bisa menimbulkan keprihatinan yang mendalam bagi kita bersama. Kalau sama-sama kita amati apa yang sedang terjadi dewasa ini di negara kita, maka memang kita bisa bertanya-tanya : apakah Deklarasi Universal HAM PBB itu sudah dikenal secara luas di negara kita? Sebab, kalau kita sudah menyimak kembali dokumen internasional yang penting ini (teks lengkapnya ada di bawah), maka mungkin kita bisa terperanjat atas keterbelakangan pola berfikir yang masih dianut oleh banyak orang di Indonesia dewasa ini.

Untuk bisa melihat gejala-gejalanya secara lebih jelas, marilah sama-sama kita cermati ucapan dan tindakan para tokoh penting negeri kita, baik para menteri, anggota DPR/MPR, pimpinan partai politik, pejabat tinggi pemerintahan, pimpinan militer, tokoh di kalangan agama, di kalangan intelektual, di kalangan media massa, mau pun dalam masyarakat. Kemudian, sudilah kiranya membandingkannya, atau mengukurnya, atau menghubungkannya dengan teks dan jiwa Deklarasi Universal HAM. Maka, kita bisa menarik berbagai kesimpulan bahwa, umpamanya, dan antara lain :

1. Deklarasi Universal HAM (DUHAM) belum begitu dikenal secara baik oleh para tokoh kita.
2. Mereka sudah pernah membaca, tetapi sudah dilupakan saja.
3. Mereka tidak lupa DUHAM, tetapi tidak mempedulikannya.
4. Hati-nurani mereka sudah begitu dikotori oleh macam-macam fikiran yang kurang beradab, sehingga tidak bisa membaca arti dan jiwa DUHAM.
5. Tingkat kebudayaan mereka yang masih belum cukup.

Mengingat tingkah-laku para tokoh di berbagai bidang dewasa ini, yang berkaitan dengan situasi negeri kita di bidang politik, sosial, ekonomi dan moral, maka sudah sepantasnyalah kalau kita beramai-ramai mengingatkan _ dan memperingatkan !!! _ mereka, dan juga kita semua, bahwa tidak mungkin ada solusi (pemecahan) terhadap berbagai persoalan gawat yang sedang kita hadapi bersama, kalau fikiran dan tindakan mereka (baca : kita juga) bertentangan dengan prinsip-prinsip Deklarasi Universal HAM. Dokumen internasional ini penting, bahkan makin terus menjadi lebih penting sekarang, dalam mengurusi persoalan ummat manusia di dunia (termasuk di Indonesia). Dan, karenanya, banyak ulasan atau penelaahan, yang bisa sama-sama kita lakukan mengenai persoalan ini. Tetapi, untuk kali ini, perlulah kiranya kita simak kembali dokumen ini, untuk sekedar menyegarkan ingatan kita bersama. Lengkapnya, teks itu berbunyi sebagai berikut (masih tanpa mukadimah, yang akan disusulkan pada kesempatan lain) :

## Deklarasi Universal hak asasi manusia

Artikel 1 Semua mahluk manusia dilahirkan secara bebas dan memiliki martabat dan hak yang sama. Mereka mempunyai kenalaran (reason) dan kesedaran (conscience) dan kewajiban untuk bertindak antara yang satu dan lainnya dalam semangat persaudaraan (in a spirit of brotherhood)..

Artikel 2 Semua orang berhak untuk memiliki hak dan kebebasan seperti yang dicantumkan dalam Deklarasi ini, tanpa perbedaan apa pun dalam hal ras, warna kulit, kelamin, bahasa, agama, opini politik atau pun opini lainnya, asal kebangsaan atau asal sosial, perbedaan kekayaan, kelahiran atau status lainnya. Lebih lagi, tidak diperbolehkan adanya pembedaan (no distinction shall be made) yang didasarkan atas status politik, juridis atau international negara atau teritori (wilayah) di mana ia menjadi warganegaranya, tanpa mempedulikan apakah negara itu merdeka, di bawah pengawasan, tidak otonom atau berada dalam kedaulatan yang terbatas.

Artikel 3 Semua individu berhak untuk hidup, untuk menikmati kebebasan dan keamanan bagi pribadinya . Artikel 4 Tidak seorangpun boleh diperlakukan dalam perbudakan (slavery) atau dalam perhambaan (servitude) dan perdagangan budak (slave trade) dilarang dalam segala bentuknya.

Artikel 5 Tidak seorang pun boleh disiksa (torture) atau mendapat hukuman dan perlakuan yang kejam, tidak berperikemanusiaan dan merendahkan martabat manusia (cruel, inhuman or degrading treatment).

Artikel 6 Dimana pun, semua orang berhak untuk mendapat pengakuan sebagai seseorang di depan hukum (recognition everywhere as a person before the law).

Artikel 7 Semua orang mempunyai kedudukan yang sama di depan hukum, dan tanpa kecuali berhak untuk mendapat perlindungan hukum yang sama. Semua orang berhak untuk mendapat perlindungan terhadap diskriminasi apa pun yang melanggar Deklarasi ini dan juga terhadap semua hasutan yang menganjurkan diskriminasi itu (against any incitement to such discrimination).

Artikel 8 Setiap orang berhak untuk mengadukan kepada pengadilan nasional yang kompeten semua pelanggaran terhadap hak asasinya yang dijamin oleh Konstitusi atau undang-undang.

Artikel 9 Seorang pun tidak boleh secara sewenang-wenang ditangkap, ditahan atau di-exilkan (arbitrary arrest, detention or exile).

Artikel 10 Semua orang berhak, dalam kedudukan yang sama, untuk menuntut agar urusannya bisa diperiksa secara adil dan secara terbuka oleh pengadilan yang bebas dan imparsial (tidak memihak) untuk menentukan hak dan kewajibannya, atau memeriksa semua dakwaan pelanggaran kriminal (any criminal charge) yang ditujukan kepadanya.

Artikel 11 1. Setiap orang yang dituduh melakukan tindakan pidana haruslah dianggap tidak bersalah sampai kesalahannya itu bisa dibuktikan secara hukum oleh pengadilan terbuka di mana ia mempunyai semua jaminan yang dibutuhkan bagi pembelaannya (all the guarantees necessary for his defence). 2. Tidak seorang pun boleh dihukum akibat suatu tindakan atau ketidaksengajaan (kealpaan, omission) yang pada waktu kejadian itu, menurut hukum nasional atau internasional, tidaklah merupakan tindakan pidana. Demikian juga, hukuman yang lebih berat tidak boleh dijatuhkan ketimbang hukuman yang berlaku pada waktu pelanggaran itu diperbuat.

Artikel 12 Tidak seorangpun boleh secara sewenang-wenang diganggu (arbitrary interference with his privacy) kehidupan pribadinya, keluarganya, rumah tinggalnya atau surat-menyuratnya, dan dilanggar kehormatannya atau nama-baiknya (reputation). Semua orang mempunyai hak atas perlindungan hukum terhadap gangguan atau pelanggaran semacam itu.

Artikel 13 1. Di dalam wilayah suatu negeri, semua orang berhak untuk bersikulasi secara bebas (freedom of movement) atau menentukan tempat tinggal menurut pilihannya. 2. Semua orang berhak untuk meninggalkan setiap negeri, termasuk negerinya sendiri dan untuk kembali kenegerinya sendiri (to leave any country, including his own, and to return to his country).

Artikel 14 1. Menghadapi suatu persekusi, semua orang berhak untuk mencari tempat perlindungan (asylum) dan mendapatkan asylum dari negeri lain. 2. Hak ini tidak boleh dituntut dalam hal pengusutan terhadap kejahatan yang benar-benar berdasar kriminal (non-political crimes) atau terhadap tindakan- tindakan yang bertentangan dengan tujuan dan prinsip PBB.

Artikel 15 1. Semua orang mempunyai hak atas kewarganegaraan (nationality) 2. Tidak seorangpun bisa dicabut kewarganegaraannya secara sewenang-wenang, atau dilarang haknya untuk merobah kewarnegaraannya.

Artikel 16 1. Semua orang, laki-laki maupun perempuan dewasa, tanpa pembatasan ras, kebangsaan atau agama, berhak untuk kawin dan membentuk rumahtangga. Mereka mempunyai hak yang sama mengenai masalah perkawinan, selama perkawinan dan ketika perceraian. 2. Perkawinan hanyalah dapat dilaksanakan dengan persetujuan bebas dan penuh (free and full consent) antara calon suami-istri. 3. Keluarga adalah kelompok (group unit) masyarakat yang alamiah dan fondamental dan berhak atas perlindungan oleh masyarakat dan negara.

Artikel 17 1. Semua orang berhak memiliki harta-benda, baik sendiri-sendiri maupun secara bersama-sama (in association with others). 2. Tidak seorangpun boleh dirampas harta-bendanya secara sewenang-wenang.

Artikel 18 Semua orang berhak untuk mempunyai kebebasan fikiran, keyakinan dan agama (freedom of thought, conscience and religion). Hak ini mencakup kebebasan untuk mengganti agama atau kepercayaannya, dan kebebasan untuk secara sendirian atau bersama-sama dengan orang lain, baik di depan umum maupun di tempat tersendiri (private) memanifestasikan agamanya atau kepercayaannya lewat pendidikan, praktek, sembahyang dan upacara (worship and observance).

Artikel 19 Semua orang mempunyai hak atas kebebasan berfikir dan menyatakan pendapat (the right to freedom of opinion and expression); hak ini mencakup kebebasan untuk mempunyai pendapat tanpa mendapat gangguan (to hold opinions without interference) dan kebebasan untuk mencari, memperoleh dan menyebarkan informasi dan gagasan (to seek, receive and impart information and ideas), lewat media yang manapun dan tanpa memandang perbatasan negara.

Artikel 20 1. Semua orang mempunyai hak untuk menyelenggarakan rapat atau perkumpulan yang bertujuan damai ((peaceful assembly and association). 2. Tidak seorang pun boleh dipaksa untuk menjadi anggota sesuatu perkumpulan.

Artikel 21 1. Semua orang berhak untuk ambil bagian dalam pemerintahan negerinya, secara langsung atau lewat perwakilannya yang dipilih secara bebas. 2. Setiap orang berhak untuk mendapat akses yang sama pada jabatan pemerintahan negerinya (equal acces to public service in his country). 3. Kehendak rakyat haruslah menjadi landasan bagi otoritas pemerintah: kehendak rakyat ini haruslah dinyatakan oleh pemilihan umum secara periodik dan jujur, lewat pemungutan suara secara universal dan setara (by universal and equal suffrage) dan diselenggarakan dengan suara rahasia (by secret vote) atau dengan prosedur pemungutan suara secara bebas lainnya

Artikel 22 Setiap orang, sebagai anggota masyarakat, mempunyai hak jaminan sosial (his right to social security), dan mendapat bagian dari realisasi, lewat usaha nasional dan kerjasama internasional dan sesuai dengan pengaturan dan kemampuan setiap negaranya, atas hak ekonomi, sosial dan kebudayaan yang sangat dibutuhkan bagi martabatnya dan pengembangan kepribadiannya secara bebas (indispensable for his dignity and the free development of his personality).

Artikel 23 1. Setiap orang mempunyai hak untuk bekerja, untuk menentukan pilihan pekerjaannya secara bebas, untuk bekerja dengan syarat-syarat yang adil dan mendapat perlindungan dari bahaya pengangguran. 2. Setiap orang, tanpa diskriminasi apa pun, berhak untuk menerima upah yang sama untuk pekerjaan yang sama. 3. Setiap orang yang bekerja mempunyai hak untuk menerima upah yang adil dan menguntungkan untuk memberikan jaminan baginya sendiri dan keluarganya atas kehidupan yang sesuai dengan martabat manusia, dan ditambah, kalau perlu, dengan cara-cara proteksi sosial lainnya. 4. Setiap orang mempunyai hak untuk membentuk serikat-buruh atau bergabung di dalamnya (to form and to join trade unions) demi melindungi kepentingannya.

Artikel 24 Setiap orang mempunyai hak untuk mendapat istirahat dan hiburan, termasuk dibatasinya jam kerja dan mendapat hari libur yang dibayar, menurut batas-batas yang masuk akal.

Artikel 25 1. Setiap orang mempunyai hak atas standar hidup yang memadai bagi kesehatan dirinya dan keluarganya, termasuk makan, pakaian, perumahan, pengobatan, dan pelayanan sosial, dan atas jaminan dalam menghadapi pengangguran, sakit, cacad, kematian suami atau istri (widowhood), hari-tua, atau menghadapi situasi kehidupan sulit yang di luar kemauannya. 2. Masa keibuan (motherhood) dan masa kekanakan (childhood) berhak untuk mendapatkan pertolongan dan bantuan khusus. Semua anak yang lahir, baik yang lahir dalam perkawinan atau di luarnya, harus menerima proteksi sosial yang sama (same social protection).

Artikel 26 1. Setiap orang mempunyai hak atas pendidikan. Pendidikan haruslah bebas beaya, setidak- tidaknya bagi pendidikan tahap elementer (elementary stage) dan dasar. Pendidikan dasar haruslah wajib. Pendidikan teknik dan kejuruan (professional) haruslah tersedia untuk umum dan pendidikan tinggi harus terbuka bagi semua dengan hak yang sama berdasarkan merit masing-masing. 2. Pendidikan harus diarahkan untuk pengembangan sepenuhnya kepribadian seseorang sebagai manusia (full development of the human personality) dan untuk memperkokoh dihargainya hak-hak manusia dan kebebasan-kebebasan dasar (fundamental freedoms). Pendidikan ini harus mempromosikan saling pengertian, toleransi dan persahabatan antara semua bangsa, grup sosial atau agama, dan memperkuat aktivitas PBB untuk mempertahankan perdamaian. 3. Orang tua anak mempunyai hak yang utama (prior right) untuk memilih jenis pendidikan yang harus diberikan kepada anak mereka.

Artikel 27 1. Setiap orang berhak untuk berpartisipasi secara bebas dalam kehidupan kebudayaan masyarakatnya, menikmati kesenian dan memperoleh bagian dari kemajuan ilmu beserta hasil-hasilnya. 2. Setiap orang mempunyai hak untuk mendapat perlindungan atas kepentingan moral atau material yang dilahirkan oleh produk ilmiah, literer atau artistik yang diciptakannya.

Artikel 28 Setiap orang mempunyai hak atas adanya orde sosial dan internasional, di mana hak-hak dan kebebasan-kebebasan yang dicantumkan dalam Deklarasi ini dapat direalisasi secara sepenuhnya.

Artikel 29 1. Setiap orang mempunyai kewajiban terhadap masyarakatnya di mana dimungkinkan pengembangan kepribadiannya secara bebas dan sepenuhnya. 2. Dalam mempertahankan hak-hak dan kebebasan-kebebasannya, setiap orang harus dikenakan pembatasan oleh undang-undang yang tujuannya adalah semata-mata untuk mengakui dan menghormati secara selayaknya hak dan kebebasan orang lain dan untuk memenuhi tuntutan moral, ketertiban umum dan kesejahteraan bersama dalam suatu masyarakat demokratis. 3. Hak-hak dan kebebasan-kebebasan ini tidak dapat, bagaimana pun juga, dijalankan secara berlawanan dengan tujuan dan prinsip-prinsip PBB.

Artikel 30 Tidak ada satu pun bagian Deklarasi ini bisa diartikan oleh suatu negara, grup atau perseorangan, sebagai hak untuk melakukan kegiatan apa pun atau melancarkan tindakan apa pun yang bertujuan untuk menghancurkan semua hak-hak dan kebebasan yang dicantumkan di dalamnya.

* * *

(PS. Teks dokumen, yang terdiri dari 30 artikel, habis di sini. Diterjemahkan dari teks resmi PBB dalam bahasa Inggris dan dibandingkan dengan teks bahasa Prancis. Mengharapkan bantuan untuk men-cek dengan teks standard dalam bahasa Indonesia, yang sudah digunakan secara resmi oleh lembaga-lembaga kita, dan mengirimkan koreksiannya kepada penulis)
