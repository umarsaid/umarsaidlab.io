---
layout: post
title: Lembaga-lembaga terkorup di Indonesia
date: 2007-12-07
---
Hasil penelitian yang dilakukan Transparency Internasional Indonesia (TII) mengenai korupsi di negeri kita makin meyakinkan kita bahwa kebejatan moral atau kerusakan akhlak sudah melanda secara ganas dan juga meluas sekali di kalangan sebagian besar aparat dan lembaga negara. Menurut hasil penelitian tersebut, institusi kepolisian, parlemen, lembaga peradilan, dan partai politik menduduki peringkat teratas sebagai lembaga terkorup di Indonesia (Kompas, 6 Desember 2007).


Membaca berita tersebut di atas, kita semua bisa geleng-geleng kepala, dan bertanya-tanya :”Sudah begitu parahkah sakitnya bangsa kita ?” Betapa tidak ! Sebab, menurut penelitian tersebut, institusi kepolisian menduduki peringkat teratas sebagai lembaga terkorup di Indonesia. Demikian juga parlemen dan lembaga peradilan. Demikian juga partai-partai politik. Kalau sudah begini, lalu apa saja di negeri kita yang masih bersih dari korupsi?


Kita tahu sendiri, bahwa selama ini koran dan televisi di Indonesia juga sering sekali menyiarkan banyaknya pejabat di kalangan eksekutif yang tersangkut korupsi. Termasuk adanya menteri atau mantan menteri (bahkan menteri agama!!!), gubernur, bupati, walikota yang mencuri kekayaan negara atau uang rakyat. Yang agak jarang terdengar adalah adanya korupsi di kalangan TNI. Dan itu pun tidak berarti bahwa tidak ada korupsi atau penyelewengan di kalangan TNI, melainkan karena ketatnya “pembungkusan” informasi, sehingga jarang sekali bocor.


Parahnya wabah korupsi yang sudah begitu meluas ini, mencerminkan rusaknya sebagian besar akhlak atau merosotnya moral bangsa.Dan kerusakan moral atau kebejatan akhlak ini sudah begitu meluas, sehingga “budaya korupsi” sudah tidak membikin malu atau takut bagi banyak orang. Demikian buruknya moral ini sehingga korupsi atau mencuri harta publik sudah dianggap normal saja, atau hal yang dianggap wajar dan “lumrah” saja. Mengapa negeri kita jadi begini rusak? Siapakah yang harus bertanggung jawab? Apa saja yang menyebabkan kerusakan moral atau kebejatan akhlak ini?


Banyak orang yang masih ingat bahwa selama pemerintahan dipimpin oleh Bung Karno sampai 1965, korupsi adalah sedikit sekali kalau dibandingkan dengan sekarang ini.Walaupun waktu itu keadaan sulit, karena ekonomi diboikot oleh kekuatan asing dan dirongrong oleh berbagai pergolakan dalam negeri (pemberontakan PRRI-Permesta, DI-TII, peristiwa tiga daerah, konfrontasi dengan Belanda mengenai Irian Barat dll dll) tetapi rakyat banyak masih menyokong berbagai politik Presiden Sukarno. Suasana perjuangan dan mengabdi kepentingan rakyat secara gotong-royong membikin negara dan bangsa Indonesia terkenal di dunia sebagai bangsa yang revolusioner.


Tetapi, setelah Suharto dkk membangun rejim militer Orde Baru, kerusakan moral atau kebejatan akhlak di kalangan elite Indonesia menjadi ciri utama bangsa, sehingga korupsi merajalela secara ganas. Hal yang menyedihkan begini ini dianggap “lumrah” saja karena Suharto sebagai pimpinan tertinggi negara sendiri sudah menjadi maling terbesar di seluruh dunia, dan tidak diapa-apakan. Banyaknya berita tentang urusan harta yang diperoleh oleh keluarganya (istri dan anak-anaknya, antara lain Tommy dan Tutut) secara haram, menjadi contoh yang jelek bagi banyak orang.


Seperti sudah sama-sama kita saksikan kerusakan moral atau kebejatan akhlak ini adalah produk utama pemerintahan Orde Baru, yang diwariskan kepada bangsa kita. Dan kerusakan moral inilah yang dipertahankan, bahkan dikembangkan oleh kekuatan sisa-sisa Orde Baru. Sebab, justru dengan kerusakan moral bangsa inilah (atau berkat kebejatan akhlak inilah)) mereka dapat juga dengan mudah menumpuk kekayaan dengan cara haram pula. Jadi, dengan merajalelanya korupsi di seluruh aparat dan lembaga negara, mereka malah diuntungkan. Karena itu, pada dasarnya, mereka tidak berkepentingan akan terberantasnya korupsi. Mereka malah dirugikan, kalau pemberantasan korupsi berjalan dengan sungguh-sungguh dan efektif.


Jadi, jelaslah bahwa pemberantasan korupsi tidak akan bisa berjalan dengan sukses, selama sisa-sisa kekuatan Orde Baru masih menguasai alat kepolisian, parlemen, lembaga peradilan, dan partai-partai politik. Sebab, di kalangan mereka-mereka itulah yang banyak melakukan korupsi dan kolusi. Pemberantasan korupsi secara tuntas hanya mungkin kalau kekuasaan politik sudah dapat direbut oleh kekuatan-kekuatan demokratis dan pro-rakyat. Kita semua harus membuang ilusi bahwa reformasi bisa berjalan dan korupsi bisa sungguh-sungguh diberantas tuntas dengan orang-orang lama yang sekarang menduduki tempat-tempat penting di berbagai lembaga negara kita. Kita memerlukan wajah-wajah baru, yang mempunyai komitmen tegas pro-rakyat, dan yang betul-betul anti Orde Baru.


Sejarah dan waktu akan membuktikan kepada kita semua, bahwa perubahan-perubahan besar di negeri kita untuk kesejahteraan rakyat banyak tidak akan bisa dilakukan oleh orang-orang yang bermental Orde Baru.


Paris, 7 Desember 2007
*** *** ***


Lampiran : berita Kompas 6 Desember 2007 :


.” Institusi kepolisian, parlemen, lembaga peradilan, dan partai politik menduduki peringkat teratas sebagai lembaga terkorup di Indonesia. Demikian hasil penelitian yang dilakukan Transparency International Indonesia (TII).


Dalam pengumuman TII yang disiarkan Kamis (6/12), pada acara Peluncuran Barometer Korupsi Global 2007, kepolisian mendapatkan nilai indeks korup 4,2, disusul lembaga peradilan dan parlemen dengan angka 4,1, dan partai politik, yang meraih indeks 4,0.


Penelitian yang dilakukan terhadap 1010 responden di tiga kota besar di Indonesia yaitu Jakarta, Surabaya dan Bandung ini juga mendapati bahwa 59 persen responden yakin akan terjadi peningkatan korupsi, 22 persen yakin tingkat korupsi akan turun, 18 persen yakin tidak akan ada perubahan sama sekali.
Mengenai hasil penelitian yang telah dilakukan untuk ke lima kalinya ini, Ketua TII todung Mulya Lubis menilainya sebagai sangat mengecewakan. "Hasil penelitian ini menunjukkan adanya mosi tidak percaya masyarakat pada lembaga-lembaga penegak hukum, parpol dan DPR," ujar Tobing.
Oleh karenanya TII mendesak KPK agar lebih serius menangani kasus-kasus korupsi yang terjadi di lingkungan penegak hukum khususnya kepolisian dan kejaksaan.


Selain itu pemerintah juga dituntut agar mempercepat proses reformasi demokrasi di institusi penegak hukum, meminta Presiden secara aktif memantau pelaksaan rencana aksi pemberantasan korupsi, serta mengintensifkan kembali fungsi pengawasan dalam konteks pemberantasan korupsi. (kutipan dari Kompas selesai)
