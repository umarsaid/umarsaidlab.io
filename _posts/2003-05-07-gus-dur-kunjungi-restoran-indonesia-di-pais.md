---
layout: post
title: Gus Dur kunjungi restoran INDONESIA di Pais
date: 2003-05-07
---

Bagi keluarga besar Restoran INDONESIA di Paris tanggal 5 Mei 2003 merupakan hari yang penting untuk dijadikan kenang-kenangan. Pada hari itu Gus Dur, mantan Presiden RI, memerlukan datang uutuk berkunjung ke Restoran INDONESIA dan bertemu dengan orang-orang Indonesia yang berhimpun sekitar restoran koperasi ini. Gus Dur datang ke Paris untuk suatu pertemuan internasional yang diadakan di gedung Unesco mulai tanggal 5 Mei sampai dengan tanggal 9 Mei, dan diselenggarakan oleh « Budhist-Muslim Dialogue Conference, Global Ethic and Good Governance »

Bahwa Gus Dur memerlukan datang ke Restoran INDONESIA pada hari pertama konferensi dan sesudah ia mengucapkan keynote speechnya (pidato utama konferensi) adalah sesuatu yang mempunyai arti penting tersendiri. Gus Dur mengenal sejarah restoran Indonesia, dan pada tahun 1995 ia berkali-kali sudah mengunjunginya. Ia tahu bahwa restoran ini didirikan oleh orang-orang yang terpaksa “klayaban” di luarnegeri karena dilarang pulang ke tanah-air oleh rezim militer Suharto. Waktu itu ia mengunjungi restoran bersama teman-teman terdekatnya (antara lain : M. A.S. Hikam, Sobari).

## Sebagai Mantan Presiden Ri

Gus Dur berkunjung ke restoran INDONESIA dalam tahun 1995 ketika rezim militer Suharto masih jaya-jayanya dan ketika restoran INDONESIA masih diboikot oleh KBRI dan Deplu. Peristiwa ini merupakan sikap politik yang berani dan sikap moral yang penting waktu itu, sebagai tokoh utama Nahdatul Ulama. Kunjungan yang kali ini, yang dilakukannya sebagai mantan Presiden RI, mempunyai dimensi lain.

Karena, sejak sebelum meninggalkan Jakarta, ia sudah menyampaikan pesan lewat Andree Feillard, seorang expert Prancis soal-soal Islam di Indonesia, bahwa di Paris ia pasti akan bertemu dengan teman-teman Indonesia yang bekerja di restoran INDONESIA. Sudah tentu berita ini diterima dengan gembira oleh semua orang yang terhimpun sekitar restoran. Karena, ini menunjukkan bahwa Gus Dur, yang pernah menjadi presiden RI, tetap menaruh perhatian dan juga bersikap bersahabat dengan orang-orang yang pernah dimusuhi oleh rezim militer Suharto ini.

Itu sebabnya, pada tanggal 4 Mei, Ibaruri bersama penulis ikut menjemput kedatangan Gus Dur beserta rombongan (seluruhnya terdiri 5 orang, termasuk istrinya) di airport Roissy Charles de Gaule bersama-sama dengan staf KBRI dan staf perwakilan Indonesia di Unesco. Dalam kunjungannya ke Paris untuk pertemuan internasional ini Gus Dur, sebagai mantan Presiden RI, masih mendapat pelayanan dan penghormatan yang selayaknya dari KBRI dan perwakilan Indonesia di Unesco. Dubes RI untuk Prancis Adian Silalahi, dan Dubes RI untuk Unesco Bambang Suhendro berturut-turut menyelenggarakan jamuan makan malam bagi Gus Dur beserta rombongan.

## Kunjungan Yang Mengandung Pesan

Jadi, bisalah diartikan bahwa kunjungan Gus Dur ke restoran INDONESIA di Paris bukannya sekedar untuk makan-makan saja. Hasratnya untuk bertemu dengan teman-teman yang bekerja di restoran INDONESIA kelihatan juga ketika di Flathotel diadakan pembicaraan tentang pengaturan jadwal. Staf perwakilan Indonesia di Unesco dan staf KBRI cukup sibuk mengatur jadwal yang terlalu padat. Tetapi Gus Dur akhirnya memutuskan untuk pergi ke restoran INDONESIA sesudah ia mengucapkan keynote speechnya (pidato yang utama dalam konferensi).

Dengan kunjungannya ke restoran INDONESIA ini kelihatannya Gus Dur, sebagai tokoh penting ummat Islam, ingin menunjukkan kepada umum sikapnya yang mengandung pesan politik dan moral. Ini tercermin dalam tegur-sapanya yang terasa hangat terhadap Ibaruri, J.J. Kusni dan penulis sendiri, dan dari isi pembicaraannya dalam pertemuan dengan teman-teman di restoran.

Isi pembicaraan dalam pertemuan itu sendiri menyangkut soal-soal penting dan secara “blak-blakan” (terus terang), tetapi Gus Dur tetap berpendapat bahwa “yang penting kita sudah bertemu”.

## Dari Soal Inul Sampai Soal Pemilu

Dalam pertemuan yang berlangsung lebih dari satu jam itu Gus Dur menguraikan pendapatnya tentang situasi di tanah-air dan menjawab pertanyaan-pertanyaan yang diajukan para teman yang berkaitan dengan Pemilu, PKB, TNI, korupsi, masalah Inul Telah diangkat juga dalam pertemuan itu masalah TAP MPR nomor 25/1966 dan diajukannya masalah korban ’65 di Jenewa oleh delegasinya Dr Tjiptaning, Ir Setiadi dan Heru Atmodjo.

Tentang situasi di tanah-air Gus Dur antara lain menyampaikan kekuatiran fihak tertentu akan terjadinya kekacauan yang disebabkan oleh timbulnya revolusi sosial. Dalam menghadapi situasi yang demikian itu, Gus Dur memberi peringatan kepada fihak militer yang ingin mengadakan perebutan kekuasaan lewat senjata bahwa ia akan ikut bertindak untuk menggagalkan perebutan kekuasaan ini.

Tentang Pëmilu yang akan datang ia optimis sekali bahwa PKB akan memperoleh suara yang cukup banyak. Ini tercermin dalam rapat-rapat umum yang diadakan selama ini di berbagai daerah, yang umumnya dihadiri oleh tidak kurang dari 50.000 orang. Bahkan ada rapat umum yang dihadiri oleh 300.000 orang. Menjawab pertanyaan seorang teman, Gus Dur menyatakan PKB terbuka buat semua orang, termasuk orang Katolik. Bahkan ada pimpinan PKB di daerah-daerah yang beragama Katolik.

## Pandangan Yang Humanis

Pandangan humanis Gus Dur kelihatan ketika ia memberikan komentar soal Inul yang dikemukakan oleh seorang teman. Ia menjelaskan bahwa ia tidak setuju dengan adanya larangan-larangan yang datang dari perseorangan terhadap kegiatan semacam ini. Yang berhak melarang adalah Mahkamah Agung.

Seorang peserta pertemuan mengangkat masalah korban ’65 dengan mengambil contoh anak mantan brigjen Supardjo (yang dihukum mati karena peristiwa G30S) yang Gus Dur ikut menyelesaikan masalah perkawinannya. Anak mantan brigjen Supardjo ini kawin dengan anak mantan Menteri Agama jaman kabinet Presiden Sukarno, Kyai Haji Saefudin Zuhri dari NU. Mengenai soal ini, Gus Dur mengomentarinya dengan enteng :” Mereka dua-duanya sudah suka sama suka, mau diapain lagi ?”

Ketika ia mendengar bahwa restoran kita baru merayakan hari ulangtahunnya yang ke-20, Gus Dur menyatakan bahwa tidak menyangka bahwa restoran INDONESIA sudah begitu panjang umurnya.

Itulah sekelumit cerita tentang kunjungan Gus Dur ke restoran INDONESIA di Paris. Suatu peristiwa yang mengandung message (pesan) kepada ummat Islam dan siapa saja yang mendambakan toleransi dan peri-kemanusiaan.
