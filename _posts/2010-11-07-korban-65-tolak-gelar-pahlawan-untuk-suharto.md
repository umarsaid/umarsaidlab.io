---
layout: post
title: Korban 65 Tolak Gelar Pahlawan untuk Suharto
date: 2010-11-07
---

S E R U A N

Jakarta 07 November 2010   

Dengan ini diserukan  kepada Kawan-kawan Korban, Keluarga Korban  dan
Organisasi-Organisasi Korban Tragedi Kemanusiaan 1965/1966 serta Kawan-Kawan NGO dan simpatisan Korban  yang peduli terhadap  perjuangan penegakan HAM, untuk bisa hadir dalam  acara Pengiriman Surat Petisi  Penolakan  Suharto sebagai Pahlawan Nasional ke pada Presiden Republik Indonesia .

Pada Hari/Tanggal: Senen 08 November 2010
Pukul: 11.00 WIB
Tempat: Depan Gerbang Gedung Sekretariat Negara RI, Jl. Veteran No.17 Jakarta.

Atas perhatiannya, diucapkan banyak terima kasih.

Salam hangat,

Bedjo Untung
Ketua YPKP 65

* * *


PERNYATAAN BERSAMA KORBAN, KELUARGA KORBAN, ORGANISASI KORBAN
TRAGEDI KEMANUSIAAN 1965/1966




MENOLAK DENGAN TEGAS UPAYA PEMBERIAN GELAR PAHLAWAN NASIONAL KEPADA MANTAN PRESIDEN RI JENDERAL  SUHARTO

Dengan ini kami para Korban, Keluarga Korban  dan Organisasi-Organisasi Korban Tragedi Kemanusiaan 1965/1966  baik yang tinggal di Dalam maupun Luar Negeri sebagai akibat tindakan represif rejim militeristik Suharto yang berkuasa sejak 1966-1998 di mana jumlah Korbannya tidak kurang dari 20 juta jiwa, menyatakan:
Ada pun yang menjadi dasar  pertimbangan  alasan  penolakan  ialah sebagai berikut:

Pertama, Suharto telah melakukan kejahatan pelanggaran konstitusi, yaitu  pengkhianatan  terhadap  falsafah Pancasila dan UUD 1945, yaitu dengan tindakannya melakukan perebutan kekuasaan secara merangkak (creeping coup d’etat) atas Presiden RI pertama yang sah Bung Karno. Kemudian, Suharto melakukan serangkaian tindakan yang kontra revolusioner, menjadikan Indonesia tidak lagi menjalankan politik yang bebas aktif melainkan lebih berfihak kepada kepentingan imperialisme, neokolonialisme.

Suharto telah mengkhianati  SP 11 Maret 1966 yaitu tidak melindungi ajaran Pemimpin Besar Revolusi untuk melanjutkan perjuangan anti imperialisme, melainkan membawa Indonesia menjadi negara yang berpihak kepada kepentingan kapitalisme dan imperialisme, dengan mengundang para investor asing menjarah kekayaan bumi Indonesia.

Kedua, selama periode kepemimpinannya sebagai Presiden Republik Indonesia pada 1966-1978, Suharto telah melakukan serentetan kejahatan pelanggaran HAM berat, yaitu antara lain, genocida,  pembunuhan massal atas  3 juta putra-putri terbaik bangsa Indonesia pada Tragedi Kemanusiaan 1965/1966. Jutaan rakyat telah ditangkapi, disiksa, dibuang,ditahan dan dibunuh tanpa melalui proses hukum. Harta benda korban dirampas, dimiliki tanpa hak. Aturan hukum dan perundang-undangan diskriminatif  ia ciptakan  untuk melanggengkan kekuasaan. Telah melakukan pelanggaran HAM berupa pencabutan paspor tanpa proses hukum terhadap warga negaranya yang ketika itu  sedang bertugas belajar/bekerja di Luar Negeri.

Tindakan Suharto bisa dikategorikan  sebagai Crimes against Humanity dan untuk mempertanggungjawabkan tindakannya itu, ia  bisa diseret ke Mahkamah Internasional.

Selama berkuasa, Suharto juga orang yang paling bertanggungjawab atas terjadinya pelanggaran HAM berat  DOM di Aceh, Timor Leste, Papua, Kasus Penculikan Aktivis Mahasiswa, Kasus Tanjung Priuk, Kasus  Talangsari Lampung, Pembunuhan Mahasiswa Trisakti, Kerusuhan Mei 1998, Tragedi SemanggiI/II, Penyerbuan Kantor PDI jl. Diponegoro, Jakarta , Penembakan Misterius, Pembunuhan Aktivis HAM Munir serta berbagai kasus pelanggaran ekonomi, sosial, budaya lainnya.

Ketiga, selama Suharto berkuasa ia  telah melakukan serentetan  tindak kejahatan kriminal di bidang ekonomi, yaitu sebagai koruptor terbesar nomor satu di dunia. Menurut laporan Global Stolen Asset Recovery Initiative, United Nations (2005), selama ia berkuasa telah mewariskan kerusakan lingkungan berupa pembabatan hutan  dan  hak  penguasaan hutan untuk para kroni-kroninya. Sumber tambang dan mineral yang semestinya untuk kemakmuran sebesar-besarnya kepada rakyat Indonesia justru diberikan kepada asing (contohnya, tambang emas PT. Free Port yang diberikan kepada pengusaha Amerika Serikat).

Suharto  telah mewariskan hutang yang berjumlah trilyunan rupiah kepada rakyat yang tidak menikmatinya.

Keempat, Suharto adalah orang yang paling bertanggung jawab atas terjadinya krisis multi dimensional yang  hingga kini belum terselesaikan. Kehancuran akhlak, lunturnya patriotisme, nasionalisme. Suharto adalah orang yang harus bertanggung jawab atas terjadinya kehancuran di bidang hukum, politik, ekonomi.  Ia adalah sosok yang menjadikan Indonesia terpuruk baik di dalam negeri mau pun luar negeri. Indonesia tidak lagi menjadi negara yang disegani karena ia lebih dikenal sebagai negara yang melindungi tindak kejahatan korupsi serta negara yang tidak melindungi Hak-Hak Asasi Manusia.

Kelima,  Ketetapan MPR RI No.XI/MPR/1998 tanggal 13 November1998 tentang Penyelenggaraan Negara yang Bersih dan Bebas KKN, masih berlaku, dan pasal 4 berbunyi: “Upaya pemberantasan korupsi, kolusi dan nepotisme harus dilakukan secara tegas terhadap siapa pun juga, baik pejabat negara, mantan pejabat negara, keluarga dan kroninya maupun pihak swasta/konglomerat termasuk mantan presiden Soeharto.” Oleh karena itu upaya menetapkan Suharto sebagai pahlawan nasional bertentangan dengan ketetapan MPR tersebut.

Hal-hal yang tersebut di atas belum pernah dipertanggungjawabkan baik secara politik mau pun hukum  oleh mantan presiden Suharto sampai ia wafat. Namun demikian, tidak berarti kasus  pelanggaran HAM  berat yang ia lakukan, yang ia ikut merekayasa  telah selesai begitu saja. Sampai hari ini para Korban belum memperoleh hak yang ia rampas secara sewenang-wenang, yaitu  hak Pemulihan : Kebenaran, Keadilan dan Rehabilitasi.

Upaya memberi gelar Pahlawan Nasional kepada Suharto sangat menyakiti hati para Korban Pelanggaran HAM. Bagaimana mungkin seorang pembunuh bangsanya sendiri dinobatkan sebagai pahlawan? Ini benar-benar  di luar pemikiran akal sehat. Suharto dikenal sebagai  orang yang licik, penuh kebohongan, kotor  dan menjijikkan. Sama sekali tidak layak sebagai panutan bangsa mau pun  suri tauladan bagi orang lain.

Atas dasar itu, kami para  Korban, Keluarga Korban  dan Organisasi-Organisasi Korban Tragedi Kemanusiaan 1965/1966  baik secara sendiri-sendiri mau pun secara bersama-sama, yang tinggal di dalam maupun luar negeri sebagai akibat tindakan represif rejim militeristik Suharto, mendesak Presiden SBY-Budiono untuk:

Menolak Usulan Pengangkatan Suharto sebagai Pahlawan Nasional,  segera menuntaskan kasus Tragedi Kemanusiaan 1965/1966 dengan memintai pertanggungan jawab kepada partai berkuasa saat itu yaitu Golkar dan Angkatan Darat sebagai pendukung rejim otoriter Orde Baru Suharto yang terus melakukan politik diskriminasi sampai hari ini. Mendesak rezim SBY-Budiono mengeluarkan kebijakan untuk mengungkap kebenaran, menghadirkan keadilan dan memulihkan hak-hak korban dengan menggelar Pengadilan HAM ad hoc  seperti yang diamanatkan UU No 26/2000. Pemerintah SBY-Budiono  harus berani   meminta maaf kepada para korban  atas terjadinya pelanggaran HAM  serta berjanji untuk tidak mengulangi  kejadian serupa di masa mendatang.

Demikian  Surat Pernyataan ini  kami sampaikan. Atas perhatiannya diucapkan banyak terima kasih.


Jakarta, 07 November 2010

Hormat kami,

Bedjo Untung, Ketua YPKP 65
S.Utomo, Ketua  LPRKROB
Sugiri, Korban 65/ LPKP
Eddi Sugiyanto, YPKP 65
Heru Suprapto, YPKP 65
Gustaf Dupe, Korban 65
Lestari, Korban 65
Sri Sulistyowati, Korban 65
Mujayin, LPRKROB
Kusnendar, LPRKROB
Sanuar, YPKP 65 Padang Pariaman
Amir Suripno, YPKP 65
Ait Esa, YPKP 65
Mulyono, SH, LPRKROB
Yoyo, Korban 65
Effendi Saleh, Korban 65
Eddo, YPKP 65
Endang, LPRKROB
Mardjuki, LPKP
John Pakasi, LPKP
Sasmaya, Korban 65
Djadji, Pakorba
Nadiani, YPKP 65  Bukittinggi
Trikoyo Ramidjo, Korban 65
Haryogyo, Seniman Korban 65/YPKP 65
Aad Hidaya, YPKP 65 Kuningan
Asep, YPKP 65 Sukabumi
Udin Muhidin, YPKP 65 Cianjur
MD.Karta Prawira, LPK 65 Belanda
Samin, YPKP 65 Riau
Ngadi Suradi, Balikpapan
Mudlofir, YPKP 65 Pemalang
Supardi, YPKP 65 Pati
Handoyo, YPKP 65 Pati
Wayan Sante, Korban 65  Bali
Putu Oka, Korban 65 Bali
Prayitno, Korban 65 Bali
Adi Wijaya, Yogyakarta
Bangun Kinardi, Korban 65 Boyolali
Supomo, Korban 65 Boyolali
Adon Sutrisno, Korban 65 Kertosono
Put Moeinah, Korban 65 Blitar
Abdul Jalil, Korban 65 Pati
Sumini Martono, Korban 65 Tangerang


Y.T.Taher, Korban 65 Australia


Arif harsana, Korban 65 FEID Jerman

Tom Ilyas, Korban 65 Swedia    

Ibrahim Isa, Korban 65 Belanda      

Umar Said, Korban 65 Perancis         

Heru Atmodjo Korban 65 AURI Jakarta                                                                                        

Bambang Poernomo  Korban 65 mantan militer Temanggung                                                        

Hutomo Korban 65 mantan  staff Kejagung RI                                                                               

A.Rahman  YPKP 65 Bandung                                                                                                                   

Ny. Sulastri    Korban 65 Cilacap                                                                                                              

Umi Siraj   Korban 65 Bekasi                                                                                                                       

Ny. Rasumi M.Thaib Korban 65 Comal Pemalang                                                                            

Murba Tengku Satrio  Korban 65 Pemalang                                                                                          

Eko Wardoyo  YPKP 65 Tangerang                                                                                                   

Slamet   YPKP 65 Lampung                                                                                                                         

St. Sudarno    YPKP 65  Pekalongan                                                                                                            

Bu Muhayati  Korban 65 YPKP 65 Yogyakarta                                                                  
dll 
