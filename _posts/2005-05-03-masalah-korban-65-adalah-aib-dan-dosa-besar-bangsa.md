---
layout: post
title: Masalah korban 65 adalah aib dan dosa besar bangsa
date: 2005-05-03
---

Menurut Tempo Interaktif (26 April 2005), "sekitar 30 orang eks tahanan dan narapidana politik mendatangi Komisi A DPRD Jawa Barat Selasa (26/4) siang. Mereka yang mewakili puluhan ribu tahanan dan narapidana politik lainnya di Jawa Barat meminta dewan agar mendesak pemerintah mencabut semua peraturan yang diskriminatif terhadap eks tahanan dan narapidana politik beserta keluarganya yang diduga terlibat G 30 S.

“Menurut Muyono dari Yayasan Penelitian Korban Peristiwa (YPKP) 1965-1966 Jawa Barat, saat ini Orde Baru sudah tidak berkuasa. Namun para eks tapol dan napol masih mengalami pelanggaran Hak Asasi Manusia. "Misalnya ada tanda khusus ET dalam KTP yang berarti Eks Tapol,"kata Mulyono.

“Akibat dari perlakukan diskriminatif itu, eks tapol dan napol beserta keluarga mereka tidak boleh masuk dalam struktur pemerintahan di tingkat desa atau kelurahan. Mereka pun sulit mendapatkan pekerjaan dan jaminan sosial lainnya. "Sudah saatnya penyiksaan serta pelanggaran terhadap hak kami dihentikan,"ujar Mulyono.

“Menurut Mulyono, sampai saat ini diperkirakan ada sekitar 30 ribu eks tahanan politik yang ada di Jawa Barat. Selain itu, dari data yang dikumpulkan oleh YPKP, ada lebih dari 10 ribu tapol lainnya yang sampai sekarang bermukim di kawasan Subang Jawa Barat dan sekitarnya.”

## Kalangan Lanjut Usia

Sementara itu, puluhan korban pelanggaran hak azasi manusia pada peristiwa di tahun 1965 di kawasan Tanah Karo, Sumatera Utara, pada tanggal 12 April 2005 juga melakukan unjuk rasa di depan Istana Merdeka, Jalan Medan Merdeka Utara, Jakarta Pusat.

Mereka melakukan orasi dan membawa spanduk serta sejumlah poster yang berisi tuntutan mereka. Massa yang terdiri dari kalangan lanjut usia ini menuntut agar Presiden mengadili dan menuntaskan kasus Peristiwa 1965 di Sumatera Utara.

Diantara pelaku yang diinginkan oleh para pengunjuk rasa agar diadili adalah mantan Presiden RI, Soeharto. Massa menganggap bahwa Soeharto merupakan dalang utama dalam peristiwa tersebut.

Menurut mereka, dalam peristiwa itu, ratusan orang di Tanah Karo tewas karena tindak kriminal dan intervensi kepentingan politik yang dilakukan oleh Soeharto. Pasalnya para korban tersebut dituduh terlibat melakukan kudeta dalam Gerakan 30 September 1965. (Elshinta, 12 April 2005)

## Surat Komnas HAM Kepada Presiden SBY

Harian Kompas (21 Februari 2005), dengan judul “Komnas HAM Minta Hak Mantan Tapol PKI Dipulihkan » telah menurunkan berita yang antara lain berbunyi sebagai berikut :

« Komisi Nasional Hak Asasi Manusia (Komnas HAM) mendesak Presiden Susilo Bambang Yudhoyono segera memulihkan hak asasi dan kebebasan mantan tahanan politik yang dikaitkan dengan peristiwa Gerakan 30 September 1965, yang sejak masa Orde Baru hingga saat ini tidak pernah diproses hukum dan dibuktikan bersalah. Presiden juga didesak untuk mencabut atau menghentikan kebijakan, peraturan, atau praktik diskriminatif terhadap mantan tahanan politik (tapol) dan keluarga, yang merupakan pelanggaran dan pengingkaran hak asasi.

Desakan itu disampaikan Ketua Komnas HAM Abdul Hakim Garuda Nusantara melalui surat yang dikirim kepada Presiden Yudhoyono awal Februari 2005. "Kami mendesak Presiden supaya segera mengambil keputusan merehabilitasi dan memberikan kompensasi. Masalah ini sudah berlangsung sekian puluh tahun," ujar Abdul Hakim, Jumat (18/2) di Jakarta.

Ditegaskannya, negara berkewajiban memulihkan hak orang yang menjadi tapol yang dibuang ke Pulau Buru, Salemba, dan sejumlah tempat pembuangan. Sebab, penahanan terhadap orang-orang itu merupakan perbuatan melanggar HAM, apalagi mereka tidak pernah dibuktikan bersalah. Menurut Abdul Hakim, jumlah orang yang menjadi korban tapol tersebut banyak. "Jumlahnya diperkirakan puluhan ribu, bahkan ratusan ribu. Bahkan mereka kini punya organisasi," ujarnya.

Pemulihan hak para korban tapol G30S/PKI merupakan tanggung jawab negara. Tidak hanya Komnas HAM, Mahkamah Agung beberapa waktu lalu sudah mengirim surat kepada pemerintah bahwa para tapol itu telah mengalami ketidakadilan dan harus dipulihkan haknya. Bahkan Mahkamah Konstitusi juga telah melakukan review terhadap Pasal 60 huruf (g) UU No 12 Tahun 2003 tentang Pemilihan Umum.

"Ini tanggung jawab negara yang harus ditanggapi Presiden Yudhoyono. Tiga lembaga sudah meminta, jadi tak ada alasan untuk menunda-nunda supaya tidak ada lagi warga negara yang hak asasinya diabaikan," ujarnya. (kutipan dari Kompas selesai).

## Gugatan Yang Mempunyai Dasar Yang Kuat

Tiga berita tersebut di atas merupakan tambahan bukti yang menunjukkan dengan jelas bahwa gugatan “class action” yang diajukan LBH Jakarta di Pengadilan Negeri Jakarta Pusat terhadap 5 Presiden RI (mengenai kasus para korban 65) mempunyai alasan atau dasar yang kuat, baik secara hukum, politik maupun moral.

Demo yang dilakukan para eks-tapol Sumatra Utara di depan Istana Negara Jakarta dan para eks-tapol Jawa Barat di depan gudung DPRD Jawa Barat, adalah manifestasi dari ledakan kemarahan, atau puncak kekesalan para eks-tapol (dan para korban peristiwa 65 lainnya) atas perlakuan yang tidak adil (dan tidak berperikemanusiaan!) yang mereka alami secara terus-menerus selama hampir selama 40 tahun. Karena hebatnya berbagai macam repressi, persekusi, dan intimidasi yang dilakukan secara ketat dan total oleh rejim militer Suharto dkk maka para korban peristiwa 65 pada umumnya tidak bisa bersuara lantang untuk menggugat kekejaman dan ketidak-adilan yang mereka derita.

Hanya setelah jatuhnya Suharto dari kekuasaan mutlaknya (sejak tahun1998) maka sedikit demi sedikit, dan setapak demi setapak, para korban peristiwa 65 mulai “bersuara” dan melakukan berbagai kegiatan untuk mempersoalkan situasi mereka. Tetapi, karena hebatnya trauma yang mereka alami akibat persekusi besar-besaran selama puluhan tahun, kegiatan mereka untuk memprotes dan menggugat perlakuan yang tidak adil (sekali lagi : dan tidak berperikemanusiaan) ini belum bisa mengakhiri penyiksaan besar-besaran yang berkepanjangan sampai sekarang ini.. Di samping itu, terlalu banyak undang-undang atau peraturan-peraturan yang dibikin oleh rejim militer Orde Baru masih belum dicabut, sampai sekarang. Semuanya itu membikin sebagian besar masyarakat Indonesia masih terus dihinggapi “penyakit mental” dan ikut-ikutan memusuhi para korban 65, sebagai akibat indoktrinasi sesat yang dijalankan selama puluhan tahun.

## Masalah Korban 65 Adalah Urusan Kita Semua

Gugatan LBH Jakarta yang diperkuat dengan aksi-aksi demo para korban peristiwa 65 tersebut di atas, merupakan peringatan kepada seluruh bangsa (termasuk pemerintah dan masyarakat luas) bahwa ada masalah besar yang menyangkut keadilan dan perikemanusiaan yang harus diselesaikan bersama, yaitu masalah korban peristiwa 65. Masalah para korban peristiwa 65 adalah masalah besar bangsa !. Ini bukanlah melulu hanya urusan jutaan orang-orang yang pernah jadi anggota dan simpatisan PKI (atau yang hanya dituduh demikian), yang pernah disiksa secara sewenang-wenang selama puluhan tahun oleh rejim militer Suharto. Ini juga bukan melulu hanya urusan keluarga, saudara, sanak-kadang (dekat dan jauh) para korban 65, yang jumlahnya puluhan juta orang di seluruh Indonesia.

Masalah besar para korban peristiwa 65 adalah urusan seluruh bangsa. Sebab, ini berkaitan dengan ketidakadilan dan penyiksaan (lahir dan batin) terhadap orang-orang tidak bersalah, yang sudah berlangsung begitu lama dan terhadap begitu banyak orang di seluruh Indonesia. Penyiksaan besar-besaran atau perlakuan tidak adil dan sewenang-wenang ini merupakan aib besar bangsa dan dosa berat negara. Ini merupakan pelanggaran HAM yang serius, yang tidak kepalang tanggung. Aib besar atau dosa berat bangsa ini tidak menguntungkan siapa-siapa. Bahkan, orang-orang atau golongan yang anti-komunis atau anti Bung Karno pun tidaklah patut sama sekali untuk bergembira atau menyetujui pelestarian penyiksaan dan perlakuan tidak adil terhadap para korban 65 ini.

## Mereka Berhak Sepenuhnya Menuntut

Gugatan LBH Jakarta dan aksi-aksi demo para korban 65 juga mengingatkan kita semua bahwa para korban 65 (termasuk para eks-tapol) berhak sepenuhnya untuk mengadakan aksi-aksi atau berbagai macam kegiatan untuk mengakhiri ketidakadilan dan penyiksaan yang sudah berlangsung begitu lama itu. Tindakan mereka itu seratus persen sah, benar dan adil, dipandang dari sudut manapun juga, baik dari sudut politik, hukum, moral maupun agama. Dan, adalah justru salah sama sekali kalau mereka tidak mengadakan aksi-aksi atau berbagai kegiatan, untuk mengakhiri ketidak-adilan yang tidak berperkemanusiaan ini.

Oleh karena itu, patutlah kiranya kita semua memandang bahwa gugatan LBH Jakarta atau aksi-aksi demo para korban 65 itu adalah justru untuk kebaikan kita semua, dalam usaha bersama untuk menghilangkan aib bangsa dan dosa besar negara. Penyiksaan atau perlakuan tidak adil itu sudah berlangsung terlalu lama! Korban dan penderitaan sudah bertumpuk-tumpuk dan berlapis-lapis. Di antara mereka banyak yang sudah lanjut usia, dalam kesehatan yang kurang baik, karena sulitnya kehidupan sehari-hari. Dihentikannya penyiksaan terhadap para korban 65 akan mendatangkan kebaikan untuk kehidupan bangsa kita sebagai keseluruhan. Tidak ada orang atau golongan yang dirugikan oleh adanya rehabilitasi dan kompensasi para korban 65. Sebaliknya, kalau masalah korban 65 tidak diselesaikan secara baik, maka bangsa kita akan terus-menerus dihinggapi berbagai penyakit, yang akan terus merusak hati nurani, membikin sakit jiwa, mencupetkan nalar, dan mematikan rasa kemanusiaan banyak orang.

Orang-orang atau golongan yang benar-benar menjunjung tinggi-tinggi Pancasila, atau yang sungguh-sungguh membela HAM tentu akan merasa sedih dan juga marah melihat begitu banyak orang tidak bersalah disiksa begitu lama. Hanyalah orang-orang yang hati nuraninya sudah rusak, yang jiwanya sakit, yang nalarnya cupet -- yang juga rasa kemanusiaannya sudah mati ! -- merasa gembira dengan adanya penyiksaan yang begitu lama (dan penderitaan yang begitu parah !) yang dialami oleh para korban peristiwa 65.
