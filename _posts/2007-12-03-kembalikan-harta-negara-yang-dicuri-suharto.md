---
layout: post
title: Kembalikan harta negara yang dicuri Suharto
date: 2007-12-03
---

Mohon perhatian para pembaca terhadap berita Suara Pembaruan (3 Desember 2007) yang berjudul «Kembalikan Harta Negara yang Dicuri Soeharto » yang disajikan selengkapnya seperti di bawah ini. Karena, dari berita ini saja kita bisa mendapat gambaran bahwa usaha PBB dan Bank Dunia membantu berbagai negara untuk memperoleh kembali harta yang dicuri oleh para koruptor akan menemui berbagai kesulitan atau halangan di Indonesia.


Artinya, usaha untuk membongkar kejahatan Suharto (beserta keluarganya) yang berupa KKN (korupsi, kolusi dan nepotisme) akan tetap sulit dilaksanakan , karena masih adanya atau banyaknya pejabat di berbagai aparat atau lembaga negara yang masih bersimpati kepada rejim Orde Baru. Padahal, banyak orang yakin bahwa harta Suharto (beserta keluarganya, terutama Tommy Suharto) yang bertumpuk-tumpuk sampai menggunung itu bukanlah hasil jerih payah usaha secara normal dan jujur, melainkan karena menyalahgunakan kekuasaan, alias kejahatan.


Sebab, menurut nalar yang sehat, jelaslah bahwa kekayaan Suharto (dan anak-anaknya, dan terutama Tommy Suharto) tidak mungkin mencapai sampai begitu besar (harap baca kembali laporan majalah Time dan hasil riset George Aditjondro), kalau tidak melalui jalan yang tidak sah alias jalan haram.
Karena itu, ketika kita membaca bahwa menurut Jaksa Agung Hendarman Supanji temuan PBB dan Bank Dunia mengenai Suharto itu “ masih diragukan kebenarannya” patutlah kita mempertanyakan kejujurannya dan juga mempersoalkan integritas pernyataannya tersebut. Pernyataan Jaksa Agung yang demikian ini menggambarkan -secara implisit -- tidak adanya “political will” untuk mengusut kejahatan Suharto. Dan kita bisa menduga bahwa sikap Jaksa Agung yang demikian ini tidaklah berdiri sendirian, artinya ada kekuatan yang mendukung. Dan bahwa yang mendukung sikap Jaksa Agung ini adalah kalangan sisa-sisa Orde Baru yang masih berpengaruh.

Jadi, kita semua perlu menyadari bahwa berbagai kejahatan Suharto itu tidak akan bisa terbongkar dan diadili, selama sisa-sisa kekuatan Orde Baru masih bercokol terus di berbagai aparat atau lembaga negara. Hanya kekuasaan politik yang betul-betul anti Orde Baru-lah yang bisa dan mau mengadili berbagai kejahatan Suharto. Dan hanya kekuasaan politik yang anti-Orde Baru-lah yang bisa dan mau betul-betul menjalankan reformasi secara tuntas. Sekarang makin jelas bagi kita bahwa untuk memperbaiki segala kebobrokan moral yang sudah merajalela di kalangan « orang-orang tua », diharapkan munculnya peran angkatan muda (kaum muda) untuk berusaha mengambil-alih kekuasaan politik. Sebab, sesudah 32 tahun kekuasaan di bawah Suharto dan diteruskan dengan 10 tahun berbagai kekuasaan di bawah Habibi, Gus Dur, Megawati dan SBY-Kalla, maka keadaan negara kita tetap menghadapi berbagai kerusakan dan kemerosotan.

Pemilu tahun 2009 juga tidak akan (tidak mungkin !!!) bisa mendatangkan perubahan-perubahan besar, karena hasilnya akan tetap dikuasai partai-partai politik yang selama ini ikut bertanggung-jawab terhadap segala kebobrokan, termasuk kebobrokan moral yang tercermin dalam penyakit korupsi yang sudah merajalela.

Perubahan besar, termasuk dibongkarnya kejahatan-kejahatan Suharto, hanya bisa dilaksanakan kalau ada perubahan yang substansial dalam kekuasaan politik !!!


A. Umar Said

***


Berita di Suara Pembaruan (tanggal 3 Desember 2007) itu adalah sebagai berikut :


Medio Oktober 2007, Stolen Asset Recovery (StAR) Initiative: Challenges, Opportunities and Action Plan mempublikasikan harta jarahan sejumlah pemimpin dan mantan pemimpin negara. Mantan Presiden Soeharto ditempatkan sebagai orang yang paling banyak mencuri harga negara dengan jumlah US$ 15 juta - US$ 35 miliar.


Pengumuman yang resmi dilakukan PBB dan Bank Dunia itu memunculkan sedikitnya dua pendapat di dalam negeri. Pertama, temuan dan pengumuman itu disambut positif. Temuan PBB dan Bank Dunia itu harus dijadikan pintu masuk mengusut harta Soeharto di luar negeri dan mengembalikan harta-harta itu ke negara.
Advokat senior, Todung Mulya Lubis mengatakan, PBB dan Bank Dunia adalah lembaga yang kredibel, sehingga tidak mungkin temuan itu mengada-ada dan tidak berdasar pada fakta dan metodologi yang benar. Oleh karena itu, kata Todung, pemerintah harus menindaklanjuti secara serius temuan tersebut.
Sosiolog yang juga penulis buku "Korupsi Kepresidenan, Reproduksi Oligarki Berkaki Tiga: Istana, Tangsi, dan Partai Penguasa," George Junus Aditjondro mengatakan, temuan PBB dan Bank Dunia tak berlebihan. Pemerintah, dalam hal ini Kejaksaan Agung, segera mengambil langkah mengembalikan aset negara yang dicuri Soeharto yang kini berada di sejumlah negara.


Kedua, temuan PBB dan Bank Dunia itu tidak perlu digubris. Pasalnya, temuan itu didasarkan sumber-sumber yang diragukan. "Laporan PBB dan Bank Dunia itu hanya berdasarkan pada kliping koran. Jadi diragukan kebenarannya," kata salah satu kuasa hukum Soeharto, Muhammad Assegaf.
Jaksa Agung Hendarman Supandji ketika menjadi pembicara kunci dalam seminar Pengkajian Hukum Nasional 2007 dengan tema "Pengembalian Aset Melalui Instrumen Stolen Asset Recovery (StAR) Initiative, dan Perundang-undangan Indonesia" di Jakarta, Rabu (28/11), mengatakan, temuan PBB dan Bank Dunia itu masih diragukan kebenarannya.


Kemauan Politik
Ketua Program Studi Magister Hukum Fakultas Hukum Universitas Airlangga, Surabaya, Peter Mahmud Marzuki dalam makalahnya sebagai salah satu pembicara dalam seminar yang diselenggarakan Komisi Hukum Nasional (KHN) di atas mengatakan, pemerintah harus tetap berusaha mengembalikan harta negara yang dicuri Soeharto.


Untuk melakukan itu, kata dia, pemerintah harus memiliki kemauan politik yang sungguh-sungguh dan membentuk lembaga khusus. Lembaga ini hanya menginventarisasi kontrak-kontrak yang dibuat oleh BUMN-BUMN pada rezim Soeharto, akte-akte pendirian perusahaan yang dibangun pada masa pemerintahan Soeharto sampai lengsernya Soeharto, serta kontrak-kontrak yang dibuat oleh perusahaan-perusahaan itu.
Akan tetapi, untuk mengetahui akte-akte terutama akte notaris dan dokumen-dokumen itu diperlukan izin pengadilan. Dalam hal inilah pengadilan dapat membantu apakah perusahaan-perusahaan yang didirikan itu memang layak didirikan.


Dalam studi yang dilakukan oleh United Nations Office on Drugs and Crime (UNODC) dan World Bank Group (WBG), kata Peter, inisiatif mengembalikan aset yang telah dicuri oleh suatu rezim yang memerintah secara otoriter dan korup mulai dalam negeri negara yang asetnya telah dicuri itu. [SP/Siprianus Edi Hardum]
