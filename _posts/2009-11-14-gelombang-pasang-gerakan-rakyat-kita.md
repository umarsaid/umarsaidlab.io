---
layout: post
title: Gelombang pasang gerakan rakyat kita
date: 2009-11-14
---

(Sebuah curhat tentang bubrahnya negeri kita)

Negeri kita dewasa ini sedang digoyang heboh luar biasa besarnya sekitar kisruh KPK-POLRI-Kejaksaan yang sedang mengguncang opini publik di seluruh Indonesia. Banyak sekali orang di seluruh Indonesia yang bingung, marah, geram, muak, dan berontak melihat dan mendengar di televisi dan membaca di pers membanjirnya berita-berita, simpang siurnya komentar, atau membeludaknya reaksi keras berbagai kalangan dari rakyat kita yang 240 juta. Maklum, peristiwa tersebut  menyinggung masalah besar  (dan yang penting sekali) yang  berkaitan dengan berbagai bidang kehidupan bangsa dan negara kita.

Mengingat hebatnya gejolak dan besarnya akibat yang ditimbulkannya dalam  opini publik di seluruh negeri, maka sekarang ada kalangan yang menyamakan peristiwa ini sebagai “gempa raksasa” di kalangan atas atau bahkan “tsunami politik” tingkat tinggi, yang  masih sulit diduga bagaimanakah atau apa sajakah akibatnya dan juga kemana sajakah juntrungnya. Sebab, banyak kalangan yang melihat bahwa persoalan POLRI-KPK-Kejaksaan ini ada hubungan atau kaitannya dengan mega skandal lainnya (yang bahkan jauh lebih besar lagi !!! )  yang bernama kasus bank Century.

Clash antara pemerintah dan hati nurani rakyat

Karena itu, dengan mengamati perkembangan persoalan POLRI, Kejaksaan, KPK sampai sekarang kita semua bisa melihat bahwa masalah-masalah tersebut sudah bukan lagi hanyalah masalah hukum dan soal-soal kriminal saja, melainkan juga sudah melebar, meluas, dan menyangkut bidang politik, partai-partai, DPR, bahkan peran presiden SBY sendiri.  Ada kalangan yang melihat masalah ini sebagai clash (perbenturan) antara kredibilitas pemerintah (atau kalangan elite) versus hati nurani rakyat banyak.

Kiranya, betullah kata orang bahwa sesudah peristiwa besar dan bersejarah tahun 1998 ( ketika Suharto dipaksa turun dari kekuasaannya), baru heboh mengenai kisruh besar antara POLRI - KPK dan Kejaksaan (dan DPR) sekarang inilah yang merupakan peristiwa terbesar dan terdahsyat, yang akibatnya bisa sangat penting bagi negara dan bangsa kita di kemudian hari.

Persoalan POLRI-KPK-Kejaksaan yang sudah terus-menerus setiap hari memenuhi halaman suratkabar, majalah, dan layar televisi di seluruh Indonesia, merupakan salah satu di antara banyak bukti bahwa negara kta memang sedang dirusak oleh mafia hukum, mafia peradilan, mafia kekuasaan. Sebagai akibatnya, sejak lama belum pernah ada peristiwa yang sudah membikin brontak emosi dan fikiran begitu banyak dan begitu luas berbagai golongan dalam masyarakat.

Kebobrokan, kebusukan, dan kebejatan moral di kalangan tinggi aparat negara kita ini bukan saja telah kelihatan telanjang bulat di Mahkamah Konstitusi, atau dari proses penelitian oleh TPF (Tim 8), melainkan juga dari berbagai pernyataan orang-orang yang tersangkut dalam perkara besar ini, termasuk dalam perkara Antasari dan kasus bank Century.



Sejak jaman Orde Baru sudah banyak mafia



Sebenarnya, kalau kita cermati dalam-dalam, maka nyatalah bahwa negara kita sejak pemerintahan Orde Baru sudah dijangkiti oleh penyakit parah yaitu : mafia hukum, mafia peradilan, dan mafia kekuasaan. Seperti yang sama-sama kita ingat, sejak rejim militer Suharto penyakit-penyakit tersebut adalah jauh lebih parah, lebih luas, dan juga lebih lama (paling sedikit 32 tahun !).  Dan kita juga sama-sama tahu bahwa itu semua adalah karena negara kita waktu itu sedang dikuasai oleh suatu rejim yang merupakan mafia raksasa yang dikepalai Suharto.

Nah, dalam mafia raksasa inilah  banyak sekali pejabat penting negara (militer ataupun sipil seperti tokoh-tokoh utama Golkar) yang menjadi penjahat, yang bersekongkol dengan berbagai anasir atau oknum yang bergelimang dengan praktek-praktek korupsi.  Tetapi, kita juga sama-sama ingat bahwa karena adanya mafia hukum, mafia peradlan dan mafia kekuasaan (yang dikelola oleh Suharto bersama jenderal-jenderalnya) maka sedikit sekali berbagai kejahatan (kejahatan HAM yang luar biasa, kejahatan korupsi, kejahatan ekonomi) bisa dibongkar apalagi diadili. Dan itu berlangsung lama sekali, selama 32 tahun !!!

Sebenarnya, sejak pemerintahan Orde Baru secara resminya saja sudah bubar dalam tahun 1998 dan digantikan oleh berbagai pemerintahan sampai sekarang, mafia hukum, mafia peradilan, dan mafia kekuasaan masih terus-menerus merajalela di berbagai tingkat dan bermacam-macam bidang, dengan berbagai bentuk dan skala yang berbeda-beda juga. Ini terjadi di Jakarta, di tingkat propinsi, kabupaten, sampai kecamatan. Jadi korupsi sama sekali bukanlah soal baru bagi sebagian terbesar rakyat kita.Karena sudah merajalela sejak 1965.



Negara kita adalah negara bedebah



Namun demikian, terbongkarnya sebagian dari kebobrokan dan kebusukan pemerintahan  kita yang dipertontonkan oleh pemutaran rekaman percakapan tilpun antara para pejabat dan penjahat oleh Mahkamah Konstitusi, disusul dengan  proses dan hasil-hasil pekerjaan  TPF (Tim 8), ditambah dengan berbagai pernyataan para “tokoh” utama dalam persoalan yang rumit ini (antara lain : Kapolri, Jaksa Agung, Antasari, Anggodo, Ari Muladi, Bibit Samad Rianto, Chandra Hamzah) menunjukkan kepada kita semua dengan jelas bahwa  heboh tentang KPK-Polri-Kejaksaan ini sudah betul-betul membikin makin jelas lagi bagi banyak orang bahwa negara kita ini adalah negara para bedebah (untuk meminjam ungkapan Adhie Massardi dalam puisinya yang bagus sekali dan sangat populer dewasa ini)

Heboh tentang kisruh di aparat negara di tingkat tertinggi ini ditambah dengan munculnya heboh baru tentang sikap komisi III DPR yang kelihatan sekali bersikap membela POLRI dalam persoalan pimpinan KPK (yang sudah di non aktifkan) Bibit Samad Rianto dan Chandra Hamzah merupakan pendidikan politik dan moral secara besar-besaran yang skalanya atau jangkauannya belum pernah terjadi di masa lampau.

Dengan banyaknya orang (beberapa puluh juta) di seluruh Indonesia yang mengikuti siaran berbagai televisi  (antara lain dan terutama Metro TV, SCTV, TV One, RCTI)  setiap pagi, siang dan malam yang banyak sekali menayangkan berita, komentar, dialog , perdebatan dll mengenai kasus KPK-POLRI-Kejaksaan  (sekitar Bibit-Chandra dan Antasari) maka banyak orang bisa mengetahui dengan gamblang sekali bahwa negara  kita sedang dirusak oleh berbagai macam mafia. Dan karena penyelesaian kasus-kasus ini (lewat pengadilan atau lewat cara-cara lain) masih akan makan waktu  lama sekali, maka pendidikan tentang politik dan  moral, tentang pemerintahan yang bersih, tentang pemberantasan korupsi dan segala macam kejahatan lainnya, yang diberikan oleh kasus-kasus ini akan luar biasa pentingnya, dan juga tanpa preseden sebelumnya.



Rakyat kita mendukung KPK dengan antusias



Disiarkannya oleh Mahkamah Konstitusi (selama empat setengah jam) rekaman percakapan tilpun antara berbagai pejabat top di kepolisian dan kejaksaan dengan Anggodo dan orang-orang lainnya telah membikin meledaknya kemarahan dan berkobarnya kebencian puluhan juta orang di seluruh negeri terhadap kelakuan sebagian pejabat-pejabat di kepolisian dan kejaksaaan. Ini merupakan fenomena yang penting sekali (dan menggembirakan) , yang menunjukkan bahwa rakyat sudah tidak bisa lagi diperlakukan seperti yang dialami selama masa rejim militer Orde Baru selama 32 tahun.

Fenomena lainnya yang juga  amat penting (bahkan terpenting kalau dilihat dari segi jangka jauh) adalah lahirnya gerakan besar-besaran dan luas sekali – yang juga tidak ada preseden sebelumnya – untuk mendukung KPK. Hampir di semua kota besar Indonesia, bahkan di banyak kota kecil dan daerah-daerah (di Sumatera, Kalimantan, Sulawesi, Maluku, Nusa Tenggara) muncul berbagai macam gerakan, yang menggunakan nama atau bentuk : koalisi, aliansi,  gerakan, aksi, front. Atau nama-nama lainnya yang menunjukkan besarnya antusiasme  yang menggebu-gebu dari berbagai golongan untuk mendukung KPK

Satu indikasi yang menunjukkan betapa hebatnya gejolak yang disebabkan oleh kisruh POLRI-KPK-Kejaksaan (dan kemudian juga dengan DPR) adalah banyaknya orang yang ikut dalam gerakan face-book untuk mendukung KPK bersama Bibit-Chandranya. Dalam tempo yang singkat saja (kurang lebih sepuluh hari) sudah tercatat ada lebih dari 1.200.000. Luar biasa!

Patutlah kiranya juga kita perhatikan bersama-sama bahwa dalam segala gerakan atau aksi-aksi membela KPK di banyak tempat di seluruh Indonesia ini kalangan muda dari macam-macam golongan masyarakat mengambil peran atau inisiatif terbesar. Mereka tidak saja mengadakan berbagai macam demonstrasi, long march, mogok makan, tetapi juga rapat-rapat umum, mengirimkan petisi, teater, pembacaan puisi dll dll. (Sebagai contoh, di antara aksi-aksi yang banyak dilakukan tiap hari di seluruh Indonesia itu adalah aksi mogok makan di halaman KPK, oleh aktivis-aktivitas berbagai organisasi, termasuk dari LMND/PRD yang berjalan selama lebih dari 10 hari).

Kalau kita perhatikan tayangan televisi tentang berbagai macam gerakan untuk mendukung KPK  dalam melawan usaha-usaha untuk meng kriminalisasikannya atau bahkan mematikannya, maka nampak bahwa angkatan muda bangsa kitalah yang tampil paling depan atau ambil peran utama. Ini adalah sungguh-sungguh hal yang paling indah dalam hiruk-pikuk tentang kisruh atau skandal besar yang sedang kita tonton tiap hari selama ini.



Peran penting sekali kalangan muda bangsa



Kita patut bergembira juga bahwa dalam gerakan mendukung KPK ini kalangan muda datang dari berbagai golongan masyarakat, antara lain dari golongan Islam, Kristen, nasionalis, atau kiri (pendukung Bung Karno, atau golongan komunis). Ikut sertanya kalangan muda dari berbagai golongan (atau partai politik) ini menunjukkan bahwa terjadi perkembangan penting di kalangan muda.
Bangkitnya kalangan  muda kali ini menunjukkan bahwa bangsa kita masih mempunyai harapan akan adanya perubahan-perubahan yang menguntungkan kepentingan orang banyak.

Ketika kalangan tua mereka (yang duduk dalam pemerintahan dan DPR sebagai wakil berbagai partai politik) kelihatan pasif atau adem-ayem saja dalam menghadapi heboh KPK-POLRI-Kejaksaan, karena sudah diikat atau diborgol oleh koalisi dengan SBY-Budiono, maka banyak kalangan  muda dari berbagai golongan ini terpaksa mengambil  jalannya sendiri dan tidak tunduk kepada   angkatan tua mereka masing-masing.

 “Rupture” (perpecahan atau  retak) antara kalangan muda - termasuk sebagian  besar intelektual-  yang umumnya membela KPK dan “angkatan tua” di partai-partai politik dan aparat negara (atau pemerintahan) menumbuhkan harapan akan timbulnya gerakan ekstra-parlementer yang luas dan kuat dalam berbagai bentuk dan cara. Lahirnya gerakan yang luas ini adalah petunjuk yang jelas bahwa kredibilitas pemerintahan SBY yang disokong oleh banyak partai politik sudah jatuh merosot, dan bahwa DPR dan partai-partai juga makin tidak dipercayai rakyat banyak.

Kasus skandal KPK-Polri-Kejaksaan (ditambah dengan kisruh di DPR) menunjukkan bahwa kemunafikan dan kebejatan moral di kalangan atas sudah mencapai puncaknya ketika kita saksikan bahwa “sumpah demi Allah” sudah ramai-ramai diobral murah untuk menutupi kejahatan atau dosa-dosa, dan ketika nama Tuhan juga  sudah  dicatut atau di salahgunakan (atau dilecehkan) dalam berbagai kesempatan. Sebenarnya, mafia hukum, mafia peradilan, dan mafia kekuasaan yang penuh dengan kemunafikan ini sudah lama menginjak-injak agama, walaupun banyak pelaku-pelakunya menyebut dirinya sebagai muslim dan rajin sembahyang atau berkali-kali naik haji (ingat, antara lain : kasus Suharto, Tommy, Ibnu Sutowo, Bob Hasan,  dll dll dll)

Merajalelanya korupsi yang sejak lama disuburkan oleh mafia hukum, mafia peradilan (termasuk mafia pengacara), yang akhir-akhir ini dibeber dengan adanya geger kasus KPK-POLRI-Kejaksaan membikin melek sebagian besar rakyat kita tentang kebejatan moral di kalangan elite kita. Orang pun bertanya-tanya : sudah sampai sebegitu bejatnyakah akhlak pejabat-pejabat kita, dan mengapa bisa terjadi penyakit yang separah itu, dan apakah  bisa mengobatinya, dan bagaimana caranya.?

Jelaslah, bahwa jawaban kepada pertanyaan-pertanyaan itu adalah tidak gampang dan juga bisa macam-macam sekali. Sebab ini ada kaitannya dengan soal besar yang fundametal sekali, yaitu moral. Juga dengan soal-soal lainnya, antara lain :  moral publik (“korupsi sudah lumrah”), pemerintahan yang lemah, tidak adanya panutan yang berwibawa (yang seperti Bung Karno), tidak adanya nasionalisme atau patriotisme, tidak peduli kepada kepentingan umum atau rakyat, mengutamakan secara berlebih-lebihan kekayaan atau kemewahan, tidak malu lagi mencuri harta umum,



Hubungan erat antara korupsi dengan mafia-mafia



Mengingat itu semua, agaknya dengan sistem dan politik pemerintahan yang sudah dijalankan sejak 65 akan sulit sekalilah memberantas korupsi yang berkaitan erat dengan mafia hukum, mafia peradilan dan mafia kekuasaan. Apalagi oleh pemerintahan SBY (atau pemerintahan-pemerintahan lainnya yang serupa atau sehaluan dengannya).

Sekarang ini sudah makin jelas bagi kita semua bahwa korupsi yang merajalela sejak pemerintahan Orde Baru dan dibikin lebih gamblang lagi oleh skandal besar KPK-Poliri-Kejaksaan (dan DPR) tidak kita temukan persamaannya atau bandingannya  dengan masa di bawah pemerintahan Bung Karno. Kalaupun di masa Bung Karno ada juga   korupsi dan penyalahgunaan kekuasaan, maka skalanya adalah jauh lebih kecil, bisa diumpamakan antara gurem dan gajah, atau antara kerikil dan gunung., dibandingkan dengan yang terjadi di masa Orde Baru, sampai sekarang.

Karena kelihatannya masalah-masalah besar yang dihadapi rakyat dan negara sulit sekali diatasi oleh pemerintahan SBY dengan koalisinya (partai-partai) yang kualitasnya sudah mulai kelihatan mengewakan banyak orang dan kehilangan martabatnya, maka dengan segala jalan serta berbagai macam cara dan bentuk kita perlu menyokong terus sehebat-hebatnya dan sekuat-kuatnya  gerakan rakyat (terutama kalangan muda) dalam perjuangan bersama melawan korupsi, mafia hukum, mafia peradilan dan mafia kekuasaan.

Cerakan rakyat untuk mendukung KPK yang sudah meluas di seluruh negeri sekarang ini adalah momentum yang luar biasa bagusnya untuk dijadikan modal bagi perjuangan bersama dalam mengadakan perubahan-perubahan sejati dan yang fundamental di negeri kita. Momentum yang begini indah ini tidak boleh kita biarkan lewat dengan percuma saja.

Sekarang makin dibuktikan oleh pengalaman-pengalaman selama lebih dari 40 tahun bahwa hanya gerakan rakyat besar-besaran yang luas, yang dijiwai ajaran-ajaran revolusioner Bung Karno lah yang bisa diharapkan untuk mengadakan  perubahan-perubahan besar di negeri kita.
