---
layout: post
title: 'Menurut Wiranto : Bung Karno bapak bangsa kita'
date: 2006-08-15
---

Menjelang Hari Kemerdekaan 17 Agutus, mohon perhatian pembaca terhadap berita yang sebagai berikut:
”Ketua Umum Dewan Pimpinan Pusat Perhimpunan Kebangsaan, Jenderal (Pur) Wiranto menegaskan bangsa Indonesia mengalami kelunturan nasionalisme, akibat eforia reformasi yang kebablasan.Hal itu diungkapkan Wiranto dalam pidato politik usai melantik Perhimpunan Kebangsaan Dewan Pengurus Daerah Lampung hari Sabtu (12/8).

Wiranto menegaskan, kondisi berbangsa dan bernegara mengalami kemunduran. Berbagai persoalan muncul, konflik horizontal, maraknya separatisme, egoisme sektoral,sektarianisme, eforia otonomi daerah, yang mengarah pada instabilitas politik.Bahkan kemiskinan, pengangguran dan jurang antara kaya dan miskin makin lebar.

Wiranto yang mengutip BPS menyatakan angka kemiskinan mencapai 48 juta orang, versi Bank Dunia 100 juta rakyat Indonesia miskin.Yang ironis, katanya setiap bayi yang lahir dibebani utang negara Rp7 juta, dan total utang kita mencapai Rp1.500 triliun, sesuatu yang luar biasa bagi sebuah negara berkembang.

Karena itu, kata Wiranto, kita terjebak dalam neokolonialisme, neoliberalisme, ketergantungan pada pihak asing sangat besar dan menjadi bangsa yang tidak mandiri.Menurut Wiranto, kita harus tetap mencontoh keteladanan bapak bangsa kita Bung Karno yang selalu menanamkan nilai-nilai kebangsaan, pembangunan karakter nasional yang selalu menjadi pedoman, kita bangsa besar yang memiliki nilai-nilai dan watak bangsa.

Wiranto mengajak seluruh rakyat Indonesia tetap mempertahankan nilai-nila nasionalisme, Pancasila, UUD 1945 dan tetap memiliki konsistensi, transparansi, dan revitalisasi menuju karakter bangsa dalam rangka pembangunan nasional.” (dikutip dari Media Indonesia, 12 Agustus 2006).



Pepatah Jawa : “Becik ketitik olo ketoro”

Reaksi pertama orang membaca berita ini bisa macam-macam. Umpamanya : “Ah, jangan percaya, tipu saja itu”. Atau yang begini “ Mana bisa, jenderal TNI didikan Suharto bicara begitu bagus tentang Bung Karno”. Atau, yang lainnya lagi ;”Itu cuma siasat baru, orang Orde Baru bicara tentang neo-liberalisme dan neo-kolonialisme, bah!”. Yang lebih sinis lagi, kalau ada yang mengatakan “ Sejak kapan, jenderal pendukung Suharto sudah berubah menjadi patriot pro-rakyat?”. Mereka yang tidak percaya kepada pernyataan mantan petinggi ABRI ini bisa juga menyindirnya :”Dia bicara begitu karena sudah terpinggir saja, dan mau cari muka atau menebus dosa”.

Tetapi, bisa juga ada orang-orang yang melihatnya dari segi positif. Barangkali ada yang menaruh harapan bahwa pernyataan mantan Panglima ABRI Wiranto itu pertanda adanya perubahan-perubahan tertentu di kalangan TNI (khususnya TNI-AD). Atau, bisa saja ada orang yang melihat bahwa Wiranto yang tadinya begitu setia kepada Suharto, sekarang sudah berubah, karena berbagai sebab. Tentu ada juga orang yang berpendapat bahwa Wiranto ini sekarang rupanya mau mengkoreksi kesalahan-kesalahannya dan mungkin juga kesalahan ABRI pada umumnya, dan karena itu harus didukung dan diberi kesempatan.

Mana yang betul? Harap jangan kuatir, atau jangan bingung........... Hari kemudian pasti akan memberitahu kita semua, dan sejarah akan menjadi saksi. “Becik ketitik olo ketoro”, kata pepatah Jawa. (Yang baik akan ketahuan dan apa yang jelek akan kelihatan).



Apa benar-benar serius dan tulus?

Yang jelas, pernyataan pensiunan jenderal Wiranto seperti yang diberitakan Media Indonesia itu adalah menarik, dan sekaligus juga menimbulkan berbagai pertanyaan. Dan ini adalah wajar. Sebab, kita semua tahu bahwa selama 40 tahun (yaitu sejak 1965) TNI, terutama TNI-AD, merupakan masalah besar bangsa dan negara. Dari 40 tahun itu, golongan militer pernah selama 32 tahun mengkerangkeng rakyat Indonesia dengan rejim militernya yang dinamakan Orde Baru. Selama 32 tahun lamanya, golongan militer (sekali lagi, terutama Angkatan Darat) dipakai oleh Suharto untuk menghancur-luluhkan kekuatan politik presiden Sukarno beserta pendukung utamanya, yaitu PKI.

Sejarah sudah menunjukkan betapa sengitnya pertentangan antara sejumlah petinggi-petinggi TNI-AD dan presiden Sukarno sebelum terjadinya G30S dan sesudahnya. Bung Karno, yang sejak mudanya sudah bersikap - dan bertindak - sebagai pejuang anti-kolonialisme dan anti-imperialisme ditentang oleh sebagian pimpinan militer (Angkatan Darat), yang bersekutu dengan kekuatan pro-imperialisme (terutama AS) dengan kakitangannya di dalam negeri. (ingat, antara lain : peristiwa Oktober 1952, peristiwa 3 Selatan, berdirinya Dewan-dewan di daerah, pembrontakan PRRI-Permesta, peristiwa G30S dll ).

Oleh karena itu, ketika mantan jenderal Wiranto bicara ““kita harus tetap mencontoh keteladanan bapak bangsa kita Bung Karno yang selalu menanamkan nilai kebangsaan, pembangunan karakter nasional” bisa ditafsirkan macam-macam, dan kita bisa saja bertanya-tanya apakah ia benar-benar serius – dan tulus -- dengan pernyataannya itu. Apakah tidak ada udang di balik batu, dan apa saja effek yang diperhitungkannya?



Apa arti “Bung Karno bapak bangsa”

Sebab, kalau (harap diperhatikan kata “kalau” di sini) ucapannya ini betul-betul serius dan tulus, maka akan merupakan peristiwa penting sekali dalam kehidupan bangsa dan negara, sejak didirikannya rejim militer Orde Baru setelah peristiwa G30S. Ini bisa berarti bahwa tindakan pimpinan Angkatan Darat di masa lalu dalam menentang politik Bung Karno dan kemudian menggulingkannya lewat kudeta merangkak adalah salah atau disalahkan.

Kalimat Wiranto yang berbunyi :”Kita harus tetap mencontoh keteladanan bapak bangsa kita Bung Karno yang selalu menanamkan nilai kebangsaan, pembangunan karakter nasional” bisa mempunyai arti yang dalam dan banyak. Karena ada kata-kata “ kita harus tetap mencontoh keteladanan” dan juga “bapak bangsa kita Bung Karno”. Mencontoh keteladanan berarti menganggap baik apa yang perlu diteladani. Artinya, usaha bung Karno dalam menanamkan nilai kebangsaan dan pembangunan karakter nasional dianggap baik oleh mantan panglima ABRI Wiranto. Perkataan “bapak bangsa” adalah penghargaan yang amat terhormat dan tinggi sekali.

Usaha Bung Karno untuk menanamkan nilai kebangsaan dan pembangunan karakter nasional tercermin dengan gamblang sekali dalam karya-karyanya yang terkumpul dalam buku “Di bawah Bendera Revolusi”, dan dalam pidato-pidatonya sesudah peristiwa G30S (lihat buku “Revolusi belum selesai”.). Kita bisa lihat, bahwa dalam rangka inilah Bung Karno telah : mengucapkan pleidooi-nya yang terkenal “Indonesia menggugat”, memainkan perannya (di belakang layar) dalam lahirnya Sumpah Pemuda tahun 1928, mencetuskan Pancasila, gagasan Nasakom, Manipol, Berdikari, Resopim, dan banyak ajaran-ajaran revolusionernya yang lain.



Dampaknya bisa tidak kecil

Penanaman nilai kebangsaan dan pembangunan karakter nasional telah dilakukan Bung Karno dengan gigih selama puluhan tahun melalui ajakan atau seruannya kepada rakyat Indonesia untuk berjuang guna mencapai masyarakat adil dan makmur dan menentang imperialisme dan kolonialisme dalam segala bentuknya. Berkat ajaran-ajaran revolusionernya, bangsa Indonesia pernah dipandang terhormat oleh banyak rakyat di dunia, terutama di Asia, Afrika dan Amerika Latin. (Ingat : Konferensi Bandung, gerakan non-blok, Konferensi Pengarang Asia-Afrika, Konferensi Wartawan Asia-Afrika, Konferensi Pemuda Asia-Afrika dll dll.)

Justru hal-hal yang baik yang menjadi garis politik dan isi perjuangan Bung Karno inilah yang kemudian dihancurkan secara khianat oleh segolongan pimpinan Angkatan Darat, yang bersekongkol dengan kekuatan AS beserta sekutu-sekutunya di dalamnegeri.

Kalau mengingat betapa besarnya dosa Suharto dkk dalam mengkhianati Bung Karno beserta para pendukung utamanya, PKI, maka kata-kata Wiranto yang menyebut Bung Karno sebagai “bapak bangsa kita” merupakan suatu peristiwa yang besar artinya, dan karenanya perlu mendapat perhatian kita. Sebab, kalau (sekali lagi, harap dicatat perkataan “kalau” di sini!) ucapan ini dijiwai keseriusan, ketulusan atau kejujuran, maka dampaknya akan tidak kecil. Artinya, seluruh politik pimpinan Angkatan Darat selama Orde Baru yang berkaitan dengan Presiden Sukarno perlu dikoreksi besar-besaran !

Tetapi, soalnya, apakah pernyataannya itu dilakukannya sebagai “seorang Wiranto pribadi”, ataukah bisa dianggap mewakili satu golongan atau pencerminan satu aliran fikiran dalam TNI (khususnya Angkatan Darat)?

Memang, sekali lagi, ketika mantan jenderal Wiranto bicara “kita harus tetap mencontoh keteladanan bapak bangsa kita Bung Karno yang selalu menanamkan nilai-nilai kebangsaan, pembangunan karakter nasional” kita bisa saja bertanya-tanya apakah ia benar-benar serius – dan tulus ! -- dengan pernyataannya itu.

Sebab, sejarah sudah menunjukkan betapa sengitnya pertentangan antara sejumlah petinggi-petinggi TNI-AD dan presiden Sukarno sebelum terjadinya G30S dan sesudahnya. Bung Karno yang sejak mudanya sudah bersikap - dan bertindak - sebagai pejuang anti-kolonialisme dan anti-imperialisme ditentang oleh sebagian pimpinan militer (Angkatan Darat), yang bersekutu dengan kekuatan pro-imperialisme (terutama AS) dengan kakitangannya di dalam negeri. (ingat, antara lain : peristiwa Oktober 1952, peristiwa 3 Selatan, berdirinya Dewan-dewan di daerah, pembrontakan PRRI-Permesta).

Utang dan kemiskinan yang besar


Yang juga menarik dari pernyataan Wiranto bukannya hanya soal yang berkenaan dengan Bung Karno, melainkan juga ketika ia berani mengakui bahwa kemiskinan, pengangguran dan jurang antara kaya dan miskin makin lebar di Indonesia sekarang ini. Ia mengutip statiskik yang menyatakan angka kemiskinan mencapai 48 juta orang, sedangkan menurut versi Bank Dunia 100 juta rakyat Indonesia miskin.Yang ironis, katanya, setiap bayi yang lahir dibebani utang negara Rp7 juta, dan total utang kita mencapai Rp1.500 triliun, sesuatu yang luar biasa bagi sebuah negara berkembang.

Yang tidak dijelaskan olehnya adalah bahwa angka kemiskinan yang sebegitu tinggi tidak hanya terdapat selama pemerintahan Habibi, Abdurrahman Wahid, Megawati, dan SBY-Jusuf Kalla saja, melainkan sudah ada sejak zaman Orde Barunya Suharto. Dan juga bahwa utang yang begitu banyak itu (termasuk utang luarnegeri sebesar sekitar $US 140 miliar) sebagian terbesar terjadi ketika rejim militer sedang berkuasa.

Utang yang sebanyak itu sebagian besar dikorup dan dihambur-hamburkan secara royal dan secara khianat oleh para tokoh Orde Baru (baik sipil maupun militer dan para pengusaha), sehingga menurut Prof Dr Sumitro (alm) kira-kira 30% dari anggaran pembangunan kita mengalami kebocoran dan pemborosan (waste and loss). Oleh karena itu, adalah masuk akal atau bisa diterima oleh nalar sehat , kalau sekarang ini ada gerakan untuk menuntut dihapuskannya utang luarnegeri. Sebab, utang luarnegeri kita sudah menjadi beban yang amat berat bagi rakyat, termasuk bagi anak-cucu kita yang belum lahir. Sedangkan yang “menikmatinya” hanyalah sebagian kalangan atas, dan itu pun sering sekali melalui jalan yang haram dan cara-cara yang bathil.

Pernyataan mantan Panglima ABRI, Wiranto, yang berbunyi :” kita terjebak dalam neokolonialisme, neoliberalisme, ketergantungan pada pihak asing sangat besar dan menjadi bangsa yang tidak mandiri” merupakan pengakuan yang tidak langsung tentang kesalahan politik rejim militer Suharto. Sebab, Suharto-lah yang sejak tahun 1967 yang membuka pintu lebar-lebar terhadap neo-kolonialisme dengan mengeluarkan Undang-undangg Penanaman Modal Asing, yang serba terlalu menguntungkan kapitalisme internasional (ingat perjanjian - yang patut dikutuk - dengan Freeport, dan kemudian dengan berbagai perusahaan besar lainnya di bidang pertambangan, perminyakan, agro-bisnis, tenaga listrik, transport dll dll).



Bung Karno tetap orang besar
“Ketergantungan pada pihak asing sangat besar dan menjadi bangsa yang tidak mandiri”, adalah juga kesalahan besar lainnya lagi yang dibuat oleh rejim militer Orde Baru selama puluhan tahun, dan yang kemudian diteruskan oleh pemerintahan Habibi, Gus Dur, Megawati dan sekarang oleh SBY-Jusuf Kalla. Banyak orang masih ingat bahwa Prof Dr Sumitro beserta para “ahli ekonomi” Berkeley Mafia-lah yang dengan amat getolnya menganjurkan selama puluhan “kerjasama” dengan “dunia neo-liberal”, sehingga bangsa Indonesia menjadi bangsa yang tidak mandiri. Apa yang dilakukan oleh ahli-ahli ekonomi pro Amerika ini bertentangan sama sekali dengan politiknya Bung Karno yang mengutamakan prinsip berdikari dalam bidang ekonomi.

Dulu, Suharto beserta para pendukung setianya berteriak-teriak setinggi langit ketika Indonesia di bawah pemerintahan Bung Karno mempunyai hutang luarnegeri US$ 2 miliar dalam tahun 1965. Itupun sebagian besar dipakai untuk tujuan perjuangan (pembelian persenjataan). Sekarang utang luarnegeri RI berjumlah sekitar US$ 140 miliar, sehingga untuk membayar bunga dan cicilan utang saja sudah seperti hampir sekarat setiap tahunnya.

Mengingat itu semuanya, bolehlah kiranya dikatakan bahwa pernyataan Wiranto yang begitu bagus mengenai Bung Karno adalah - terlepas dari apakah serius dan tulus ataukah hanya sebuah taktik dengan tujuan tertentu – perkembangan yang patut kita cermati dengan teliti. Sebab, dihubungkan dengan berbagai masalah nasional dan internasional di masa lalu, dan juga masa kini, nyatalah kiranya bahwa bagaimanapun ia sudah difitnah atau dikhianati, tetaplah Bung Karno orang besar !

---

Komentar Roeslan

16 Agustus 2006

Phenomena Jendral Wiranto.



Wiranto adalah pengikut setia dan selalu siap bersedia melindungi keselamatan dan kekayaan Jendral TNI AD Soeharto dari dulu sampai sekarang. Ini tercermin dalam ucapannya pada waktu Suharto menyatakan “lengser” keprabon pada tahun 1998.


Jika kita melihat ke belakang, kita akan melihat bahwa Jendral TNI AD Soeharto adalah pembunuh secara keji terhadap Presiden pertama Republik Indonesia dan proklamator kemerdekaan 17 Agustus 1945 yaitu almarhum Presiden Soekarno, untuk selanjutnya Soeharto mengangkat dirinya sebagai presiden RI, yang disokong oleh semua jajaran TNI dan segenap jendral-jendralnya termasuk Wiranto.

Atas dasar apa, sekarang Wiranto menyebut-nyebut bahwa Bung Karno (baca almarhum presiden Soekarno) adalah bapak bangsa Indonesia, sedangkan Wiranto pengikut setia Soeharto tak pernah sepatah katapun mengutuk Jendral TNI AD Soeharto atas perbuatan kejinya dalam membunuh Presiden Soekarno yang dianggapnya sebagai bapak bangsa Indonesia.
Bukankah ini merupakan suatu phenomena yang perlu untuk diwaspadai?


Dalam konteks ini kita jangan sampai kehilangan orientasi, karena setiap phenomena yang paling pelikpun mampu kita atasi, jika kita pandang dari segi Holargi. Dari segi Holargi kita dapat melihat saling hubungan baik yang kongkrit yang dapat kita saksikan dengan mata telanjang, bahkan kita sanggup pula „melihat“ saling hubungan yang sinergetis antar corpus yang satu dengan corpus lainnya yang tidak kelihatan, misalnya saja tidak semua orang akan mengakui bahwa >Karakter manusia yang tadinya baik, kemudian berangsur-angsur „berubah“ menjadi jelek atau sebaliknya, akibat pengaruh konsum yang oleh psychoneurologist disebutnya >the consume-syndrome-disease<. Penyakit >konsum-syndrom< ini adalah element yang tidak kelihatan yang bermukim didalam „placenta“-induknya yang kita kenal dalam konteks ini adalah > ideologi militerisme dan fasisme orde baru Soeharto<


Oleh karena itu, kita tahu dan bisa mengerti bahwa seseorang seperti Wiranto itu bisa saja berubah menjadi baik, tapi itu harus mengalami proses transformasi, yang dalam konteks ini adalah proses reformasi dan demokratisasi yang arahnya harus cenderung mengarah pada langkah positif, yaitu mengadili bekas penguasa orde baru Soeharto atas semua perbuatan perbuatan korupsinya dan pelanggaran berat HAM yang petrnah dilakukan; dan juga mengadili semua pelaku-pelaku pelanggran berat HAM yang lainnya selama periode kekuasaan rezim militer fasis Soeharto. Dalam konteks ini kita semua tahu bahwa Wiranto tidak pernah menyokong gerakan reformasi yang menuntut untuk diadilinya pelaku-pelaku pelanggaran berat HAM, seperti misalnya kasus-kasus Tanjung periuk, Trisakati dll.


Kita semua tahu bahwa Wiranto adalah termasuk kader Golkar yang bisa digolongkan dalam jajaran elite Golkar. Sedangkan politik Golkar akan selalu menghidupkan kembali orde baru, oleh karena itu Golkar tidak akan sudi untuk merehabilitasi nama baik Bng Karno, yang sudah dituduh sebagai dalang G30S/PKI sampai sekarang ini. Dalam kenyataan yang kita saksikan Wiranto tetap sebagai kader dan elite Golkar, oleh karena itulah Wiranto sampai saat ini masih tetap mengidap virus penyakit KS (Konsum syndrom) seperti yang sudah disebutkan di atas. Oleh karena itu kita jangan sampai kehilangan orientasi dalam menilai ucapan Wiranto,yang mengatakan Bung Karno sebagai Bapak Bangsa.


Memang Wiranto tak berdosa mengucapkan bahwa Bung Karno sebagai bapak Bangsa, tapi kita yang mendengar ucapan itu harus waspada.Karena kita semua tahu bahwa selama ini di era Reformasi (dari dulu sampai sekarang) Wiranto selalu bepihak pada Bekas penguasa orde baru Soeharto yang secara sadar telah membunuh Bung Karno dan membantai para pengikut-pengikutnya. Tiada sepatah katapun dari Wiranto yang pernah mencerminkan adanya suatu sikap yang mengutuk Soeharto dalam tidakan yang tak manusiawi yaitu melaksanakan pembunuhan keji terhadap Presiden Soekarno dan pembantaian masal terhadap 3 juta rakyat Indonesia yang tak bersalah dan pencinta Bung Karno.

Adalah suatu yang tidak masuk akal sehat bahwa seorang yang menyokong pembunuhan keji terhadap Bung Karno dan tidak pernah merasa berdosa, sekarang ini mengatakan bahwa Bung Karno adalah bapak Bangsa Iandonesia. Oleh karena itulah Wiranto tidak dapat dikatakan sudah berubah jati dirinya menjadi baik dan memihak Rakyat Indonesia, meskipun sudah berapi-api mengatakan Bung Karno adalah bapak bangsa. Benar seperti yang dikatakan oleh pak koentyo soekadar, bahwa Wiranto adalah seorang OPORTUNIS, yang ingin merebut kekuasaan politik di negeri ini.
