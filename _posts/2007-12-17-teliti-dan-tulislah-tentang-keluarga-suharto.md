---
layout: post
title: Teliti dan tulislah tentang keluarga Suharto
date: 2007-12-17
---

Berikut di bawah ini disajikan E-mail dari Sdr Yanti Mirdayanti, yang ditujukan kepada saya, mengenai pentingnya bagi para mahasiswa Indonesia mengadakan banyak riset serius dan membikin karya-karya tulis mengenai persoalan keluarga Cendana (keluarga Suharto). Sdr Yanti Mirdayanti, yang kini mengajar di salah satu universitas di Bonn (Jerman), menganjurkan para mahasiswa untuk menjadikan keluarga Cendana sebagai sumber yang kaya sekali dengan berbagai bahan yang bisa digunakan untuk membuat skripsi (S1), tesis (S2) maupun disertasi (S3).

Anjuran Sdr Yanti Mirdayanti ini sangat menarik dan juga sangat penting mengingat bahwa -- sampai sekarang ini -- masih belum begitu banyak karya-karya ilmiah yang dibuat oleh kalangan sarjana Indonesia mengenai berbagai soal yang berkaitan dengan keluarga Suharto, baik yang bersifat memuji-muji segi positif keluarga Suharto maupun yang mengkritik atau menghujat segi-segi negatifnya. Apa yang sudah sering dan banyak beredar adalah tulisan-tulisan dalam majalah dan suratkabar atau brosur-brosur. Bahkan, yang amat menonjol adalah karya-karya para wartawan, penulis atau ilmuwan asing. Di antaranya, yang patut dicatat adalah laporan majalah TIME 24 Mei 1999.

Padahal, Suharto adalah “tokoh yang luar biasa” dalam sejarah bangsa dan negara kita. Suharto pernah menjadi presiden RI yang terlama (32 tahun, sedangkan Bung Karno hanya sekitar 20 tahun). Dalam waktu 32 tahun boleh dikatakan Suharto adalah negara Indonesia. Atau, dengan kalimat lain, Suharto adalah Orde Baru. Dan, Suharto adalah jiwa atau “tali nyawa” rejim militer, yang ternyata sudah dicampakkan oleh generasi muda bersama rakyat dalam tahun1998.

Oleh karena peran yang amat besar yang dimainkan Suharto sebagai diktator selama 32 tahun, yang memungkinkan ia melakukan berbagai macam pelanggaran HAM secara besar-besaran dan juga menjadi pencuri harta rakyat yang terbesar di dunia, maka banyak sekali (dan bermacam-macam sekali !!!) hal yang bisa digali atau diangkat oleh para peneliti atau ilmuwan. Para mahasiswa (atau sarjana) dapat membikin riset ilmiah di bidang sejarah, politik, ekonomi, sosial, hukum, militer, pemerintahan, HAM, demokrasi, moral, yang berkaitan dengan Suharto dan keluarganya.

Segala karya ilmiah sebagai hasil riset serius tentang Suharto beserta keluarganya adalah menarik dan juga penting, termasuk karya-karya yang masih memuji-muji dan memuja-muja Suharto beserta keluarganya. Dengan makin banyak terbongkarnya kejahatan Suharto (beserta keluarganya) maka makin kelihatan amat janggallah (atau sangat lucu dan aneh) kalau ada orang yang masih berani terang-terangan mengagung-agungkan Suharto seperti di masa-masa yang lalu.

Karya-karya ilmiah mengenai Suharto beserta keluarganya amat penting bagi khasanah perpustakaan sejarah bangsa. Anak cucu kita di kemudian hari perlu tahu – lebih banyak lagi -- mengapa Suharto berhasil menggulingkan Bung Karno, mengapa Suharto bekerja sama dengan imperialisme AS dalam memadamkan revolusi bangsa Indonesia, mengapa Suharto (dan konco-konconya) membunuhi jutaaan anggota dan simpatisan PKI. Bukan itu saja! Anak-cucu kita juga perlu tahu bahwa kerusakan moral Suharto dan kebejatan akhlak Tommy Suharto merupakan pendorong atau teladan yang buruk sekali bagi banyak orang, sehingga korupsi merajalela seperti sekarang ini.

Adalah ideal sekali kalau karya-karya ilmiah ini bisa juga mengangkat sebab-sebab Suharto sampai sekarang belum bisa diadili (karena alasan kesehatan dll dll), dan mengapa anjuran PBB dan Bank Dunia untuk mengembalikan harta yang dicuri koruptor kelihatannya akan macet di Indonesia.

Apalagi, seperti yang dianjurkan oleh Sdr Yanti Mirdayanti, menjadikan kasus Tommy Suharto sebagai sasaran penelitian adalah bukan saja menarik dan mengasyikkan melainkan juga penting. Sebab, kasus uang haram Tommy Suharto betul-betul mencerminkan -- dengan gamblang sekali – betapa besar kerusakan akhlaknya. Kerusakan akhlak Tommy Suharto adalah pengejawantahan (perwujutan) kebejatan moral keluarga Suharto. Dan kerusakan akhlak keluarga Suharto adalah cermin kebejatan moral Orde Baru.

Dalam kaitan inilah saya mendukung anjuran Sdr Yanti Mirdayanti kepada para mahasiswa (dan sarjana umumnya) untuk membikin karya-karya ilmiah berdasarkan riset yang serius mengenai Suharto beserta keluarganya, termasuk Tommy Suharto. Saya ikut menganjurkan kepada seluruh dosen dan pimpinan universitas di berbagai universitas di Indonesia, untuk secara aktif ambil bagian dalam usaha yang sangat penting bagi pendidikan bangsa kita ini.

Karya para mahasiswa (atau sarjana) mengenai seluk-beluk keluarga Suharto ini akan memperkaya dan lebih melengkapi karya yang gemilang (dan bersejarah) yang pernah dibuat oleh Sdr George Aditjondro. Dan juga akan merupakan sumbangan penting dan berharga sekali dari kalangan universitas kepada rakyat dan negara.

Paris, 17 Desember 2007

A. Umar Said

*** *** ***

PS. Berikut adalah E-mail dari Sdr Yanti Mirdayanti :

Yth. Pak Umar,

Menurut saya, sebenarnya hanya dengan bertemakan

'Lika Liku Duit Tommy Soeharto' para mahasiswa

Indonesia yang rajin bermedia online maupun rajin baca

koran bisa menghasilkan sebuah karya tulis, baik

berbentuk skripsi (S1), tesis (S2), maupun disertasi

(S3). Ini terutama berlaku bagi para mahsiswa yang

pusing memikirkan tema apa yang harus mereka tulis

untuk karya ilmiah kesarjanaannya. Sebagai contohnya,

berbagai rangkuman yang dilakukan Pak Umar pun bisa

banyak memberi kontribusi. Saya kira, sudah ada

ilmuwan Indonesia yang memulai, tetapi semakin banyak

akan semakin baik bagi kekayaan kepustakaan Indonesia.

Tak dapat kita sanggah, bahwa keluarga Cendana

(keluarga Soeharto) merupakan sumber kaya bagi para

ilmuwan muda Indonesia untuk dijadikan fokus riset

mereka. Misalnya bisa dijadikan bahan penelitian

mereka-mereka yang dari bidang studi ekonomi, politik,

hukum, dan sejarah. Tinggal kumpulkan berbagai kliping

media cetak maupun informasi-informasi dari

media-online. Kemudian ditambah riset kepustakaan (di

berbagai perpustakaan). Plus, melakukan juga riset

lapangan sendiri, misalnya dengan ikut langsung

menghadiri persidangan-persidangan pengadilan soal

tema yang bersangkutan atau melakukan wawancara dengan

berbagai nara sumber yang mudah dijangkau, dsb. Dalam

jangka satu tahun atau mungkin beberapa bulan (kalau

rajin), sudah akan lengkaplah data-data/bahan untuk

tulisan.

Ini berlaku bukan saja bagi para mahasiswa yang sedang

studi di Indonesia, melainkan juga para ilmuwan muda

Indonesia yang sedang menuntut pendidikan di luar

negeri. Tema-tema yang bersumber keluarga Cendana

masih sangat relevan dan kait-mengait dengan

perkembangan sejarah, ekonomi, politik, dan hukum

Indonesia hari ini dan di masa depan.

Yanti, Bonn, Desember 2007
