---
layout: post
title: Arti penting Temu Eropa II di Essen
date: 2003-11-04
---

Ketika situasi di tanah air sedang menghadapi berbagai gejolak berhubung dengan makin dekatnya Pemilu, dan makin banyaknya persoalan gawat di bidang sosial ekonomi dan hukum, maka Temu Eropa II di Essen (Jerman) merupakan angin segar yang menghembus di tengah-tengah suasana yang kadang-kadang terasa sumpek. Dan ketika gerakan atau kekuatan pro-demokrasi di tanah-air sedang menghadapi persoalan atau banyak kesulitan, berbagai kekuatan pro-demokrasi di Eropa menyatukan diri dan mengadakan langkah-langkah bersama.

Sekitar 50 orang yang mewakili 29 LSM, organisasi non-pemerintah , yayasan , gereja, dan perseorangan telah ikut dalam berbagai kegiatan selama tiga hari (antara tanggal 29 Oktober sampai dengan 2 November 2003). Ini merupakan perkembangan penting yang menggembirakan, sebagai sumbangan untuk mengkonsolidasi kekuatan pro-demokrasi, baik di luarnegeri maupun di tanahair.

Temu Eropa II di Essen merupakan kemajuan besar dalam usaha bersama yang dilancarkan berbagai fihak yang pro-demokrasi di Eropa sejak bertahun-tahun, terutama oleh Temu Eropa I yang diselenggarakan dalam bulan Mei 2003 di Indonesia House (Amsterdam). Dalam pertemuan di Amsterdam ini telah dibicarakan antara lain perlunya memperbaki komunikasi, koordinasi dan konsolidasi gerakan yang mendukung proses penguatan demokratisasi di Indonesia, baik di tingkat nasional maupun internasional.

## Peristiwa Yang Penting Dan Indah

Temu Eropa II di Essen menampilkan ciri-ciri yang positif dari hasil kerjasama dan usaha keras dari berbagai fihak yang tergabung dalam berbagai organisasi atau kelompok masyarakat Indonesia, yang terdapat di berbagai negeri, antara lain : Jerman, Belanda, Prancis, Swedia, dan Indonesia sendiri.

Berbagai LSM, yayasan atau organisasi non-pemerintah (dan perseoragan) yang berpartisipasi aktif atau mendukung Temu Eropa II, walaupun selama ini sudah mengadakan kegiatan-kegiatan menurut tujuan dan kekhususan mereka masing-masing, namun mereka sepakat untuk menemukan platform bersama guna menghadapi kebutuhan dan situasi yang berkembang di tanah air.

Dari sudut inilah dapai dilihat bahwa apa yang terjadi di Essen selama tiga hari adalah sesuatu yang indah dalam sejarah kehidupan berbagai LSM, atau organisasi non-pemerintah, atau kelompok masyarakat Indonesia di Eropa. Dapat diharapkan bahwa apa yang sudah dimulai di Amsterdam bulan Mei 2003, dan diteruskan dalam pertemuan di Essen dapat dikembangkan dalam Temu Eropa III , IV atau V, dengan menyesuaikan diri dengan perkembangan dan kebutuhan situasi, baik di Indonesia mapun di Eropa sendiri.

## Dukungan Yang Luas

Temu Eropa II di Essen (Jerman) , bisa diselenggarakan dengan lancar berkat kerja keras dan kerjasama yang baik dari anggota Panitia (yang terdiri dari wakil-wakil berbagai organisasi/kelompok dari Holland dan Jerman). Tetapi dukungan terhadap Temu Eropa II ini luas sekali, dan tidak terbatas pada kehadiran sekitar 50 orang saja.

Dari Indonesia sendiri dukungan dan harapan terhadap pertemuan ini adalah besar sekali, terutama dari kalangan yang memperjuangkan HAM dan rehabilitasi para korban Orde Baru, dan khususnya para korban peristiwa 65, seperti dari LPR-KROB, Pakorba, LPKP, YPKP, SNB dll.

Jaringan Kerja Budaya (Jakarta) mengirimkan aktivis-utamanya Hilmar Farid untuk menyampaikan makalahnya jang berjudul « Agenda rakyat mengatasi krisis ekonomi ». Sedangkan ORI (Organisasi Rakyat Independen) dari Bengkulu mengirimkan tokohnya yang bernama Usin. LSM dari Aceh diwakili oleh Suraiya Kamaruzzaman (Flower Aceh).

Dukungan terhadap Temu Eropa II ini juga kelihatan dari banyaknya makalah yang diterima oleh panitya, antara lain : Ibrahim Isa –Kejahatan terhadap kemanusiaan ; Budiman Sujatmiko-Visi baru kepemimpinan ; Burhan Azis –Politik ekonomi Orba dalam perbandingan ; JKB Jakarta –Agenda Rakyat mengatasi krisis ekonomi ; Bonnie Setiawan – Akhir globalisasi maut ; Wilson –Bahaya militerisme dan imperialisme.

## Masalah Korban 65

Banyak soal telah dibicarakan dalam dua kelompok kerja Temu Eropa II ini, yang membicarakan masalah berbagai pelanggaran HAM di Indonesia dan berbagai kesulitan ekonomi yang dihadapi oleh rakyat Indonesia (antara lain : hutang, korupsi, pengangguran).

Yang merupakan hal yang patut dicatat tentang Temu Eropa II ialah betapa besarnya perhatian para peserta terhadap masalah pelanggaran HAM di Indonesia, khususnya masalah rehabilitasi para korban peristiwa 65. Ini merupakan perkembangan penting, karena selama ini belum pernah ada pertemuan yang seluas itu membicarakan secara khusus masalah korban peristiwa 65.

Dalam diskusi-diskusi tentang masalah ini para peserta mengupas pentingnya surat ketua Mahkamah Agung (Bagir Manan) dan Ketua Komnas Ham kepada Presiden Megawati yang minta perhatiannya dan supaya menggunakan hak prerogatifnya untuk merehabilitasi para korban dan memperoleh hak-hak sipil mereka sepenuhnya.

Sikap yang diambil oleh Temu Eropa II mengenai masalah rehabilitasi para korban peristiwa 65 ini jelas merupakan dukungan yang kuat terhadap usaha berbagai kalangan di Indonesia yang dengan gigih memperjuangkan HAM. Artinya, dengan hasil yang dicapai oleh Temu Eropa II ini perjuangan di tingkat nasional dan internasional untuk merehabilitasi para korban 65 diharapkan akan mencapai tahap baru.

## Yang Di Luarnegeri Dan Tanahair

Dengan terselenggaranya pertemuan yang serupa di Essen dan Amsterdam itu terasa adanya suasana pendekatan antara berbagai golongan dan kelompok masyarakat Indonesia yang terdapat di berbagai negeri Eropa. Ini merupakan langkah penting untuk terjalinnya saling pengertian dan kerjasama, untuk menangani bersama-sama soal-soal besar yang menjadi kepedulian bersama, dengan menghormati perbedaan dan kekhususan masing-masing.

Dengan adanya kerjasama yang aktif dan tulus dari semua fihak di Eropa, maka akan tercipta syarat-syarat yang lebih baik bagi kita semua untuk membantu perkembangan demokratisasi di Indonesia. Sebab, kita semua sama-sama menyaksikan bahwa banyak sekali pekerjaan yang masih belum tertangani di berbagai bidang, Dan lagi pula, sisa-sisa Orde Baru masih aktif bekerja,dan masih relatif kuat.

Situasi yang tidak menentu ( yang bisa eksplosif di sana sini dan dalam berbagai bentuk) menjelang dan sesudah Pemilu, memerlukan kewaspadaan kita bersama. Dalam situasi yang demikian inilah semangat yang sudah dipupuk oleh Temu Eropa I dan II harus kita kembangkan terus. Perkembangan yang baik dalam kerjasama kita di luarnegeri, akan memberikan dampak positif kepada gerakan pro-demokrasi di Indonesia.
