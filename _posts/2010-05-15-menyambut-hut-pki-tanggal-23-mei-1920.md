---
layout: post
title: Menyambut HUT PKI tanggal 23 Mei 1920
date: 2010-05-15
---

Sudah dapat diduga lebih dahulu bahwa tulisan yang berjudul « Menyambut HUT PKI tanggal 23 Mei 1920 » ini akan mendapat banyak tanggapan atau reaksi yang bermacam-macam dari berbagai kalangan. Karena, judul yang demikian itu sangat  menggelitik hati dan fikiran banyak orang. Dan kalau kemudian ternyata ada banyak sekali orang yang marah, jengkel, menghamburkan  caci-maki atau melampiaskan ketidaksenangan mereka, demikian itu adalah wajar, dan bisa dimengerti, Menyatakan pendapat adalah hak setiap orang.

Karena, dilihat dari berbagai segi, memang banyak hal yang berkaitan dengan PKI patut diketahui dan dimengerti oleh bangsa kita. Sayangnya, terlalu banyak orang yang kurang mengerti, salah mengerti atau bahkan tidak mengerti apa itu PKI. Yang paling  disayangkan  -- dan  patut dikasihani --  adalah mereka yang tidak pernah mau mengerti, karena berbagai sebab. Kalaupun ada yang bersikap demikian adalah hak mereka yang sah-sah saja.

Sebab, terlalu lama bangsa Indonesia dilarang secara resmi oleh pemerintahan sejak 1966 (harap catat baik-baik : hampir setengah abad !!!) untuk mengetahui, mendengar, berbicara, menyebarkan, menerima informasi atau bahan-bahan bacaan tentang PKI. Artinya, segala macam kegiatan atau perbuatan yang berkaitan dengan PKI dilarang, sejak ditetapkannya keputusan nomor 25 tahun 1966 oleh MPRS (badan legislatif gadungan, bikinan secara paksa oleh kalangan militer pendukung Suharto beserta berbagai kekuatan reaksioner lainnya yang anti Bung Karno).

Keputusan MPRS nomor 25 tahun 1966 yang  melarang secara resmi kegiatan PKI dan penyebaran Marxisme ini telah dipakai oleh rejim militer Suharto beserta para pendukung Orde Baru untuk melakukan terror  -- fisik dan mental -- secara besar-besaran, intensif, menyeluruh, dan berjangka lama (paling sedikitnya  32 tahun) terhadap seluruh kekuatan kiri yang mendukung politik Bung Karno dan PKI.

Dengan ketetapan MPRS no 25 tahun 1966 inilah penguasa militer di bawah Suharto telah melumpuhkan atau menghancurkan kekuatan kiri yang sebelum 1965 menjadi tulangpunggung kekuatan  revolusioner Indonesia di bawah pimpinan Bung Karno. Ketetapan MPRS nomor 25 tahun 1966 ini jugalah yang telah membungkam suara revolusioner rakyat Indonesia, dan menjerumuskan bangsa dan negara dalam kegelapan dan pembusukan atau dekadensi, seperti yang sudah kita saksikan bersama puluhan tahun sejak jamannya Orde Baru,  sampai sekarang !



Melarang PKI berarti melumpuhkan Bung Karno

Para pengamat sejarah yang objektif - atau semua orang yang jujur atau bernalar sehat – akan bisa melihat bahwa penggulungan atau penghancuran PKI beserta para simpatisannya (oleh golongan militer di bawah pimpinan Suharto dengan bantuan imperialisme AS)  sebenarnya atau pada hakekatnya adalah juga dengan tujuan untuk melumpuhkan atau menghancurkan kekuatan politik revolusioner Bung Karno.

Dari sudut ini kita bisa melihat bahwa ketetapan MPRS 25 tahun 1966 (larangan terhadap PKI) sesungguhnya bukan hanya ditujukan kepada PKI saja, melainkan juga kepada Bung Karno. Karena, dengan melarang PKI, yang merupakan kekuatan utama pendukung berbagai politik revolusioner Bung Karno  -- yang sejak tahun 1920-an sudah anti-kolonialisme dan anti-imperialisme dan pro-sosialisme – maka impian sejak lama dari kekuatan imperialis asing untuk melenyapkan Bung Karno menjadi kenyataan.

Dalam sejarah bangsa Indonesia akan dicatat oleh generasi sekarang dan generasi yang datang, bahwa dihancurkannya kekuatan politik Bung Karno dan PKI oleh Suharto beserta pendukungnya (baik sipil maupun militer) adalah pada hakekatnya merupakan pengkhianatan berat terhadap tujuan revolusi rakyat Indonesia serta perusakan yang parah terhadap Republik Indonesia. Akibat buruknya kita saksikan dalam situasi negara kita sekarang ini, yang penuh dengan berbagai macam kebobrokan, kebejatan, dan kebusukan.



TAP MPRS no 25/1966 adalah aib bangsa

Kalau difikir secara dalam-dalam, dan dengan hati yang jernih pula,  maka jelaslah  bahwa diputuskannya TAP MPRS no 25/1966 hampir setengah abad yang lalu (sekali lagi : hampir setengah abad yang lalu !) adalah betul-betul aib bangsa yang besar sekali. Bangsa Indonesia mungkin punya macam-macam aib atau segi-segi yang negatif, tetapi TAP MPRS yang satu ini adalah aib yang terbesar yang perlu dicatat oleh generasi-generasi kita yang akan  datang.

Sebab, larangan terhadap PKI ini secara implisit membenarkan  (atau menghalalkan  atau mensyahkan ) adanya pembantaian besar-besaran jutaan anggota dan simpatisan PKI yang tidak bersalah sama sekali, pemenjaraan ratusan ribu (juga orang-orang yang tidak bersalah apa-apa !!!) dalam jangka yang lama sekali.

Karena adanya TAP MPRS ini pulalah puluhan juta keluarga anggota atau simpatisan PKI (antara lain : pegawai negeri, buruh, tani, pemuda, mahasiswa) menderita selama hampir setengah abad ( !!!) bermaca-macam terror  -- mental dan fisik  -- karena dipersekusi, didiskriminasi, dicurigai, dikucilkan, dihina, disengsarakan, atau dimusuhi.  Kekejaman fasisme Hitler saja tidak seseram dan sebengis dan sebegitu lama yang dilakukan rejim Orde Baru !

Sebagian kecil dari bermacam-macam kisah yang menyedihkan tentang itu semua sudah mulai diketahui, karena sudah ditulis atau diceritakan dari mulut ke mulut. Tetapi, sebagian terbesar masih tetap belum terbuka, karena berbagai sebab. Padahal, kisah-kisah sedih tentang berbagai kejahatan Orde Baru adalah khasanah yang berharga sekali bagi sejarah bangsa dan anak cucu kita.

TAP MPRS tentang larangan terhadap PKI itu dalam jangka lama sekali (terutama selama pemerintahan Orde Baru) bukan saja dipakai sebagai alat terror terhadap golongan kiri pendukung  berbagai politik  revolusioner Bung Karno, melainkan juga alat untuk mengintimidasi semua orang atau semua golongan yang berani menentang Suharto secara terang-terangan.



Orde Baru lebih biadab dari pemerintahan Belanda

Sudah menjadi pengalaman banyak orang  -- atau sudah disaksikan juga oleh banyak kalangan masyarakat  -- selama puluhan tahun Orde Baru  bahwa  rejim militer Orde Baru (dengan pendukung utamanya Golkar beserta berbagai golongan reaksioner lainnya di Indonesia dan di luarnegeri) telah banyak sekali melakukan pelanggaran HAM  yang serius dan amat berat selama puluhan tahun.

Dalam sejarah bangsa Indonésia akan tercatat (dan harus dicatat !!!) bahwa pernah ada jutaan warganegara Republik Indonesia yang dibunuhi secara biadab oleh bangsanya sendiri atau penguasa militernya sendiri, walaupun tidak mempunyai dosa sedikit pun  atau tidak melakukan kesalahan yang bagaimana pun dan tidak membuat kejahatan apa pun.

Patutlah kiranya kita sama-sama ingat bahwa pemerintahan kolonial Belanda yang menjajah negeri kita selama 350 tahun tidak pernah  membunuh jutaan orang tidak bersalah dalam dua tahun saja seperti yang dilakukan oleh militernya Suharto. Pemerintahan kolonial Belanda  (ingat :Suharto dulunya adalah serdadu kolonial Belanda - KNIL !) telah menindas pembrontakan besar PKI tahun 1926 dengan memenjarakan 13 000 anggauta dan simpatisan PKI dan mengirimkan 1 308 ke pembuangan di Digul setelah mereka kebanyakan diperiksa oleh pengadilan kolonial.

Tetapi penguasa militer di bawah Suharto sudah memenjarakan  -- secara sewenang-wenang --  ratusan ribu anggota dan simpatisan  PKI (bahkan juga orang-orang yang tidak ada hubungannya sama sekali dengan PKI) dalam jangka waktu yang lama sekali, tanpa proses pengadilan. Di antara mereka itu terdapat lebih dari 10 000 tapol yang ditahan di kamp-kamp konsentrasi  di Pulau Buru, dengan perlakuan yang jauh lebih kejam dari pada yang dialami oleh kaum pembuangan di Digul



Larangan terhadap PKI merusak jiwa bangsa

Barangkali ada orang-orang yang menganggap bahwa kalimat « larangan terhadap PKI merusak jiwa bangsa » agak keterlaluan atau berlebih-lebihan atau sembarangan saja. Sebaiknya, kalau ada orang-orang atau kalangan yang begitu itu, kita ajak merenungkan  -  dengan hati nurani yang bersih dan nalar yang sehat – berbagai hal sebagai berikut :

Larangan terhadap PKI adalah suatu pelanggaran HAM yang berat sekali yang bertentangan dengan piagam PBB yang juga ditandatangani oleh pemerintah Indonesia  Larangan ini juga bertentangan dengan jiwa UUD (Konstitusi) Republik Indonesia  Jadi, bisalah dikatakan bahwa larangan terhadap PKI merupakan kesalahan besar, atau dosa berat, terhadap rakyat dan bangsa Indonesia.

Seperti sudah kita semua rasakan , atau kita saksikan, atau kita alami sendiri, larangan terhadap PKI telah menyebabkan sebagian rakyat Indonesia memusuhi, mencurigai, menyingkirkan sebagian lainnya dari rakyat kita sendiri.

Karena adanya larangan terhadap PKI maka  -- sebagai akibatnya --ajaran-ajaran revolusioner Bung Karno dalam jangka lama sekali ikut diboikot atau dilarang juga, karena dianggap mengandung isi yang menguntungkan PKI (umpamanya, antara lain : NASAKOM, Manipol, Yo kadang yo sanak,)

Larangan terhadap PKI selain meracuni fikiran banyak orang, juga telah mendorong sebagian bangsa kita menjadi reaksioner atau kontra-revolusioner, dan ikut-ikut menyetujui segala kebiadaban yang dilakukan Suharto beserta segala macam pendukungnya (Golkar, militer, termasuk sebagian kalangan Islam)

Selama TAP MPRS nomor 25/1966 belum dicabut maka bangsa kita akan terus mengidap penyakit parah, yang merusak jiwa bangsa, yang menggerogoti persatuan bangsa, yang merugikan kekuatan revolusioner rakyat Indonesia



Apa faedahnya larangan terhadap PKI ?

Setelah larangan terhadap PKI diberlakukan sejak tahun 1966  (artinya selama hampir setengah abad) oleh penguasa militer Suharto – yang masih diteruskan oleh berbagai pemerintahan sampai sekarang--  kita semua bisa bertanya-tanya apa saja hasil positifnya  bagi negara dan bangsa, dan apa pula akibat negatifnya. Yang berikut ini adalah sekadar sedikit sumbangan untuk bahan pemikiran bersama :

Kalau  mau dikatakan bahwa larangan terhadap PKI adalah untuk keamanan masyarakat, ketertiban, dan untuk stabilisasi politik, maka pengalaman hampir setengah abad sejak 1965-1965 menunjukkan bahwa keamanan negara kita tetap sering terganggu oleh berbagai peristiwa, terutama terror  dari segolongan kecil Islam fanatik yang mau mendirikan negara Islam. Dan sama sekali bukannya oleh PKI, anasir PKI, atau PKI malam, atau « bahaya laten PKI »

Walaupun PKI tidak ada (karena dinyatakan dilarang secara resmi), kestabilan politik juga tidak pernah terwujud lama, kecuali di era Suharto ketika stabilisasi politik didasarkan  pada diktatur militer yang dijalankan dengan keras (tangan besi). Kestabilan politik di era Suharto hanyalah kestabilan politik semu, yang dipaksakan, dan  didasarkan kepada  berbagai macam terror. Kestabilan politik juga tidak ada selama pemerintahan Habibi, Gus Dur, Megawaii, dan SBY-Budiono sekarang ini.

Seperti yang sudah ditunjukkan oleh sejarah, larangan terhadap PKI (TAP MPRS 25/1966) dipakai  oleh Suharto untuk melapangkan jalan baginya dan juga bagi rejimnya untuk melakukan banyak  pelanggaran konstitusi, berbagai kejahatan kemanusiaan, pencekekan demokrasi, korupsi dan kejahatan-kejahatan lainnya, semuanya itu untuk melanggengkan  kekuasaan Suharto beserta Orde Barunya.

Larangan terhadap PKI adalah « papan-nama » yang buruk sekali  bagi bangsa dan negara Indonesia di mata dunia yang beradab. Sebab sedikit sekali negara di dunia yang melarang secararesmi dan terang-terangan adanya partai  komunis. Sedangkan di Amerika Serikat  sendiri saja partai komunis dibolehkan melakukan kegiatan-kegiatan seperti partai-partai lainnya.

Selama ada larangan terhadap PKI,  negara dan bangsa Indonesia tidak pantas, atau tidak berhak, atau tidak sah, untuk menamakan diri sebagai negara dan bangsa yang demokratis sepenuhnya dan sebenarnya.

Larangan terhadap PKI sama sekali tidak menguntungkan kepentingan rakyat Indonesia, melainkan sebaliknya, hanya menguntungkan musuh-musuh rakyat dan bangsa Indonesia, dan merugikan tujuan revolusi kemerdekaan, seperti yang dicita-citakan oleh kita bersama, sesuai dengan ajaran-ajaran revolusioner Bung Karno
