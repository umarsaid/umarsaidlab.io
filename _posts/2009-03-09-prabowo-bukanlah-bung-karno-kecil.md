---
layout: post
title: 'PRABOWO bukanlah "Bung Karno kecil"'
date: 2009-03-09
---

M enjelang makin dekatnya penyelenggaraan pemilihan legislatif dan presiden yang akan datang, kiranya menarik bagi kita untuk sama-sama menyimak kembali isi berita Gatra 7 Februari 2009, yang ringkasannya berbunyi antara lain seperti berikut :

« Tepuk riuh ratusan pendukung dan simpatisan sontak bergema memenuhi ruang Balai Sarbini, Semanggi, kala mantan politisi senior PDI Perjuangan tampil di depan podium pada HUT pertama Partai Gerakan Indonesia Raya (Gerindra), Jum`at (6/2) malam.

Permadi, yang baru tiga hari bergabung dengan Gerindra, secara khusus didaulat untuk menyampaikan sepatah dua patah kata, pada para kader, pendukung dan simpatisan partai berlambang kepala garuda emas itu. "Saya bergabung dengan Gerindra, karena sosok Prabowo-nya," katanya,

Ketua Dewan Pembina Gerindra, Prabowo Subianto, di mata Permadi, adalah Bung Karno kecil karena visi dan misi partai yang bangunnya sangat berbau Soekarnois, seperti slogan `kembali ke UUD 1945`, `Pancasila dan Bhinneka Tunggal Ika`, belum lagi `Berdikari` alias berdiri di atas kaki sendiri.

"Saya yakin, dengan kemampuan dan pengetahuannya, Prabowo pada masa datang dapat melebihi pesona Megawati Soekarnoputri," ujarnya mantap, yang lantas disambut teriakan yang meriah oleh kader, pendukung, dan simpatisan Gerindra (kutipan dari Gatra selesai).


Seandainya diucapkan oleh sembarangan orang, penyebutan bahwa Prabowo adalah “Bung Karno kecil” akan bisa dianggap remeh atau ditanggapi sebagai angin lalu saja Tetapi karena ini diungkapkan oleh Permadi, mantan “tokoh” PDI Perjuangan dan anggota DPR, yang juga Sukarnois yang biasanya lantang suaranya, dan juga sebagai seorang paranormal (tukang peramal) yang cukup dikenal, maka masalahya menjadi lain.

Apalagi ungkapan ini dikumandangkan menjelang pemilu 2009, antara lain dalam HUT partai Gerindra, yang bisa menimbulkan effek tertentu dalam opini umum. Mengingat pentingnya penyebutan Prabowo sebagai “Bung Karno kecil”, dan kemungkinan dampaknya yang menyesatkan bagi banyak orang, maka tulisan ini mencoba mengajak para pembaca untuk bersama-sama menelaahnya.

Penghinaan kepada Bung Karno

Tanpa bermaksud menghina atau merendahkan penilaian Permadi bahwa Prabowo adalah “Bung Karno kecil” (pendapat begitu itu adalah haknya sepenuhnya !), dalam tulisan ini disajikan berbagai pendekatan terhadap masalah ini, karena banyak sekali hal-hal yang perlu dipersoalkan,
atau patut difikirkan bersama.

Penyebutan Prabowo sebagai « Bung Karno kecil” merupakan penghinaan kepada Bung Karno, pemimpin besar bangsa Indonesia yang sampai sekarang belum ada tandingannya. Sebab, ditinjau dari banyak segi, seluruh sejarah hidup revolusioner Bung Karno bertentangan sama sekali atau bertolak belakang dengan sejarah hidup Prabowo. Bisa diibaratkan dengan bumi dan langit.

Kalau Prabowo adalah “Bung Karno kecil” maka ini berarti bahwa ia betul-betul harus mengerti atau menjiwai gagasan-gagasan besar dan ajaran-ajaran revolusioner Bung Karno yang tercermin dalam karya-karya agungnya, umpamanya : Indonesia Menggugat, Lahirnya Pancasila, Dibawah Bendera Revolusi, Revolusi Belum Selesai dan banyak karya bersejarah lainnya.

Artinya, kalau Prabowo memang “Bung Karno kecil” maka ia tentunya menerima sepenuhnya atau menyetujui Pancasila (versi asli Bung Karno), Berdikari, Manipol-Usdek, Tavip, Nasakom dan ajaran-ajaran Bung Karno lainnya. Tetapi apakah Prabowo sungguh-sungguh menerima sepenuhnya atau betul-betul menyetujui seluruhnya ajaran-ajaran dan gagasan-gagasan revolusioner Bung Karno adalah satu soal yang sekarang ini masih belum jelas, sehingga mengatakan bahwa Prabowo adalah “Bung Karno kecil” merupakan ungkapan yang bisa dianggap gegabah atau sembarangan.

Tidak gampang menjadi “Bung Karno kecil”

Memang, dalam pidatonya di HUT partai GERINDRA, Prabowo sudah menyebut-nyebut Bung Karno, bahkan mengutip pidato Bung Karno, yang antara lain berbunyi sebagai berikut :

“Proklamator kita , Bung Karno, pernah meramalkan hal ini terjadi.
Bahwa kita terperangkap dalam suatu era neo-kolonialisme dan
Neo-imperialisme. Terlepas dari kelebihan dan kekurangan yang beliau
miliki, pada saat ini kita harus mengakui, pandangan beliau jauh
kedepan dan terbukti bahwa kita memang sedang dalam keadaan terjajah
secara ekonomi

“Saya mengutip apa yang pernah disampaikan Proklamator kita pada sebuah
kongres di tahun 1932; "Beri aku seribu orang dan dengan mereka aku
akan menggerakkan gunung Semeru, tapi berilah aku sepuluh pemuda yang
membara cintanya kepada tanah air dan aku akan mengguncang dunia…"

Di samping itu, dalam pidatonya itu, Prabowo sudah mengutarakan pandangan-pandangan yang patriotik, yang nasionalis, yang pro rakyat kecil, yang anti-neoliberal, yang anti dominasi modal asing, yang sepintas lalu terdengar mirip-mirip dan bisa disamakan dengan pidato-pidato Bung Karno. Namun, kiranya kita tidak bisa dengan gampang-gampangan menamakan Prabowo sebagai “Bung Karno kecil” hanyalah karena ia sudah mengutip pidato-pidato Bung Karno atau bicara galak tentang neo-kolonialisme dan neo-imperialisme. Kalau begitu, maka setiap “tokoh” yang berani dan rajin mengutip Bung Karno atau bicara lantang menentang imperialisme lalu bisa dinamakan “Bung Karno kecil” ?.

Rejim Suharto adalah musuh besar Bung Karno

Memang, adalah suatu hal menarik untuk sama-sama kita perhatikan bahwa Prabowo, yang dulunya Letnan Jenderal TNI-AD, dan lama menjadi pimpinan Kopassus dan Komandan KOSTRAD -- satuan-satuan militer yang merupakan tulang punggung utama kekuasaan Suharto – sudah mengangkat Bung Karno dalam pidato-pidatonya. Terlepas dari segala pertimbangan atau latar belakang sebenarnya mengapa ia menyebut-nyebut Bung Karno, yang bisa memberikan kesan bahwa ia menghargai pemimpin besar bangsa kita ini, ucapannya yang begitu itu adalah suatu perkembangan yang perlu mendapat perhatian kita semua.

Sebab, apakah sikapnya mengenai Bung Karno itu betul-betul datang dari lubuk hatinya yang tulus dan juga lahir dari kesadaran politiknya yang dalam , dan apakah sebaliknya hanya sebagai lamis bibir dan salah satu siasat saja dalam rangka meraih simpati sebagian opini publik, ini masih merupakan tanda tanya yang besar. Ini mengingat bahwa ia sampai kira-kira sepuluh tahun yang lalu, masih menjadi satu dengan rejim militernya Suharto, yang dalam hakekatnya adalah musuh besar Bung Karno.,dan bahkan, pengkhianat besar Bung Karno.

Di samping itu perlulah kiranya kita ingat juga bahwa Prabowo sudah pensiun dari jabatan kemiliterannya, dan ia bicara sebegitu baik tentang Bung Karno itu dalam kapasitasnya sebagai Ketua Umum partai GERINDRA. Namun, mengingat sejarah kemiliterannya yang sangat menonjol di kalangan TNI (terutama AD) di masa lalu dan kemungkinan masih adanya pengaruh tertentu sekarang ini di kalangan militer (yang masih aktif maupun yang sudah pensiun) maka sikapnya mengenai Bung Karno (kalau benar-benar tulus dan tidak palsu !!!) merupakan hal yang baru dalam masalah hubungan TNI (AD) dengan Bung Karno.

Fenomena yang demikian ini menunjukkan bahwa sekarang ini, sebagian dari kalangan pendukung Orde Baru, (baik dari kalangan sipil maupun militer) makin sadar akan kebenaran berbagai gagasan-gagasan Bung Karno mengenai masalah-masalah bangsa. Kalau (sekali lagi : kalau !) berbagai ungkapan Prabowo mengenai Bung Karno itu memang betul-betul merupakan kesedaran politik, maka akan bisa mempunyai arti yang tidak kecil. Sebab, bisa merupakan dorongan akan adanya perubahan sikap kalangan militer terhadap Bung Karno. Kita melihat dengan jelas bahwa selama di bawah pimpinan Suharto (dan juga sampai sekarang) militer (terutama TNI AD) sudah dijadikan musuh besar Bung Karno, berikut golongan kiri pendukungnya (termasuk PKI).

Tidak mengenal ajaran-ajaran Bung Karno

Dari segi sejarah perjuangan bangsa Indonesia, kejahatan atau dosa besar Suharto (beserta jenderal-jenderal pendukungnya) adalah digiringnya dan dirubahnya TNI menjadi penentang politik dan ajaran-ajaran revolusioner Bung Karno. Selama 32 tahun Orde Baru (dan diteruskan sampai sekarang) boleh dikatakan bahwa TNI dibikin tidak mengenal sama sekali, bahkan membenci Bung Karno beserta ajaran-ajaran atau gagasan-gagasan revolusionernya. Dengan digiringnya TNI ke dalam wilayah politik anti-Sukarno, maka TNI menjadi terpisah dari tradisi revolusioner rakyat Indonesia, dan akhirnya hanya menjadi alat penggebuk rejim militer yang reaksioner dan pro-imperialis, yang dipakai untuk membunuhi dan memenjarakan jutaan bangsanya sendiri secara sewenang-wenang dan bahkan secara biadab.

Suharto beserta jenderal-jenderal pendukungnya telah merusak TNI dan menjadikannya sebagai kekuatan yang menentang Bung Karno beserta segala ajaran-ajaran revolusionernya. Padahal adalah jelas sekali bahwa menentang Bung Karno beserta ajaran-ajaran revolusionernya berarti menentang atau mengkhianati perjuangan rakyat Indonesia. Bung Karno adalah pengejawantahan perjuangan revolusioner rakyat Indonesia menuju masyarakat adil dan makmur, masyarakat sosialisme à la Indonesia. Sampai sekarang tidak ada pemimpin Indonesia yang mempunyai pendirian yang seteguh Bung Karno mengenai hal ini, dan yang visinya sejauh dan seterang yang telah digelarnya selama puluhan tahun.

Karenanya, dilihat dari berbagai segi, penyebutan Prabowo sebagai “Bung Karno kecil” adalah sesuatu yang sama sekali tidak benar, hanya karena ia telah kelihatan menghargai atau menghormati Bung Karno. Sekali lagi, kalau (harap diperhatikan di sini, kalau !!!) sikapnya itu betul-betul timbul karena kesedaran politik dan hati yang tulus, memang merupakan perkembangan yang menarik dan penting, Walaupun begitu, sampai menyebut-nyebutnya sebagai “Bung Karno kecil” adalah ungkapan yang kebablasan, adalah penamaan yang kelewatan atau keterlaluan, dan ........yang bisa menyesatkan banyak orang.

Bung Karno belum ada tandingannya

Sebab, pemimpin besar rakyat Indonesia, Bung Karno, – yang sampai sekarang keagungan berbagai fikiran-fikirannya mengenai bangsa belum ada tandingannya ! – tidaklah bisa sama sekali dengan gampang-gampangan .disamakan atau disejajarkan dengan “tokoh-tokoh” Indonesia lainnya, apalagi dengan “tokoh-tokoh” semasa Orde Baru ( dan juga sesudahnya). Dibandingkan dengan kebesaran sejarah hidup revolusioner Bung Karno maka nampaklah dengan jelas sekali betapa kecilnya, atau betapa kerdilnya, sosok-sosok para tokoh itu semuanya, termasuk Prabowo.

Kalau kita teliti kembali sejarah perjuangan bangsa kita dengan seksama, maka nyatalah bahwa Bung Karno adalah satu-satunya di antara para pemimpin yang paling dicintai oleh sebagian terbesar rakyat kita yang terdiri dari berbagai suku, agama, asal ras atau bangsa, dan disegani atau dihormati oleh banyak pemimpin dan rakyat di Asia, Afrika, Amerika Latin yang berjuang melawan imperialisme dan kolonialisme.

Bung Karno sudah menjadi tokoh besar rakyat Indonesia, ketika masih muda-belia mengucapkan pidato pembelaannya di depan pengadilan kolonial Belanda (ingat: Indonesia Menggugat) dan mendirikan Partai Nasional Indonesia dan kemudian dibuang ke Endeh dan Bengkulu oleh pemerintahan kolonial (perlu sekali dicatat bahwa Suharto pada waktu itu adalah serdadu kolonial KNIL)

Berkat besarnya peran dan sumbangannya dalam gerakan untuk mencapai kemerdekaan bangsa yang kelihatan melebihi dari pada peran dan sumbangan para tokoh lainnya maka secara mutlak ia dipilih (bersama Bung Hatta) untuk menjadi proklamator kemerdekaan bangsa Indonesia.

Tokoh besar secara nasional dan juga internasional
Selama sekitar 20 tahun memimpin rakyat sebagai presiden (antara 1945-1965), Bung Karno telah membikin negara dan rakyat Indonesia menjadi terkenal, dihormati atau disegani oleh banyak rakyat di dunia, berkat sikap politiknya yang revolusioner, anti-kolonialisme dan anti-imperialsime, yang sudah disandangnya sejak usia mahasiswa. Karenanya, Bung Karno menjadi simbul terbesar dalam perjuangan bangsa Indonesia, di samping menjadi tokoh raksasa bagi rakyat-rakyat negeri lain yang sedang melakukan perjuangan. Dengan kalimat lain, Bung Karno adalah sekaligus besar secara nasional dan juga besar secara internasional. Dilihat dari sudut ini, maka jelas sekalilah betapa besar bedanya antara sosok Bung Karno dengan Suharto atau pemimpin-pemimpin Indonesia lainnya.

Kebesaran Bung Karno kelihatan bukan hanya sebagai proklamator kemerdekaan bangsa saja, tetapi juga sebagai promotor Konferensi Asia-Afrika di Bandung (tahun 1955), juga sebagai tokoh penting dalam Gerakan Non-blok (bersama Tito), juga sebagai promotor berbagai pertemuan Asia-Afrika, juga sebagai tokoh yang membikin kagetnya banyak kalangan di dunia dengan keluarnya Indonesia dari PBB dan seruan “Go to hell with your aid” yang ditujukan terutama kepada AS. Kebesaran Bung Karno di panggung internasional juga nampak ketika ia mengucapkan pidatonya yang bersejarah di depan sidang umum PBB tahun 1960 (“To build the World Anew”) yang mendapat sambutan luar biasa hangatnya dari hadlirin.

Patutlah kiranya selalu sama-sama kita ingat bahwa dalam sejarah bangsa kita, Bung Karno adalah tokoh raksasa politik di skala internasional, yang kalibernya sejajar atau setara bahkan ada juga yang melebihi tokoh-tokoh dunia seperti (antara lain): Gamal Abdul Nasser (Mesir), Josip Bros Tito (Yugoslavia), Jawaharlal Nehru (India), Bandaranaike (Srilanka), Ho Chi Minh (Vietnam), Mao Tse Tung dan Zhou Enlai (Tiongkok), Fidel Castro (Kuba). Tidak ada tokoh Indonesia lainnya (sampai sekarang) yang mempunyai sosok atau stature sebesar dan setinggi Bung Karno (artinya, Prabowo juga tidak).

Sayangnya, Bung Karno, pemimpin rakyat yang besar dan agung inilah yang sudah dikhianati oleh Suharto beserta jenderal-jenderal dan pendukungnya. Kejahatan dan dosa besar ini patut dicatat dan selalu diingat oleh kita semua, dan juga oleh anak-cucu kita!
