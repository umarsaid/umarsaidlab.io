---
layout: post
title: Garuda (Bung Karno) di dadaku, Garuda (Bung Karno ) kebanggaanku !
date: 2010-12-30
---

Berikut di bawah  ini disajikan sekadar  penjelasan mengapa judul tulisan kali ini disusun dengan kalimat seperti tersebut di atas. Intinya adalah bahwa, dilihat dari  berbagai segi dan sudut pandang, Garuda Indonesia adalah Bung Karno. Mengapa ? Silakan simak selengkapnya. Terimakasih



Luar biasa !  Sungguh luar biasa, hebatnya antusiasme rakyat Indonesia untuk mendukung tim nasional Garuda kita yang menghadapi tim Malaisia dalam pertandingan final AFF, baik yang di Kuala Lumpur (tanggal 26 Desember) maupun di Jakarta (tanggal 29 Desember). Boleh dikatakan bahwa seluruh negeri bergolak dan seperti mendidih serta menggelora, lebih dari pada yang sudah-sudah. Fenomena yang luar biasa ini  (!)  patut untuk sama-sama kita telaah, apa artinya bagi kehidupan bangsa dan negara kita, baik untuk dewasa ini maupun untuk masa datang



Padahal, di luar dugaan banyak orang, timnas Indonesia telah dikalahkan oleh Malasia dengan angka yang cukup besar, yaitu 3 lawan 0, dalam pertandingan final  babak pertama di Kuala Lumpur tanggal 26 Desember yang lalu itu. Sudah tentu, kekalahan ini membikin banyak orang kecewa. Namun, ternyata antusiasme dan dukungan kepada Timnas Garuda masih tetap besar dan luas sekali , dengan harapan bahwa kemenangan akan diperoleh dalam pertandingan final babak kedua di Jakarta tanggal 29 Desember ini. Dan ternyata bahwa harapan ini tercapai juga, dengan kemenangan Indonesia, dengan angka 2 lawan 1.

Dengan hasil yang demikian maka Malaisialah yang  berhasil menggondol piala AFF, karena jumlah total gol dalam kedua pertandingan final ini menjadi 4 untuk Malaisia dan 2 untuk Tim Nasional Garuda Indonesia. Sudah tentu, hal yang demikian ini mengecewakan banyak orang, karena timnas Garuda sudah mengalahkan 6 kali berbagai kesebelasan negara-negara lain dan hanya dikalahkan satu kali.

Antusiasme dan dukungan rakyat yang luar biasa

Namun demikian, banyak hal yang menunjukkan bahwa kebangkitan dunia sepakbola kita merupakan sumbangan penting bagi tumbuhnya kembali nasionalisme, mengobarkan lagi kebanggaan menjadi orang Indonesia, menggelorakan semangat persatuan di kalangan rakyat. Hal-hal yang demikian inilah yang jarang sekali kita saksikan di Indonesia akhir-akhir ini.

Kita lihat di TV puluhan ribu orang selama berhari-hari berebut untuk membeli tiket di Jakarta, kepanasan dan kehujanan, dan banyak yang harus antre panjang sampai lebih dari 10 jam. Bahkan ada yang pingsan atau jatuh terhimpit karena berdesak-desakan, atau yang terpaksa tidur berhari-hari di tempat. Contoh lainnya adalah bahwa di Medan saja ada gerakan untuk mengumpulkan sejuta tandatangan untuk mendukung timnas. Supporter-pun berdatangan ke Jakarta dari Sumatera, dan berbagai daerah di Jawa, Sulawesi. Kali ini sepakbola membuat banyak orang di negeri kita betul-betul menjadi  tergila-gila  atau “kesurupan”, lebih dari pada yang sudah-sudah.

Kita lihat bahwa dimana-mana rakyat  senang dan bangga sekali bahwa sepakbola Indonesia yang sejak tahun 1991 tidak menunjukkan kemampuan untuk unggul di arena Asia, akhir-akhir ini meraih kemenangan berturut-turut menghadapi kesebelasan negara-negara tetangga.  Sudah terlalu lama kesebelasan Indonesia sering dikalahkan oleh kesebelasan Singapura, Malaisia, Thailand, Filipina, Vietnam, Taiwan atau negara-negara lainnya. Sehingga banyak orang mengatakan bahwa selama ini mereka malas atau malu menonton pertandingan kesebelasan Indonesia melawan negara lainnya.

Sekarang ini situasi sudah berubah. Animo kalangan luas untuk menonton pertandingan sepakbola naik membubung tinggi, sehingga membikin banyak orang geleng-geleng kepala, termasuk di kalangan pencinta sepakbola sendiri. Ini kelihatan nyata pada tingkah laku dan wajah berbagai kalangan masyarakat di mana-mana di seluruh negeri  yang mengenakan baju kaos merah berlambang Garuda, dan meneriak-riakkan lagu “Garuda di dadaku, Garuda kebanggaanku”!

Kebanggaan menjadi orang Indonesia timbul kembali

Kebanggaan menjadi orang Indonesia timbul kembali, sesudah puluhan tahun bangsa kita dipandang rendah oleh berbagai bangsa lain akibat banyaknya masalah besar dan parah yang selalu dihadapi rakyat selama puluhan tahun. Di antara masalah-masalah besar dan parah itu adalah pembantaian jutaan orang tidak bersalah, yang umumnya terdiri dari orang-orang kiri pendukung politik Bung Karno, pemenjaraan tanpa pengadilan ratusan ribu orang selama puluhan tahun, kediktaturan militer yang sewenang-wenang, korupsi yang merajalela (sampai sekarang !!!), menggilanya mafia hukum dan mafia peradilan, dan kebobrokan moral di banyak bidang kehidupan.

Sebagian dari masalah-masalah itu masih terrus menjangkiti bangsa kita dewasa ini, yang diperparah oleh bermacam-macam persoalan-persoalan yang buruk di bidang sosial ekonomi, yang menyebabkan banyaknya pengangguran dan kemiskinan. Sebagian terbesar rakyat kita hidup dalam keadaan sempit atau kekurangan. Sementara itu penyelengggaraan negara dan kehidupan politik dewasa ini juga semrawut, kacau  atau ambur-adul. Di tengah-tengah situasi yang begitu itulah berlangsung kebangkitan kembali dunia olahraga sepakbola kita. Karena itu, sepakbola merupakan pelipur lara bagi banyak orang, yang bisa melupakan sejenak berbagai persoalan yang sedang dihadapi bangsa.

Agaknya, ketandusan perasaan kebanggaan yang sudah begitu lama menimpa rakyat kita, dan kerinduan akan adanya prestasi besar yang bisa dipersembahkan bangsa, adalah salah satu di antara berbagai faktor yang menyebabkan begitu banyaknya orang yang sudah berhari-hari berkerumun disekitar Gelora Bung Karno, dan memasang lambang Garuda Pancasila di baju mereka, serta menyanyikan lantang lagu “Garuda di dadaku, Garuda kebanggaanku”. Jelaslah bahwa kali ini rakyat merasa betul-betul  memiliki negeri ini, bahwa kali ini rakyat betul-betul menunjukkan kecintaan mereka kepada Sang Saka Merah Putih. Sudah lama sekali, sejak lebih dari 45 tahun, tidak pernah kelihatan lagi fenomena yang begitu mempesonakan banyak kalangan rakyat seperti itu.

Yang menggembirakan para pencinta Bung Karno

Satu hal yang agaknya menarik untuk diperhatikan adalah fenomena yang mengindikasikan bahwa  di antara berbagai kalangan yang gembira dengan kebangkitan kembali dunia sepakbola sekarang ini terdiri dari para pencinta Bung Karno. Bukan saja bahwa pertandingan-pertancingan semi final dan pertandingan final dengan Malaisia diadakan di Gelora Bung Karno (yang didirikan atas gagasan beliau) , melainkan juga banyaknya orang yang memasang lambang Garuda di baju mereka.

Dengan banyaknya orang memakai kaos merah yang berlambang Garuda dan  memenuhi Gelora Bung Karno dan sekitarnya (dan juga di banyak tempat di seluruh tanah air), dan menyanyikan “Garuda di dadaku”, maka hari-hari pertandingan di Gelora Bung Karno ini sudah menjadi semacam pesta besar rakyat untuk mengelu-elukan atau mengenang kembali Bung Karno. Mungkin, bagi sebagian orang, hubungan antara Bung Karno dan lambang Garuda tidak begitu jelas. Tetapi, umumnya, bagi para pencinta Bung Karno jelas sekali.

Bagi para pencinta Bung Karno (dan bagi banyak orang lainnya) lambang Garuda yang berisi simbul-simbul Pancasila adalah  pengejawantahan dari jatidiri Bung Karno. Bung Karno-lah penggagas Pancasila, ideologi dan dasar negara, dan pedoman besar seluruh rakyat dan bangsa kita, yang gambar-gambarnya tercantum dalam lambang Garuda itu. Karena itu, lambang Garuda itu bernama lengkapya Garuda Pancasila.  Di bawah pimpinan Bung Karno-lah gambar lambang Garuda Pancasila digarap dan diselesaikan oleh suatu panitia, dan akhirnya disempurnakan oleh Bung Karno.

Di samping itu, dalam lambang Garuda Pancasila juga tercantum dengan gagah dan jelas Bhinneka Tunggal Ika, motto agung atau semboyan luhur seluruh bangsa dan negara kita yang terdiri dari ratusan suku dan bahasa daerah, berbagai agama dan kepercayaan serta 17 000  pulau. Di bagian bawah gambar Garuda Pancasila itu kelihatan kaki burung perkasa ini mencengkeram pîta yang bertuliskan  Bhinneka Tunggal Ika, kalimat yang berasal dari kitab kuno Mpu Tantular  dan berarti : Berbeda-beda tetapi tetap satu jua.

Bung Karno adalah Garuda, Pancasila, dan Bhinneka Tunggal Ika

Bung Karno bukanlah pencipta Bhinneka Tunggal Ika, tetapi sejarah sudah menunjukkan bahwa sosok Bung Karno adalah pengejawantahan isi semboyan yang luhur ini. Fikiran-fikiran besar Bung Karno, pandangan hidupnya dan ajaran-ajarannya mencerminkan Bhinneka Tunggal Ika ini. Sikapnya sejak ia umur 25 tahun untuk mempersatukan seluruh bangsa dalam perjuangan sudah diperlihatkannya dalam tulisannya yang terkenal “Nasionalisme, Islamisme dan Marxisme” (tahun 1926), juga petunjuk bahwa Bhinneka Tunggal Ika betul-betul menjadi darah daging atau “tali-nyowo” Bung Karno, sampai wafatnya dalam tahanan Suharto.

Dewasa ini, puluhan juta rakyat Indonesia memasang lambang Garuda Pancasila dibaju mereka atau menyanyikan lagu “Garuda di dadaku”. Walaupun sebagian dari mereka  itu mungkin saja tidak tahu apa arti yang sebenarnya lambang-lambang itu dan arti penting hubungannya dengan sosok sejarah Bung Karno, kegandrungan mereka kepada Garuda Pancasila  sudah merupakan fenomena penting yang positif sekali bagi bangsa.

Adalah suatu hal yang sudah patut sangat menggembirakan kita semua bahwa sebagian besar rakyat kita menunjukkan kecintaan mereka kepada tim nas kita, kepada sang Merah Putih, kepada Garuda Pancasila, kepada Bhinneka Tunggal Ika, walaupun tidak menyebut-nyebut nama Bung Karno, oleh karena berbagai sebab

Sebab, pada hakekatnya, atau pada akhirnya, mereka yang betul-betul (sekali lagi : betul-betul !!!) mencintai  Merah Putih, Garuda Pancasila, dan Bhinneka Tunggal Ika berarti  juga mencintai atau menghormati Bung Karno. Tidak bisa lain ! Orang tidak bisa mengatakan betul-betul cinta kepada Garuda Pancasila atau hormat kepada Bhinneka Tunggal Ika, tetapi juga sekaligus bersikap anti Bung Karno atau melawan ajaran-ajaran revolusionernya ! Sebab, seperti sudah ditunjukkan secara jelas sekali oleh sejarah bangsa kita, Bung Karno adalah satu dan senyawa dengan Merah Putih, dengan Pancasila, dengan Bhinneka Tunggal Ika, dengan Indonesia.

Garuda adalah pilihan utama Bung Karno

Sampai-sampai seorang tokoh politik (alm. Subadio Sastrosatomo, “gembong” PSI) pernah menerbitkan suatu buku kecil yang diberi judul “Indonesia adalah Sukarno, dan Sukarno adalah Indonesia”. Oleh karena itu, dapatlah kiranya juga diartikan bahwa pesta rakyat besar-besaran  di seluruh negeri sekarang ini untuk mendukung tim nasional,  dengan dimeriahkan dimana-mana lagu “Garuda di dadaku, Garuda kebanggaanku”  adalah   -- pada hakekatnya, meskipun tidak langsung -- juga pesta besar rakyat untuk menghormati Bung Karno.

Bagi para pencinta Bung Karno, ketika melihat burung Garuda  yang terdapat dalam lambang Bhinneka Tunggal Ika berarti juga seperti melihat sosok Bung Karno sendiri. Hal yang demikian.bisa dimengerti dan wajar-wajar saja. Sebab, bukan saja Bung Karno memang tokoh yang menyukai Garuda sebagai simbul keperkasaan atau kebesaran dan kejayaan, namun juga sebagai sesuatu yang gagah dan berani dalam perjuangan. Itulah sebabnya Bung Karno sering menyebut-nyebut Garuda dalam pidato-pidatonya. Bukan pemimpin-pemimpin Indonesia lainnya, termasuk bukan Suharto atau orang-orang lain sejenisnya. Dan Bung Karno pulalah yang memilih nama Garuda untuk maskapai penerbangan kita, yaitu Garuda Indonesian Aîrways.

Tentu saja, bagi para pencinta Bung Karno  (dan juga bagi banyak orang lainnya) pemilihan nama Tim Nasional Garuda untuk tim nasional adalah kebanggaan tersendiri Sebab, bagi mereka, ini adalah  salah satu manifestasi dari cita-cita Bung Karno untuk menjadikan olahraga Indonesia salah satu dari 10 olahraga bangsa yang teratas di dunia. Cita-cita ini sudah mulai diusahakan direalisasi dengan penyelenggaraan Asian Games (tahun 1961) dan  Games of the New Emerging Forces (GANEFO) dalam tahun 1963  (di Jakarta)  yang besar, megah  dan menghebohkan dunia olahraga internasional waktu itu.

Gemuruh  Gelora Bung Karno akan berkumandang terus

Sekarang, pertandingan-pertandingan untuk memperebutkan piala AFF sudah selesai, dan kehidupan rakyat sehari-hari akan kembali seperti biasanya lagi. Yang penuh dengan masalah-masalah besar dan parah di bidang politik, ekonomi, sosial dan moral (!) dan korupsi yang tetap merajalela( !!!). Namun begitu, agaknya,  pertandingan-pertandingan memperebutkan piala AFF 2010 yang dilakukan oleh Timnas Garuda secara gemilang akan tetap menjadi kenang-kenangan  indah yang membanggakan banyak kalangan rakyat Indonesia.

Sementara itu, Gelora Bung Karno akan tetap menjadi tanda kenang-kenangan kepada bapak bangsa yang sejak lama sudah mencintai dunia olahraga kita. Lambang Garuda Pancasila akan tetap dicintai oleh sebagian terbesar rakyat Indonesia, terutama kalangan muda (!!!) dan pencinta sepakbola. Dan nyanyian « Garuda di dadaku, Garuda kebanggaanku »  masih akan sering disuarakan.

Gemuruh Gelora Bung Karno akan berkumandang terus, dan makin lama akan makin nyaring dan makin besar, untuk menyongsong datangnya perubahan besar dan fundamental di tanah air kita.
