---
layout: post
title: Para korban Orba, tulislah kisah hidupmu!
date: 2003-02-21
---

Sejarah rezim militer Suharto dkk yang dinamakan Orde Baru merupakan halaman hitam dalam sejarah bangsa Indonesia. Selama periode inilah Republik Indonesia telah dirusak oleh tokoh-tokoh TNI-AD yang bersekongkol dengan Golkar dan sebagian golongan Islam. Selama lebih dari 32 tahun demokrasi telah dicekek, DPR dan MPR dibungkam, oposisi dihancurkan, dan suara kritis dibabat.

Boleh dikatakan bahwa selama periode yang panjang itu rakyat tidak dibolehkan mempunyai suara apa-apa, bahkan mengeluh secara terang-terangan pun tidak bisa. Karena “wakil-wakil” mereka dalam DPR, atau para tokoh dalam masyarakat, atau para intelektual, atau wartawan dan pengarang , umumnya takut untuk bersuara. Banyak juga di antara mereka yang mengabdi kepada Orde Baru karena yakin akan kebenaran politiknya. Tapi, ada juga yang melulu hanya karena pertimbangan “perut keluarga”. Jadi, karena terpaksa, demi cari makan.

## Pembungkaman Mulut Selama 32 Tahun

Selama 32 tahun Orde Baru telah menjalankan terror negara dan persekusi politik yang intensif, sistematik dan menyeluruh. Dengan dalih momok palsu “bahaya laten PKI” segala kritik atau suara yang dianggap menentang Suharto dkk ditindak. Oleh karena itu, walaupun selama puluhan tahun terjadi pelanggaran HAM besar-besaran, KKN serta penyalahgunaan kekuasaan merajalela di segala bidang, boleh dikatakan bahwa semua orang terpaksa tutup mulut.

Itulah sebabnya maka selama puluhan tahun masalah pembantaian besar-besaran atas jutaan orang tidak bersalah dalam tahun 65 tidak pernah jadi persoalan yang bisa dibicarakan terang-terangan oleh umum. Dalam jangka lama pers Indonesia (atau media massa lainnya) juga bungkam saja terhadap masalah yang menyedihkan dan sekaligus mengerikan ini. Padahal, masalah ini adalah masalah ketidak-adilan dan pelanggaran HAM yang amat besar.

Selama puluhan tahun para korban Orde Baru dipaksa harus berdiam diri tentang kezaliman yang telah ditimpakan kepada puluhan juta orang. Mereka dilarang memprotes tentang para ayah atau para ibu yang dibunuh begitu saja tanpa proses hukum. Mereka dicegah mengeluh tentang anak yang dihilangkan atau diculik oleh pasukan bersenjata. Mereka dipaksa diam saja walaupun paman atau kemenakan mereka dipenjarakan selama bertahun-tahun. Padahal, mereka tidak bersalah apa-apa, atau tidak berdosa.

## Sekarang Bukalah Suara

Sekarang, ketika Suharto sudah bisa dipaksa turun dari jabatannya, dan ketika Orde Baru sudah mulai dinajiskan orang banyak, adalah kesempatan bagi para korban Orde Baru untuk “buka suara”. Adalah sangat perlu dan sangat baik bagi bangsa kalau para korban Orde Baru dapat mengutarakan pengalaman mereka sejujur-jujurnya dan sebebas-bebasnya. Ini merupakan kesempatan bagi bangsa kita untuk belajar mengenal kejahatan dan kesalahan Orde Baru lebih baik dan lebih dalam.

Pengalaman para korban Orde Baru adalah sangat berharga bagi sejarah bangsa kita. Banyak hal mengenai korban Orde Baru ini yang perlu diketahui oleh generasi bangsa kita yang sekarang, dan lebih-lebih lagi bagi generasi kita yang akan datang. Dengan kalimat lain, banyak sekali hal-hal yang bisa - dan perlu - diceritakan tentang apa yang dialami korban Orde Baru selama itu.

Untuk itu sekarang perlu diciptakan, bersama-sama, segala kemungkinan atau kesempatan bagi para korban Orde Baru mengabadikan pengalaman atau kisah mereka. Kepada mereka dianjurkan untuk menulis sebisanya atau seadanya tentang seluk-beluk kehidupan mereka sebagai korban Orde Baru. Tulisan semacam ini bukan saja amat penting sebagai kenang-kenangan pribadi masing-masing, tetapi juga sebagai kenang-kenangan kolektif masyarakat. Tulisan-tulisan ini bisa merupakan dokumen bangsa.

Hal ini terasa lebih penting lagi mengingat bahwa sebelum para pelakunya dan para saksinya menghilang karena mati, atau sebelum ingatan semakin menjadi tumpul oleh karena jalannya waktu.

## Khasanah Berharga Bangsa

Para korban Orde Baru, dari segala golongan, suku, agama, dan keyakinan politik, perlu dianjurkan untuk lebih berani menulis tentang pengalaman mereka. Perlu disadari bersama-sama bahwa pengalaman mereka adalah khasanah berharga bangsa. Sayang sekali kalau khasanah ini dibiarkan hilang begitu saja ditelan waktu, atau dilupakan. Tulisan-tulisan ini, dalam segala bentuk dan isinya, bisa nantinya merupakan bahan untuk penelitian sejarah, penulisan novel atau penggubahan scenario sandiwara atau film, atau karya-karya lainnya.

Pengalaman para korban Orde Baru adalah gudang raksasa yang berisi timbunan beraneka ragam bahan pendidikan politik, moral, perikemanusiaan, agama, dan nilai-nilai sosial lainnya. Pada waktunya, bahan-bahan ini bisa digali untuk keperluan jang macam-macam. Memang, selama ini sudah ada sejumlah buku, brosur, atau penerbitan-penerbitan bentuk lainnya, yang berisi tulisan-tulisan tentang para korban Orde Baru. Tetapi dibandingkan dengan apa yang terjadi sesungguhnya, apa yang sudah ditulis selama ini masih belum apa-apa.

Masih banyak sekali para korban Orde Baru yang masih belum menyadari betapa pentingnya pengalaman mereka untuk diingat-ingat, kemudian dicatat dan ditulis. Kiranya sekarang perlu dilancarkan suatu gerakan massal untuk saling mengingatkan bahwa menulis tentang pengalaman kehidupan selama Orde Baru adalah penting untuk bangsa. Tulisan itu tidak mesti hanya berisi protes terhadap penderitaan dan gugatan terhadap kesedihan yang ditimbulkan oleh keganasan para penguasa Orde Baru saja, melainkan juga tentang bagaimana mengatasi kesulitan-kesulitan. Dalam perjalanan hidup para korban Orde Baru beserta keluarga (anak dan istri) tentu ada hal-hal baik yang dialami, umpamanya tentang orang-orang atau organisasi yang pernah menolong mereka. Semua ini merupakan bahan pendidikan politik dan moral.

## Mengapa Perlu Menulis

Mengingat pentignya tulisan tentang pengalaman para korban Orde Baru sebagai khasanah bangsa, maka perlu kiranya adanya seruan atau imbauan kepada semua fihak untuk melahirkan sebanyak mungkin inisiatif ke arah itu. Berbagai Ornop atau sebagian besar LSM atau organisasi buruh, tani, pemuda, mahasiswa, perempuan bisa mendorong para korban Orde Baru menulis (atau menceritakan) pengalaman mereka. Karena membuat tulisan tidaklah mudah bagi setiap orang, maka sebagian para koban Orde Baru terpaksa harus dibantu mengerjakannya. Umpamanya dengan menyuruh mereka bercerita, dan cerita mereka itu kemudian direkam dalam tape.

Sebagian korban Orde Baru barangkali masih mempunyai keengganan atau ketakutan untuk menulis (atau menceritakan segala sesuatunya ) terang-terangan dengan nama asli. Bagi kasus yang demikian sebaiknya digunakan nama samaran dan nama tempat palsu. Yang penting yalah supaya pengalaman itu nantinya, pada waktunya, bisa diketahui oleh orang banyak. Tulisan ini bisa dibuat dalam bentuk macam-macam. Untuk menembus blokade pers (atau media lainnya), yang pada umumnya masih belum bisa meninggalkan pola berfikir Orde Baru, saluran Internet harus dipergunakan sebanyak mungkin.

Tulisan ini tidak mesti panjang-panjang, tiga sampai lima halaman sudah cukup, sebagai permulaan. Kemudian, kalau kondisi memungkinkan, bisa dilengkapi dengan tulisan lebih panjang. Yang penting ialah dimulainya pekerjaan ini, walaupun sederhana dan terbatas. Sebab, kalau banyak orang sudah melakukannya, maka akan lahir dari lautan tulisan-tulisan ini karya-karya yang bagus, bermutu dan besar. Segi penting lainnya dari gerakan massal tulisan korban Orde Baru ini ialah segi psikologis. Para korban Orde Baru harus bisa mencampakkan jauh-jauh perasaan ”minder”, atau perasaan “bersalah”, atau perasaan bahwa mereka adalah bukan warganegara RI seperti lainnya. Mereka harus berani menyatakan bahwa mereka selama ini adalah orang-orang tidak bersalah yang dizalimi oleh Orde Baru. Mereka adalah orang-orang tidak bersalah yang diperlakukan secara tidak adil selama puluhan tahun. Karena itu mereka berhak menggugat.

## Arti Penting Tulisan Para Korban Orde Baru

Tulisan-tulisan tentang kisah para korban Orde Baru ini penting karena masih saja ada orang atau golongan yang, sampai sekarang, belum sadar benar-benar bahwa rezim militer Suharto, yang bersendikan Golkar dan sebagian TNI-AD, adalah rezim yang diktatorial, yang korup, yang menginjak-injak HAM selama puluhan tahun.

Tulisan-tulisan ini akan selalu mengingatkan kembali semua orang bahwa bangsa Indonesia telah pernah mengalami masa gelap itu, dan bahwa masa yang membawa kesengsaraan dan penderitaan bagi banyak orang itu tidak boleh kembali lagi. Banyak sekali kisah besar yang masih terpendam, yang melambangkan keagungan jiwa, dan tindakan-tindakan terpuji. Kisah ibu-ibu yang berhasil membesarkan anak-anak dengan susah-payah karena ditinggalkan ayah yang dipenjarakan. Atau kisah anak-anak yang dengan sukses gemilang telah berjuang memperoleh diploma berkat bantuan kemanusiaan seorang pendeta. Semua ini patut dipersembahkan kepada bangsa.

Memang sudah cukup banyak buku-buku yang sudah ditulis mengenai peristiwa 65 dan Orde Baru, tetapi yang ditulis oleh para korban sendiri masih belum banyak. Apa yang sudah ditulis itu tidak atau belum sepadan dengan besarnya persoalan yang pernah dihadapi atau dialami oleh para korban Orde Baru. Masih terlalu banyak dan terlalu beragam kisah-kisah yang menarik yang bisa dan perlu didengar dari mereka. Mereka berjumlah jutaan -bahkan puluhan juta berikut keluarga mereka – dan telah terpaksa mengalami berbagai percobaan hidup selama puluhan tahun. Umpamanya kisah sekitar pembantaian massal, pemenjaraan sewenang-wenang, seluk-beluk surat bebas G30S, bersih lingkungan, litsus, wajib lapor, “bahaya laten PKI” dan lain-lainnya.

Sekarang Suharto sudah jatuh, tetapi sisa-sisa pengaruh politik rezim militernya masih kuat di berbagai bidang. Pemerintahan sudah diganti berkali-kali, dari pemerintahan di bawah Presiden BJ Habibi, Gus Dur dan sekarang ini Megawati-Hamzah. Tidak lama lagi akan ada Pemilu dan Presiden dan pemerintahan akan berganti lagi. Tetapi, masih belum bisa dipastikan apakah masalah rehabilitasi korban Orde Baru akan dapat diselesaikan oleh pemerintahan yang akan datang.

Tanpa mempedulikan siapa yang akan menjadi presiden dan pemerintahan yang bagaimana yang akan dibentuk nantinya, satu hal yang sudah jelas ialah bahwa para korban Orde Baru harus terus berusaha bersama-sama “buka suara”, untuk menuntut rehabilitasi. Sebab dalam perjuangan menuntut rehabilitasi ini akan tercakup perjuangan untuk keadilan, untuk HAM, untuk demokrasi, untuk reformasi. Suara para korban Orde Baru adalah suara yang mencerminkan keadilan.

Dalam rangka inilah amat penting peran tulisan tentang kisah para korban Orde Baru. Tulisan semacam ini tidak akan menimbulkan keonaran dan tidak akan merugikan siapa-siapa. Yang digugat adalah kezaliman dan ketidakadilan. Jadi, kisah para korban Orde Baru hanya mendatangkan kebaikan bagi bangsa, baik sekarang maupun di kemudian hari. Generasi kita yang akan datang akan berterimakasih kepada tulisan-tulisan itu.
