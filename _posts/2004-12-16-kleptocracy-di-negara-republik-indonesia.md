---
layout: post
title: Kleptocracy di negara Republik Indonesia
date: 2004-12-16
---

Adalah menarik sekali membaca berita yang disiarkan oleh banyak kalangan pers Indonesia, antara lain oleh harian Republika 9 Desember 2004, mengenai pidato Presiden SBY pada upacara peluncuran Gerakan Nasional Pemberantasan Korupsi di Istana Negara. Pada kesempatan itu Presiden SBY telah mengatakan bahwa « korupsi harus diberantas dengan cara-cara yang luar biasa, karena tindakan yang melawan hukum ini telah terjadi hampir di semua bidang. Korupsi yang merupakan tindakan kejahatan harus diberantas dengan cara-cara luar biasa," katanya.

Kepala Negara menjelaskan bahwa pada tanggal 9/12, ia telah menandatangani Instruksi Presiden (Inpres) nomor 5 tahun 2004 tentang percepatan pemberantasan korupsi. Dengan dikeluarkannya Inpres tersebut, maka diharapkan pemberantasan korupsi bisa dilaksanakan secara lebih baik sehingga pada tahun-tahun mendatang citra Indonesia akan membaik.

"Sungguh menyedihkan bila bangsa kita dijuluki sebagai bangsa yang korup, walaupun korupsi juga terjadi di negara-negara lain," kata Presiden pada acara yang dihadiri pula Ketua DPR Agung Laksono, Ketua DPD Ginanjar Kartasasmita, serta Ketua Mahkamah Konstitusi Jimly Asshidiqie.

« Karena korupsi telah merasuk ke hampir semua bidang, maka korupsi harus diperangi secara besar-besaran,” katanya. Presiden juga meminta para alim ulama, cendekiawan, serta tokoh masyarakat untuk ikut membantu pemberantasan korupsi.

Yudhoyono menjelaskan « Inpres nomor 5 tahun 2004 itu ditujukan kepada semua menteri, jaksa agung, panglima TNI, Kapolri, pimpinan lembaga pemerintah dan departemen serta gubernur dan bupati. Ketika menguraikan Inpres tersebut, Presiden menyebutkan Menkeu Jusuf Anwar telah diperintahkan untuk mengawasi secara ketat bidang perpajakan dan bea cukai. Menkeu diperintahkan untuk memantau penerimaan dan pengeluaran negara agar tidak terjadi korupsi.

Sementara itu, Menpan Taufik Effendy diinstruksikan untuk memperbaiki sistem pelayanan publik di pusat dan daerah. Yudhoyono memberi contoh, jika ada pelayanan publik yang harus dibayar oleh masyarakat, maka tarifnya harus dicantumkan secara jelas. Menteri Negara BUMN, Sugiharto diminta untuk mengeluarkan petunjuk tentang pengelolaan BUMN secara baik. Khusus kepada Kapolri dan Jaksa Agung, Presiden menginstruksikan agar semua kasus korupsi terutama yang besar terus disidik, dan para pelakunya dihukum berat.

"Saya optimis penilaian dunia akan berubah sehingga kita memiliki peringkat yang terhormat, dan mengharapkan pada masa mendatang akan tercipta pemerintahan yang bersih.”

## Kleptocracy Menguasai Negeri Kita

Pernyataan Presiden SBY tersebut di atas bisa menimbulkan harapan besar di kalangan banyak orang, bahwa korupsi yang sudah merajalela secara besar-besaran sejak jaman rezim militer Orde Baru itu akhirnya mulai diusahakan ( !!!) diberantas. Sebab, meskipun tidak diungkap secara jelas dan terang-terangan olehnya, tetapi korupsi di negara kita sudah sedemikian parahnya, sehingga banyak orang mengatakan bahwa negara kita didominasi oleh berbagai penjahat, maling, atau preman. Bahkan, sudah banyak orang (termasuk di luar negeri) yang menamakan Republik Indonesia sudah dikuasai oleh kleptocracy (keterangan : klepto berasal dari kata Yunani kleptein yang berarti mencuri, dan cracy berarti kekuasaan). Seperti kita ketahui, Republik Indonesia sudah lama termasuk di antara beberapa negara yang terkorup di seluruh dunia.

Presiden SBY menegaskan bahwa “korupsi telah merasuk ke hampir semua bidang”. Kita bisa sama-sama menyaksikan sendiri selama ini bahwa korupsi yang parah sudah berjangkit, antara lain, di bidang-bidang : eksekutif, legislatif, judikatif, perekonomian, kebudayaan, agama, pendidikan. Jadi, korupsi sudah merupakan penyakit atau kejahatan yang menyeluruh di negara kita, sehingga (menurut presiden SBY) “harus diberantas dengan cara-cara luar biasa”.

## “Dengan Cara Cara Yang Luar Biasa”

Kita semua masih belum jelas betul apa yang dimaksudkannya dengan mengatakan bahwa korupsi “harus diberantas dengan cara-cara yang luar biasa”. Apakah ini berarti ia akan menjadikan Instruksi Presiden nomor 5 tahun 2004 (yang ditujukan kepada semua menteri, jaksa agung, panglima TNI, Kapolri, pimpinan lembaga pemerintah dan departemen serta gubernur dan bupati) sebagai perintah yang sungguh-sungguh serius untuk “mempercepat pemberantasan korupsi, dan bisa melaksanakannya secara lebih baik sehingga pada tahun-tahun mendatang citra Indonesia akan membaik” ?

Kita semua menunggu terus dengan tidak sabar adanya tindakan-tindakan yang akan diambil pemerintahan di bawah presiden SBY yang betul-betul tegas, berani, jujur, adil, dan tanpa pandang bulu, terutama terhadap para koruptor kelas “kakap” di kalangan manapun juga (termasuk di kalangan militer). Sebab, karena korupsi sudah merusak akhlak sebagian besar tokoh-tokoh atau elite berbagai kalangan dalam masyarakat Indonesia, maka kejahatan ini “harus diperangi secara besar-besaran” (kalimat antara tanda kutip adalah dari pidato presiden SBY).

Perintah khusus presiden SBY kepada Kapolri dan Jaksa Agung, yang menginstruksikan agar semua kasus korupsi terutama yang besar terus disidik, dan para pelakunya dihukum berat, merupakan unsur yang amat penting sekali dalam melaksanakan Gerakan Nasional Pemberantasan Korupsi. Tetapi, jelaslah juga bahwa keberhasilan gerakan nasional ini tidak bisa dipisahkan dari keharusan adanya “pembersihan” dalam kedua aparat negara ini dari oknum-oknum yang busuk dan korup. Sebab, pemberantasan korupsi tidak mungkin akan berhasil baik kalau kepolisian dan kejaksaan (dan aparat-aparat negara lainnya, seperti pengadilan dll) masih terus ditongkrongi pejabat-pejabat yang berakhlak buruk.

## Korupsi Yang Sudah Keterlaluan

Kerusakan dan kerugian yang disebabkan oleh kleptocracy di Indonesia selama ini sungguh-sungguh keterluan besarnya. Pernah diperkirakan bahwa sekitar 30% dari APBN setiap tahun sudah dicuri oleh maling-maling yang bercokol di berbagai bidang kehidupan negara. Ini jumlah yang besar sekali !!! Karena itu, ini juga merupakan kejahatan yang luar biasa pula, yang sudah berjalan selama puluhan tahun.Sampai sekarang!

Akhir-akhir ini Gubernur Aceh, Sumatera Barat, Banten , Sulawesi Utara, dan para bupati di berbagai daerah telah diperiksa oleh aparat-aparat negara karena dituduh korupsi. Demikian juga ratusan anggota-anggota Dewan Perwakilan Rakyat Daerah (DPRD) telah dicurigai dan diperiksa atau ditindak, karena mencuri uang negara dan rakyat dengan berbagai cara. Ditambah dengan korupsi yang terjadi -tetapi belum ditindak- di Pemerintahan Pusat (kementerian-kementerian dan lembaga-lembaga tinggi negara lainnya, misalnja Mahkamah Agung, Kejaksaan Agung,), maka jelaslah bahwa instruksi presiden nomor 5/2004 itu mempunyai arti yang penting.

Kalau diingat korupsi besar-besaran yang berkaitan dengan BLBI, di berbagai BUMN,di bank-bank terkemuka (ingat, antara lain, peristiwa Bank BNI Kebayoran dan bank Global akhir-akhir ini ) maka nyatalah bahwa korupsi besar-besaran sudah merusak banyak bidang kehidupan bangsa. Ini belum terhitung banyaknya korupsi yang terjadi dalam berbagai “Yayasan” yang dikuasai oleh militer, yang sulit sekali dibongkar, karena tersangkutnya sejumlah tokoh-tokoh militer (ingat masalah pembelian tank-tank Scorpion dari Inggris, di mana Tutut disebut-sebut tersangkut).

## Partisipasi Masyarakat Perlu

Patut dicatat bahwa Presiden SBY juga “meminta para alim ulama, cendekiawan, serta tokoh masyarakat untuk ikut membantu pemberantasan korupsi”. Permintaan presiden ini perlu mendapat tanggapan serius dari kalangan seluas-luasnya. Karena, parahnya kerusakan akhlak sebagian besar kalangan elite bangsa, dan luar biasa besarnya kerugian harta negara dan rakyat yang sudah dicuri oleh maling-maling, maka korupsi ““harus diperangi secara besar-besaran, dan harus diberantas dengan cara-cara luar biasa”.

Untuk itu, berbagai tokoh ornop, atau LSM, atau gerakan masyarakat (termasuk partai-partai politik) perlu berpartisipasi dalam gerakan pemberantasan korupsi ini. Ikut berpartisipasinya banyak kalangan masyarakat luas dalam gerakan ini akan merupakan dorongan bagi berbagai usaha untuk mensukseskan pemberantasan korupsi, dan sekaligus juga untuk mengawasinya atau mengkontrolnya. Pemerintahan dibawah presiden SBY harus terus menerus diawasi oleh opini publik, sehingga tidak “menyeleweng” dari janji-janji yang sudah dikeluarkan.

Dalam hal ini peran alim ulama adalah penting sekali. Di masa lalu kalangan NU sudah pernah mengumumkan fatwa yang cukup “menghebohkan”. Fatwa kalangan ulama NU itu menegaskan bahwa korupsi adalah kemungkaran yang sangat besar. Sehingga, para koruptor layak dihukum mati, dan kalau koruptor mati tidak perlu di shalati. Bukan hanya kalangan NU yang sudah memfatwakan anti-korupsi. Kaum ulama Muhammadiyah juga telah menyatakan bahwa « korupsi adalah syirik akbar yang dosanya tidak diampuni oleh Allah » (Tempo Interaktif, 8 Desember 2004)

Tetapi, fatwa-fatwa para ulama NU dan Mummadiyah itu rupanya tidak diacuhkan sama sekali oleh banyak orang, sehingga para koruptor tetap meneruskan kejahatan-kejahatan mereka. Perlu dicatat bahwa banyak di antara para koruptor itu yang mengaku sebagai orang Muslim yang rajin sembahyang , pergi ke masjid atau bahkan pernah naik haji ke Mekah.

Dengan diluncurkannya Gerakan Nasional Pemberantasan Korupsi dan diumumkannya Inpres N o 5/2004, maka terbuka kemungkinan bagi semua organisasi masyarakat yang peduli dengan masalah pemberantasan korupsi (umpamanya, Indonesia Corruption Watch dan sejenisnya) untuk lebih menggalakkan kegiatan mereka. Pemerintah wajib memberikan bantuan, kemudahan-kemudahan, dan kerjasama sebesar-besarnya kepada organisasi-organisasi ini. Partisipasi seluas-luasnya dari semua orang dan golongan yang anti-korupsi ini tidak boleh dihalang-halangi atau dipersulit oleh pemerintah. Untuk mensukseskan Gerakan Nasional Pemberantasan Korupsi pemerintah harus melakukan tindakan terhadap pejabat-pejabat yang menimbulkan kesulitan bagi kegiatan-kegiatan para aktivis anti-korupsi ini.

## Tanpa Kompromi Dan Tanpa Pandang Bulu

Pemberantasan korupsi, terutama yang besar-besar dan dilakukan oleh kalangan “atas” dari berbagai golongan dan bidang, adalah masalah urgen, yang menjadi program prioritas pemerintahan di bawah presiden SBY.Tetapi, tugas ini tidak akan mudah. Sebab, korupsi sudah merajalela sejak jaman Orde Baru, dan sudah merusak - secara parah sekali !!! - akhlak banyak tokoh di berbagai bidang kehidupan bangsa. Karenanya, “perang terhadap kejahatan korupsi secara besar-besaran dan dengan cara-cara yang luar biasa” ini akan menghadapi banyak halangan, antara lain baik dari para simpatisan Orde Baru maupun dari mereka yang non-Orde Baru tetapi bermental busuk.

Untuk memperbaiki citra bangsa Indonesia yang sudah sangat buruk ini, korupsi harus diberantas habis-habisan, dengan sikap tanpa kompromi dan tanpa pandang bulu terhadap golongan mana pun juga (termasuk keluarga Suharto dan kalangan terdekatnya atau pendukung-pendukung setianya). Hanya dengan sikap yang demikian gerakan pemberantasan korupsi akan berhasil.
