---
layout: post
title: Suharto bukanlah pahlawan !
date: 2008-02-03
---

Tulisan ini dimaksudkan guna mengajak para pembaca untuk bersama-sama merenungkan adanya suara-suara ( terutama dari tokoh-tokoh Golkar) yang mengusulkan supaya Suharto diberi gelar pahlawan nasional sesudah ia meninggal. Sebab, usul yang demikian ini menunjukkan bahwa perlu diragukan kejernihan nalar orang-orang yang mengusulkannya, atau, dengan kalimat lainnya, pantas disangsikan akan kesehatan cara berfikir mereka. Adanya usul atau fikiran yang segila itu, menunjukkan bahwa ada sebagian dari “ kalangan elite” di negeri kita yang sedang diserang penyakit rochani yang akut.

Sebab, gagasan atau usul untuk memberikan gelar “pahlawan nasional” kepada Suharto itu bukan saja merupakan tantangan yang tidak tanggung-tanggung atau provokasi besar sekali bagi banyak kalangan di negeri kita (dan juga kalangan internasional) tetapi juga membikin makin ruwetnya berbagai masalah Suharto yang selama ini memang sudah ruwet dan juga rumit. Usul gila ini merupakan salah satu di antara serentetan panjang blunder (kesalahan besar) dari Golkar yang sudah sering dilakukan selama berpuluh-puluh tahun.

Usul Golkar (dengan dukungan para Suhartois lainnya dalam berbagai kalangan, terutama dari kalangan militer) untuk memberi gelar pahlawan kepada Suharto menunjukkan bahwa Golkar -- yang sekarang mendominasi kekuasaan politik dalam Orde Baru jilid II dewasa ini -- menyangka bahwa meninggalnya Suharto merupakan kesempatan baik untuk “mengembalikan” nama baik dan kehormatannya, yang sudah rusak atau anjlok sejak turunnya sebagai presiden dalam tahun 1998.

Rusaknya atau anjloknya nama dan kehormatan Suharto adalah akibat dari berbagai politiknya menjalankan rejim militer Orde Baru dan banyak kejahatannya di bidang HAM dan KKN.

Golkar, yang selama 32 tahun merupakan alat utama Suharto (ditambah militer) dalam menguasai sepenuhnya pemerintahan tangan besi Orde Baru, berkepentingan sekali bahwa Suharto dapat “merebut” kembali nama baik dan kehormatannya. Oleh karena adanya hubungan yang erat sekali antara Golkar dan Suharto maka kehancuran nama dan “kehormatan” Suharto bisa juga menyebabkan kemerosotan “nama baik dan kehormatan” Golkar.

Pimpinan Golkar sekarang, kelihatan salah perhitungan atau keliru sekali membaca situasi, dengan mengajukan usul pemberian gelar pahlawan nasional kepada Suharto. Karena, usul ini telah menuai banyak reaksi keras yang mencerminkan dengan jelas kemarahan dan juga membangkitkan perlawanan dari banyak kalangan (Harap simak juga “Kumpulan berita” di rubrik “Meninggalnya mantan presiden Suharto’ dan “Sesudah Suharto meninggal”).

Perlawanan keras dari berbagai kalangan (terutama dari kalangan generasi muda) terhadap usaha-usaha kekuatan Orde Baru jilid II untuk memberi gelar pahlawan kepada Suharto ini merupakan peristiwa yang cukup penting dalam sejarah gerakan perlawanan rakyat terhadap kekuatan Suhartois. Bisa diharapkan bahwa gerakan perlawanan yang sekarang ini akan berkembang terus kemudian sehingga merupakan kelanjutan dari gerakan besar-besaran dalam tahun 1998 yang bersejarah itu dan yang membikin jatuhnya kekuasaan Suharto sebagai presiden.

Cuplikan dari beberapa di antara reaksi keras tersebut di bawah ini kiranya cukup jelas untuk mengukur suhu atau derajat kemarahan berbagai kalangan terhadap usul gila ini :

Reaksi keras dari kalangan muda
Sejumlah aktivis gerakan mahasiswa angkatan 1977/1978 menolak rencana pemerintah untuk memberikan gelar pahlawan kepada mantan presiden Soeharto. “Sampai sekarang kejahatan kemanusiaan Soeharto belum disentuh. Pemberitaan yang berlebihan tentang prestasi dan jasa Soeharto, dirasakan tidak adil dan melukai hati para korban kebijakan Soeharto. Seperti dalam kasus Aceh, Papua, Lampung, Tanjung Priok, Kasus Gerakan Mahasiswa 77-78, 27 Juli, Haur Koneng, Gerakan 30 September, Petrus, Waduk Nipah, Tapol-Napol, »ujar Mahmud Madjid, salah seorang aktivis angkatan 1977-1978 di Bandung (Tempo Interaktif, 31 Januari 2008)

Puluhan mahasiswa di Bali menggelar aksi unjuk rasa menolak usulan pemberian gelar pahlawan bagi almarhum mantan Presiden RI Soeharto. Mereka justru menyebut Soeharto sebagai Bapak Pelanggaran HAM. Mereka juga membagi-bagikan pernyataan yang berisi tuntutan akan kasus korupsi Soeharto terus diusut hingga ke kroni-kroninya. “Tidak ada alasan untuk menghentikan apalagi hanya karena ingin memberi gelar pahlawan,” tegas Hasan yang menjadi Korlap aksi itu. Poster yang dibuat antara lain bertuliskan, “Adili kroni Soeharto”, “Tidak ada Kata Maaf untuk Pelanggar HAM”, dan lain-lain.

Mahasiswa yang berasal dari berbagai organisasi itu menilai, pemerintahan Presiden Susilo Bambang Yudhoyono telah bertindak represif dengan mewajibkan pengibaran bendera setengah tiang sebagai tanda duka cita atas meninggalnya Soeharto. Padahal, sepanjang kekuasaannya Soeharto telah menyakiti hati rakyat dengan rangkaian pelanggaran HAM sejak pasca-peristiwa G-30S PKI hingga penerapan Daerah Operasi Militer (DOM) di Aceh. Dalam tuntutannya, mereka meminta Presiden Yudhoyono menuntaskan pengungkapan kasus-kasus pelanggaran HAM, menetapkan rezim Orde Baru sebagai rezim pelanggaran HAM, menyatakan Soeharto sebagai penjahat HAM dan mengusut tuntas serta melacak harta hasil korupsi milik Soeharto (Koran Tempo, 30 Januari 2008).

Penolakan pemberian gelar kepada mendiang Soeharto juga diungkapkan Mahasiwa Fakultas Tarbiyah IAIN Walisongo Semarang iyang mengatakan bahwa « tidak ada gunanya pemerintah memberikan gelar pahlawan kepada penguasa yang otoriter seperti Soeharto. Apalagi, masyarakat saat ini masih terbebani akibat kebijakan-kebijakan Soeharto yang menjerumuskan. "Gimana disebut sebagai pahlawan kalau ia menjerumuskan bangsa ini ke jurang kemiskinan," katanya. (Koran Tempo, 30 Januari 2008).

Sakiti Hati Rakyat

Sejumlah kalangan menilai mantan Presiden Soeharto tidak layak diberi gelar pahlawan. Pasalnya, sampai ia meninggal dunia, statusnya masih sebagai terdakwa kasus korupsi.

Selain itu, begitu banyak orang yang dibunuh dan dipenjara tanpa melalui proses hukum pada masa pemerintahan Soeharto. "Kalau ia diberi gelar pahlawan, justru menyakiti hati rakyat, terutama para keluarga korban kekejaman di masa pemerintahannya," kata Koordinator Komisi untuk Orang Hilang dan Korban Tindak Kekerasan (Kontras) Usman Hamid di Jakarta.

Selain itu, kata Usman, sangat ironis kalau Soeharto diberi gelar pahlawan karena sejumlah mahasiswa yang tewas ditembak "anak buah" Soeharto pada 1998 karena menuntut dia mundur telah diberi gelar pahlawan reformasi. Sedangkan di sisi lain, Soeharto yang diduga kuat sebagai dalang peristiwa itu juga akan diberi gelar pahlawan.

Senada dengan itu, Koordinator Tim Advokasi dan Rehabilitasi Korban Tragedi 1965 Witaryono Reksoprodjo mengatakan kalau Soeharto diberi gelar pahlawan, selain menyakiti rakyat Indonesia, juga memalukan bangsa dan negara. Pasalnya, di mata dunia internasional Soeharto adalah mantan kepala negara yang mencuri harta negaranya paling tinggi dibanding kepala negara lain yang juga korup. Selain itu, ketika Soeharto meninggal dunia media massa asing memberitakan Soeharto sebagai seorang mantan diktator yang kejam. "Sudahlah. Ia tidal layak diberi gelar pahlawan," kata dia.


John Pakasi, salah satu korban pelanggaran HAM berat 1965, mengatakan Soeharto adalah diktator yang kejam, bahkan lebih kejam dari Hitler. "Mana bisa orang seperti dia diberi gelar pahlawan?" ujar pria yang dipenjara selama 9 tahun tanpa melalui proses hukum oleh Soeharto dengan alasan terlibat PKI.

Sehari setelah mantan Presiden Soeharto dimakamkan, sejumlah warga kota Solo dan sekitarnya yang tergabung dalam Aliansi Masyarakat untuk Kesejahteraan Rakyat mendatangi Kantor Kejaksaan Negeri Solo. Mereka menolak masa berkabung nasional dan pengibaran bendera setengah tiang.Aliansi Masyarakat untuk Kesejahteraan Rakyat (Amuk Rakyat)—yang terdiri atas aktivis beberapa badan eksekutif mahasiswa (BEM) dari sejumlah perguruan tinggi dan aktivis lembaga swadaya masyarakat—juga mendesak kroni-kroni Soeharto yang terkait dengan pelanggaran hak asasi manusia (HAM) dan kasus korupsi segera diadili.

Para aktivis yang dipimpin Winarso, menemui Kepala Kejaksaan Negeri Solo Momock Bambang Soemiarso. Dalam pertemuan itu, Winarso menyerahkan pernyataan sikap Amuk Rakyat yang intinya menolak masa berkabung nasional tujuh hari dan pengibaran bendera setengah tiang. Alasannya, Soeharto tidak pantas mendapatkan penghormatan itu.

"Kasus-kasus Soeharto dan kroni-kroninya sampai saat ini belum jelas penyelesaiannya. Bagi kami, berkabung nasional tujuh hari hanya dikhususkan untuk pahlawan. Soeharto yang penuh dengan kasus apakah pantas disebut sebagai pahlawan," tutur Winarso.

Di Yogyakarta, ahli sejarah dari Universitas Sanata Dharma Yogyakarta, FX Baskara T Wardaya, mengingatkan agar pemerintah tidak buru-buru memberi gelar pahlawan kepada mantan Presiden Soeharto. Sebab, ketokohan Soeharto masih menyisakan kontroversi, di samping sebagian besar masyarakat dipastikan tidak akan mendukung pemberian gelar.
Menurut Baskara, Soeharto tidak bisa digolongkan dalam kategori pahlawan nasional. Kematiannya bahkan menyisakan ketidakjelasan dalam hal sejarah, terutama menyangkut peristiwa Serangan Umum 1 Maret 1949.

Selain itu, Soeharto juga dinilai sebagai tokoh yang bertanggung jawab dalam peristiwa gerakan 30 September 1965. Peristiwa itu telah menyebabkan munculnya dua tragedi, yaitu terbunuhnya sejumlah jenderal dan pembantaian rakyat secara massal (Kompas, 30 Januari 2008)

Sejarawan Asvi Marwan Adam mengatakan, untuk mendapatkan gelar pahlawan nasional ada sejumlah kriteria yang harus dipenuhi selain masalah prosedural. Sesuai dengan ketentuan, orang yang dianugerahi pahlawan nasional harus berjasa dan tidak memiliki cacat dalam perjuangan. Dari

sisi tersebut, Soeharto tidak memiliki kriteria yang tidak cacat. Saat ini dia masih berstatus tersangka kasus dugaan korupsi di tujuh yayasan dan terindikasi melakukan pelanggaran HAM berat. Untuk kasus korupsi, Soeharto masih berurusan dengan peradilan perdata. Adapun kasus HAM,

Komisi Nasional Hak Asasi Manusia (Komnas HAM) juga sedang menindaklanjutinya. "Ini sangat berisiko mengangkat Soeharto sebagai pahlawan. Nanti bisa memalukan. Mengangkat tersangka korupsi dan pelaku indikasi pelangaran HAM berat akan merepotkan. Apalagi untuk mencabutnya nanti juga susah," katanya

"Itu sangat berisiko tinggi. Sebab, misalkan gelar pahlawan sudah diberikan kepada Soeharto dan ternyata dia kemudian dinyatakan sebagai koruptor atau juga pelanggar HAM berat. Tentu pemerintah yang akan repot. Itu juga akan menjadi aib bangsa Indonesia. Masa gelar itu akan dicopot. Padahal, dalam sejarah, belum pernah ada gelar pahlawan dicopot," ujar Asvi di Jakarta,

Soeharto bukan pahlawan

Yayasan LBH Indonesia, yang diketuai oleh Patra M. Zen juga mengeluarkan pernyataan yang berisi sikap politik mengenai gelar pahlawan untuk Suharto ini, yang nada dan isinya sangat tajam. Dalam pernyataan tersebut dikatakan :


Berita kematian Soeharto pada 27 Januari 2008 menghiasi media massa nasional dan internasional. Beberapa media massa menampilkan dengan sangat berlebihan. Berlebihan, dalam arti mengajak pemirsa mengenang kembali 'jasa-jasa' penguasa Orde Baru selama 32 tahun itu. Tayangan tersebut
berusaha menyeret simpati publik terhadap sosok Soeharto.

Sikap politik dan 'keberpihakan' terhadap Soeharto juga ditunjukkan oleh Presiden Susilo Bambang Yudhoyono. Melalui Menteri Sekretaris Negara Hatta Radjasa, pemerintah meminta rakyat Indonesia mengibarkan bendera merah-putih setengah tiang sebagai tanda berkabung selama tujuh hari berturut-turut. Sikap politik pemerintah terhadap Soeharto juga ditunjukkan dengan wacana berupa penyematan gelar pahlawan buat Soeharto.

Yayasan LBH Indonesia (YLBHI) menilai sikap dan proses politik yang mengiringi kematian Soeharto yang ditunjukkan oleh pemerintah begitu gegabah dan berlebihan. Pasalnya, kita tahu, pada kematiannya di usia 86 tahun tersebut, Soeharto meninggal tanpa pernah diadili atas perbuatan-perbuatannya..

Kita patut mengerti, bahwa, Soeharto berpulang dalam kondisinya berlumuran darah atas perbuatan masa lalu di masa Orde Baru. Kasus pembantaian orang-orang yang dituduh Partai Komunis Indonesia (PKI) 1965, kasus penembakan misterius (Petrus), kasus Tanjung Priok, kebijakan daerah operasi militer di Aceh dan Papua, adalah contoh lumuran darah masa Soeharto.

Selain itu, perbuatan korupsi, kolusi, dan nepotisme, yang dilakukan oleh Soeharto dan keluarga serta kroni-kroninya, juga telah merusak mental bangsa Indonesia, selain memporak-porandakan bangunan ekonomi dan sosial bangsa Indonesia.

Karena itulah, YLBHI bersikap, sangat tidak patut dan layak, pemerintah memberikan predikat pahlawan kepada Soeharto. Ketidaklayakan itu didasarkan pada alasan bahwa secara hukum, Soeharto tidak bisa dikatakan bersalah maupun tidak bersalah, karena proses hukum atas perbuatannya tidak selesai. Demikian pernyataan YLBHI.

Tiada ma’af bagi Suharto

Kemarahan juga telah dilontarkan oleh Keluarga Alumni SMID (Solidaritas Mahasiswa Indonesia untuk Demokrasi, organisasi di bawah pimpinan PRD), yang mengeluarkan pernyataan antara lain sebagai berikut :

“Bagaimana kami dan massa rakyat harus mengeja kata *"maaf"* dan *"mengenang jasa"* untuk orang seperti Soeharto? Mari kita buka kembali rentang panjang sejarah penindasan yang dialami rakyat Indonesia dan semua kekuatan yang berlawan dengannya. Adakah kita lupa bagaimana tanah-tanah rakyat petani direbut paksa demi kepentingan ekonomi para kroni Soeharto, contohnya di Tapos, Cilacap, Badega, Kedungombo, dan banyak lagi? Atau sudah lupakah kita pada pembunuhan aktivis buruh Marsinah dan penindasan terhadap kaum buruh lainnya? Sudah berapa ribu nyawa sekarat akibat digasak bayonet di Tanjung Priok, Lampung, Aceh, Papua, dan Timor Leste? Jutaan orang dijebloskan ke penjara pada tahun 1965 tanpa pengadilan berikut jutaan nyawa tak berdosa melayang sebagai tumbal kekuasaan ekonomi dan politiknya? Rezim Soeharto, beserta kolaboratornya, dan juga rezim sesudahnya, tak sekalipun berucap * "maaf"* atas kebiadaban ini. Kini kita dibujuk untuk memberi maaf atas rezim yang membunuhi rakyatnya sendiri.

Sekali lagi, mari kita bertanya secara arif, berjasakah jika, Soeharto, yang menurut data Bank Dunia menempati urutan teratas pemimpin politik dunia terkorup yang selama 32 tahun berkuasa, telah berhasil menjarah bumi Indonesia dengan total harta mencapai USD 15-35 miliar. Sementara, dalam rentang masa penjarahan itu, menghasilkan puluhan juta rakyat miskin dengan pendapatan hanya USD 1 per hari atau sekitar Rp 9.200). Dengan tangan besinya, Suharto tak segan-segannya membantai rakyat sendiri. Memaafkankah kita jika ingat pembantaian tahun 1965, Timor Leste, Aceh, Papua, Jenggawah, pembunuhan Marsinah, peristiwa 27 Juli 1996, dan masih banyak kasus lainnya yang semuanya diselesaikan dengan pertumpahan darah dibawah kendali Soeharto.

Jelaslah hasil karya Soeharto yang paling nyata adalah pemiskinan rakyat yang luar biasa, penindasaan dan penghisapan terhadap kaum buruh yang juga luar biasa, barisan pengangguran yang tak kalah dahsyatnya, beserta juga kisah sebuah negeri, berikut kekayaan alamnya, dan harga diri rakyatnya yang telah digadai ke tangan kekuatan imperialisme. Soeharto, adalah peletak dasar kolaborasi dengan para imperialis ini. Demikian antara lain Keluarga Alumni SMID.

Pernyataan Kal. SMID ini senafas dengan isi wawancara di televisi oleh Sukmawati Sukarnoputri yang mengungkap berbagai hal tentang perbedaan yang jauh sekali antara perlakuan terhadap Bung Karno ketika wafat dengan perlakuan terhadap Suharto. Dalam wawancara inilah Sukmawati juga mengeluarkan kata-kata yang tajam dan keras mengenai Suharto, dengan mengatakan bahwa “tiada ma’af Suharto” dan bahwa Suharto adalah pengkhianat (wawancara yang menarik dan penting ini bisa disimak kembali lewat Youtube http://www.youtube.com/watch?v=PXvh_4MdFyw&feature=related)

Dari sedikit bahan-bahan yang disajikan di atas nyatalah - dengan jelas sekali pula - bahwa Suharto memang tidaklah pantas sama sekali disebut sebagai pahlawan nasional. Bahkan, seperti yang sudah dikatakan Sukmawati dengan tegas, ia adalah pengkhianat !!!
