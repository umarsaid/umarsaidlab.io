---
layout: post
title: Bangsa kita tidak membutuhkan Golkar
date: 2003-07-15
---

Akhir-akhir ini makin banyak hiruk-pikuk yang terdengar sekitar pemilu yang akan datang (2004). Dan makin dekat dengan pemilu sudah pastilah bahwa hiruk pikuk ini akan makin bertambah ramai. Di antara persoalan yang sudah jadi perhatian banyak orang, adalah masalah partai Golkar. Dari kalangan partai ini sudah terdengar nama-nama yang dicalonkan sebagai presiden (capres). Di antara nama-nama itu terdapat Wiranto, Prabowo dan Akbar Tanjung, di samping nama-nama lain seperti Nurcholis Madjid dan Agum Gumelar dan lain-lain.

Tulisan kali ini lebih banyak mengangkat berbagai masalah yang berkaitan dengan partai Golkar dan hubungannya dengan pemilu dan pemilihan presiden. Berhubung dengan kompleksnya masalah ini, mungkin diperlukan beberapa kali tulisan untuk bisa menyorotinya dengan lebih seksama. Sebab, masalah Golkar adalah masalah yang amat penting untuk sama-sama kita telaah, mengingat besarnya peran Golkar selama berkuasanya rezim militer Orde Baru lebih dari 30 tahun. Oleh karena di masa yang lalu Golkar sangat erat hubungannya dengan Suharto, maka faktor Suharto ini juga akan dibahas di sana-sini dalam tulisan-tulisan ini.

Sebab, meskipun masalah peran negatif Suharto dalam tindakan pengkhianatannya menggulingkan Presiden Sukarno dan membunuhi jutaan anggota dan simpatisan PKI sudah banyak ditulis selama ini di Indonesia dan di luarnegeri, rupanya masih saja ada oknum-oknum tertentu yang menganggap (dan menggembar-gemborkan) Suharto sebagai pahlawan yang “berjasa” bagi rakyat dan bangsa Indonesia.

Apapun alasannya, memuji-muji Suharto adalah perbuatan yang bertentangan dengan opini majoritas rakyat kita dan tindakan yang tidak berdasarkan nalar sehat, alias tidak waras. Sebab, sejarah sudah membuktikan, dengan gamblang pula, betapa besar dan betapa banyak dosa Suharto terhadap rakyat, terhadap Bung Karno, terhadap Republik Indonesia.

## Golkar Yang Sekarang Sama Dengan Yang Dulu

Kalau opini umum di Indonesia (dan di luarnegeri) sudah meyakini bahwa Orde Baru adalah kekuasaan yang banyak melakukan tindakan-tindakan yang merugikan demokrasi dan melanggar HAM dan korup, maka masih cukup banyak orang yang tidak (atau belum) jelas tentang peran Golkar dalam Orde Baru. Bahkan, banyak yang mengira bahwa partai Golkar yang sekarang dipimpin Akbar Tanjung tidak ada hubungannya dengan Golkar zaman Orde Baru, dan bahwa Golkar yang sekarang adalah berlainan dengan Golkar zaman Orde Baru.

Apa betul begitu ? Tidak. Sejak digulingkannya Suharto sebagai presiden dan Orde Baru resminya jatuh, memang ada perobahan sana-sini dalam partai Golkar. Tetapi, pada pokoknya, atau secara hakekat, partai Golkar yang sekarang dipimpin Akbar Tanjung masih dijiwai oleh langgam, praktek dan cara berfikir Golkar yang pernah “berjaya” zaman Orde Baru. Partai Golkar (yang sekarang) tetap menjadi alat atau kendaraan dari orang-orang atau kalangan yang mempunyai simpati kepada Suharto beserta Orde Barunya.

Jadi, partai Golkar dewasa ini pada pokoknya merupakan konsentrasi orang-orang atau kalangan yang sebagian terbesar mempunyai simpati kepada Orde Baru atau kepada Suharto karena berbagai sebab. Sebagian karena mereka tidak menyadari betapa besar kerusakan dan kejahatan terhadap bangsa dan negara yang ditimbulkan Golkar selama lebih dari 32 tahun. Sebagian lainnya disebabkan karena mereka tertipu oleh penampilan Golkar dewasa ini.

## Apa Arti Golkar Bagi Bangsa

Sudah jelaslah, kiranya, bahwa dalam sejarah Republik Indonesia nama Golkar tidak bisa dihilangkan berhubung dengan peran besar dan penting yang dimainkannya. Peran besar dan penting ini merupakan halaman-halaman hitam dalam sejarah Indonesia, karena penuh dengan dosa dan pengkhianatan terhadap rakyat dan negara, terhadap Sukarno, terhadap Pancasila, dan terhadap Bhinneka Tunggal Ika.

Golkar adalah kendaraan politik utama Suharto dkk, yang bersama-sama TNI (terutama Angkatan Darat) merupakan tulang punggung kekuasaan Orde Baru. Oleh karena rezim militer Suharto dkk sudah banyak melakukan berbagai kejahatan negara terhadap rakyat selama puluhan tahun, maka Golkar tidak bisa melepaskan diri dari tanggungjawab ini. Dalam jangka yang lama sekali, Golkar adalah satu dan senyawa dengan rezim militer Orde Baru. Artinya, sebagian terbesar kejahatan, pelanggaran HAM, penyalahgunaan kekuasaan, dan korupsi yang merajalela di Indonesia adalah dilakukan oleh pejabat-pejabat tinggi militer dengan kolaborasi (atau dengan persetujuan) dengan orang-orang utama Golkar.

Karenanya, sebenarnya dan seharusnya, menolak Orde Baru berarti juga menolak Golkar. Bangsa kita sudah menutup buku - dan menutup pintu – terhadap Orde Barunya Suharto dkk. Tetapi, simpatisan-simpatisan Orde Baru dan simpatisan Suharto tidak hanya terdapat dalam Golkar saja. Mereka ini juga terdapat di partai-partai lain, dan menduduki pos-pos penting dalam lembaga eksekutif, legislatif, judikatif, dan kehidupan masyarakat. Mereka inilah, yang dewasa ini, berusaha menjalankan neo-Orde Baru, dengan berbagai cara dan bentuk.

## Golkar Ikut Berdosa Atas Segala Kejahatan Orba

Mengapa bisa dikatakan bahwa Golkar yang sekarang pada pokoknya – atau pada intinya – adalah Golkar zaman Orde Baru? Sebab, Golkar yang sekarang dipimpin oleh Akbar Tanjung tidak pernah mau secara tegas dan tuntas menegasi atau mengutuk berbagai kejahatan atau kesalahan serius yang dibikin Golkar di masa yang lalu. Di antaranya adalah mengutuk serentetan tindakan Suharto dkk dalam melakukan kudeta merangkak terhadap Presiden Sukarno dan penindasan terhadap PKI serta pembantaian jutaan manusia tidak bersalah.

Golkar (yang sekarang) tidak pernah berusaha memperbaiki kesalahan Golkar era Orde Baru yang mendukung politik rezim militer terhadap para eks-tapol dan para korban peristiwa ’65 lainnya. Golkar (yang sekarang) tidak mau secara tegas menyokong tindakan terhadap koruptor-koruptor kelas kakap yang terdapat di kalangan elite (terutama kalangan Golkar sendiri). Golkar tidak mau dengan tegas mendukung usaha-usaha reformasi, bahkan sebaliknya, menyabotnya.

Golkar (yang sekarang) tidak mugkin melakukan itu semuanya karena itu merpakan “bunuh diri”. Karena, selama Orde Baru berkuasa justru Golkar-lah yang dipakai Suharto dan pembesar-pembesar militer untuk menjalankan sebagian terbesar politiknya. Karenanya, bolehlah dikatakan bahwa Golkar-lah yang ikut bertanggungjawab terhadap segala kebusukan, keburukan, kejahatan Negaa, pelanggaan HAM, korupsi, penyalahgunaan kekuasaan, dekadensi moral. Pendeknya, Golkar ikut berdosa atas segala kebusukan Orde Baru. Tidak bisa lain. Dan Golkar tidak bisa menghindarinya. Juga Golkar yang sekarang tidak bisa memungkirinya.

## Tidak Semua Anggota Gokar Busuk

Kalau sudah bisa disimpulkan bahwa Golkar adalah kekuatan politik yang banyak dosanya terhadap rakyat dan Negara Republik Indonesia, ini tidak berarti bahwa semua anggota Golkar adalah jelek dan karenanya harus ikut bertanggungjawab. Ada orang-orang yang menjadi anggota Golkar karena terpaksa oleh keadaan dan kebutuhan (pribadi, keluarga dan berbagai sebab lainnya). Mereka terpaksa menjalankan politik rezim militer Suharto, walaupun tanpa menyetujuinya.

Tetapi, ada orang-orang yang menjadi anggota Golkar karena memang menyetujui politik Suharto, artinya mendukung rezim militer Orde Baru dengan penuh keyakinan. Ada juga orang-orang yang menjadi anggota Golkar hanya dengan tujuan pribadi yang nista dan tidak bermoral (mengejar kekayaan pribadi, kenaikan pangkat, kemungkinan korupsi dll). Dan jumlah orang-orang yang bermoral rendah ini cukup banyak terdapat di Golkar.

Untuk menguji apakah konstatasi yang demikian itu berdasarkan kebenaran, cukuplah kita perhatikan tingkah-laku, cara hidup pribadi, dan kegiatan politik tokoh-tokoh utama Golkar, baik yang aktif di masa Orde Baru maupun yang sekarang. Pengamatan yang teliti terhadap kehidupan pribadi dan kegiatan publik tokoh-tokoh Golkar, baik yang di Pusat maupun di daerah-daerah (propinsi, kabupaten bahkan kecamatan), akan menunjukkan bahwa Golkar memang merupakan sarang dari orang-orang yang pada umumnya tidak bermoral tinggi.

## Akbar Tanjung, Wiranto Dan Prabowo

Dalam rangka pemilu 2004 dan pemilihan presiden sekarang sudah muncul di kalangan Golkar sejumlah nama-nama, yang antara lain terdapat nama Akbar Tanjung, Wiranto dan Prabowo. Bahwa nama Akbar Tanjung masih bisa disebut-sebut sebagai calon presiden menunjukkan betapa sudah memuncaknya pembusukan kehidupan politik di Indonesia. Sebab, Akbar Tanjung sudah dijatuhi hukuman 3 tahun penjara karena masalah korupsi Rp 40 milyar dana Bulog. Jadi, ia adalah nara-pidana. Walaupun ia masih naik banding, tetapi bahwa ia masih berani – artinya tidak segan-segan – mencalonkan diri sebagai presiden, ini merupakan penghinaan kepada kecerdasan dan hati-nurani bangsa. Bayangkan, seorang nara-pidana sebagai presiden RI!

Juga bahwa nama Wiranto dan Prabowo disebut-sebut sebagai calon presiden dari partai Golkar adalah suatu hal yang menunjukkan bahwa para pendukung setia Orde Baru mengira bahwa opini umum (di Indonesia dan di luarnegeri) sudah lupa kepada masa suram ketika negeri kita dicengkeram oleh rezim militer selama lebih dari 30 tahun. Dan berdasarkan pengalaman yang sudah-sudah, dan pengamatan terhadap riwayat hidup kedua bekas tokoh militer ini, maka sudah bisa disimpulkan bahwa negara kita tidak akan menjadi lebih baik dengan Kepala Negara seperti dua orang mantan jenderal ini.

Negara dan bangsa kita membutuhkan seorang Kepala Negara (dan wakilnya) yang menjadi pangayom seluruh warganegaranya, yang bisa mempersatukan seluruh bangsa, yang bisa menjamin demokrasi, yang bisa menegakkan hukum berdasarkan keadilan, yang berani bertindak tegas terhadap korupsi yang sudah merajalela, yang berani melancarkan reformasi di segala bidang yang sudah dirusak oleh Orde Baru. Ini semua tidak bisa diharapkan dari orang-orang sejenis Wiranto dan Prabowo.

## Bangsa Kita Tidak Memerlukan Golkar

Berdasarkan pengalaman pahit selama lebih dari 32 tahun, sudah bisa dikatakan dengan pasti bahwa negara dan bangsa tidak memerlukan lagi kehadiran Golkar dalam kehidupan politik kita. Kehadiran Golkar tidak bisa membawa kebaikan bagi bangsa dan negara, melainkan sebaliknya. Golkar hanya menguntungkan kepentingan tokoh-tokohnya atau sejumlah kecil anggota-anggotanya. Tetapi, Golkar masih akan bisa menarik simpati banyak orang, berkat besarnya dana yang dimilikinya selama ini, dan berkat luasnya jaring-jaringan yang bisa dikuasainya. Kekuasaan rezim militer Orde Baru yang sudah bercokol begitu lama memungkinkan Golkar untuk memiliki itu semuanya.

Oleh karena eksistensi Golkar dibangun atas dasar yang tidak sehat (harap ingat bahwa Golkar ,- waktu itu namanya Sekber Golkar - didirikan sebelum peristiwa ’65 oleh kalangan Angkatan Darat justru untuk menghadapi Bung Karno serta PKI), maka tidak bisa diharapkan oleh bangsa kita bahwa dari kalangan Golkar bisa timbul gagasan-gagasan yang sungguh-sungguh besar dan mulia untuk Rakyat dan Negara. Berbagai dosa dan beraneka-ragam kejahatan dan kesalahan yang sudah dilakukannya selama lebih dari 32 tahun tidak memungkinkan Golkar berobah dalam sekejap mata saja. Dalam banyak hal, Golkar yang sekarang adalah Golkar yang dulu juga.

Bahasa yang dipakai dalam tulisan ini mungkin terasa keras, dan cara pengungkapannya pun mungkin terlalu polos. Tetapi cara yang demikian itu perlu, untuk mengingatkan kita semua bahwa Golkar adalah suatu kekuasaan politik yang dalam sejarah Republik kita sudah melakukan begitu banyak kerusakan dan pembusukan di banyak bidang selama puluhan tahun. Dalam bahasa yang lebih sederhana bisa dikatakan : Golkar adalah perusak cita-cita revolusi 17 Agustus ’45, adalah pengkhianat tujuan masyarakat adil dan makmur, adalah penyabot “nation building and character building”, adalah pemalsu jiwa Pancasila.

Sudah jelas, karenanya, bahwa tanpa kehadiran Golkar bangsa kita tidak kehilangan apa-apa. Bahkan sebaliknya!
