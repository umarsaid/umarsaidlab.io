---
layout: post
title: Menyongsong hari 9 Desember
date: 2009-12-07
---

Tulisan kali ini merupakan ajakan untuk sama-sama merenungkan atau memikirkan tentang situasi negeri kita dewasa ini dan kemungkinannya di kemudian hari bagi kehidupan bangsa kita yang berjumlah lebih dari 230 juta ini. Oleh karena itu, di samping akan disinggung berbagai masalah yang berkaitan dengan kasus kriminalisasi KPK dan skandal besar perampokan Bank Century, akan disinggung tentang gerakan besar-besaran di seluruh Indonesia pada tanggal 9 Desember untuk memperingati Hari Anti Korupsi Sedunia. Sebab, masalah-masalah itu ada kaitannya antara yang satu dengan  lainnya.

Setelah perhatian sebagian besar dari rakyat negeri kita disedot oleh heboh besar tentang kebejatan moral yang merusak sebagian kalangan atas di kepolisian dan kejaksaan dengan terbongkarnya rekaman percakapan telpun antara Anggodo dan sejumlah pejabat-pejabat penting, maka heboh baru – yang lebih besar dan juga lebih serius – muncul dengan meledaknya skandal raksasa Bank Century.

Skandal raksasa Bank Century ini merupakan kasus besar yang serius sekali, bukan hanya karena besarnya dana Rp 6,7 triliun  yang dirampok, melainkan juga karena dalam masalah serius  ini terlibat pejabat-pejabat tingkat tinggi negara (antara lain : Wapres Budiono dan Menkeuangan Sri Mulyani.). Karenanya, kasus Bank Century merupakan masalah besar dan rumit yang  merisaukan banyak sekali kalangan.

Gerakan di 400 kota dan 33 propinsi

Hebatnya berbagai macam reaksi keras yang membludak dari berbagai kalangan di seluruh Indonesia terhadap kasus-kasus tersebut menunjukkan dengan jelas bahwa masalah skandal besar Bank Century (dan kasus KPK) sudah merupakan puncak kemarahan rakyat terhadap penyakit parah bangsa, yaitu korupsi yang sudah merajalela sejak puluhan tahun di Indonesia, dan yang telah membusukkan moral sebagian bangsa atau membejatkan akhlak banyak orang, terutama dari kalangan elite kita.

Kemarahan rakyat ini yang tercermin dalam menggeloranya aksi-aksi  kalangan muda (organisasi pemuda dan  mahasiswa) yang berbentuk demo dan kegiatan-kegiatan lainnya di banyak kota akan memuncak pada tanggal 9 Desember besok, ketika gerakan besar-besaran dari berbagai kalangan masyarakat akan dilancarkan di sekitar 400 kota dari 33 propinsi di seluruh Indonesia.

Dilihat dari perkembangan situasi selama ini bisalah kiranya diduga bahwa gerakan besar-besaran tanggal 9 Desember ini akan merupakan peristiwa yang mengandung arti penting sekali bagi kehidupan bangsa dan negara kita.  Karena, tanggal 9 Desember itu akan menunjukkan bahwa kesedaran politik rakyat sudah meningkat dibandingkan dengan tahun-tahun yang lalu, dan opini  sebagian besar rakyat juga sudah tidak sama dengan waktu selama Orde Baru.

Dari pembicaraan-pembicaraan yang dilakukan oleh berbagai organisasi seperti KOMPAK , KONTRAS dll  di kantor Muhammadiyah dan PB NU  dan ikutnya secara aktif organisasi mahasiswa dari golongan Islam seperti HMI, PMII dan organisasi kiri (seperti PRD/LMND)  mencerminkan bahwa gerakan moral anti korupsi 9 Desember yang digerakkan besar-besaran kali ini menggabungkan berbagai kekuatan sosial dengan lintas aliran politik, lintas agama, dan non-partai politik.

Tetapi adanya juga organisasi-organisasi rakyat dan pemuda yang sudah sejak meletusnya persoalan kriminalisasi  KPK (dan kemudian skandal raksasa Bank Century)  melakukan aksi-aksi atau gerakan yang melawan atau menghujat politik pemerintahan SBY juga akan bergerak pada tanggal 9 Desember, maka timbul kecurigaan dari presiden SBY bahwa peringatan hari Anti Korupsi Internasional 9 Desember itu akan “ditunggangi” oleh golongan-golongan yang menggunakan kegiatan pada hari itu untuk tujuan-tujuan politik yang menyerang presiden SBY dengan pemerintahannya.

Presiden SBY kelihatan panik dan bingung

Bahkan presiden SBY sudah menunjukkan kepanikan, atau kebingungan menghadapi tuduhan atau dugaan yang beredar dalam masyarakat bahwa Partai Demokrat  menerima aliran dana yang dicuri dari Bank Century, antara lain dengan memberikan peringatan bahwa ada usaha-usaha untuk makar atau usaha menggulingkan pemerintahan, serta menggunakan fitnah untuk menjatuhkan nama baik dll dll. Dari kerasnya peringatan presiden SBY tentang itu semua kelihatan jelas bahwa persoalan korupsi yang terkait dengan Bank Century (dan lain-lainnya) sudah menyentuh jantung atau ulu hati pemerintahan SBY.

Dari banyaknya reaksi kritis atau negatif terhadap berbagai pernyataan presiden SBY akhir-akhir ini, nampak jelas bahwa kepercayaan rakyat yang pernah memilihnya sampai lebih dari 60 % suara dalam pemilu presiden yang lalu, sekarang  -- dalam beberapa bulan saja -- sudah merosot banyak sekali. Dalam batas tertentu, bolehlah dikatakan bahwa pemerintahan SBY-Budiono sudah diskredit atau tidak dipercayai lagi oleh sebagian besar rakyat. Agaknya, banyak problem yang serius bisa timbul di kemudian hari sebagai akibatnya,  kalau kepercayaan rakyat makin menipis atau bahkan menghilang.

Ketidakpercayaan terhadap Pansus DPR

Ketidak percayaan terhadap pemerintah ini diperburuk lagi oleh ketidakpercayaan rakyat terhadap DPR kita, dengan adanya hiruk-pikuk serta permainan politik diantara berbagai partai politik yang tergabung dalam koalisi pendukung pemerintahan SBY dalam membentuk Pansus Hak Angket DPR mengenai persoalan Bank Century. Pansus Hak Angket yang terdiri dari 30 orang ini terbentuk setelah melalui perundingan-perundingan alot dan berbelit-belit untuk memilih ketuanya. Akhirnya, melalui persetujuan di belakang layar, kedudukan ketua Pansus direbut oleh Idrus Marham dari Partai Golkar dengan dukungan besar dari Partai Demokrat dan sekutu-sekutunya.

Karena susunan Pansus Hak Angket DPR yang demikian ini maka meledak-ledaklah kemarahan banyak kalangan. Karena, mereka menduga bahwa Pansus yang demikian ini tidak akan bisa betul-betul berusaha membongkar kasus Bank Century setuntas-tuntasnya, dan tidak mungkin bekerja secara sungguh-sungguh transparan. Pansus ini akhirnya akan menutupi banyak kejahatan atau pelanggaran-pelanggaran serius sekitar dirampoknya dana sebesar Rp 6,7 triliun itu. Tujuan Hak Angket DPR akan dibelokkan, atau ditelikung, atau dibajak oleh Pansus.

Dengan latar belakang yang demikian kumuh, demikian rumit, dan demikian memuakkan itulah Hari Anti Korupsi Internasional akan diperingati oleh banyak kalangan yang mau memuntahkan kemarahan mereka terhadap penyakit korupsi yang bersumber kepada kebejatan moral atau kerusakan akhlak yang sudah terlalu parah di negeri kita.

Korupsi tidak bisa dipisahkan dari sistem politik

Kalau sama-sama kita tilik dalam-dalam, maka nyata sekalilah bahwa korupsi bukanlah hanya suatu kejahatan yang berkaitan dengan hukum saja, melainkan juga erat hubungannya dengan masalah politik, atau sistem pemerintahan. Karenanya, makin jelas juga bahwa korupsi yang sangat merajalela dengan ganasnya di Indonesia ini juga tidak bisa dipisahkan dari sistem politik atau sistem kekuasaan sejak era Orde Baru sampai pemerintahan yang sekarang. Korupsi adalah inherent (jadi satu) dengan watak dari sistem politik  -- yang pada intinya atau pada dasarnya -- dikelola oleh orang-orang yang bermoral busuk atau berakhlak bejat dan anti rakyat.

Dari sudut ini kita bisa melihat bahwa perjuangan membrantas korupsi di Indonesia tidak bisa tidak harus sekaligus merupakan perjuangan politik juga, di samping adanya perjuangan moral. Perjuangan moral saja tidak cukup  -- betapa pun pentingnya  -- untuk membrantas korupsi. Ini terbukti dari betapa pernah menggebu-gebunya NU mengobarkan   -- sejak bertahun-tahun yang lalu --  jihad terhadap korupsi, namun korupsi toh masih mengganas terus dalam merusak negara dan bangsa. Itulah sebabnya, bahwa dalam gerakan 9 Desember nanti (dan juga dalam gerakan-gerakan selanjutnya di kemudian hari ) perjuangan melawan korupsi perlu disatukan  dengan perjuangan di bidang politik atau pemerintahan.

Sebab, perlu sekalilah kita perhatikan bahwa dengan sistem pemerintahan yang dianut oleh presiden SBY beserta para pendukungnya, dan yang didominasi oleh orang-orang yang dalam sejarah hidupnya menunjukkan simpati mereka kepada Suharto beserta Orde Barunya, maka tidak mungkin diadakan perubahan-perubahan besar dan fundamental di negeri kita, termasuk pembrantasan korupsi secara total dan tuntas. Telah dibuktikan dari pengalaman-pengalaman selama ini, bahwa korupsi besar-besaran yang banyak terjadi sejak era Orde Baru sampai sekarang telah dilakukan  -- pada umumnya atau sebagian terbesar  -- oleh orang-orang (sipil, militer, pemuka-pemuka masyarakat  dan pengusaha) yang bersimpati kepada rejim militer Suharto.

Perjuangan anti korupsi satu dengan perjuangan anti Orba

Itulah sebabnya maka perjuangan untuk pembrantasan total terhadap korupsi adalah juga perjuangan terhadap golongan pendukung Orde Baru, yang umumnya terdiri dari orang-orang atau golongan yang bersikap anti-rakyat, yang reaksioner, atau bahkan yang kontra-revolusioner. Korupsi adalah pada dasarnya atau pada intinya sikap yang anti-rakyat, yang reaksioner, yang kontra-revolusioner, dari oknnum-oknum yang bermoral busuk. Jadi gerakan rakyat tanggal 9 Desember bisa dilihat sebagai gerakan moral yang berdasarkan politik, atau juga gerakan politik,yang berlandaskan moral.

Hari tanggal 9 Desember juga mencerminkan bahwa pemerintahan SBY sudah diskredit (tidak dipercaya rakyat) dan bahwa sebagian besar rakyat menaruh “public distrust” (kecurigaan publik) terhadap DPR yang 76% kursinya dikuasai atau dikungkung oleh presiden SBY beserta koalisinya yang antara lain terdiri dari partai-partai Partai Demokrat, Golkar, PKS, PKB, PAN, dan PPP. Koalisi yang dipimpin oleh SBY ini sebenarnya adalah kartel partai-partai dan monopoli kekuasaaan yang bisa menyerupai diktatur.

Ketidakpercayaan atau kemarahan masyarakat terhadap presiden SBY dapat dilihat adanya gerakan “mosi tidak percaya” terhadapnya  lewat face-book yang bertujuan mencapai target sejuta orang, dan aksi pembakaran fotonya di berbagai kota (antara lain di Makasar). Keputusan presiden SBY untuk mengembalikan kedudukan Bibid-Chandra di pimpinan KPK yang diumumkan tanggal 6 Desember rupanya tidak membikin surutnya kemarahan banyak orang terhadap adanya skandal raksasa Bank
Century.

Generasi muda cari jalan baru

Memang, masih sulitlah kiranya untuk meramalkan  apa saja yang bisa terjadi pada tanggal 9 Desember, serta apa sajakah dampaknya di kemudian hari bagi kehidupan bangsa dan negara kita.
Namun, bagaimana pun juga, sudah bisa sama-sama kita amati sejak sekarang bahwa gerakan anti korupsi kali ini, -- berkat adanya kasus kriminalisasi KPK dan meletusnya skandal raksasa Bank Century – mempunyai dimensi yang lebih luas, yang mencakup golongan-golongan agama, nasionalis, dan kiri. Sejumlah fenomena atau indikasi sudah mulai nampak jelas bahwa gerakan anti-korupsi kali ini juga mengandung arti menentang mental, politik, atau praktek-praktek Orde Baru.

Ikut sertanya dalam gerakan ini golongan-golongan yang tadinya bersimpati kepada Suharto beserta Orde Barunya adalah suatu perkembangan yang penting sekali, yang menunjukkan bahwa sedang terjadi perubahan-perubahan dalam opini publik yang makin tidak menguntungkan kepentingan sisa-sisa kekuatan Orde Baru dan para pejabat bermental busuk serta segala jenis koruptor.

Perubahan opini publik, terutama dari kalangan muda bangsa, yang menimbulkan harapan bahwa perubahan-perubahan besar akhirnya akan bisa diciptakan di kemudian hari, adalah satu pertanda yang penting dalam gerakan tanggal 9 Desember ini. Hal yang demikian itu mengandung petunjuk bahwa generasi muda mulai mencari-cari  jalan baru bagi bangsa, dan sudah emoh atau jijik kepada jalan gagal atau jalan sesat atau jalan buntu yang selama ini sudah ditempuh Orde Baru beserta segala pemerintahan berikutnya. Pengalaman selama pemerintahan Orde Baru dan penerusnya sudah membuktikannya dengan gamblang sekali, yang bisa kita saksikan sampai sekarang.

Hari tanggal 9 Desember ini  bisa merupakan peristiwa penting sekali bagi lahirnya kekuatan extra-parlementer yang luas sekali yang bisa memperjuangkan kepentingan rakyat, terutama rakyat miskin yang merupakan sebagian terbesar dari bangsa kita. Lahirnya kekuatan extra-parlementer yang besar ini adalah untuk mengimbangi atau menandingi kekuatan pemerintah dan DPR yang bisa disebutkan sudah tidak mewakili lagi kepentingan rakyat.  Tanpa melakukan aksi-aksi yang bisa dianggap makar atau gerakan bisa dicap subversif, gerakan rakyat besar-besaran dan luas ini adalah agen untuk mengadakan perubahan besar dan radikal.

Ajaran Bung Karno tentang revolusi

Rakyat sudah terlalu lama (lebih dari 40 tahun !) menunggu-nunggu datangnya perubahan besar ini, tetapi selalu sia-sia, karena tiadanya kekuatan yang bisa didjadikan senjata sekaligus tameng oleh rakyat banyak untuk menciptakan perubahan yang sejati.

Oleh karena itu pulalah agaknya maka  akhir-akhir ini sudah sering terdengar perkataan revolusi di tengah-tengah banyaknya keruwetan dan persoalan parah yang menimbulkan keputus-asaan banyak orang. Banyak orang sudah tidak bisa melihat jalan keluar yang lain kecuali revolusi, seperti yang  sering sekali diserukan atau terus-menerus dianjurkan Bung Karno sebelum beliau dikhianati oleh Suharto. Bung Karnolah yang dalam memimpin rakyat Indonesia selama berpuluh-puluh tahun mengajarkan kepada rakyat Indonesia untuk terus mengobarkan revolusi sebagai jalan satu-satunya menuju  masyarakat adil dan makmur di Indonesia.  

Dalam pidato-pidatonya, yang bisa dibaca dalam Dibawah Bendera Revolusi  atau Revolusi Belum Selesai, Bung Karno banyak sekali atau berulang kali menyampaikan ajaran-ajarannya tentang revolusi yang harus dikobarkan terus-menerus oleh rakyat Indonesia.

Dengan mengambil inti-sari ajaran-ajaran revolusioner Bung Karno sebagai pedoman, maka gerakan rakyat besar-besaran yang dikobarkan lewat kegiatan-kegiatan seperti yang dilancarkan oleh kalangan luas dalam peringatan tanggal  9 Desember  2009 ini akan memberikan perspektif yang cerah untuk adanya perubahan-perubahan besar dan fundamental di kemudian hari.

Hidup semangat dan pesan besar yang terkandung dalam peringatan tanggal 9 Desember !!!
