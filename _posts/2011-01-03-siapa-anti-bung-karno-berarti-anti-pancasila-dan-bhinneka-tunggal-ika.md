---
layout: post
title: Siapa anti Bung Karno berarti anti Pancasila dan Bhinneka Tunggal Ika
date: 2011-01-03
---

Tulisan kali ini dimaksudkan sebagai sambutan terhadap tahun baru 2011.  Tahun baru, yang mulai menghembuskan  -- sedikit-sedikit dan perlahan-lahan – angin kebangkitan kembali nasionalisme atau kebanggaan sebagai bangsa.   Memang, judulnya bisa dianggap oleh sebagian pembaca sebagai asal « njeplak » saja,  atau hanya asbun (asal bunyi) ,  atau ngawur saja. Kepada mereka yang berang atau merasa tidak senang  atau « gondok » dengan judul di atas, penulis  mohon supaya dengan fikiran jernih membaca  lebih lanjut tulisan ini, dan dengan nalar yang sehat merenungkan bersama-sama berbagai hal yang dipaparkan di dalamnya.

Sebab, dalam tulisan ini diutarakan (walaupun hanya secara singkat-singkat) berbagai fikiran atau pendapat yang bisa saja, bagi orang-orang tertentu, terdengar terlalu kasar, atau terlihat terlalu lugas, atau terasa terlalu keras, atau « kurang sopan ».



Dengan menggeloranya lagu « Garuda di dadaku, Garuda kebanggaanku » oleh jutaan orang di seluruh tanair air baru-baru ini, maka timbul  secercah harapan bahwa kebangkitan kembali nasionalisme, dan kecintaan kepada Pancasila (yang asli)  dan Bhinneka Tunggal Ika, yang dipecut oleh dunia sepakbola kita, akan kembali menjulang tingggi, seperti ketika Indonesia masih ada di bawah pimpinan Bung Karno.

Bagi penulis, yang selama 10 tahun bekerja sebagai salah satu dari pimpinan Persatuan Wartawan Asia-Afrika (dari tahun 1963 sampai 1973, sebagian di Jakarta dan sebagian lagi di Peking) menyaksikan dengan pengalaman pribadi sendiri betapa tingginya  pandangan yang penuh hormat (dan kebanggaan) dari rakyat berbagai negeri terhadap perjuangan rakyat Indonesia di bawah pimpinan Bung Karno.

Manifestasi besar-besaran yang ditunjukkan akhir-akhir ini oleh rakyat (sekali lagi, oleh rakyat, dan terutama oleh kalangan muda) terhadap timnas Garuda, terhadap lambang besar kita Bhinneka Tunggal Ika dan Pancasila, terhadap Merah Putih, menunjukkan dengan jelas adanya kebangkitan kembali nasionalisme yang menggebu-gebu, patriotisme yang sudah lama pudar, dan tumbuhnya kembali kebanggaan menjadi orang Indonesia.

Fenomena yang demikian ini mengingatkan banyak sekali orang kepada masa-masa ketika bangsa dan negara ada di bawah pimpinan Bung Karno. Pada waktu itu kebanggaan bangsa Indonesia  menjulang  tinggi berkat adanya gagasan-gagasan besar Bung Karno. Di bawah pimpinan Bung Karno-lah rakyat atau bangsa Indonesia dihormati oleh rakyat berbagai dunia.

Berbagai kegiatan di lebih dari  47 negeri

Kebanggaan dan respek berbagai rakyat terhadap perjuangaan rakyat Indonesia itu dirasakan dan dilihat secara langsung oleh penulis ketika ia melakukan kegiatan (sebagai pengurus PWAA) ke berbagai negeri di Asia dan Afrika (lebih dari 47 negeri) dan menghadiri banyak konferensi-konferensi internasional  (Untuk lebih jelas lagi harap simak CV singkat dan riwayat hidup penulis di website A. Umar Said  yang berjudul « Perjalanan hidup saya »)

Berkat kegiatan-kegiatan professionalnya sebagai wartawan pendukung politik Bung Karno selama menjadi Pemimpin Redaksi Harian Ekonomi Nasional (terbit di Jakarta 1960-1965, dan dilarang oleh militernya Suharto sesudah terjadinya G30S) dan menjadi bendahara PWI Pusat sejak 1963-sampai 1965 dan juga pernah ikut beberapa kali sebagai anggota rombongan Presiden Sukarno ke luarnegeri, maka penulis (82 tahun) adalah sisa-sisa wartawan (yang tinggal sedikit) dari angkatan pra-G30S yang masih hidup.

Dengan latar belakang sejarah hidup dan professional yang demikian, maka banyak hal yang bisa diceritakan tentang pengalaman-pengalaman dalam kehidupan bangsa dan negara selama Indonesia ada di bawah pimpinan Bung Karno sebelum G30S, selama pemerintahan Orde Baru, dan sampai sekarang. Hal-hal itu akan selanjutnya diusahakan disajikan dalam tulisan-tulisan, untuk bahan pemikiran dan renungan atau pertimbangan bagi semua pembaca,

Tindakan-tindakan Bung Karno yang menjadi kebanggaan

Tulisan kali ini mengambil thema soal Bung Karno, Pancasila dan Bhinneka Tinggal Ika, karena (sekali lagi) adanya gejala kebangkitan kembali nasionalisme dan kecintaan kepada lambang Garuda (lengkapnya Garuda Pancasila) berkat prestasi besar tim nasional sepakbola Indonesia akhir-akhir ini. Perkembangan yang demikian menggembirakan ini belum pernah terjadi selama puluhan tahun sejak Bung Karno digulingkan secara khianat oleh Suharto (beserta para pendukung-pendukugnya di dalam maupun di luar negeri) dan amat penting sekali diamati dan disambut hangat oleh seluruh rakyat.

Pada masa-masa yang lalu, kebanggaan bangsa Indonesia bukan saja oleh karena proklamasi kemerdekaan di mana Bung Karno (beserta Bung Hatta dan pemimpin-pemimpin lainnya) memainkan peranan utama, melainkan karena  (antara lain) lahirnya Pancasila, pembuatan lambang Garuda Pancasila, pembangunan Gelora Bung Karno, Konferensi  Asia-Afrika di Bandung, pembangunan mesjid Istiqlal, pembangunan Monas, konferensi non-blok,  pidato Bung Karno di PBB « To build the world anew », dan berbagai ajaran-ajaran revolusioner Bung Karno yang termuat dalam buku « Di bawah Bendera Revolusi ».

Dalam semua tindakan Bung Karno dalam perjuangan menentang imperiaslisme dan kolonialisme, dan mempersatukan seluruh bangsa, beliau selalu dibimbing oleh dasar pemikiran  atau jiwa pokok yang terdapat dalam Pancasila dan Bhinneka Tunggal Ika. Ini tidak bisa lain !!! Sebab Bung Karno adalah satu-satunya sumber asli Pancasila dan Bhineka Tunggal Ika adalah pengejawantahan jati-diri Bung Karno.

Apa yang dibangun Bung Karno dihancurkan oleh Suharto

Sayangnya,  justru akibat ciri-ciri Bung Karno yang demikian besar dan revolusioner inilah  ia digulingkan oleh Suharto beserta pendukung-pendukungnya. Dan sayangnya lagi, sebagian besar dari apa yang telah dibangun oleh Bung Karno untuk persatuan bangsa dan negara selama 20 tahun (dari 1945 sampai 1965) telah dihancurkan atau dirusak oleh Suharto selama 32 tahun.  Namun sejarah juga membuktikan, dengan jelas sekali, bahwa sampai wafatnya (dalam status sebagai tahanan)  Bung Karno tetap dengan gigih memegang erat-erat Pancasila dan Bhinneka Tunggal Ika dan berbagai ajaran-ajaran revolusionernya.

Kiranya, para pakar di berbagai bidang ilmu bisa menyatakan dengan tegas bahwa Bung Karno adalah satu-satunya pemimpin Indonesia yang paling agung yang pernah dimiliki rakyat Indonesia, sampai sekarang !!! Tidak ada pemimpin Indonesia lainnya yang bisa menyamai kebesaran sosok Bung Karno. Tidak ada juga pemimpin lainnya yang bisa menandinginya. Apalagi, tidak ada pemimpin lainnya yang bisa menggantikannya. Untuk selama-lamanya !!!

Kiranya perlulah diulang-ulang bahwa Bung Karno adalah perwujudan Pancasila dan Bhinneka Tungal Ika. Itulah sebabnya  mengapa memusuhi Bung Karno berarti sama saja dengan memusuhi Pancasila dan Bhinneka Tunggal Ika. Dan, itulah praktek atau keadaan yang sesungguhnya terjadi sejak selama rejim militer Suharto selama puluhan tahun, sampai sekarang. Selama Orde Baru dikoar-koarkan setinggi langit Pancasila, Pancasila, dan Pancasila (antara lain demokrasi Pancasila, Ekonomi Pancasila, moral Pancasila dan seabreg-abreg omongkosong lainnya), tetapi dalam prakteknya malahan melanggar atau merusak, memalsu dan melacurkan isi dan arti sejati Pancasila.

Sejarah sudah membuktikan dengan jelas sekali ( !!!) bahwa Bung Karno telah dikhianati, digulingkan, dan akhirnya « dibunuh » oleh Suharto beserta pendukung-pendukungnya, di dalam negeri maupun di luar negeri (terutama imperialisme AS beserta sekutu-sekutunya). Atas nama Pancasila (yang palsu !) selama 32 tahun lebih, nama baik Bung Karno telah dicemarkan oleh Suharto beserta pendukung-pendukungnya dengan tuduhan-tuduhan  yang tidak benar, fitnahan-fitnahan yang jahat, dan segala macam rekayasa dan praktek yang bathil.

Karenanya, dosa-dosa Orde Baru ,yang amat besar dan tidak bisa dima’afkan atau  dilupakan oleh rakyat kita, adalah pengkhianatan besar-besaran oleh Suharto terhadap Bung Karno, di samping kejahatan-kejahatan besarnya  (sekali lagi, luar biasa besarnya) terhadap golongan kiri pendukung politik Bung Karno.

Golongan Suharto yang munafik dan pura-para cinta Pancasila

Dewasa ini, ada kalangan yang memasang lambang Garuda di baju mereka, dan ikut menyuarakan dengan lantang « Garuda di dadaku, Garuda kebanggaanku », namun yang sebenarnya adalah orang-orang dari kalangan yang anti-Bung Karno dan pendukung Suharto dengan Orde Barunya. Jadi, mereka adalah sesungguhnya orang-orang munafik, penipu, orang-orang reaksioner, yang terdiri dari kalangan atas  sampai bawah. Mereka itulah, yang dalam masa-masa lalu yang lama sekali, telah bersatu dengan Suharto dalam menghancurkan kekuatan politik Bung Karno beserta pendukung-pendukungnya.

Di antara mereka ada yang dewasa ini selalu  menyatakan diri sebagai orang yang mencintai  Pancasila dan menjunjung tinggi-tinggi Bhinneka Tunggal Ika, tetapi tidak menghormati Bung Karno  dan bahkan melawan ajaran-ajaran revolusionernya, dengan berbagai dalih atau alasan. Dan jumlah mereka tidak sedikit.

Kalau ada orang atau kalangan yang munafik semacam itu berarti bahwa mereka tidak mengerti sama sekali arti Pancasila atau tidak faham tentang isi Bhinneka Tunggal Ika, atau sama sekali tidak tahu  -- atau tidak mau tahu !  -- sejarah hidup dan sejarah perjuangan Bung Karno. Atau, mereka itu sebenarnya mengerti arti Pancasila dan Bhinneka Tunggal Ika,  tetapi bersikap tidak jujur karena mempunyai berbagai kepentingan-kepentingan yang bathil atau haram dan anti-rakyat banyak.

Mereka yang mencintai Pancasila mestinya juga  pro Bung Karno

Bolehlah kiranya dikatakan  bahwa orang-orang atau golongan yang betul-betul ( !) menghayati Pancasila, dan sungguh-sungguh ( !)  menjunjung tinggi Bhinneka Tunggal Ika, dan menghormati ajaran-ajaran pro-kepentingan rakyat banyak, akan bersikap pro Bung Karno, dan anti-Suharto beserta Orde Barunya. Dan juga bolehlah  kiranya dikatakan bahwa orang-orang yang mendukung Suharto adalah – pada hakekatnya – anti Pancasila dan anti Bhinneka Tunggal Ika.

Jadi, lambang Garuda yang menggambarkan Pancasila dan Bhinneka Tunggal Ika, yang dipasang di baju merah oleh begitu banyak orang (terutama anak-anak muda) sebenarnya bukanlah sekedar pajangan yang kosong dan tidak mempunyai arti yang besar sekali bagi seluruh bangsa dan negara.

Kalau ditelaah secara serius atau direnungkan dalam-dalam, sebenarnya  fenomena membludaknya orang memakai labang Garuda dan menggeloranya lagu « Garuda di dadaku, Garuda kebanggaanku »  adalah sutu pertanda bahwa sedang terjadi perkembangan yang penting dan menarik di opini rakyat kita, terutama sekali di kalangan anak muda. Yaitu pertanda bahwa Bung Karno hadir kembali di dalam dada dan kepala banyak orang.

Sebagian  (sekali lagi, sebagian) dari orang-orang yang memasang Garuda di baju merah atau menyanyikan « Garuda di dadaku, Garuda kebanggaanku » tidak mengerti, atau kurang memahami, atau tak acuh, bahwa pada hakekatnya (atau sebenarnya) mereka sadar atau tidak, dan secara tidak langsung, sedang mengelu-elukan kembali dan menghormati bapak bangsa kita, Bung Karno.

Namun demikian, adalah suatu hal yang menggembirakan dan patut disambut dengan positif sekali oleh kita semua bahwa lambang Garuda sudah begitu populer atau sungguh-sungguh dicintai rakyat banyak, walaupun sebagian orang masih  tidak (belum)  mengerti banyak atau tidak (belum)  faham tentang arti, sejarah, dan kedudukan simbul agung atau pedoman besar bangsa kita itu. Dan terutama sekali tentang eratnya hubungan atau satunya lambang Garuda, dengan Pancasila dan Bhinneka Tunggal Ika dan  dengan Bung Karno

Menghormati lambang Garuda berarti menghormati Bung Karno

Lambat laun, dan pada waktunya, banyak orang akhirnya akan mengetahui bahwa menghormati lambang Garuda, adalah juga berarti   -- sebenarnya ! -- menghormati Bung Karno. Sebab, dalam lambang Garuda ini terlukis dengan jelas gambar-gambar yang melambangkan Pancasila (gagasan besar Bung Karno) dan yang diperjelas lagi dengan tulisan Bhinneka Tunggal Ika, yang diusulkan oleh Bung Karno  untuk dibubuhkan juga di dalam lambang Garuda tersebut..

Pada dewasa ini, sikap politik pro Bung Karno dan anti Suharto adalah soal yang masih relevan atau erat berkaitan dengan situasi politik, sosial, ekonomi, kebudayaan dan moral ( !!!) yang dihadapi rakyat. Sikap politik pro Bung Karno adalah sikap yang menganjurkan kepada seluruh bangsa untuk mengobarkan  revolusi rakyat terus-menerus, demi kepentingan rakyat banyak menuju masyarakat adil dan makmur, atau masyarakat sosialis à la Indonesia.

Fenomena yang menunjukkan bahwa sebagian besar rakyat kita menghormati Bung Karno adalah amat penting, bahkan maha penting, bagi negara dan bangsa kita, untuk dewasa ini dan apalagi untuk masa depan kita bersama.

Sebab, pengalaman  bangsa kita sejak dikhianatinya  Bung Karno oleh Suharto, sampai sekarang ( !!!)) menunjukkan dengan jelas bahwa semua pemerintahan yang menjalankan berbagai politik yang serba anti-Bung Karno adalah pada hakekatnya  - dan dalam praktek sesungguhnya -- adalah anti Pancasila dan anti Bhinneka Tunggal  Ika, dan karenanya juga anti-rakyat.

Sejarah bangsa Indonesia sudah menunjukkan  dengan jelas -- dan akan terus menunjukkan dengan lebih terang lagi di kemudian hari --  bahwa semua politik dan praktek rejim Suharto dan segala pemerintahan yang berbau Orde Baru, tidaklah mendatangkan kesejahteraan dan kebahagiaan bagi rakyat banyak, melainkan hanya keuntungan bagi kaum elite yang korup dan bagi kepentingan  neo-liberalisme beserta para pendukungnya.

Hanya politik yang didasari jiwa ajaran-ajaran revolusioner Bung Karno-lah yang akan menyelamatkan bangsa dan negara kita. Hanya kesetiaan yang sungguh-sungguh dan tulus kepada Pancasila serta Bhinneka Tunggal Ika-lah yang dapat mendatangkan kesejahteraan dan kebahagiaan kepada rakyat banyak.

Tulisan untuk menyambut Tahun Baru 2011 ini diakhiri dengan harapan bahwa angin yang mulai membawakan benih-benih kebangkitan kembali nasionalisme dan menghidupkan kembali kenang-kenangan  kepada berbagai ajaran revolusioner Bung Karno akan lebih bergelora lagi di masa-masa datang.
