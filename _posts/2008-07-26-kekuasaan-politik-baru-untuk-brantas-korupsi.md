---
layout: post
title: Kekuasaan politik baru untuk brantas korupsi
date: 2008-07-26
---

Mohon parhatian para pembaca terhadap berita yang dimuat dalam Kompas tanggal 21 Juli 2008 mengenai masalah korupsi di negeri kita, yang secara ringkas telah menyoroti masalah ini dengan mengutip berbagai pendapat, termasuk antara lain dari pimpinan Muhammmadiyah, NU, dan kalangan intelektual. Untuk menanggapi berita ini maka disajikan satu tulisan singkat, yang mencoba mengajak para pembaca untuk melihat masalah korupsi ini dari berbagai sudut pandang.

A. Umar Said


============

Kompas 21 Juli 2008

Pemerintah dari Aceh sampai Papua Terjerat Korupsi


Korupsi menyebar merata di wilayah negara ini, dari Aceh hingga Papua. Kasus korupsi yang muncul tak hanya menjerat sejumlah penyelenggara negara, tetapi juga menghambat penyejahteraan rakyat. Korupsi pun menimbulkan gejolak di daerah.


Maraknya korupsi di sejumlah daerah terungkap dari penelusuran data dan pemberitaan yang dilakukan Kompas. Sepanjang tahun 2008 saja, sejumlah kepala daerah dan pejabat di daerah berstatus tersangka, terdakwa, atau terpidana, bahkan dipenjara, terbelit korupsi dengan beragam kasus. Mereka pasti tidak bisa optimal melayani rakyat.
Gubernur Nanggroe Aceh Darussalam Irwandi Jusuf secara khusus melaporkan terjadinya dugaan korupsi pada tujuh pemerintahan kabupaten di wilayahnya ke Komisi Pemberantasan Korupsi (KPK) (Kompas, 19/3). Hal itu dilakukan karena korupsi ”mengganggu” upayanya mewujudkan kesejahteraan rakyat.


Ketua Umum Pimpinan Pusat Muhammadiyah Din Syamsuddin pun memprihatinkan korupsi di negeri ini. Padahal, ibarat gunung es, kasus yang muncul hanyalah puncaknya. Masih banyak kasus yang belum terungkap. Ia terutama memprihatinkan korupsi yang melibatkan kalangan eksekutif dan legislatif. Jelas ini akan memengaruhi kebijakan untuk menyejahterakan rakyat dan memerhatikan hajat hidup orang banyak.


Kian merajalela
Guru besar hukum dari Universitas Sebelas Maret Surakarta (UNS), Adi Sulistyono, Minggu (20/7), menilai korupsi memang kian merajalela, merambah ke berbagai sektor, dari tingkat pusat hingga daerah. Itu terjadi karena selama ini proses hukum pada pelaku korupsi sama sekali tak menjerakan. ”Koruptor yang menimbulkan kerugian negara miliaran sampai triliunan rupiah paling hanya divonis tiga sampai empat tahun. Jadi, bagaimana ada efek jera,” ujarnya di Jakarta.


Menurut Adi, sanksi pidana yang rendah membuat koruptor tidak kapok. Apalagi, penegakan hukum di negeri ini penuh toleransi, memberi koruptor peluang menikmati berbagai fasilitas. ”Kalaupun masuk penjara, beberapa tahun saja. Dengan uang, di penjara dia bisa mendapat fasilitas,” ujarnya. Sanksi sosial pun tidak ada.
”Lihat saja di Solo. Ada anggota DPRD yang pernah dihukum karena korupsi bisa kembali menjalani tugas sebagai wakil rakyat lagi,” kata Adi.
Menghadapi korupsi yang kian parah, menurut Adi, seharusnya pelaku dihukum mati agar ada efek jera. ”Jika tidak dihukum mati, terpidana korupsi harus dimasukkan ke penjara khusus terisolasi sehingga tidak bisa melakukan kontak dengan siapa pun. Mungkin dengan cara seperti ini mereka bisa jera dan tak berani korupsi lagi,” ujarnya.


Banyak peluang
Pakar politik dan pemerintahan dari Universitas Gadjah Mada, Purwo Santoso, mengakui, praktik korupsi yang dilakukan kepala daerah dan pejabat di daerah disebabkan terbuka lebarnya peluang korupsi, antara lain karena kekacauan administrasi keuangan pemerintahan. ”Kesempatan untuk korupsi terbuka lebar. Ada begitu banyak kesempatan bagi pejabat di daerah dan pusat untuk korupsi,” katanya.


Purwo mencontohkan, keberadaan dana taktis atau dana nonbudgeter yang hampir dimiliki setiap kantor pemerintahan di Indonesia sangat membuka peluang untuk korupsi. Hampir tidak ada lembaga pemerintahan yang bisa hidup tanpa dana nonbudgeter. ”Anggaran biasanya baru turun bulan Juni, padahal kantor sudah melakukan kegiatan sejak Januari. Dari mana dana kegiatan itu bisa diambil kalau bukan dari nonbudgeter,” ujarnya.
Selain itu, kata Purwo, tingginya biaya politik yang dikeluarkan kepala daerah selama pencalonan juga memicu tindakan korupsi. Korupsi bukan semata-mata dipicu keserakahan oknum. ”Saat kepala daerah diisi orang-orang politik dan orang politik itu masuk dunia politik dengan membayar, termasuk membayar pemilih, ia sedang memperlakukan jabatannya sebagai komoditas. Dan, saat menjabat, ia harus mencari dana untuk mengembalikan modal yang digunakannya,” katanya lagi.


Kondisi itu sulit dihentikan karena masyarakat pun sebenarnya juga menikmati ”penyuapan” yang dilakukan calon kepala daerah saat pemilihan kepala daerah. ”Kalau kondisi ini tak segera diatasi terus-menerus, kita akan dihadapkan pada situasi politik yang mahal,” ujarnya.


Ketua Umum Pengurus Besar Nahdlatul Ulama KH Hasyim Muzadi pun mengakui, banyaknya pejabat publik melakukan korupsi karena besarnya biaya politik dan sosial yang harus mereka keluarkan, dan budaya hedonistik yang mereka anut. Kondisi itu diperparah oleh sikap sebagian rakyat yang selalu meminta kepada pejabat publik.
Ongkos politik untuk menjadi bupati/wali kota dalam pilkada langsung mencapai puluhan miliar rupiah. Jumlah yang lebih besar dikeluarkan jika mereka ingin menjadi gubernur. Selama menjabat, kepala daerah itu bergaya hidup hedonis, glamour, serta memiliki gengsi yang harus lebih tinggi daripada masyarakat. Kondisi itu berkebalikan dengan sikap pejabat publik negara lain yang mampu hidup sederhana.


Di sisi lain, lanjut Hasyim, dalam masyarakat juga berkembang budaya selalu meminta kepada pejabat, baik permintaan yang wajar maupun tidak wajar, permintaan yang terkait kebutuhan publik, individu, atau kelompok.
Menurut Hasyim, pejabat publik sebenarnya juga merasa ngeri melihat banyaknya pejabat lain yang ditangkap karena korupsi. Namun, mereka sulit keluar dari ”kubangan” yang membuat mereka tetap korupsi.
Sosiolog hukum dari Universitas Diponegoro, Semarang, Satjipto Rahardjo, menegaskan, penanganan korupsi di negeri ini cenderung masih konvensional sehingga korupsi tetap marak. Karena itu, perlu dibuat strategi total yang progresif untuk berperang melawan korupsi. Landasan perang total itu adalah keadilan yang diamanatkan dalam UUD 1945.

* * *

Berikut adalah sejumlah bahan renungan mengenai masalah korupsi untuk kita telaah bersama-sama dan kita coba melihatnya dari berbagai segi :

Korupsi yang sudah puluhan tahun merajalela dengan ganas di negeri kita adalah terutama produk dari sistem politik, sikap mental atau moral para penyelenggara negara dan tokoh-tokoh politik (termasuk parpol-parpol) sejak rejim Orde Baru, yang diteruskan oleh berbagai pemerinatahan sampai sekarang. Adalah jelas sekali bahwa kebanyakan sikap mental atau moral kalangan elite di negeri kita dewasa ini jauh berbeda sekali dengan yang terdapat selama pemerintahan Presiden Sukarno.

Sebab, walaupun dalam pemerintahan Presiden Sukarno ada juga kasus-kasus korupsi, namun tidak sampai seganas dan sebesar seperti yang terjadi sejak Orde Baru sampai sekarang. Presiden Sukarno beserta para menteri-menterinya atau para pembantunya tidak menumpuk kekayaan haram dari curian melalui korupsi. Dan, seluruh pemerintahan di bawahnya tidak dihinggapi penyakit korupsi separah seperti yang terjadi sekarang ini.

Kerusakan moral dan kebejatan iman yang melanda masyarakat luas di negeri kita sekarang ini dipacu oleh contoh negatif yang dipertontonkan oleh kalangan elit (termasuk keluarga Suharto dan pejabat-pejabat tinggi negara baik sipil maupun militer). Dan, karenanya, kerusakan moral atau kebejatan iman ini sudah menjadi kejahatan besar dan merupakan pengkhianatan terhadap rakyat Indonesia. Sekali lagi, perlu ditekankan, KEJAHATAN dan PENGKHIANATAN; yang besar terhadap rakyat.

Seperti yang kita saksikan bersama selama ini, agama pun sudah tidak mampu memperbaiki atau menyembuhkan kerusakan moral dan memperbaiki kebejatan iman yang parah ini. Buktinya, kebanyakan korupsi telah dilakukan oleh mereka yang katanya saja pemeluk agama; yang melakukan sumpah di depan Tuhan, yang rajin sembahyang dan puasa, yang sering mengucapkan ayat-ayat suci, dan yang juga pergi ke Mekah atau Roma.

Karena sebagian terbesar rakyat Indonesia adalah penganut agama Islam, maka perlu diprihatinkan bahwa Muhammadiyah dan NU (dan organisasi-organisasi Islam lainnya) tidak - atau belum - berhasil menjadi pembendung banjir korupsi, yang melanda masyarakat, terutama kalangan elite. Banyaknya kasus korupsi yang parah mencerminkan kegagalan missi yang mereka pikul dan juga menunjukkan kemandulan seruan-seruan yang mereka keluarkan dengan sia-sia saja..

Korupsi yang merajalela di kalangan elit di bidang eksekutif, legislatif, dan judikatif adalah bukti yang jelas bahwa negara kita sudah dirusak secara parah sekali oleh maling-maling yang bekedok macam-macam. Karena kemiskinan rakyat yang sudah sangat parah dan luas sekali, maka kejahatan yang berupa korupsi ini seharusnya mendapat hukuman yang seberat-beratnya.

Bahwa korupsi merusak negara dan bangsa kita dapat dilihat juga dari penyelenggaraan Pilkada di seluruh tanahair kita. Dalam sebagian besar Pilkada ini terdapat berbagai masalah yang berkaitan dengan pengumpulan dana untuk kampanye, untuk “beli suara”, atau untuk operasi-operasi lainnya. Dalam banyak kasus, dana untuk kepentingan Pilkada ini datang dari sumber-sumber atau saluran-saluran yang “tidak halal”. Karenanya, bisalah dikatakan bahwa hasil Pilkada itu kebanyakan bukanlah “pilihan rakyat yang murni”, melainkan kebanyakan adalah hasil rekayasa, dimana faktor dana “tidak halal” memainkan peran yang utama.

Hal yang serupa berlaku bagi Pemilu yang akan datang, bahkan dalam skala yang lebih besar dan cara-cara yang lebih hebat. Partai-partai yang ikut dalam Pemilu memerlukan dana yang besar sekali, dan untuk itu perlu diadakan pendekatan, atau perundingan, atau persetujuan, atau persekutuan, dengan pemilik-pemilik dana yang besar-besar. Sudah dapat diterka, bahwa sebagian besar dari pemberian dana ini tidak gratis saja atau karena kemurahan hati atau berkat “goodwill” saja. Sebenarnya, juga inilah korupsi dalam bentuknya yang lain.

Jadi, kalau ditinjau secara dalam-dalam, hasil Pemilu yang akan datang pun bukanlah sepenuhnya hasil yang lahir 100% dari fikiran, perasaan atau kemauan rakyat, melainkan hasil yang dibuat oleh uang, atau pengaruh, atau berbagai manipulasi lainnya. Hal semacam ini tidak terjadi di Indonesia saja, melainkan juga di negara-negara lain, dalam kadar dan bentuk yang berbeda-beda. Namun, karena situasi yang khusus di Indonesia, maka apa yang terjadi di negeri kita bisa lebih parah dari pada negeri-negeri lainnya.

Mengingat itu semua, nyatalah bahwa Pemilu yang diikuti oleh partai-partai yang mengumpulkan dana dengan cara-cara “tidak halal” itu akan menghasilkan parlemen yang -- kasarnya -- “tidak halal” pula. Artinya, sebenarnya, parlemen tidak akan sepenuhnya merupakan hasil pilihan rakyat yang sesungguhnya. Karena itu, sebagai akibatnya, pemerintahan yang akan dibentuk dengan persetujuan parlemen pun jadinya “tidak halal” juga, kasarnya. Namun, sampai itu sajalah pengertian “demokrasi” yang sekarang ini berlaku di negeri kita dewasa ini.

Dengan sistem politik dan sikap moral tidak pro-rakyat seperti yang sedang dianut oleh pemerintahan SBY-JK serta partai-partai politik sekarang ini, maka bisa diperkirakan bahwa Pemilu 2009 tidak akan menghasilkan DPR dan pemerintahan baru, yang bisa memberesi segala kebobrokan di bidang eksekutif, legislatif, dan judikatif, sehingga bisa memperbaiki situasi ekonomi dan sosial negeri kita yang sudah sangat parah selama ini.

Dengan komposisi politik yang seperti sekarang, yang didominasi oleh unsur-unsur lama dan sisa-sisa Orde Baru, Pemilu 2009 akan menghasilkan korupsi yang tetap merajalela. Sebab, seperti yang kita saksikan dari kasus-kasus korupsi yang dibongkar KPK baru-baru ini banyak dari pelaku-pelakunya yang ditangkap terdiri dari orang-orang pendukung Orde Baru .Berdasarkan pengalaman selama ini dapatlah kiranya dikatakan bahwa korupsi adalah ciri utama Orde Baru,.

Jadi, walaupun akan ada pemerintahan dan DPR yang baru sebagai hasil Pemilu 2009 (atau, bahkan, Pemilu 2014 pun ) nantinya, namun selama sistem politik dan ekonomi pada pokoknya masih seperti yang sekarang juga, maka kecillah harapan bahwa korupsi bisa akan dibrantas besar-besaran. KPK bisa saja menggalakkan tindakan-tindakannya , namun akan tetap tidak akan bisa menangani masalah yang sudah begitu besar dan begitu parah, dan begitu luas pula.

Korupsi hanya dibrantas besar-besaran dan habis-habisan oleh satu kekuasaan politik yang baru, yang menjalankan sistem yang baru pula di bidang politik, sosial, ekonomi dan juga kebudayaan. Kekuasaan politik yang baru ini, harus berbeda sama sekali dengan kekuasaan politik lama yang justru sudah terbukti gagal dalam menyelenggarakan negara secara baik demi kesejahteraan rakyat.

Dengan pandangan yang demikian, nyatalah bagi seluruh kekuatan demokratis – dari golongan dan aliran politik yang mana pun -- untuk terus-menerus membangun kekuatan, dengan berbagai cara, sehingga memungkinkan adanya perubahan yang besar dan fundamental dalam kekuasaan politik.

Negeri kita, Indonesia, sudah memerlukan adanya penggantian kekuasaan politik, dengan yang baru, yang dipegang oleh kalangan muda.
