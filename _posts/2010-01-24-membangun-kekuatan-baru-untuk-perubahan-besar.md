---
layout: post
title: Membangun kekuatan baru untuk perubahan besar
date: 2010-01-24
---

Tulisan ini mengajak para pembaca untuk bersama-sama mencoba menelaah  - dari berbagai segi dan sudut pandang – masalah-masalah besar yang sedang dihadapi oleh bangsa dan negara kita. Sebab, seperti yang sama-sama kita saksikan,  dewasa ini kita semua sedang disuguhi sejumlah besar persoalan-persoalan besar dan kecil yang  menunjukkan bahwa  rakyat kita sedang berada dalam keadaan parah yang tidak menentu. Kiranya, di antara sebagian dari masalah-masalah  yang  bisa sama-sama kita simak adalah yang sebagai berikut :

Banyak kalangan sedang menunggu-nunggu apa saja yang akan terjadi tanggal 28 Januari yang akan datang, yang bertepatan dengan peringatan 100 hari pemerintahan SBY. Karena, sudah ada desas-desus (yang cukup santer) bahwa akan terjadi aksi besar-besaran dari berbagai kalangan, baik dari yang pro pemerintahan SBY maupun  yang dari kalangan oposisi. Bisa saja terjadi macam-macam hal atau keadaan yang tidak terduga ketika  peringatan tanggal 28 Januari ini.

Persoalan skandal raksasa Bank Century sudah berbulan-bulan menarik perhatian dan bahkan menggugah kemarahan besar dari berbagai kalangan, berhubung kasus ini telah menunjukkan bahwa sudah terjadi banyak kesalahan, penyelewengan, atau kejahatan berkaitan dengan « nyasarnya » dana sebesar Rp 6,7 triliun (jelasnya : Rp 6,7 000 000 000 000)  oleh penjahat besar Robert Tantular, yang bersekongkol dengan berbagai fihak.

Pansus Angket DPR yang sudah melakukan pemeriksaan beruntun terhadap berbagai soal yang berkaitan dengan skandal Bank Century sejak permulaan bulan Desember yang lalu sekarang sudah menginjak tahap-tahap permulaan perumusan keputusan atau rekomendasi.

Pansus DPR tidak akan bisa memuaskan

Apa pun yang akan menjadi perumusan keputusan atau rekomendasi Pansus DPR tentang kasus besar Bank Century ini pasti akan menimbulkan heboh. Karena, sekarang saja sudah ada prediksi dari berbagai kalangan bahwa Pansus Angket DPR akhirnya tidak akan bisa mengambil keputusan yang memuaskan bagi sebagian terbesar rakyat yang melihat adanya kejahatan dan penyelewengan besar-besaran di Bank Century ini.

Kita bersama-sama akan bisa melihat bahwa  presiden SBY beserta  pendukung-pendukungya akan terus berusaha menggunakan segala daya dan cara untuk bisa « melumpuhkan »  Pansus DPR yang terdiri dari wakil-wakil partai,  yang justru sebagian besar adalah bagian dari koalisi yang sudah digalang SBY. Ini bisa dilakukannya melalui tekanan-tekanan, negosiasi atau « rekayasa »  (halus dan kasar), terhadap pimpinan partai-partai, termasuk para menteri dan pejabat-pejabat penting di berbagai bidang.

Persoalan lain yang cukup besar adalah masalah Wakil Presiden Budiono dan Menteri Keuangan Sri Mulyani, yang selama ini sudah menjadi sasaran  dari macam-macam aksi atau gerakan (terutama oleh kalangan muda bangsa kita) setiap hari. Persoalan Budiono dan Sri Mulyani ini akan tetap menjadi masalah hangat  dalam beberapa bulan yang akan datang. Agaknya, masih banyak kemungkinan-kemungkinan yang bisa saja terjadi di sekitar ini.

Walaupun Presiden SBY mungkin tidak bisa dilengserkan atau dimakzulkan (impeachment), namun banyak sekali orang menganggapnya bahwa ia harus bertangunggungjawab (secara langsung atau tidak langsung) atas terjadinya penggelontoran uang sebanyak Rp 6,7 triliun oleh KSSK (Menkeu Sri Mulyani bersama Budiono sebagai Gubernur Bank Indonesia).

Dari sidang-sidang pemeriksaan Pansus DPR, muncul berbagai indikasi juga bahwa dalam hal penggelontoran dana sebesar Rp 6,7 triliun kepada Bank Century ini banyak kesalahan-kesalahan yang dilakukan oleh berbagai pejabat (di bidang pengawasan) di Bank Indonesia, yang menyebabkan kelirunya tindakan-tindakan oleh KSSK (Budiono dan Sri Mulyani)

Sesudah Pansus DPR tentang Bank Century bersidang sekitar dua bulan, maka terdengar suara-suara yang menghendaki supaya pekerjaan Pansus ini dipercepat, sehingga seluruh tenaga dan fikiran bangsa dapat dikonsentrasikan kepada penanganan berbagai masalah penting. Sebab, menurut suara-suara ini, masih banyak sekali soal-soal  mendesak yang harus diselesaikan atau dikerjakan.

Adanya suara-suara yang memperdengarkan kejenuhan atau tidak kesenangan terhadap dibongkarnya berbagai ketidakberesan dalam pemerintahan atau lembaga-lembaga penting negara oleh Pansus DPR haruslah kita curigai atau kita sikapi dengan waspada. Sebab, selama sidang-sidang terbuka oleh Pansus DPR banyak sekali keburukan, kebusukan, kesalahan, dan penyelewengan dari pajabat-pejabat negara kita telah ter-ekspose atau tertelanjangi.

Pendidikan politik besar-besaran bagi rakyat

Sidang-sidang terbuka untuk umum Pansus  DPR tentang Bank Century merupakan pendidikan politik  besar-besaran dan luas sekali bagi banyak kalangan masyarakat, yang dapat mengikutinya melalui siaran pers dan terutama televisi. Pendidikan politik yang dimungkinkan oleh munculnya kasus skandal raksasa Bank Century ini merupakan sumbangan yang amat penting bagi kehidupan bangsa, yang belum pernah terjadi sebelumnya (kecuali dalam tahun 1998 ketika Suharto dijatuhkan oleh gerakan besar-besaran oleh angkatan muda kita).

Petunjuk penting dari naiknya kesedaran politik rakyat kita adalah bangkitnya sebagian penting kalangan muda kita (terutama mahasiswa) yang bergerak terus-menerus di banyak tempat di negeri kita untuk melampiaskan kebencian mereka terhadap korupsi yang merajalela dimana-mana, terutama yang terwujud dalam skandal Bank Century dan kasus Anggodo. Gerakan atau aksi-aksi yang dengan gigih dilancarkan kalangan muda kita merupakan motor atau bensin bagi bermacam-macam gerakan rakyat kita lainnya.

Revolusi dan Bung Karno disebut-sebut

Di antara berbagai petunjuk tentang  naiknya kesedaran politik rakyat kita (sekali lagi, terutama sekali angkatan muda) adalah terdengarnya tuntutan terhadap  perubahan besar dan fundamental di negeri kita, disuarakannya kata-kata revolusi, disebut-sebutnya nama Bung Karno, dan dicemoohkannya Orde Baru atau pemerintahan  era Suharto.

Adalah menarik sekali untuk diperhatikan bahwa sejak munculnya kasus Bank Century maka perbedaan antara berbagai aspek pemerintahan Suharto dan pemerintahan Bung Karno telah diangkat dalam berbagai kesempatan. Dalam kaitan ini kita semua dapat menyaksikan bahwa selama kasus Bank Century dipersoalkan, maka puji-pujian terhadap Suharto tidak terdengar.

Dari berbagai sudut pandang dapatlah kiranya kita lihat bahwa kasus skandal raksasa Bank Century tidaklah menguntungkan kepentingan kubu sisa-sisa Orde Baru. Karena, masyarakat melihat bahwa kebanyakan pelaku-pelaku dalam kasus besar skandal ini (seperti halnya dalam kasus BLBI) terdiri dari koruptor-koruptor yang bermental reaksioner, yang pada umumnya adalah pendukung sistem politik anti-kiri atau anti-Bung Karno. Orang-orang yang benar-benar berpandangan kiri atau betul-betul tulus pro-rakyat tidaklah akan tega hati melakukan korupsi secara besar-besaran seperti yang sudah dilakukan para pendukung Orde Baru selama puluhan tahun, sampai sekarang.

Seperti yang sudah kita semua ketahui, korupsi adalah kejahatan yang merugikan kepentingan publik, atau dengan kata lain : mencuri atau maling kekayaan rakyat. Oleh karenanya, korupsi  sebenarnya merupakan pengkhianatan rakyat. Dan pengkhianatan terhadap kepentingan rakyat ini adalah kejahatan yang dosanya besar sekali dan,  karenanya, harus mendapat hukuman yang seberat-beratnya. Oleh sebab itu  gerakan anti- korupsi yang dilancarkan besar-besaran dan terus-menerus di seluruh negeri ini adalah benar-benar tugas mulia dan tujuan luhur dari semua kalangan dan golongan.

Perbedaan besar dengan pemerintahan di bawah Bung Karno

Kiranya, dari sudut pandang ini jugalah kita bisa melihat kasus perampokan Bank Century (dan juga kasus Anggodo), sebagai  hanya puncak dari sebuah gunung es besar sekali,  yang terbenam di bawah pemerintahan Suharto selama 32 tahun dan berbagai pemerintahan sesudahnya.

Korupsi dan segala macam kejahatan yang ditimbulkan oleh berbagai penyalahgunaan kekuasaan adalah ciri utama pemerintahan Suharto (yang diteruskan oleh pemerintahan-pemerintahan lainnya, termasuk pemerintahan SBY). Hal yang demikian sangatlah berbeda atau, bahkan, bertentangan sama sekali dengan pemerintahan di bawah Bung Karno.

Pemerintahan di bawah Bung Karno yang dicintai sebagian terbesar rakyat adalah bertentangan sama sekali, atau berbeda seperti langit dan bumi, dengan pemerintahan Suharto atau pemerintahan-pemerintahan lain sesudahnya. Juga, adalah jelas sekali bagi kita semua, bahwa pemerintahan SBY sebagian besar  - atau pada pokoknya  --  didukung oleh sisa-sisa kekuatan Orde Baru yang anti kiri atau anti ajaran-ajaran Bung Karno.

Padahal, sejarah terbaru bangsa kita sudah menunjukkan dengan jelas bahwa pemerintahan yang menentang ajaran-ajaran besar dan menyampingkan gagasan-gagasan luhur dan revolusioner Bung Karno tidaklah akan  bisa mengadakan perubahan-perubahan besar dan sejati menuju masyarakat adil dan makmur. Agaknya, demikian jugalah halnya dengan pemerintahan SBY.

Buanglah ilusi terhadap partai-partai pendukung koalisi SBY

Dengan perspektif yang demikian ini, maka makin jelaslah bagi seluruh kekuatan demokratik di Indonesia  --  tidak peduli dari golongan yang mana pun, atau dari agama dan suku apa pun --  perlunya untuk membuang jauh-jauh segala ilusi terhadap partai-partai politik yang dewasa ini tergabung dalam parlemen atau menjadi bagian dari koalisi SBY.

Sebab tidak tertutup kemungkinan bahwa dalam menghadapi kasus besar Bank Century  ini (dan masalah-masalah besar bangsa lainnya) akan terjadi berbagai macam kompromi dan negosiasi dalam permainan di belakang layar, yang pada dasarnya atau pada akhirnya akan merupakan persekongkolan kontra-revolusioner terhadap perjuangan rakyat untuk menciptakan masyarakat adil dan makmur.

Agaknya,  sudah dapat diramalkan sejak sekarang, bahwa setelah selesainya pekerjaan Pansus DPR tentang Bank Century yang seluruhnya berlangsung selama 3 bulan, tidak akan terjadi banyak perubahan besar dan penting bagi kehidupan bangsa dan negara. Sebab, banyak urusan penting mengenai negara dan rakyat ini akan tetap diurusi oleh orang-orang yang sekarang juga, atau, oleh mereka yang sejenis dan mempunyai pandangan politik yang serupa. Dan yang begini ini akan berlangsung selama lima tahun lagi !!!

Mengingat itu semua perhatian dan harapan besar berbagai kalangan ditujukan  kepada KPK, sebagai lembaga penegakan hukum dan pemberantasan korupsi. Namun kepercayaan publik terhadap KPK ini harus juga disertai pengawalan dan dukungan, di samping pengawasan yang waspada. Sebab, haruslah sama-sama kita cegah supaya KPK tidak merosot dan membusuk, dengan adanya bermacam-macam korupsi, kejahatan atau penyelewengan.

Patah tumbuh hilang berganti !!!

Karena itu, semua kekuatan demokratik Indonesia,  terutama sekali yang non-partai, perlu setapak-setapak menggalang berbagai bentuk kekuatan, dengan segala cara dan jalan, untuk bersama-sama membentuk kekuatan baru, yang bisa diajak mendatangkan perubahan-perubahan besar dan fundamental. Sebab, sekarang sudah makin jelas bahwa perubahan besar dan fundamental yang menguntungkan rakyat banyak tidak bisa diharapkan lagi sama sekali dari orang-orang yang bermental korup, yang berpandangan reaksioner, yang anti ajaran-ajaran revolusioner Bung Karno, dan yang menjadi pendukung politik Suharto beserta konco-konconya.

Ketika situasi di dunia sedang mengalami perubahan besar terus-menerus (ingat, antara lain : munculnya China dan India sebagai kekuatan besar ekonomi dunia yang baru, kemajuan-kemajuan besar di Rusia, Vietnam, Kuba dan Venezuela, merosotnya peran Amerika Serikat, berkembangnya Uni Eropa) maka Indonesia pun memerlukan perubahan-perubahan besar dan fundamental yang bisa mendatangkan kemajuan besar bagi rakyat.

Bergejolaknya secara besar-besaran opini publik yang disebabkan oleh munculnya kasus Bank Century, dan dilancarkannya terus-menerus bermacam-macam aksi oleh kalangan muda bangsa, merupakan indikasi bahwa benih-benih untuk perubahan besar sedang mulai  tumbuh dimana-mana. Berbagai gejala juga menunjukkan bahwa berbagai kekuatan politik pendukung Bung Karno yang pernah dipatahkan oleh kekuatan reaksioner di bawah Suharto mulai tumbuh kembali dan apa yang hilang dalam tahun 1965-1966 sedang juga berganti.

Kekuatan baru yang digalang atau dibangun bersama-sama oleh berbagai kalangan rakyat inilah, yang bisa meneruskan perjuangan yang telah dilakukan Bung Karno bersama rakyat menuju masyarakat adil dan makmur. Bukannya yang lain !!!
