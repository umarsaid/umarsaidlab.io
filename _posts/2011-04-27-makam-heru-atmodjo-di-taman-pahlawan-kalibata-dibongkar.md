---
layout: post
title: Makam Heru Atmodjo di Taman Pahlawan Kalibata Dibongkar
date: 2011-04-27
---

Atas desakan Gerakan Umat Islam Bersatu

dan perintah pimpinan Angkatan Darat



Berita tentang pembongkaran makam mantan Letkol AURI Heru Atmodjo dan disuruh memindahkannya dari Taman Pahlawan Kalibata merupakan hal yang mengejutkan banyak kalangan, dan dianggap sebagai tindakan yang tidak beradab. Berikut adalah cuplikan dari beberapa berita mengenainya, dan sebuah komentar sebagai bahan renungan untuk kita bersama.



Dibongkar karena Tak Penuhi Kriteria ???



Jakarta (Detik News)   26 April 200- Makam Heru Atmodjo di Taman Makam Pahlawan (TMP) Kalibata dibongkar. Hal itu dilakukan setelah dilakukan penelitian dan ternyata Heru tidak memenuhi syarat untuk dimakamkan di pemakaman khusus para pahlawan itu.

"Setelah diadakan penelitan, ternyata Pak Heru Atmodjo tidak masuk kriteria yang dapat dimakamkan di TMP Kalibata. Karena itu makamnya dipindahkan," kata Kadispenum TNI Kolonel Minulyo saat berbincang dengan detikcom, Selasa (26/4/2011).

Namun Minulyo tidak mengetahu pasti apa kriteria yang membuat Heru tidak dapat dimakamkan di TMP Kalibata. Untuk lebih jelasnya, Minulyo meminta agar dikonfirmasi ke Mabes TNI Angkatan Udara (AU).

"Silakan dikonfirmasi ke Mabes TNI AU ya untuk tahu detilnya dan ke mana dipindahkannya," kata Minulyo.

Menurut Minulyo, makam Heru dipindahkan sekitar satu bulan yang lalu. Untuk proses pembongkarannya, difasilitasi oleh Garnisun. "Tapi untuk pembongkarannya tentu melibatkan keluarga juga," katanya.


Terkait dengan protes massa anti-komunis di Jawa Timur  

Jakarta - Sebuah makam di Blok W Taman Makam Pahlawan Kalibata, Jakarta Selatan, 'dibongkar' setelah 'pemiliknya' telah tidur kekal di dalamnya pada 29 Januari 2011 lalu. Belum diketahui pasti alasan pembongkaran tersebut, yang jelas nisan penghuni makam Heru Atmodjo tidak lagi terbaring di rumah abadinya.

Pembongkaran makam Heru ini diduga terkait protes massa antikomunis misalnya saja protes yang berlangsung di Gedung Grahadi, Surabaya, Jawa Timur, Kamis (10/3/2011) lalu. Heru Atmojo yang berpangkat terakhir Letkol Udara itu, menurut Gerakan Ummat Islam Bersatu (GUIB) Jatim terlibat dalam organisasi PKI.

Saat detikcom berkunjung ke Taman Makam Pahlawan Kalibata, Senin (25/4/2011), tepatnya di Blok W, makam Heru Atmodjo sudah tidak bernisan lagi dan makamnya berada di antara makam Haji Kaya Mas Nien dan Nana Sutresna.

Makam tersebut saat ini rata dengan tanah berwarna merah dan sebuah batu besar diletakkan di tengahnya. Di samping makam tersebut, sebuah papan nisan kayu yang patah tergeletak dengan tulisan sebuah nama, yaitu 'Her'.

" Siapa Heru Atmodjo?

Heru adalah seorang mantan perwira yang berpangkat Letnan Kolonel di kesatuan Angkatan Udara. Sebuah Bintang Gerilya disematkan pada dirinya dari pemerintah karena telah ikut berjuang melawan penjajah pada masa perang kemerdekaan.

Berdasarkan informasi yang dikumpulkan detikcom dari berbagai sumber, Heru Atmodjo dituduh bersalah oleh Mahmilub (Mahkamah Militer Luarbiasa) dengan hukuman seumur hidup dan dipecat dari ketentaraan akibat keterlibatannya dengan PKI (Partai Komunis Indonesia) yang selanjutnya mendapatkan keringanan sehingga menjalani masa hukuman selama 15 tahun.

Tuduhan yang dialamatkan kepada Heru, yang saat itu menjabat sebagai Asisten Direktur Produksi Intelijen Departemen Angkatan Udara, adalah mengetahui tentang rencana PKI yang dikomandoi oleh Letkol Untung untuk melakukan Gerakan 30 September untuk menghabisi petinggi-petinggi militer yang mengganggu rencana PKI. Saat itu Heru menjabat wakil komandan di PKI, posisi nomor 3 setelah Letkol Untung (Ketua Dewan) dan Brigjen Supardjo (Wakil Ketua Dewan).

Pada hari pemakaman Heru, seperti yang dituliskan seorang kawan Heru, Ibrahim Isa di blog jurnal toddoppuli.wordpress.com, pemakaman Heru juga berlangsung khidmat.

“Upacara pemakaman protokoler TNI kamarade Heru dari pk 13.00 – 14.30 wib berlangsung hikmat. Banyak kw hadir baik dr angkatan tua mopun pemuda, mahasiswa, aktivis LSM, disamping keluarga besar alm. Atas nama TNI/Pemerintah mengucapkan penghargaan atas bhakti, kesetiaan dan jasa serta pengabdian alm kepada RI. Upacara kemiliteran satu regu prajurit2 AURI melakukan salvo senjata, pengeningan cipta dan diiringi musik penghormatan terakhir bagi sang pahlawan.”

 Demikian kutipan dari Detiknews



Pernyataan Yayasan Penelitian Korban Pembunuhan 65 (YPKP)



Di bawah ini disajikan pernyataan Bedjo Untung , ketua  YPKP 65 (Yayasan Penelitian Korban Pembunuhan 65), yang selengkapnya berbunyi sebagai berikut :

“Belum ada 100 hari sejak pemakaman Letkol Penerbang Heru Atmodjo (alm), masyarakat  sipil di Jakarta khususnya  yang peduli terhadap hak asasi manusia dikejutkan oleh raibnya jenasah Heru Atmodjo  di Taman Makam Pahlawan Kalibata Jakarta, batu nisan mau pun tulisan yang menyebutkan Heru Atmodjo dimakamkan  hilang dan makam rata dengan tanah (Viva News 24 April 2011).

Ada suatu pertanyaan, mengapa seorang yang berjasa bagi negerinya, yang menyandang bintang gerilya kehormatan bagi Tanah Air yang ketika meninggal dunia  dilakukan upacara kenegaraan resmi  dengan salvo dan upacara militer Angkatan Udara yang mengharukan dan dapat  sebagai pelipur lara bagi para Korban Tragedi kemanusiaan 1965/1966, tetapi kemudian hilang bagaikan  layaknya orang-orang yang terbuang, meski sudah  berujud jenasah?

Inilah bukti  perlu segera penyelesaian secara menyeluruh kasus tragedi kemanusiaan  1965/1966 itu. Negara tidak boleh mengabaikan, malahan memetieskan dan melupakannya. Sampai hari ini Negara belum mengeluarkan sepatah kata apa pun untuk penyelesaian kasus 1965. Padahal, telah diketemukan bukti baru (novum) yaitu adanya rekayasa sistematik untuk melakukan pembunuhan massal  atas orang-orang yang diduga  anggota Partai Komunis Indonesia dan simpatisannya,yang dilakukan oleh aparat  tentara (baca Angkatan Darat)  dengan  berkedok  konflik horisontal  masyarakat, yang difasilitasi oleh CIA, danberujung pada penggulingan kekuasaan Presiden  Sukarno, dan masuklah pemodal Kapitalisme Global,Imperialisme  untuk menguras habis kekayaan Indonesia.



Pembongkaran dengan tekanan

Kelompok Orde Baru khususnya tentara AD  tidak rela jenasah alm Heru Atmodjo dimakamkan di TMP Kalibata. Dengan menggunakan kelompok agama tertentu, memobilisasi massa untuk melakukan penolakan atas pemakaman di Taman Makam Pahlawan.  Massa Islam Jawa Timur mendatangi kantor Gubernur dan mendesak agar Heru Atmodjo dikeluarkan dari Kalibata, dengan tuduhan sebagai dalang G.30.S, tokoh Komunis dan sebagainya, malahan lebih sadis lagi, perlu dibentuk Densus Detasemen Khusus  untuk memburu orang-orang mau pun sisa-sisa anggota Partai Komunis Indonesia.Lagi-lagi, tuduhan tidak berdasar dan terkesan memutar balikkan fakta. Bukan PKI yang berontak terhadap negara, tetapi justru anggota-anggota PKI yang dibunuhi tanpa proses hukum dan pada akhirnya  Jendral Suharto lah yang mengkudeta Sukarno.

Sekelompok militer AD kira-kira 6-7 orang, ada di antaranya yang berpakaian sipil mendatangi rumah keluarga Heru atmodjo di bilangan Cileungsi Bogor. Mereka datang dari markas Cilangkap yang minta agar jenasah Heru Atmodjo dipindahkan.

Jumat  25 Maret 2011 pukul 23.00 malam hari,  makam dibongkar,  pagi harinya Sabtu 26 Maret 2011 jenasah Heru Atmodjo diterbangkan ke Malang dan pada pukul 11.00 siang langsung dimakamkan di Taman Pemakaman Umum desa Bangil Sidoarjo dengan disaksikan oleh utusan dari Angkatan Udara Republik Indonesia dan beberapa anggota keluarga terdekat dalam jumlah yang sangat sedikit. Jenasah disemayamkan disamping ibunda Heru Atmodjo. Tidak ada upacara pemakaman, nyaris bertolak belakang, tidak seperti  ketika diadakan upacara kenegaraan di Taman Makam Pahlawan pada  Sabtu 29 Januari 2011 yang lalu.

Berita ini tersimpan rapat untuk tidak disiarkan ke publik mengingat keluarga ingin ketenangan juga arwah pak Heru Atmodjo ingin ketenteraman di peristirahatan yang terakhir. Namun sebulan kemudian  (25 April 2011) tersiar berita makam Heru Atmodjo telah dibongkar, sehingga banyak kalangan ingin tahu duduk persoalannya.

Sudah 46 tahun  berlalu sejak tragedy kemanusiaan 1965/1966.  Korban yang berjumlah jutaan jiwa sampai hari ini belum ada kejelasan di mana mereka dibunuh, di mana mereka hilang.

Sementara itu, Korban yang masih hidup yang lolos dari maut, telah mengalami siksaan kejam ketika berada di kamp-kamp penahanan Pulau Buru, Nusakambangan,Salemba, Tangerang, Kalisosok, Plantungan,Pulau Kemarau,dll., belum memperoleh hak-hak yang semestinya mereka  dapatkan.

Mereka hidup dalam penderitaan phisik, penderitaan bathin dan penderitaan ekonomi, dan  mereka masih mendapatkan perlakuan diskriminatif, ancaman dan teror.Komisi Nasional Hak Asasi Manusia  telah membentuk Tim Penyelidikan pro justicia untuk Kasus Peristiwa 1965-1966, namun  telah 3 tahun bekerja belum melaporkan hasilnya.

Sampai kapan negeri ini hanya dibalut  persoalan masa lalu  tanpa ada kesudahan? Diperlukan  kemauan politik untuk berani mengatakan  salah  dan meminta maaf kepada Korban pelanggaran HAM 65 dan kemudian memulihkan harkat dan martabat Korban 65 yang terampas secara  melawan hukum. » Demikian isi pernyataan Bedjo Untung, Ketua YPKP 65.



***



Sekelumit renungan tentang kasus pembongkaran

makam Heru Atmodjo



Di bawah berikut ini adalah sedikit bahan-bahan untuk renungan atau pemikiran bersama tentang kasus pembongkaran dan pemindahan makam Heru Atmodjo dari Taman Pahlawan Kalibata.  :



Setelah dibebaskan dari penjara selama 15 tahun, ia aktif sekali dalam kegiatan-kegiatan untuk membela korban Orde Bau. Ia bergabung atau menjalin kerjasama aktif dengan  banyak organisasi , antara lain : Pakorba, LPR-KROB, YPKP, LBH, YLBHI,KONTRAS, dan berbagai organisasi angkatan muda. Nama Heru Atmodjo sering disebut-sebut di banyak kalangan yang menentang rejm militer Suharto dan oleh kalangan pencinta dan pendukung politik Bung Karno.

Seperti banyak perwira atau prajurit AURI (atau  mantan perwira dan prajuiit AURI lainnya) Heru Atmodjo adalah pendukung politik Bung Karno yang jelas-jelas anti imperialisme dan kolonialisme yang pro-sosialisme à la Indonesia,yang pro-rakyat kecil. Itulah sebabnya, ia dibenci atau dimusuhi oleh petinggi-petinggi Angkatan Darat, yang tunduk kepada Suharto, sampai sekarang.

Perintah pembongkaran makam Heru Atmodjo di Taman Pahlawan oleh pimpinan Angkatan Darat (entah terdiri dari siapa-siapa saja dan dari tingkat yang mana saja) adalah satu perintah yang menimbulkan bermacam-macam reaksi di banyak kalangan, karena menusuk sanubari kalangan beragama dan juga mereka yang umumnya menghormati siapa saja yang sudah meninggal dunia.

Perintah pembongkaran makam ini menunjukkan bahwa di kalangan Angkatan Darat masih (sampai sekarang !!!) terdapat fikiran keliru atau pandangan sesat anti-kiri (artinya anti Bung Karno dan anti-PKI) yang selama lebih dari 32 tahun sudah dipakai oleh Orde Barui, sehingga akibatnya negara dan bangsa kita menjadi rusak dan serba semrawut seperti yang kita saksikan sekarang ini.

Pembongkaran makam Heru Atmodjo atas perintah pimpinan Angkatan Darat akan menimbulkan dampak yang besar – dalam berbagai bentuk dan bermacam-macam kadar-  di banyak kalangan bangsa. Dampaknya tidak hanya terdapat di kalangan AURI dan Angkatan Darat saja, melainkan juga antara golongan pendukung Suharto dan golongan pendukung Bung Karno dan simpatisan PKI.

Selama ini, Heru Atmodjo (seperti juga halnya Omar Dhani, mantan Panglima AURI yang Sukarnois seratus persen) mendapat simpati dari banyak kalangan di AURI, berkat sikap politiknya terhadap Bung Karno.

Kalau sama-sama kita renungkan dalam-dalam, maka jelaslah bahwa  perintah pimpinan Angkatan Darat untuk membongkar makam Heru Atmodjo dan memindahkannya dari Taman Pahlawan sama sekali tidak menguntungkan usaha bersama untuk menggalang persatuan bangsa dan mengusahakan tercapainya rekonsiliasi nasional

Dengan pembongkaran makam heru Atmodjo ini pimpinan Angkatan Darat masih tetap menujukkan diri sebagai penyebar terus-menerus perpecahan, seperti yang sudah puluhan tahun dilakukannya, dengan memusuhi berbagai politik revolusioner Bung Karno beserta pendukungnya, yaitu PKI.

Sekarang makin banyak bukti bahwa dengan memusuhi politik Bung Karno dan PKI, tidaklah mungkin Angkatan Darat menjadi penjaga keselamatan Republik Indonesia, seperti yang digembar-gemborkan selama ini.. Justru karena memusuhi ajaran-ajaran revolusioner Bung Karno dan anti-PKI maka timbul masalah-masalah  yang bermacam-macam, yang bisa mengancam keselamatan Republik Indonesia.

Juga, Angkatan Darat tidak akan bisa sungguh-sungguh menjadi pelindung Republik Indonesia, dengan memusuhi PKI dan bersekutu atau bersekongkol   - secara   terang-terangan atau sembunyi-sembunyi  - dengan musuh-musuh RI, antara lain dengan macam-macam kelompok-kelompok fundamentalis Islam yang anti-Bung Karno dan anti-PKI.

Pada dasarnya, atau pada hakekatnya, dengan bersikap anti-ajaran-ajaran revolusioner Bung Karno (artinya, juga anti-PKI) maka Angkatan Darat nyatanya akan  tetap terus menjadi perusak persatuan bangsa, menjadi penjegal Pancasila, menjadi pembunuh Bhinneka Tunggal Ika

Dengan perintah pembongkaran makam Heru Atmodjo, pimpinan Angkatan Darat (yang sekarang)  menunjukkan diri sebagai penerus pengkhianatan besar (dan kesalahan serius sekali) yang telah dilakukan para pendahulunya dalam tahun 1965-1966-1967 terhadap Bung Karno dan PKI.

Selama pimpinan Angkatan Darat (termasuk yang sekarang) belum merobah sikapnya terhadap politik Bung Karno beserta ajaran-ajaran revolusionernya (dan juga  terhadap PKI), maka akan tetap  terus ada banyak persoalan parah bangsa dan negara yang muncul Tidak bisa lain !!! Ini sudah dibuktikan dengan gamblang sekali dengan pengalaman-pengalaman pahit rakyat selama pemerintahan jaman Orde Baru, dan pemerintahan-pemerintahan seterusnya, sampai sekarang.

Dari sudut ini jugalah kiranya kita patut memandang (dan merenungkan !) persoalan pembongkaran dan pemindahan makam Heru Atmodjo dari Taman Pahlawan Kalibata atas perintah pimpinan Angkatan Darat.
