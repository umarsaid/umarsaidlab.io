---
layout: post
title: Gejala yang menggembirakan dari Hari Buruh Internasional 1 Mei
date: 2006-05-03
---

Peringatan Hari Buruh Internasional 1 Mei tahun 2006 di Indonesia baru-baru ini merupakan peristiwa penting yang mungkin akan bisa menimbulkan effek tertentu dalam kehidupan politik, ekonomi dan sosial, berhubung dengan banyaknya orang yang merayakannya dengan pesta dan juga dengan aksi-aksi yang dilakukan di berbagai kota besar dan daerah. Arti penting perayaan Hari Buruh 1 Mei kali ini diperkuat dengan berbagai kegiatan untuk menentang secara besar-besaran - dan serentak - terhadap revisi Undang-Undang Ketenagakerjaan yang dianggap oleh kalangan buruh sebagai rencana yang sangat merugikan kepentingan kaum buruh.

Yang merupakan perkembangan baru dan menggembirakan adalah bersatunya berbagai serikat buruh atau serikat pekerja dalam aksi besar-besaran yang belum pernah terjadi sejak Orde Baru sampai sekarang ini. Hari Buruh 1 Mei tahun 2006 bisa dianggap sebagai kebangkitan kembali gerakan buruh Indonesia, setelah rejim militer Suharto mencekiknya dan membikinnya loyo atau impoten selama 32 tahun. Belum pernah begitu luas dan begitu banyak partisipasi kalangan buruh/pekerja dalam aksi nasional seperti dalam Hari Buruh Internasional 1 Mei 2006 ini. Boleh dikatakan bahwa sebagian terbesar gerakan buruh/pekerja, yang terdiri dari berbagai aliran politik, suku, agama, dan lapangan kerja ikut mengadakan aksi.

Pesan politik dan moral yang dikumandangkan lewat slogan, orasi atau berbagai aksi lainnya selama gerakan nasional 1 Mei ini juga menunjukkan bahwa gerakan buruh Indonesia sedang dalam kebangkitan dan peningkatan kesadaran. (Di antara slogan-slogan yang diusung dalam aksi-aksi itu berbunyi “"Aparat militer jangan ikut campur urusan buruh", "Jadikan 1 Mei sebagai Hari Libur Nasional", dan "Birokrasi Buruk Jangan Salahkan Buruh." – Media Indonesia 30 April).



Fitnah bahwa ditunggangi PKI

Aksi-aksi yang dilakukan oleh bermacam-macam serikat buruh/pekerja di seluruh Indonesia ini merupakan pukulan berat kepada seluruh kekuatan pro Orde Baru dan sisa-sisa rejim militer Suharto dkk. Sebab, kalau selama ini ditiup-tiupkan omong-kosong bahwa gerakan buruh Indonesia selalu didalangi oleh kekuatan kiri atau PKI, maka fitnah rendahan semacam ini terbantah dengan luasnya dan besarnya partisipasi berbagai serikat buruh/pekerja dalam aksi Hari Buruh Internasional 1 Mei 2006. Sekarang, para simpatisan rejim militer Suharto dkk tidak bisa secara gampang-gampangan memfitnah aksi-aksi besar-besaran di berbagai daerah Indonesia hari Senin yang lalu itu sebagai “ditunggangi” oleh anasir kiri atau simpatisan PKI.

Adalah jelas sekali bahwa sasaran aksi-aksi yang dilancarkan itu ditujukan kepada politik pemerintah SBY-Jusuf Kalla (antara lain : revisi UU Ketenagakerjaan, investasi modal asing, kenaikan BBM, kenaikan ongkos hidup sehari-hari), dimana peran militer dan Golkar masih bermain secara kuat. Dan yang “menyulitkan” sisa-sisa Orde Baru adalah kenyataan bahwa pukulan ini dilakukan tidak saja oleh serikat buruh/pekerja yang berhaluan kiri, melainkan juga oleh golongan-golongan yang berfaham kanan, bahkan yang tadinya anti-kiri, atau dulunya anti-SOBSI atau biasanya anti-PKI.

Sudah tentu, gerakan buruh yang berhaluan kiri telah ikut serta dalam berbagai kegiatan Hari Buruh Internasinoal 1 Mei 2006 itu, bahkan mungkin telah ambil bagian yang paling aktif, tetapi barangkali hanya merupakan bagian dari arus besar yang sedang melanda tanah-air kita bersama ini. Kiranya, dapat diramalkan bahwa gerakan buruh kiri Indonesia ini akan terus makin besar peran dan pengaruhnya, seiring dengan perkembangan situasi nasional dan internasional yang makin tidak menguntungkan dunia kapitalisme dengan neo-liberalismenya serta sistem ekonomi globalisasinya itu. Agaknya, arah perkembangan inilah yang tidak akan dapat dicegah atau dilawan terus-menerus oleh kekuatan-kekuatan reaksioner di Indonesia (baca: Angkatan Darat, Golkar dan sebagian golongan Islam).

Aksi besar-besaran 1 Mei yang lalu itu juga menunjukkan dengan gamblang kepada banyak kalangan dari gerakan buruh Indonesia yang selama ini masih bisa ditipu, atau dibujuk, atau dipengaruhi oleh hasutan anti-kiri atau anti-komunis bahwa dari pemerintahan yang masih didominasi oleh kekuatan-kekuatan pro Orde Baru tidak bisa diharapkan politik atau tindakan yang betul-betul menguntungkan kaum buruh/pekerja.



Pernyataan yang “aneh” Jusuf Kalla

Contohnya, dari seorang pengusaha besar yang bernama Jusuf-Kalla, yang sekarang menjadi Wakil Presiden Republik Indonesia dan juga Ketua Umum Partai Golkar, tidak akan dapat diharapkan adanya pengertiannya yang benar - dan jernih - tentang arti Hari Buruh Internasional. Menurut Tempo Alternatif (29/4) Jusuf Kalla “menyesalkan hari buruh diperingati dengan menggelar aksi unjuk rasa. Seharusnya, hari buruh diperingati dengan baik, bergembira, dan syahdu. Hanya Indonesia yang memperingati dengan merencanakan demo. Biasanya di negara-negara sosialis perayaannya selalu bersifat bergembira. Kita harapkan di Indonesia seperti itulah, riang gembiralah. Di Cina dan Rusia hari buruh dirayakan layaknya hari Lebaran. Para buruh pulang ke kampungnya untuk memperingati hari buruh dengan kegiatan yang syahdu dan baik’, kata Kalla.

Pandangan Jusuf Kalla ini lebih-lebih lagi menunjukkan permusuhannya terhadap gerakan buruh dan kurangnya pengetahuannya tentang Hari Buruh Internasional ketika ia mengatakan “tidak setuju pada permintaan buruh agar 1 Mei ditetapkan sebagai hari libur nasional. Apa alasannya?

"Di Indonesia begitu banyak profesi. Ada buruh, tani, nelayan, tentara, guru, wartawan dan lain-lain. Bisa ratusan profesi. Kalo tiap profesi minta libur, nanti kita tidak bekerja kan? Kalau kita setuju buruh libur, bagaimana petani, nelayan, ibu-ibu nanti? Karena itu kita tidak jadikan hari libur," ujar Kalla.

Dia menjelaskan, di negara sosialis dan komunis, dari dulu sampai sekarang tidak ada agama. "Jadi ya tidak ada lebarannya. Maka yang dipakai lebaran diantaranya adalah 1 Mei. Di Cina dan Rusia sekarang libur seminggu. seperti kita lebaran toh? Jadi kultur kita udah beda," begitu argumen Kalla.(Detikcom 16/4/2006)

Hari Buruh Internasional ciptaan Marx ?

Cara berfikir Wakil Presiden Jusuf Kalla (sekali lagi : yang juga Ketua Umum Golkar dan pengusaha besar ini !) yang menggambarkan “keanehan” seorang negarawan di puncak pemerintahan kita sekarang ini, agaknya lebih gamblang lagi kalau juga kita perhatikan cara berfikir sebagian tokoh-tokoh lainnya dalam legislatif. Contohnya adalah yang sebagai berikut :

Menrut Antara (1 Mei 2006) dalam rapat paripurna DPR yang dipimpin Ketuanya Agung Laksono di DPR Aria Bima dan Effendi MS Simbolon, keduanya dari FPDIP, dan Yuddy Chrisnandi (FPG), lewat interupsi masing-masing, mengusulkan agar pemerintah menetapkan 1 Mei sebagai hari libur nasional.

Usulan itu ditentang anggota DPR dari fraksi Partai Golkar Yahyah Zaini dengan argumen bahwa Hari Buruh Internasional itu dicetuskan oleh Karl Marx, gembong marxisme yang namanya pernah dianggap sebagai momok di era Orde Baru.

"Tak layak dan tak relevan menjadikan Hari Buruh Internasional sebagai hari libur nasional karena hal itu ciptaan Karl Marx," kata Yahya, politisi senior dari Partai Golkar itu (kutipan selesai).

Memang, tidak semua orang tahu banyak tentang sejarah Hari Buruh Internasional 1 Mei, dan ketidakpengetahuan ini bukan hal yang membikin orang nista atau memalukan. Tetapi, kalau karena ketidakpengetahuannya ini kemudian menyatakan :"Tak layak dan tak relevan menjadikan Hari Buruh Internasional sebagai hari libur nasional karena hal itu ciptaan Karl Marx," ini sudah bukan hanya pernyataan yang bodoh (ma’af atas kata kasar ini) lagi, melainkan juga nenunjukkan sikapnya yang sama sekali tidak bersahabat dengan kalangan buruh.

Bung Karno : kaum buruh adalah soko guru revolusi

Aksi-aksi yang dilancarkan oleh berbagai kalangan buruh di banyak tempat di Indonesia seperti Jakarta, Bandung, Jogya, Semarang, Surabaya, Makassar, Medan adalah bukti yang jelas bahwa ada kesenjangan yang besar sekali atau, bahkan, sikap yang berlawanan sama sekali sekali antara aspirasi kaum buruh dengan para tokoh partai politik yang berhaluan Orde Baru. Sekarang ini, melalui pengalaman Hari Buruh Internasioal 1 Mei beberapa hari yang lalu, makin banyak orang - baik di Jakarta maupun di daerah-daerah - yang sadar bahwa ada perbedaan yang besar sekali antara politik Presiden Sukarno dengan berbagai politik Suharto beserta rejim militernya.

Kalau Bung Karno menghormati tinggi-tinggi kaum buruh dan kaum tani sebagai soko-guru revolusi dan mengangkat wakil-wakilnya dalam berbagai lembaga penting negara (umpamanya : Dewan Pertimbangan Agung, Dewan Perancang Nasional, MPRS, DPR Gotong Royong , Front Nasional dll ) maka rejim militer Suharto dkk telah berusaha - dengan segala daya-upaya dan rekayasa- untuk mencegah kaum buruh (dan kaum tani) memainkan peran dalam kehidupan politik bangsa. Dan sikap semacam ini berlangsung lama sekali, lebih dari 32 tahun!

Rejim Orde Baru yang di-tulang-punggungi oleh Angkatan Darat dan Golkar (dan sebagian dari golongan Islam) telah menghancur-leburkan sebagian terbesar gerakan buruh yang dimotori oleh SOBSI, dan pemimpin-pemimpinnya di tingkat nasional, propinsi, kabupaten, kota, (dan dalam jawatan pemerintahan atau perusahaan) telah ditangkapi, dipecati, dibunuh atau di Pulau Buru-kan. Dengan dalih atau tuduhan bahwa SOBSI adalah gerakan buruh kiri yang jadi alat PKI, Suharto dkk telah melarang puluhan serikat buruh yang tergabung dalam SOBSI (Sentral Organisasi Buruh Seluruh Indonesia). Dulu, dalam situasi perang dingin, hasutan anti-komunis rejim miiliter Suharto terhadap gerakan buruh masih dapat pasaran.

Situasi internasional berubah terus
Tetapi, sekarang, situasi internasional terus mengalami perubahan. Kubu imperialis yang dibenggoli Amerika Serikat menghadapi segala macam musuh yang makin banyak dan juga makin berani. Bukan hanya di Amerika Latin saja, melainkan juga di benua-benua lainnya. Perlawanan berbagai rakyat di dunia terhadap neo-liberalisme dan globalisasi tercermin dengan jelas dalam peringatan Hari Buruh Internasional 1 Mei. Televisi dunia mentayangkan banyak demonstrasi – di samping pesta gembira - di Paris, Roma Berlin, London, Moskow, Peking, Hongkong, Tokio, Manila, Sidney, New York dan tempat-tempat penting lainya. Siaran televisi dunia dan berita-berita dalam pers ini merupakan bantahan yang sekaligus juga merupakan pemblejetan terhadap pernyataan-pernyataan yang menyesatkan dari Jusuf Kalla serta pembesar-pembesar Golkar lainnya.

Ketika di banyak tempat di dunia dirayakan Hari Buruh Internasional 1 Mei 2006 ini berbagai kalangan multi-nasional di dunia dibikin terkejut – dan marah! – oleh dekrit presiden Bolivia Evo Morales menasionalisasi industri gas bumi yang dimiliki oleh sejumlah modal asing. Presiden Evo Morales mengatakan bahwa sejak sekarang sumber kekayaan alam Bolivia tidak boleh dikuras oleh modal asing. Untuk itu ia mengirimkan tentaranya untuk mengamankan ladang-ladang gas bumi yang banyak terdapat di Bolivia. Bolivia merupakan negeri nomor dua di Amerika Latin yang memiliki sumber gas bumi, sesudah Venezuela (tentang nasionalisasi industri gas bumi Bolivia ini akan ada tulisan tersendiri).

Kelihatannya, sepintas lalu, perkembangan situasi internasional yang dewasa ini tidak menguntungkan kepentingan Amerika Serikat sebagai dedengkot neo-liberalisme dan globalisasi ekonomi sudah mulai ada dampaknya di Indonesia, sedikit demi sedikit. Dan apa yang tidak menguntungkan Amerika Serikat, pada akhirnya dan pada dasarnya, tidaklah menguntungkan kepentingan sisa-sisa rejim militer Orde Baru yang pernah menjadi sekutu setia nekolim selama puluhan tahun

Isyarat ke arah perkembangan yang demikian ini tercermin dalam sikap dan perjuangan berbagai gerakan buruh di Indonesia. Dalam konteks inilah kita melihat gejala adanya kebangkitan kembali gerakan buruh seperti yang dimanifestasikan dalam Hari Buruh Internasional 1 Mei baru-baru ini. Semoga.

Hidup Hari Buruh Internasional 1 Mei !
