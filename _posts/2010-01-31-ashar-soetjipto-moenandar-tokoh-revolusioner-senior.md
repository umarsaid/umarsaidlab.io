---
layout: post
title: Ashar Soetjipto Moenandar, tokoh revolusioner senior
date: 2010-01-31
---

Tulisan ini dimaksudkan sebagai catatan mengenai wafatnya seorang tokoh revolusioner senior Indonesia, Ashar Sutjipto Moenandar (85 tahun) , yang agaknya patut diketahui oleh golongan kiri di Indonesia pada khususnya dan khalayak luas pada umumnya, mengingat pentingnya posisi yang ditempatinya dalam sejarah gerakan kiri di Indonesia.

Pada tanggal 26 Januari 2010 di Rotterdam (Holland) telah  dilangsungkan upacara pemakaman tokoh gerakan revolusioner Indonesia tersebut, dengan mendapat perhatian yang besar sekali dari banyak orang dari berbagai kalangan. Di luar kebiasaan dari kebanyakan upacara  pemakaman lainnya, upacara pemakaman Bung Tjipto ini dihadiri oleh sekitar 250 orang, sedangkan upacara perpisahan dengan almarhum yang  diselenggarakan dua hari sebelumnya dihadiri sekitar 100 orang. Jadi, jumlah orang yang menghadiri dua upacara untuk memberikan penghormatan terakhir kepada Bung Tjipto alm ada sekitar 350 orang. Suatu jumlah yang termasuk luar biasa bagi pemakaman kebanyakan orang asing di Eropa.

Banyaknya orang yang memberikan penghormatan terakhir kepada Bung Tjipto alm dan yang berdatangan dari Holland, Prancis, Jerman dan Swedia, menunjukkan bahwa ia memang dihormati oleh banyak orang. Penghormatan ini berkat sejarah perjuangannya sebagai tokoh pimpinan gerakan kiri di Indonesia sebelum peristiwa 65, dan berbagai kegiatannya di luarnegeri sesudah terjadinya penghancuran kekuatan kiri oleh Suharto beserta konco-konconya.

Untuk memberikan gambaran sedikit tentang kehidupan dan perjuangan Bung Tjipto sebagai pejuang gerakan kiri berikut ini adalah sedikit (dan secara singkat sekali)  dari riwayat hidupnya:

Bung Tjipto dilahirkan di Semarang pada tanggal 2 Mei 1924, sebagai anak keluarga priyayi Jawa. Ayahnya bekerja sebagai dokter pribumi yang bekerja pada pemerintahan kolonial Belanda, sedangkan ibunya adalah seorang yang berdarah bangsawan Jawa

Sebagai anak priyayi ia mendapat pendidikan di sekolah HBS (Hoogere Burger School kemudian di Technische Hooge School di Bandung. Ketika zaman pendudukan Jepang ia bekerja di sebuah laboratorium di Bogor. Sesudah proklamasi  dan terjadi gencatan senjata antara RI dan pihak Belanda, yang melahirkan perjanjian Linggarjati, terbuka kesempatan baginya untuk studi di  negeri Belanda dengan beasiswa Malino. Bung Tjipto mengajukan permohonan dan diterima untuk studi di Technische Hogeschool Delft.

Ketika sudah di Holland Bung Tjipto berkenalan dengan mahasiswa Indonesia lainnya yang belajar di Universitas Leiden,  seperti Zain Nasution (Redaksi Harian Rakyat, tahanan militer sesudah peristiwa G30S, kemudian wafat), dan yang belajar di Amsterdam (antara lain Suraedi Tahsin, Yoke Mulyono, Soekotjo Slamet)

Perkenalan dan diskusi-diskusi dengan anggota-anggota Perhimpunan Indonesia (seperti Soenito, Sunan Hamzah, Soedibyo,Slamet Faiman) memperluas cakrawala pandangan politik Bung Tjipto. Karena, mereka adalah tokoh-tokoh yang berjuang melawan fasisme Jerman dan mendukung kemerdekaan Indonesia.

Bung Tjipto juga mengikuti kegiatan-kegiatan Perhimpunan Nederland-Indonesie  yang dipimpin oleh Prof. Wertheim, dan mengenal CPN (Partai Komunis Belanda)  yang dengan kuat mendukung kemerdekaan Indonesia.

Dengan terjadinya clash kedua antara RI dan Belanda pada tanggal 19 Desember 1948, maka banyak mahasiswa dari Indonesia ini yang tidak mau lagi meneruskan beasiswa  dari pemerintah Belanda, termasuk Bung Tjipto.

Dalam kehidupan yang cukup sulit  karena sudah menolak beasiswa dari pemerintah Belanda,  datanglah kesempatan baginya untuk belajar di Praha (Cekoslowakia) yang diusahakan oleh Perhimpunan Indonesia dan Sugiono (wakil Indonesia di International Union of Students –IUS). Bung Tjipto menggunakan kesempatan itu bersama Zain Nasution, Jack Sumabrata, Bintang Suradi (yang kemudian menjadi pimpinan majalah PKI « Bintang Merah » di Jakarta).

Selama di Praha Bung Tjipto bergaul juga dengan  Suripno, yang waktu itu menjabat sebagai duta besar  berkuasa penuh yang berhasil menggalang hubungan diplomatik antara RI dan Uni Soviet  (Suripno kemudian pulang ke Indonesia bersama tokoh PKI Musso, dan akhirnya mereka dieksekusi bersama Amir Syarifudin bersama kawan-kawannya yang lain dalam peristiwa Madiun)

Bung Tjipto menggunakan kesempatan hidup di Praha untuk belajar tentang sosialisme dan Marxisme dan melakukan kegiatan-kegiatan internasional, antara lain dengan menghadiri pertemuan-pertemuan IUS (International Union of Students) dan ikut festival pemuda sedunia dll (bahan-bahan dirangkum dari « Kilasan Kenangan  Masa Lalu » dalam Kumpulan Tulisan oleh A.S. Moenandar).

Setelah kembali ke Indonesia dalam tahun lima puluhan, Bung Tjipto bekerja dalam dunia pendidikan untuk hidup, sambil ikut dalam berbagai macam kegiatan revolusioner. Karena gaji guru rendah pada waktu itu ia pernah merangkap jadi guru di tiga SMA  di Jakarta.
Kemudian ia juga merangkap sebagai anggota redaksi badan penerbit Yayasan Pembaruan, suatu badan besar yang menerbitkan buku-buku tentang PKI atau buku kiri lainnya tentang sosialisme dan Marxisme.

Karena lapangan kehidupan dan bidang pekerjaan Bung Tjipto sebagai seorang revolusioner adalah pendidikan dan ilmu, maka ia kemudian ditarik untuk ikut memimpin Universitas Rakyat (UNRA) dan mengajar ilmu ekonomi politik. Pada masa inilah Bung Tjipto bertemu dan bergaul akrab dengan para tokoh revolusioner senior yang mengambil bagian langsung dalam pembrontakan nasional melawan pemerintahan kolonial dalam tahun 1926-1927 yang dipimpin oleh PKI.

Ketika dalam 1959 didirikan Akademi Ilmu Sosial ALIARCHAM (AISA) maka Bung Tjipto menjadi salah satu dosennya. Dewan Kurator AISA terdiri dari tokoh-tokoh Politbiro CC PKI, SOBSI, BTI.  Rektor AISA adalah Bismar Oloan Hutapea (meninggal ditembak dalam operasi militer di Blitar Selatan)

Pada tahun 1964 Bung Tjipto diangkat menjadi prorektor AISA. Dengan kedudukannya yang penting ini jelaslah bahwa ia punya hubungan dan kegiatan yang erat dengan pimpinan PKI lainnya, seperti D. N. Aidit, MH Lukman, Njoto, Sudisman, Sakirman, Hutapea dan banyak kader-kader tinggi PKI lainnya, yang sebagian terbesar dibunuhi tanpa pengadilan atau meninggal dalam tahanan.

Waktu kejadian peristiwa 30 September Bung Tjipto tidak ada di Indonesia.  Pada bulan Juli 1965 Bung Tjipto berangkat ke DDR (Jerman Timur) atas undangan Institut Ilmu Sosial di Berlin. Tetapi setelah AISA diobrak-abrik dalam peristiwa 30 September dan AISA menjadi institut pendidikan pertama yang dilarang oleh Orde Baru, tertutuplah kemungkinan  baginya untuk pulang ke tanah air. Mulailah masa pengembaraannya selama puluhan tahun, dan akhirnya Bung Tjipto bermukim di Nederland.

Dengan berpaspor warganegara Belanda Bung Tjipto sudah berkali-kali ke tanahair, untuk menemui istrinya, Siti Tojimah, kawan seperjuangannya, yang pernah aktif di Gerwani dengan mendirikan Taman Kanak-Kanak. Istrinya meninggal dunia dalam tahun 2006 karena sakit yang ditanggungnya sejak lama.

Sejak Bung Tjipto bermukim di Nederland maka ia tetap meneruskan berbagai kegiatan revolusionernya bersama-sama dengan teman-teman seperjuangannya untuk menentang politik rejim militer Suharto, dan menggalang solidaritas dengan bermacam- macam gerakan kiri atau progresif  di berbagai negeri.

Bersama-sama kawan-kawan seperjuangannya dari Indonesia yang lain ia mendirikan Yayasan Studi Asia (« Stichting Azie Studie ») dimana ia menjadi Ketua Pengurusnya. Dalam jangka lama ia menjadi anggota aktif dari Komite Indonesia yang dipimpin oleh Prorf Wertheim dan Go Gien Tjwan, dua sahabatnya sejak lama.

Ia menjadi anggota aktif dan sesepuh dari Perhimpunan Persaudaraan di Nederland, suatu perkumpulan besar dari orang-orang  kiri yang terhalang pulang dan warganegara Indonesia dan Belanda lainnya. Ia termasuk rajin menghadiri pertemuan-pertemuan dari Perkumpulan Persaudaraan ini, dan berkali-kali menjadi pembicara atau ikut dalam kegiatan-kegiatan lainnya. Ia termasuk orang yang dituakan dalam perhimpunan persaudaraan ini.

Juga dengan Lembaga Pembela Korban 65 Nederland ia mempunyai hubungan yang erat untuk bekerjasama mengenai berbagai kegiatan, di samping dengan Amnesty International Nederland.  Dengan CPN (Partai Komunis Nederland) Bung Tjipto mempunyai hubungan dengan kenalan-kenalan lamanya.

Dari itu semua (walaupun serba singkat-singkat saja) kelihatan bahwa Bung Tjipto adalah seorang tokoh intelektual revolusioner, seorang senior dalam gerakan kiri Indonesia, yang sejak muda seluruh hidupnya diabdikan kepada perjuangan untuk kemerdekaan bangsa dan pembebasan rakyat dari segala penindasan, untuk terwujudnya masyarakat adil dan makmur seperti yang dicita-citakan oleh Bung Karno.

Ia sudah berusaha mempraktekkan pandangan hidupnya, sebelum peristiwa 65, dengan menyatukan diri  dengan gerakan kiri di Indonesia, sehingga ia termasuk ke dalam jajaran kepemimpinan yang senior. Sedangkan  ketika ia terpaksa hidup di perantauan puluhan tahun ia tetap  terus.melakukan berbagai macam kegiatan yang berkaitan dengan perjuangan rakyat untuk menegakkan demokrasi dan membela HAM.

Dalam melakukan berbagai kegiatan selama puluhan tahun di perantauan ini, Bung Tjipto selalu kelihatan mengambil sikap « low profile » atau tidak mau menonjol.  Sering sekali ia bertindak atas usul atau anjuran berbagai kawan serjuangannya atau sahabat-sahabat terdekatnya, dan menurut kebutuhan situasi.

Karena kedudukannya yang termasuk « top » dalam gerakan kiri di Indonesia sebelum peristiwa 65, dan juga karena ia selalu mengikuti dari dekat perkembangan situasi di tanahair (melalui internet, pers Indonesia dan kontak-kontak  dengan berbagai kalangan di Indonesia) ia sering dimintai nasehat atau pendapat, ceramah, temuwicara atau partisipasi dalam berbagai pertemuan. Begitulah maka Bung Tjipto menjadi tokoh yang dituakan atau sesepuh oleh banyak kalangan.

Selama puluhan tahun saya sendiri  sering melakukan kegiatan bersama untuk macam-macam soal, dan karenanya merasa dekat dengannya. Barangkali karena itu pulalah maka saya diminta oleh keluarganya (anak-anaknya)  untuk juga mengucapkan kata-kata perpisahan dalam upacara pemakamannya pada tanggal  26 Januari  2010 di Rotterdam itu.

Bagi saya pribadi, seperti halnya bagi banyak orang lainnya yang mengenal apa yang sudah dilakukan dalam hidupnya, merupakan kehilangan seorang yang pantas menjadi panutan dan juga teladan.

Selamat jalan Bung Tjipto,  kita semua selalu mengenangmu !

PS. Secara tersendiri disajikan  teks yang saya bacakan dalam upacara pemakaman Bung Tjipto di Rotterdam tanggal 26 Januari 2010 di samping teks Bung Ibrahim Isa (pengurus Yayasan Wertheim) dan Sungkono yang mewakili Perhimpunan Persaudaraan di Nederland.
