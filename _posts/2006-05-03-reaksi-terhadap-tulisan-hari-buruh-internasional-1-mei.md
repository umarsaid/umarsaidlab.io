---
layout: post
title: Reaksi terhadap tulisan Hari Buruh Internasional 1 Mei
date: 2006-05-03
---

Tulisan yang berjudul “Gejala yang menggembirakan dari Hari Buruh Internasional 1 Mei”, yang disalurkan lewat berbagai mailing-list dan juga dipasang di website http://perso.club-internet.fr/kontak) telah mendapat sejumlah reaksi. Di antara reaksi-reaksi itu terdapat yang dibuat oleh Sdr. Yap Hong Gie, yang disiarkan, antara lain, oleh Tionghoa-net. Untuk diketahui oleh para pembaca, berikut di bawah ini disajikan tulisannya selengkapnya. Dengan membacanya secara cermat, para pembaca bisa menarik kesimpulan sendiri atau mempunyai pendapat tentang isi dan cara atau gaya penulisannya.

Mengenai tulisan Sdr Yap Hong Gie ini, pada saat ini, dan untuk sementara, dari saya sendiri tidak ada atau belum ada niat untuk membalas secara langsung tentang hal-hal yang ditulisnya, baik yang mengenai diri saya maupun mengenai soal-soal lainnya. Barangkali, di kemudian hari, kalau saya merasa perlu, saya akan memberikan pendapat atau reaksi tentang pendapatnya mengenai berbagai hal. Sebab, selama lebih dari 20 tahun saya sudah sering mengutarakan pendapat saya melalui tulisan-tulisan mengenai banyak soal yang berkaitan dengan masalah PKI, tentang buruh, tentang G30S, tentang Angkatan Darat, dan tentang pengkhianatan terhadap negara dan rakyat, tentang rejim militer Orde Baru, tentang Suharto, tentang Bung Karno dll dll dll.

Sebagian dari tulisan ini dapat disimak oleh siapa saja, termasuk oleh Sdr Yap Hong Gie dan orang-orang lain sejenisnya, dalam rubrik “Daftar kumpulan tulisan” dalam website. Atau, dapat juga dibaca dari tulisan-tulisan di berbagai saluran lewat Internet, dengan membuka Google dan meng-klik kata kunci A. Umar Said.

Memang, setiap orang mempunyai hak yang sah untuk menyatakan pendapatnya tentang apa saja, seperti halnya setiap orang pun berhak secara sah juga untuk memberikan balasan, jawaban, penjelasan, atau bantahan, kalau beranggapan bahwa dirinya “diserang” oleh orang lain, apalagi dengan cara atau isi yang terasa kurang mengena atau bisa dianggap menyesatkan opini umum.

Namun, tentang reaksi Sdr Yap Hong Gie terhadap tulisan “Gejala yang menggembirakan dari Hari Buruh Internasional 1 Mei” ini adalah menarik adanya tanggapan-tanggapan, seperti yang disajikan berikut di bawah ini.

A. Umar Said

====================

From: Yap Hong Gie

To: Posting Tionghoa-net

Sent: Wednesday, May 03, 2006 8:42 PM

Subject: [t-net] Gejala yang menggembirakan dari Hari Buruh 1 Mei =HKSIS

Propaganda dan provokasi massa Bpk A. Umar Said --dalam hal ini

memanfaatkan aksi kaum "buruh"-- masih saja seperti gaya PKI tahun 60an.

Manifestopolitik para elite PKI untuk menggerakan revolusi

rakyat, sekarang ini menggunakan istilah "modern" seperti a.l.: "class

action", "peoples power" dlsb, tapi intinya sami-mawon: Revolusi!

Aksi "buruh", yang ditunggangi elemen-elemen oposisi seperti PRD

dll, berlangsung brutal dan anarkhis; melakukan sweeping ke pabrik-

pabrik, melempari aparat dengan batu (melukai 3 anggota Brimob)

merobohkan pembatas jalan tol dan pintu gerbang DPR/MPR, menghancurkan halte bus

serta merusak segala fasilitas umum sepanjang jalan.

Empat puluh tahun lalu, PKI juga meng-claim beranggotakan 3 juta

orang, sedangkan angkatan ke-5; kaum Buruh dan Tani akan

dipersenjatai. Tapi, ketika Angkatan Darat bangkit dan melakukan gebrakan balik,

para elite PKI kabur tunggang langgang meninggalkan dan mengorbankan massa simpatisannya.

Mirip seperti paska kejadian G30S-PKI, sore tadi 30 ribuan

orang "buruh bayaran" dalam waktu singkat bisa dibubarkan aparat kepolisian.

Namun apabila kalangan oposisi mengingingkan Revolusi; seperti

pengulangan peristiwa G30S atau peristiwa Mei'98, maka untuk memancing

kemarahan massa perlu jatuh korban "martir".

Dan bilamana itu terjadi, saya mendukung slogan: "Aparat militer

jangan ikut campur urusan buruh", sehingga kerusuhan bisa berlangsung selama seminggu atau lebih.

Setelah itulah TNI keluar barak untuk melakukan "cleaning

service".

Sejarah akan mencatat kembali pembuktian siapa-siapa

mengkhianati negara ini dan siapa yang menanggung "the cost of the revolution" ....

Wassalam, yhg.

--------------------

From: "HKSIS" <SADAR@

Date: Wed May 3, 2006 1:56 am

Subject: Fw: Gejala yang menggembirakan dari Hari Buruh 1 Mei

hksis_99

Catatan A. Umar Said

(tulisan ini juga disajikan dalam website

http://perso.club-internet.fr/kontak)

Gejala yang menggembirakan dari

Hari Buruh Internasional 1 Mei

-----------------------------------------------------------------

-

Dari Sub Rosa II (Odeon Café), Nasional List, 5 Mei 2006

Saya cukup heran kenapa Oom Chan CT masih mau saja meladeni fosil

McCarthyisme zaman paleotikum seperti Bpk. Yap Hong Gie ini.

Pertama, dengan tanpa etika sedikit pun Bpk. Yap Hong Gie menuduh

Bpk. Umar Said melakukan "provokasi massa". Apa yang beliau maksud

dengan "provokasi massa"? apa mungkin `provokasi massa' itu via

media internet? Dia juga mesti mempertanggung-jawabkan omongannya

bahwa Pak Umar Said itu anggota PKI.

Kedua, Seorang pengusaha spt Yap Hong Gie mana pernah ngerti

penderitaan kaum buruh. Bahkan apa arti "kaum buruh" pun amat

diragukan. Dari komentarnya yang membabi buat itu, tampak beliau

ingin mengatakan bahwa kaum buruh itu identik dengan paham

komunisme. Komentar ini akan membuat marah kaum buruh. Jelas, Bpk.

Yap Hong Gie tidak tau kalau begitu banyak buruh yang berafiliasi ke

PPP, PDIP atau bahkan PKS.

Ketiga, Bpk. Yap HG itu benar-benar tidak mengerti apa arti

kata `revolusi'. Tampaknya, di alam pikirannya, REVOLUSI itu identik

dengan huru hara. Tragedi Mei 98 bisa dia sebut sebagai REVOLUSI.

Aneh betul. Saran saya, lebih baik Pak Yap Hong Gie belajar

linguistik dulu sehingga dapat mengerti apa arti evolusi, revolusi,

run-amock, suksesi, anarkisme dsb. Baru bicara...

Dalam kesempatan ini, bpk Yap Hong Gie juga memberi tudingan pedas

untuk PRD yang ia katakan `menunggangi aksi buruh'. Padahal, pada

demonstrasi hari kedua di depan gedung DPR itu tidak ada elemen PRD.

PRD baru muncul di aksi 1 mei.

Sebagai sebuah partai politik, maka PRD memiliki platform dan visi

tersendiri. Kebetulan PRD memperjuangkan nasib dan perbaikan

kesejahteraan kaum pekerja. Persis sama dengan PKS yang

memperjuangkan kaum islam. Sehingga apabila PRD terlihat amat

concern terhadap kaum buruh maka hal tersebut bukan sesuatu yang

luar biasa. Sama wajarnya apabila PKS terlibat dalam pengajian-

pengajian tilawatil quran. Lewat pernyataan sembarangan ini, pihak

PRD bisa menuntut lewat jalur hukum legal.

Keempat, dengan brutal Pak Yap Hong Gie ngomong kalau 30 ribu massa

buruh itu adalah `buruh bayaran'. Buset….!!! Siapa yang bisa bayarin

30 ribu massa buruh? Nominal kaum buruh yang aksi di depan DPR itu

sekitar 100 ribu orang. Dari sini saja terlihat pengamatan minim Yap

Hong Gie. Lantas dengan tanpa bukti berani menuduh adanya pihak yang

membayari aksi buruh sejumlah 30 ribu orang. Benar-benar ngelantur.

Memangnya, aksi ribuan buruh itu sama dengan aksi beberapa puluh

mahasiswa??

Sub-Rosa II

--- In nasional-list@yahoogroups.com, "ChanCT" <SADAR

Bung YapHG yb,

Entah bung sedang kena penyakit trauma berat atau kebencian-

berat pada komunis-PKI, sehingga apa juga dihubung-hubungkan dengan

PKI. Saya hanya mengenal Umar Said sebagai redaksi Harian "Ekonomi

Nasional" di tahun 60-an, saya tidak jelas apakah Umar Said seorang

komunis atau bukan, tapi yang jelas termasuk orang yang di-"PKI-kan

oleh penguasa Orba, sehingga menjadi manusia-manusia yang "klayaban"

di-Paris karena dicabut kewarganegaraan RI-nya. Namun, saya tertarik

dengan gaya tulisannya dalam membela kebenaran dan keadilan yang

seringkali terinjak-injak itu. Itulah sebab, saya ikut

menyebarluaskan agar lebih banyak orang bisa ikut membacanya.

Bisakah bung Yap membuktikan Umar Said adalah seorang komunis,

bahkan anggota PKI, misalnya? Sehingga mengambil kesimpulan

tulisannya itu merupakan "Propaganda dan provokasi" gaya PKI tahun

60an. Padahal tulisan itu hanya mengkomentari aksi buruh 1 Mei yang

berlangsung secara damai dan tertib itu. Bagaimana bisa dikatakan

tulisan tersebut yang provokasi membuat aksi massa puluhan ribu

mengamuk marah sampai menimbulkan kerusuhan-kerusakan kemarin itu?

Benar, yang provokator adalah Wapres Kalla itu, tuding Ketua Komisi

IX Ribka Tjiptaning.

Sebenarnya saja, saya juga tidak bisa menyetujui sikap-

tindakan massa yang anarkis, yang merusak begitu, ... apa yang

hendak dicapai dengan merusak? Tapi, kita juga harus mengerti mereka

betul-betul terjepit, meledak dengan kemarahan yang tak tertahankan

menghadapi pejabat pemerintah yang tidak bijaksana.

Dimana masalah sesungguhnya? Saya sependapat bahwa kontradiksi

antara majikan-pengusaha dengan kaum buruh harus dibuat ketentuan-

ketentuan yang bijaksana dan relatif adil. Artinya, disatu pihak

ketentuan itu harus bisa menguntungkan majikan-pengusaha, tapi

dipihak lain kesejahteraan buruh harus dijamin lebih baik. Si

majikan ingin mendapatkan keuntungan lebih besar, si buruh juga

ingin menikmati kesejahteraan yang lebih baik. Pada saat ketentuan

itu dirasakan lebih berat memperhatikan kepentingan buruh dan tidak

menguntungkan majikan, tentu harus dirubah agar lebih baik suasana

penanaman modal di Indonesia. Tapi, kenyataan massa buruh yang

sedang terhimpit masalah hidup dengan kenaikan ongkos hidup,

Pemerintah boleh menundanya lebih dahulu, dan tidak memaksakan

sekarang juga dirubah. Sedang, untuk mencapai tujuan semula

menciptakan suasana yang lebih baik dan menguntungkan bagi majikan-

pengusaha, boleh menempuh jalan lain lebih dahulu.

Apa itu? Sementara pengusaha melaporkan (termasuk pengusaha

Jepang) bahwa pungutan-liar yang terjadi di Indonesia, yang berlapis-

lapis dari berbagai birokrasi Pemerintahan, bisa mencapai 30% ongkos

produksi. Luar biasa! Masukilah dari sini lebih dahulu, untuk

meringankan beban pengusaha. Pemerintah harus bertindak tegas,

jangan biarkan pejabat-pejabat hidup menjadi parasit,

mendapatkan "penghasilan" tambahan tanpa berkeringat, yang kenyataan

sangat-sangat menghambat kelajuan perkembangan ekonomi nasional.

Tulisan bung Harsutejo kemarin ini, juga menunjukkan hal ini.

Dilain pihak, Pemerintah juga boleh menjalankan kebijaksanaan

pengurangan pajak, bahkan menghidupkan kembali apa yang dinamakan

Holiday-tax bagi penanam modal di Indonesia yang telah lama

ditiadakan itu. Sebagai upaya menarik lebih banyak modal asing

menanamkan modalnya di Indonesia. Begitulah kenyataan pajak di

Indonesia dirasakan tinggi dibanding negara-negara tetangga dan

adanya pungli sangat tidak menguntungkan penanaman modal di

Nusantara ini.

Bukankah dengan cara demikian, Pemerintah bisa membuat massa

buruh tidak usah marah, dan majikan-pengusaha juga bisa bernapas

lebih lega? Kenyataan yang dihadapi Indonesia dimana ekonomi masih

terpuruk, tanpa menciptakan syarat-syarat yang lebih baik, yang

menguntungkan bagi penanaman modal di Indonesia, tidak hanya modal

asing tidak akan tergiur masuk, modal domestik juga akan melarikan

modalnya keluar, mencari tempat penanaman modal yang bisa lebih

menguntungkan, dan, .... kalau keadaan jelek demikian yang terjadi,

akibatnya ekonomi nasional makin terpuruk dan massa buruh lebih

banyak yang jatuh menganggur, dan mati kelaparan terus berlangsung

menjadi-jadi.

Mudah-mudahan Pemerintah bisa bertindak bijaksana dan kita

semua bisa menyaksikan perkembangan ekonomi-nasional menjadi lebih

baik dan kehidupan rakyat banyak terangkat lebih makmur.

Salam,

ChanCT

* * *

Bung Yap yb,

Tidak, saya tidak menjadi kesal dengan tanggapan bung terhadap tulisan bung Umar Said. Saya anggap itu wajar saja, tidak ada sesuatu yang aneh, setiap orang boleh mempunyai pendapat dan pendirian sendiri. Kita tetap boleh dan bisa bertukar pikiran dengan kepala jernih, mencoba mengangkat masalah-masalah yang penting untuk dibicarakan bersama, bertukar pikiran secara baik-baik.

Dari ketegasan sikap dan pendirian bung yang "mengutuk habis tindakan PKI", seperti kemarin saya katakan, bisa dimengerti. Karena memang lebih banyak kesalahan-kesalahan telah dilakukan PKI ketika itu. Masih cukup baik, bung tetap bisa memisahkan lembaga organisasi, manifes politik dan kesalahan politik PKI dengan orang-orangnya, khusus bung Umar Said pribadi. Tapi, ternyata bung belum berhasil mengajukan deengan jelas, dimana tulisan bung Umar Said itu yang bisa dikatakan sebagai pernyataan yang memprovokasi kamu buruh melakukan demo lagi, demo kerusuhan 3 May itu?

Bung disini hanya mencoba membuktikan bahwa demo 1 & 3 May dibelakangnya terutama PRD dan Ribka Citaning (PDIP) yang berperan dan, ... tidak murni untuk penolakkan revisi UU No.13/2003, tapi adalah rekayasa menjatuhkan pemerintah SBY-JK. Ke-tidak murni-an setiap gerakan tentu bisa dibenarkan, karena setiap gerakan kemungkinan besar bisa saja keselundupan, dimasuki kekuatan-kekuatan lain yang mencoba menunggangi, kekuatan-kekuatan yang berusaha mengail diair keruh, bahkan tujuan semula demo secara "damai" juga bisa ditingkatkan sementara oknum menjadi kerusuhan untuk tujuan politik tertentu. Bisa saja, dan kemungkinan itu bisa saja terjadi. Tidak aneh.

Jadi, 2 masalah ini, apakah benar dibelakang demo itu terutama PRD dan PDIP, dan apakah rekayasa menjatuhkan pemerintah SBY-JK, biarlah kita tunggu bagaimana kesimpulan pemerintah dalam mengusut masalah ini saja. Karena untuk membuktikannya masih diperlukan data-data yang lebih lengkap lagi.

Akan lebih baik kalau kita kembali pada persoalan siapa sesungguhnya provokator demo 3 May yang menimbulkan kerusuhan itu? Yang pasti tidak ada hubungan dengan tulisan bung Umar Said! Saya setuju deengan Tjiptaning yang menuding wapres Kalla provokatornya, yang berkeras mensahkan revisi UU No.13/2003 itu. Kenyataan inilah yang alasan yang digunakan kaum buruh turun kejalan berdemo, ... dan seandainya pemerintah cukup bijaksana, dengan sigap meredam dengan menangguhkan dahulu pengesahan revisi uu No.13/2003 itu, bukankah kaum Buruh tidak ada alasan lagi untuk turun kejalan berdemo. Dan kalau tidak berhasil menggerakkan buruh turun kejalan berdemo, tentu kekuatan-kekuatan yang bung bilang ber-rekayasa untuk menjatuhkan pemerintah SBY-JK juga tidak bisa menggunakan kesempatan ini, bukan?!

Jangan menantang kaum buruh! Apalagi begitu banyak kaum buruh sedang kelaparan, menderita kemiskinan yang sudah berkepanjangan, ditambah makin terhimpit oleh ongkos hidup yang terus membumbung tinggi. Akhirnya Pemerintah juga harus mengalah, menangguhkan pengesahan revisi UU no.13 ini. Betul! Harus begitu, kalau tidak ingin terguling.

Kondisi dan syarat-syarat untuk investasi harus diperbaiki. Ongkos Produksi harus diturunkan dan tidak mungkin dengan menurunkan gaji Buruh! Tanpa menurunkan ongkos produksi di Indonesia yang memungkinkan kapitalis domestik tumbuh lebih baik, dan modal asing juga tertarik menanamkan modalnya di Indonesia, bagaimana mau meningkatkan perkembangan ekonomi, bagaimana mau mengatasi kemiskinan yang makin parah itu? Pemerintah harus tegas menghapuskan pungli, pungutan liar yang kabarnya sudah menelan sampai 30% dari ongkos produksi. Sudah keterlaluan! Dengan berhasil dihapuskannya pungli bukankah kondisi-syarat penanaman modal di Indonesia menjadi jauh lebih baik. Bahkan kalau berhasil baik, masih memungkinkan untuk sedikit meningkatkan kesejahteraan buruh/pegawai lebih baik.

Bukankah begitu, mestinya?

Salam,
ChanCT

=============


----- Original Message -----
From: Yap Hong Gie
To: Posting Wahana-news ; Posting Nasional-list
Sent: Sunday, May 07, 2006 1:43 AM
Subject: [nasional-list] Gejala yang menggembirakan dari Hari Buruh 1 Mei => ChanCT


Bung Chan Yth,

Kekesalan Bung Chan bisa saya mengerti, oleh karena Anda hanya menilai
tanggapan saya terhadap tulisan Bpk A. Umar Said, tanpa bisa mengikuti
secara akurat fakta dan kenyataan permainan politik yang sedang berlangsung
di negeri ini.

Saya tidak pernah menutup-nutupi pandangan dan pendapat politik saya, yang
dengan ini saya pertegas kembali bahwa, saya mengutuk habis tindakan PKI,
dengan segala onderbouwnya.
Jadi, yang saya tentang secara absolut adalah lembaga organisasi dengan
manifesto politik PKI, serta cara-cara penghasutan dan provokasi massa,
seperti gaya tulisan, ulangi, gaya tulisan Bapak A. Umar Said.
Akan tetapi saya tidak membeci person-nya, apalagi beliau tidak saya kenal
secara pribadi.


Indikasinya simpel saja, darimana sumber dana yang segitu besar untuk
mengakomodir dan mobilisasi massa dan buruh sampai puluhan bahkan ratusan
ribu orang di Jakarta dan kota-kota besar lainnya?

Sejak minggu malam 30 April saya mengikuti radio aparat keamanan yang
mendata bus-bus dan massa buruh yang masuk di Parkir Timur Senayan, para
Korlap (koordinator lapangan) ternyata berasal dari unsur PRD, yang sebagian
menginap di Sofian Hotel.

Senin 01 Mei 2006 pukul 11.30 WIB, Komisi IX yang diketuai oleh Dr. Ribka
Ciptaning Proletariati (RCP), menerima 50 orang perwakilan massa buruh,
kemudian membuat kesepakatan bersama yang mengatasnamakan keputusan DPR-RI,
untuk menolak membahas revisi UU No 13/2003 tentang Ketenagakerjaan (UUK).
Tadinya skenario sudah di set untuk men- fait a complit dan memaksa pimpinan
DPR membuat pernyataan menolak draft revisi UU. 13/2003, namun tidak
berhasil. Maksud dan tujuannya adalah untuk membenturkan lembaga legislatif
dengan eksekutif, seperti waktu menjatuhnya pemerintahan Presiden Gus Dur.


Proses persiapan RCP untuk menyambut demo buruh sudah terlihat
tanda-tandanya jauh sebelumnya.
Rakyat Merdeka, Jumat, 31 Maret 2006,
(kutipan):
Ribka Tjiptaning Proletariyati menegaskan, partainya sudah pasti akan
menolak revisi tersebut. "Anggota PDIP yang tidak menolak, mereka PDIP
gadungan," jelas Ketua Komisi IX ini saat dihubungi Jumat sore (31/3).

Siapa gerangan Ketua Komisi IX; Dr. Ribka Ciptaning Proletariati (RCP)?
RCP adalah Ketua Umum Paguyuban Korban Orde Baru (Pakorba), penulis buku
"Aku Bangga Jadi Anak PKI". Jadi tidak heran kalau RCP membuat pernyataan
bahwa Wapres Kalla --yang kebetulan adalah Ketua Umum Golkar-- yang
melakukan provokasi.


Pada tanggal 1 Mei saya terima SMS dari seorang sahabat beraliran
"kiri-luar", yang berbunyi:
"Bangunlah kaum yang terhina!
Bangunlah kaum yang lapar!
Mari lawan penindasan dan penghisapan!
Bersatulah kaum Buruh Sejagad!
SELAMAT HARI BURUH DUNIA!"

Lhaaa ucapan selamat semacam ini apa iya sambutan perayaan ulang tahun atau
lebih cocok disebut menabuh gederang perang?


Semua orang juga tahu bahwa demo buruh pada tangga 01 dan 03 Mei 2006
bukanlah murni, dalam rangka memperjuangkan penolakan Revisi UU No.13 Thn
2003, tetapi hanya merupakan salah satu agenda dari rangkaian aksi untuk
menjatuhkan pemerintahan Presiden SBY-JK.

Jadi slogan-slogan manis "membela kebenaran dan keadilan" hanyalah merupakan
variasi hiasan, guna menarik simpati massa, sekaligus meng-kamuflir agenda
politik yang sebenarnya.

Wassalam, yhg.
