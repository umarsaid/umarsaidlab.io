---
layout: post
title: Nasional Demokrat beserta gerakan perubahannya
date: 2010-02-07
---

Barangkali saja ( sekali lagi, barangkali !)  dideklarasikannya kelahiran ormas baru Nasional Demokrat pada tanggal 1 Februari 2010 di Istora Senayan (kompleks Gelora Bung Karno) Jakarta bisa   merupakan peristiwa yang penting sekali bagi kehidupan bangsa dan negara. Karena pentingnya, maka agaknya patutlah mendapat perhatian dari banyak kalangan dan golongan. Termasuk Anda, yang membaca tulisan ini.



Sebab, bagaimanapun juga, munculnya Nasional Demokrat adalah  satu fenomena yang sangat menarik dalam situasi negeri kita dewasa ini. Sebagian dari aspek-aspek yang menarik dari lahirnya ormas baru ini bisa kita coba bersama-sama menelitinya. Yang berikut di bawah ini adalah berbagai hal, yang berkaitan dengan lahirnya Nasional Demokrat ini :



Deklarasi yang mahal biayanya



Pertama-tama, untuk mendeklarasikan berdirinya  Nasional Demokrat telah dikeluarkan biaya lebih dari RP 1 miliar (tapi tidak sampai Rp 4 miliar) Biaya yang besar itu adalah untuk konsumsi 13 000 hadirin (kotak nasi dan kueh), honor artis, penyanyi, musisi, penari dari berbagai daerah    (menurut Persda Network, 2 Februari 2010)



Seperti sudah dapat diduga, sebagian terbesar sekali dari pembiayaan upacara besar-besaran dan megah ini, dikeluarkan oleh Surya Paloh, seorang pengusaha besar, pimpinan Grup Media Indonesia, pemilik stasion televisi nasional yang makin populer Metro TV, yang juga Ketua Dewan Pembina Golkar, dan pernah mencalonkan diri sebagai calon presiden. Ketika Munas Golkar di Pakanbaru tahun yang lalu, ia dikalahkan oleh Aburizal Bakri dalam pertarungan untuk menjadi Ketua Umum Golkar.



Yang dipertanyakan oleh banyak orang adalah mengapa Surya Paloh bersedia mengeluarkan biaya yang begitu besar untuk mendirikan Nasional Demokrat bersama Sri Sultan Hamengkubuwono (kedua-duanya masih sebagai tokoh-tokoh terkemuka Golkar,  dan menjadi inisiator didirikannya ormas baru ini). Ada kalangan yang memperkirakan bahwa hal ini disebabkan oleh sakit hati atau marahnya Surya Paloh karena ia dikalahkan Aburizal Bakri. Ada pula yang menduga bahwa karena Surya Paloh ingin mendirikan partai baru dan ikut dalam pemilu presiden yang akan datang.



Apapun latar belakangnya, banyak kalangan yang meramalkan bahwa didirikannya Nasional Demokrat ini tidak menguntungkan partai Golkar.



Gerakan perubahan dan restorasi Indonesia



Dalam berbagai pernyataan yang dikeluarkan oleh tokoh-tokoh Nasional Demokrat ditegaskan  bahwa Nasioal Demokrat adalah ormas baru yang menyatakan diri sebagai suatu gerakan perubahan untuk merestorasi Indonesia.



Lahirnya satu ormas baru di Indonesia dengan slogan besar dan bendera penting – yaitu : gerakan perubahan – adalah sangat menarik, ketika negara dan bangsa kita berada dalam situasi yang penuh kebobrokan (ingat antara lain banyaknya korupsi, kasus Bank Century, kerusakan moral, kebusukan mental para pejabat dan politisi, yang sudah berjalan lama sekali)



Kalau Nasional Demokrat nantinya bisa sungguh-sungguh (!!!) membuktikan diri sebagai gerakan perubahan untuk merestorasi Indonesia, maka kehadirannya akan sangat memperkuat gerakan extra-parlementer Indonesia yang akhir-akhir ini sudah mulai kelihatan berkembang dalam berbagai bentuk, cara dan jalan.



Apalagi kalau Nasional Demokrat bisa menjadi gerakan perubahan yang betul-betul lintas partai, lintas agama, lintas suku, dan didukung oleh sebanyak mungkin  lapisan masyarakat, sampai ke tingkat akar rumput.




Apa saja yang akan dirubah ?



Adalah suatu janji yang besar  -- atau  slogan yang kedengarannya tinggi -- ketika Nasional Demokrat berani menamakan diri sebagai gerakan perubahan. Agaknya, orang pun bisa saja bertanya-tanya : “apa saja sih  yang mau dirubah, dan dirubah menuju kemana atau untuk dirubah menjadi yang bagaimana?”.



Sebab,  kalau betul-betul mau mengadakan perubahan,  pekerjaaan ini tidak kecil dan mudah. Kerusakan atau kebusukan di negeri kita sudah terlalu besar dan juga terlalu luas atau terlalu parah, yang disebabkan oleh pemerintahan rejim militer Suharto, dan oleh sisa-sisa yang diwariskan Orde Baru

Jadi, justru segala yang berbau Orde Baru itulah yang perlu dirubah, atau segala hal negatif yang bercorak Suharto-lah yang perlu dirombak. Tidak bisa lain !!!



Gerakan perubahan tidaklah mungkin dilakukan dengan  (atau bersama-sama) sisa-sisa Orde Baru atau pendukung politik Suharto. Artinya,  gerakan perubahan (yang sungguh-sungguh !) tidaklah bisa dijalankan  oleh kalangan yang anti-ajaran Bung Karno. Gerakan perubahan  yang sejati (!!!)  tentunya menentang segala Suharto-isme. Karena, seperti yang sudah sama-sama kita saksikan  selama 32 tahun Orde Baru, Suharto-isme adalah kontra-revolusi besar-besaran, yang secara khianat sudah berusaha memadamkan revolusi rakyat di bawah pimpinan Bung Karno. Suharto-isme adalah reaksioner, atau kontra-revolusioner, artinya : kemunduran dan kerusakan atau pembusukan bangsa dan negara.



Nasional Demokrat dan ajaran-ajaran Bung Karno



Karena Surya Paloh adalah tokoh tinggi Partai Golkar (Ketua Dewan Pembina) dan Sri Sultan Hamengkubowono juga tokoh Golkar yang cukup ternama maka ketika kita mendengar bahwa kedua tokoh ini menjadi inisiator Nasional Demokrat maka timbul pertanyaan : “apa sih sebenarnya ormas baru ini ?”



Sebab, ketika upacara di Istora Senayan dilangsungkan, terasa sekali bahwa suasana pada waktu kental sekali dengan “jiwa” Bung Karno. Suasana yang mengingatkan kepada Bung Karnp ini diperkuat atau dipertegas dengan pidato Surya Paloh yang  mengangkat peran Bung Karno dalam sejarah Republik Indonesia. Ia mengingatkan lebih dari sepuluh ribu hadirin (yang memakai baju biru sebagai uniform Nasional Demokrat) tentang keberanian Bung Karno untuk menyelenggarakan GANEFO sebagai perlawanan Komite Olimpiade Internasioal.



Surya Paloh dengan berapi-api mengungkap betapa pentngnya  pidato Bung Karno di Sidang Umum PBB  ( “To build the world anew”)  dan ajaran-ajarannya tentang nasionalisme, patriotisme, gotong royong, dan melawan fikiran-fikiran yang hanya mementingkan diri sendiri atau golongan sendiri.



Yang menarik perhatian adalah bahwa Surya Paloh, sebagai tokoh tingkat atas atas Partai Golkar, jelas-jelas banyak memuji-muji Bung Karno, sedangkan satu patah kata pun tidak diucapkannya mengenai Suharto.



Ini adalah satu indikasi adanya perkembangan yang perlu diamati terus. Apakah Nasional Demokrat selanjutnya akan “mengambil jarak” dari sisa-sisa Orde Baru ? Apakah pidato Surya Paloh tentang Bung Karno betul-betul berdasarkan ketulusan dan keyakinan ? Dan bukannya sekadar “taktik” atau kemunafikan ? Dan apakah Nasional Demokrat akan betul-betul menjadi gerakan perubahan untuk merestorasi Indonesia ? Kita akan saksikan, barangkali (!), tidak lama lagi.



Sambutan hangat untuk Nasional Demokrat



Mengingat situasi politik di Indonesia dewasa ini, dapatlah kiranya dikatakan bahwa lahirnya ormas baru Nasional Demokrat sebagai alat baru (atau alat tambahan) bagi perjuangan banyak kalangan  adalah suatu hal yang baik. Kekuatan gerakan extra-parlementer, yang kelihatan makin memainkan peran penting akhir-akhir ini, bisa mendapat “tambahan tenaga ” yang kuat dan penting.  Ketika peran partai-partai politik (dan juga DPR dan DPRD) dan pemerintah makin kehilangan kepercayaan publik, maka peran gerakan extra-parlementer yang kuat adalah besar (dan penting sekali). Ini sudah ditunjukkan, sebagian, dengan pembongkaran kasus Bank Century.



Kita harapkan saja bahwa Nasional Demokrat nantinya memang betul-betul  akan bisa menjadi bagian dari gerakan extra-parlementer yang kuat, bahkan menjadi unsur utama atau elemen penting – di samping banyak ormas-ormas lainnya --  yang bergerak untuk adanya perubahan besar-besaran  di dalam bidang politik (secara langsung atau tidak langsung), ekonomi, sosial, dan,moral.



Sebab, agaknya, Nasional Demokrat nantinya bisa dimasukkan dalam jajaran ormas yang besar, seperti ormas keagamaan NU atau Muhammadiah. Karena itu peran Nasional Demokrat bisa besar sekali, kalau saja betul-betul menjadi gerakan perubahan. Perubahan besar-besaran dan fundamental sudah ditunggu-tunggu  -- sejak lama ! -- oleh rakyat Indonesia, yang sebagian terbesar masih dalam keadaan yang memprihatinkan sekali.



Dan untuk mengulangi lagi, gerakan perubahan yang dijadikan program besar Nasional Demokrat tidaklah mungkin tanpa memerangi, menghancurkan, dan meninggalkan sama sekali segala macam sistem semasa era Suharto (sistem politik, ekonomi, sosial, kebudayaan, dan moral).



Citra Bung Karno yang makin meninggi



Satu hal yang menarik sekali tentang  hadirnya ormas baru Nasional Demokrat dengan penampilan dirinya sebagai gerakan perubahan dan restorasi Indonesia kelihatan dari dijadikannya Bung Karno sebagai sumber inspirasi gerakan. Hal ini bukan saja hanya kelihatan dari suasana ketika pendeklarasian lahirnya ormas baru ini serta pidato Surya Paloh yang berapi-pi tentang Bung Karno.Melainkan juga dari siaran-siaran Metro TV, yang pemiliknya adalah Surya Paloh.



Metro TV setiap hari sejak pagi sampai sore menyajikan siaran-siaran (berupa pesan-pesan atau iklan, nyayian) tentang Nasional Demokrat. Juga setiap pagi, pagi-pagi sekali, kita bisa mendengarkan Bung Karno mengucapkan proklamasi 17 Agustus 1945, yang didahului oleh lagu kebangsaan kita Indonesia Raya. Kiita dibangunkan  setiap pagi oleh ingatan bersama kepada sosok Bung Karno beserta proklamasi 17 Agustus 1945. Alangkah indahnya pesan politik dan moral yang kita dengar bersama setiap pagi demikian ini.



Oleh karena itu, kalau jalan ini  -- artinya : jalan Bung Karno  --  yang dipilih oleh Nasional Demokrat dalam perjalanannya untuk gerakan perubahan, maka besarlah harapannya bahwa ormas ini akan mendapat dukungan luas atau simpati besar sekali dari banyak orang  Sebab, jalan Bung Karno adalah jalan revolusi yang terus-menerus, artinya jalan perubahan besar yang tidak ada henti-hentinya.



Ajaran-ajaran besar Bung Karno tidak saja terkandung   dalam Bhinneka Tunggal Ika atau Pancasila saja, melainkan juga kelihatan dalam pemikiran-pemikirannya yang lain yang tercantum dalam bukunya “Di bawah Bendera Revolusi” atau buku “Revolusi Belum Selesai”.



Jadi, kalau Nasional Demokrat mau berhasil dengan gerakan perubahannya, demi kepentingan rakyat dan negara, maka sudah seyogianyalah bahwa jalan revolusi (artinya perubahan besar yang terus-menerus) Bung Karno-lah yang dipilih. Jalan lain, seperti yang sudah ditempuh Suharto dengan Orde Barunya selama 32 tahun  - disusul oleh pemerintahan-pemerintahan berikutnya – adalah jalan kegagalan.
