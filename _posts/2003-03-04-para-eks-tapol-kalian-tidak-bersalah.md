---
layout: post
title: Para eks-tapol, kalian tidak bersalah!
date: 2003-03-04
---

Ada satu berita menarik yang patut mendapat perhatian kita semua. Berita ini menunjukkan, untuk kesekian kalinya, bahwa sisa-sisa fikiran Orde Baru masih kuat melekat di kalangan aparat negara. Juga menunjukkan kepada kita semua, bahwa perjuangan untuk memenangkan reformasi, demokrasi, HAM, peri-kemanusiaan, kebenaran dan keadilan, adalah tetap merupakan tugas nasional yang urgen bangsa kita.

Berita menarik itu adalah yang disiarkan oleh harian Suara Merdeka (Semarang) tanggal 2 Maret 2003, mengenai eks-tapol di Jawa Tengah. Untuk dapat sama-sama kita telaah isinya atau kita analisa berbagai aspeknya, maka disajikan kembali teks lengkap berita itu, yang berbunyi sebagai berikut :

“Pernyataan Pangdam IV/Diponegoro Mayjen TNI Cornel Simbolon yang menyebutkan lebih dari 10.000 eks tahanan politik (tapol) golongan A, B, dan C mendirikan partai, LSM, serta yayasan akan segera ditindaklanjuti jajaran Polda Jateng.

''Informasi dari Pangdam itu sangat baik, karena itu akan segera kami tindaklanjuti,'' ungkap Kapolda Jateng Irjen Pol Drs Didi Widayadi ketika menghadiri HUT Ke-53 Kodam IV/Diponegoro di Makodam.
Didi meminta agar semua komponen masyarakat selalu mewaspadai adanya people power yang bertujuan seolah-seolah untuk kepentingan rakyat, tetapi hanya untuk kepentingan kelompok atau golongannya. ''Masyarakat juga harus ikut mengawasi atau mewaspadai gerakan tersebut.''

Dia menekankan, akan mengambil tindakan tegas bila kekuatan masyarakat itu melanggar hukum. Tanpa memandang apakah kelompok masyarakat itu dari eks tapol atau bukan. ''Seperti tindakan sweeping, itu kan tindakan melanggar hukum. Jadi tidak boleh dilakukan sembarang orang.'' Ketika ditanya wartawan, daerah mana saja yang rawan tumbuhnya partai, LSM atau yayasan eks tapol, Kapolda tak memberikan penjelasan terperinci. Dia justru yakin, para wartawan sudah mengetahui daerah itu.

Seperti diberitakan, Pangdam IV/Diponegoro Mayjen TNI Cornel Simbolon menyatakan lebih dari 10.000 mantan tapol golongan A, B, dan C saat ini mendirikan partai, LSM atau yayasan.

Kodam bahkan sudah menginventarisasi partai, yayasan, dan paguyuban yang didirikan mantan tapol tersebut. Diyakini sebagian besar pengurus adalah bekas tapol. Ada beberapa nama yayasan, LSM atau partai, seperti Yayasan Pencari Pembunuhan Tahun 45, Partai Perjuangan Rakyat, dan Yayasan
Paguyuban Orba (kutipan berita Suara Merdeka selesai).

## Eks-Tapol Tidak Merupakan Bahaya

Apa saja yang bisa kita jabarkan dari berita ini? Banyak! Antara lain bahwa Kodam Jawa Tengah sudah menginventarisasi partai, yayasan, dan paguyuban yang dididirikan mantan tapol. Bahwa Kodam menyelidiki segala kegiatan para eks-tapol adalah wajar. Asal dengan maksud dan tujuan yang baik. Sebab, dengan menyelidiki atau mengamati kehidupan dan kegiatan para eks-tapol maka Kodam (atau aparat negara lainnya, termasuk kepolisian) bisa mengetahui secara detail - dan kongkrit! - bahwa para eks-tapol bukan merupakan bahaya bagi negara dan keamanan umum.

Kodam Jawa Tengah, atau semua Kodam di seluruh Indonesia (termasuk BIN dan Mabes TNI-AD di Jakarta) sebenarnya sudah tahu presis bahwa para eks-tapol, yang jumlahnya ratusan ribu dan tersebar di seluruh negeri itu, tidak melakukan kegiatan-kegiatan subversif, tidak mengadakan gerakan-gerakan yang menimbulkan ketakutan kepada masyarakat, dan tidak menimbulkan kekacauan.

Sebaliknya, kodam-kodam di seluruh Indonesia mengetahui dengan presis bahwa bahaya yang mengancam keselamatan Negara Kesatuan RI datang dari “fihak sana”. Pengalaman beberapa tahun terakhir ini menunjukkan dengan jelas bahwa ledakan-ledakan bom, pembunuhan-pembunuhan, atau gangguan keamanan (termasuk aksi-aksi “sweeping” yang macam-macam itu) bukan bikinan kalangan para eks-tapol. Tetapi, dilakukan oleh golongan-golongan yang justru memusuhi para eks-tapol, seperti kalangan yang menamakan dirinya Front Anti Komunis, dengan persekongkolan dengan sebagian dari aparat-aparat negara.

## Dosa Besar Tni-Ad Terhadap Eks-Tapol

“Kodam bahkan sudah menginventarisasi partai, yayasan dan paguyuban yang didirikan mantan tapol tersebut. Diyakini sebagian besar pengurus adalah bekas tapol”, demikian kita baca dalam berita tersebut. Ada kesan bahwa penegasan tentang “inventarisasi” ini mengandung ancaman atau peringatan. Seolah-olah mau dijelaskan bahwa para eks-tapol tidak boleh mendirikan partai, yayasan, LSM atau paguyuban dan menjadi pengurusnya. Dan bahwa persoalan-persoalan mengenai eks-tapol yang dikemukakan oleh Pangdam Diponegoro (Mayjen Cornel Simbolon) ini akan segera “ditindaklanjuti” oleh jajaran Polda Jateng.

Pernyataan Panglima Kodam Diponegoro ini menunjukkan bahwa pada hakekatnya TNI-AD masih belum menjalankan reformasi dan masih belum mau menebus berbagai dosa-dosanya atau memperbaiki kesalahan-kesalahannya yang serius pada masa lalu. Dalam kasus para eks-tapol ini besarnya dosa dan beratnya kesalahan TNI-AD ini amat jelas sekali. Di Jawa Tengah saja berapa banyak orang tidak bersalah telah dibunuhi secara sembarangan, dan berapa banyak pula di seluruh negeri ? Di Jawa Tengah berapa orang yang tidak bersalah telah secara sewenang-wenang ditahan di penjara (dan di pulau Buru) selama bertahun-tahun? Berapa istri dan anak-anak telah terpaksa hidup terlantar dan dalam kesengsaraan karena ditinggalkan secara paksa oleh suami atau bapaknya?

Kalau TNI-AD, sebagai soko-guru Orde Baru (bersama-sama Golkar), mau menebus dosanya yang besar dan kesalahannya yang berat, maka salah satu di antara berbagai cara penting adalah membantu rehabilitasi para eks-tapol beserta keluarga mereka. Kesalahan atau dosa TNI-AD terhadap para eks-tapol (beserta keluarga mereka) telah menyebabkan kesengsaraan yang panjang dan penderitaan yang lama sekali, sampai sekarang. Ini tidak bisa dibantah, sebab sejarah sudah membuktikannya. Saksi dan bukti masih cukup banyak. Juga di Jawa Tengah.

## Yayasan Dan Paguyuban Para Eks-Tapol

Para eks-tapol (beserta keluarga mereka) telah dizalimi secara kejam dan tidak berperikemanusiaan dalam jangka waktu yang lama sekali oleh rezim militer Suharto dkk. Karenanya, banyak sekali di antara mereka yang menderita parah dan berkepanjangan. Tidak tanggung-tanggung, puluhan tahun! Setelah Suharto jatuh, mereka berusaha menolong diri mereka sendiri, karena pemerintah (dan masyarakat) tidak - atau belum - menolong mereka. Mereka tetap dibiarkan menderita, dan tetap dipinggirkan. Untuk menghadapi situasi yang tidak adil ini mereka berusaha bersama-sama mendirikan berbagai organisasi, LSM, yayasan atau paguyuban. Ini adalah hak mereka yang sah sebagai warganegara. Seharusnya, kegiatan mereka ini mendapat sambutan positif dan luas dari semua fihak, termasuk berbagai bantuan dan fasilitas dari pemerintah beserta aparat-aparatnya.

Sebab, melalui berbagai kegiatan mereka memberi sumbangan penting untuk memperbaiki kesalahan-kesalahan yang pernah dilakukan selama Orde Baru (baca : TNI-AD dan Golkar), terhadap para eks-tapol (beserta keluarga mereka). Tidak ada golongan atau fihak yang dirugikan dengan kegiatan semacam ini. Bahkan sebaliknya. Rehabilitasi para eks-tapol (beserta keluarga mereka) memudahkan terciptanya rekonsiliasi. Rehabilitasi adalah hak mereka yang sah! Sebab, mereka pernah disiksa secara sewenang-wenang dan secara tidak sah. Rehabilitasi adalah tuntutan mereka yang benar dan adil, baik secara hukum maupun secara moral. Perlakuan sewenang-wenang terhadap para korban Orde Baru adalah kejahatan.

Membiarkan berlarut-larutnya penderitaan para eks-tapol adalah melanjutkan terus kesalahan dan dosa yang pernah dibuat di masa lalu. Penderitaan berkepanjangan para eks-tapol tidak mendatangkan kebaikan sedikit pun bagi bangsa. Tidak ada fihak mana pun yang diuntungkan dengan melestarikan ketidakadilan.

## Para Eks-Tapol, Tegakkan Kepalamu!

Membaca pernyataan Pangdam Kodam Diponegoro, ada baiknya kita serukan kepada para eks-tapol Jawa Tengah (dan juga para eks-tapol di daerah-daerah lainnya) : tegakkan kepalamu! Teruskan kegiatan-kegiatan untuk memupuk solidaritas antara sesama eks-tapol beserta keluarga mereka. Galanglah terus persaudaraan dan kerjasama dengan berbagai organisasi lainnya, dalam memperjuangkan demokrasi, reformasi dan rehabilitasi, demi tercapainya rekonsiliasi.

Kalian sudah terlalu lama dizalimi. Kalian sudah berpuluh-puluh tahun diperlakukan sewenang-wenang secara tidak berperi-kemanusiaan. Padahal, kalian tidak bersalah dan tidak berdosa terhadap kepentingan negara dan rakyat. Kalian tidak mengganggu ketenteraman umum dan tidak merugikan kepentingan siapa-siapa.

Karenanya, kalian berhak menggugat “mereka”. Menggugat mereka adalah sah, benar dan adil! Sebaliknya, tidak menggugat, berarti membiarkan kesalahan terus terjadi. Artinya, tidak menggugat adalah salah. Kalian di fihak yang benar. Sejarah memihak kalian, bukannya “mereka”.

Karena itu, selanjutnya di kemudian hari janganlah segan-segan atau takut-takut untuk menggunakan hak-hak kalian sebagai warganegara Republik Indonesia. Kalian ikut memiliki negara ini. Kalau ada sisa-sisa Orde Baru – dari fihak yang mana pun datangnya - mau menghalang-halangi kegiatan kalian, bongkarlah masalahnya di depan opini publik. Opini publik perlu tahu dan ikut mengutuk adanya tindakan atau fikiran yang masih bercorak Orde Baru demikian ini.

Para eks-tapol harus berani menggunakan segala cara dan sarana untuk menunjukkan bahwa “mereka” –lah yang telah berdosa, dan bahwa, karenanya, mereka harus memperbaiki kesalahan yang telah berjalan begitu lama ini. Artinya, para eks-tapol tidak perlu merasa bersalah, karena memang tidak bersalah. Artinya lagi, menjadi “tapol” bukanlah suatu hal yang hina. Yang hina adalah “mereka”.
