---
layout: post
title: Rakyat menanti-nantikan perubahan besar
date: 2009-12-23
---

(Tulisan untuk menyambut tahun baru 2010)



Sekarang makin kelihatan bahwa situasi di negara kita menunjukkan berbagai pertanda-pertanda bahwa sedang terjadi perkembangan yang sangat penting dan menarik sekali untuk sama-sama kita amati. Sepuluh tahun lebih sesudah Suharto turun dari rejim militernya, sekarang sedang terjadi perubahan-perubahan yang cukup besar kalau dibandingkan dengan 32 tahun berkuasanya Orde Baru.

Yang paling menonjol dalam perkembangan dewasa ini adalah makin tingginya kesedaran politik dari berbagai golongan masyarakat dalam menghadapi beraneka ragam  masalah serius yang sudah, sedang dan akan dihadapi oleh bangsa dan negara.

Ini menunjukkan bahwa rakyat kita, sesudah selama puluhan  tahun dicekek, dibungkam, ditindas dengan berbagai cara diktatorial,maka setapak demi setapak mulai bangkit untuk memperjuangkan hak-hak dan kepentingan mereka dengan  melawan segala macam kebobrokan, kebejatan, atau ketidakberesan yang sedang melanda negeri kita.

Kita semua masih ingat betapa ketatnya cengkeraman golongan militer dalam menjalankan kekuasaan waktu itu, sehingga sebagian terbesar rakyat Indonesia takut bersuara akibat berbagai macam intimidasi atau cara terror yang dijalankan secara menyeluruh dan di berbagai bidang kehidupan bangsa. Oleh karena itu, dalam jangka yang lama sekali, tidak banyak orang yang berani atau bisa mengkritik Suharto beserta pemerintahannya, walaupun terjadi berbagai macam kejahatan yang serius, kesalahan yang besar, atau pelanggaran yang berat, baik di bidang politik, ekonomi, sosial maupun  HAM (termasuk pembantaian jutaan orang tidak bersalah dan pemenjaraan ratusan ribu orang tanpa pengadilan)

Permulaan lahirnya situasi baru

Sekarang ini, kalangan yang manapun atau golongan yang apapun, bisa dan berani bersuara secara bebas, mengkritik atau menghujat segala macam kebobrokan, yang masih terus terjadi di negeri kita, sebagai warisan rejim Orde Baru, dan yang masih diteruskan oleh pemerintahan SBY dewasa ini. Dan seperti yang kita saksikan bersama, di antara banyak penyakit parah yang menyerang tubuh bangsa ini adalah masalah korupsi. Korupsi adalah  salah satu di antara bukti-bukti tentang hebatnya kerusakan moral di berbagai kalangan bangsa kita, terutama sekali di lapisan atas.

Walaupun sejak jatuhnya Suharto dalam tahun 1998, masalah korupsi sudah dinyatakan sebagai penyakit bangsa yang harus diberantas, namun, nyatanya, korupsi masih merajalela terus dengan hebatnya. Dengan munculnya kasus kriminalisasi Bibid-Chandra dan heboh besar tentang skandal raksasa Bank Century, masalah korupsi sedang menjadi masalah besar bangsa, yang skalanya atau dimensinya menjadi sangat luas dan juga sangat jauh, melebihi kasus-kasus sebelumnya.

Kasus kriminalisasi KPK dan skandal raksasa Bank Century telah menyundut kemarahan rakyat banyak secara besar-besaran, yang agaknya bisa  merupakan permulaan dari lahirnya situasi baru dalam gerakan rakyat atau gerakan ekstra-parlementer yang kuat dan besar di Indonesia. Bagaimanapun juga,  situasi politik, sosial dan  ekonomi yang keruh dan penuh gejolak dewasa ini merupakan pendidikan politik (yang besar sekali !) bagi banyak kalangan dalam masyarakat.

Keadaan yang begini tidak boleh diteruskan

Kasus kriminalisasi KPK, apalagi skandal raksasa Bank Century telah menyebabkab dibeberkannya banyak kebejatan moral banyak pejabat tinggi dan tokoh-tokoh masyarakat, yang betul-betul mengagetkan banyak orang. Kita disuguhi tiap hari berita-berita tentang “hilangnya” uang Rp 6,7 triliun ( supaya lebih jelas, Rp 6,7 triliun adalah Rp 6,7 juta dikalikan sejuta), tentang jalannya sidang Pansus DPR soal Bank Century, tentang Anggodo yang masih belum juga ditindak, tentang perlu tidaknya Wapres Budiono dan Menkeu Sri Mulyani non-aktif, dan tentang apakah Presiden SBY terlibat (atau harus turut bertanggungjawab) dalam soal Bank Century.  

Hal yang demikian ini adalah wajar, mengingat bahwa korupsi yang ditimbulkan oleh bejatnya moral dan kebrengsekan sikap politik sebagian besar politikus dan pemuka-pemuka masyarakat yang reaksioner dan bermental korup ( yang umumnya atau sebagian terbesar adalah pendukung atau pemuja Suharto dan juga anti-Bung Karno) sudah membikin negeri kita sebagai negara yang sakit parah.

Sebagian besar rakyat negeri kita dewasa ini mulai makin jelas melihat bahwa keadaan yang sudah dialami selama puluhan tahun pemerintahan Orde Baru  -- dan yang diteruskan oleh berbagai pemerintahan sesudahnya sampai sekarang-- , tidak bisa dan tidak boleh diteruskan lagi. Pemerintahan SBY pun, yang terbukti tidak banyak bedanya dengan pemerintahan-pemerintahan lainnnya di masa lalu, dianggap oleh banyak golongan sebagai pemerintahan yang tidak akan bisa memperbaiki kehidupan sebagian terbesar rakyat kita.

Pentingnya peringatan 9 Desember 2009

Itulah sebabnya maka peringatan Hari Internasional Anti-korupsi Sedunia (tanggal 9 Desember yang lalu) dijadikan kesempatan oleh banyak sekali golongan masyarakat di seluruh negeri (terutama kalangan muda) untuk secara besar-besaran memuntahkan rasa muak mereka yang sudah memuncak dengan mengadakan aksi-aksi di berbagai kota. Meskipun gerakan rakyat itu bisa dihambat oleh  imbauan -- bercampur intimidasi !-- presiden SBY, namun toh tanggal 9 Desember 2009 telah menjadi peristiwa yang penting dalam membangunkan kesedaran banyak golongan untuk tetap terus menjadikan skandal Bank Century sebagai sarana atau wahana dalam perjuangan besar untuk adanya perubahan-perubahan.

Banyak sekali golongan atau organisasi yang sebelum dan sesudah 9 Desember 2009 telah mengadakan aksi-aksi anti korupsi, sampai sekarang masih terus melakukan berbagai macam kegiatan. Aksi-aksi ini masih tetap dilangsungkan tidak henti-hentinya (hampir tiap hari !) di berbagai kota  terutama oleh kalangan muda (organisasi-organisasi mahasiswa dan pemuda). Berbagai macam golongan masyarakat juga tetap terus bergerak dalam macam-macam  bentuk dan cara. (Contohnya : menurut TV One, pada tanggal 21 Desember 2009 saja ada 10 macam demonstrasi atau aksi yang diadakan di Jakarta dalam satu hari)

Yang menarik untuk diamati adalah bahwa berbagai macam gerakan  atau aksi yang dilancarkan di seluruh negeri dewasa ini pada umumnya tidak dilancarkan oleh partai-partai politik. Karena, kebanyakan partai-partai politik yang besar-besar (yang lolos dalam pemilu yang lalu) sudah tergabung dalam koalisi yang dibentuk oleh SBY, dan karenanya banyak partai-partai ini sedang “diborgol”  atau “diloyokan” untuk tidak bisa melakukan oposisi yang terang-terangan terhadap pemerintahan SBY.

Kiranya, makin menjadi jelaslah bagi banyak orang, bahwa di Indonesia dewasa ini, yang bisa betul-betul menjadi motor gerakan besar-besaran untuk perjuangan mengadakan perubahan adalah kekuatan rakyat yang terdapat di luar partai-partai politik dan di luar DPR. Dan juga makin jelas bahwa rakyat yang mendambakan adanya perubahan-perubahan drastis dan fundamental di negeri kita tidak bisa lagi  dan tidak boleh terus-menerus menggantungkan harapan yang terlalu besar kepada peran kebanyakan partai-partai politik, DPR atau kabinet SBY.

Motornya perubahan adalah di “parlemen jalanan dll “

Kiranya, perkembangan situasi di negeri kita akan membuktikan bahwa perubahan-perubahan besar dan sejati hanya akan bisa dilahirkan oleh motor-motor perjuangan  yang terdapat di “parlemen jalanan”, di berbagai macam gerakan di kampus-kampus universitas di seluruh negeri , di pabrik-pabrik, dan di pedesaan (mudah-mudahan juga, nantinya,  di pesantren-pesantren)

Sebab, apa yang dilakukan partai-partai politik dalam menghadapi masalah KPK dan Bank Century sudah dengan jelas menunjukkan sikap atau tindakan yang mengecewakan sekali bagi rakyat banyak, dan karenanya kredibilitas partai-partai politik, DPR dan kabinet yang terdiri dari wakil-wakil partai, sudah jatuh merosot jauh sekali.

Berbagai macam aksi atau gerakan yang tanpa henti-hentinya sudah -- dan sedang terus -- dilakukan di banyak tempat di seluruh tanah-air menunjukkan adanya perkembangan opini publik yang sangat penting untuk menghadapi masalah-masalah besar rakyat dan negara.

Yaitu,  pendapat bahwa partai-partai politik atau DPR atau kabinet SBY tidak bisa mengadakan perubahan-perubahan besar untuk adanya perbaikan kehidupan rakyat,  tanpa desakan, atau tanpa dorongan, bahkan , tanpa paksaan (!)  dari rakyat. Kesedaran yang demikian inilah yang akan bisa menjadi dasar atau landasan yang kuat untuk berkembangnya gerakan extrs-parlementer di Indonesia.

Aksi-aksi atau gerakan yang dilakukan terus-menerus oleh berbagai golongan masyarakat untuk melawan korupsi (antara lain dalam kasus KPK da Bank Century) merupakan syarat utama atau bagian penting sekali dari perjuangan bersama melawan politik pemerintahan SBY yang didukung         oleh bermacam-macam golongan reaksioner dan anti-rakyat.

Barisan oposisi makin meluas

Perkembangan situasi politik di Indonesia dewasa ini menunjukkan bahwa sedang terjadi keretakan atau perpecahan di kalangan elite reaksioner yang umumnya korup dan  yang dalam jangka lama sekali tadinya mendukung Orde Baru. Sedangkan barisan oposisi terhadap pemerintahan SBY menjadi makin meluas dengan menggabungnya berbagai kalangan (intelektual, pemuda, mahasiswa, aktivis-aktivis buruh dll dll).

Barisan oposisi, yang terdiri dari gerakan ekstra-parlementer yang makin banyak ragamnya, agaknya akan berkembang terus sebagai akibat persoalan skandal Bank Century yang masih makan waktu lama untuk menyelesaikannya. Seperti kita ketahui, di antara banyak sekali gerakan extra parlementer di negeri kita ini, di Jakarta saja terdapat puluhan, antara lain KOMPAK, Gerakan Indonesia Bersih, Revolusi Desember, KAPAK dll dll.

Agaknya, karena skandal raksasa Bank Century mungkin masih akan terus menjadi persoalan besar yang membikin keruh situasi , maka rakyat banyak akan bisa mendapat pelajaran politik yang penting dan berharga, untuk mengenal lebih baik lagi sifat-sifat serta praktek dari pejabat dan para pemuka masyarakat yang korup, reaksioner dan anti-rakyat.

Rakyat menanti-nantikan perubahan besar   

Yang juga patut diamati bersama adalah bahwa dalam hiruk-pikuk atau heboh mengenai Bank Century dewasa ini adalah bahwa opini publik memandang segala kebobrokan, kebusukan, dan kejahatan korupsi ini ada hubungannya dengan sisa-sisa Orde Baru. Karena, makin jelas juga bahwa sebagian terbesar sekali kebobrokan dan kejahatan yang kita saksikan dewasa ini adalah warisan atau akibat (atau produk) dari sistem politik dan sikap mental Orde Baru.

Itulah sebabnya bahwa akhir-akhir ini nama Suharto sudah tidak banyak disebut-sebut lagi, dan bahwa banyak sekali atau sering sekali adanya berbagai macam ejekan tentang kejelekan-kejelekan Orde Baru. Hal yang demikian ini nampak sekali kalau kita sering melihat berbagai tayangan televisi yang menyajikan perdebatan, atau dialog dengan para pemirsa, dan terutama dalam siaran-siaran Metro TV (dan TV One).

Segala macam aksi atau gerakan anti korupsi dan anti pemerintahan SBY yang menggelora dewasa ini adalah pertanda penting bahwa rakyat dan negeri kita sedang menghadapi masa-masa keruh yang penuh gejolak, lebih besar dan lebih serius dari pada yang sudah-sudah.

Ini menunjukkan bahwa rakyat kita sedang  menanti-nantikan (dan juga mempersiapkan)  kedatangan  perubahan-perubahan besar dan fundamental, yang akan memunculkan pimpinan nasional (yang sejiwa dengan  Bung Karno) dan pemerintahan type baru,  untuk menjalankan sistem politik dan ekonomi yang berlainan sama sekali dari yang sudah dijalankan sejak zaman Orde Baru sampai sekarang.

Kiranya, makin jelas bahwa inilah jalan revolusi, jalan perubahan besar,  menuju masyarakat adil dan makmur, yang dicita-citakan rakyat sejak proklamasi 17 Agustus 45  !!!
