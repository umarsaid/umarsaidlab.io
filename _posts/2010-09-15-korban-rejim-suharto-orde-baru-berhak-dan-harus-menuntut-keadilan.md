---
layout: post
title: Korban rejim Suharto (Orde Baru) berhak dan harus menuntut keadilan !
date: 2010-09-15
---

Dalam mengenang peristiwa 30 September 1965 satu hal yang tidak bisa dilupakan adalah masalah para korban kekuasaan militer Suharto atau rejim Orde Baru. Sebagai akibat atau dampak peristiwa 30 September 1965 ini puluhan juta orang telah menjadi korban, yang terdapat di banyak kalangan atau golongan dan dalam berbagai bentuk atau macam-macam  keadaan.

Di antara para korban yang banyak sekali itu ada yang terdiri dari anggota atau simpatisan PKI, tetapi juga banyak sekali yang tak ada hubungannya sama sekali dengan PKI. Yang jelas adalah bahwa  di antara para korban Orde Baru itu banyak sekali (bahkan sebagian terbesar) yang menjadi pendukung politik Bung Karno, baik yang aktif sekali maupun hanya ikut-ikutan saja.

Karena Bung Karno adalah pemimpin revolusioner bangsa yang paling terkemuka dan Presiden Republik Indonesia yang dicintai rakyat banyak, maka dengan sendirinya pendukungnya juga amat banyak. Dan karena Bung Karno sejak muda sudah menunjukkan dirinya sebagai pejuang revolusioner, maka dengan sendirinya pula, berbagai golongan yang berhaluan revolusioner (atau golongan kiri) mendukung politiknya yang anti-kapitalisme, anti-kolonialisme, anti-imperialisme dan pro-sosialisme.

Di antara berbagai golongan kiri itu  PKI beserta puluhan ormas-ormasnya merupakan pendukung utama politik Bung Karno, baik yang berkaitan dengan masalah-masalah dalam negeri (nasional) maupun luar negeri (internasional). Dapatlah kiranya disimpulkan bahwa dukungan yang kuat dari PKI terhadap Bung Karno adalah karena adanya persamaan pandangan revolusioner dalam perjuangan untuk kepentingan bangsa menuju masyarakat adil dan makmur, yang bebas dari penindasan manusia oleh manusia dan penindasan bangsa oleh bangsa.

Berbagai peristiwa anti Bung Karno dan anti-PKI di masa lalu

Sebenarnya, kaum reaksioner dalam negeri (antara lain : sebagian dari kalangan Masyumi, PSI, dan sebagian dari Angkatan Darat) sudah sejak lama mempunyai sikap yang anti-Bung Karno dan anti-PKI, jauh sekali sebelum peristiwa 30 September 1965. Di antara tonggak-tonggak sejarah anti-Sukarno dan anti-PKI adalah (yang pokok-pokok atau yang besar) : RMS, peristiwa Kahar Muzakkar, peristiwa Andi Azis, DI-TII, peristiwa 3 Selatan, PRRI-Permesta.

Dalam berbagai peristiwa anti-Bung Karno dan anti-PKI itu sudah terlibat campur-tangan kepentingan  imperialisme (terutama Belanda, Inggris dan Amerika Serikat) yang bersekongkol dengan agen-agennya di dalam negeri, secara langsung atau tidak langsung. Campur-tangan kekuatan gelap Amerika Serikat yang langsung adalah ketika terjadi masalah PRRI/Permesta, yang kemudian disusul dengan bantuan -- dengan bermacam-macam cara --  kepada kudeta merangkaknya Suharto terhadap Bung Karno melalui peristiwa 30 September 1965.

Sekadar untuk menyegarkan ingatan kita bersama, patutlah disebutkan di sini bahwa dalam berbagai peristiwa anti Bung Karno dan anti-PKI di masa-masa lalu telah terjadi banyak penahanan atau pemenjaraan anggota-anggota atau simpatisan PKI (antara lain  dalam peristiwa 3 Selatan, yaitu di Sumatera Selatan, Kalimantan Selatan, dan Sulawesi Selatan). Pembunuhan dan pembunuhan massal terhadap orang-orang PKI (atau yang dianggap PKI) juga banyak terjadi selama berlangsungnya peristiwa PRRI/Permesta antara tahun 1958-1960.

Tetapi pembunuhan massal dan penahanan besar-besaran yang paling luar biasa hebatnya dalam sejarah Republik Indonesia adalah sesudah terjadinya peristiwa 30 September 1965. Sekitar 3 juta orang kiri (atau hanya diduga berhaluan kiri) dan menjadi anggota atau simpatisan PKI atau pendukung Bung Karno dibantai secara massal dan biadab oleh pasukan militer atau oleh berbagai kalangan  (terutama sebagian dari kalangan Islam yang anti-Bung Karno,  atas dorongan, pimpinan, atau petunjuk pimpinan militer).

Seharusnya mereka semuanya tidak bisa dipersalahkan

Walaupun terjadi pembunuhan sebegitu banyak orang, dan di berbagai tempat di seluruh negeri pula, namun selama 32 tahun rejim militer Suharto berkuasa tidak banyak yang diketahui tentang apa sebenarnya yang terjadi dan sampai di mana luasnya atau berapa besarnya pembunuhan massal itu.
Karena, semua orang takut bicara terang-terangan, walaupun tahu serba sedikit, tentang apa yang terjadi atas pembunuhan bapaknya, ibunya, anak-anaknya, atau saudara-saudaranya. Kebanyakan di antara mereka hanya berani bicara bisik-bisik kepada keluarga terdekat yang bisa dipercaya. Keadaan yang semacam itu banyak sekali yang berlangsung  selama puluhan tahun, sampai hari jatuhnya Suharto dan runtuhnya Orde Baru dalam tahun 1998.

Tidak dapat dibayangkan betapalah besarnya penderitaan keluarga yang punya saudara dibunuhi secara biadab seperti itu, sedangkan mereka tidak bersalah apa-apa kecuali menjadi anggota (atau simpatisan PKI) atau hanya menjadi pendukung Bung Karno. Dan sekalipun menjadi anggota PKI atau simpatisannya, mereka seharusnya tidak bisa dipersalahkan, karena waktu itu PKI adalah partai yang sah dan legal, punya wakil dalam parlemen dan pemerintahan, dan menjadi pendukung utama Presiden RI, Bung Karno, yang juga PBR (Pemimpin Besar Revolusi, menurut keputusan bulat MPRS).

Selama puluhan tahun korban tindakan Orde Baru ini  tidak bisa berbuat apa-apa, karena menghadapi kekuasaan militer yang terus-menerus melakukan  terror, persekusi dan intimidasi, yang hebat sekali intensitasnya. Mereka tidak bisa mengadu atau minta perlindungan dan pembelaan kepada siapapun. Dan kebanyakan orang pun tidak berani membela mereka.

Korban lainnya rejim militer Suharto adalah ratusan ribu tahanan politik golongan A, B, dan C, yang ditahan atau dipenjarakan atau dimasukkan kamp-kamp tahanan yang terdapat hampir di seluruh pulau-pulau yang penting, sampai bertahun-tahun, tanpa diadili, dan terbukti tidak bersalah apa-apa juga. Di antara tapl-tapol itu ada yang dibuang ke dalam tahanan di pulau Buru sampai kurang lebih 10 tahun. Mereka juga harus meninggalkan keluarga mereka, yang kebanyakan terpaksa hidup sulit sekali karena berbagai macam penderitaan.

Pelanggaran HAM yang jarang bandingannya di dunia

Sampai sekarang adalah sulit sekali diperkirakan jumlah anggota-anggota keluarga yang ditinggalkan para korban yang dibunuh, dan para tapol yang dipenjarakan berkepanjangan ini, yang terdiri dari istri atau suami, atau anak-anak dan saudara-saudara terdekat mereka, yang ikut dalam penderitaan.

Kalau diingat itu semua, maka nyatalah bahwa dosa-dosa Suharto bersama Orde Baru-nya akibat kejahatan-kejahatannya yang begitu besar dan tidak manusiawi itu merupakan pelanggaran HAM yang jarang bandingannya di dunia, kecuali yang dilakukan oleh Hitler. Namun, yang lebih biadab lagi adalah bahwa Suharto  -- beserta pendukung-pendukungnya dari berbagai golongan – telah melakukan macam-macam  kejahatan atau pelanggaran HAM besar-besaran itu terhadap bangsanya sendiri !

Dan lagi, kalau diingat bahwa jumlah orang yang dibunuh secara sewenang-wenang dan membabi-buta itu begitu banyak, maka dosa Suharto (dan pendukung-pendukungnya) adalah juga besar sekali. Sebab kalau membunuh satu orang yang tidak bersalah saja sudah harus dihukum lebih dari 10 tahun, maka membunuh atau menyuruh bunuh (atau harus bertanggung jawab atas pembunuhan ) jutaan orang tidak bersalah,  berapa besarkah hukuman yang seharusnya  dijatuhkan kepada Suharto atau pendukung-pendukung setianya.

(Dalam kaitan ini, baiklah kita ingat bersama bahwa Antasari Azhar dijatuhi hukuman 18 tahun penjara, karena dinyatakan oleh pengadilan telah membunuh Direktur PT Putra Rajawali, Nasrudin Zulkarnain. Juga bahwa Tommy Suharto divonnis 15 tahun penjara karena terlibat dalam pembunuhan hakim agung Syafiudin Kartasasmita)

Jutaan orang dibunuh, namun tidak seorangpun dikenakan hukuman

Kita sama-sama tahu bahwa meskipun sudah ada jutaan orang yang sama sekali tidak bersalah apa-apa  telah dibunuh secara sewenang-wenang dan biadab pula, namun tidak seorang pun yang dituntut  atau dikenakan hukuman. Inilah yang keterlaluan luar biasanya !!! Seolah-olah pembunuhan begitu banyak orang itu dianggap tidak ada saja, atau dianggap soal remeh yang bisa « dicuekin » saja. Sungguh keterlaluan, sungguh ! Satu ayam, atau satu kambing, atau satu sapi mati saja ada artinya, apalagi kalau jumlahnya jutaan.  Apalagi, sekali lagi apalagi ( !), yang mati (dibunuh) adalah jutaan manusia-manusia yang tidak bersalah apa-apa sama sekali. Marilah sama-sama kita renungkan dalam-dalam masalah yang begini besar dan menyedihkan ini.

Dosa-dosa yang sudah berat dari pembunuhan massal yang berskala besar di seluruh negeri itu ditambah lebih berat lagi dengan pemenjaraan ratusan ribu orang lainnya (yang sama sekali juga tidak bersalah apa-apa !!!) dengan sewenang-wenang, dan dalam jangka lama sekali pula. Mereka semua itu juga dipaksa harus meninggalkan keluarga mereka dalam penderitaan yang berkepanjangan.

Pendidikan politik dan pendidikan moral untuk bangsa

Sebagian kecil dari bermacam-macam kisah tentang anggota-anggota keluarga yang ditinggalkan oleh mereka yang dibunuh dan dipenjarakan sebagai tapol itu sudah banyak ditulis sebagai artikel dalam majalah-majalah atau sebagai buku, atau ada juga yang dibikin film, walaupun tidak banyak. Karena itulah, maka sedikit demi sedikit, dan hanya sebagian kecil saja dari masalah korban tindakan penguasa militer Suharto itu mulai diketahui oleh masyarakat.

Tetapi, masih banyak sekali ( !) masalah yang sebenarnya tentang  korban rejim militer Orde Baru yang belum diketahui oleh rakyat kita, karena belum bisa terungkap akibat berbagai sebab. Di samping sisa-sisa Orde Baru masih berusaha untuk menghalanginya, juga masih banyak orang yang takut atau enggan berbicara terus terang. Dan sebagian di antara mereka yang memilih diam saja, dan membiarkan apa yang sudah terjadi mereka  tanggung sendiri saja, untuk tidak menimbulkan persoalan atau kesulitan.

Padahal, segala penderitaan yang diakibatkan oleh berbagai kejahatan penguasa militer dan Orde Baru terhadap para korban beserta keluarga (dan saudara-saudara mereka itu ) perlu sekali diketahui sebanyak-banyaknya dan sejelas-jelasnya oleh rakyat kita. Ini untuk kepentingan sejarah  bangsa kita dan anak cucu kita di kemudian hari. Juga untuk pendidikan politik dan pendidikan moral bagi generasi muda dewasa ini serta generasi-generasi yang akan datang, supaya pengalaman bangsa yang begitu menyedihkan dan begitu tidak manusiawi itu tidak akan terulang lagi untuk kedua kalinya.

Karena itu, para korban kejahatan rejim militer Suharto itu patut berusaha terus, dengan macam-macam jalan dan cara, untuk selalu mengangkat masalah besar bangsa ini setiap ada kesempatan atau kemungkinan. Kegiatan semacam itu tidaklah sekadar karena dendam dan membalas sakit hati, walaupun jutaan orang sudah dibunuhi secara biadab dan ratusan ribu lainnya dipenjarakan sebagai tapol. Kegiatan semacam itu perlu sekali dilakukan, sesering mungkin atau sebanyak mungkin,  demi dihargainya rasa keadilan oleh seluruh bangsa, termasuk mereka yang tadinya pernah mendukung Suharto beserta Orde Barunya.

Para korban rejim Orde Baru berhak menuntut keadilan

Mengingat itu semuanya, patutlah kiranya sama-sama kita hargai  -- dan kita dukung -- segala usaha atau kegiatan yang dilakukan oleh berbagai kalangan dan golongan (umpamanya, antara lain : oleh LPR-KROB, YPKP, Pakorba, Komnas HAM) untuk selalu menyegarkan ingatan bangsa kita kepada tragedi besar yang telah menimbulkan begitu banyak korban. Apa yang mereka lakukan itu sangatlah perlu dan juga mulia bagi keseluruhan bangsa, termasuk bagi anak cucu kita di kemudian hari.

Para korban rejim militer Suharto yang begitu banyak itu -- terutama sekali yang dari golongan kiri dan pendukung politik Bung Karno --  sudah terlalu lama ( kebanyakan sudah sekitar 45 tahun), mendapat berbagai perlakuan yang melanggar HAM dari penguasa (dan juga dari sebagian masyarakat). Mereka itu berhak menuntut dan berjuang bersama-sama untuk memperoleh hak mereka sebagai warganegara yang lain atau direhabilitasi sepenuhnya.

Mereka juga berhak sepenuhnya  – dan sudah seharusnya pula  – untuk menuntut diperbaikinya kesalahan-kesalahan yang begitu besar dan ditebusnya dosa-dosa berat yang sudah dilakukan begitu lama oleh Orde Baru beserta para penerusnya. Diperbaikinya kesalahan dan ditebusnya dosa-dosa terhadap para korban itu akan mendatangkan kebaikan bagi kehidupan bangsa, yang sampai sekarang masih tercabik-cabik, atau tersayat-sayat, sehingga melukai Pancasila dan merusak Bhinneka Tunggal Ika.

Tidak ada kebaikannya  memperpanjang penderitaan para korban

Penyelesaian secara tuntas dan integral masalah korban rejim militer Orde Baru ini akan merupakan salah satu dari langkah-langkah penting menuju persatuan bangsa, yang sejak pemerintahan Orde Baru dirusak atau dibusukkan oleh berbagai masalah besar lainnya. Sebaliknya, kalau masalah para korban Orde Baru ini tetap tidak  ditangani secara baik, maka akan tetap terus menjadi duri dalam daging bangsa, yang sekarang sudah dalam keadaan sakit parah ini.

Jelaslah kiranya bagi kita semua, bahwa bangsa dan negara kita tidak akan mendapat keuntungan apa-apa sama sekali dengan adanya berbagai penderitaan para korban rejim militer, yang masih terus berlangsung puluhan tahun, sampai sekarang ! Demikian juga bagi para pendukung Suharto dan Orde Barunya ( atau sisa-sisanya dan penerusnya sekarang), tidak ada kebaikannya sama sekali untuk melanjutkan   -- terus-menerus --  kesalahan dan dosa besar yang sudah berlangsung berkepanjangan itu.

Sebaliknya, masih terkatung-katungnya nasib para korban begitu lama itu hanya menunjukkan bahwa para penguasa, yang bertanggung jawab di masa yang lalu dan sekarang, adalah orang-orang yang sama sekali tidak mempunyai rasa kemanusiaan, yang rusak moralnya, yang sesat imannya, dan yang menyalahi perintah-perintah agama atau Tuhan. Orang-orang yang macam inilah yang juga membikin negara kita penuh dengan koruptor, orang-orang munafik, yang suka bersekongkol dengan kaum reaksioner di dalam negeri dan luar negeri, termasuk dengan kekuatan neo-liberalisme.

Dalam makamnya di Blitar, pastilah  Bung Karno marah sebesar-besarnya melihat begitu banyak orang kiri pendukungnya dibunuhi secara besar-besaran dengan cara-cara yang sama sekali bertentangan dengan kebiasaan dan kepribadian bangsa Indonesia, dan dengan Pancasila serta Bhinneka Tunggal Ika.

Keadilan bisa ditegakkan dengan  Meneruskan Revolusi Rakyat

Oleh karena itu,  maka para korban rejim militer Orde Baru, berhak sepenuhnya berjuang terus untuk ditegakkannya keadilan bagi mereka dan juga bagi bangsa seluruhnya. Namun, sekali lagi namun, kita semua tahu bahwa keadilan bagi para korban rejim militer Suharto tidak mungkin ditegakkan selama kekuasaan masih terus ada di tangan kaum reaksioner yang anti ajaran-ajaran revolusioner Bung Karno atau anti-kiri pada umumnya. Sebaiknya, ilusi bahwa kekuasaan reaksioner bisa menegakkan keadilan bagi bangsa haruslah dibuang jauh-jauh saja.

Keadilan bagi semua golongan, termasuk para korban rejim Suharto barulah bisa ditegakkan di negeri kita hanya melalui perubahan-perubahan besar dan fundamental. Dan untuk mewujudkan perubahan-perubahan besar dan fundamental itu, jalan yang terbaik atau cara yang paling tepat adalah jalan Meneruskan Revolusi Rakyat, yang sudah ditunjukkan oleh Bung Karno berulangkali melalui ajaran-ajaran revolusionernya
