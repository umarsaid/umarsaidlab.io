---
layout: post
title: Negara Dalam Bahaya dan Menuju Jurang Kehancuran
date: 2011-08-10
---

Menjelang seluruh bangsa kita merayakan Hari Kemerdekaan 17 Agustus, ada peristiwa penting yang patut  mendapat perhatian kita semua. Peristiwa ini adalah pertemuan 45 tokoh nasional, yang menghasilkan sikap bersama mengenai keadaan negara dan bangsa dewasa ini.  Pertemuan ini diadakan dengan pokok pembicaraan  Revitalisasi Semangat Proklamasi Kemerdekaan Dan Akhlak Kepemimpinan.

Seperti yang diberitakan oleh pers, yang dikutip di bawah ini, sikap 45 tokoh nasional  itu (yang tidak mewakili partai-partai politik)  mengambil sikap yang sangat tegas dan keras terhadap SBY beserta pemerintahannya yang telah dianggap gagal.

Dengan bahasa yang terdengar sangat  keras atau terus terang  mereka menyerukan agar « DPR mengambil langkah politik untuk segera mengakhiri kekuasaan yang hanya menyandera rakyat. SBY sudah  terbukti tidak mampu dan secara moral sudah tidak patut untuk menyelenggarakan negara dan kekuasaan pemerintahan »

Dengan tertangkapnya Nazarrudin di Cartagena (Colombia) dan adanya kemungkinan untuk dipulangkanya ke Indonesia, maka pemerintah di bawah pimpinan SBY mendapat ujian baru lagi tentang kredibilitas dalam mengurusi masalah hukum dan korupsi. Kelanjutan kasus Nazaruddin dan segala kasus lainnya yang menyangkut tokoh-tokoh Partai Demokrat akan menunjukkan lebih banyak lagi tentang kebobrokan pemerintahan SBY.

Untuk membantu para pembaca dapat mengetahui betapa penting pernyataan sikap 45 tokoh dalam konteks situasi dewasa ini maka di bawah berikut ini disajikan berita yang disiarkan Rimanews (8 Agustus 2011), yang  adalah sebagai berikut  (dengan huruf tebal di sana-sini, sebagai tanda penggaris-bawahan) :

SBY-Boediono Gagal, Harus Tahu Diri dan Mundur

« Sedikitnya 45 tokoh nasional berkumpul di Hotel Four Season, Jakarta Senin malam (8/9). Dengan tegas mereka menyatakan sikap jika pemerintahan SBY-Boediono telah melenceng dari tujuan dan cita-cita kemerdekaan, SBY-Boed gagal dan harus tahu diri serta secepatnya mundur !.


Hariman Siregar menyatakan pernyataan sikap dalam forum ini menunjukkan sudah ada kesamaan penilaian, pandangan dan visi bahwa pemerintahan SBY-Boediono sudah tak layak dipertahankan karena gagal, tak amanah, dan  negara dalam keadaan bahaya. ''Indonesia dalam keadaan kritis, dalam bahaya, kita harus terpanggil untuk mewakafkan diri kita demi keselamatan bangsa  dan negara,'' tegas Tokoh Malari dan mantgan Ketua Umum Dewan Mahasiswa UI  ini.


Diantaranya ke 45 tokoh yang hadir dan berbicara antara lain,  Prof KH Ali Yafie, Hariman Siregar, Sukardi Rinakit, Soeryadi Sudirja, Adnan Buyung Nasution, Soegeng Sarjadi, Letjen Marinir (Purn) Suharto,  Sri Palupi, Monsinyer Situmorang, Fanny Habibie, Bursah Zarnubi, Gurmilang Kartasasmita, Tyuk Sukadi, B Wiwoho,Mayjen TNI (Purn) Purwanto, Mulyana W Kusuma dan Tyasno Sudarso.


Ke 45 tokoh tersebut sepakat saat ini sudah terjadi penyimpangan terhadap cita-cita dan semangat proklamasi kemerdekaan. Kehidupan bernegara dan berbangsa, kata mereka telah mengarah ke jurang kehancuran.


Cita-cita proklamasi 1945 dengan tegas menyatakan bahwa tujuan bernegara adalah untuk melindungi segenap bangsa Indonesia dan seluruh tumpah darah Indonesia, meningkatkan kesejahteraan umum, mencerdaskan kehidupan bangsa dan ikut serta dalam pergaulan dunia yang berdasarkan kemerdekaan, perdamaian abadi dan keadilan sosial. Namun pada kenyataannya, saat ini semakin benar-benar sudah jauh dari yang dicita-citakan para founding fathers.


Secara objektif kondisi bangsa saat ini semakin memburuk bahkan mengarah pada kehancuran. Kerusakan telah terjadi di semua aspek dan lini kehidupan.


Setidaknya ada 7 krisis nasional yang melanda. Yakni krisis kewibawaan kepala pemerintahan, krisis kewibawaan kepala negara, krisis kepercayaan terhadap parpol, krisis kepercayaan kepada parlemen, krisis efektifitas hukum, krisis kedaulatan sumber daya alam, krisis kedaulatan pangan, krisis pendidikan, krisis integrasi nasional.


Dalam hemat ke 45 tokoh, semua itu terjadi karena pemerintahan presiden Susilo Bambang Yudhoyono(SBY) tidak efektif, lemah, dan hanya mengejar pencitraan diri ketimbang kerja nyata. Terjadi disorientasi dari SBY selaku presiden. Rakyat sekarang ini bertahan hidup karena usaha mereka sendiri dan bukan karena peran negara. Negara tidak hadir ketika rakyat membutuhkan.


Terlalu beresiko bagi bangsa jika situasi seperti ini terus berlangsung. Rakyat akan semakin pesimis dan terpuruk. Apabila presiden SBY tidak bisa menghentikan demoralisasi dan anomali kehidupan berbangsa dan bernegara maka keharusan konstitusional bahwa SBY harus diganti pada 2014 otomatis gugur. Bangsa dan negara harus diselamatkan saat ini juga.
Saat ini perubahan merupakan conditio qua non, semakin cepat semakin baik agar ongkos politik, biaya ekonomi, dan resiko sosial-budaya bisa berkurang.


"Kami menegaskan agar DPR mengambil langkah politik untuk segera mengakhiri kekuasaan yang hanya menyandera rakyat. Kepemimpinan SBY sudah terbukti tidak mampu dan secara moral sudah tidak patut untuk menyelenggarakan negara dan kekuasaan pemerintahan," demikian salah satu poin desakan yang disampaikan ke 45 tokoh nasional. Advokat tiga zaman, Adnan Buyung Nasution, adalah salah seorang tokoh nasional yang hadir dalam pertemuan 45 tokoh di Hotel Four Seasons, Jakarta, malam ini (Senin 8/8).


Bang Buyung mengungkapkan bagaimana perjuangan menuju kemerdekaan telah dilakukan dengan susah payah. Tapi, kemerdekaan itu dikhianati oleh pemerintahan saat ini.
"Saya dukung sepenuhnya wacana ini (mengakhiri pemerintahan), pemerintah sudah tidak melihat dan mendengar," ujarnya diikuti tepuk tangan dan gema takbir.


Bahkan lebih dari itu, menurut Buyung pemerintah SBY-Boediono tidak lagi mempunyai hati nurani.
"Saya sudah capek memperingati SBY ini dari dalam dan luar. Kita tidak bisa membiarkan rakyat seperti ini. Kita setuju harus segera melakukan aksi nyata," tegasnya.


"Kita juga lakukan seruan kepada DPR untuk mengambil sikap tegas. Kalau tidak rakyat akan bubarkan mereka. Saya harap SBY tahu diri harus mundur," pinta Buyung.
Keprihatinan terhadap rendahnya kinerja pemerintahan Presiden Susilo Bambang Yudhoyono ditunjukkan oleh 45 tokoh bangsa dalam rapat revitalisasi cita-cita dan semangat proklamasi kemerdekaan di Jakarta, Senin (8/8). Para tokoh menilai berbagai penyimpangan di Indonesia saat ini sudah sangat memprihatinkan.

Salah satu tokoh Soegeng Sarjadi merasakan bahwa fungsi kenegaraan tidak berjalan efektif. Partai politik dan pemerintah pun tidak bekerja baik. Para tokoh pun akhirnya merasa terpanggil untuk membahas persoalan negara.
Ketua Soegeng Sarjadi Syndicate itu mengatakan pesan dari pertemuan 45 tokoh ini seharusnya menjadi pegangan pemerintah dan DPR. "Jika pesan para tokoh ini tidak didengarkan dan rakyat yang berbicara itu justru lebih berbahaya," ujarnya.
Pertemuan ini untuk membuka mata hati pemimpin negara agar dapat lebih bijak lagi. Karena masih banyak korupsi dan kebijakan-kebijakan yang masih belum sesuai »(kutipan dari RIMA selesai)



 Sedikit komentar :

Dari apa yang sudah diungkap  oleh 45 tokoh nasional dalam pernyataan di atas nyatalah bahwa banyak hal mencerminkan perasaan dan fikiran banyak kalangan dalam masyarakat dewasa ini. Kecuali ( tentu saja !!!) kalangan partai-partai politik pendukung koalisi SBY dan para pendukung pemerintahan, yang pada umumnya tidak peduli dengan nasib rakyat dan situasi negara yang sudah rusak.

Kemarahan 45 tokoh nasional, seperti yang dengan jelas dituangkan dalam pernyataan  « keras » tersebut di atas  pada hakekatnya adalah  juga kemarahan « kalangan bawah », yang terdiri dari berbagai organisasi, buruh, tani, nelayan, perempuan, pemuda, mahasiswa, intelektual, seniman, pengusaha kecil dan kalangan lainnya.

Puncak kemarahan mereka adalah ketika mereka menyatakan bahwa SBY telah melenceng dari tujuan dan cita-cita kemerdekaan, serta gagal dalam banyak hal dan harus tahu diri serta secepatnya mundur !. Bahkan mereka menyatakan bahwa keharusan konstitusional bahwa SBY harus diganti pada 2014 otomatis gugur. Bangsa dan negara harus diselamatkan saat ini juga.

Mereka juga menyatakan dengan tegas bahwa « Kepemimpinan SBY sudah terbukti tidak mampu dan secara moral sudah tidak patut untuk menyelenggarakan negara dan kekuasaan pemerintahan,"

Pertemuan 45 tokoh nasional yang mengeluarkan pernyataan keras seperti tersebut diatas, ditambah dengan makin maraknya berbagai gejolak sosial dan politik, merupakan perkembangan yang mengindikasikan bahwa kebutuhan akan adanya perubahan besar-besaran yang mendasar, yang fundamental, yang drastis, makin terasa mendesak sekali.

Namun, pengalaman sudah menunjukkan dengan jelas sekali, bahwa perubahan besar-besaran secara mendasar ini tidak mungkin dilaksanakan dengan tetap memakai cara-cara yang telah ditempuh  selama ini (sejak Orde Baru sampai sekarang).

Kerusakan atau kebobrokan di banyak bidang sudah begitu parah dan begitu luas, sehingga tidak mungkin lagi diperbaiki oleh siapa pun dan oleh pemerintahan yang bagaimanapun (sampai kapan pun !!!) yang masih belum membersihkan diri dari sisa-sisa buruk Orde Baru beserta penerus-penerusnya.

Oleh karena itu jiwa atau isi pernyataan pertemuan 45 tokoh yang tegas, berani, dan mencerminkan perasaan dan fikiran banyak kalangan masyarakat itu perlu diperkuat dengan dukungan atau partisipasi kongkrit dari banyak kalangan kekuatan demokratis pro-rakyat di Indonesia. Semua itu merupakan sumbangan atau bagian dari usaha membangun kekuatan politik alternatif, untuk menghadapi partai-partai politik tradisional yang sudah kehilangan kepercayaan rakyat.

Karena, kiranya sudah dapat diperkirakan bahwa DPR  (yang didominasi oleh Partai Demokrat beserta koalisi pendukungnya) tidak akan bisa   -- dan tidak mau !!! --  mengakhiri kekuasaan presiden SBY.

Hanya kekuatan politik alternatif rakyat, yang pro-rakyat, yang anti-neo liberalisme, yang dibangun bersama-sama secara luas, akan bisa mengadakan perubahan-perubahan besar dan fundamental.

Perubahan besar-besaran dan fundamental akan memungkinkan negara kita diselamatkan dari segala macam penyakit besar dan parah yang merusak moral banyak orang, sejenis dalam kasus Nazaruddin, Anas Urbaningrum, Nunun, Andi Nurpati, atau kasus-kasus besar yang selama ini membikin kotor bidang eksekutif, legislatif dan judikatif.

Pemerintahan SBY, yang sudah berjalan  7 tahun, tidak akan bisa menunaikan tugas-tugas revolusi 17 Agustus 45, menjalankan UUD 45 (terutama pasal 33), mengusahakan terciptanya masyarakat adil dan makmur, seperti yang dicita-citakan oleh bangsa dengan pimpinan revolusioner Bung Karno.

Bangsa dan negara Indonesia memerlukan   -- dan dengan mendesak sekali ! – adanya pimpinan  yang baru. Kiranya, inilah arah perkembangan untuk selanjutnya.
