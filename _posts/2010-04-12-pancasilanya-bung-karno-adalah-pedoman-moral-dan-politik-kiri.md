---
layout: post
title: Pancasilanya Bung Karno adalah pedoman moral dan politik kiri
date: 2010-04-12
---

Berkaitan dengan keputusan Kongres ke-7 PRD bersejarah beberapa waktu yang lalu,  yang mencerminkan perubahan besar selama 14 tahun dalam kehidupannya,  berikut di bawah ini disajikan sebuah tulisan Gede Sandra, sekjen baru Komite Pimpinan Pusat PRD.  Dalam tulisan ini ia jelaskan mengapa PRD menganggap perlu sekali mengangkat kembali ke tempatnya yang sesungguhnya (dan seharusnya !!!), yaitu Pancasila yang kiri seperti yang digagas oleh penggagagasnya atau penciptanya, yaitu Bung Karno.  



Dari tulisan ini kelihatan makin jelas sekali bagi kita semua, bahwa Pancasila yang digagas oleh Bung Karno adalah sama sekali berbeda, atau berlawanan dan bertolak belakang dengan apa yang sudah dijajakan (dicekokkan), dengan paksa pula, oleh rezim Orde Baru beserta pendukung-pendukungnya selama lebih dari 32 tahun.



Suharto beserta para pendukungnya (militer atau sipil, terutama Golkar) sudah menggunakan (lebih tepatnya : menyalahgunakan) Pancasila yang palsu atau sudah divermaak atau dikebiri untuk membunuh ajaran-ajaran revolusioner Bung Karno, termasuk Pancasila yang asli, yaitu Pancasilanya Bung Karno. -Pancasila"nya Suharto adalah sebenarnya pengkhianatan terhadap Pancasilanya Bung Karno.



Inti atau isi Pancasilanya Bung Karno adalah berjiwa kiri atau revolusioner, sedangkan "Pancasila"-nya Suharto adalah reaksioner atau kontra-revolusioner. Oleh karena itu, sekarang ini semua atau seluruh kekuatan demokratis di Indonesia, yang sedang berjuang untuk membrantas kebejatan moral dan pembusukan politik (contohnya yang terakhir: kasus Anggoro-Anggodo, Artalita, Bank Century, Gayus Tambunan, soal pajak Aburizal Bakrie dan seabreg-abreg kasus korupsi di daerah-daerah) perlu mengangkat kembali Pancasilanya Bung Karno sebagai senjata untuk meneruskan revolusi, guna mengadakan perubahan-perubahan besar dan fundamental bagi kepentingan rakyat banyak.



Selama hampir setengah abad, sejak 1965,  makin  jelas bagi banyak orang bahwa dirusaknya, atau dibusukkannya Pancasila oleh Suharto beserta para pendukungnya telah membikin rakyat Indonesia kehilangan pedoman moral dan bimbingan politik yang benar-benar pro-rakyat, pro-kemanusiaan, yang revolusioner, yang  sungguh-sungguh anti-imperialisme.



Pancasilanya Bung Karno, yang tercermin  juga dalam jiwa Konferensi Bandung adalah gagasan besar yang sekarang ini (dan juga dalam jangka lama di masa datang !!!) masih relevan untuk dipakai oleh seluruh kekuatan demokratik di Indonesia (jadi bukan hanya oleh PRD) sebagai senjata untuk menghadapi berbagai masalah besar rakyat. Gagasan atau karya-karya besar Bung Karno seperti yang tercermin dalam Bhinneka Tunggal Ika, Pancasila, Trisakti, Konferensi Bandung, Go to hell with your aid, Ganefo dll adalah  pedoman moral dan politik (moral and political guidance) yang masih sangat diperlukan bagi perjuangan rakyat Indonesia dewasa ini, dan juga di masa datang.



Sebab, sejak digulingkannya kekuasaan politik Bung Karno oleh Suharto (beserta pendukung-pendukungnya ) berbagai pemerintahan dan tokoh politik sudah silih berganti, tetapi hasilnya adalah seperti yang sama-sama kita saksikan dengan gamblang dewasa ini : serba rusak,  serba bejat, serba semrawut dan serba busuk, terutama di kalangan atas atau kalangan elite.



A. Umar Said



12 April 2010





* * *   * * *



Berikut di bawah ini adalah tulisan  Gede Sandra, sekjen PRD yang baru :







Pancasila, dari Trauma ke Persatuan Anti Neolib





Oleh: Gede Sandra



Sekretaris Jenderal Komite Pimpinan Pusat- Partai Rakyat Demokratik (PRD)



 De-Sukarnoisasi ala Orde Baru

"Lahirnya Pantjasila, Membangun Dunia Kembali, dan Penfjelasan Manipol/Usdek adalah merupakan rangkaian jang sangkut bersangkut untuk harus kita ketahui dalam alam sekarang ini jaitu untuk menudju masjarakat Sosialis Indonesia."

(Kutipan dari Buku "Seri Tanja-Djawab Buku2 Manipol: 260 Tanja Djawab Lahirnja Pantjasila, Membangun Dunia Kembali (to build the world new), Pendjelasan Manipol-Usdek". Penerbit Miswar Djakarta, tjetakan pertama, 1965" hal. 7)

Banyak kamerad di kalangan kaum gerakan dan intelektual yang cukup kaget saat mengetahui Pancasila coba diangkat Partai Rakyat Demokratik (PRD) dari "lumpur kenistaan" ke tempat yang lebih bersih, terang dan tinggi. Sebagiannya malah keblinger, menuduh PRD sudah bergeser ke sebelah kanan, PRD sudah ditunggangi intelejen negara, PRD akan menjadi organisasi reaksioner, PRD akan melakukan penataran P4 seperti layaknya di zaman kegelapan dahulu, dan lain-lain dan sebagainya. Namun, semua itu justru menyiratkan pendeknya ingatan kolektif rakyat kita tentang Pancasila. Sangat disayangkan hanya Pancasila yang "kanan", Pancasila yang "pembunuh", dan Pancasila yang "bengis" a'la Jenderal Suharto yang masih diingat, bukan Pancasila yang sesuai keinginan sang penggali Ir. Sukarno: Pancasila yang kiri.

Stigma kanan Pancasila diperoleh dari hasil pendistorsian panjang Orde Baru. Karena banyak aktivis dan intelektual oposan kerap menjadi "korban" represi/intimidasi dari kelompok-kelompok yang mengklaim membela Pancasila, maka adalah wajar jika rasa trauma terhadap segala yang berbau Pancasila atau asas tunggal masih agak lekat. Mimpi buruk selama Orde Baru ini belum ditinggal, masih dibawa-bawa sehingga menjadi beban psikologis (semacam mental blocking) sampai sekarang.

Bukan salah kaum muda yang lahir di zaman Orde Baru jika mereka tak mampu mengingat Pancasila-nya Ir. Sukarno. Yang salah tetap adalah Jenderal Suharto yang telah mendistorsi esensi Pancasila selama 32 tahun (1965-1998). Selama itu pula Pancasila tampil dalam raut wajah yang bengis dan kejam. Namun, seperti kita menghadapi penyakit-penyakit mental umumnya, trauma yang diderita akibat Pancasila Orde Baru tidak boleh dipersalahkan, apalagi dihakimi, yang seharusnya dilakukan adalah mengobatinya (healing).

Jika diurai, sebenarnya trauma terhadap Pancasila berakar pada dua hal sbb:

1. Devide et impera antara Pancasila dengan Kaum Kiri

Pada pertengahan tahun 1965-1967, selagi Jenderal Suharto tengah melakukan "kudeta merangkak" (creeping coup, pen: baca buku terbaru John Roosa tentang Dalih Pembunuhan Massal), ia menyempatkan menetapkan tanggal 1 Oktober 1965 sebagai Hari Kesaktian Pancasila melalui Surat Keputusan Menteri/Panglima Angkatan Darat tanggal 17 September 1966 (Kep 977/9/1966). Narasi tunggal yang hendak didesainnya saat itu adalah seakan-akan Pancasila sebagai dasar negara berusaha diserang oleh kaum kiri, dan ia adalah satu-satunya pahlawan penyelamat Pancasila. Dengan dalih penyelamatan tersebutlah, Jenderal Suharto seperti mendapat restu untuk membunuh secara massal ratusan ribu sampai jutaan kaum kiri di Indonesia hanya dalam waktu 3 tahun. Kesuksesan adu domba tersebut diteruskan untuk selama masa 32 tahun berikutnya, di mana Pancasila selalu dijadikan momok untuk membungkam sisa-sisa kaum kiri (yang selamat dari pembunuhan massal atau bebas dari penjara/kamp kerja paksa) ataupun kaum kiri baru yang lahir di era 1980an (seperti PRD). Maka dari itu, kita boleh saja untuk ke depannya membulatkan tekad, mengusulkan untuk pergantian nama tanggal 1 Oktober dari Hari Kesaktian Pancasila menjadi Hari Kesaktian Orde Baru.

2. Pembohongan Asal Usul Pancasila

Sebuah kebohongan pun jika dipropagandakan ribuan kali dapat bermutasi menjadi sebuah kebenaran publik. Itu adalah teknik dari Menteri Propaganda Nazi Jerman Goebbels yang biasa disebut "Big Lie", yang ternyata dipraktekkan dengan sangat baik oleh Jenderal Suharto di Indonesia selama 32 tahun pemerintahannya. Kebohongan/distorsi terutama dari Jenderal Suharto adalah tentang tanggal lahir dan penggali sejati Pancasila, yang dipropagandakan intensif melalui Pelajaran Sejarah dan Perjuangan Bangsa (PSPB) dan penataran-penataran Pedoman Penghayatan dan Pengamalan Pancasila (P-4). Orde Baru menyebut, bahwa tanggal 18 Agustus 1945 adalah hari lahir Pancasila dan Mr. Mohammad Yamin adalah penggalinya. Padahal yang sebenarnya adalah Pancasila lahir pada tanggal 1 Juni 1945 dan Ir. Sukarno adalah penggalinya. Jelas sekali ini adalah upaya de-Sukarnoisasi yang "sistemik". Meski kemudian kebohongan ini sempat dibantah oleh testamen Dr. Mohammad Hatta pada tahun 1976 , Orde Baru tetap tidak bergeming hingga Reformasi menggulingkannya. Baru setelah itu penataran P-4 dibubarkan, PSPB dicabut, dsb.

Dapat dirangkai dari kedua akar di atas, bahwa trauma terhadap Pancasila sejatinya disebabkan oleh mega proyek de-Sukarnoisasi Orde Baru. Taktik Orde Baru ini pun bukan tanpa alasan yang strategis, mengingat betapa kuatnya persatuan antara kaum kiri bersama Ir. Sukarno menjelang 1965. Pada masa itu kaum kiri adalah pendukung sejati Ir. Sukarno, karenanya untuk melumpuhkan politik Ir. Sukarno, kaum kiri harus dipisahkan darinya dan dihabisi terdahulu. Barulah Ir. Sukarno menjadi lemah dan mudah digulingkan, setelah terlebih dahulu diisolasi dari rakyat yang menjadi energi perjuangannya sejak muda.

Akhirnya sejarah mencatat, bahwa Pancasila yang awalnya dilahirkan Ir. Sukarno untuk memerangi imperialisme dan kolonialisme, berdistorsi menjadi alat yang digunakan Jenderal Suharto untuk tanpa keadilan mempersilahkan datangnya penjajahan gaya baru selama 32 tahun dan tanpa kemanusiaan menghabisi lawan-lawan politiknya.

Pancasila untuk Persatuan Anti Neoliberal

"Lima Sila ini kalau disatukan menjadi kepal akan menjadi tinju untuk menunju imperialis, lawan-lawan bejat, lawan-lawan kemerdekaan, penjajah yang menjajah Indonesia. Ini kepal rakyat Indonesia yang bersatu!"

(Cuplikan pidato Soemarsono, pimpinan Pemuda Republik Indonesia dan kader Partai Komunis Indonesia (PKI) ilegal pada tanggal 21 September 1945 di tengah RAPAT SAMUDERA yang dihadiri 150 ribu massa Marhaen di Stadion Tambaksari. Dikutip dari Buku Revolusi Agustus, Kesaksian Seorang Pelaku Sejarah, penerbit Hasta Mitra, 2008, hal. 37)

Obat dari trauma rakyat terhadap Pancasila hanyalah pembangkitan kembali ingatan kolektif perjuangan rakyat merebut (kembali) kedaulatan nasional sepanjang periode 1945-1965. Ingatan suram tentang Pancasila yang "reaksioner" di masa Orde Baru harus ditinggalkan, sedangkan ingatan tentang Pancasila yang "revolusioner" di masa Ir. Sukarno harus terus menerus digali kembali.

Pancasila yang akan kita emban bukanlah Pancasila-nya Orde Baru yang mengizinkan ExxonMobil, Freeport, Chevron, Inco, Vico, BHP Billiton, Thiess, ConocoPhilips, GoodYear, Total, Newmont, dll menjajah kekayaan alam bangsa. Pancasila yang kita ingin munculkan kembali adalah yang digunakan oleh Ir. Sukarno sebagai "pembenaran" untuk menasionalisasi perusahaan-perusahaan Belanda, mengusir modal asing yang menghisap jauh-jauh dari bumi Indonesia (dengan atau tanpa ganti rugi). Jika dikontekstualisasi ke zaman ini mungkin tidak akan jauh berbeda: bukan Pancasila yang mengabdi kepada rezim neoliberalisme, tetapi Pancasila yang mendamba akan kedaulatan nasional sepenuhnya demi keadilan sosial seluas-luasnya menuju sosialisme Indonesia.

Dengan perkataan lain, Pancasila yang akan kita amalkan harus memiliki semangat anti penjajahan, anti penghisapan manusia atas manusia ataupun penghisapan bangsa atas bangsa, semangat pembebasan nasional menuju cita-cita sosialisme Indonesia seperti digariskan oleh Pembukaan UUD 1945.


Dengan berpegangan pada Pembukaan UUD 1945, kita dapat merangkum setiap esensi dari perjuangan anti neoliberal di semua sektor rakyat ke dalam butir-butir Pancasila. Dari sari-sari perjuangan kita di lapangan perburuhan, kaum miskin perkotaan, petani, hingga mahasiswa-pelajar dan kebudayaan dapat kita sempalkan semua ke dalam Pancasila. Dan masing-masing sektor dapat saling mendukung perjuangan di sektor lainnya secara bahu membahu, holobis kuntul baris. Inilah watak sejati Pancasila yang lebih dikenal sejak masa nenek moyang kita dengan istilah gotong royong (1) .

Semisal dalam soal outsourcing dan sistem kerja kontrak, kita akan bilang bahwa konsep labour flexibility milik neoliberal tersebut bertentangan dengan pengamalan Sila ke 2 Kemanusiaan yang Adil dan Beradab sekaligus juga Sila ke 5 Keadilan Sosial Bagi Seluruh Rakyat Indonesia. Atau semisal juga tentang maraknya penggusuran kampung kumuh di perkotaan yang mengekspresikan program City Without Slump milik neoliberal, kita juga dapat bilang itu melanggar sila ke 2 dan ke 5. Artinya, sepanjang Menakertrans masih "mengamalkan" sistem outsourcing dan kontrak yang menghisap kaum buruh se-Indonesia; atau sepanjang Gubernur DKI Jakarta masih rajin "mengamalkan" penggusuran terhadap anak jalanan dan perkampungan kumuh, sepanjang itu jualah mereka dengan sengaja telah melanggar Pancasila sila ke 2 dan ke 5 sekaligus. Menjadi sah kemudian jika kita jatuhkan tudingan bahwa Muhaimin Iskandar adalah musuh Pancasila, karena masih membiarkan kaum buruh Indonesia bergelimang dalam perbudakan modern; dan Fauzi Bowo juga adalah musuh Pancasila, karena tidak pernah melindungi hak hidup warga miskin Jakarta. Gampangnya, kedua oknum pejabat tersebut adalah musuh Pancasila, maka mereka adalah musuh bersama kaum buruh dan kaum miskin kota.

Kesimpulan akhir: neoliberal dan semua operatornya adalah musuh Pancasila; dan semua kaum yang dirugikan (oleh neoliberal) wajib bergotong royong bersatu dalam aksi menentangnya.

****
(1) Pada sidang di mana Ir. Sukarno berpidato tentang Pancasila tanggal 1 Juni 1945, ia diminta oleh pimpinan sidang untuk memerah Pancasila (5) menjadi Trisila (3) dengan pertimbangan lima sila masih terlalu panjang. Ia menyanggupi dan menyebutkan Trisila, yaitu Sosio-demokrasi, Sosio-nasionalisme, dan Ketuhanan yang Berkebudayaan. Namun pimpinan sidang masih kurang puas dan menantang Ir. Sukarno apakah dapat memerah tiga sila menjadi satu sila saja. Dan Ir. Sukarno kembali menyanggupi, ia menyebut Gotong Royong (pen)
***
"Pancasila jangan sebatas wawasan saja, tetapi harus menjadi ideologi aksi dalam praktek."Joesoef Isak dalam suatu tulisan pengantar di tahun 2008 (sebelum tutup usia)
***
"Tetapi Republik Indonesia menghadapkan kita dengan satu keadaan jang istimewa. Rakjat adalah beraneka ragam, beraneka adat, beraneka ethnologi. Rakjat yang demikian itu membutuhkan satu "dasar pemersatu". Dasar pemersatu itu adalah Pantjasila."
Ir. Sukarno, dalam suatu sambutan tanggal 17 Agustus 1955
