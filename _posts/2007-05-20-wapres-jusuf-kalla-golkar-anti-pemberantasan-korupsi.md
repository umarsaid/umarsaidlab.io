---
layout: post
title: Wapres Jusuf Kalla (Golkar) anti pemberantasan korupsi ?
date: 2007-05-20
---

Apakah Jusuf Kalla adalah Wakil presiden RI dan ketua umum Golkar, yang anti-pemberantasan korupsi, seperti yang tercermin dalam berita yang disiarkan oleh Suara Pembaruan tanggal 12 Mei 2007? Memang, kalau dilihat dari pernyataannya tentang kasus transfer uang haram yang dilakukan oleh Tommy Suharto dengan bantuan Yusril dan Hamid Awalludin, maka orang mudah mendapat kesan bahwa Jusuf Kalla “melindungi” kedua mantan pembesar itu. Untuk berusaha sama-sama menyoroti sikap Jusuf Kalla ini dari berbagai segi, maka di bawah ini disajikan berita tersebut di atas, disertai sejumlah komentar atau ulasan, yang lengkapnya adalah sebagai berikut :


“Pernyataan Wakil Presiden Jusuf Kalla bahwa tindakan Yusril Ihza Mahendra dan Hamid Awaludin menggunakan rekening Departemen Hukum dan HAM untuk menerima transfer uang Tommy Soeharto sebesar Rp 90 miliar dari Banque Nationale de Paris et de Paribas (BNP Paribas) cabang London bukan merupakan tindak pidana korupsi merupakan pernyataan yang menghalangi semua usaha pemberantasan korupsi yang sedang berjalan sekarang ini.
"Pernyataan Kalla itu, secara tidak langsung menghalangi aparat penegak hukum, seperti KPK, Kejaksaan atau Polri, untuk melakukan penyelidikan dan penyidikan terhadap Yusril dan Hamid terkait kasus tersebut," kata Koordinator Tim Pembela Demokrasi Indonesia (TPDI), Petrus Selestinus SH, kepada SP, Sabtu (12/5).


Sebelumnya, Wakil Presiden Muhammad Jusuf Kalla yakin bahwa mantan Menteri Hukum dan Hak Asasi Manusia Hamid Awaludin dan mantan Menteri Sekretaris Negara Yusril Ihza Mahendra tidak memiliki masalah hukum yang dikategorikan tindak pidana korupsi.
"Hukum ya hukum. Sejauh ini saya yakin tidak punya suatu masalah hukum yang kategori korupsi. Korupsi itu kan memperkaya orang lain, dengan mengambil uang negara. Itu ukuran korupsi," jelas Kalla saat ditanya tentang dugaan tindak pidana korupsi yang dilakukan Hamid dan Yusril dalam kasus uang Hutomo Mandala Putra atau Tommy Soeharto di Kantor Wapres Jakarta Jumat (11/5).
Petrus mengatakan akibat lebih jauh dari pernyataan Kalla itu adalah aparat penegak hukum tidak berani melakukan penyelidikan dan penyidikan terhadap semua aparat pemerintah yang diduga melakukan tindak pidana, terutama tindak pidana korupsi.


Uang Tommy Soeharto sebesar Rp 90 miliar atau US$ 10 juta yang disimpan di BNP Paribas cabang London sudah ditransfer melalui rekening Direktorat Administrasi Hukum Umum Departemen Hukum dan Perundang-undangan, yang "dipinjamkan" Yusril Ihza Mahendra dan diurus kantor pengacara miliknya, Ihza & Ihza. Pada Februari 2005, uang tersebut mengalir ke rekening Dirjen Administrasi Hukum Umum Departemen Hukum dan HAM. Dari rekening itu, Tommy Soeharto menarik dananya.
Sebagaimana diberitakan, sejumlah advokat dari TPDI, Komite Pembaruan Peradilan Indonesia (KPPI), dan East Solidarity, yang tergabung dalam Forum Peduli Pemerintahan yang Bebas Kolusi, Korupsi dan Nepotisme (KKN), telah melaporkan Yusril dan Hamid ke Mabes Polri.


Hal itu dilakukan karena kedua mantan menteri itu menggunakan rekening negara untuk menerima transfer uang Tommy Soeharto. Kedua mantan menteri itu patut diduga telah melakukan tindak pidana korupsi dan pencurian uang karena tindakan mereka bukan untuk menyelamatkan uang negara yang dikorupsi, malah membantu Tommy untuk terus menguasai uang hasil korupsi ayahnya.
Lebih jauh Petrus mengatakan pernyataan Kalla mengandung makna Wakil Presiden tidak ingin anak buahnya diperiksa dalam kasus-kasus tindak pidana korupsi. "Sikap Kalla itu memperlihatkan ia tidak memiliki semangat sebagai negarawan, yang selalu membela kepentingan rakyat. Ia lebih memilih membela kolega dalam ruang lingkup kecil," kata Petrus.


Seharusnya, kata Petrus, Wakil Presiden Jusuf Kalla meminta aparat penegak hukum, seperti Kejaksaan, Polri atau KPK, untuk menyelidiki Yusril dan Hamid. "Belum ada penyelidikan, Kalla malah berkoar-koar seperti itu. Hal itu sungguh bukan teladan yang baik dari seorang pemimpin. Sekarang ini memang ada sebagian pejabat negara yang sedang membangun jaringan terselubung untuk memperkuat jaringan KKN, dengan cara memperlemah institusi penegak hukum dengan segala cara," katanya.” (kutipan dari Suara Pembaruan selesai).


Citra buruk Ketua Umum Golkar

Dari secuwil berita ini saja sudah kelihatan jelas sekali bahwa sikap Jusuf Kalla, pedagang besar yang menjadi Wakil Presiden RI merangkap Ketua Umum Golkar, mengenai korupsi --penyakit bangsa yang sudah parah sekali ini -- , adalah sangat mengecewakan. Ia mengatakan “yakin bahwa mantan Menteri Hukum dan Hak Asasi Manusia Hamid Awaludin dan mantan Menteri Sekretaris Negara Yusril Ihza Mahendra tidak memiliki masalah hukum yang dikategorikan tindak pidana korupsi. “. Pernyataannya yang demikian itu hanya memperburuk citranya, dan tidak menguntungkan bagi usaha bersama untuk membrantas korupsi.

Padahal, sudah jelas bahwa lolosnya penarikan uang haram Tommy Suharto dari Banque Nationale de Paris (BNP) sebanyak US$ 10 juta adalah karena bantuan kedua pejabat tinggi tersebut, dengan menyalahgunakan rekening Departemen Hukum. Kesalahan kedua pejabat tinggi ini menyolok sekali. Sebab, apapun dalih yang dipakai oleh baik Yusril maupun Hamid, mestinya mereka - sebagai orang-orang yang bisa dianggap “ahli hukum” -- mengerti bahwa sebagian terbesar kekayaan yang dimiliki Tommy dan ayahnya adalah uang haram yang sudah sejak lama dicuri dari rakyat dan negara. Karena, masalah korupsi yang dilakukan Suharto berikut keluarga (atau anak-anaknya) ini sudah, sejak lama, menjadi pengetahuan umum di Indonesia, bahkan juga di dunia internasional.

Dengan membantu meloloskan uang haram Tommy Suharto, ini berarti bahwa mereka (Yusril dan Hamid Awaludin) jelas-jelas dapat digolongkan sebagai unsur-unsur yang berusaha tetap terus melindungi kepentingan Suharto beserta keluarganya. Bahwa Yusril mempunyai sikap yang tetap setia kepada keluarga Suharto adalah wajar, karena ia pernah “dibesarkan” oleh Suharto sebagai penulis sebagian dari pidato-pidatonya yang penting-penting.

Dari itu semua, dapat dimengerti bahwa Jusuf Kalla, yang pada dewasa ini menjadi ketua umum Golkar, berusaha juga -sedapat mungkin -- melindungi kepentingan keluarga Suharto. Karena, Suharto selama puluhan tahun pernah menjadi pembina dan pengayom Golkar. Bahwa Golkar sampai sekarang (dalam tahun 2007) dapat bertahan menjadi kekuatan politik yang utama di negara kita adalah sebagai kelanjutan dari pembinaan dan pimpinan Suharto beserta para jenderalnya.

Jadi, pembelaan Jusuf Kalla terhadap Yusril (dan Hamid) dapat juga dilihat dari segi keterkaitan historis yang erat antara Golkar dengan keluarga Cendana. Oleh karena itu, walaupun mereka berdua (Yusril dan Hamid) sudah terbukti menyalahgunakan rekening Departemen Hukum, Yusuf Kalla tetap mengatakan juga bahwa mereka tidak melakukan pelanggaran apa-apa.



Presiden BY terpaksa mencopot Yusuril dan Hamid

Ucapan Jusuf Kalla mengenai kasus Yusril dan Hamid ini kelihatan berbeda sekali dengan tindakan presiden SBY yang akhirnya harus “mencopot” kedudukan mereka sebagai Menteri Sekretaris Negara dan Menteri Hukum dan HAM. Presiden SBY terpaksa mengambil keputusan untuk mencopot kedua menteri itu akibat derasnya dan lantangnya suara-suara dalam masyarakat (termasuk bertubi-tubinya pemberitaan dalam pers) yang mengecam atau memprotes persekongkolan untuk meloloskan uang haram Tommy Suharto dari BNP. Dicopotnya kedua menteri oleh presiden SBY ini bisa dianggap sebagai bukti tidak-benarnya pernyataan Jusuf Kalla bahwa mereka berdua tidak melakukan pelanggaran apa-apa.

Segi lain yang menarik untuk diperhatikan dari pernyataan Jusuf Kalla tentang Yusril dan Hamid adalah bahwa sebagai wakil presiden RI – dan ketua umum Golkar ! – ia telah memperlihatkan sikapnya sebagai pelindung orang-orang atau anak-buahnya yang melakukan korupsi. Memang, selama ini sudah banyak tokoh-tokoh (besar atau kecil) Golkar yang terpaksa sekali diadili atau dihukum karena kejahatan korupsi, baik tokoh-tokoh di Pusat maupun di daerah-daerah. Tetapi, mereka yang sudah ditindak atau diadili ini jumlahnya kecil atau sedikit sekali kalau dibandingkan dengan kejahatan korupsi yang sebenarnya terjadi selama ini. Selain itu, juga penindakan terhadap korupsi ini dilakukan dengan cara “tebang-pilih”.



“Nama baik” Golkar dan Orba

Jusuf Kalla, sebagai ketua umum Golkar, berkepentingan sekali untuk “menjaga nama baik” Golkar. Oleh karena orang-orang Golkar banyak sekali menduduki tempat-tempat penting di badan-badan eksekutif, legislatif dan yudikatif, maka selama puluhan tahun ini sudah banyak sekali terjadi berbagai pelanggaran, penyalahgunaan kekuasaan atau kejahatan, dan di antaranya yang berbentuk korupsi besar dan kecil. Selama Suharto berkuasa memang sedikit sekali -- sekali lagi : sedikit sekali! -- adanya berita-berita mengenai korupsi yang dilakukan oleh orang-orang penting Golkar. Ini tidak berarti bahwa selama Orde Baru tidak ada korupsi,! Tetapi rejim militer Suharto dkk selalu berusaha menutup-nutupi masalah korupsi, demi nama baik atau citra Orde Baru.

Padahal, seperti sama-sama kita saksikan selama ini, korupsi adalah sebagian penting dari watak atau jati-diri rejim militer Suharto dkk. Oleh karena itu, banyak jenderal-jenderal dan pimpinan Golkar dari berbagai tingkat, di Pusat mau pun di daerah-daerah, yang bisa menumpuk kekayaan haram berkat kejahatan korupsi yang dilakukan secara “berjamaah”, saling melindungi dan saling bersekongkol, di bawah pengayoman sang koruptor besar yang bernama Suharto. “Kebudayaan korupsi” yang berkembang dengan hebatnya selama Orde Baru, yang diteruskan oleh orang-orang Golkar -- dan oleh para simpatisan Orde Baru lainnya -- sampai sekarang, adalah penyakit yang amat parah yang telah disebarkan rejim militer selama puluhan tahun.

Oleh karena itu, bisalah kiranya dikatakan bahwa selama Golkar (dan para simpatisan rejim militer Orde Baru) masih terus memegang peran penting di pemerintahan, baik sekarang ini maupun di kemudian hari, maka sangat tipislah harapan untuk adanya perubahan-perubahan sejati yang mendasar, yang memungkinkan pembrantasan korupsi secara tuntas. Sebab, perlu sekali sama-sama kita ingat bersama bahwa sudah sejak lebih dari 40 tahun yang lalu Golkar (dengan dukungan TNI-AD sebagai tulang-punggungnya) telah merusak moral sebagian besar kalangan elite bangsa kita.

Kebejatan moral dan pembusukan mental yang parah yang kita saksikan bersama selama ini di berbagai bidang -- dan yang masih nampak nyata sekali sampai sekarang dimana-mana-- adalah produk dari sistem politik, ekonomi dan sosial yang dijalankan oleh rejim militer Suharto dkk, dimana Golkar dan sebagian besar pimpinan TNI-AD memegang peran yang amat besar sekali. Yang patut diamati oleh para sejarawan, budayawan, dan para tokoh masyarakat kita serta kita catat bersama-sama, adalah bahwa rejim militer Orde Baru ini sama sekali tidak pernah menghasilkan hal-hal yang besar, yang mempunyai nilai luhur, yang bisa dijadikan kebanggaan bangsa.



Pemberantasan korupsi dan perubahan sejati

Sebaliknya, rejim militer Suharto dkk telah menimbulkan kerusakan-kerusakan besar sekali kepada kepentingan rakyat dan Republik Indonesia. Kerusakan-kerusakan besar ini, terutama di bidang moral, sebenarnya sudah berjalan sejak puluhan tahun. Begitu besarnya dan begitu luasnya atau begitu dalamnya kerusakan-kerusakan ini, sehingga sampai sekarang bangsa kita harus terus menanggungnya, termasuk di antaranya yang berbentuk penyakit korupsi, penyalahgunaan kekuasaan, dan pelanggaran HAM. Sekali lagi, tanggungjawab Golkar sebagai tulangpunggung (bersama TNI-AD) atas kerusakan dan pembusukan yang sudah berlangsung puluhan tahun ini adalah besar sekali.

Sekarang ini , Jusuf Kalla menunjukkan sikap yang melindungi Yusril dan Hamid Awaludin, yang pada hakekatnya berarti juga menolong Tommy Suharto. Sebenarnya, hal ini tidak mengherankan, kalau diingat bahwa Jusuf Kalla adalah ketua umum Golkar, dan bahwa Golkar adalah –- dalam jangka waktu puluhan tahun -- telah merupakan alat kekuasaan yang amat besar keluarga Cendana. Karena itu, masalah pemberantasan korupsi di negara kita akan tetap tidak bisa dijalankan sampai akar-akarnya, selama orang-orang yang sejenis Jusuf Kalla masih banyak memegang kekuasaan.

Perubahan sejati, atau perombakan besar-besaran, atau perbaikan menyeluruh tidak bisa dilakukan di negara kita, selama Golkar masih menguasai bidang-bidang penting dalam pemerintahan kita. Perubahan besar di Indonesia hanya akan bisa terjadi, kalau Golkar sudah bisa dilumpuhkan oleh perjuangan bersama yang dilancarkan oleh berbagai golongan dalam masyarakat kita.
