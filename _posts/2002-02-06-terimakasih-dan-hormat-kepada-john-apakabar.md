---
layout: post
title: Terimakasih dan hormat kepada John Apakabar
date: 2002-02-06
---

Bung John yang baik,

Surat terbuka ini adalah sambungan “surat pribadi” yang sudah saya kirimkan kepada Anda tanggal 4 Februari yang lalu, berkaitan dengan pengumuman tentang niat Anda untuk menghentikan situs Indopubs.com dalam waktu sebulan lagi, dan akan ditutupnya “Apakabar” (Indonesia-L) mulai tanggal 8 Februari ini. Seperti yang sudah saya nyatakan dalam E-mail saya tanggal 4 Februari itu, saya menerima berita itu dengan rasa sedih, yang bercampur dengan rasa sympati, hormat dan terimakasih.

Perasaan saya yang demikian itu menjadi makin tebal dan makin besar setelah saya membaca berbagai E-mail yang telah dikirimkan, selama beberapa hari yang terakhir ini, oleh banyak orang yang telah mengutarakan perasaan dan pendapat mereka tentang akan dihentikannya Indopubs.com dan “Apakabar”. Surat terbuka ini merupakan penggaris-bawahan berbagai hal yang sudah diungkapkan itu. Memang, sejarah lahirnya “Apakabar” dan apa yang sudah Anda kerjakan selama 11 tahun, mempunyai arti yang penting dan peran yang besar dalam sejarah perjuangan bersama untuk menegakkan demokrasi di Indonesia, membela hak asasi manusia dan melawan rezim Orde Baru. Oleh karena itu, “Apakabar” akan lama dikenang oleh banyak orang Indonesia, dengan rasa terimakasih, hormat dan kekaguman.

Mengapa saya katakan begitu? Ketika di Indonesia tidak ada kebebasan menyatakan pendapat selama Orde Baru, dan ketika banyak orang menemui kesulitan besar sekali untuk mempunyai alat atau sarana perjuangan, Anda telah menyediakan tenaga, fikiran dan dana, untuk menciptakan sarana ini bagi mereka. Selama jangka waktu yang lama sekali (sampai jatuhnya Suharto dalam tahun 1998) “Apakabar” telah menjadi corong bagi banyak orang, dalam menyampaikan fikiran dan perasaan mengenai situasi Indonesia pada waktu itu. Entah sudah berapa orang, berapa kelompok, berapa kalangan, yang selama itu telah menggunakan “Apakabar”, baik dengan nama terang-terangan maupun (dan ini yang terbanyak) dengan nama samaran.

Ketika situasi di Indonesia dicengkam oleh terror fisik dan terror mental yang amat kejam, Anda telah menolong mereka untuk menembus berbagai terror itu. Sumbangan “Apakabar” kepada perjuangan melawan rezim Orde Baru adalah besar sekali. Ini saya dengar sendiri, ketika berkali-kali berkunjung ke Indonesia dan bertemu dengan banyak teman di berbagai kota dan daerah. Juga ketika berbicara dengan banyak teman Indonesia yang terpaksa bermukim di luarnegeri.

## Anda adalah bagian dari perjuangan ini

Mengingat sulitnya situasi dalam jangka waktu yang begitu lama dalam memperjuangkan demokrasi dan melawan rezim Orde Baru, maka wajarlah bahwa banyak orang berterimakasih, menaruh hormat, bahkan kagum terhadap kesediaan Anda untuk menemani atau menolong mereka dalam perjuangan ini. Sejumlah orang merasa heran bahwa ada seorang Amerika yang begitu teguh dalam pemihakannya kepada perjuangan mereka. Bahkan, di masa lalu, ada orang-orang Indonesia yang mengatakan :” John adalah orang kita”.

Betul, John! Seperti yang sudah saya tulis dalam E-mail yang lalu, dalam perjuangan untuk demokrasi dan hak asasi manusia di Indonesia, banyak teman menganggap bahwa Anda adalah bagian dari mereka, bagian dari kita. Dan, ketika Suharto sudah jatuh, dan ketika perjuangan untuk demokrasi dan reformasi berjalan seret dan kadang-kadang macet, Anda masih tetap terus dipandang sebagai bagian dari “kita” ini.

Oleh karena itu, maka dapatlah kiranya dimengerti bahwa banyak orang merasa sedih ketika mendengar bahwa “Apakabar” akan dihentikan. Memang, sekarang situasi sudah agak berbeda. Di Indonesia ( dan di negara-negara lain juga) sudah banyak situs yang dibuka oleh orang-orang Indonesia. Namun, bagi banyak orang yang sudah mengenal “Apakabar” sejak begitu lama, sarana yang ada di bawah asuhan Anda mempunyai integritas dan nama yang tersendiri. Sebab, selama 11 tahun, Anda dengan “Apakabar” bukan saja telah memperlihatkan idealisme yang tinggi, melainkan juga sikap toleransi dan pandangan luas dalam mengelola pertukaran fikiran (bahkan perdebatan) mengenai persoalan-persoalan yang berkaitan dengan Indonesia. Seperti yang sudah saya tulis dalam E-mail tanggal 4 Februari yl, saya menganggap bahwa “Apakabar” adalah monumen dalam perjuangan untuk demokrasi di Indonesia.

## Harapan saya (seperti orang yang lain juga)

Seperti halnya banyak orang lain, saya juga terimakasih kepadamu John. Sebab, sejak 1995 saya mengenal “Apakabar” lewat seorang teman di Paris, dan mulai mengirimkan tulisan-tulisan kepada “Apakabar” dengan berbagai nama< samaran. Saya jelaskan kepada Anda bahwa saya, waktu itu, terpaksa menggunakan begitu banyak nama samaran itu karena berbagai pertimbangan, yang berhubungan dengan situasi di Indonesia waktu itu. Saya masih ingat bahwa dalam tahun 1996 Anda memberikan dorongan (encouragement) mengenai berbagai aspek tulisan-tulisan saya waktu itu. Di samping itu, Anda juga telah menyampaikan kepada saya sejumlah reaksi yang baik dari berbagai orang mengenai tulisan-tulisan itu.

Karena itu, saya juga merasa kehilangan dengan adanya berita bahwa “Apakabar” yang historis dan legendaris ini akan dihentikan, walaupun akan tetap menjadi kenangan indah bagi banyak orang. “Apakabar” adalah unik bagi pengalaman banyak orang Indonesia. Dan walaupun milis-milis lainnya sekarang banyak yang muncul dan bisa digunakan sebagai sarana, namun seyogyanya tradisi “Apakabar” masih bisa diteruskan. Untuk itu, berbagai jalan bisa ditempuh bersama-sama. Umpamanya, mungkin gagasan yang sudah dimunculkan untuk meneruskannya dengan menggunakan fasilitas yahoogroups bisa kita pelajari bersama.

Mengingat jasa John yang sudah begitu besar, dan juga mengingat integritas John yang sudah diakui oleh banyak orang, dan sudah dibuktikan dalam waktu yang begitu lama, maka adalah sangat ideal kalau John masih ikut dalam penggunaan fasilitas yahoogroups ini. Tentu saja, perlu dicari cara, sehingga pekerjaan John tidak terlalu berat. Kita berharap bahwa John masih bisa terus memikul “missi” yang sudah begitu lama Anda laksanakan dengan< baik itu.

Namun, walaupun akhirnya “Apakabar” tidak diteruskan juga, satu hal yang sudah jelas dan pasti adalah yang berikut : “John, terimakasih atas kesediaanmu untuk membantu, mendorong, dan menemani perjuangan kita dalam perjalanan panjang menuju demokrasi di Indonesia”.

Mengharapkan Anda selalu dalam kesehatan, dan bisa terus mengerjakan hal-hal yang berguna bagi kemanusiaan, di manapun Anda berada.
