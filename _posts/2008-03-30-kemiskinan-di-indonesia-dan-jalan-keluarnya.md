---
layout: post
title: Kemiskinan di Indonesia dan jalan keluarnya
date: 2008-03-30
---

Di tengah-tengah terus mengalirnya berbagai berita tentang buntut yang panjang dari kasus BLBI, yang menyebabkan dirugikannya rakyat sampai ratusan triliun Rupiah dan persidangan di pengadilan tentang dana Yayasan Supersemar, maka berita tentang parahnya kemiskinan yang menimpa penduduk di banyak daerah di Indonesia merupakan peringatan keras kepada kita semua bahwa negara dan bangsa kita dewasa ini memang sedang menghadapi situasi yang memerlukan perbaikan atau perubahan secara besar-besaran. Kasus suapan 6 miliar Rupiah kepada jaksa Urip dan orang-orang lainnya dari Kejaksaan Agung oleh Artalyta Suryani (orang dekat Syamsul Nursalim) menunjukkan hanya sebagian kecil sekali dari banyaknya pejabat penting negara dan « tokoh-tokoh » utama masyarakat Indonesia yang sudah tumbang imannya atau bejat moralnya.


Keruntuhan iman atau pembusukan moral ini tercermin dalam banyaknya korupsi dan penyalahgunaan kekuasaan untuk menumpuk kekayaan dengan cara-cara haram, atas kerugian negara dan rakyat. Contoh-contohnya dapat selalu kita baca dalam pers Indonesia, yang sebagian di antaranya juga sering disiarkan dalam website http://kontak.club.fr/index.htm dalam rubrik « Korupsi memalukan Islam dan bangsa ».


Kalau kita baca berita-berita soal korupsi di kalangan « atasan », yang jumlahnya sering bisa sampai bermiliar-miliar Rupiah, dan kemudian kita banding-bandingkan dengan berita tentang banyaknya orang miskin dan anak-anak balita yang kurang gizi dan busung lapar, maka bisa mengertilah kita bahwa ada orang-orang yang sudah keterlaluan marahnya sehingga sampai mengatakan bahwa negara kita memerlukan revolusi sosial. Memang, adalah hal yang benar atau hal yang sah (artinya, baik sekali !) bahwa hati dan fikiran banyak orang « brontak » terhadap situasi yang membikin puluhan juta -- bahkan mungkin lebih dari seratus juta -- penduduk Indonesia menderita kesengsaraan yang parah dan berkepanjangan terus.


Berikut di bawah ini disajikan beberapa kutipan pers dan juga tulisan yang bisa dijadikan bahan untuk renungan kita bersama tentang kemiskinan yang parah di kalangan rakyat kita. Apa yang diungkapkan di sini dimaksudkan sekadar sebagai dorongan kepada kita semuanya untuk bersama-sama ikut memikirkan tentang sebab-sebab kemiskinan ini dan kemungkinan-kemungkinan untuk mencari jalan mengatasinya.


Di Banten, Jatim, Sulsel dan Jateng


Menurut harian Sinar Harapan (25 Maret 2008) : » Jumlah penduduk miskin di Indonesia semakin meningkat dari tahun ke tahun sebagai akibat dari kondisi perekonomian yang tidak stabil. Kenaikan harga-harga akhir-akhir ini termasuk sembako dikhawatirkan akan semakin meningkatkan angka kemiskinan. Fakta tersebut dirangkum Sinar Harapan dari beberapa daerah termasuk Banten, Surabaya (Jawa Timur), Makassar (Sulawesi Selatan), dan Cilacap (Jawa Tengah).
“Badan Pusat Statistik (BPS) Provinsi Banten mencatat jumlah kemiskinan mengalami kenaikan. Jika tahun 2006 tercatat 786.700 keluarga miskin, tetapi pada awal tahun 2008 menjadi 886.000 keluarga. Jika satu keluarga terdiri dari suami, istri, dan satu anak, maka jumlah orang miskin di Banten mencapai 2.685.000 orang, dari 9,5 juta penduduk Banten. Dinas Kesehatan (Dinkes) Provinsi Banten mengakui terdapat 11.244 bayi di bawah umur lima tahun (balita) yang menderita gizi buruk, di antaranya 15 balita meninggal.


“Di Jawa Timur, dari sekitar 38 juta jiwa penduduknya, 7,1 juta jiwa masih berada di bawah garis kemiskinan. Kemiskinan ini dipicu oleh jaminan kehidupan yang sangat rendah, mulai dari pendapatan rendah, pendidikan rendah, jumlah tanggungan banyak, atau karena musibah. Fakta lain bisa dilihat dari angka balita gizi buruk yang cukup tinggi. Pada Januari 2008, di Surabaya tercatat pasien gizi buruk sebelas anak dan balita. Pada Februari 2008, sembilan pasien gizi buruk dirawat. Hingga pertengahan Maret lalu, sudah delapan pasien dirawat karena kasus yang sama. Data gizi buruk tersebut hanya yang tercatat di RS Dr Soewandie Surabaya, belum termasuk di RS lainnya.


Jumlah warga miskin makin bertambah


“Dari data Dinkes Surabaya, pada tahun 2006, prevalensi balita gizi kurang sebesar 8,32 persen dan pada 2007 turun menjadi 6,86 persen. Tahun 2006 sebesar 2,09 persen, dan tahun 2007 menjadi 1,96 persen. Kepala Dinas Kesehatan Kota Surabaya dr Esti Martiana mengatakan tingginya kasus gizi buruk karena perilaku hidup sehat masyarakat yang memang rendah, ditunjang dengan rendahnya daya beli. Semburan lumpur Sidoarjo yang telah berlangsung hampir dua tahun ini memiliki kontribusi munculnya kemiskinan baru. Ribuan warga kehilangan pekerjaan. Demikian juga bencana banjir yang melanda lebih dari 15 daerah di Jatim semakin menambah keterpurukan petani, apalagi harga kebutuhan pokok semakin melambung.

”Di Kota Makassar, jumlah warga miskin sekitar 350.780 jiwa (70.156 keluarga) atau sekitar 30 persen dari total penduduk 1,2 juta jiwa lebih. Sementara itu tahun 2005 jumlahnya masih sekitar 60.000 keluarga yang tersebar di 14 kecamatan. Berdasarkan data sensus daerah (Susda) Provinsi Sulsel dua tahun lalu, jumlah penduduk miskin masih 201.487 juta keluarga (sekitar 820.000 jiwa) atau sekitar 10,85 persen dari sekitar 8 juta jiwa penduduk di daerah ini. Jumlah tersebut terus berkembang hingga saat ini. Dinas Ketahanan Pangan (DKP) Sulsel mencatat, dari 23 kabupaten/kota di Sulsel, masih terdapat tujuh kabupaten dalam kondisi rawan pangan, diantaranya Kota Makassar, Kabupaten Jeneponto, Takalar, dan Selayar.


“Begitu pula di Cilacap, Jawa Tengah, 635.000 jiwa atau sekitar 163.000 keluarga berstatus warga miskin. Hal itu disampaikan secara resmi oleh Badan Perencanaan Pembangunan Daerah (Bappeda) pada pekan lalu. Jumlah warga miskin tersebut merupakan 37 persen dari jumlah total penduduk Cilacap yang mencapai 1,7 juta jiwa. Sementara itu, berdasarkan jatah beras untuk warga miskin (raskin) di wilayah Bulog Subdivisi Regional IV Banyumas, di Cilacap yang mendapat bantuan raskin sekitar 170.000 keluarga, di Banyumas 173.479 keluarga, Kabupaten Purbalingga 105.690 keluarga dan Banjarnegara 112.979 keluarga. (Kutipan dari Sinar Harapan, 25 Maret 2008, disingkat)


Bisa makin bertambah parah lagi


Apa yang tercantum di atas adalah baru satu berita dari satu koran pada satu hari saja, tetapi toh sudah cukup kiranya bagi seseorang untuk membayangkan betapa besarnya kemiskinan yang juga melanda berbagai daerah lainnya di negeri kita, umpamanya di Sumatera, Kalimantan, Sulawesi Tengah, Maluku, Indonesia Timur, Nusa Tenggara, termasuk di pulau Jawa. Penderitaan rakyat yang diakibatkan oleh kemiskinan yang luas ini sekarang makin bertambah lagi, dengan adanya kenaikan yang tinggi sekali harga-harga pangan (beras, jagung, kedelai, cabe, daging sapi, ayam, minyak goreng dll) dan bahan bakar. Kenaikan harga pangan ini masih akan bisa lebih parah lagi kalau krisis pangan di skala internasional sudah mulai juga memasuki negeri kita. Maka, betul-betul cilakalah sebagian besar rakyat Indonesia !!!


Adalah sangat menarik untuk diperhatikan bersama bahwa soal krisis pangan ini rupanya mendorong presiden SBY mengirim surat kepada Sekjen PBB , Ban Ki Moon, karena harga-harga pangan di skala internasional sudah melambung tinggi dengan kenaikan 40 %. Ini masih ditambah gejolak keuangan global yang sampai sekarang belum rampung dan kita belum tahu tentang kerusakan atau dampak yang terjadi. Pernyataan presiden SBY ini dikuatkan oleh pernyataan Deputi Gubernur Bank Indonesia, Miranda Goeltom, yang mengatakan bahwa harga komoditas pangan dunia saat ini mencapai puncak tertinggi. (Sinar Harapan, 27 Maret 2008)


Kalau harga-harga makin memuncak dan krisis pangan mulai menyerang, maka akan makin banyak jugalah anak-anak balita yang mengalami gizi buruk atau busung lapar. Artinya, kalau menurut Kepala Pusat Ketersediaan dan Kerawanan Pangan Departemen Pertanian (Deptan) RI Tjuk Eko Hari Basuki, 27 persen bayi di bawah lima tahun (balita) di Indonesia mengalami gizi buruk, maka angka 27 persen itu akan bertambah, entah dengan berapa persen. "Gizi buruk itu tidak terjadi mendadak, tapi sudah lama. Hasilnya, kami memberikan Rp25 juta kepada setiap dari 300 kabupaten/kota yang tergolong miskin. Di Jatim sendiri tercatat delapan daerah miskin, terutama di Madura dan kawasan `tapal kuda`," katanya, menurut berita Antara 13 Maret 2008.


Anak-anak balita yang kurang gizi


Para pembaca yang budiman, mohon direnungkan dalam-dalam isi beberapa berita tersebut di atas. Mengapa di negeri kita, yang terkenal sebagai negeri yang kaya dengan sumber alam dan beraneka-ragam tumbuh-tumbuhan, dan sebagian besar tanahnya juga subur, bisa menghadapi kemiskinan yang demikian parah ? Siapakah yang salah, dan apanya sajakah yang salah ? Atau, siapa yang harus bertanggungjawab atas keadaan yang sudah membikin kesengsaraan puluhan juta, bahkan ratusan juta penduduk ini ? Dan lagi, mohon juga ikut difikirkan akibat yang menyedihkan bagi generasi kita yang akan datang, kalau 27 persen dari anak-anak balita di seluruh Indonesia menderia kurang gizi dan busung lapar. Karena, anak-anak balita yang kurang gizi ini akan kurang normal pertumbuhannya, sehingga akan merusak mutu generasi kita di kemudian hari.


Untuk menambah gambaran lainnya tentang akibat kemiskinan yang meluas di Indonesia adalah angka-angka yang juga cukup “mengerikan” yang bersumber dari UNICEF dan disiarkan oleh harian Kompas (28 Maret 2008). Di situ dijelaskan bahwa 69 juta orang di Indonesia tidak memiliki akses terhadap sanitasi dasar dan 55 juta orang di Indonesia tidak memiliki akses terhadap sumber air yang aman. Menurut sumber tersebut, keadaan yang demikian ini menyebabkan setiap tahun 100.000 anak berusia dibawah 3 tahun di Indonesia meninggal karena penyakit diare. Ditambahkan juga bahwa setiap harinya ada sekitar 5.000 anak dibawah umur 5 tahun yang meninggal karena diare itu.


Kiranya, jelaslah bahwa sebagian besar kemiskinan yang begitu parah di berbagai daerah negeri kita ini sama sekali bukanlah kesalahan puluhan juta penduduk itu sendiri. Dan, jelaslah juga bahwa 27% dari anak-anak balita di seluruh Indonesia yang kurang gizi atau busung lapar adalah bukan pula dosa anak-anak itu atau orang tua mereka masing-masing. Dan kiranya perlu pula diyakini oleh kita semua bahwa kemiskinan yang menimpa begitu banyak orang itu sama sekali bukanlah kehendak Tuhan atau takdir semata-mata. Atau, juga sama sekali bukanlah hukuman Tuhan atau cobaan terhadap jutaan manusia yang tidak bersalah apa-apa. Artinya, kemiskinan yang luas itu bukanlah « nasib » semata-mata, yang harus diterima dengan sabar dan tawakal saja. Kemiskinan itu adalah akibat perbuatan manusia-manusia juga, yang juga bisa dirubah atau dilawan bersama-sama.
Sebab, walaupun banjir sering melanda berbagai daerah, atau gempa menggoncang banyak tempat, atau bahaya kekeringan menimpa banyak lahan, atau lumpur Lapindo sudah menenggelamkan banyak rumah penduduk, namun penderitaan banyak orang bisa ditanggulangi, dan kemiskinan bisa juga dikurangi, asal saja ada pengelolaan negara yang beres. Negara dan pemerintahan ini adalah bikinan manusia. Negara dan pemerintahan bisa baik, kalau dikelola oleh orang-orang yang baik dan dengan sistem yang baik pula. Dan orang-orang beserta sistem inilah merupakan unsur utama dari suatu kekuasaan politik.


Kemiskinan yang meluas adalah produk kekuasaan politik


Kemiskinan yang sekarang ini melanda Indonesia secara luas, pengangguran yang membengkak sampai puluhan juta orang, anak-anak balita yang kurang gizi yang begitu banyak (27% dari seluruh balita di Indonesia), korupsi yang terus merajalela, kerusakan moral dan kebejatan iman yang telah membusukkan kehidupan « elite » bangsa, kasus BLBI yang berbuntut panyang, kasus KKN-nya Suharto beserta anak-anaknya, bobroknya sistem hukum dan peradilan, berbagai pelanggaran HAM, adalah semuanya produk satu kekuasaan politik. Yaitu produk kekuasaan politik yang mula-mula dibangun oleh Suharto dengan Orde Barunya, dan diteruskan oleh berbagai pemerintahan, sampai pemerintahan SBY-JK yang sekarang ini.


Dengan naiknya harga-harga yang makin menyulitkan kehidupan sehari-hari bagi rakyat, maka banyak golongan dalam masyarakat akhir-akhir ini menggelar berbagai kegiatan atau aksi-aksi di banyak daerah, untuk memanifestasikan kemarahan mereka dan aspirasi mereka akan adanya perubahan untuk perbaikan hidup mereka. Banyaknya aksi-aksi dan beraneka-ragamnya tuntutan yang mereka lancarkan adalah tanda yang penting (dan menggembirakan sekali) yang menunjukkan bahwa sebagian besar rakyat berani bangkit dan mengeluarkan suara-suara mereka, untuk mengkritik penyelenggaraan pemerintahan yang tidak beres, untuk menghujat korupsi dan penyelewengan kekuasaan, dan untuk melawan segala ketidakadilan.


Banyaknya aksi-aksi atau beraneka-ragamnya kegiatan yang dilakukan oleh berbagai golongan ini (antara lain : pemuda, mahasiswa, buruh, tani, pegawai negeri, perempuan, pedagang kecil, korban Lapindo, korban gempa dan banjir, pekerja perkebunan) juga menunjukkan makin bertambahnya kesadaran banyak orang untuk berorganisasi dan melakukan kegiatan atau perjuangan secara kolektif dan terkoordinasi. Walaupun sebagian dari aksi-aksi ini untuk sementara masih berjalan sendiri-sendiri atau terpencar-pencar, namun tetap merupakan bagian dari perkembangan yang penting. Sebab, perkembangan perjuangan berbagai golongan ini akhirnya akan melahirkan kekuatan-kekuatan baru dan pemimpin-pemimpin baru, setelah melalui “seleksi” jangka panjang oleh rakyat yang mendambakan demokrasi dan keadilan. Dalam situasi yang begini ini, peran kaum muda dari berbagai kalangan adalah sangat penting, sebagai bagian dari agen-agen perubahan.


Sekali lagi, patut diulangi, bahwa bangkitnya berbagai kalangan atau golongan masyarakat melalui aksi-aksi atau kegiatan yang beraneka-ragam adalah maha-penting untuk perjuangan memperbaiki kehidupan sehari-hari dan melawan politik pemerintah yang merugikan kepentingan rakyat. Bangkitnya berbagai golongan melalui aksi-aksi yang terkoordinasi juga akan merupakan sumbangan penting kepada usaha untuk mengadakan perubahan-perubahan besar, termasuk perubahan dalam kekuasaan politik. Karena, makin jelaslah sudah sekarang ini, bahwa banyak lembaga negara dan pemerintahan (umpamanya DPR) makin kehilangan kepercayaan rakyat. Karena itu, maka aksi-aksi atau kegiatan extra-parlementer akan memegang peran yang makin penting dan utama dalam mengusahakan adanya perubahan-perubahan yang besar dan mendasar.


Mengikuti jejak Amerika Latin


Perlulah kiranya diulangi, untuk kesekian kalinya, bahwa pengalaman di banyak negeri Amerika Latin (terutama Venezuela dan Bolivia) menunjukkan betapa pentingnya berbagai aksi-aksi massa luas sebagai sumbangan -- yang menentukan! -- kepada berhasilnya perjuangan parlementer untuk mengambil alih kekuasaan politik dari tangan kaum reaksioner. Akan datanglah saatnya nanti, bagi kekuatan demokratis di Indonesia, untuk mengikuti jejak serta pengalaman negeri-negeri Amerika Latin, tetapi dengan menemukan cara dan jalannya sendiri, yang sesuai dengan kondisi kongkrit negeri kita.


Jadi, singkatnya, kemiskinan, pengangguran, anak balita yang kurang gizi, dibarengi dengan korupsi dan kebobrokan moral yang bisa kita saksikan bersama-sama dengan jelas dewasa ini adalah semuanya merupakan “penyakit kronis’ yang ditimbulkan oleh kekuasaan politik yang tidak mengutamakan kepentingan rakyat banyak. Kekuasaan politik ini (yang didominasi oleh Golkar serta para simpatisan Orde Baru) namanya yang sekarang adalah pemerintahan SBY-JK.


Dan, sudah bisalah kita ramalkan, bahwa berbagai “penyakit kronis” yang demikian parah itu tidak akan bisa diberantas dengan adanya pemerintahan baru yang dihasilkan Pemilu tahun 2009. Sebab, sudah jelas bahwa Pemilu 2009 tidak akan melahirkan kekuasaan politik yang pro-rakyat, yang anti-dominasi ekonomi asing, dan yang tegas-tegas berorientasi masyarakat adil dan makmur, seperti yang dicita-citakan sejak lama oleh rakyat bersama Bung Karno.
