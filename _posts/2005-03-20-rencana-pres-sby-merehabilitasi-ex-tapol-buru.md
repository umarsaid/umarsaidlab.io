---
layout: post
title: Rencana Pres. SBY merehabilitasi ex-tapol Buru
date: 2005-03-20
---

Baru-baru ini dalam pers Indonesia (antara lain : Kompas, Suara Pembaruan, Jakarta Post) dimuat berita yang bisa menimbulkan perasaan gembira bagi banyak orang, terutama bagi para korban 65 dan eks-tapol. Contohnya adalah yang diberitakan oleh Suara Pembaruan tanggal 17 Maret 2005 yang berbunyi sebagai berikut :

« Presiden Susilo Bambang Yudhoyono berencana merehabilitasi para mantan tahanan politik (tapol) Pulau Buru yang sampai saat ini belum pulih hak-haknya sebagai warga negara. Mantan tapol Pulau Buru adalah orang-orang yang dituduh terlibat G30S pada 30 September 1965 yang sampai saat ini belum pernah ditangani dengan baik oleh negara.

« Rencana Presiden itu dikemukakan Ketua Komnas HAM, Abdul Hakim Garuda Nusantara kepada wartawan seusai bertemu Presiden Susilo Bambang Yudhoyono didampingi beberapa anggota Komnas HAM, di Kantor Presiden, Rabu (16/3).

« Menurut Ketua Komnas HAM, Presiden juga berencana memberikan kompensasi dan pemulihan penuh para mantan tapol Pulau Buru itu sebagai Warga Negara Indonesia. "Namun konkretnya seperti apa, Presiden belum mengemukakan karena memang ini baru rencana," katanya. « Presiden memberikan rehabilitasi terhadap para mantan tapol G30S itu terkait dengan akan dibentuknya Komisi Kebenaran dan Rekonsiliasi (KKR) yang undang-undangnya sampai sekarang ini masih dibahas di DPR (kutipan berita selesai).

## Presiden SBY Perlu Diingatkan Dan Ditagih Terus

Pernyataan Presiden SBY tersebut di atas, seperti yang disampaikan oleh Ketua Komnas HAM, adalah sesuatu yang sudah ditunggu-tunggu lama sekali oleh banyak orang. Sebab, banyak orang berpendapat bahwa memberikan rehabilitasi (dan kompensasi) kepada para tahanan Pulau Buru adalah suatu keharusan yang wajar bagi pemerintah RI. Karena, pembuangan sewenang-wenang terhadap kurang lebih 10.000 tahanan politik di pulau yang masih merupakan hutan-belukar, dan sampai 10 tahun lebih lamanya, adalah suatu penyiksaan yang sangat kejam dan tidak berperi-kemanusiaan sama sekali. Apalagi kalau diingat bahwa para tapol itu (seperti halnya kebanyakan para korban 65 lainnya di seluruh Indonesia) ternyata sama sekali tidak pernah bersalah apa-apa, tidak berdosa, tidak melanggar undang-undang negara, dan tidak merugikan kepentingan orang lain).

Tapi, pernyataan Presiden SBY yang menggembirakan itu masih bisa menimbulkan berbagai pertanyaan. Umpamanya, apakah rencana merehabilitasi dan memberikan kompensasi kepada para ex-tapol Pulau Buru akan dapat dilaksanakan segera ? Dan rencana itu kongkritnya nanti bagaimana ? Bagaimana melaksanakan rencana itu, dengan keputusan presiden atau dekrit atau bentuk perundang-undangan lainnya ? Bagaimana menghadapi kemungkinan perlawanan atau tentangan terhadap rencana ini ?

Di samping itu, ada persoalan lain yang juga cukup penting untuk mendapat klarifikasi. Mengapa (menurut berita tersebut di atas) rencana ini hanya ditujukan kepada para eks-tapol Pulau Buru ? Apa rencana tersebut di atas merupakan bagian dari rehabilitasi (dan kompensasi) bagi korban 65 lainnya yang jumlahnya jutaan itu ? Sebab, masalah ex-tapol Pulau Buru hanyalah sebagian dari persoalan besar yang berkaitan dengan para korban 65. Memberikan rehabilitasi dan kompensasi kepada ex-tapol Pulau Buru adalah tindakan yang baik dan adil. Tetapi adalah lebih baik dan lebih adil, kalau tindakan ini juga diperlakukan terhadap para korban 65 lainnya.

Betapa pun juga, (kalau betul-betul sudah bisa dilaksanakan !), rencana Presiden SBY merupakan peristiwa besar sekali, yang bisa menghilangkan kepedihan luka parah, yang diderita oleh begitu banyak orang selama puluhan tahun. Oleh karena itu, segala usaha hendaknya dilakukan oleh semua fihak untuk terus-menerus mengingatkan, atau menagih, atau mendesak, atau mengontrol Presiden SBY, supaya rencananya itu bisa dilaksanakan. Ini untuk kepentingan kita semua masa kini, dan untuk generasi kita yang akan datang. Janganlah sampai rencana ini mati sebelum lahir, atau mandeg di tengah jalan, karena berbagai sebab.

## Rehabilitasi Adalah Hak Mereka Yang Adil

Dan, untuk mendorong Presiden SBY supaya lebih tegas dan lebih nyata dalam usahanya merealisasikan rencananya, maka perlu adanya berbagai (dan banyak !) kegiatan dari macam-macam golongan dalam masyarakat untuk mendukung sikapnya yang amat positif ini. Dukungan yang kuat dari berbagai fihak amatlah diperlukan untuk memperkuat posisi Presiden SBY dalam menghadapi golongan-golongan yang tidak menyetujui rencana untuk memberikan rehabilitasi dan kompensasi kepada para korban 65. Dukungan ini dapat dilakukan dengan berbagai cara dan melalui berbagai saluran, termasuk melalui para pembantunya dan orang-orangnya yang terdekat.

Kepada yang tidak (atau belum) menyetujui rehabilitasi korban 65, seyogianya diingatkan bahwa rehabilitasi itu adalah hak mereka yang wajar, hak mereka yang adil, hak mereka yang sah ! Orang-orang yang tidak menyetujui rehabilitasi patut diajak merenungkan bersama-sama bahwa sama sekali tidak ada untungnya bagi siapapun untuk melestarikan kesalahan berat yang juga merupakan dosa besar bangsa ini. Perlu disadarkan kepada mereka bahwa perlakuan terhadap korban 65 (eks-tapol Pulau Buru dan lain-lainnya) merupakan pelanggaran HAM yang amat serius sekali, dan yang menjadi aib besar bangsa kita.

Orang-orang yang bernalar sehat - dan berhati nurani bersih !!! - pastilah akan ikut berpendapat bahwa merehabilitasi dan memberikan kompensasi kepada para eks-tapol atau para korban 65 lainnya (yang terbukti tidak bersalah ini) adalah perbuatan yang adil. Sebaliknya, tidak memberikan rehabilitasi (dan kompensasi) kepada orang-orang ini adalah sama saja dengan meneruskan berlangsungnya perlakuan buruk yang tidak mencerminkan peradaban. dan yang sudah berlangsung puluhan tahun ini.

Mengingat itu semua, maka kalau rencana Presiden SBYuntuk memberikan rehabilitasi dan kompensasi kepada para korban 65 (termasuk mantan tapol Pulau Buru) dapat direalisasikan dengan baik dan tuntas, ini bukan saja merupakan koreksi terhadap kesalahan besar yang pernah dilakukan oleh rezim militer Orde Baru, melainkan juga akan menjadi sumbangan yang besar untuk menggalang persatuan bangsa berdasarkan keadilan, kesetaraan, perikemanusiaan, sebagai sesama warganegara dan sebagai manusia.

## Noda Hitam Dalam Sejarah Bangsa

Sesudah hampir selama 40 tahun menanggung berbagai penderitaan, karena dipenjarakan tanpa pengadilan, dan kemudian dilepas tetapi masih tetap terus diperlakukan tidak sebagai warganegara yang penuh, maka para korban 65 (termasuk para ex-tapol Pulau Buru ) sekarang bisa mempunyai harapan bahwa akan ada perobahan dalam nasib mereka. Rencana Presiden SBY seperti yang bisa kita baca di atas, bisa merupakan perkembangan yang penting dalam kehidupan bangsa, kalau sudah bisa dilaksanakan secara baik.

Pembunuhan besar-besaran tahun 65 dan pemenjaraan begitu banyak warganegara RI (termasuk ke Pulau Buru) adalah noda hitam dalam sejarah bangsa, atau dosa besar yang dibikin oleh segolongan kecil bangsa kita. Begitu besarnya pelanggaran terhadap kemanusiaan ini, sehingga opini internasional pernah beramai-ramai mengutuknya. Akhirnya, berkat tekanan dan kutukan gencar dari opini internasional yang terus-menrus, para tapol Buru terpaksa dibebaskan oleh rezim Orde Baru menjelang tahun 1979 dan 1980.

Tetapi, para tapol itu dibebaskan dari penjara di kamp pembuangan Pulau Buru dan penjara-penjara lainnya di seluruh Indonesia, hanya untuk kemudian dimasukkan ke dalam kamp pembuangan yang lebih besar, yaitu masyarakat. Sebab, selama puluhan tahun sesudah dibebaskan, mereka masih terus mendapat berbagai perlakuan (dari penguasa dan sebagian masyarakat) yang tetap mendatangkan berbagai penderitaan atau siksaan. Sebagian terbesar dari para korban 65 ini, yang jumlahnya kurang-lebih 20 juta, telah diperlakukan sebagai warganegara kelas dua atau kelas kambing.

Dan kita perlu ingat juga bahwa Orde Baru, dalam jangka lama sekali -- yaitu kurang lebih selama 30 tahun ! – telah meracuni fikiran dan merusak hati banyak orang, sehingga mungkin tidak begitu mudah bagi Presiden SBY untuk merealisasikan rencananya. Karenanya, kita akan menyaksikan, di hari-hari yang akan datang, bahwa cukup banyak orang yang tetap tidak setuju rehabilitasi dan kompensasi para korban 65, dan yang mau meneruskan berlangsungnya ketidakadilan yang sudah terbukti menimbulkan kesengsaraan bagi begitu banyak orang itu. Terhadap orang-orang semacam itu, kita doakan saja semoga Tuhan memberikan ampun sebesar-besarnya serta menunjukkan jalan yang benar.....

Dari segi inilah kita bisa melihat bahwa perealisasian rencana Presiden SBY mengenai rehabilitasi dan kompensasi untuk para eks-tapol mempunyai arti yang besar sekali bagi kehidupan bangsa kita dewasa ini, dan juga bagi generasi yang akan datang.
