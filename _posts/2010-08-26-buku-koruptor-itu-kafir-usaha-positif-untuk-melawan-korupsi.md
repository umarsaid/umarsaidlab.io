---
layout: post
title: 'Buku « Koruptor itu kafir » usaha positif untuk melawan korupsi'
date: 2010-08-26
---

Berbagai pendapat tentang sikap NU dan Muhammadiyah

terhadap hukuman untuk para koruptor



Akhir-akhir ini ada perkembangan yang menarik di kalangan dua organisasi  Islam di Indonesia Nahdlatul Ulama dan Muhammadiyah. Yaitu dengan diterbitkannya  buku « Koruptor itu kafir », yang isinya mengindikasikan adanya kesatuan pikiran antara dua organisasi Islam besar ini tentang masalah  korupsi.


Diterbitkan Mizan dengan pendanaan dari Kemitraan Patnership ini, buku ini diluncurkan Rabu 18 Agustus 2010 di Jakarta, menjelang acara buka puasa. Hadir dalam peluncuran buku itu,  editor buku, Bambang Widjojanto yang juga salah satu kandidat pimpinan KPK. Juga hadir Sekjen Suriyah PBNU Malik Madani.



Berikut ini adalah sejumlah bahan-bahan pemikiran untuk sekadar kajian atau renungan bersama mengenai hal yang bisa menjadi soal yang banyak dibicarakan di kalangan Islam dan di kalangan rakyat Indonesia pada umumnya :



Adanya  kesatuan pikiran dua organisasi besar Islam tentang kejahatan besar dan  meluas di seluruh negeri ini adalah teramat penting dalam usaha untuk membersihkan dan menyehatkan negara dan bangsa yang sedang sakit parah sekarang ini.



Sebab, negara kita Republik Indonesia yang mempunyai penduduk nomor 4 besarnya di dunia ini juga mempunyai warganegara yang sebagian besarnya adalah beragama Islam. Karena itu, penduduk  Indonesia yang beragama Islam adalah yang terbesar dari penduduk negara mana pun di dunia yang beragama Islam. Bahkan lebih besar dari seluruh penduduk negara-negara Arab dijadikan satu. Dan seperti kita ketahui bersama, penduduk yang beragama Islam merupakan majoritas di Indonesia.



Tetapi, malangnya, kemajoritasan penduduk yang beragama Islam ini tidak selalu menunjukkan aspek-aspek positif bagi kehidupan Republik Indonesia yang berdasarkan Pancasila dan Bhinneka Tunggal Ika. Kadang-kadang, masih ada saja terdengar dari kalangan Islam suara-suara miring yang menentang Pancasila dan Bhinneka Tunggal Ika sebagai perwujudan pluralisme, yang terkandung dalam kedua dasar-dasar dan pedoman negara kita bersama itu.



Kita semua patut menghargai sikap kedua organisasi Islam Nahdlatul Ulama dan Mumammadiyah, yang walaupun kadang-kadang mempunyai sikap yang  tidak sesuai dengan kehendak atau keinginan seluruh bangsa Indonesia, tetapi pada pokoknya masih mendukung prinsip-prinsip Pancasila dan Bhinneka Tunggal Ika. Kedua organisasi Islam ini masih tetap terus merupakan penghalang atau penentang golongan Islam fanatik dan fundamentalis dalam kalangan Islam di Indonesia, dan penentang terrorisme.



Sumbangan positif Nahdlatul Ulama dan Muhammadiyah



Sekarang, diterbitkannya buku « Korupsi itu kafir » merupakan sumbangan positif lainnya yang penting dari Nahdlatul Ulama dan Muhammadiyah kepada bangsa dan negara, mengingat besarnya kejahatan dan beratnya dosa para koruptor terhadap rakyat Indonesia, termasuk terutama sekali para pemeluk agama Islam sendiri.



Sebab, tidaklah bisa dan tidak perlu dibantah lagi bahwa kebanyakan atau  sebagian terbesar dari para koruptor (besar dan kecil, di kalangan atas maupun di kalangan bawah) yang melakukan berbagai macam kejahatan di seluruh Indonesia adalah pemeluk agama Islam, atau yang menyatakan diri sebagai orang Muslimin.



Juga tidak bisa disangkal oleh siapapun juga, bahwa yang menjadi korban terbesar kejahatan para koruptor di Indonesia adalah justru pemeluk Islam. Jadi para koruptor mempunyai dosa besar – bahkan terbesar ! – terhadap pemeluk agama Islam, di samping terhadap warganegara lainnya.



Mereka melakukan korupsi (atau berbagai kejahatan lainnya), walaupun tiap hari sembahyang solat lima waktu, atau menjalani puasa, atau sering memberikan zakat fitrah, atau berkali-kali naik haji. Yang sungguh-sungguh  keterlaluan adalah orang-orang  yang rajin sekali sembahyang dan berdoa kepada Tuhan dengan tujuan supaya mereka diselamatkan dari dosa dan hukuman, karena melakukan korupsi.



Puncak kerusakan akhlak atau kebusukan iman



Puncaknya kerusakan akhlak dan kebusukan iman  yang dilakukan oleh para koruptor (yang mengaku dirinya Muslimin) adalah berbagai macam kejahatan korupsi yang dilakukan besar-besaran dan berjumlah besar (dan kecil) dan yang telah berlangsung lama di Departemen Agama.



Bermacam-macam korupsi dalam berbagai tingkat, dan melalui beragam jalur pula,  juga terjadi dalam pengurusan perjalanan haji ke Tanah Suci tiap tahun, yang menyangkut ratusan ribu pemeluk Islam di seluruh Indonesia.



Padahal, para pejabat yang mengurusi soal-soal agama di Departemen Agama dan para pejabat atau tokoh-tokoh yang mengurusi perjalanan haji adalah orang-orang yang menyatakan dirinya sebagai orang saleh yang mengabdi kepada agama dan Tuhan.



Selain itu, sebagian terbesar dari para pembesar atau para tokoh di bidang eksekutif, legislatif, dan judikatif yang melakukan korupsi adalah kebanyakan pemeluk Islam, yang dalam pidato-pidatonya sering diucapkan kalimat-kalimat yang dikutip dari Al Qur’an.



Hal yang demikian tidak saja terjadi di Pemerintahan Pusat, namun juga (bahkan lebih-lebih lagi !!!) di daerah-daerah, dengan adanya pemilihan kepala dan dewan-dewan perwakilan daerah. « Korupsi berjemaah » (atau korupsi beramai-ramai) sudah banyak terjadi di Pemerintahan Pusat dan terutama di daerah-daerah.



Karenanya, kalau kalangan Islam berhasil ikut mengurangi kejahatan korupsi di seluruh negeri, maka jasa mereka bagi negara dan bangsa (dan bagi agama !) akan lebih nyata, lebih kongkrit, dan lebih besar dari pada segala macam kegiatan  mereka  lainnya. Sebab, bisa diartikan bahwa melawan korupsi adalah pada dasarnya menjalankan perintah Tuhan juga.



Bung Karno dan Gus Dur tentunya menyetujui kesepakatan ini



Para koruptor yang telah diperiksa oleh kepolisian, kejaksaan atau pengadilan, pada umumnya, atau sebagian terbesar sekali diantaranya,  telah berjanji atau bersumpah sebagai pemeluk Islam, walaupun nyatanya memberikan keterangan atau pengakuan palsu.



Dalam banyak perkara kejahatan (terutama korupsi) para koruptor pada umumnya memperlihatkan ketidak jujuran dan memperlakukan sumpah atas nama agama dan Tuhan sebagai sampah saja, yang tidak perlu sama sekali dianggap sebagai sesuatu yang serius sama sekali, atau ditakuti.



Mengingat itu semuanya, maka kesepakatan Nahdlatul Ulama dan Muhammadiyah bahwa « koruptor itu kafir » adalah sesuatu yang baik bagi golongan Islam Indonesia, dan juga baik untuk seluruh rakyat pada umumnya, termasuk golongan non-Islam.



Jiwa dan tujuan kesepakatan ini tentulah disetujui oleh Bung Karno dan Gus Dur, seandainya kedua orang besar bangsa  ini masih hidup. Di samping itu, kesepakatan ini bisa juga dianggap  sebagai sentilan atau kritik terhadap pemerintahan yang sekarang dan  para tokoh masyarakat yang kelihatan tidak berdaya, loyo, atau kurang sungguh-sungguh menghadapi korupsi yang telah membikin kerusakan akhlak dan  kebusukan iman secara besar-besaran dan meluas di negeri kita ini.



Dengan kesepakatan kalangan Nahdlatul Ulama dan Muhammadiyah untuk menyatakan « koruptor itu kafir » ini bisa diharapkan sebagai langkah untuk mengurangi merajalelanya korupsi di kalangan  pemeluk Islam, mengingat ancaman akibatnya di akhirat nantinya.



Kalau semua usaha gagal, perlu ditempuh jalan lain : revolusi rakyat



Apakah kesepakatan kedua organisasi Islam tersebut akan bisa betul-betul mengurangi atau ikut memerangi korupsi di kalangan Islam di Indonesia sulit untuk diperkirakan hasil kongkritnya. Namun, sebagai salah satu di antara berbagai macam usaha ke arah itu adalah suatu hal yang dapat dianggap positif yang memberikan harapan.



Kalau ternyata kemudian bahwa buku « Koruptor itu kafir » itupun tidak « mempan «  untuk mengurangi merajalelanya korupsi,  ditambah dengan berbagai banyak usaha lainnya di masa-masa yang lalu oleh Nahdlatul Ulama dan Muhammadiyah, maka ini berarti bahwa larangan agama atau perintah Tuhan mengenai kejahatan sejenis korupsi itu tidak ditakuti lagi oleh banyak pemeluk Islam di Indonesia.



Padahal, negara dan bangsa Indonesia sulit  atau tidak bisa diperbaiki atau dirubah tanpa perbaikan akhlak dan penyehatan iman para pemeluk Islam yang merupakan  majoritas.  Kalau memang demikian halnya mungkin jalan lain perlu ditempuh untuk perbaikan akhlak dan pelurusan iman bagi sebagian terbesar rakyat Indonesia. Jalan lain ini di antaranya adalah jalan revolusi rakyat, seperti yang pernah digerakkan oleh Bung Karno selama bertahun-tahun.



Banyak sekali saksi hidup yang masih bisa menceritakan bahwa selama masa revolusi rakyat di bawah pimpinan Bung Karno, sebagian terbesar rakyat Indonesia –yang terdiri dari pemeluk agama Islam – mempunyai moral yang lebih baik dari pada moral kebanyakan orang di masa sekarang. Pada waktu itu jarang sekali (atau sedikit sekali) terdengar adanya korupsi besar-besaran yang meluas di kalangan pemeluk agama Islam seperti sekarang ini.



Mengingat itu semuanya maka makin jelaslah bahwa Negara Republik Indonesia dan bangsa Indonesia akan menjadi lebih sehat kalau korupsi makin berkurang  atau makin sedikit di kalangan  para pemeluk Islam.



Paris,  26  Agustus 2010                                



A. Umar Said



* * *



Tambahan : Di bawah ini disajikan berita Tempo Interaktif tentang peluncuran buku « Koruptor itu kafir »





NU-Muhammadiyah Sepakat Koruptor itu Kafir

Kamis, 19 Agustus 2010


TEMPO Interaktif, Jakarta - Apa yang terjadi jika dua organisasi Islam besar di negeri ini yaitu Nahdlatul Ulama dan Muhammadiyah berkolaborasi melawan korupsi? Hasil sementara, setidaknya ditunjukkan ketika keduanya menyatukan pikiran dalam sebuah buku: " Koruptor itu Kafir"  
Buku warna hitam dan bersampul obor terbakar dengan tangkai berlapis uang itu, berjudul "Koruptor itu Kafir", Telaah Fiqih Korupsi dalam Muhammadiyah dan Nahdatul Ulama (NU)."


Diterbitkan Mizan dengan pendanaan dari Kemitraan Patnership ini, buku ini diluncurkan Rabu 18 Agustus 2010 di Jakarta, menjelang acara buka puasa. Hadir dalam peluncuran buku itu,  editor buku, Bambang Widjojanto yang juga salah satu kandidat pimpinan KPK. Juga Sekjen Suriyah PBNU Malik Madani.


Dalam siaran persnya, Direktur Eksekutif Kemitraan Wicaksono Sarosa mengatakan, dua ormas besar Islam sepakat membedah beragam dimensi korupsi. Lengkap dengan cara-cara strategis untuk memberantasnya. Baik merujuk pada warisan pemikiran Islam yang ditemukan dalam tradisi fiqih maupun pemahaman hukum kontemporer.


"Buku ini tidak saja menyajikan korupsi dari sisi pandang Muhammadiyah dan NU tapi juga menawarkan beberapa alternatif pemberantasan korupsi di kalangan masyarakat Islam dan bagi negara," ujarnya.


Dalam buku itu, kata Wicaksono, ditegaskan pula kalau tindakan koruptif yang pada dasarnya meletakkan uang di atas segalanya sama saja dengan syirik. Kemitraan juga berharap bahwa penegasan ini dapat menjadi pendorong gerakan sosial anti-korupsi." Kami berharap muncul gerakan sosial untuk menata pemerintahan yang lebih baik" ujarnya
