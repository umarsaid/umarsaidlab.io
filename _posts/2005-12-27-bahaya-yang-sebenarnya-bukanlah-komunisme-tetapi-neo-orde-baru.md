---
layout: post
title: Bahaya yang sebenarnya bukanlah komunisme tetapi neo-Orde Baru
date: 2005-12-27
---

Para pembaca yang terhormat, mohon Anda sekalian membaca dengan cermat berita yang dimuat dalam harian Kompas tanggal 24 Desember 2005, yang antara lain berbunyi sebagai berikut :

“Tanggal 24 Desember 2005 Komando Daerah Militer Jakarta Raya genap berusia 56 tahun. Berkaitan dengan itu, wartawan harian Kompas mewawancarai Panglima Kodam Jaya Mayjen TNI Agustadi Sasongko Purnomo..

“Menurut Panglima Kodam : "Situasi sekarang ini sangat kondusif untuk berkembangnya faham komunisme yang sekarang juga masih banyak dianut sejumlah orang, baik perorangan maupun lewat organisasinya." Kemiskinan, kesulitan hidup, ketidakpuasan pada pemerintah, adalah kondisi masyarakat yang sangat mudah disusupi faham komunisme. Jika ini tidak diantisipasi, dibiarkan terus berkembang, bukan tidak mungkin dalam Pemilu 2009 ada partai berfaham komunis yang ikut berkompetisi.

"Target mereka adalah mencabut semua peraturan yang melarang komunisme. Sesudah itu, organisasi dan partai politik berasas komunis akan segera didirikan."Nah, untuk mengantisipasi hal itu, TNI dengan babinsanya terus berusaha membentengi masyarakat agar tidak mudah terpengaruh komunisme. Karena itu, Panglima Kodam Jaya juga mengimbau agar masyarakat sadar akan bahaya komunisme tersebut ", demikian antara lain Mayjen TNI Agustadi Sasongko Purnomo,( kutipan dari harian Kompas selesai).

* * *

Kalau kita baca isi pernyataan Panglima Kodam Jakarta Raya di atas itu maka kita ingat kepada pernyataan yang sudah sering sekali diucapkan selama puluhan tahun oleh berbagai pembesar militer Orde Baru. Selama 32 tahun, kita sering mendengar adanya « bahaya laten komunis ». Sedangkan, banyak orang orang tahu bahwa rejim militer Suharto sudah menghancurkan - dengan cara-cara kejam sekali – kekuatan PKI, sampai lumpuh sama sekali.

Boleh dikatakan bahwa sebagian terbesar kadernya, baik yang tingkat tinggi, menengah maupun tingkat bawah di seluruh Indonesia telah dibunuhi secara massal, tanpa pengadilan dan dengan cara-cara yang tidak manusiawi atau biadab sekali. Sedangkan jutaan anggota dan simpatisan PKI, juga telah dibunuhi atau dipenjarakan dalam jangka waktu yang lama sekali, walaupun tidak bersalah apa-apa sama sekali.

Walaupun PKI sudah dilumpuhkan sama sekali, selama 32 tahun rejim militer Suharto terus-menerus mengumbar momok “bahaya laten PKI” sebagai alat untuk menterror siapa saja yang berani menentang Orde Baru atau berani bersikap kritis, dan dengan maksud untuk tetap mengkonsolidasi kekuasaannya yang otoriter, kejam dan korup.

Sekarang, setelah Suharto sudah turun dari tahtanya sejak 1998, mulai lagi terdengar slogan-slogan ‘bahaya laten komunis” seperti halnya di masa-masa puluhan tahun Orde Baru. Dan yang terakhir adalah yang diucapkan oleh Pangdam Jakarta Raya, Mayjen Agustadi Sasongko Purnomo.

Jelaslah kiranya bahwa apa yang diucapkan Mayjen Agustadi Sasongko bukanlah pendapatnya pribadi seorang diri, melainkan cermin dari garis politik TNI-AD (yang sebenarnya !) mengenai berbagai soal, termasuk masalah “bahaya laten komunis”. Dari pernyataannya kita dapat memperoleh gambaran bahwa, pada hakekatnya, TNI-AD masih belum sama sekali menjalankan reformasi secara serius dan menyeluruh di kalangannya. Artinya, TNI-AD yang sekarang ini adalah, pada dasarnya, masih seperti TNI-AD di jaman Orde Baru.

Rupanya, dengan menyatakan bahwa “TNI dengan babinsanya terus berusaha membentengi masyarakat agar tidak mudah terpengaruh komunisme” tokoh-tokoh militer semacam Mayjen Agustadi ingin memberikan kesan kepada umum bahwa TNI selama ini adalah “pengayom” atau “pelindung” rakyat.



PENGKHIANATAN TERHADAP BUNG KARNO DAN RAKYAT


Padahal, kalau selama puluhan tahun Orde Baru orang tidak berani bicara tentang berbagai kejahatan dan pelanggaran yang dilakukan golongan militer, sekarang makin banyak orang yang berani mengatakan dengan terus terang bahwa justru TNI-AD di bawah pimpinan Suhartolah yang telah menimbulkan penderitaan bagi puluhan juta para korban peristiwa 65 beserta sanak-saudara mereka, dan juga bagi banyak golongan lainnya di kalangan rakyat Indonesia.

TNI-AD di bawah Suharto-lah yang telah melakukan pengkhianatan besar terhadap Bung Karno, dan melalui pengkhianatan terhadap pemimpin besar bangsa Indonesia ini mereka juga sekaligus mengkhianati revolusi bangsa Indonesia, dan mensabot perlawanan rakyat terhadap imperialisme AS dan neo-kolonialisme.

Dengan mengeluarkan pernyataan seperti tersebut di atas itu, orang mendapat kesan bahwa kesalahan monumental yang dibuat oleh pimpinan TNI-AD di bawah Suharto selama Orde Baru masih mau diteruskan oleh sebagian pimpinan TNI-AD yang sekarang, semacam Mayjen Agustadi Sasongko itu.

Dalam sejarah bangsa Indonesia belum pernah pmpinan TNI-AD (yang dulu-dulu maupun yang sekarang) mengakui - dengan tegas dan terang-terangan - kesalahan dan pengkhianatan yang pernah mereka lakukan terhadap Bung Karno. Mereka juga tidak pernah mengakui kesalahan atau kejahatan dengan membunuhi jutaan orang tidak bersalah dan memenjarakan ratusan ribu tapol dalam jangka yang lama sekali.

Kalau direnungkan dalam-dalam, kesalahan atau kejahatan pimpinan TNI-AD terhadap Bung Karno dan terhadap jutaan anggota dan simpatisan¨PKI, dan kesalahan mereka mendirikan rejim militer Orde Baru adalah dosa yang paling besar selama sejarah bangsa sampai sekarang. Dosa ini makin besar karena itu semua mereka lakukan dengan dukungan atau persekongkolan dengan kekuatan imperialis dan neo-kolonialisme yang dikepalai AS.



MOMOK “BAHAYA KOMUNISME”


Sekarang, ketika gerakan atau aksi-aksi berbagai kalangan atau golongan dalam masyarakat menuntut perbaikan hidup, dan melawan pengangguran, dan menentang merajalelanya korupsi, Mayjen Agustadi mengumbar momok tentang “kondisi masyarakat yang sangat mudah disusupi faham komunisme”.

Dengan ungkapan ini secara tidak langsung ia memperingatkan bahwa di belakang segala macam aksi atau gerakan untuk menuntut perbaikan sosial ekonomi itu berdiri PKI atau faham komunisme. Barangkali, ia pun tahu bahwa, sebenarnya, tidak semua aksi atau gerakan sosial ekonomi yang sekarang di mana-mana itu digerakkan oleh orang-orang yang berfaham komunis saja, melainkan juga oleh orang-orang lainnya yang berkepedulian terhadap nasib rakyat banyak.

Seperti yang dapat diilihat dalam sejarah bangsa kita, memang bisalah dikatakan bahwa kebanyakan orang komunis di Indonesia sangat peduli terhadap masalah kemiskinan dan ketidakadilan dalam segala manifestasinya. Tetapi sikap yang demikian ini bukanlah monopoli orang-orang komunis saja. Juga orang-orang dari kalangan atau golongan lainnya bisa mempunyai sikap yang mulia atau sifat yang luhur ini.

Jadi, salahlah kiranya Mayjen Agustadi kalau ia mengira bahwa segala ketidakpuasan terhadap pemerintah itu ada bahaya “kesusupan faham komunime”. Persoalan parah dan kesulitan serius di bidang politik, ekonomi, sosial, kebudayaan, dan moral yang dihadapi oleh bangsa kita adalah besar dan luas sekali.

Karenanya, yang bergerak dan mengadakan aksi-aksi perlawanan untuk perubahan menuju perbaikanjuga terdiri dari macam-macam orang dari berbagai aliran politik, keyakinan agama, dan kesukuan atau ras.
Bahwa dalam perjuangan bersama melawan musuh-musuh yang sama ini kemudian terdapat persamaan pandangan atau persamaan sikap antara orang-orang dari berbagai golongan dengan pandangan atau sikap orang-orang komunis itu adalah soal yang wajar. Ini tidak terjadi di Indonesia saja, melainkan juga di banyak negeri di dunia.



BAHAYA YANG SEBENARNYA ADALAH NEO-ORDE BARU

Dalam hal yang demikian itu, orang seperti Mayjen Agustadi yang mengimbau “agar masyarakat sadar akan bahaya komunisme tersebut” perlu diingatkan bahwa bahaya bagi Republik Indonesia sekarang ini bukannya komunisme. Bahaya yang jelas-jelas sudah nyata sekarang adalah terrorisme dan munculnya kembali neo-Orde Baru di mana Partai Golkar dan sebagian golongan militer memegang peran aktif.

Rejim militer Orde Baru yang sudah mengangkangi Republik Indonesia selama 32 tahun dan secara nyata telah menimbulkan banyak kerusakan parah dan pembusukan di berbagai bidang (termasuk yang paling parah : bidang moral!!!) adalah bahaya dan musuh yang nyata bangsa kita. Sejarah kita sudah menjadi saksinya !

Barangkali perlu untuk sama-sama kita renungkan dalam-dalam soal yang berikut ini : Apakah momok “bahaya komunis” yang ditiup-tiupkan oleh orang-orang semacam Mayjen Agustadi ini tidak dimaksudkan untuk menakut-nakuti - kasarnya menterror secara psikologis - semua orang dari berbagai kalangan atau golongan yang berani mengeluarkan suara vokal atau kritis sekali dan mengadakan aksi-aksi (atau gerakan) mengenai segala ketidakberesan dan kebusukan yang sedang melanda negeri kita?

Slogan “Bahaya laten komunis” yang selama 32 tahun telah dipakai untuk melakukan terror terhadap orang-orang dari berbagai golongan yang tidak menyukai rejim militernya Suharto dkk ini sudah tidak “mempan” lagi atau tidak “se-ampuh” lagi sewaktu jaman Orde Baru.

Dengan mengumbar lagi slogan lama itu, orang-orang semacam Mayjen Agustadi hanya menjadi cemooh banyak orang. Bisa disamakan seperti pedagang yang gembar-gembor percuma karena menawarkan - dengan suara lantang pula – barang busuk dan usang, yang sudah lama tidak laku karena tidak disukai orang.

Sejak runtuhnya tembok Berlin, dan sejak “selesainya” (secara pokok) Perang Dingin, maka slogan “bahaya komunis” yang juga dalam jangka puluhan tahun diuar-uarkan di luarnegeri sudah hilang dari pasaran. Soviet Uni dan negara-negara Eropa Timur sudah berubah, Tiongkok dan Vietnam juga sudah tidak seperti dulu-dulu lagi.

Negeri-negeri Amerika Latin (antara lain Venezuela, Bolivia, dan juga Kuba sejak lama) sedang megalami perubahan juga, yang tidak menguntungkan imperialisme AS. Melihat perkembangan situasi politik di dunia sekarang ini, maka dapat dikatakan bahwa dalam banyak hal, visi Bung Karno tentang imperialisme dan nekolim, ternyata ada kebenarannya, walaupun sudah 40 tahun berlalu.

Seperti yang bisa kita amati bersama-sama, di seluruh dunia perjuangan rakyat berbagai negeri untuk mencapai perbaikan hidup, melawan ketidakadilan dan penghisapan, terus berlangsung tanpa henti-hentinya. Perjuangan ideologi dan politik dalam bentuknya yang lama (semasa Perang Dingin) sudah digantikan dengan bentuknya yang baru, antara lain lewat perjuangan bersama terhadap neo-liberalisme dan globalisasi ekonomi.

Pada pokoknya, perjuangan rakyat berbagai negeri terhadap neo-liberalisme ini adalah perjuangan terhadap musuh yang lama juga, yaitu imperialisme AS beserta kekuatan-kekuatan pendukungnya yang terdapat dalam IMF, World Bank, WTO dan lain-lain badan yang jadi alat kapitalisme internasional.



REPUBLIK INDONESIA MEMBUTUHKAN TNI-AD YANG ANTI-ORDE BARU

Di Indonesia, dewasa ini sedang berkembang aksi-aksi dan gerakan dari berbagai kalangan dan golongan masyarakat yang memperlihatkan pertanda kebangkitan rakyat untuk memperjuangkan perbaikan hidup dan melawan segala macam ketidakadilan. Benar atau tidak benar pernyataan orang semacam Mayjen Agustadi (tentang bahaya komunisme yang menyusup di belakang aksi-aksi atau gerakan ini) bukan merupakan penghalang bagi tumbuhnya dan meluasnya gerakan perlawanan rakyat ini.

Pernyataan Mayjen Agustadi merupakan pertanda (atau bukti) bahwa TNI-AD yang sekarang - pada pokoknya - masih sama saja dengan TNI-AD di bawah pimpinan Suharto. Yaitu TNI-AD yang menjadi pembangun utama rejim militer Orde Baru, suatu rejim otoriter, yang oleh rakyat sudah dinajiskan dan dibuang dalam keranjang sejarah.

Sebenarnya, ditumbangkannya Suharto oleh generasi muda dengan dukungan rakyat berarti juga ditolaknya oleh rakyat kehadiran sebagian pimpinan TNI-AD yang menjadi kaki-tangan setia diktator korup yang menjalankan praktek-praktek fasis.

TNI-AD yang mendukung Orde Baru inilah yang selama 32 tahun merusak sendi-sendi Republik (republik asal katanya dari bahasa latin res publica, yang artinya untuk rakyat atau untuk umum). Seperti yang kita saksikan bersama-sama, selama puluhan tahun itu TNI-AD di bawah Suharto telah mengubah Republik kita menjadi negara militer dalam negara, atau menjadi maffia militer yang menguasai segala-galanya untuk kepentingan segolongan kecil mereka.sendiri.

Negara kita memang memerlukan TNI-AD, tetapi yang betul-betul anti-Orde Baru ! Artinya, bangsa kita tidak membutuhkan TNI-AD (dan golongan militer lainnya) yang masih secara diam-diam mendukung Suharto atau masih berjiwa Orde Baru !!!
