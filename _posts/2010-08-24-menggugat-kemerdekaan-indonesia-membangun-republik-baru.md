---
layout: post
title: 'Menggugat kemerdekaan Indonesia: membangun republik baru'
date: 2010-08-24
---

Pertemuan «  buka puasa bersama »  oleh Persatuan

Mahasiswa Katolik Republik Indonesia



Menurut berita di « Berdikari Online », website PRD (yang ditulis oleh Data Brainanta), Persatuan Mahasiswa Katolik Republik Indonesia (PMKRI) pada tanggal 18 Agustus 2010 telah menyelenggarakan  « buka  puasa bersama »  berthemakan “Menggugat kemerdekaan Indonesia: membangun republik baru”.

Acara tersebut melibatkan puluhan aktivis pergerakan yang tergabung dalam sejumlah organisasi, antara lain PMII, HMI MPO, IMM, KPRM, SMI, PRD, Repdem dan Serikat Pekerja PLN.

Ilham Syah dari SBTPI dalam kesempatan itu berbicara tentang pentingnya persatuan di antara kelompok pergerakan dan peran utama buruh dalam perjuangan bersama ini.

Sementara itu Binbin (aktivis PRD) berpendapat bahwa kaum pergerakan masih kecil dan perlu  membuka diri terhadap tokoh-tokoh oposisi yang sejalan. Menurutnya, landasan republik baru dapat mengacu pada pembukaan UUD 45.

Seorang pimpinan KPRM, Suryanta, menganalisa bahwa tenaga produktif adalah persoalan utama bangsa ini.“Itu hanya bisa dikoreksi bila imperialisme bisa diusir dari bumi Nusantara.” Menurutnya kaum elit terbukti tidak mampu membawa perubahan yang diharapkan.

Ridwan dari IMM menambahkan bahwa perjuangan ini perlu dilakukan secara bergotong royong, sebagaimana dicontohkan dulu oleh Bung Karno. Ia juga menyitir konsep Trisakti Bung Karno untuk memberikan gambaran lebih detail tentang prinsip-prinsip yang akan melandasi republik Indonesia yang baru.

Ba’itz dari HMI MPO berpendapat bahwa aktivitas kaum pergerakan hendaknya tidak terbatas pada aksi-aksi di jalanan saja untuk merespon isu-isu yang terpisah, tapi juga melakukan kegiatan-kegiatan konsolidasi yang berkelanjutan.

Ketua SP-PLN turut memberikan pendapatnya dengan menawarkan suatu proyek konkrit untuk merespon kenaikan TDL pada 1 Mei tahun depan, sehingga tersedia waktu yang cukup untuk berkonsolidasi dan mempersiapkan mobilisasi.

“Kegiatan seperti ini harus disambung terus, kita harus terus melakukan tukar pemikiran seperti ini dan membiasakan diri menerima perbedaan sambil tetap bekerjasama. Tidak sekedar berebut dan bersaing untuk tampil dalam aksi” jelas Ilham Syah. (Demikian, antara lain, menurut siaran website « Berdikari »)



* * *



Dari berita yang disiarkan »Berdikari » ini, kiranya kita semua dapat melihat adanya berbagai aspek yang menarik dan penting. Di bawah berikut ini adalah sejumlah bahan-bahan, yang di antaranya dapat dianggap sebagai sekadar sumbangan untuk pemikiran kita bersama :

-- Pertemuan dalam rangka « buka puasa bersama » ini bisa mempunyai arti yang jauh jangkauannya. Sebab, pertemuan ini diselenggarakan oleh organisasi mahasiswa. Ini saja sudah mempunyai arti yang penting, sebagai inisiatif yang baik dari golongan generasi muda, yang terdiri dari calon kader-kader bangsa di kemudian hari.

-- Lagi pula, « buka puasa bersama » ini diadakan oleh generasi muda Katolik,  yang juga punya arti yang besar. Kegiatan ini menceminkan sikap yang toleran dan hormat dari golongan Katolik kepada golongan Islam, karena menunjukkan penghargaan kepada salah satu ajaran penting agama Islam.

-- Secara tidak langsung, ini merupakan koreksi atau kritik atau pelajaran bagi golongan Islam fanatik dan fundamentalis yang suka membakari gereja atau mengganggu ibadah golongan Katolik atau golongan agama lainnya.

-- Karenanya, pertemuan ini bisa dipandang sebagai miniatur pentrapan dalam praktek dasar-dasar  negara dan pedoman agung bangsa kita Pancasila dan Bhinneka Tunggal Ika. Sebab, yang hadir dalam « buka puasa bersama » ini adalah dari unsur-unsur golongan atau aliran nasionalis, agama dan kiri (komunis atau sosialis).

-- Pada hakekatnya,  pertemuan ini adalah  pelaksanaan salah satu dari ajaran-ajaran  revolusioner Bung Karno tentang revolusi rakyat Indonesia : menjebol dan membangun,  untuk membangun republik baru.

-- Thema « buka puasa bersama » itu juga mengandung message (pesan)  yang besar «  Menggugat kemerdekaan Indonesia : membangun republik baru » Artinya, menggugat kemerdekaan yang sudah ke 65 tahun, tetapi masih banyak yang terbengkalai, rusak,  kacau-balau, busuk, dan kehilangan arah.

-- « Membangun republik baru », berarti bahwa Republik Indonesia  yang sekarang harus dibongkar, atau dirobah, atau dijebol, atau diperbaiki dan diganti dengan republik baru. Ini berarti perubahan besar-besaran atau penggantian yang mendasar, yang drastis, yang fundamental.

-- Hadirnya wakil-wakil gerakan buruh/pekerja dalam buka puasa bersama ini merupakan persatuan atau kerjasama  di antara gerakan generasi muda dengan gerakan buruh/pekerja. Kalau pola ini bisa dikembangkan lebih luas lagi di seluruh negeri, maka akan merupakan sumbangan yang besar sekali dalam pembentukan  front untuk membangun republik baru.

-- Dalam pertemuan ini dikutip berbagai ajaran Bung Karno, antara lain pentingnya Trisakti (berdaulat dalam politik, berdikari dalam ekonomi, dan berkepribadian dalam kebudayaan),  jiwa gotong royong, dan perjuangan terhadap imperialisme.

-- Disebutkannya ajaran-ajaran revolusioner Bung Karno dalam pertemuan itu berarti, secara langsung atau tidak langsung, sebagai penolakan terhadap jiwa Orde  Barunya Suharto (bersama sekutu-sekutunya kaum reaksioner di dalam dan luar negeri).

-- Semangat kerjasama atau saling pengertian dalam perjuangan yang tercermin dalam buka puasa bersama yang diselenggarakan  PMKRI dengan berbagai wakil kelompok pergerakan ini perlu sekali diketahui oleh masyarakat seluas-luasnya di seluruh negeri.



Mengingat itu semua, nyatalah bahwa inisiatif PMKRI (Persatuan Mahasiswa Katolik Republik Indonesia) untuk mengadakan « buka puasa bersama » dengan berbagai organisasi generasi muda, organisasi buruh/pekerja dan golongan lainnya baru-baru ini adalah peristiwa penting ketika negara dan bangsa sedang mengalami berbagai kemunduran,  kemerosotan atau keterpurukan  seperti yang sama-sama sedang kita saksikan dewasa ini.

Tekad atau kemauan generasi muda  untuk mengajak berbagai golongan dalam  menggugat kemerdekaan yang sudah ke-65 tahun dan menyerukan perjuangan bersama untuk membangun  republik baru adalah phenomena yang menggembirakan bagi semua kalangan dan golongan yang menginginkan  perubahan.

Membangun republik baru merupakan  slogan perjuangan yang besar, yang dipersembahkan oleh generasi muda kepada bangsa. Kalau seruan ini dilakukan dengan mentrapkan ajaran-ajaran revolusioner Bung Karno maka tidak akan merusak jiwa dan tujuan Republik Indonesia, bahkan sebaliknya ( !!!)  akan membersihkannya dan memperkuatnya, dengan melaksanakan sungguh-sungguh Pancasila dan  Bhinneka Tunggal Ika.

Karena itu, Membangun republik baru, bisa menjadi juga program besar bagi seluruh kekuatan demokratis di Indonesia, terutama bagi semua generasi muda dan golongan lainnya yang menginginkan adanya perubahan atau  pembaruan besar-besaran dan fundamental di negara kita. Program besar bersama ini adalah untuk mencapai tujuan Proklamasi 17 Agustus 45, yang asli dan semurni-murninya, dan yang tidak diselewengkan, dikotori atau dikhianati.

Dengan begitu, maka  « Membangun republik baru » pada hakekatnya adalah sama dengan melaksanakan ajaran revolusioner  Pemimpin Besar Revolusi (PBR) Bung Karno tentang meneruskan  revolusi rakyat, yang pada pokoknya berarti « menjebol dan membangun (destruksi dan konstruksi) dan « revolusi belum selesai ».
