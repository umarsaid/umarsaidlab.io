---
layout: post
title: Ajakan renungan tentang rehabilitasi korban 65
date: 2006-01-10
---

Tulisan berikut ini merupakan dukungan kepada petisi tentang rehabilitasi umum bagi para korban peristiwa 65 yang telah disampaikan hampir sebulan yang lalu (13 Desember 2005) oleh LPR-KROB, LPKP dan Pakorba, kepada Presiden SBY dan Ketua DPR RI Agung Leksono. Sekaligus juga merupakan ajakan kepada para penyelenggara negara dan semua kalangan atau golongan dalam masyarakat kita untuk ikut merenungkan berbagai aspek dari masalah yang merupakan aib besar dan dosa berat bagi Republik Indonesia yang kita cintai bersama ini.

Petisi tentang rehabilitasi para korban peristiwa 65 ini, sekali lagi mengingatkan kita semua, bahwa di tengah-tengah banyaknya urusan-urusan politik-sosial-ekonomi yang rumit dan merajalelanya korupsi dewasa ini, ada soal yang tidak bisa – dan tidak boleh ! – dilupakan atau diremehkan begitu saja, yaitu masalah rehabilitasi para korban peristiwa 65.

Adalah penting sekali disadari oleh para tokoh negara kita (termasuk pimpinan TNI-AD) dan para pemuka golongan dalam masyarakat (termasuk tokoh-tokoh Islam) bahwa rehabilitasi para korban peristiwa 65 ini tidak hanya untuk kepentingan para korban peristiwa 65 melulu, atau untuk kepentingan para eks-tapol beserta keluarga mereka saja, atau untuk kepentingan para mantan anggota PKI beserta simpatisan-simpatisannya saja. Bukan, tidak hanya terbatas untuk kepentingan mereka saja, melainkan lebih dari itu semua !!! Jangkauannya lebih jauh dan lebih luas, dan dimensinya pun lebih besar.

Rehabilitasi para korban peristiwa 65 adalah justru untuk kepentingan seluruh bangsa. Sebab, setiap orang yang mempunyai nalar sehat tentu akan mengakui bahwa masih adanya begitu banyak orang (yang jumlahnya puluhan juta) yang menanggung berbagai penderitaan berkepanjangan sebagai akibat kesalahan atau kejahatan rejim militer Orde Baru adalah sesuatu yang tidak baik sama sekali bagi kehidupan bangsa kita. Kiranya, hanya orang-orang yang berotak tidak waras sajalah yang bisa bangga atau gembira dengan perlakuan yang begitu tidak manusiawi itu.

KESALAHAN YANG TIDAK MENGUNTUNGKAN SIAPA PUN

Keanehan, atau kejanggalan, atau ke-tidak-normalan memang telah berlangsung sejak lama di Republik kita. Sekitar 3 juta orang tidak bersalah telah dibunuhi, dan ratusan ribu pernah ditangkapi atau ditahan secara sewenang-wenang. Tetapi, orang-orang yang seharusnya bertanggung jawab terhadap pembunuhan dan penahanan begitu banyak orang itu sampai sekarang tidak pernah ditindak menurut hukum. Bahkan, banyak di antara mereka yang menduduki jabatan-jabatan penting dalam berbagai badan pemerintahan atau jadi tokoh masyarakat.

Pembunuhan besar-besaran terhadap sekitar 3 juta orang tidak berdosa apa-apa , dan penahanan ratusan ribu orang (termasuk lebih dari 10 000 di Pulau Buru) adalah kesalahan serius sekali atau kejahatan besar yang telah dilakukan oleh TNI-AD di bawah pimpinan Suharto. Kesalahan yang begitu serius atau kejahatan yang begitu besar ini tidak mudah dilupakan begitu saja oleh banyak orang yang mempunyai nalar sehat dan iman yang benar, atau yang memiliki rasa keadilan dan perikemanusiaan

Mohon para pembaca ikut merenungkan masalah yang berikut ini : apakah, sebenarnya, negara dan bangsa kita diuntungkan dengan adanya puluhan juta orang para korban peristiwa 65 yang terus-menerus menanggung berbagai penderitaan sampai sekarang? Atau, dengan kalimat lain, apakah keuntungannya bagi negara maupun bagi masyarakat luas untuk membikin berbagai penderitaan terhadap puluhan juta orang terus-menerus begitu lama ? Tidak ada sama sekali, sekali lagi, sama sekali tidak !!!!! (tanda seru lima kali). Bahkan, sebaliknya.

Masih adanya begitu banyak undang-undang, peraturan pemerintah, atau ketentuan-ketentuan lainnya ( sekitar 30) , yang ditujukan terhadap para korban peristiwa 65 merupakan aib besar atau cacad yang monumental bagi negara dan bangsa kita, yang katanya menjunjung tinggi-tinggi peradaban dan kemanusiaan, serta menghormati berbagai piagam PBB.


MEMBIARKAN KESALAHAN BERLANGSUNG 40 TAHUN

Belum direhabilitasinya para korban peristiwa 65 berarti bahwa politik yang otoriter, yang tidak bermanusiawi ( atau biadab), yang dibikin rejim militer Suharto dkk masih diteruskan sampai sekarang. Padahal, resminya Orde Baru sudah jatuh dengan runtuhnya kekuasaan Suharto sejak 1998. Mestinya, atau seharusnya, dengan runtuhnya Orde Baru berkat perjuangan generasi muda dengan mendapat dukungan dari rakyat, ikut terbuang juga segala produk yang busuk bikinan rejim militer ini. Termasuk segala undang-undang dan peraturan busuk yang ditujukan kepada para korban peristiwa 65.

Adalah omong-kosong orang-orang yang tidak waras fikirannya, yang mengatakan bahwa bangsa kita adalah bangsa yang beradab dan beriman, kalau selama 40 tahun (!!!) bangsa ini membiarkan puluhan juta manusia menderita, hanya sebagai akibat dari kejahatan sekelompok pemimpin-pemimpin militer (khususnya TNI-AD) yang anti Bung Karno dan anti-PKI dan bersekongkol dengan imperialisme AS.

Sama sekali tidak ada segi positifnya bagi kalangan atau golongan yang tadinya mendukung Orde Baru, yang anti Bung Karno, atau anti-PKI, atau anti-golongan kiri lainnya, untuk meneruskan kesalahan serius atau kejahatan besar yang sudah dilakukan selama puluhan tahun itu. Juga tidak ada keuntungannya sedikit pun bagi siapa pun juga, untuk meneruskan penderitaan atau siksaan yang sudah 40 tahun dialami oleh begitu banyak orang tidak bersalah apa-apa.

Adalah fikiran yang keliru sama sekali, kalau mengira bahwa dengan terus mempertahankan sekitar 30 undang-undang dan peraturan yang diskriminatif terhadap para korban peristiwa 65 ini maka faham komunis akan terbendung atau terberantas. Dan juga, adalah fikiran yang salah sama sekali, kalau mengira bahwa dengan terus-menerus membikin berbagai penderitaan terhadap puluhan juta korban peristiwa 65, maka “bahaya laten PKI” (yang tidak ada itu) bisa diberantas.

Kiranya perlu disadari oleh mereka yang selama ini anti-PKI dan anti Bung Karno bahwa dengan membikin penderitaan dan sakit hati puluhan juta para korban peristiwa 65 beserta sanak-saudara mereka – terus-menerus dan dalam jangka lama pula – tidak membikin mereka yang telah diperlakukan sewenang-wenang ini menjadi orang-orang yang menghormati para pendukung Orde Baru.


MEREKA TIDAK PERNAH BERSALAH APA-APA

Selama para korban peristiwa 65 (beserta keluarga mereka) tidak atau belum direhabilitasi, sebenarnya kita tidak bisa atau tidak pantas menganggap negara dan pemerintahan kita sebagai negara yang menjunjung tinggi-tinggi demokrasi dan berpedoman Panca Sila atau berjiwa Bhinneka Tunggal Ika.

Selama sisa-sisa kesalahan besar atau kejahatan rejim militer Orde Baru ini belum dihapus, tidak bisa disalahkan bahwa sebagian besar rakyat Indonesia akan tetap menaruh sakit hati atau menyimpan dendam atas penderitaan yang berpuluh-puluh tahun sudah mereka alami. Adalah wajar, dan adalah juga manusiawi, kalau para korban peristiwa 65 itu merasa sakit hati berkepanjangan terhadap para pendukung rejim militer Orde Baru yang telah membikin penderitaan selama puluhan tahun, sedangkan mereka tidak pernah berbuat salah apa-apa terhadap siapa pun juga.

Sebaliknya, yang tidak wajar dan tidak manusiawi adalah kalau bersikap menerima saja, atau pasrah saja, atau diam seribu bahasa saja, walaupun sudah diperlakukan tidak adil atau dibikin menderita selama puluhan tahun secara sewenang-wenang. Sebab, mereka itu tidak pernah melakukan kesalahan apa-apa terhadap negara dan bangsa. Mereka juga tidak berdosa sedikit pun terhadap siapa pun.

Adalah hak yang sah dan benar bagi setiap orang untuk protes atau melawan dengan berbagai cara kalau diperlakukan – sekali lagi, selama 40 tahun !!! - secara tidak adil, didiskriminasi, dirampas hak-haknya yang fundamental sebagai manusia, dianggap warganegara kelas dua atau tidak dianggap sama sekali sebagai warganegara normal. Melakukan perlawanan, dengan berbagai cara, terhadap ke-tidak-adilan atau kejahatan yang sedemikian serius ini adalah benar, adil, dan sah, dan merupakan sikap yang luhur dan juga kewajiban yang mulia.

Setiap orang yang waras fikirannya, atau sehat nalarnya, atau benar imannya, atau bersih hatinya, atau jernih pandangannya, tentu tidak setuju dengan perlakuan yang begitu kejam terhadap begitu banyak orang yang tidak bersalah apa-apa, dan dalam jangka waktu yang begitu lama !
Dengan kalimat lain, setiap orang yang sehat nalarnya dan jalan benar imannya pasti tidak senang melihat adanya ketidakadilan dan kebiadaban yang telah dilakukan dalam jangka waktu yang begitu panjang, oleh rejim militer Orde Baru, dan yang masih diteruskan sampai sekarang.

.
TIDAK MERUPAKAN PENDIDIKAN BAGI GENERASI MUDA

Belum direhabilitasinya jutaan orang tidak bersalah yang - telah secara keliru dan sewenang-wenang !!! - “dihukum” oleh rejim militer Suharto dkk itu tidak merupakan pendidikan untuk bangsa dan generasi muda kita. Kalau para korban peristiwa 65 tidak direhabilitasi maka bangsa kita dan generasi muda kita mengira bahwa perlakuan biadab Orde Baru terhadap mereka itu adalah benar dan juga adil. Kalau sampai demikian, maka, sungguh, ini merupakan malapetaka yang besar bagi hari kemudian bangsa kita.

Sebab, sekali lagi, bagi semua orang yang bernalar sehat, atau yang beriman secara benar, atau yang beradab dan manusiawi, adalah sudah jelas bahwa apa yang dilakukan oleh rejim militer Suharto dkk terhadap para korban peristiwa 65 beserta para sanak-saudara mereka adalah kejahatan besar yang merupakan aib dan dosa besar para tokoh negara kita. Seperti kita ketahui, rejim militer Orde Baru sudah banyak sekali melakukan kesalahan atau kejahatan selama mengangkangi negeri kita puluhan tahun. Tetapi di antara banyak kesalahan (atau kejahatan) itu yang paling besar adalah kesalahan (tepatnya kejahatan) membunuhi dengan sewenang-wenang jutaan orang, memenjarakan ratusan ribu orang tidak bersalah, dan membikin tergulingnya Presiden Sukarno.

Adalah masuk di akal setiap orang yang waras otaknya dan benar imannya bahwa kejahatan yang begitu besar dan serius itu tidak mudah dilupakan begitu saja oleh orang-orang (sekali lagi : yang tidak bersalah apa-apa !!!) yang telah dibikin menderita puluhan tahun. Dan adalah juga wajar-wajar saja bahwa banyak di antara para korban peristiwa 65 itu yang menaruh dendam dan sakit hati kepada mereka yang telah melakukan kejahatan besar selama waktu yang begitu panjang. Kebencian atau dendam terhadap ketidak-adilan yang begitu menyolok sekali itu adalah benar.


HILANGKAN SEGERA SUMBER DENDAM INI!

Kalau direntangpanjangkan, atau diurut-urut lebih lanjut, rehabilitasi umum bagi korban peristiwa 65 akan mendatangkan banyak kebaikan bagi kehidupan bangsa dan negara kita. Tidak bisa disangkal, bahwa ada semacam perasaan sakit hati atau dendam yang mendalam di kalangan para korban peristiwa 65 ( baik orang-orang komunis atau yang bersimpati kepada PKI maupun yang non-komunis pendukung Bung Karno) terhadap para pendukung rejim militer Orde Baru.

Perasaan sakit hati atau dendam ini tidak hanya karena kejadian-kejadian yang berkaitan dengan G30S saja, melainkan juga karena serentetan panjang praktek-praktek yang tidak bermanusiawi selama 32 tahun Orde Baru. Perasaan sakit hati atau dendam ini wajar, sah, dan bisa dimengerti oleh nalar yang sehat. Tetapi, terus dilestarikannya sakit hati dan dendam ini sama sekali tidak menguntungkan bagi kehidupan bangsa, dan tidak mendorong adanya persatuan dan rekonsiliasi, yang sangat dibutuhkan oleh negara dan bangsa, untuk bisa menangani berbagai persoalan rumit dewasa ini dan di masa yang akan datang.

Jelaslah kiranya bahwa sumber sakit hati atau dendam ini adalah berbagai tindakan pimpinan TNI-AD yang berkaitan dengan peristiwa G30S, yang diteruskan sampai sekarang oleh para pendukung Orde Baru. Karena itu, rehabilitasi umum bagi para korban peristiwa 65 beserta sanak-saudara mereka, dan dihapuskannya segala undang-undang atau peraturan busuk yang ditujukan kepada mereka, merupakan langkah besar dan penting untuk meghapuskan sakit hati dan dendam ini.

Oleh karena itu semua, jelaslah bahwa persoalan rehabiltasi puluhan juta korban peristiwa 65 ini bukan hanya urusan para korban saja, melainkan urusan seluruh bangsa seluruhnya, baik yang sekarang maupun yang akan datang. Rehabilitasi para korban peristiwa 65 juga merupakan koreksi yang amat penting terhadap salah satu di antara berbagai kesalahan berat rejim militer Orde Baru.

Kita perlu berusaha bersama-sama supaya kesalahan serius dan kejahatan besar Suharto (beserta para pendukung setianya) tidak terulang lagi, oleh siapa pun, bagaimana pun, kapan pun, dalam bentuk apa pun, dan dengan dalih apa pun.! Karena, korban sudah terlalu banyak, dan kerusakan juga sudah terlalu besar.

(Catatan akhir : copy tulisan yang berupa ajakan renungan ini dapat dikirimkan oleh siapa saja kepada para tokoh negara dan masyarakat kita)
