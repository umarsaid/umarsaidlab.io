---
layout: post
title: Pejuang Perempuan Dan Pemimpin Gerwani Umi Sardjono Meninggalkan Kita
date: 2011-03-16
---

Beberapa waktu yang lalu, setelah membaca berita bahwa mantan Ketua Umum Gerwani (Gerakan Wanita Indonesia), Umi Sardjono, telah wafat dalam usia 88 tahun, maka terbayang kembalilah dalam ingatan saya peristiwa-peristiwa dalam tahun 1945, ketika saya masih berumur 17 tahun.

Saya ingat kembali bahwa saya sudah pernah bertemu dengannya dalam tahun 1945 itu, dalam situasi yang penuh gelora proklamasi 17 Agustus. Hal ini saya ceritakan dalam otobiografi « Perjalanan hidup saya » yang diterbitkan di Jakarta dalam tahun 2004, dalam halaman 37 dan 38.

Dalam halaman-halaman itu ditulis : « Ketika 17 Agustus diproklamasikan di Jakarta oleh Bung Karno dan Bung Hatta dalam tahun 1945  saya ada di Kediri. Pada masa-masa selanjuntnya terjadilah peristiwa-peristiwa yang hanya bisa saya ingat samar-samar sekarang ini :  peristiwa perlucutan senjata tentara Jepang di berbagai tangsi di daerah keresidenan  Kediri dan berbagai badan perjuangan  melakukan kegiatan-kegiatan.

Tetapi, masih jelas dalam ingatan saya, bagaimana saya ikut, bersama-sama  dengan banyak orang lainnya, beramai-ramai menuju rumah penjara Kediri, untuk membebaskan orang-orang yang ditahan oleh Jepang. Saya tahu bahwa paman saya, Boeamin, ditahan oleh Kenpeitai Jepang (polisi militer ) di penjara ini.

Paman Boeamin adalah seorang yang pandai bahasa Jepang dan pernah menjadi jurubahasa Jepang untuk Kenpeitai di Blitar. Entah bagaimana, pada tahun 1943-1944 diketahui oleh Kenpeitai bahwa ia termasuk gerakan di bawah tanah untuk menentang Jepang. Ia disiksa secara kejam, digantung dan disuruh minum air sabun banyak-banyak.

Karena itu, ketika ada orang-orang di kalangan KNI (Komite Nasional Indonesia) Kediri berbicara untuk membebaskan tahanan-tahanan Jepang dari rumah penjara Kediri, saya ikut. Saya masih ingat, bahwa selain paman Boeamin, banyak tahanan-tahanan Jepang lainnya yang telah dikeluarkan pada saat itu.

Salah seorang di antara tahanan itu terdapat wanita yang waktu itu masih muda, yaitu yang bernama Umi Sardjono (Ia menjabat sebagai pimpinan GERWANI sampai terjadinya G30S dalam  tahun 1965). Pidatonya yang bersemangat dan berapi-api di depan orang banyak, yang berkerumun di depan pintu penjara, sangat mengesankan bagi saya  Mungkin ini juga merupakan faktor bagi perkembangan pikiran-pikiran saya di kemudian hari. » (kutipan dari otobiografi « Perjalanan hidup saya » selesai)

Pengaruh pidato Umi Sardjono bagi pandangan hidup saya

Sekarang ini, dalam usia hampir 83 tahun dan hidup di Paris, ketika mengingat-ingat kembali peristiwa-peristiwa masa remaja saya di Kediri  (dan Blitar), terasa sekali bahwa kasus dipenjarakannya Umi Sardjono  (dan paman saya Boeamin) oleh tentara pendudukan Jepang dan pidatonya yang sangat mempesonakan di depan pintu penjara menimbulkan dampak yang dalam terhadap fikiran-fikiran saya kemudian.

Hal yang demikian itu adalah wajar. Karena,  wakltu itu saya baru berumur 17 tahun, dan gelora perjuangan yang dicetuskan proklamasi 17 Agustus membakar dada dan kepala banyak pemuda waktu itu. Mungkin sekali bahwa kasus Umi Sardjono (dan kasus paman saya Boeamin) memang betul-betul kemudian menjadi pendorong perjalanan hidup saya yang berliku-liku.

Karena, tidak lama kemudian saya bersama-sama belasan pelajar-pelajar SMP Kediri dan SGL (Sekolah Guru Laki-laki) Blitar berangkat ke Surabaya pada tanggal 9 November untuk menggabungkan diri dalam Tentara Republik Indonesia Pelajar (TRIP) dan ikut dalam pertempuran 10 November yang terkenal itu. Dan sejak itu mulailah kehidupan saya yang penuh dengan pergolakan dan perjuangan, yang saya tulis dalam buku otobiografi saya itu.

Umi Sardjono sejak belia sudah berjuang untuk kemerdekaan bangsa

Dengan wafatnya Umi Sardjono, maka terbuka kesempatan untuk mengenang kembali kebesaran sosoknya sebagai pejuang untuk kemerdekaan bangsa kita dan sebagai tokoh utama GERWANI. Bagi saya, kenyataan bahwa ia sudah dipenjarakan oleh tentara fasis Jepang (sebelum proklamasi 17 Agustus) karena ikut gerakan di bawah tanah menentang Jepang adalah satu indikasi yang kuat bahwa ia termasuk pejuang yang dini untuk kemerdekaan bangsa, jauh lebih dulu dari pada banyak “pejuang-pejuang” (harap perhatikan tanda buka dan tutup), termasuk Suharto sendiri atau jenderal-jenderal Angkatan Darat lainnya.

Umi Sardjono ditangkap dan dipenjarakan oleh tentara fasis Jepang bersama banyak orang-orang lainnya, yang melakukan gerakan di bawah tanah, yang kebanyakan terdiri dari anggota-anggota atau simpatisan PKI.  Pada waktu itu, gerakan  di bawah tanah yang dilakukan di bawah pimpinan PKI banyak terdapat di Jawa Timur, terutama di daerah Blitar dan Kediri. Salah satu tokohnya  adalah Pak Nata, seorang yang sejak jaman kolonial Belanda sudah melakukan kegiatan  dengan menyamar sebagai pengusaha minuman Sarsaparila di jalan Herenstraat (sekarang Jalan Merdeka) di tengah-tengah kota Blitar.

Apakah Umi Sardjono pada waktu pendudukan tentara fasis Jepang sudah menjadi anggota PKI atau hanya simpatisan saja, tidak diketahui dengan pasti. Namun, bahwa ia telah dipenjarakan ketika masih muda belia (sekitar 22 tahun) karena ikut melakukan gerakan di bawah tanah adalah satu indikasi bahwa ia termasuk salah satu di antara orang-orang yang pantas (dan, bahkan, sebenarnya  berhak !!!) disebut sebagai perintis kemerdekaan bangsa kita. Ini sudah dibuktikan melalui penderitaannya atau kesengsaraannya dalam penjara Jepang. Dan  orang-orang yang seperti Umi Sardjono tidaklah sedikit jumlahnya di Indonesia.

Perlakuan rejim Suharto terhadap Umi Sardjono dan GERWANI

Kalau mengingat itu semua, dan kemudian memperhatikan perlakuan rejim militer Suharto terhadap Umi Sardjono dan organisasi perempuan GERWANI yang dipimpinnya sesudah terjadinya G30S, maka bisa dimengertilah bahwa banyak orang mengatakan  -- dengan kemarahan besar atau emosi yang meluap-luap – bahwa penguasa militer Suharto dengan para jenderal yang mendukungnya adalah sekelompok oknum-oknum yang merupakan sampah bangsa, atau penyakit parah yang membikin rusaknya jiwa banyak orang Indonesia.

Perlakuan terhadap GERWANI dan Umi Sardjono, pimpinan tertinggi organisasi perempuan yang terbesar di Indonesia (sekitar 1,5 juta anggotanya di seluruh Indonesia) dan tokoh yang terkenal sebagai  pendukung politik Bung Karno, adalah sekelumit kecil saja dari dosa-dosa  rejim militer Orde Baru. Dosa-dosa besar ini tidak bisa – dan tidak boleh !!! – dilupakan sama sekali oleh bangsa Indonesia berikut generasi-generasi yang akan datang.

Umi Sardjono ditangkap dan dipenjarakan belasan tahun setelah terjadinya G30S, tanpa putusan pengadilan, tanpa kesalahan apa-apa, seperti halnya puluhan ribu pengurus GERWANI di seluruh daerah Indonesia. Banyak di antara mereka ini yang telah dibunuh begitu saja, atau ditahan dan disiksa (termasuk diperkosa). Sebagian dari tokoh-tokohnya dipenjara bertahun-tahun  (bahkan banyak yang sampai belasan tahun) berpindah-pindah di Cipinang, Bukitduri, Plantungan, dan berbagai penjara atau tempat tahanan (di Kodim-kodim) di seluruh Indonesia.

Penderitaan para anggota atau simpatisan Gerwani, akibat penyiksaan secara fisik dan mental secara besar-besaran dan berjangka lama ini dapat disimak dalam berbagai tulisan atau dokumen yang sudah beredar selama ini. Dengan menyimak bahan-bahan itu,  -- yang sebagian juga disajikan dalam website http://umarsaid.free.fr/ -- nyatalah bahwa para penguasa rejim militer Suharto adalah betul-betul merupakan orang-orang yang berakhlak rendah, atau oknum-oknum yang sama sekali tidak berhak dan juga tidak pantas menyebutkan diri mereka sebagai orang-orang yang beradab dan bermanusiawi.

Cerita-cerita yang serba bohong tentang GERWANI

Orde Baru telah selama 32 tahun secara sistematis, terus-menerus, dan secara luas serta berjangka panjang (!!!) , menyebar banyak sekali kebohongan dan segala macam fitnah, untuk menimbulkan kebencian masyarakat luas terhadap GERWANI (dan PKI). Di antara kebohongan atau fitnah itu adalah bahwa GERWANI terlibat dalam G30S. Padahal, banyak bukti-bukti yang menunjukkan dengan gamblang sekali bahwa kebanyakan pengurusnya atau anggota-anggotanya – baik di tingkat nasional maupun daerah – tidak ada yang tersangkut (sedikit pun !!!)  dalam G30S. Bahkan, mengetahuinya sedikit pun juga tidak !

Dalam jangka yang lama sekali koran-koran dan majalah jaman  Orde Baru menyiarkan cerita-cerita gila, atau isapan jempol, atau dongeng-dongeng tentang GERWANI, umpamanya bahwa organisasi perjuangan ini adalah gerakan perempuan yang a-susila atau menganjurkan sex bebas  serta pelacuran. Banyak cerita yang dikarang-karang tentang anggota GERWANI yang ikut mencukil mata, bahkan memotong kemaluan atau menyayat-nyayat para jenderal yang terbunuh dengan silet.

Cerita gila dan fitnah yang keterlaluan busuknya dan juga yang pernah banyak beredar (dan terkenal !) adalah upacara pesta gembira di Lubang Buaya dengan terbunuhnya para jenderal. Dalam upacara yang diberi nama “Harum Bunga” ini anggota-anggota GERWANI menari-nari dan menyanyi-nyanyi  dengan telanjang !!! Sudah jelaslah kiranya bahwa segala kebohongan yang begitu besar dan semua fitnah yang begitu busuk itu tentunya tidak bisa datang dari fikiran orang-orang yang sehat jiwanya atau waras nalarnya.

Rehabilitasi untuk Umi Sardjono dan GERWANI

Sekarang Umi Sardjono sudah wafat. Terakhir ia  tinggal di rumah seorang sahabatnya yang merawatnya, karena satu kakinya lumpuh. Umi Sardjono pernah ditahan di penjara di Bukitduri (Jakarta) sampai belasan tahun, seperti halnya banyak pimpinan GERWANI lainnya. Meskipun tidak bersalah apa-apa sama sekali. Sejak dibebaskan dari tahanan di penjara Bukitduri sampai wafatnya ia terpaksa hidup dengan berbagai penderitaan.

Perlakuan yang tidak manusiawi, yang melanggar HAM, yang bertentangan dengan peradaban, yang melanggar hukum, yang semacam yang dialami Umi Sardjono inilah yang perlu diangkat atau dipersoalkan dan dihujat terus-menerus oleh sebanyak mungkin dari berbagai kalangan di Indonesia dewasa ini, dan untuk selanjutnya.

Sebab, bangsa kita tidak bisa disebutkan sebagai bangsa yang beradab, bangsa yang menghargai peri kemanusiaan, bangsa yang menjunjung keadilan, bangsa yang berpedoman Pancasila, kalau membiarkan terus kasus Umi Sardjono seperti angin lalu saja. Sebab, kasus Umi Sardjono hanyalah satu kasus saja dari jutaan kasus yang serupa atau sejenis yang dialami oleh banyak orang di Indonesia. Artinya, orang-orang yang tidak bersalah apa-apa sama sekali namun diperlakukan sewenang-wenang (antara lain ditangkapi, disiksa, diperkosa, dipenjara  bahkan dibunuh !!!) hanya karena mereka mempunyai sikap kiri atau pro-PKI. Apakah yang demikian ini negara hukum ???

Umi Sardjono adalah pejuang politik yang sudah dipenjarakan karena memperjuangkan kemerdekaan bangsa Indonesia. Ia kemudian menjadi pimpinan GERWANI dan sering berkunjung ke Moskou, Berlin, Praha, Peking dan kota-kota lainnya. Saya pernah bertemu dengannya di Paris ketika ia menjadi salah satu dari anggota rombongan Presiden Sukarno. Ia tokoh nasional yang terkemuka dalam gerakan perempuan di Indonesia. Sungguh besar kesalahan rejim Suharto, dan sungguh berat dosa-dosa para pendukungnya, karena memperlakukan Umi Sardjono (dan kawan-kawannya di GERWANI), dengan cara-cara yang membikin noda besar dalam sejarah  bangsa Indonesia.

Para penguasa rejim militer Orde Baru (termasuk tokoh-tokoh GOLKAR) harus bertanggung-jawab atas kesalahan atau kejahatan yang begitu besar, dan menebus dosa-dosa mereka. Antara lain dengan minta ma’af, dan mengakui kesalahan mereka, dan berusaha – dengan macam-macam cara dan segala jalan – untuk merehabilitasi Umi Sardjono dan GERWANI serta memberi kompensasi selayaknya kepada ùereka. Sekarang masih belum terlambat! Karena, meskipun sudah banyak yang wafat, namun masih banyak yang tetap hidup, walaupun dengan macam-macam penderitaan.

Dengan merehabilitasi Umi Sardjono dan GERWANI, maka kita bisa mendudukkannya kembali pada tempatnya yang  seharusnya  sebagai organisasi perempuan Indonesia yang patut jadi kebanggaan seluruh bangsa Indonesia.

Rehabilitasi GERWANI adalah satu langkah penting menuju rehabilitasi gerakan kiri Indonesia pada umumnya. Dan rehabilitasi gerakan kiri Indonesia adalah penting untuk kebaikan bangsa sebagai keseluruhan. Gerakan kiri adalah asset bangsa Indonesia yang sangat diperlukan, seperti yang sudah ditunjukkan   --  dengan gamblang sekali !!! --  oleh  era di bawah kepemimpinan Bung Karno.
