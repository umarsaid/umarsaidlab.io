---
layout: post
title: Hari Pahlawan dan para pengkhianat
date: 2003-11-06
---

Seperti setiap tahun, kita akan merayakan lagi Hari Pahlawan 10 November. Dengan sedih, dapat sama-sama saksikan bahwa sejak Orde Baru berkuasa peringatan atau perayaan Hari Pahlawan seolah-olah makin kehilangan semangatnya, kurang gregetnya, atau makin pudar cahayanya. Makin berkurang jumlah orang yang betul-betul menghayatinya. Banyak orang sudah tidak acuh tak acuh saja. Lebih banyak lagi orang yang tidak mengerti apa itu arti Hari Pahlawan. Betul, Hari Pahlawan selama itu memang tetap dirayakan dengan berbagai acara atau beraneka upacara, tetapi tanpa jiwa atau miskin isi.

Tidak bisa lain. Sebab, Orde Baru (baca : Golkar plus sebagian pimpinan TNI-AD) sudah membunuh semangat revolusioner rakyat Indonesia. Dengan kalimat lain, Orde Baru telah mengkhianati revolusi rakyat Indonesia. Orde Baru (sekali lagi, supaya lebih jelas, Golkar plus sebagian pimpinan TNI-AD) telah « melibas » atau “menghancurkan” Bung Karno, berikut segala politik beliau, ajaran-ajaran beliau, sejarah hidup beliau, serta para pendukung setia beliau.

Para pakar di bidang sejarah, politik , ketatanegaraan ( termasuk kita semua yang awam ini), perlu memeriksa kebenaran ungkapan bahwa Orde Baru pada hakekatnya adalah Orde Pengkhianat revolusi. Dengan menghabisi Bung Karno, Orde Baru telah menyabot atau merusak jalannya revolusi rakyat Indonesia. Sebab, tidaklah dapat disangkal, bahwa Bung Karno merupakan simbul utama perjuangan rakyat melawan kolonialisme dan imperialisme. Bung Karno adalah tokoh revolusioner pemersatu bangsa Indonesia. Dengan dihancurkannya Bung Karno, maka revolusi rakyat Indonesia mengalami pukulan yang mematikan.

## Hari Pahlawan Adalah Revolusioner

Selama ini orang banyak bicara tentang Hari Pahlawan, tanpa - pada umumnya - mengerti bahwa Hari Pahlawan sesungguhnya adalah sesuatu yang revolusioner. Ini ada hubungannya dengan usul yang diajukan oleh tokoh pembrontakan rakyat Surabaya melawan pasukan Sekutu (Inggris, Belanda dll) , Sumarsono, kepada bung Karno, untuk menjadikan 10 November sebagai Hari Pahlawan. Usul ini untuk memperingati bahwa pada tanggal 10 November 1945 pasukan Sekutu telah melakukan serangan besar-besaran dengan mengerahkan 30 000 pasukan, 50 pesawat terbang dan sejumlah kapal perang, terhadap pasukan rakyat di Surabaya dan sekitarnya.

Berbagai pasukan bersenjata rakyat waktu itu menguasai kota Surabaya (dan sekitarnya) dan melakukan perlawanan heroik terhadap pasukan Sekutu dengan bantuan rakyat. Karena besarnya kekuatan musuh yang menggunakan alat-alat perang modern, maka pasukan-pasukan bersenjata rakyat ini (yang kebanyakan menggunakan senjata rampasan dari tentara Jepang) akhirnya terpaksa mundur keluar kota Surabaya. Besarnya serangan yang dilakukan dengan menggunakan alat-alat perang modern ini menyebabkan gugurnya banyak pejuang dan penduduk, yang jumlahnya sulit ditaksir. Pertempuran Surabaya adalah yang terbesar yang pernah terjadi selama revolusi 45.

Dalam jangka lama pertempuran Surabaya merupakan sumber inspirasi perjuangan bagi seluruh negeri. Berkat peran Bung Karno, maka sebelum Orde Baru berkuasa, perayaan Hari Pahlawan selalu dikaitkan erat dengan perlawanan bangsa Indonesia terhadap kolonialisme dan imperialisme. Bung Karno telah menjadikan Hari Pahlawan sebagai sumber insiprasi perjuangan, sebagai sarana untuk pendidikan politik dan patriotisme, sebagai penghargaan terhadap tokoh-tokoh dari berbagai suku, agama, dan keyakinan politik yang telah berjuang untuk kepentingan rakyat.

## Orde Baru Merusak Hari Pahlawan

Kiita semua masih ingat bahwa selama Orde Baru berkuasa Hari Pahlawan masih terus diperingati, tetapi dilakukan demi kepentingan politik rezim militer, dan dalam rangka politik de-Sukarnoisasi. Oleh karena itu selama Orde Baru Hari Pahlawan sudah kehilangan api revolusionernya. Api revolusioner perjuangan rakyat melawan kolonialisme, imperialisme, dan penindasan adalah jiwa Hari Pahlawan yang sesungguhnya. Inilah yang selalu dihidupkan dan dikobarkan oleh Bung Karno dalam pidato-pîdatonya ketika merayakan Hari Pahlawan.

Tetapi, Orde Baru telah mereduksi Hari Pahlawan menjadi peringatan atau perayaan yang penuh dengan acara atau upacara, tetapi hampa arti kerevolusionerannya. Orang merasakan bahwa perayaan Hari Pahlawan semasa Orde Baru hanyalah merupakan ritual yang dipaksakan penguasa , tanpa partisipasi aktif dan penuh antusiasme rakyat. Terasa adanya kesenjangan antara rakyat dan penguasa ketika Hari Pahlawan hanya diisi dengan upacara penghormatan bendera, nyanyian lagu-lagu standar, pidato-pidato yang steril dan penuh kesemuan, atau kunjungan ke Taman Makam Pahlawan.

Ini wajar dan tidak bisa lain. Sebab, pada dasarnya Orde Baru adalah kontra-revolusioner. Orde Baru telah didirikan oleh sejumlah pimpinan militer untuk menghacurkan kekuatan revolusioner di bawah pimpinan Bung Karno. Ada orang yang merumuskan bahwa pada hakekatnya Orde Baru adalah sama saja dengan pembrontak PRRI-Permesta, yang dengan bantuan kekuatan asing (AS dan Inggris) mencoba menggulingkan kekuasaan politik Bung Karno.

Selama puluhan tahun, Hari Pahlawan di bawah Orde Baru telah diisi dengan “visi dan misi” rezim militer Suharto dkk. Dalam rangka ini telah dipupuk citra – termasuk secara terselubung - bahwa “pahlawan” adalah terutama militer. Contohnya, antara lain, ialah selalu dimakamkannya pejabat tinggi militer di Taman Makam Pahlawan, tidak peduli apakah pejabat tinggi itu korup atau bermoral atau tidak. Sebagian Taman Makam Pahlawan telah menjadi kumpulan kuburan bagi mereka yang sebenarnya tidak berhak menyandang sebutan “pahlawan”.

Dapat dikatakan bahwa rezim militer Orde Baru telah merusak arti sesungguhnya Hari Pahlawan dan mempersempit pengertian pahlawan, dengan memupuk anggapan bahwa “pahlawan” adalah terutama dari kalangan militer.

## Kembalikan Arti Hari Pahlawan

Kita semua tidak boleh membiarkan terus Hari Pahlawan dirayakan tanpa isi yang berbobot, atau hanya dilaksanakan dengan upacara-upacara ritual yang bersifat rutine dan kering, seperti yang terjadi selama Orde Baru. Pada masa lalu, perayaan Hari Pahlawan terlalu didominasi oleh politik penguasa, dan karenanya kehilangan partisipasi rakyat secara aktif, sukarela, dan penuh antusiasme. Pengertian bahwa pahlawan adalah kebanyakan, atau terutama, adalah militer perlu kita luruskan.

Ini penting untuk mengingatkan bersama kembali, bahwa dalam sejarah perjuangan bangsa telah muncul banyak tokoh, yang bisa dikategorikan sebagai pahlawan, dari berbagai suku seperti Sultan Agung, Imam Bonjol, Diponegoro, Patimura, Cut Nyak Dhien, Soekarno, Hatta, Jenderal Besar Sudirman, dan lain-lain. Juga untuk mengingatkan bahwa dalam perlawanan terhadap kolonialisme Belanda telah berkorban banyak orang yang meninggal di tanah pengasingan di Digul dan tempat-tempat lainnya.

Untuk selanjutnya, perlu difikirkan bersama-sama bagaimana menjadikan Hari Pahlawan benar-benar sebagai hari besar bangsa di mana sebanyak mungkin golongan masyarakat ikut berperan aktif, dan bukan hanya sebagai penonton pasif suatu pertunjukan yang membosankan karena kosong isinya. Hari Pahlawan harus dijadikan kembali hari revolusioner, seperti di zaman Bung Karno, di mana rakyat bisa memainkan perannya secara aktif dan leluasa.

Untuk itu pemerintah dituntut untuk membantu segala inisiatif masyarakat yang bertujuan untuk berpartisipasi aktif dalam merayakan Hari Pahlawan, sehingga sebanyak mungkin golongan dalam masyarakat merasa sungguh-sungguh bahwa ini hari besar mereka, dan bukan hanya urusan para penguasa saja.

## Mereka Tak Berhak!

Dalam merayakan Hari Pahlawan terasa sangat pedih dan menyayat hati melihat bahwa banyak “tokoh-tokoh” yang ikut membasmi Bung Karno (beserta pendukung beliau) ikut-ikut bersuka-ria dalam berbagai upacara yang megah-megah, bahkan ikut mengucapkan pidato yang muluk-muluk tetapi penuh kemunafikan dan kebohongan. Sebenarnya, pengkhianat-pengkhianat terhadap Bung Karno ini tidak berhak bicara tentang jasa-jasa para pahlawan yang telah berkorban untuk revolusi dan kemerdekaan nusa dan bangsa. Sebab, dengan menjadi pendukung setia Orde Baru, mereka sebenarnya sudah terang-terangan dan jelas mengkhianati para pahlawan itu sendiri.

Dan sangat menyedihkan bahwa ketika para pengkhianat revolusi ini bisa berpesta-pora merayakan Hari Pahlawan maka para perintis kemerdekaan dan tokoh-tokoh perjuangan yang pernah di-tapolkan oleh Orde Baru terpaksa merayakan hari bersejarah ini dengan cara mereka masing-masing. Cukup banyak para pejuang kemerdekaan yang di-tapolkan oleh Orde Baru, dan mendapat siksaan fisik dan batin dalam jangka waktu yang panjang.

Sebagai contoh yang patut dicatat ialah kasus Sumarsono. Pemimpin pembrontakan rakyat dan pertempuran Surabaya 10 November 1945 ini pernah di-tapolkan oleh Orde Baru selama bertahun-tahun, tanpa salah atau dosa apa pun juga . Tokoh besar pertempuran Surabaya ini, yang mengusulkan kepada Bung Karno dirayakannya tanggal 10 November sebagai Hari Pahlawan, tidak mendapat penghargaan apa-apa dari pemerintah dan seolah-olah dilupakan saja. Bahkan, di hari tuanya (sekarang usia 82 tahun) ia terpaksa hidup di luarnegeri, di Australia. Sungguh, kenyataan yang menyedihkan yang patut disayangkan.

Ini semua mengingatkan kita bahwa banyak hal-hal yang sudah dirusak oleh Orde Baru yang harus kita perbaiki bersama-sama. Termasuk bagaimana selanjutnya merayakan Hari Pahlawan sebagai hari revolusioner.
