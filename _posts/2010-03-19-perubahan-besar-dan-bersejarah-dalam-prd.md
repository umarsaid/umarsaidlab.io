---
layout: post
title: Perubahan besar dan bersejarah dalam PRD
date: 2010-03-19
---

Di tengah-tengah maraknya gerakan berbagai kalangan dalam masyarakat Indonesia untuk menentang neo-liberalisme, ditambah berkobarnya gejolak dalam opini publik karena timbulnya skandal raksasa Bank Century, Keputusan DPR soal Bank Century, serta berita-berita besar soal korupsi di kalangan "atas", ada peristiwa menarik yang kiranya patut mendapat perhatian dari berbagai fihak.

Peristiwa tersebut adalah kongres ke-7 PRD yang diadakan baru-baru ini, yang telah mengambil keputusan-keputusan penting dan bersejarah, yang mencerminkan adanya perubahan-perubahan besar dalam kehidupan dan perjuangan PRD selama 14 tahun. Bagi mereka yang banyak sedikitnya sudah mengenal sepak terjang PRD di masa yang lalu (dan juga bagi yang ingin mengetahui perkembangannya sekarang ini), seyogyanya mencermati dengan teliti (!)  keputusan-keputusan kongres, yang disajikan di bawah ini.

Karena, PRD yang bagi banyak kalangan dipandang sebagai organisasi kiri yang menjadi legendaris karena perjuangannya yang berani, gigih dan banyak pengorbanan dalam melawan rejim militer Orde Baru (dan biasanya banyak bekerja di bawah tanah atau clandestine) sudah mengambil keputusan untuk menjadi partai yang bersifat terbuka, mengibarkan azas Pancasila seperti yang digagas oleh Bung Karno. PRD memandang, bahwa nilai-nilai Bung Karno itu perlu kembali diangkat, dipahami, dan digunakan oleh Rakyat Indonesia, sebagai dasar pemikiran (asas) menuju cita-cita nasional.

Kongres PRD juga memutuskan sebagai tujuan : "Mewujudkan masyarakat adil dan makmur, menghapuskan penindasan manusia atas manusia serta penindasan bangsa atas bangsa".  Keputusan kongres itu, juga mencerminkan bahwa PRD sekarang ini ingin membuang kesan-kesan dari publik selama ini, bahwa PRD adalah eksklusif  (atau sektaris) dalam era keterbukaan politik, dan karenanya menciptakan sekat atau jarak antara PRD dengan masyarakat.

Bisalah kiranya diharapkan, bahwa dengan perubahan-perubahan besar ini, PRD akan dapat berkembang lehih besar dan kuat untuk bisa memberikan sumbangan yang lebih  besar kepada perjuangan bersama  -  dengan golongan demokratis lainnya -  untuk akhirnya mendatangkan perubahan-perubahan besar dan fudamental di negeri kita.

Sebab, negara dan bangsa kita membutuhkan adanya gerakan extra-parlementer yang besar, kuat, dan luas, termasuk PRD !


Paris, 19 Maret 2010

A. Umar Said



* * **



PRD: Titik Balik Dalam 14 Tahun Perjalanan Partai



 Keputusan kongres ke-7 PRD :



Pada tanggal 1 sampai 3 Maret 2010, Partai Rakyat Demokratik (PRD) menyelenggarakan Kongresnya yang ke-VII (tujuh) di Salatiga, Jawa Tengah. Kongres ini diikuti sekitar 100 kader/perwakilan dari 19 provinsi. Kongres ke-VII ini, disebut-sebut sebagai titik balik (point of return) dalam kurun empat belas tahun perjalanan PRD. Partai Rakyat Demokratik, dideklarasikan tanggal 22 Juli 1996, sempat melewati masa kritis akibat represi rezim otoriter Orde Baru. Namun, di antara tahun 1996-1998 itu, PRD tetap melakukan perlawanan dengan taktik perjuangan bawah tanah (clandestine).



Semangat Berkongres

Setelah Orde Baru ambruk, sejumlah perdebatan internal mewarnai keberadaan dan gerak PRD, baik perdebatan teoritis maupun praktis. Perdebatan menyangkut kesimpulan atas persoalan pokok masyarakat Indonesia, rumusan asas, strategi politik, strategi organisasi, dan bahkan tujuan PRD. Perdebatan-perdebatan ini kerap berujung perpecahan. Namun, saat ini coba dimaknai kembali, bahwa perdebatan-perdebatan tersebut merupakan upaya (atau jalan) untuk mencapai bentuk dan pola perjuangan partai yang terbaik, di atas kondisi-kondisi yang baru. Perdebatan-perdebatan tersebut, dan bercampur pengalaman praktisnya, telah diambil sebagai pelajaran berharga untuk merumuskan dan menatap masa depan.



Terkait pemaknaan akan hal tersebut di atas, terbersit semangat yang kuat di kalangan kader PRD untuk: (1) menghilangkan kecenderungan dogmatisme terhadap teori-teori perjuangan. (2) berupaya semakin mengenal kondisi-kondisi masyarakat Indonesia pada bidang ekonomi, politik, dan sosial budaya, sekalian mendalami sejarahnya, dan merumuskan solusi atas masalah-masalahnya.



Persoalan pokok Masyarakat Indonesia

Kongres PRD secara aklamasi memutuskan jawaban atas pertanyaan: apa persoalan pokok masyarakat Indonesia saat ini? Disepakati, bahwa gerak perkembangan masyarakat Indonesia, untuk keluar dari kemiskinan dan keterbelakangan, terhambat oleh suatu bentuk penjajahan baru. Sejak pemilihan umum presiden tahun 2009 lalu, khalayak umum mulai mengenal hambatan/persoalan ini dengan istilah: "neoliberalisme".



Neoliberalisme, adalah sebuah sistem sosial yang menyokong penguasaan/perampasan sumber daya ekonomi (tanah/lahan/tempat tinggal, modal/uang, pasar/konsumen/pembeli, alat kerja, dan pengetahuan/teknologi) oleh modal besar yang mayoritas milik asing. Penguasaan/perampasan sumber daya ekonomi tersebut, secara cepat atau lambat, semakin menciptakan ketidakadilan sosial yang luar biasa, antara yang sangat kaya dan yang miskin, antara negeri-negeri/bangsa-bangsa yang maju dan yang terbelakang.



Di Indonesia sekarang, terdapat hampir empat puluh juta pengangguran, dan 70% dari 115 juta angkatan kerja merupakan pekerja sektor informal. Mereka adalah tenaga produktif yang sedang disia-siakan oleh negara, dalam kekuasaan politik/cengkraman neoliberalisme, sehingga juga ditelantarkan. Indonesia tertinggal dalam hal perkembangan ilmu pengetahuan dan teknologi. Indikasi fakta ini bisa dikongkritkan, bahwa setelah 65 tahun proklamasi kemerdekaan, masih terdapat 55% angkatan kerja yang hanya mengecap pendidikan pada Sekolah Dasar (SD).



Neoliberalisme turut mengancam keberadaan borjuasi (pengusaha/pemilik modal) nasional karena persaingan-persaingan sebagai sesama pengusaha/pemilik modal. Neoliberalisme berpadu dengan korupsi melahirkan penindasan struktural yang tak berperikemanusiaan dan tak berperikeadilan, khususnya terhadap golongan-golongan termiskin dalam masyarakat-yang saat ini berjumlah lebih dari 100 juta jiwa.



Persoalan-persoalan masyarakat lainnya, telah digolongkan sebagai bagian dari sistem neoliberalisme, atau terkait secara tidak langsung. Masalah budaya politik di bawah iklim liberalisme yang berpadu sisa otoritarianisme dan budaya feodalisme (seperti politik pencitraan, politik represi, dan politik uang) cukup sering disebut dalam Kongres. Juga persoalan korporat media yang memonopoli dan mengendalikan isu/materi penyiaran.

Kongres PRD juga menyimpulkan adanya gerak perubahan situasi geopolitik internasional, dari dunia unipolar (dengan sentralnya di Amerika Serikat) menuju multipolar. Indikasinya, antara lain, kemunculan potensi kekuatan-kekuatan dunia baru BIRC - Brasil, India, Rusia, dan Cina/Tiongkok, serta Amerika Latin. Tampak juga, kecenderungan kapital di banyak negeri mulai coba berlindung di balik kepentingan nasionalnya. Meski belum menampak jelas, perubahan situasi geopolitik internasional ini turut membawa dampak tertentu ke dalam negeri.



Asas PRD

Hasil penting lain adalah perubahan asas PRD, dari Sosial Demokrasi Kerakyatan menjadi Pancasila. Sosial Demokrasi Kerakyatan masih dinilai positif dalam makna perjuangan bagi demokrasi dan keadilan sosial, namun kurang mengekspresikan semangat kebangsaan/kepentingan nasional-yang telah menjadi kebutuhan obyektif. Perubahan ini tidak membawa argumentasi: "untuk menghindari stigma komunis" yang selama ini dilekatkan kepada PRD. Stigmatisasi tidak merubah kenyataan kongkrit. Lebih jauh, pilihan Pancasila sebagai asas PRD merupakan hasil pendalaman terhadap hakikat Pancasila yang dimaksud oleh pencetusnya dan pada zamannya, Soekarno.



Orde Baru menyalahgunakan Pancasila untuk menyelubungi kejahatannya. Tapi penyalahgunaan ini tidak sanggup menghapus  nilai-nilai dan maksud baiknya bagi seluruh rakyat Indonesia dan masyarakat dunia. PRD memandang, nilai-nilai itu perlu kembali diangkat, dipahami, dan digunakan oleh Rakyat Indonesia, sebagai dasar pemikiran (asas) menuju cita-cita nasional. Asas tersebut ada di tengah kehidupan masyarakat Indonesia: Ketuhanan atau Spiritualitas, Kesetaraan Manusia/Internasionalisme, Kebangsaan/Nasionalisme, Demokrasi/ Kedaulatan  Rakyat, dan Keadilan Sosial/Sosialisme.

Usulan opsi selain Pancasila yang muncul dalam dinamika Kongres adalah: 1) Sosio Demokrasi - Sosio Nasionalisme/Bung Karnoisme; 2) Sosialisme Indonesia/Soekarnoisme; 3) Marxisme-Leninismee, dan; 4) Gotong Royong/Pluralisme Kiri.



Tujuan

Beberapa usulan berbeda sempat kembali muncul dalam pembahasan tujuan PRD. Dan pada akhirnya tujuan PRD yang diputuskan dan ditetapkan adalah "Mewujudkan masyarakat adil dan makmur, menghapuskan penindasan manusia atas manusia serta penindasan bangsa atas bangsa".

Opsi di atas unggul dalam perolehan suara terhadap beberapa usulan lain, yaitu: (1) "membangun pemerintahan persatuan nasional yang berjiwa Tri Sakti", (2) "Membentuk pemerintahan revolusioner sementara buruh dan tani; dan (3) "Esensi Preambule/Pembukaan UUD 1945".



PRD Partai Terbuka

Kongres ke VII PRD secara aklamasi menetapkan PRD sebagai partai yang bersifat terbuka. Kesan eksklusif sempat melekat pada PRD, sebagai konsekuensi mekanisme penyaringan anggota yang ketat, serta pengaruh lanjutan dari taktik perjuangan bawah tanah di era kediktoran Orde Baru-yang tepat pada masa/situasinya. Disadari kemudian, bahwa dua belas tahun penerapan model tersebut dalam era keterbukaan politik, justru menciptakan sekat atau jarak antara Partai dengan masyarakat. Sekat ini tetap ada dan terasa, sekalipun mayoritas kader PRD terlibat aktif dalam pengorganisasian dan perjuangan masyarakat di berbagai sektor, seperti; buruh, petani, rakyat miskin perkotaan, pedagang/pengusaha kecil, mahasiswa, pencari kerja, pekerja paruh waktu, profesional, seniman dan budayawan.



Keputusan menjadi partai terbuka adalah upaya menghilangkan jarak atau sekat tersebut. Dengan demikian, tiap-tiap warga atau rakyat Indonesia yang setuju pada asaz, tujuan, serta Anggaran Dasar/Anggaran Rumah Tangga (AD/ART) PRD dapat menjadi anggota. Dalam konggres sempat tergambarkan konsep untuk menjalankan mekanisme organisasi yang demokratis, dalam mengolah hak dan kewajiban tiap-tiap kader dan anggotanya. Konsep ini akan dielaborasi lebih jauh, agar dapat merajut potensi yang ada menjadi kesatuan perjuangan yang berhari depan. Baik perjuangan di lapangan elektoral maupun di lapangan non-elektoral.



PRD bertekad membangun basis-basis konstituen secara berkelanjutan, dan kaderisasi untuk melahirkan calon-calon pemimpin di tingkat lokal maupun nasional. Capaian pengorganisasian tersebut akan dilaporkan secara reguler kepada seluruh kader dan anggota. PRD tetap dan akan berupaya menjalin kerjasama sebaik-baiknya dengan kekuatan politik nasional lain dalam perjuangan menghadapi neoliberalisme.



Struktur dan Pengurus/Kepemimpinan

Kongres menetapkan struktur badan penyusun PRD terdiri atas: Kongres (sebagai badan pengambil keputusan tertinggi), berikut Presidium Nasional (Presnas), Komite Pimpinan Pusat (KPP), Komite Pimpinan Wilayah (KPW), Komite Pimpinan Kabupaten/Kota (KPK), Komite Pimpinan Kecamatan (KPC), Komite Pimpinan Desa (KPD) dan Komite Pimpinan Kelurahan (KPL).



Pemilihan Ketua dan Sekretaris Jenderal Presidium Nasional (Presnas), yang sekaligus menjabat Ketua dan Sekretaris Jenderal Komite Pimpinan Pusat PRD telah dilangsungkan. Pencalonan bersistem paket ini akhirnya dimenangkan oleh pasangan Agus Jabo Priyono sebagai Ketua, dan I Gede Sandra sebagai Sekretaris Jenderal. Kepemimpinan terpilih mencerminkan perpaduan dua generasi dalam PRD. Agus Jabo Priyono, awalnya seorang aktivis mahasiswa Universitas Negeri Sebelas Maret - Solo (UNS) dekade 1990-an, yang turut menjadi pendiri PRD.



I Gede Sandra adalah aktivis politik, alumnus Institut Teknologi Bandung (ITB) tahun 2005. Paket ini unggul pada putaran kedua pemilihan, atas dua paket lainnya yaitu: Agus Jabo Priyono (calon Ketua) - Binbin Firman Tresnadi (calon Sekjen), dan paket Lalu Hilman Afriandi (calon Ketua) - I Gede Sandra (calon Sekjen). Sejumlah nama lainnya sempat meramaikan putaran pertama sebagai calon Ketua dan Sekjen PRD. Mereka antara lain Marlo Sitompul, Data Brainata, Yulia Evina Bhara, dan Rudi Hartono (mengundurkan diri dari pencalonan).



(Demikian siaran yang disajikan oleh Berdikari Online, organ PRD, pada tanggal 15 Maret 2010)
