---
layout: post
title: Kejahatan negara ini tidak boleh diteruskan
date: 2003-06-17
---

Tulisan kali ini, sebagai sambungan dari tulisan terdahulu (tanggal 2 Juni 2003) masih mengenai kasus Nani Nurani, seorang eks-tapol yang dinyatakan tidak berhak mendapatkan KTP Seumur Hidup, meskipun sudah berumur lebih dari 60 tahun. Karena, menurut SK Mendagri No 24/1991, KTP Seumur Hidup tidak bisa diberikan kepada yang terlibat langsung kegiatan organisasi terlarang. Berita yang disiarkan oleh suratkabar Sinar Harapan (14 Juni 2003) memberikan bahan-bahan atau informasi tambahan mengenai kasus tersebut, yang sekaligus juga menunjukkan betapa besarnya ketidakadilan dan pahitnya penderitaan yang telah dialami Nani Nurani - untuk jangka waktu yang lama sekali - dalam hidupnya sebagai eks-tapol.

Sekali lagi, dan untuk kesekian kalinya, perlu ditegaskan dalam tulisan ini bahwa Orde Baru yang didirikan dan dijalankan oleh rezim militer Suharto dkk (pada hakekatnya Golkar dan sebagian TNI-AD) adalah satu rezim yang telah melakukan pelanggaran peri-kemanusiaan secara besar-besaran, pencekekan kehidupan demokrasi secara brutal, pelecehan hukum secara sewenang-wenang, dan perusakan moral secara serius. Dan, dalam jangka yang lama sekali pula, yaitu lebih dari 30 tahun. Kejahatan ini serius sekali. Dan besar sekali.

Rezim militer Orde Barunya Suharto dkk ini sudah begitu buruknya dan begitu busuknya, sehingga tidak banyak orang lagi yang berani dan juga mau secara terang-terangan membelanya, kecuali sejumlah orang-orang yang patut dipertanyakan kesehatan nalarnya dan disangsikan kebersihan hati nuraninya. Nasib orang-orang yang masih terang-terangan berani membela politik Suharto, memang tidak bisa lain, kecuali akan dicatat oleh sejarah bangsa. Membela politik Suharto dkk yang jelas-jelas sudah mendatangkan kerusakan-kerusakan besar pada negara dan bangsa, adalah - pada akhirnya dan pada dasarnya – sama saja dengan mengkhianati kepentingan rakyat Indonesia.

## Kejahatan Negara Ini Tidak Boleh Diteruskan

Seperti yang sudah dikemukakan dalam tulisan terdahulu, kasus Nani Nurani adalah bukti nyata dari kejahatan Negara yang dilakukan oleh rezim militer Suharto dkk Kejahatan Negara ini dilakukan besar-besaran dan menyeluruh.

Apa yang dialami Nani Nurani adalah hanya setitik kecil dari segunung besar pelanggaran perikemanusiaan yang telah dilakukan oleh rezim militer Suharto dkk selama lebih dari 30 tahun. Ratusan ribu orang, bahkan puluhan juta orang lainnya, telah menderita - dalam berbagai bentuk dan kadar berbeda-beda - sebagai korban Orde Baru. Ada puluhan juta orang yang kehilangan suami, istri, ayah, ibu, saudara, menantu dan kemenakan karena pembantaian besar-besaran dalam tahun ’65. Banyak pula yang ditahan secara sewenang-wenang selama bertahun-tahun, meskipun tidak bersalah apa-apa, sepêrti halnya Nani Nurani.

Para eks-tapol (beserta keluarga mereka) sampai sekarang masih menghadapi berbagai penderitaan, baik secara batin maupun fisik. Banyak yang tidak menerima pensiun atau bantuan apapun dari pemerintah, karena dipecati begitu saja secara sewenang-wenang. Peraturan-peraturan yang dibikin oleh rezim militer Orde Baru masih banyak yang ditrapkan terus, sampai sekarang. Kejahatan negara ini, sebagai peninggalan Orde Baru, seperti yang dialami oleh Nani Nurani, tidak boleh diteruskan oleh pemerintah.

Sebagai bahan renungan bersama, di bawah berikut ini disajikan kembali tulisan yang dimuat oleh suratkabar Sinar Harapan tanggal 14 Juni yl :

“MANTAN PENARI DI ISTANA CIPANAS DITOLAK PEROLEH KTP SEUMUR HIDUP“ Meski sudah hampir empat dekade, bekas-bekas peristiwa berdarah 30 September 1965 masih saja terlihat bahkan masih terasa kegetirannya. Salah satu yang ikut merasakan getah noktah hitam dalam sejarah Indonesia itu adalah Nani Nurani.

Wanita lajang ini menyandang predikat mantan tahanan politik Partai Komunis Indonesia (PKI). Namun, dia mengaku sampai hari ini tidak pernah mengetahui alasan pencidukan dirinya oleh Polisi Militer Cianjur pada 21 Desember 1968.

Sejak itu, selama tujuh tahun ke depan Nani harus menerima kenyataan menjadi penghuni sel tahanan di penjara wanita Bukit Duri, Jakarta Selatan, atas tuduhan menjadi anggota PKI. Hukuman itu dijalaninya tanpa suatu proses pengadilan pun.

Sebelum ditangkap, Nani terkenal sebagai penyanyi tradisional klasik Cianjuran (sinden) dan penari klasik Sunda. Suara merdu dan keterampilannya menari membuatnya sering diundang Presiden Soekarno untuk tampil di Istana Cipanas, Cianjur, Jawa Barat.

Keluwesan gerak kaki dan tangannya serta kemerduan suaranya kerap dipertontonkan dan diperdengarkan untuk menghibur Sang Proklamator. Alhasil, Nani menjadi salah satu penari dan penyanyi kesayangan Bung Karno. Ini ditunjukkan setiap kali Bung Karno singgah di Istana Cipanas.

”Setiap kali bapak datang singgah dan tinggal di Istana, saya pasti dipanggil untuk menari atau menyanyi dan menghibur beliau,” katanya mengenang.

Ketika itu, dia pegawai pada Dinas Kebudayaan Dati II Cianjur sebagai penari. Instansi tempatnya bekerja hanya ada dua pegawai, yakni kepala dinas dan dirinya sendiri. Praktis, setiap ada acara dia harus hadir mendampingi atasan.

## Menari Di Hut PKI

Sekali waktu pada pada bulan Juni 1965, tiga bulan sebelum meletusnya peristiwa 30 September 1965, dia tampil pada acara hari ulang tahun (HUT) PKI yang diadakan di gedung pertemuan di Cianjur. Pada kesempatan itu hadir seluruh pejabat setempat seperti Bupati, Dandim, Kepala Kejaksaan, dan para pejabat lainnya.

Dalam bulan yang sama, ia pindah ke Jakarta dan tinggal di rumah kakaknya sampai pecah peristiwa 30 September 1965. Di bulan Oktober, dia bekerja di sebuah perusahaan yang bernama Takari dan kemudian dialihkan menjadi sekretaris pribadi Brigadir Jendral Soerjosoemarsono di PT MUGI-NCR Div. pada tahun 1967 sampai akhirnya ia ditangkap tahun 1968.

”Sampai sekarang saya masih bertanya-tanya, kenapa saya harus ditahan dan dipaksa mengaku terlibat PKI. Apakah karena saya pernah menjadi penari kesayangan Bung Karno yang juga pernah dituduh terlibat PKI, lantas kemudian saya dianggap juga terlibat PKI? Atau mungkin karena pernah menari di acara HUT PKI kemudian dianggap antek-anteknya PKI? Saya tidak tahu,” katanya.

Brigjen Soeryosoemarsono pun pernah menyampaikan kepada pemerintah bahwa Nani sama sekali tidak terlibat dengan PKI. Namun, dia tetap harus mendekam dalam tahanan. Udara kebebasan datang pada tahun 1975.
Nani dibebaskan berdasarkan surat perintah pembebasan dari tahanan penuh G30S/PKI, yakni surat perintah Kepala Teperda Jaya Laksus Pangkopkamtib No.40/III/1876 tanggal 29 Maret 1976.

Sebagai catatan, surat perintah pembebasan terhadap Nani dikeluarkan oleh sebuah dinas militer dan bukan putusan pengadilan yang pro justitia dan berkekuatan hukum tetap. Hingga saat ini pun, tidak ada satu proses peradilan—apalagi putusan pengadilan yang berkekuatan hukum tetap— yang menyatakan dirinya bersalah dan terlibat dengan organisasi terlarang PKI. Alih-alih pemerintah merehabilitasi namanya, malah cap sebagai eks tapol terus disandang selamanya.

## Minta Diakui

Sekarang di usianya yang ke 61 tahun, ia ingin diakui sebagai warga negara yang hak-haknya juga diakui oleh negara tanpa adanya perlakuan diskriminatif. Sama seperti warga lainnya, dia ingin mendapatkan Kartu Tanda Penduduk (KTP) seumur hidup, sesuai dengan Pasal 25 Ayat (1) Perda No. 1 tahun 1996 dan Pasal 1 Ayat (1) Kepmendagri No. 24 tahun 1991 yakni telah berumur lebih dari 60 tahun. Dia menghendaki perlakuan yang sama. Syarat sebagaimana diatur dalam Pasal 25 Ayat (2) Perda No 1 tahun 1996 dan tidak terlibat dengan organisasi terlarang sudah terpenuhi sebab dalam pandangannya tidak ada peradilan mana pun yang memutuskan dirinya terlibat organisasi terlarang.

Dia boleh saja berpendapat demikian, namun tidak demikian dengan Kepala Pemerintahan Kecamatan Koja Jakarta Utara, yang berwenang mengeluarkan KTP seumur hidup bagi dirinya. Nani tinggal di Jalan Cemara Angin Z/24 RT 7/RW6 Rawa Badak Utara, Koja.

Menurut Camat Koja, Usman, pihaknya tidak mengeluarkan KTP seumur hidup kepada Nani karena berpegang pada SK Mendagri No 24/1991, pasal 2, yang intinya KTP seumur hidup tidak bisa diberikan kepada yang terlibat langsung kegiatan organisasi terlarang dan sampai sekarang SK itu belum dicabut. Berdasarkan informasi dari Suku Dinas Kependudukan Jakarta Utara, Nani Nurani terbukti terlibat organisasi terlarang. ”Dengan patokan itu kami tidak bisa mengeluarkan KTP seumur hidup kepada yang bersangkutan,” kata Usman ketika dihubungi SH. Kepada Nani kecamatan hanya mengeluarkan KTP biasa yang harus diperpanjang setiap lima tahun.


## Ajukan Ke Ptun

Penjelasan itu rupanya tidak memuaskan Nani. Ketika semua pintu-pintu tampaknya sudah tertutup bagi kemungkinan dirinya diterima sebagai warga negara secara tidak diskriminatif, maka dia mencari peluang melalui Pengadilan Tata Usaha Negara, dan prosesnya masih berlangsung.

Pegangannya adalah pasal 18 ayat (1) UU No. 39 tahun 1999 tentang Hak Asasi Manusia dimana yang menyatakan: setiap orang yang ditangkap, ditahan, dan dituntut karena disangka melakukan tindak pidana berhak dianggap tidak bersalah, sampai dibuktikannya kesalahannya secara sah dalam suatu sidang pengadilan dan diberikan segala jaminan hukum yang diperlukan untuk pembelaannya, sesuai dengan ketentuan perundang-undangan.

”Saya hanya ingin mendapatkan hak sebagai warga negara biasa. Kalau saya masih dianggap terlibat PKI tapi sampai sekarang tidak ada bukti atau putusan pengadilan yang memutuskan saya terlibat PKI, itu karena saya memang bukan PKI,” katanya kepada SH. Jadi, biarlah pengadilan yang memutuskannya.” (kutipan dari Sinar Harapan selesai)
