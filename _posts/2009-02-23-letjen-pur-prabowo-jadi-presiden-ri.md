---
layout: post
title: Letjen (Pur) PRABOWO jadi presiden RI ?
date: 2009-02-23
---

Kiranya, dapat sama-sama kita perhatikan, bahwa di antara banyak masalah
yang sangat menarik dalam menghadapi pemilihan umum 2009 di Indonesia adalah
pencalonan letnan jenderal (Pur) Prabowo sebagai presiden kepala negara RI.
Sebab, pencalonan diri Prabowo sebagai presiden RI menyangkut berbagai
masalah yang penting sekali bagi negara dan bangsa. Oleh karena itu, tulisan
ini mengajak para pembaca untuk mencoba menelaahnya dari berbagai segi dan
sudut pandang. Tulisan ini adalah sekadar penyajian beberapa aspeknya saja
tentang persoalan ini, sebagai sumbangan bahan untuk pemikiran bersama.

Terlebih dahulu baiklah sama-sama kita catat bahwa Letnan Jenderal (Pur)
Prabowo adalah tokoh militer tingkat tinggi (jenderal bintang tiga), yang
pernah menjadi kebanggaan Suharto dan juga menantunya (suami dari Titiek
Suharto). Karena berbagai sebab kekeluargaan dan bersifat pribadi,
perkawinannya (dalam tahun 1983) dengan anak Suharto ini tidak dapat
dipertahankan terus-menerus. (Namun, tentang soal ini tidak akan
dibentangkan di sini, karena masih banyak soal-soal lainnya yang lebih
penting, yang lebih patut dan juga perlu diangkat, justru karena menyangkut
kepentingan umum).

Prabowo ini (anak ahli ekonomi Prof. Dr Sumitro Djojohadikusumo, tokoh PSI
yang menjadi penentang besar presiden Sukarno, dan yang kemudian jadi
gembong gerakan pembrontakan PRRI- PERMESTA) adalah perwira muda yang karir
militernya naik dengan kecepatan luar biasa, berkat kedekatannya dengan
keluarga Suharto. Dalam kehidupannya sebagai perwira militer selama 24
tahun, yang patut dicatat adalah bahwa ia pernah memainkan peran penting
dalam operasi di Timor Timur dan memimpin Kopassus (antara 1995-1998) dan
kemudian jadi komandan tertinggi KOSTRAD (Komando Strategis Angkatan Darat)
tahun 1998 . Ia pernah membawahi sejumlah perwira-perwira AD yang
penting-penting, antara lain : mayjen Adam Damiri, mayjen Mahidin Simbolon,
brigjen Amirul Isnaeni, kolonel Gerhan Lentara.

Berbagai pelanggaran HAM Prabowo


Dalam masa ia menjadi pimpinan militer di Timor Timur dan Kopassus inilah ia
dituduh oleh berbagai kalangan (di Indonesia dan di luarnegeri) bertanggung
jawab atas pelanggaran HAM (antara lain berbagai penculikan dan pembunuhan)
terhadap penentang-penentang rejim militer Orde Baru. Karena banyaknya
pelanggaran HAM di Timor Timur inilah maka satu organisasi yang berpusat di
Australia “East Timor International Support Center “ pernah menuntut supaya
Prabowo diajukan di depan pengadilan Internasional tentang Kejahatan Perang
(International War Crimes Tribunal). Di samping itu nama Prabowo tercantum
dalam berbagai dokumen-dokumen yang pernah dikeluarkan oleh
organisasi-organisasi internasional seperti Amnesty International,
International Human Rights Watch. Selama puluhan tahun nama Prabowo
disangkut-pautkan oleh berbagai kalangan di luarnegeri dengan masalah Timor
Timur. Jadinya, nama Prabowo dikenal sekali di kalangan internasional yang
membidangi masalah HAM.

Ketika tahun 1998, ia pernah disebut-sebut sebagai “the rising star” ,
perwira muda yang dalam umur 47 tahun menjadi paglima KOSTRAD, dan yang
mungkin akan menggantikan Suharto. Namun, pada waktu itu pulalah (setelah
Suharto dipaksa mundur dari jabatannya) ia dicopot juga dari jabatannya di
KOSTRAD (Mei 1998). Banyak kalangan di Indonesia mengkaitkan Prabowo dengan
diculiknya dan dihilangkannya aktivis-aktivis anti-rejim Suharto (terutama
anak-anak muda PRD) , dan juga disebut-sebut tentang keterlibatannya dengan
peristiwa rasial anti-Tionghoa di Jakarta dalam bulan Mei 1998.

Jadi konglomerat besar


Setelah pensiun dari dinas militer, ia menjadi pengusaha besar. Sekarang,
setelah lima tahun pensiun, sebagai konglomerat ia memimpin suatu armada
dari perusahaan-perusahaan yang bergerak di bidang (antara lain) :
perikanan, pertanian, kelapa sawit, bubur kertas, minyak, pertambangan.
Perusahaan-perusahaan ini yang semuanya tergabung dalam Nusantara Group
adalah : Nusantara Energy, Kiani Kertas, Tidar Kerinci Agung, Tusam Hutani
Lestari, Nusantara Kaltim Coal, Jaladri Swadesi Nusantara, Gardatama
Nusantara.

Untuk pemilu 2004, Prabowo pernah berusaha mencalonkan diri sebagai capres
dari kalangan Golkar, tetapi tidak berhasil karena bersaing dengan Wiranto.
Sekarang, untuk pemilu 2009, dengan kedudukannya sebagai ketua umum partai
Gerindra (Gerakan Rakyat Indonesia Raya) ia mencalonkan diri untuk menjadi
presiden RI. Nah, mengingat berbagai hal yang berkaitan dengan sepak
terjangnya sebagai petinggi Angkatan Darat di masa yang lalu dan juga sikap
politiknya itu semualah maka persoalan pencalonan dirinya sebagai capres
menjadi menarik sekali, karena bisa menimbulkan berbagai pertanyaan,
kecurigaan, kekuatiran, juga harapan-harapan dan perlawanan atau penolakan.

Tokoh pendukung kekuasaan Suharto


Reaksi semacam itu semua adalah lumrah atau wajar saja. Sebab, mengapa
sesudah menjadi konglomerat yang menguasai begitu banyak perusahaan dan kaya
raya itu masih juga ingin menjadi presiden? Lagi pula, ia tentu sudah tahu
bahwa bagi banyak kalangan (di Indonesia dan di luarnegeri) riwayat hidupnya
sebagai petinggi militer dan dekat sekali dengan keluarga Suharto adalah
faktor yang tidak menguntungkannya sebagai calon presiden? Atau, apakah
justru ia berpendapat sebaliknya, yaitu bahkan sebagai faktor-faktor yang
membantunya? Dan apakah pernyataannya bahwa ia ingin jadi presiden itu
betul-betul karena secara jujur dan tulus ingin membela rakyat kecil dan
memperbaiki situasi negara dan bangsa? Dan apakah kalau seandainya (!!!) ia
jadi presiden maka negara dan bangsa Indonesia akan bisa menjadi lebih baik
daripada yang selama ini? Dan apakah justru tidak akan mengulangi lagi
segala hal buruk seperti yang banyak sekali terjadi di jaman Orde Baru ?

Adalah jelas sekali bahwa Prabowo, yang sejak dari mudanya (lulusan Akabri
tahun 1974) bertugas di pasukan Kopassanda, kemudian bertahun-tahun menjadi
komandan Kopassus yang mempunyai kekuasaan dan pengaruh yang besar sekali di
kalangan militer (terutama AD), dan kemudian lagi jadi komandan Kostrad
adalah tokoh pendukung kekuasaan Suharto yang “tidak tanggung-tanggung”.
Sikap politiknya terhadap Suharto juga jelas sekali. Ia adalah pengagum dan
penganut setia Suharto. Ia pernah menjadi unsur penting rejim militer
Suharto. Karena iru, wakaupun sudah pensiun, ia masih juga mempunyai
pengaruh tertentu yang tidak kecil di kalangan militer dan Golkar.

Kiranya, dari sudut pandang ini jugalah kita perlu menelaah pencalonan
Prabowo sebagai presiden RI. Sebab, ini hal ini ada kaitannya yang erat
dengan janjinya untuk mengadakan perubahan besar-besaran guna mengatasi
keterpurukan bangsa dan negara. Sedangkan, patut sekali disangsikan apakah
sebagai pelaku yang penting dan aktif rejim militer Orde Baru sampai tahun
1998 (sekitar 10 tahun yang lalu) ia betul-betul mau – dan juga bisa ! –
mengadakan perubahan besar-besaran.

Perlunya menghilangkan sisa-sisa Orde Baru

Sebab, perubahan besar-besaran berarti harus dengan tegas mau meninggalkan
hal yang buruk, yang jahat, yang haram, yang busuk, dari kepemimpinan
Suharto. Atau berarti harus berani merombak segala sisa-sisa politik dan
tindakan rejim Orde Baru yang tidak menguntungkan rakyat banyak. Atau harus
sungguh-sungguh mau menghilangkan segala peraturan dan undang-undang yang
bertentangan dengan HAM dan Pancasila yang masih berbau rejim militer Orde
Baru. Singkatnya, perubahan besar hanya bisa dilakukan dengan harus
memutuskan segala hubungan politik, ideologis, dan praktek-praktek buruk
rejim militer Suharto, yang dilakukan selama 32 tahun Orde Baru ditambah
dengan sekitar 10 tahun pasca-Suharto.

Jelaslah bahwa pekerjaan ini besar sekali, dan juga sangat rumit, karena
banyaknya persoalan yang parah, seperti yang sama-sama kita saksikan dewasa
ini di banyak bidang. Dan kita melihat sendiri bahwa berbagai masalah yang
gawat atau parah dewasa ini sumbernya adalah sisa-sisa pemerintahan Suharto
dalam waktu yang begitu lama itu, yang kemudian diteruskan atau diwarisi
oleh berbagai pemerintahan yang menyusulnya sampai sekarang.

Mengingat begitu besarnya dan begitu banyaknya pula masalah parah yang harus
ditangani demi kepentingan sebagian terbesar rakyat Indonesia, dan juga
mengingat jejak-rekam Prabowo sebagai komandan Kopassus yang penuh dengan
halaman hitam (harap ingat, antara lain, juga perannya dalam menindas
penentang-penentang Freeport di Papua), maka sangat sangat kecil harapan
bahwa Prabowo adalah tokoh yang dapat – dan mau sungguh-sungguh !!! -
mengadakan perubahan-perubahan yang dibutuhkan negara dan rakyat dewasa ini.

Dana besar untuk kampanye


Memang, patut dicatat dan diakui bahwa munculnya Prabowo sebagai capres
tahun lalu dengan diusung oleh partai yang baru lahir pula, yaitu Gerindra,
merupakan fenomena yang bisa “mengejutkan” banyak orang. Berkat besarnya
atau banyaknya dana yang dikerahkan untuk kampanye (lewat iklan dan
siaran-siaran lainnya di televisi dan media massa, rapat-rapat umum,
cetakan dan selebaran) popularitas Prabowo sebagai capres sudah mengalahkan
(menurut hasil berbagai survey) banyak sekali capres lainnya, kecuali SBY
dan Megawati.

Dengan dana yang besar ini, maka Prabowo bisa menggunakan berbagai saluran
dan jalan untuk merekrut tenaga dan mengumpulkan “pendukung” yang sekarang
sudah disebut-sebut mencapai belasan juta orang. Walaupun angka-angka yang
diumumkan oleh sumber Prabowo itu diperkirakan sudah digelembungkan, tetapi
berkat pengerahan melalui mesin organisasi partai Gerindra dan HKTI
(Himpunan Kerukunan Tani Indonesia) yang diketuainya bisa tetap merupakan
jumlah yang relatif “mengkuatirkan” bagi capres-capres lainnya, termasuk SBY
dan Megawati.

Memang, nampak sekali bahwa Prabowo sudah menggunakan cara-cara yang “lihay”
untuk menampilkan diri sebagai capres yang bisa diterima oleh banyak fihak,
jadi tidak hanya oleh kalangan yang masih bersimpati kepada Suharto beserta
Orde Barunya. Oleh karena itu ia banyak berbicara tentang situasi ekonomi
yang membikin sengsaranya rakyat kecil, terutama kalangan buruh, tani dan
nelayan. Ia juga galak sekali dalam mengkritik pemerintah yang terlalu
takluk kepada kepentingan modal asing dan membikin utang luar negeri yang
memberatkan ekonomi negara dan rakyat. Semunya itu, menurutnya, hanya
menguntungkan golongan elit saja.

Dengan menggunakan ungkapan-ungkapan yang patriotik, nasionalis, pro-rakyat
kecil, dan anti dominasi ekonomi asing, yang menyerupai atau mirip-mirip
ungkapan Bung Karno, ia berusaha menampilkan diri sebagai sosok yang bisa
menyaingi sosok lainnya sebagai presiden RI. Begitu pandainya ia bermain
kartu dalam perannya sebagai capres, maka ia minta restu Gus Dur, di samping
jiarah ke makam Bung Karno di Blitar dan juga di makam Suharto. Permainan
ini ternyata mendapat hasil yang relatif lumayan, sampai ada orang-orang
yang percaya bahwa ia adalah sosok yang menyerupai “Bung Karno kecil”
(mengingat seriusnya kebusukan masalah penyamaan Prabowo sebagai “Bung Kano
kecil” ini, akan ada tulisan khusus yang tersendiri).

Meskipun Prabowo mencoba mempertontonkan diri sebagai orang yang menghargai
atau menghormati Suharto dan Bung Karno kedua-duanya, namun wajarlah atau
masuk akallah bahwa sebenarnya ia lebih menghormati Suharto dari pada Bung
Karno. Ungkapan-ungkapan yang meniru-niru ajaran-ajaran Bung Karno hanyalah
untuk kepentingan kampanye capres-nya. Padahal, dapat diduga bahwa sulit
sekali bagi Prabowo untuk melepaskan diri dari ikatan batin dan hatinya
dengan Suharto dan Orde Barunya, yang telah membikinnya sebagai “orang
besar” di kalangan militer.

GERINDRA adalah sarang sisa-sisa Orba

Di antara berbagai indikasi yang menunjukkan ke arah ini bisa dilihat kalau
kita amati masalah partai Gerindra, yang ia pimpin (sebagai Ketua Umum) dan
mayjen (pur) Muchdi sebagai wakil ketua umumnya. Di samping Muchdi (bekas
komandan Kopassus yang kemudian jadi pimpinan BIN yang tersangkut dengan
masalah pembunuhan Munir) masih banyak pensiunan petinggi militer lainnya
yang - secara langsung atau tidak langsung – membantu Prabowo dengan
GERINDRA-nya. Dari sudut ini pulalah kita dapat melihat bahwa partai
GERINDRA adalah juga sarang kekuatan sisa-sisa rejim militer Suharto,
seperti halnya Golkar dalam jangka waktu puluhan tahun (yang sekarang sudah
makin merosot).

Karena itu, seandainya (sekali lagi, seandainya !) Prabowo -berkat dana
yang melimpah-limpah - bisa mengumpulkan dukungan yang makin besar yang
memungkinkannya meraih kedudukan tinggi negara kita, maka ini akan merupakan
perkembangan yang sangat berbahaya bagi rakyat dan negara kita. Sebab,
Prabowo bukanlah tokoh politik yang jelas-jelas, dan tegas-tegas, menentang
Orde Baru. Selama ini, tidak pernah ada pernyataannya atau sikapnya yang
terang-terangan melawan politik Suharto.

Padahal, jatuhnya Suharto dalam tahun 1998 adalah cermin yang jelas bahwa
rakyat Indonesia yang suaranya disalurkan oleh generasi muda, sudah
menyatakan kemuakan dan kemarahannya. “Kemenangan” Prabowo - besar atau
kecil – merupakan pengkhianatan terhadap aspirasi generasi muda dan rakyat
luas tentang perlu dibangunnya suatu pemerintahan yang berlainan sama sekali
dari pemerintahan Orde Baru. Singkatnya, “Kemenangan” Prabowo adalah
kemenangan sisa-sisa Orde Baru. Tidak bisa lain !!!

Kecuali, kalau Prabowo sudah terang-terangan, dan secara jujur atau
setulus-tulusnya, dan dengan tindakan nyata pula (!), mau meninggalkan sama
sekali keterkaitannya dengan segala hal yang buruk dari Suharto dan Orde
Baru. Hanya dengan cara itu, Prabowo akan bisa merubah citranya, yang penuh
dengan dosa-dosanya karena berbagai kejahatan atau pelanggaran HAM.

Tetapi, apakah Prabowo mau mengambil sikap yang demikian itu, adalah soal
yang akan bisa sama-sama kita saksikan tidak lama lagi.
