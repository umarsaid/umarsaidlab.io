---
layout: post
title: Mari Kerjasama Lebih Baik Untuk Kepentingan Indonesia
date: 2002-12-21
---

Menyambung tulisan tentang silaturahmi yang diselenggarakan tanggal 18 Desember malam antara keluarga besar Restoran INDONESIA dan keluarga besar KBRI di Paris serta Dutabesar Indonesia untuk Unesco dalam rangka peringatan Ulang Tahun ke-20 restoran, berikut di bawah ini disajikan bagian-bagian utama pidato rekan Suyoso (manager restoran) di depan pertemuan tersebut. Dengan menyimak apa yang diuraikan olehnya, maka nampaklah betapa besar harapan dan hasrat keluarga Restoran INDONESIA untuk mengkonsolidasi lebih jauh lagi hubungan baik dan kerjasama yang akhir-akhir ini sudah mulai digalang bersama-sama dengan KBRI.

Sebagai penanggungjawab utama usaha kolektif, yang sekarang terdiri dari 3 orang (bersama B. Santoso dan Didien) ia telah mengangkat secara singkat-singkat sekali berbagai pengalaman kehidupan restoran selama 20 tahun. Bahwa semuanya itu disampaikannya di depan para pejabat penting, yang mewakili pemerintah atau negara Indonesia di Prancis, mempunyai arti yang besar.

Antara lain rekan Suyoso menyatakan : Silaturahmi malam ini, kita selenggarakan dalam rangka Peringatan Ulang Tahun Restoran Indonesia yang ke 20. Melalui silaturahmi ini, kami berharap Bapak-bapak dan Ibu-ibu sekalian akan mengenal lebih dekat lagi restoran Indonesia, mengenal riwayat hidupnya.

Tepatnya, pada tanggal 14 Desember, 20 tahun yang lalu, restoran ini dibuka. Pada malam itu, untuk pertama kalinya, wangi bau sate yang dibakar, sedapnya bumbu gado-gado dan aroma saus rendang , tercium di ruangan restoran yang penuh sesak dengan para undangan. Untuk pertama kalinya pula lah lagu-lagu Indonesia berdengung di ruangan yang didekorasi sederhana dengan bambu-bambu dan kain batik Indonesia, dan sejak hari itu pulalah papan nama yang bertuliskan INDONESIA terpampang megah dipinggir jalan Vaugirard, jalan terpanjang di kota Paris dan di quartier tersohor kota Paris - Quartier Latin.

Waktu itu, yang terdapat dalam dada kami adalah rasa gembira dan rasa bangga. Kami merasa gembira, karena cita-cita untuk membuka restauran yang akan menjadi sandaran hidup bisa terwujud, dan bangga karena walaupun hanya sangat kecil, kami merasa telah memperkenalkan Indonesia, di dunia internasional.(memperkenalkan gastronominya dan keindahan musiknya, dan keramah- tamahan rakyatnya )

Restoran Indonesia telah berjalan 20 tahun. Dapat kami laporkan bahwa tujuan utama dari didirikannya restorant telah bisa tercapai. Yaitu bahwa restoran Indonesia telah bisa menampung semua teman-teman kami yang tidak mendapat pekerjaan di luar, sehingga mereka mempunyai pekerjaan dan bisa hidup secara layak di negeri orang tanpa bersandar pada keringat orang lain.

Di samping menyuguhkan makanan khas Indonesia yang lezat, sejak berdirinya, secara periodik kami memperkenalkan tanah air Indonesia melalui berbagai macam aktifitas kultural ( exposisi photo, kerajinan tangan, menyelenggarakan pelajaran bahasa dan lain-lain) walaupun dalam skala dan kemampuan yang sangat terbatas.

Di samping titik beratnya kami menciptakan sarana kerja untuk karyawannya yang tetap, selama 20 tahun ini, restoran Indonesa juga telah memberikan bantuan dan menyediakan kesempatan kerja kepada lebih dari 50 orang dari berbagai benua dan negara ( France, Belanda, Afrika, Madagaskar, Thailand, Malaya, Filipina, Korea, Kuba, Chili )

Demikianlah apa-apa yang kami lakukan sepanjang 20 tahun ini. Kami berkesimpulan, bahwa kunci dan resep keberhasilan tersebut adalah karena kami selalu berusaha mempraktekkan prinsip berdikari dan mandiri (termasuk menyerempet bahaya bila perlu ), melaksanakan semangat " gotong royong dan holopis kuntul baris " , memelihara kehidupan koperasi , serta selalu memelihara rasa persaudaraan dengan sahabat-sahabat Perancis. Ini sikap kedalam, dan keluar, kepada tamu-tamu dan langganan kami selalu menyajikan keramah-tamahan, kesopanan dan kesabaran.

Kami sangat gembira , bahwa silaturahmi dengan Bapak-bapak malam ini bisa diselenggarakan. Ini adalah kejadian penting dalam sejarah restoran Indonesia di Paris. Karena menunjukkan adanya perubahan besar dalam hubungan restoran Indonesia dengan Kedutaan Besar Indonesia di Paris.

Sejak mulainya era keterbukaan, gelombang reformasi dan gerakan demokratisasi di tanah air Indonesia, di Paris terasa pengaruhnya secara nyata. Udara yang serupa telah mencairkan kebekuan Cortambert dan Vaugirard. Dalam waktu yang cukup lama, pada saat negeri kita dikuasai rezim otoriter dan anti demokrasi, restoran Indonesia tidak memperoleh tempat dan eksistensinya tidak mendapat pengakuan . Warga Indonesia yang tinggal di Paris, yang pada awal berdirinya restoran sering berkunjung makan dan membawa teman-teman mereka, berangsur-angsur berkurang dan bahkan pernah hilang sama sekali. Turis dari Indonesia, kalaupun datang, hanyalah yang tahu secara kebetulan dan secara sembunyi sembunyi ……Hal ini merupakan kenyataan pahit yang kami alami bertahun- tahun lamanya.

Namun keadaan semacam itu sekarang telah berubah. Sejak dua tahun yang lalu, hubungan baik mulai terjalin yang diikuti dengan saling kunjung dan bahkan kerjasama telah pernah terjadi. Tamu-tamu dari kalangan masayarakat Indonesia di Paris juga sudah mulai datang kembali secara bebas. Kami yakin, keadaan demikian akan bertahan terus diwaktu yang akan datang; dan perlu dikembangkan lagi kerjasama yang lebih baik untuk kepentingan bersama, yaitu kepentingan INDONESIA.

Menilai suasana hubungan kita tersebut, dalam kesempatan ini kami ingin menyampaikan penghargaan kami dan rasa terima kasih yang sebesar-besarnya, khususnya kepada ibu Yuli Mumpuni yang sejak beliau menjabat misinya di Paris telah banyak berinisiatif, merintis dan berbuat ke arah tercapainya suasana baru tersebut.

Kami juga sangat merasakan kehangatan dan keterbukaan bapak Silalahi, yang segera setelah beliau menjabat Dutabesar Luar bisa di Paris telah berkenan secara khusus menengok kami dan bersilaturahmi di ruangan ini dan juga berkenan menerima kami dalam berbagai kesempatan di Wisma beliau maupun di KBRI.

Karena itu sekali kami kemukakan bahwa silaturahmi malam ini adalah sangat berharga dan penting bagi kami.

Selanjutnya kami persilahkan pada Bapak Umar Said, sebagai sesepuh, salah satu pendiri dari Restoran Indonesia ini untuk menyampaikan sepatah dua patah kata.

Kemudian disusul dengan kata sambutan dari M. Pascal LUTZ yang sejak persiapan restoran ini mendampingi kami , sebagai Co-manager selama 20 tahun. Mungkin ia akan mengemukakan kesannya, kenapa mau mendampingi kami ber-”avonture” dengan membuka restoran ini, dan terus bersama kami secara sukarela, tanpa memperoleh bayaran satu senpun.
Setelah itu, kami mohon juga bapak-bapak dan ibu-ibu untuk memberikan nasehat ataupun petunjuk untuk pekerjaan kami di masa datang , untuk hubungan kerjasama antara kita yang lebih baik dan untuk pengabdian yang lebih nyata buat Indonesia.”

Demikianlah, antara lain, harapan dan rasa penghargaan yang disampaikan oleh pimpinan restoran, rekan Suyoso, atas nama seluruh keluarga besar Restoran INDONESIA.
