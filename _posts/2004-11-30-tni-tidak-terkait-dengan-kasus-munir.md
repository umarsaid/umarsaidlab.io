---
layout: post
title: TNI tidak terkait dengan kasus Munir ?
date: 2004-11-30
---

Pernyataan Panglima TNI Jenderal Endriartono Sutarto bahwa “institusi TNI tidak terkait dengan kasus kematian Munir”, bisa memberi kesan kepada banyak orang bahwa ia mengucapkannya terlalu cepat, terburu-buru, atau sembarangan saja, bahkan, gegabah. Karena, semua orang tahu, bahwa pengusutan kasus besar ini baru saja dimulai dengan langkah-langkah permulaan, baik di Indonesia maupun di negeri Belanda. Kita semua tidak bisa meramalkan, kira-kira bagaimana jadinya pengusutan kasus pembunuhan Munir ini, dan apa saja yang bisa dilakukan untuk membongkarnya sampai tuntas. Jadi, mengatakan sejak sekarang, bahwa “TNI tidak terkait dengan kematian Munir” adalah suatu hal yang bisa dianggap “sembrono” saja.

Apalagi, kalau disimak lebih lanjut ucapannya yang menyatakan bahwa “Tentara Nasional Indonesia (TNI) selama ini sudah terbiasa dijadikan kambing hitam. Biarin aja, enggak bener itu. Kebenaran adalah tetap kebenaran. Semua itu hanya akan bisa dibuktikan melalui proses hukum. Pada prinsipnya kami tak ada kaitannya dengan Munir”, ujarnya (Kompas, 24 November 2004)

## Selama INI Kambing Hitam ?

Ucapannya bahwa “selama ini TNI sudah terbiasa dijadikan kambing hitam” adalah percobaannya untuk menyebarkan dalam opini publik bahwa selama ini TNI sudah menjadi “sasaran tudingan” atau “korban tuduhan” yang tidak berdasar yang datang dari berbagai fihak. Seolah-olah Panglima TNI ini tidak tahu (atau pura-pura tidak tahu !?) bahwa selama pemerintahan rezim militer Orde Baru telah terjadi banyak sekali pelanggaran, penyelewengan dan kejahatan, yang dilakukan oleh TNI atau oknum-oknum dari kalangan TNI. Karena itulah Orde Baru, yang diciptakan dan dipupuk oleh kalangan militer selama puluhan tahun, sudah dinajiskan oleh sebagian terbesar rakyat Indonesia. Dengan kalimat lain dapatlah dikatakan bahwa kebusukan dan kesalahan Orde Baru adalah pada intinya (!!!) kebusukan dan kesalahan kalangan militer, yang perwakilan terpusatnya adalah Suharto.

Justru karena itulah selama ini banyak kalangan dalam masyarakat, yang berdasarkan pengalaman mereka sendiri, atau pengamatan mereka sendiri, mempunyai pendapat, atau sikap terhadap sebagian kalangan TNI. Mereka melihat ada orang-orang yang “dihilangkan” begitu saja, atau “diamankan” secara sewenang-wenang, atau “diciduk” secara tidak sah, yang dilakukan oleh kalangan militer dan para pendukungnya (kasus 1965-1966). Banyak pula orang yang menjadi korban tindakan-tindakan yang tidak sah oleh kalangan militer dalam kasus-kasus di Jakarta, Lampung, Aceh, Jawa Tengah dan Jawa Timur, Sulawesi, Maluku dan Papua, dan juga di Timor Timur.

Banyak sekali bukti-bukti dan tanda-tanda yang membikin banyak orang yakin bahwa dalam berbagai peristiwa ini kalangan militerlah (TNI-AD, terutama) yang bertanggungjawab. Jadi, sudah jelas bahwa sikap kalangan luas di masyarakat terhadap banyak sekali pelanggaran atau berbagai kejahatan yang telah dilakukan oleh kalangan TNI itu sama sekali bukanlah sekadar untuk “menjadikan TNI kambing hitam”. Selama puluhan tahun Orde Baru banyak sekali pelanggaran atau kejahatan kalangan militer ini tidak bisa dibongkar, karena kekuasaan diktatur militer Suharto (beserta pendukungnya) mencegah - dengan segala cara ! - terbongkarnya kejahatan-kejahatan ini.

## Kebenaran Adalah Kebenaran

Ucapan Jenderal Endriartono Sutarto lainnya yang patut diperhatikan, ialah ketika ia menyatakan bahwa ia “tidak keberatan jika proses investigasi kasus itu kemudian diarahkan ke institusinya. Akan tetapi, dia menolak adanya permintaan terhadap institusi TNI untuk membuka akses sebesar- besarnya jika hanya karena muncul tuduhan terhadap anggotanya » (Kompas, 24 November 2004).

Orang bisa mendapat kesan bahwa pernyataannya « menolak adanya permintaan terhadap institusi TNI untuk membuka akses sebesar- besarnya jika hanya karena muncul tuduhan terhadap anggotanya » adalah petunjuk bahwa ia (beserta teman-teman pendukungnya) tidak mau membantu pembongkaran perkara pembunuhan Munir ini secara tuntas, transparan, dan jujur.

Ini bertentangan dengan ucapannya bahwa « Kebenaran adalah kebenaran ». Untuk dapat dibuktikan kebenaran ucapannya bahwa « TNI tidak ada kaitannya dengan kematian Munir » maka perlu diadakan pengusutan, atau penyelidikan, atau pemeriksaan. Untuk dapat mencari kebenaran, maka akses sebesar-besarnya harus dibuka, juga di kalangan TNI. Kalau panglima TNI jenderal Endriartono sudah bersikap menolak permintaan pembukaan akses sebesar-besarnya (di kalangan TNI) maka akan makin sulitlah dihilangkannya tuduhan atau kecurigaan banyak orang bahwa ada keterkaitan TNI dengan kasus pembunuhan Munir. Sebab, seperti kita ketahui, sejarah Orde Baru telah penuh dengan dosa-dosa yang banyak dilakukan oleh kalangan militer, termasuk pembunuhan politik terhadap orang-orang yang menentang rezim militer.

## Tidak ADA Prajurit TNI Yang Terlibat ?

Pernyataan yang serupa « anehnya » adalah yang diucapkan oleh Kepala Pusat Penerangan TNI, Mayor Jenderal Sjafrie Sjamsoeddin. Menurutnya, ia « merasa yakin tidak ada prajurit TNI yang terlibat, baik dalam aksi teror itu atau pembunuhan Munir. Sebab, setiap prajurit TNI memiliki moralitas sehingga tidak akan melakukan tindakan seperti itu ». (Suara Pembaruan, 25 November 2004).
Bahwa para petinggi TNI-AD berusaha menjaga nama baik atau kehormatan institusi atau « corps » mereka adalah hal yang wajar, masuk akal dan bahkan terpuji.

Tetapi, dengan mengatakan bahwa « merasa yakin tidak ada prajurit TNI yang terlibat, baik dalam aksi teror itu atau pembunuhan Munir » maka banyak orang akan bisa bertanya-tanya dengan keheranan yang besar : dari mana dan bagaimana ia sudah tahu, dan dengan yakin pula ( !), bahwa tidak ada prajurit TNI yang terlibat pembunuhan Munir ?
Juga, ucapannya bahwa « setiap prajurit TNI memiliki moralitas sehingga tidak akan melakukan tindakan seperti itu » adalah sesuatu yang bisa menyebabkan banyak orang akan tersenyum sinis saja, atau, bahkan, tertawa terbahak-bahak kegelian mendengar lelucon semacam ini.

Nama baik atau kehormatan TNI memang harus kita jaga bersama. Sebagai negara, seperti banyak negara lainnya, kita memang juga membutuhkan adanya TNI. Tetapi, TNI yang betul-betul bisa berfungsi sebagai aparat negara yang bermutu, professional, menghargai demokrasi, dan mempunyai kesadaran tinggi untuk berbakti demi kepentingan rakyat banyak. Kita harus berusaha, bersama-sama, supaya TNI tidak lagi melakukan pelanggaran, penyelewengan atau kejahatan, seperti yang pernah banyak dilakukan di jaman Orde Baru, termasuk pembunuhan politik.

## TNI Harus Membantu Pengusutan

Nampaknya, sekarang ini banyak petinggi militer yang merasa « risi », atau gundah, atau tidak tenteram hati mereka, dengan timbulnya kasus pembunuhan Munir. Ini wajar, sebab sebagian besar dari opini publik memang (secara terang-terangan atau diam-diam) sedang menuduh atau mencurigai bahwa di belakang pembunuhan Munir ini ada ulah kalangan militer. Sedangkan, sebenarnya, belumlah tentu bahwa kasus pembunuhan Munir ada keterkaitannya dengan kepentingan kalangan militer tertentu. Tetapi, justru karena itulah, pengusutan secara tuntas harus diusahakan dengan sungguh-sungguh, termasuk di kalangan militer. Dan dalam pengusutan ini pimpinan TNI harus aktif memberikan bantuan sebesar-besarnya dan dengan segala cara !

Dari sudut ini dapat kita lihat bahwa ucapan sebagian pimpinan militer (antara lain Panglima TNI dan Kapuspen) bukanlah bantuan untuk mengusut kasus pembunuhan Munir dan memperbaiki citra TNI. Dengan mengatakan serta-merta bahwa TNI tidak terkait sama sekali dengan kasus ini, bukan berarti bahwa « kecurigaan » opini publik akan bisa terhapus begitu saja.

Sesungguhnya, institusi TNI tidak perlu risau dengan tuduhan atau kecurigaan serupa itu. Tetapi, haruslah diingat bahwa di masa-masa yang lampau sudah terlalu banyak peristiwa-peristiwa pembunuhan, penculikan, penganiayaan, ancaman dan berbagai pelanggaran atau kejahatan lainnya yang telah dilakukan oleh kalangan militer. Karena banyaknya dan seringnya kejadian-kejadian ini – dan selama berpuluh-puluh tahun pula ! -, maka secara otomatis banyak orang mempunyai kecenderungan untuk « menudingnya » atau mengkambinghitamkannya.

Untuk menghilangkan tudingan, atau kecurigaan, atau « pengkambinghitaman » TNI (dalam kasus Munir) itu tidak ada jalan lain kecuali membantu atau sebesar-besarnya pengusutan atau penyelidikan secermat mungkin, termasuk di kalangan TNI sendiri. Sebab, perlu menjadi kesadaran semua fihak bahwa selama pengusutan tidak berjalan sampai tuntas, atau kalau usaha rekayasa untuk menggagalkannya sampai berhasil, maka kecurigaan terhadap TNI masih akan tetap terus menempel di fikiran banyak orang. Ini berarti bahwa pengusutan sampai tuntas, termasuk di kalangan militer, adalah juga untuk kepentingan nama baik atau kehormatan TNI sendiri.

## Sikap Presiden SBY Yang Positif

Adalah menarik (dan menggembirakan) bagi banyak orang bahwa dalam menghadapi kasus pembunuhan Munir ini, Presiden SBY telah memperlihatkan sikap yang simpatik, antara lain dengan menerima di Istana Negara Ny. Suciwati (istri alm. Munir) beserta tokoh-tokoh HAM, seperti Mufti Makarim dari Kontras, Rachland Nashidik dari Imparsial dan Todung Mulya Lubis, salah satu pendiri Kontras dan Imparsial. Dalam kesempatan itu Presiden menyampaikan keprihatinannya atas kejadian yang menimpa Munir. Presiden berjanji akan membantu semaksimal mungkin, dan akan mendukung sepenuhnya apa pun yang dilakukan untuk bisa mengungkap kasus pembunuhan tersebut.

Sikap Presiden SBY kelihatan lebih tegas lagi dalam kasus pembunuhan politik ini, ketika ia menyatakan bahwa ia ingin tahu siapa pelaku pembunuhan terhadap Munir dan ditegakkannya keadilan. Ia juga setuju dengan pembentukan tim investigasi, yang bertugas khusus untuk menyelidiki kasus ini. Presiden sudah menginstruksi Polri agar bersikap jujur, objektif, dan terbuka dalam menyelidiki kasus itu. Ia minta proses ini dilaksanakan secara transparan dan akuntabel.

Sikap positif Presiden SBY dalam menghadapi kasus Munir ini bisa berdampak besar dan (mudah-mudahan !) bisa merupakan dorongan bagi aparat-aparat Negara - dan masyarakat luas - untuk menuntaskan pengusutannya. Supaya sikap positif ini tidak « luntur » atau bahkan menyeleweng, perlulah kiranya opini publik terus-menerus mendorongnya, serta mengawasinya dengan cermat dan waspada.

Apakah sikap Presiden SBY merupakan indikasi bahwa mantan jenderal TNI ini mempunyai pandangan yang independent dari kebanyakan petinggi militer yang masih berjiwa Orde Baru? Dan apakah dengan berbagai tindakan atau kebijakannya Presiden SBY bisa nantinya kita masukkan ke dalam barisan pendukung reformasi ? Dan apakah banyak janji-janji yang diucapkannya sebelum dan sesudah Pemilu akan bisa betul-betul dilaksanakan ? Belum jelas betul. Barangkali, kita akan bisa sama-sama melihatnya dengan lebih terang dalam waktu 6 bulan atau setahun lagi.
