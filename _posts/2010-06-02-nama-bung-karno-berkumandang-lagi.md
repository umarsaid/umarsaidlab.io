---
layout: post
title: Nama Bung Karno berkumandang lagi !
date: 2010-06-02
---

Bung Karno berkali-kali menegaskan bahwa Pancasila adalah kiri



Kiranya,  ada hal-hal yang sangat penting dan menarik sekali tentang peringatan hari lahirnya Pancasila 1 Juni yang dirayakan baru-baru ini yang patut mendapat perhatian kita semua. Di antaranya adalah berkumandangnya lagi (secara cukup gegap gempita)   nama Bung Karno sebagai tokoh besar bangsa, dan  tingginya penghargaan masyarakat terhadap Pancasila. Tulisan kali ini  akan berusaha menyoroti  sebagian dari hal-hal itu, sebagai bahan untuk kajian kita bersama.

Di antara peristiwa yang sangat penting itu adalah diadakannya acara peringatan hari yang bersejarah itu oleh MPR pada tanggal 1 Juni 2010. Diselenggarakannya acara peringatan oleh MPR secara resmi dan juga dilengkapi dengan pidato presiden RI adalah untuk pertama kalinya ( !)  sejak lahirnya Orde Baru hampir setengah abad yang lalu. Dan berkumandangnya lagi nama Bung Karno sebegitu « gemuruhnya »  -- juga untuk  pertama kalinya sejak puluhan tahun disekap  -- adalah sesuatu yang baru yang menandakan bahwa situasi di tanahair sedang mengalami pergeseran, walaupun belum begitu besar.

Di antara kita banyak yang masih ingat bahwa karena sikap anti-Bung Karno dari pimpinan militer (Suharto dan para jenderalnya, dengan dukungan Golkar beserta kalangan reaksioner lainnya) di masa lalu maka Pancasila telah dicoba dipisahkan dari Bung Karno. Di sinilah kelihatan dilemma yang dihadapi rejim militer Orde Baru. Sebab, oleh karena Pancasila sudah dinyatakan sebagai dasar-dasar negara sejak Republik Indonesia dilahirkan, maka sulit untuk merobahnya atau menentangnya.

Namun, bagi kalangan pimpinan Angkatan Darat ( dulu), adalah perlu sekali untuk terus-menerus menjalankan « de-Sukarnoisasi » , sesudah Suharto melakukan pengkhianatan dengan kudeta merangkak terhadap Bung Karno. Bagi kalangan pimpinan militer (terutama AD),  yang bersekongkol dengan kekuatan imperialisme (terutama AS), maka harus dicegah – dengan segala jalan dan cara  -- adanya kemungkinan bagi kembalinya ketokohan Bung Karno. Untuk mencapai tujuan itu « de-Sukarnoisasi » harus dijalankan, di segala bidang, dan dengan berbagai cara.

Salah satu di antara banyak cara yang dijalankan adalah berusaha merekayasa, atau memelintir, atau memalsu sejarah Pancasila. Untuk itu telah diusahakan menyebarkan bantahan (atau penolakan) bahwa Bung Karno adalah tokoh penggagas atau penggali Pancasila. Dikampanyekan oleh mereka (antara lain oleh Nugroho Notosutanto, sejarawan yang mengabdi kepada kepentingan pimpinan Angkatan Darat) bahwa sebelum Bung Karno mengucapkan pidatonya di depan BPUPKI (Badan Penyelidik Usaha Persiapan Kemerdekaan Indonesia) yang mengandung isi dasar-dasar negara yang akan dibentuk kemudian ada orang-orang lainnya yang sudah mengemukakannya terlebih  dulu.

Peringatan Hari Lahirnya Pancasila dilarang Kopkamtib

Pemelintiran  atau pemalsuan sejarah ini dimaksudkan untuk menghilangkan sama sekali atau menghancurkan citra Bung Karno sebagai satu-satunya tokoh bersejarah bangsa, yang mempunyai gagasan begitu besar seperti yang dirumuskan dalam Pancasila. Usaha ini diperkuat lagi dengan dilarangnya oleh Kopkamtib (sejak 1 Juni 1970) segala peringatan tentang Hari Lahirnya Pancasila.

Larangan oleh Kopkamtib ini bisa diartikan sebagai usaha untuk mencegah banyak orang tetap mengingat kepada tokoh besar bangsa, Bung Karno, yang sebelum G30S adalah satu-satunya pemimpin yang paling dihormati oleh semua golongan yang berjuang melawan imperialisme beserta kakitangannya di Indonesia.

Larangan Kopkamtib untuk memperingati Hari Lahirnya Pancasila 1Juni adalah bukti tambahan lagi bahwa pimpinan Angkatan Darat (waktu itu) memang mempunyai rencana menghilangkan nama besar atau sosok agung Bung Karno dari sejarah perjuangan bangsa Indonesia. Larangan ini adalah kelanjutan dari tindakan pimpinan militer menempatkan Bung Karno dalam tahanan rumah dengan perlakuan yang sama sekali tidak manusiawi, sampai wafatnya dalam keadaan yang mengenaskan sekali.

Seluruh politik Orde Baru adalah anti-Pancasila

Rekayasa  dan pemalsuan Pancasila adalah salah satu di antara banyak sekali kejahatan pimpinan Angkatan Darat (dulu)  terhadap Bung Karno yang dilakukan oleh rejim militer Orde Baru. Selama 32 tahun lamanya Orde Baru telah menggembar-gemborkan Pancasila, Pancasila, Pancasila, padahal prakteknya adalah sama sekali serba bertentangan dengan isi atau jiwa atau tujuan Pancasila yang asli dan sebenarnya.

Boleh dikatakan bahwa seluruh politik dan praktek rejim Orde Baru selama puluhan tahun itu adalah pengkhianatan atau pengrusakan atau pembusukan Pancasilanya Bung Karno. Salah satu bentuknya adalah pembentukan Badan Pembinaan Pendidikan Pelaksanaan Pedoman Penghayatan dan Pengamalan Pancasila dan dipaksakannya penataran P4 ( Pedoman Penghayatan dan Pengamalan Pancasilla) yang membosankan dan kosong isinya di seluruh Indonesia.

Suharto beserta rejim militernya tidak mungkin bisa menjiwai dan mempraktekkan Pancasila yang asli dan sebenarnya, karena sikapnya yang anti-Bung Karno. Sedangkan, Bung Karno dan Pancasila adalah satu dan senyawa, dan tidak bisa dipisahkan. Jadinya, jelaslah kiranya, bahwa orang-orang atau golongan yang anti-Bung Karno tidak mungkin menjadi penganut atau penjunjung Pancasila yang sejati. Dari sudut ini bisalah kita katakan bahwa kalangan atau golongan yang anti-Bung Karno pada hakekatnya adalah pengkhianat Pancasila.

Perlulah kiranya sekali lagi ditekankan bahwa Orde Baru telah melakukan banyak sekali dan berbagai macam kesalahan atau pelanggaran atau kejahatan yang bertentangan dengan jiwa atau isi Pancasila yang asli, yaitu Pancasilanya Bung Karno. Dan ini dilakukan dalam bidang politik, ekonomi, sosial, kebudayaan,  dan moral selama puluhan tahun itu. Bisalah dikatakan bahwa dengan merusak jiwa Pancasila  itu pada hakekatnya atau sebenarnya pimpinan Angkatan Darat (dulu) sudah merusak negara dan bangsa. Akibat kerusakan yang parah  itu dapat kita saksikan dengan jelas dengan adanya kebusukan, kebobrokan, dan kebejatan yang terjadi terus-menerus sampai dewasa ini di seluruh negeri.

Rindu kepada kepemimpinan yang mempersatukan bangsa

Oleh karena itulah kita bisa melihat adanya peringatan Hari Lahirnya Pancasila yang diselenggarakan oleh MPR (harap catat :  lembaga tertinggi negara ) sebagai peristiwa penting dalam usaha untuk meluruskan sejarah dan menempatkan Bung Karno pada tempat terhormatnya yang layak dan kedudukan agungnya yang seharusnya pula.

Arti penting lainnya dari peringatan oleh MPR kali ini adalah satu indikasi bahwa usaha pimpinan Angkatan Darat  (dulu) untuk menghapus atau memperkecil ketokohan raksasa Bung Karno sebagai pemimpin bangsa yang sebenarnya sudah gagal. Tanda-tanda kegagalan pimpinan  Angkatan Darat (dulu) untuk melakukan « de-Sukarnoisasi » ini juga kelihatan dari diucapkannya pidato presiden SBY (nota bene mantan pimpinan tinggi Angkatan Darat) selama 30 menit, yang pada pokoknya berisi hal-hal yang positif tentang Bung Karno dan Pancasilanya.

Apakah pidato presiden SBY mengenai Bung Karno dan isi Pancasila itu karena dipaksa oleh karena jabatannya sebagai presiden, ataukah hanya merupakan lamis bibir dengan perhitungan-perhitungan politik tertentu, pastilah akan sama-sama kita ketahui jawabannya tidak lama lagi di kemudian hari.

Karena adanya sekarang ini banyak sekali keresahan atau kekuatiran masyarakat akibat timbulnya pertentangan suku, agama, perasaan kedaerahan, kesenjangan sosial yang makin menganga lebar-lebar, sebagai akibat buruknya situasi politik, ekonomi, sosial, maka timbullah rasa rindu kepada adanya kepemimpinan dan pedoman yang bisa mempersatukan seluruh bangsa untuk mengatasi persoalan-persoalan besar dan parah bangsa tersebut.

Juga adanya segolongan kecil dari kalangan Islam fanatik, yang mengadakan berbagai kegiatan   -- terbuka maupun tertutup --  untuk mendirikan negara Islam dan, karenanya, menentang atau mempersoalkan Pancasila, menggugah kesedaran banyak orang untuk menghidupkan kembali arti penting Pancasila yang asli seperti yang diajarkan Bung Karno.

Perubahan-perubahan besar untuk restorasi Indonesia

Dan, dalam menghadapi berbagai persoalan besar yang parah, yang menyebabkan kesengsaraan atau penderitaan sebagian besar rakyat kita (kemiskinan yang meluas, pengangguran yang membengkak, kesehatan rakyat yang tak terurus, pendidikan yang terbengkalai, kebejatan moral yang merajalela, korupsi yang mengganas) banyak orang jadinya ingat bahwa Pancasila selama ini tidak dipraktekkan sama sekali.

Pentingnya arti Pancasila (yang asli) juga diangkat secara serius oleh simposium Nasional Demokrat (yang dipimpin Surya Paloh) yang dengan thema « Restorasi Indonesia » telah mengajak banyak tokoh dan pakar dari berbagai bidang untuk membahas adanya urgensi untuk melaksanakan gagasan-gagasan besar Bung Karno dalam menghadapi masalah-masalah besar dewasa ini dan di masa datang.

Dalam batas tertentu dapatlah dikatakan bahwa simposium « Restorasi Indonesia » di Jakarta (tanggal 1 dan 2 Juni) diliputi oleh suasana hari peringatan Pancasila 1 Juni dan penghormatan kepada sosok besar Bung Karno. Dalam suasana yang demikian ini telah dikupas berbagai masalah-masalah besar yang sedang dihadapi oleh bangsa dan negara, dan  solusinya atau jalan keluarnya, dengan mengadakan perubahan-perubahan yang sudah mendesak. Ini semua merupakan angin segar, ketika situasi di Indonesia sedang diselimuti oleh kepengapan yang penuh dengan kemunafikan, kepura-puraan, yang mencerminkan kerusakan moral yang meluas.

Korban Orde Baru : pelanggaran besar terhadap Pancasila

Namun demikian, dalam memperingati Hari Lahirnya Pancasila 1 Juni kali ini seyogyanya kita semua tidak melupakan adanya pelanggaran besar-besaran atau penginjak-injakan hasil gagasan agung Bung Karno ini oleh Orde Baru, yaitu masih terkatung-katungnya atau diterlantarkannya (sampai sekarang !) kasus para korban Orde Baru yang terdiri dari anggota dan simpatisan PKI beserta sanak saudaranya. Mereka ini terdiri dari keluarga orang-orang yang dibunuhi secara besar-besaran, atau terdiri dari para tapol berserta keluarganya, yang sudah disengsarakan selama puluhan tahun.

Kalau dilihat dari berbagai segi, dan direnungkan dalam-dalam, perlakuan pimpinan Angkatann Darat (Orde Baru) terhadap golongan kiri penganut politik Bung Karno ini (terutama golongan PKI)  termasuk pelanggaran  berat dan besar sekali terhadap sila-sila Pancasila yang terdiri : Ketuhanan Yang Maha Esa, Kemanusiaan Yang Adil dan Beradab, Persatuan Indonesia, Kerakyatan, dan Keadilan Sosial bagi Seluruh Rakyat Indonesia

Berbagai pelanggaran berat rejim Suharto (yang didukung oleh Golkar dan kalangan reaksioner di Indonesia maupun di luar negeri) terhadap golongan kiri ini kelihatan lebih jelas kalau diingat bahwa Pancasila ini tidak dimusuhi oleh golongan kiri dan juga bahwa Pancasila sama sekali tidak anti-kiri.

Bahkan, sebaliknya ( !!!) menurut penjelasan penggagasnya atau penggalinya, yaitu Bung Karno, justru Pancasila adalah kiri. Mungkin, ada orang yang baru mendengar untuk pertama kalinya bahwa bahwa Bung Karno pernah berkali-kali menegaskan bahwa Pancasila adalah kiri. Namun, bagi orang yang meragukannya perlulah dikutip pernyataan oleh « sumbernya » yang paling berhak (dan juga paling sah ) untuk menjelaskannya sendiri.

Bung Karno : " Pancasila adalah kiri dan anti kapitalisme"

Penjelasan Bung Karno bahwa Pancasila adalah kiri, dapat dibaca oleh siapapun juga dalam buku « Revolusi belum selesai » (jilid I dan II), yang beberapa kutipannya antara lain adalah sebagai berikut :

« Sayalah, saudara-saudara, yang mengerti benar arti, idée, dari Pancasila. Idée, sebagai ku katakan  tadi, syukur Alhamdulillah terhadap kepada Allah SWT, saya yang menggali lima mutiara dari pada bumi Indonesia ini. Saya yang memformuleer (pen. Bhs Bld . Memformulasi)  Pancasila. Saya yang mempersembahkan Pancasila ini ke hadapan sidang dari pada pemimpin-pemimpin Indonesia dari Sabang sampai Merauke. Sayalah, saudara-saudara, jadi aku tahu Pancasila itu apa. »   (« Revolusi belum selesai », halaman 428)

« Jangan kira, saudara-saudara, kiri is alleen maar (pen. hanya) anti-imperialisme.  Jangan kira kiri hanya anti imperialisme, tetapi kiri juga anti uitbuiting (pen.pemerasan)  Kiri adalah juga menghendaki satu masyarakat yang adil dan makmur, di dalam arti tiada kapitalisme, tiada exploitation de l’homme par l’homme, tetapi kiri. Oleh karena itu, maka saya berkata tempo hari, Pancasila adalah kiri. Oleh karena apa ? Terutama sekali oleh karena di dalam Pancasila adalah unsur keadilan sosial. Pancasila adalah anti kapitalisme. Pancasila adalah anti –exploitation de l’homme par l’homme. Pancasila adalah anti exploitation de nation par nation. Karena itulah  Pancasila kiri ».  (« Revolusi belum selesai », halaman 77)

Hal-hal yang diangkat seperti tersebut di atas menunjukkan bahwa Bung Karno adalah betul-betul seorang pemimpin revolusioner, yang mengemban gagasan-gagasan kiri yang dituangkannya dalam Pancasila dan ajaaran-ajarannya yang lain dalam memimpin revolusi rakyat Indonesia. Dan ajaran-ajaran revolusionernya itu pada pokoknya masih relevan untuk diambil jiwanya atau isinya untuk menghadapi berbagai masalah besar bangsa dan negara kita dewasa ini.
