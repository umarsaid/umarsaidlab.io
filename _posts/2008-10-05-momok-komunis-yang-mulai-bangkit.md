---
layout: post
title: '"Momok komunis" yang mulai bangkit'
date: 2008-10-05
---

Mohon kepada para pembaca untuk mencermati dan merenungkan bersama-sama isi pernyataan KASAD, Jenderal Agustadi Sasongko Purnomo, di Monumen Pancasila Sakti Lubang Buaya tentang « Upaya kebangkitan komunis makin nyata » yang diuacapkannya pada tahlil di Monumen Pancasila Sakti Lubang Buaya Jakarta.

Sebab, pernyataan KASAD Jenderal Agustadi (harap baca berita Antara di bawah ini) memberikan petunjuk yang jelas bahwa sampai pada saat ini TNI (yang dulunya dinamakan ABRI) pada dasarnya masih sama saja dengan yang sewaktu di bawah pimpinan Suharto selama zaman Orde Baru, yaitu sebagai aparat yang reaksioner sekali di negara kita.

Berita tersebut antara lain berbunyi sebagai berikut :


Upaya Kebangkitan Komunis Makin Nyata, kata Kasad

Jakarta (ANTARA News) - Upaya membangkitkan ideologi komunis yang diusung Partai Komunis Indonesia (PKI) 43 tahun silam, kini semakin nyata, kata Kepala Staf Angkatan Darat (Kasad) Jenderal TNI Agustadi Sasongko Purnomo.

"Kita makin merasakan berbagai upaya sistematis untuk menghidupkan paham komunis di Indonesia," katanya, dalam sambutannya pada tahlil dan doa bersama mengenang wafatnya tujuh pahlawan revolusi di Monumen Pancasila Sakti Lubang Buaya, Jakarta, Selasa.

Kasad Agustadi mengatakan, berbagai upaya nyata dan sistematis untuk menghidupkan kembali paham komunis antara lain pemasangan gambar dan slogan komunis pada media tembok, kaos dan media lainnya.

Selain itu, tambah Agustadi, ada upaya sekelompok pihak dan golongan yang ingin menghambat dan menyimpangkan tujuan bangsa dan negara berdasarkan paham persatuan dan kesatuan berdasarkan Pancasila dalam kerangka Negara Kesatuan Republik Indonesia (NKRI).

Tidak itu saja, lanjut dia. Upaya-upaya memecah soliditas TNIB Angkatan Darat khususnya, dan TNI sebagai `musuh` kelompok PKI juga makin nyata, sistematis dan transparan.

"Upaya-upaya itu sangat sistematis dan transparan. Sehingga kita harus tetap mewaspadai segala upaya tersebut yang dilakukan simpatisan dan pengikut paham komunis," kata Kasad.

Karena itu, tanpa ingin mengungkap luka lama dan menyebarkan dendam, maka semua pihak harus dapat mewaspadai segala upaya yang sistematis tersebut dengan tetap memegang teguh dasar negara Pancasila, asas persatuan dan kesatuan secara hati-hati, arif dan bijaksana, demikian Agustadi. (Antara, 30 September 2008)

Barang dagangan yang sudah usang

Pernyataan KASAD di malam tahlilan di Lubang Buaya malam tanggal 1 Oktober itu membuktikan bahwa walaupun Suharto sudah meninggal, “momok bahaya laten komunis” yang sudah diuar-uarkan dengan gencar dan terus-menerus selama puluhan tahun, sekarang masih dicoba untuk dijajakan terus seperti barang dagangan yang sudah usang dan makin tidak laku baik di Indonesia maupun di banyak negeri di dunia.

Kita masih sama-sama ingat bahwa dalam jangka lama (puluhan tahun !!!) “momok bahaya laten komunis” telah dipakai rejim Orde Baru untuk menipu dan menakut-nakuti rakyat dengan tujuan untuk menjaga stabilitas dominasi rejim militer dan untuk mengintimidasi segala kritik, kecaman, atau perlawanan terhadap Suharto dkk. “Momok bahaya laten PKI” terus-menerus ditiup-tiupkan secara luas dan sistematis melalui berbagai cara dan jalan atau bentuk (antara lain : indoktrinasi, keharusan menonton film G30S/PKI, didirikannya monumen-menumen, diaporama, dan paksaan untuk kursus Pancasila dll dll.)

Dan karena hebatnya propaganda tentang “bahaya PKI” ini, yang dilakukan oleh pemerintah dan juga media massa (TV, suratkabar dan majalah) maka,tidak sedikit orang yang terkecoh atau “termakan” olehnya. Dalam sejarah dunia, jarang ada penguasa negara yang melakukan pembunuhan massal sampai jutaan komunis, dan memenjarakan secara sewenang-wenang ratusan ribu orang tidak bersalah dan menyengsarakan puluhan juta orang keluarga para korban peristiwa 65 selama puluhan tahun (ingat : sampai sekarang !!!). Hanyalah Hitler, Franco, dan tokoh-tokoh reaksioner dan pro-AS (antara lain berbagai diktator militer di Amerika Latin seperti Pinochet ) yang telah melakukan hal-hal yang mirip dan setujuan dengan apa yang dilakukan Suharto.

Mengapa terus ditiup-tiupkan “momok komunis”

“Momok bahaya laten PKI” dipakai juga untuk menutupi dosa-dosa besar segolongan militer di bawah pimpinan Suharto dkk dan sekaligus juga berusaha “membenarkan” pelanggaran Ham yang luar biasa besarnya itu. Itulah sebabnya, ketika kekuatan PKI yang besar sekali sebagai pendukung Bung Karno sudah dihancurkan, selama puluhan tahun masih terus juga digembar-gemborkan “bahaya laten PKI”. “Bahaya momok PKI” juga digunakan dengan tujuan untuk mengintimidasi atau melumpuhkan kekuatan pendukung Bung Karno. Dalam banyak hal, Suharto dkk menghantam terus “bahaya PKI” sebenarnya berarti juga menghantam Bung Karno.

Sekarang, situasi di Indonesia sudah mengalami perubahan selangkah demi selangkah atau sedikit demi sedikit. Oleh karena banyaknya kesalahan dan kebusukan rejm Orde Baru (antara lain : pelanggaran HAM yang banyak, pencekekan kehidupan demokratis, penyalahgunaan kekuasaan secara terang-terangan dan meluas, korupsi yang merajalela, kemerosotan moral yang parah, kemiskinan sebagian terbesar dari rakyat, pengangguran yang tinggi, kehidupan sehari-hari yang makin sulit bagi banyak orang) maka citra sisa-sisa pendukung Orde Baru (terutama golongan militer dan Golkar) sudah makin merosot. Bahkan, banyak sekali kalangan atau golongan yang makin yakin bahwa rejim militer Orde Baru adalah mala-petaka bagi negara dan bangsa Indonesia.

Sekarang, makin jelas bagi banyak orang, bahwa Suharto sama sekali bukanlah “pahlawan” yang menyelamatkan bangsa, dan bukan pula “bapak pembangunan” yang telah diagung-agungkan selama puluhan tahun. Tingkah lakunya dalam berbagai kasus KKN dan kehidupannya yang serba mewah dengan harta curian yang triliunan Rupiah (ingat kasus-kasus Tutut, Sigit, Bambang,Tommy) merupakan sebagian kecil dari kekobrokan Orde Baru.

Perjuangan juga mencakup yang non-kiri dan non-PKI
Oleh karena itulah maka dalam belasan tahun terakhir ini, nampak bahwa perlawanan banyak orang terhadap sisa-sisa politik dan praktek-praktek Orde Baru makin meningkat. Sekarang juga makin jelas bagi banyak orang bahwa perjuangan – dalam berbagai bentuk dan cara -- terhadap sisa-sisa Orde Baru adalah adil, benar, dan luhur. Oleh karenanya, perjuangan ini tidak hanya terbatas dalam golongan kiri yang pernah didholimi secara biadab dalam jangka lama sekali, melainkan mencakup juga golongan-golongan lainnya, termasuk yang non-kiri atau non-simpatisan PKI.

Situasi politik, ekonomi, sosial di negeri kita yang makin memburuk sekali akhir-akhir ini menyebabkan lahirnya gelombang besar aksi-aksi buruh, tani, pemuda dan mahasiswa yang menyuarakan berbagai tutntan, protes, dan kemarahan terhadap berbagai politik pemerintah SBY-JK. Aksi-aksi yang berbentuk lintas golongan atau lintas faham politik dan lintas agama ini telah dilakukan antara lain dalam merayakan Hari Buruh 1 Mei dimana dikibarkan banyak sekali bendera merah dan bahkan juga dilagukan Internasionale dan Darah Rakyat dll. Dalam banyak kegiatan-kegiatan masyarakat yang menentang kenaikan harga BBM, memperjuangkan kepentingan korban Lapindo dan banyak kasus-kasus lainnya, telah ikut berbagai kalangan dan golongan, termasuk golongan kiri dan simpatisan-simpatisan PKI yang mengambil bagian aktif.

Sekarang makin jelas bagi banyak orang bahwa perjuangan melawan sisa-sisa Orde Baru, menentang berbagai politik buruk pemeritahan SBY-JK, dan juga sekaligus melawan neo-liberalisme (terutama AS) adalah bukan hanya urusan golongan kiri atau simpatisan PKI saja, melainkan urusan atau tugas banyak golongan dan kalangan. Jadi, kalau nantinya di kemudian hari timbul gelombang besar aksi-aksi untuk menuntut adanya perubahan mendasar dan besar, itu bukanlah hanya “hasil hasutan” atau akibat kegiatan berbagai unsur-unsur PKI, yang menurut KASAD Jenderal Agustadi, “mulai makin nyata”.

Karena keadaan di Indonesia dewasa ini akan makin semrawut dan bobrok akibat berbagai kesalahan dan kejahatan para pengelolanya yang moralnya sudah rusak (dan imannya makin bejat) dan juga akibat resesi sistem kapitalisme di skala internasional maka pastilah akan muncul pula gerakan-gerakan rakyat luas untuk menuntut perbaikan di banyak bidang kehidupan. Seiring dengan bertambahnya kesulitan yang menyengsarakan banyak orang, pastilah akan bertambah juga perlawanan dari berbagai kalangan masyarakat, walaupun ada atau tidak ada “momok komunis”.

“Momok komunis” untuk mencegah perubahan fundamental
Bahwa banyak di antara simpatisan atau mantan anggota PKI mempunyai sikap yang anti Orde Baru, anti-golongan militer pendukung Suharto, anti-neo-liberalisme dan pro Bung Karno dan pro perubahan fundamental dan besar-besaran adalah wajar, karena ini sudah menjadi ciri PKI atau golongan kiri pada umumnya sejak lama. Jelaslah kiranya bahwa pendirian para simpatisan PKI yang demikian ini adalah sesuai atau sejiwa dengan pendapat dan aspirasi sebagian terbesar rakyat kita.

Perkembangan situasi dalamnegeri dan juga dalam skala internasional menunjukkan bahwa ungkapan KASAD Jenderal Agustadi mengenai momok “kebangkitan komunis” sudah “ketinggalan jaman”. Bukan itu saja! Ucapannya yang begitu itu juga memperlihatkan dengan jelas bahwa TNI yang di bawahnya tetap terus merupakan kekuatan yang menghambat atau menghalangi perubahan-perubahan besar yang mendasar yang menguntungkan sebagian terbesar rakyat. Ia masih tetap mau menggunakan “momok bahaya PKI” untuk mencegah adanya perubahan-perubahan besar dalam sistem kenegaraan kita, atau perubahan yang drastis di bidang politik, ekonomi dan sosial di negeri kita.

Dalam situasi nasional seperti yang kita hadapi bersama dewasa ini, yang membutuhkan perubahan besar-besaran dan fundamental, yang dibarengi dengan gelora di dunia melawan neo-liberalisme (dan imperialisme AS) sikap seperti yang dipertontonkan Jenderal Agustadi jelas-jelas hanyalah merugikan kekuatan perjuangan bersama untuk membela kepentingan rakyat Indoneia. Sebab, seperti sudah ditunjukkan dalam sejarah dimana-mana di dunia, golongan kiri (termasuk simpatisan-simpatisan komuns) adalah unsur atau bagian yang amat penting dari kekuatan untuk mengadakan perubahan.

Dengan perkataan lain, TNI kalau terus-menerus berada di bawah pimpinan orang-orang yang sejiwa seperti Jenderal Agustadi maka akan tetap merupakan musuh dari perubahan yang bisa mengantar negara dan rakyat kita menuju masyarakat adil dan makmur, sesuai dengan gagasan-gagasan besar Bung Karno. Kalau TNI tetap terus dibawa kearah yang reaksioner dan selalu menentang segala yang menjadi aspirasi rakyat banyak, maka akhirnya tidak bisa lain, yaitu : menjadi musuh rakyat !!!

Negara dan rakyat kita membutuhkan tentara yang dipimpin oleh orang-orang yang berjiwa non-Suharto atau non-Orde Baru, yang bisa menjadi peserta atau pengawal perubahan-perubahan besar, seperti yang ditunjukkan oleh Hugo Chavez di Venezuela dan negara-negara Amerika Latin lainnya.

Catatan tambahan: tidak lama lagi akan disajikan tulisan lainnya yang berkenaan dengan “Hari Kesaktian Pancasila”. Tulisan itu akan menelanjangi kebohongan Suharto dan para penguasa Orde Baru lainnya bahwa mereka menjunjung tinggi-tinggi atau menghormati Pancasila. Kenyataan selama puluhan tahun rejim militer Orde Baru sudah membuktikan dengan jelas bahwa mereka telah mengkhianati, merusak, memalsu, atau melecehkan jiwa asli Pancasila-nya Bung Karno.Tulisan ini sedang disiapkan.
