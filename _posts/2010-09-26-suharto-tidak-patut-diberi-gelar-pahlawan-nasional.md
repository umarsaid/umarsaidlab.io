---
layout: post
title: Suharto tidak patut diberi gelar pahlawan nasional
date: 2010-09-26
---

Mohon ma’af terlebih dulu kepada para pembaca bahwa dalam tulisan untuk mengenang peristiwa 30 September 1965 kali ini, banyak digunakan ungkapan-ungkapan yang barangkali kedengaran terlalu kasar, tidak sepatutnya, atau bahkan kelewatan tidak senonohnya. Sebab, tulisan ini memang merupakan  ledakan emosi atau letupan kemarahan setelah membaca berita bahwa daerah Jawa Tengah akan mengusulkan kepada pemerintah untuk memberi gelar pahlawan nasional kepada Suharto.

Berita tersebut disiarkan oleh Suara Pembaruan   tanggal 23 September 2010 (artinya, seminggu sebelum tanggal peristiwa 30 September), yang antara lain berbunyi sebagai berikut :

« Tim penilai pengkaji gelar pahlawan pusat tengah mengkaji 19 dari 30 tokoh yang diusulkan daerah untuk diberi gelar pahlawan nasional. Di antara deretan nama tersebut, Gus Dur dan Soeharto masuk nominasi pemberian gelar pahlawan. Tim penilai pengkaji gelar pahlawan tersebut, terdiri dari 13 orang yang berasal dari kalangan Masyarakat Sejarah Indonesia, akademisi, Arsip Nasional, dan Pusat Sejarah TNI.

Direktur Kepahlawanan Keperintisan dan Kesetiakawanan Sosial Kementerian Sosial (Kemsos), Suyoto Sujadi mengatakan, nama-nama tersebut, masih dikaji mendalam oleh tim, lalu diverifikasi Menteri Sosial untuk selanjutnya diserah ke Dewan Gelar di bawah naungan Presiden."Semua nama tersebut, masih dalam proses penggodokan di antaranya mantan presiden Gus Dur, Soeharto, Johanes Leimena, dan Ali Sadikin.

Gus Dur, tambah Suyoto, diusulkan dari daerah Jawa Timur, Soeharto dari Pemerintah Jawa Tengah. Sedangkan Jawa Barat mengusulkan lima nama, di antaranya Laksmi Ningrat. Nama-nama lain yang juga dinominasikan adalah Pakubowono X dan Kraeng Galengsong. Kriteria penting dalam menobatkan gelar pahlawan nasional menurutnya, sang tokoh harus memiliki bobot perjuangan yang terukur, baik skala lokal maupun nasional » (kutipan selesai).



Suharto tidak patut disebut pahlawan nasional.

Mungkin banyak orang yang terheran-heran membaca dalam berita di atas bahwa masih ada orang-orang yang menganggap bahwa Suharto pantas diberi penghargaan dengan gelar pahlawan nasional. Terlepas dari apakah orang-orang yang mengusulkan supaya Suharto diberi gelar pahlawan nasional itu waras nalarnya, ataukah sehat jiwanya, usul itu sendiri adalah betul-betul gila !

Sebab, sekarang makin banyak orang dari berbagai kalangan atau golongan yang makin sadar atau bahkan yakin bahwa Suharto adalah petinggi  Angkatan Darat, yang melakukan insubordinasi (pembangkangan) terhadap Panglima Tertinggi Angkatan Perang RI, dan mendongkel Presiden Sukarno dari kedudukannya melalui kudeta, kemudian membunuh pemimpin bangsa ini dengan menterlantarkannya secara tidak manusiawi dalam keadaan sakit parah selama dalam tahanan. Orang yang semacam ini tidaklah pantas sama sekali disebut sebagai pahlawan bangsa !

Banyak orang masih belum lupa bahwa selama sedikitnya 32 tahun  Suharto sudah memalsu dan menyalahgunakan Pancasila, sehingga Pancasila dibenci orang banyak (ingat Penataran P4). Ia juga sudah melarang disebarkannya ajaran-ajaran revolusioner Bung Karno. Dengan politiknya « de-Sukarnoisasi » yang menyeluruh ia sudah membunuh jiwa revolusi rakyat Indonesia, dan merusak jiwa revolusioner bangsa. Ini berarti bahwa ia sudah mengkhianati bangsa. Karenanya, ia tidaklah patut sama sekali disebut sebagai pahlawan bangsa !

Kalau kita amati situasi negara dan bangsa dewasa ini, yang penuh dengan kebejatan moral, korupsi yang mengganas di kalangan atas dan juga kalangan bawah, maka nyatalah bahwa keadaan yang membusuk ini adalah akibat langsung atau produk Orde Baru di bawah pimpinan Suharto. Suharto adalah perusak Republik Indonesia. Ia bukanlah seorang pahlawan nasional. Melainkan pengkhianat nasional.



Apakah Suharto memang punya bobot perjuangan?

Disebutkan  (dalam berita itu) bahwa « Kriteria penting dalam menobatkan gelar pahlawan nasional sang tokoh harus memiliki bobot perjuangan yang terukur, baik skala lokal maupun nasional ». Lalu, apakah memangnya Suharto memiliki bobot perjuangan yang terukur,  baik skala lokal maupun nasional ? Apakah pengkhianatannya terhadap Bung Karno bisa dikategorikan sebagai bobot perjuangan skala nasional ?

Bobot perjuangan Suharto sama sekali  tidak nampak ketika ia memimpin Orde Baru. Perjuangan apa atau terhadap siapa ? Terhadap kolonialisme atau imperialisme ? Sama sekali tidak ! Yang jelas dan pasti adalah banwa ia gigih dalam perjuangan terhadap seluruh kekuatan yang mendukung Bung Karno, yang terdiri dari golongan revolusioner, termasuk golongan PKI.  Puluhan juta orang di antara mereka telah diterror, diintimidasi, dibunuhi, dipenjarakan, didiskriminasi, disengsarakan (termasuk keluarga-keluarganya). Jadi, Suharto bukanlah pemersatu bangsa, melainkan sebaliknya,  ia adalah perusak persatuan bangsa.

Mengingat itu semua, usul untuk memberikan gelar pahlawan nasional kepada Suharto  jelas bisa menyakitkan hati banyak orang, yang pernah menjadi korban rejim militer yang dipimpinnya. Terutama dari kalangan pendukung politik  Bung Karno dan dari golongan kiri pada umumnya. Hal yang demikian adalah wajar, atau adalah manusiawi.



Menolak gelar pahlawan untuk Suharto adalah adil

Menolak atau menentang diberikannya gelar pahlawan kepada Suharto bukanlah sikap yang salah atau buruk, baik dilihat dari segi moral atau adat-istiadat maupun dari segi apa pun lainnya. Tidak juga berarti bahwa tidak menghargai jasa-jasanya kepada bangsa dan negara. Menolak atau menentang diberikannya gelar pahlawan nasional adalah adil, dan berdasarkan nalar yang waras, dan bersumber dari hati nurani kemanusiaan.

Sebab, menyetujui diberikannya gelar pahlawan kepadanya akan berarti mengakui bahwa apa yang dilakukan Suharto adalah benar. Pemberian gelar pahlawan kepadanya berarti bahwa pendongkelannya terhadao kedudukan Bung Karno, dan perusakannya terhadap Pancasila dan tujuan revolusi 45 adalah tindakan-tindakan yang baik. Yang lebih parah lagi, adalah bahwa pembunuhan massal terhadap jutaan orang tidak bersalah serta pemenjaraan begitu banyak orang lainnya (yang tidak bersalah apa-apa juga !!!) adalah tindakan Suharto yang dianggap tidak salah sama sekali. Wajarlah, kalau karenanya, ada orang-orang yang ngomel : « Orang yang begitu kok disebut pahlawan ! »

Kalau sudah begini, apa sajakah yang harus dikatakan mengenai jiwa atau hati nurani orang-orang Indonesia ? Bukankah ini menunjukkan jiwa yang sakit, atau nalar yang tidak beres, atau hati yang sudah membusuk ?  Atau, dengan kalimat lain, lalu negara apa namanya yang begini ini ? Orang yang membunuh  -- atau menyuruh bunuh --  begitu banyak orang, apalagi bangsanya sendiri,  diangkat sebagai pahlawan !!! Sungguh, sulit dimengerti bagi orang yang waras otaknya dan bersih hatinya.



Apakah betul rakyat Jawa Tengah mengusulkannya ?

Pengangkatan Suharto sebagai pahlawan nasional juga akan  berarti bahwa praktek  KKN yang sudah begitu terkenal di Indonesia dan juga di luarnegeri sudah dianggap tidak soal lagi atau dima’afkan saja.  Meskipun sebagian besar rakyat Indonesia, sebenarnya,  tidak akan bisa lupa kepada dosa-dosa besarnya. Jadi wajarlah  bahwa rakyat tidak akan membiarkan orang yang begitu itu dijadikan pahlawan nasional.

Soal lainnya lagi, adalah pernyataan bahwa daerah Jawa Tengah mengusulkan supaya Suharto diberi gelar pahlawan nasional. Apakah betul rakyat Jawa Tengah mengusulkannya ?  Siapa-siapa saja yang katanya mengusulkannya ? Sebab, sejarah sudah membuktikan bahwa dalam masa-masa yang lalu Jawa Tengah adalah daerah basis utama kekuatan pendukung Bung Karno dan juga kekuatan revolusioner umumnya, termasuk golongan PKI. Itu sebabnya, korban penindasan Orde Baru (antara lain : pembunuhan massal, pemenjaraan, persekusi-persekusi lainnya) juga besar sekali di Jawa Tengah. Karena itu pula, patut sekali diragukan bahwa rakyat Jawa Tengah mengajukan usul yang begitu aneh itu.

Kalau Suharto dijadikan pahlawan, maka akan merupakan soal yang bisa dipertanyakan oleh generasi-generasi yang akan datang, karena mereka toh akan tahu juga bahwa era rejim militer Orde Baru yang 32  tahun itu adalah era yang merupakan noda hitam dalam sejarah bangsa.  Jadi mereka bisa menjadi malu bahwa orang yang begitu kualitasnya toh bisa dihormati sebagai pahlawan oleh generasi pendahulunya.

Kalau Suharto diangkat sebagai pahlawan, maka banyak orang akan mengutuk atau menghujat orang-orang yang terlibat dalam proses pengusulan dan pemutusannya, baik di tingkat daerah maupun tingkat pusat. Pengutukan atau penghujatan terhadap orang-orang yang terlibat dalam kasus yang bisa menjadi skandal besar bangsa ini adalah benar, adil dan luhur. Sebab, rakyat atau bangsa kita haruslah bersikap adil terhadap kasus Suharto, tetapi juga harus adil terhadap mereka yang sudah paling sedikitnya 32 tahun menderita berbagai politiknya.



Suharto tidak mempunyai moral perjuangan untuk rakyat

Sekarang kita sama-sama melihat dari sejarah, bahwa berlainan sama sekali dengan Bung Karno, maka Suharto bukanlah seorang yang mempunyai gagasan-gagasan besar atau karya-karya penting bagi perjuangan bangsa. Suharto juga tidak memiliki moral perjuangan untuk rakyat setinggi moral Bung Karno.

Kalau Bung Karno sudah menyumbangkan kepada bangsa Indonesia karya-karya besarnya seperti Indonesia Menggugat, pidatonya tentang lahirnya Pancasila, kumpulan pidato-pidatonya dalam buku Dibawah Bendera Revolusi, Konferensi Bandung, Trisakti, pidato di PBB To build the World Anew, maka Suharto dalam 32  tahun tidak menghasilkan karya atau pemikiran yang benar-benar berbobot bagi sejarah bangsa.

Kalau Bung Karno sudah terbukti dalam sejarah bahwa ia pemersatu bangsa, dan dengan gigih berjuang untuk nation and character building, patriotisme dan internasionalisme revolusioner, maka Suharto sudah terbukti sebagai perusak persatuan rakyat, sebagai pembusuk character building bangsa, dan pengkhianat internasionalisme revolusioner.

Mengingat itu semua, maka gagasan atau usul untuk memberikan gelar pahlawan nasional kepada Suharto haruslah ditentang atau dilawan besar-besaran oleh berbagai golongan dalam masyarakat Indonesia. Usul semacam itu hanya akan mendatangkan berbagai hal negatif bagi rakyat. Karenanya, perlawanan  ini perlu dilakukan demi kebaikan bangsa kita seluruhnya, termasuk bagi anak cucu kita di kemudian hari.

Gelar pahlawan untuk Suharto tidaklah mendatangkan kebaikan bagi persatuan bangsa. Sebagian besar dari rakyat tidak merasa perlu dengan adanya penamaan yang begitu tinggi terhadap orang yang telah begitu besar membikin kerusakan, pembusukan, dan kebobrokan kepada bangsa dan negara. Bahkan, sebaliknya, yang justru pantas diberikan kepada Suharto  adalah pengkhianat bangsa !
