---
layout: post
title: Nasib Para Eks-tapol dan Korban Peristiwa 65/66
date: 2011-07-19
---

Para pembaca yang budiman. Dengan datangnya bulan puasa, yang merupakan bulan suci untuk orang Muslimin, maka berikut di bawah ini disajikan sebuah tulisan yang disiarkan oleh http://www.kaskus pada tanggal 16 Juli 2011,  untuk dipakai sebagai bahan renungan kita bersama.

Tulisan tersebut merupakan secuwil kecil saja dari masalah penderitaan ratusan ribu  eks-tapol, yang selama puluhan tahun selama Orde Baru telah mendapat perlakuan tidak manusiawi yang sangat menyedihkan.  Eks-tapol ini (dan berbagai macam korban rejim militer Orde Baru lainnya)  terdapat di seluruh daerah Indonesia.

Meskipun rejim militer Orde Baru sudah agak lama (sejak tahun 1989) berhasil digulingkan oleh gerakan massa pemuda dan mahasiswa dengan dukungan rakyat banyak, namun sampai sekarang penderitaan eks-tapol dan keluarga para korban peristiwa 65/66 masih terus berlangsung dalam berbagai bentuk dan bermacam-macam ukuran atau skala.

Tulisan yang disajikan  berikut ini memberikan gambaran sekilas tentang keadaan para eks-tapol serta kehidupan mereka di sebuah panti jompo di Jakarta

Untuk menyambut tulisan ini, dan juga melengkapinya dengan berbagai bahan renungan, maka disediakan sebuah komentar pada akhir tulisan tersebut.



Mantan Tapol G30S, Para Lansia yang Tidak Mau Jadi “Jompo”

(judul tulisan yang dikutip dari http://www.kaskus, yang selengkapnya  adalah sebagai berikut : )

« Panti jompo ini dikhususkan bagi para korban peristiwa G30S 1965 yang “tidak” memiliki keluarga. Rumah ini dibelikan oleh Taufiq Kiemas pada tahun 2003, dan diresmikan Abdurrahman Wahid pada 8 Februari 2004. Namun, rumah ini sempat ambruk pada 2006 akibat tertimpa pohon tumbang, sehingga para penghuninya sementara dipindahkan ke Depok, dan kembali lagi tahun 2007.

Dari sisi luar, Panti Jompo Waluya Sejati Abadi yang terletak di Jl Kramat 5 No 1C, Jakarta Pusat, itu tampak seperti rumah biasa. Berpagar besi setinggi 1,2 meter, tembok bercat krem, dan ubinnya berkeramik putih. Tapi begitu masuk ke dalam, kita akan menemukan sesuatu yang luar biasa. Para lansia yang terlihat renta berbalut baju daster itu merupakan para perempuan tangguh.

Ada 14 orang penghuni rumah itu, yaitu Budi (65), Pujiati (86), Sri Isnanto (83), Lestari (80), Sri Sulistiawati (71), Sri Widati (78), Sumirah (75), Tien Wartini (68), Palupi (69), Mujiati (69), Tahrin (72), Pak Marzuki (75), Pak Lukas Tumiso (71), dan Pak Ir Rosidi (86). Mereka merupakan korban peristiwa 1965 dan mantan tahanan politik (tapol). Di antara mereka ada mantan aktivis Gerakan Wanita Indonesia (Gerwani).

Lestari, Sri Sulistiawati, dan Ibu Pujiati ada di sana ketika saya bertandang. Mereka menjelaskan, Gerwani merupakan organisasi independen yang memperhatikan masalah sosialisme dan feminisme. Namun, dianggap Orde baru sebagai salah satu organisasi yang terlibat dalam peristiwa G30S. Setelah Soeharto menjadi presiden, Gerwani dilarang keberadaannya dan banyak anggotanya diperkosa dan dibunuh, seperti banyak orang yang dicurigai sebagai anggota PKI.

Mereka menjelaskan, betapa mereka dapat perlakuan yang sangat tidak manusiawi ketika itu. Seperti yang diceritakan oleh Lestari, yang pernah di penjara sebelas tahun sejak 1968-Desember 1979 di Lembaga Permasyarakatan Khusus Wanita di Blitar, Malang, Jawa Timur. Lestari saat itu ditempatkan di kamar tahanan yang diisi delapan orang. Setiap tahanan hanya dibekali satu lembar tikar tipis berukuran satu meter, dan tikar tersebut pun sudah gompal. “Udara di Malang sangat dingin, bayangkan bila kami harus tidur hanya beralaskan tikar. Untuk mengurangi rasa dingin, kami menjahit tikar-tikar itu jadi satu agar bisa menutupi lantai kamar tahanan yang dingin. Jadi kami tidur berjajar untuk mengurangi rasa dingin,” bebernya.

Ia juga pernah ditahan 11 tahun, mulai 18 Juli 1968–25 April 1979 di Penjara Bukit Duri, Jatinegara, Jakarta Timur. “Untuk alas tidur saya hanya diberi selembar tikar tipis. Kami para tapol diperlakukan jauh lebih kejam dari narapidana biasa. Untuk makan, kami hanya diberi nasi dengan seiris tempe rebus. Sedangkan narapida biasa diberi makanan cukup gizi seperti telur, daging sapi, daging, ayam, ikan, kacang hijau, dan buah-buahan. Kami diperlakukan seperti itu karena dianggap menentang pemerintah,” tambah Sri Sulistiawati.

Bahkan cerita Pujiati lebih mengerikan lagi. Ia menjalani masa tahanan mulai 10 Oktober 1965 di penjara Bukit Duri. Kemudian, tahun 1970 ia dipindahkan ke Penjara Plantungan, di daerah Sukoharjo, Kendal, Jawa Tengah. Penjara Plantungan sebelumnya adalah rumah sakit untuk penderita lepra (kusta).

“Tempat ini banyak sekali ular karena di belakangnya hutan belukar. Pemerintah memperlakukan kami dengan sangat tidak manusiawi. Bayangkan, setiap hari kami hanya diberi makan lima sendok nasi dengan lauk beling dan pasir, sampai-sampai kami tiap tahun punya pinset untuk mencabuti beling-beling halus. Ada 550 narapidana wanita yang menghuni tempat ini. Waktu saya sakit, saya dikirim ke Jakarta dan di rawat di RSPAD Gatot Subroto. Tapi di sana saya malah dicampur dengan orang gila. Bulan Desember 1979 barulah saya terbebas dari hukuman,” ungkap Pujiati.

Namun mereka mengaku sama sekali tidak menyesali apa yang telah menimpa mereka. “Kami menerima ini semua dengan lapang dada sehingga kami kuat, karena kami merasa benar. Kami ingin memperjuangkan nasib bangsa ini setelah kepemimpinan Soeharto”, kata Lestari. Ia ingin sekali bisa membentuk organisasi kewanitaan seperti dulu.

Sejak tahun 2003 hingga sekarang, belum ada subsidi dari Kementerian Sosial untuk panti jompo ini. Subsidi untuk panti jompo ini didapatkan dari siapa saja yang simpati. Panti jompo ini sudah mendapatkan subsidi tetap setiap bulan dari Dr Ribka Tjiptaning, Ketua Komisi IX DPR. Ada juga seorang pengusaha muda bernama Lulung yang membuatkan lemari-lemari pakaian dan mengajak rekreasi, serta sumbangan perorangan dan sembako dari gereja Katolik dan Protestan. Untuk pelayanan kesehatan, mereka dapat layanan cuma-cuma dari Klinik Kana di Gondangdia, Jakarta Pusat sejak 2008. Dari klinik ini, mereka tidak hanya mendapat pengobatan gratis, tapi juga sumbangan sembako dan rekreasi.

Tapi yang luar biasa, mereka tidak hanya menunggu sumbangan dari para simpatisan. Mereka juga terus berkarya untuk bertahan hidup dengan membuat berbagai macam kerajinan tangan, seperti bunga dari sedotan, tas, pajangan kaligrafi, dan seprei. Mereka juga terima banyak pesanan dari orang-orang. “Memang kami tinggal di panti jompo, tapi kami bukan orang jompo, tapi kami dijompokan. Kami dianggap bodoh oleh pemerintah, padahal otak kami tidak jompo. Otak kami masih produktif, dan masih mampu berkarya,” tegas Sri. (kutipan selesai)



Komentar (untuk direnungkan bersama)

Disajikannya tulisan tentang panti jompo di Jakarta, yang dihuni oleh para eks-tapol,  merupakan hal yang penting mengingat bahwa di tengah-tengah hiruk-pikuk tentang korupsi dan pelanggaran hukum (antara lain : kasus Nazaruddin, Andi Nurpati, Anas Urbaningrum) yang dewasa ini setiap hari membanjiri televisi dan media massa, maka masalah besar pelanggaran HAM akibat peristiwa 65/66 tidak bisa, dan tidak boleh ( !)  dilupakan begitu saja oleh banyak orang.

Sebab, kalau bangsa Indonesia melupakan saja, atau masa bodoh saja, atau diam saja, terhadap pelanggaran  kemanusiaan yang begitu besar itu, maka tidak pantas sama sekali mengaku sebagai bangsa yang katanya menjunjung Pancasila dan konstitusi 45.

Dibunuhnya jutaan orang tidak bersalah, dan dipenjarakannya ratusan ribu orang lainnya dalam jangka lama adalah kejahatan besar sekali rejim Suharto yang tidak bisa dima’afkan atau dibiarkan begitu saja   oleh generasi yang sekarang maupun oleh generasi-generasi yang akan datang.

Meskipun sebagian dari eks-tapol (atau para korban 65/66 lainnya) sudah banyak yang meninggal dunia karena tua atau karena sebab-sebab lainnya, namun banyak juga lainnya yang sampai sekarang masih terus hidup, walaupun sebagian terbesar dalam keadaan  yang teraniaya, tersisihkan, dan terlantar.

Mereka yang beruntung dapat ditampung dalam panti jompo di Jakarta itu hanyalah belasan orang. Mereka  dapat hidup dalam usia lansia, meskipun dalam segala macam keterbatasan, dan dalam berbagai macam penderitaan, baik secara batin dan  jasmani.

Padahal,  orang-orang  eks-tapol (atau korban-korban peristiwa 65/66 lainnya), seperti yang ditampung di panti jompo di Jakarta itu masih ada  ratusan ribu  yang terdapat di berbagai tempat di Sumatera, Jawa, Kalimantan, Sulawesi, Bali dan pulau-pulau lainnya.

Kebanyakan atau hampir semua mereka tidak bisa mendapat pertolongan semacam yang diterima oleh yang tinggal di panti jompo di Jakarta itu. Tidak diketahui apakah ada juga panti-panti jompo bagi eks-tapol (atau kornban 65/66) di Sumatera, Jawa Barat, Jawa Tengah, Jawa Timur, Kalimantan, Sulawesi atau pulau-pulau lainnya. Selama ini tidak ada berita-berita tentang adanya tempat-tempat untuk menampung para eks-tapol (atau korban 65/66) yang terlantar dan terpaksa hidup terlunta-lunta itu.

Karenanya, dapatlah kiranya dibayangkan betapa banyaknya para eks-tapol (dan korban peristiwa 65/66 lainnya) yang sudah lansia dan hidup menderita. Mereka banyak yang tidak bisa hidup terus menjadi beban saudara atau keluarga, tidak mendapat jaminan sosial, dan harus menghadapi banyak kesulitan karena masalah kesehatan dan usia tua .

Seandainya mereka masih berbadan sehat dan masih bisa bekerja , namun untuk mencari pekerjaan adalah sama sekali  tidakl mudah, mengingat usia mereka yang sudah lanjut. Sedangkan bagi yang muda-muda saja sudah sulit mencari pekerjaan, apalagi bagi mereka.

Lalu, siapa-siapa sajakah yang harus bertanggungjawab terhadap penderitaan yang menyedihkan begtiu banyak orang itu ? Mereka adalah juga warganegara yang sah, dan manusia biasa, seperti halnya orang-orang Indonesia lainnya. Namun, banyak di antara mereka itu yang tidak punya keluarga atau sanak saudara lagi. Banyak juga di antara mereka itu yang tadinya adalah aktivis-aktivis berbagai ormas, atau simpatisan PKI, yang mendukung berbagai politik Presiden Sukarno.

Mereka itu dijebloskan dalam penjara atau tempat tahanan lainnya secara sewenang-wenang, tanpa pengadilan, hanya dengan cap (dakwaan atau tuduhan yang tidak berdasar) sebagai tersangkut G30S/PKI. Padahal, mereka itu tidak mempunyai kaitan atau tidak tahu menahu sama sekali tentang G30S. Mereka waktu itu hidup atau tinggal jauh dari Jakarta, umpamanya di Sumatera, atau di Jawa Tengah dan Jawa Timur atau Bali.

Jadi, jelaslah kiranya bagi kita semua, bahwa mereka tidak bersalah apa-apa, tidak berdosa sedikitpun, tidak melanggar hukum sama sekali, dan bukan penjahat, atau bukan pengganggu keamanan dan ketertiban masyarakat.  Kebanyakan di antara mereka adalah orang-orang yang (laki dan perempuan) yang mau dengan sukarela berjuang untuk masyarakat adil dan makmur dan mendukung berbagai politik Bung Karno yang revolusioner dan anti-imperialis atau anti-kapitalisme.

Mengingat itu semua, maka perlulah masalah penderitaan eks-tapol (dan korban peristiwa 65/66 lainnya) yang umumnya sudah lanjut usia  (dan bahkan sudah dekat dengan akhir hidup mereka ) mendapat perhatian dari kalangan luas, baik dari pemerintah maupun masyarakat. Perhatian ini perlu sekali, demi rasa kemanusiaan dan keadilan, dan hati nurani atau nalar yang sehat.

Sebab, orang-orang yang sudah tua-tua ini umumnya telah dipenjarakan dengan sewenang-wenang dan secara salah ( !!!) selama jangka lama, atau diperlakukan tidak manusiawi,  walaupun tidak bersalah apa-apa sama sekali. Untuk kesalahan atau kejahatan yang begitu besar terhadap banyak orang itu, pimpinan Angkatan Darat atau pemerintah tidak pernah menyatakan penyesalan mereka dan minta ma’af (kecuali Gus Dur).

Dengan tidak pernah menyatakan penyesalan dan permintaan ma’af atas kesalahan-kesalahan mereka di masa yang lalu, maka sekarang membiarkan terus penderitaan orang-orang yang sudah tua dan mendekati akhir hidup mereka ini adalah suatu sikap yang tidak bisa dianggap menghargai perintah-perintah agama atau Tuhan.
