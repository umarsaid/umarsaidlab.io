---
layout: post
title: Kasus Gayus bukti bahwa kita memerlukan revolusi rakyat
date: 2010-11-18
---

Seperti yang bisa kita amati bersama, masalah kasus besar Gayus Tambunan yang sudah membikin heboh di seluruh negeri  berbulan-bulan, akhir-akhir ini meledak seperti letusan gunung Merapi dengan tersiarnya berita beserta fotonya ketika ia kepergok menonton pertandingan tennis internasional di Bali. Berita ini menjadi seperti letusan gunung api yang besar, karena kemudian disusul dengan informasi-informasi yang menyebutkan bahwa Gayus Tambunan , yang resminya masih menjadi tahanan di Rumah Tahanan Brimob Kelapa Dua di Depok, telah bisa keluar masuk tahanan sampai 68 kali selama beberapa bulan ini, dengan menyuap Kepala Rumah Tahanannya, Kompol Siswanto, sampai Rp 368 juta.

Karena Gayus bisa keluar masuk tahanan, antara lain untuk menginap di rumahnya yang mewah di Kelapa Gading dan bertemu dengan istri dan anak-anaknya, Kompol Iwan Siswanto bersama 8 anggota kepolisian lainnya, yang juga menerima suap, telah dibebas-tugaskan dan akan diajukan ke depan pengadilan.

Kasus keluar masuknya Gayus dari tahanan, dan kepergiannya ke Bali menjadi persoalan besar, karena diduga bahwa ia pergi ke Bali itu tidak hanya untuk melihat pertandingan tennis saja, melainkan ada tujuan-tujuan lainnya, yang berkaitan dengan urusan-urusan besar yang melibatkan tokoh-tokoh penting negara kita.

Berhubung menjadi makin pentingnya  persoalan Gayus Tambunan dalam pembongkaran mafia pajak dan mafia hukum  dan korupsi yang merajalela di kalangan penegak hukum atau aparat negara,  maka  website http://umarsaid.free.fr/ selanjutnya akan berusaha menyajikan sesering mungkin --  dan sebanyak mungkin --  aneka-ragam berita, tulisan, atau bahan-bahan informasi, tentang kasus besar ini.

Bukti bahwa negara kita makin rusak

Kasus Gayus Tambunan membuktikan lebih jelas lagi kepada kita semua bahwa negara Republik Indonesia memang sudah betul-betul rusak, karena pimpinan aparat kepolisiannya bobrok, karena moral tokoh-tokoh kejaksaannya membusuk, dan karena akhlak para hakim-hakimnya juga banyak yang rusak.

Kasus Gayus Tambunan sudah menunjukkan bahwa  slogan besar yang digembar-gemborkan selama ini tentang perlunya penegakan hukum adalah hanya omong kosong yang makin menjijkkan untuk didengar. Sebab, dalam realitasnya, sistem hukum di negeri kita terbukti sudah keterlaluan bobroknya, sehingga sebenarnya Republik Indonesia  - tetap terus - tidak pantas atau tidak berhak disebut sebagai negara hukum.

Selama pemerintahan Suharto (Orde Baru) banyak sekali berbagai pelanggaran HAM telah menyebabkan penderitaan jutaan orang (pembantaian dan pemenjaraan secara besar-besaran dan sewenang-wenang terhadap warganegara yang tidak bersalah) sehingga sulitlah untuk menganggap Republik Indonesia di bawah pemerintahannya  sebagai negara hukum, seperti yang biasanya banyak terdapat di dunia.

Republik Indonesia di bawah pimpinan Suharto (dan diteruskan oleh pemerintahan-pemerintahan sesudahnya) tidak bisa dengan gagah digembar-gemborkan sebagai negara hukum,  selama orang-orang yang seharusnya bertanggungjawab terhadap pembunuhan dan pemenjaraan – sekali lagi : sewenang-wenang ! --  terhadap jutaan orang kiri dan pendukung politik Bung Karno didiamkan saja atau tidak diapa-apakan sama sekali.

Selama pemerintahan Orde Baru nyata sekali bagi banyak orang, bahwa hukum berarti kekuasaan bagi penguasa-penguasa militer (dengan dukungan dari berbagai kalangan sipil) untuk mengangkangi seluruh kehidupan bangsa dan negara, dengan cara-cara yang tidak demokratis sama sekali, atas kerugian  banyak orang.

Hukum dikuasai para elite untuk kejahatan

Sekarang ini, di bawah  pemerintahan SBY (juga pemerintahan-pemerintahan sebelumnya) hukum masih tetap dikuasai, atau dipermainkan, atau disalahgunakan, atau digunakan, atau dijual-belikan oleh para elite (kalangan atas) dalam melakukan berbagai kejahatan dan penyelewengan, untuk melakukan  korupsi dalam segala bentuk dan cara.

Kasus Gayus Tambunan telah mengungkap bahwa sebagai pegawai pajak tingkat 3A (termasuk rendahan) sudah bisa mempermainkan, dan menyalahgunakan seluk-beluk hukum, untuk mengumpulkan uang haram dari berbagai perusahaan (lebih dari 50) pembayar pajak sampai ratusan miliar Rupiah.

Karena sebagian tindakannya menerima suapan dari banyak pembayar pajak ini mulai terdeteksi, maka ia telah secara berani, dan besar-besaran, serta meluas menyuap para penyidik dari kepolisian, kejaksaan, dan pengadilan, sehingga jumlah uang suapan ini mencapai ratusan miliar Rupiah.

Dengan banyaknya uang suapan yang telah diterimanya dari berbagai perusahaan pembayar pajak, Gayus telah berhasil selama ini mengaduk-aduk berbagai instansi di Mabes Polri, Kejaksaan Agung, dan Kementerian Hukum serta Satgas Mafia Hukum yang berada di bawah Presiden SBY.

Sejak tersiarnya berita tentang keluarnya Gayus dari rumah tahanan Brimob dan kemudian hadirnya dalam  pertandingan  tennis internasional di Bali bersama istrinya, maka kasus besarnya ini menjadi bahan yang dipersoalkan ramai sekali dalam pers dan televisi. Banyak siaran-siaran dalam Metro TV dan Tvone mengenai kasus ini telah membongkar berbagai aspek betapa rusaknya moral di kalangan elite bangsa kita, dan betapa bobroknya dunia hukum negeri kita.

Sepuluh atau dua puluh tahun lagi tetap impian saja

Kerusakan moral yang menjadi salah satu di antara berbagai faktor tentang bobroknya dunia hukum negeri kita sudah begitu parahnya, sehingga wajarlah kiranya bahwa kita menjadi pesimis apakah masih bisa diperbaiki dalam jangka dekat, walaupun diadakan penertiban dan perombakan di sana-sini. Kalau kerusakan moral ini masih tetap terus merajalela seperti yang kita saksikan sekarang ini, maka dalam sepuluh atau dua puluh tahun, penegakan hukum adalah tetap jadi impian saja. Dan Republik Indonesia akan menjadi makin rusak.

Timbulnya kasus Gayus Tambunan, sebagai tambahan deretan kasus-kasus besar lainnya selama ini (antara lain kasus Anggoro, Bank Century, BLBI) memperkuat pendapat bahwa berbagai macam sistem politik, ekonomi, dan sosial yang diwariskan Orde Baru merupakan sumber dari banyak kebobrokan.

Kita semua dapat bertanya-tanya mengapa ketika negara ada di bawah pemerintahan Bung Karno dulu itu tidak ada korupsi yang   begitu merajalela seperti sekarang ini ?  Di antara para lansia kita juga akan bisa dengan tegas mengatakan bahwa kerusakan moral atau kebejatan akhlak di kalangan elite bangsa tidak kelihatan menyolok di berbagai bidang seperti yang kita saksikan sekarang ini.

Kiranya, kita bisa melihat kasus Gayus Tambunan juga sebagai salah satu produk dari sikap yang yang reaksioner, yang anti-rakyat, yang anti-nasional, yang tidak peduli terhadap kepentingan orang banyak, yang hanya mementingkan kekayaan pribadi dan keluarga saja, yang merupakan sampah bangsa.  Ciri-ciri yang begini inilah yang biasanya dipunyai orang-orang atau kalangan yang mendukung Suharto dengan Orde Barunya.

Kasus Gayus Tambunan juga akan menunjukkan kepada kita semua, bahwa persoalannya akan lama dan sulit ditangani, berhubung banyaknya kalangan yang bermoral bejat atau berakhlak rusak yang ikut-ikut « menanganinya ». Terlalu banyak orang-orang yang mau dengan berbagai jalan dan cara mencari kesempatan untuk menerima suapan dari Gayus (atau jaring-jaringannya)  yang masih menyimpan uang haram sebanyak ratusan miliar Rupiah itu. Termasuk para tokoh yang ada di lembaga-lembaga tinggi negara.

Diperlukan revolusi untuk perubahan besar-besaran

Karena itu, kita tidak selayaknya untuk berharap bahwa segala macam « tokoh » yang mempunyai tugas untuk menyelesaikan kasus Gayus ini akan menjalankan tugas mereka sebaik-baiknya, termasuk kalangan terdekat presiden SBY sendiri. Sebab, kita bisa menduga-duga bahwa kasus Gayus ini bukanlah semata-mata kasus pribadinya sendiri saja. Melainkan ada kaitannya dengan kepentingan-kepentingan besar, atau, bahkan, suatu scenario raksasa. Mari sama-sama kita amati dengan cermat selanjutnya.

Itu semua menjadi dasar bagi kita untuk meyakini bahwa Republik Indonesia kita tidak akan bisa menyelesaikan penegakan hukum dan memberantas habis-habisan korupsi, atau segala macam penyelewengan lainnya,  dengan pimpinan atau tokoh-tokohnya seperti yang ada sekarang ini.

Situasi yang serba semrawut di bidang hukum dewasa ini, yang sebagian kecilnya dipertunjukkan oleh kasus Gayus, memberikan isyarat yang jelas bahwa negara kita memang memerlukan perubahan besar-besaran dan fundamental.

Dan perubahan besar-besaran atau fundamental ini hanya bisa dicapai melalui revolusi rakyat, yang jalannya sudah ditunjukkan oleh ajaran-ajaran revolusioner Bung Karno. Seperti yang sama-sama kita alami sendiri selama ini, jalan lainnya adalah jalan buntu, atau jalan sesat.
