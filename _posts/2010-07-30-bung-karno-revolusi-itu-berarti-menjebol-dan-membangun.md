---
layout: post
title: 'Bung Karno : « Revolusi itu berarti menjebol dan membangun ! »'
date: 2010-07-30
---

Menyambung tulisan « Bangsa Indonesia perlu meneruskan revolusi !!! » beberapa waktu yang lalu, berikut ini disajikan kutipan-kutipan dari pidato Bung Karno pada Hari Kemerdekaan 17 Agustus 1964 (artinya 1 tahun sebelum terjadinya peristiwa G30S) yang pernah terkenal dengan judul « Tahun Vivere Pericoloso »  disingkat TAVIP (hidup secara berbahaya, bahasa Italia).

Bagi mereka yang pernah mendengar (atau membaca) pidato « Tahun Vivere Pericoloso » akan melihat bahwa Bung Karno memang satu-satunya pemimpin Indonesia yang betul-betul kranjingan untuk mengajak  bangsa Indonesia menjadikan diri sebagai bangsa revolusioner dengan melakukan Revolusi terus-menerus¸ demi kepentingan rakyat banyak.

Dalam satu pidato 200 kali kata Revolusi

Kranjingannya atau gandrungnya Bung Karno kepada kerevolusioneran bangsa Indonesia ini tercermin dalam banyak sekali  pidato-pidatonya (atau karya-karyanya). Boleh dikatakan bahwa Bung Karno-lah satu-satunya pemimpin Indonesia yang telah mencurahkan banyak tenaga dan fikirannya supaya bangsa Indonesia menjadi bangsa yang menempuh perjuangan revolusioner dan mempunyai cita-cita dan watak patriotisme yang tinggi dalam mengabdi kepada tanah-air dan bangsa.

Salah satu contohnya yang tipikal dan jelas  (serta kuat !)  sekali menunjukkan siapakah Bung Karno, bagaimana sikapnya terhadap Revolusi, apa hukum-hukum Revolusi menurutnya, adalah pidatonya « Tahun Vivere Pericoloso » tersebut.  Dalam satu pidatonya yang ini saja, Bung Karno menyebutkan kata-kata Revolusi  (atau revolusioner) sebanyak lebih dari 200 kali. Sungguh, luar biasa !

Bagi semua orang yang masih juga mempunyai fikiran bahwa Revolusi yang selalu dianjurkan Bung Karno kepada bangsa Indonesia adalah sesuatu yang menakutkan, yang selalu merupakan tindakan yang berdarah-darah, atau kegiatan yang hanya bunuh-membunuh, atau pembrontakan  dan amuk-amukan yang sembarangan saja,  dan pengrusakan yang hanya menimbulkan anarchi atau kekacauan,  seyogianya berusaha membaca atau mempelajari  (dengan baik-baik) karya-karya Bung Karno. Bukan, bukan, dan sekali lagi bukan hal-hal negatif yang begituanlah yang menjadi ajaran-ajaran revolusioner Bung Karno.

Dengan  membaca karya-karyanya (dan mendengarkan pidato-pidatonya) kita semua akan mengerti bahwa Revolusi yang dikobarkan dan digemuruhkan Bung Karno  bersama-sama rakyat Indonesia selama bertahun-tahun adalah mobilisasi rakyat Indonesia untuk menempuh patriotisme kerakyatan, nasionalisme revolusioner, dan internasionalisme, yang semuanya ditujukan untuk menghilangkan « exploitation de l’homme par l’homme » (pemerasan manusia oleh manusia) dan « exploitation des nations par  les nations «   (pemerasan bangsa-bangsa oleh bangsa-bangsa).  Tujuan Revolusi yang begitu besar, begitu luhur dan begitu indah inilah yang menjadi tujuan Bung Karno bersama pendukung-pendukungnya.

Dalam pidato « Tahun Vivere Pericoloso » (dapat dibaca di buku Dibawah Bendera Revolusi, jilid II, hal 559 – 598) terdapat banyak kalimat-kalimat atau ungkapan Bung Karno yang sangat penting sekali untuk diketahui oleh semua orang yang ingin menilai dan menghormatinya sebagai seorang tokoh revolusioner yang sungguh-sungguh besar.

Oleh karena itu, akan diusahakan tidak lama lagi adanya  tulisan-tulisan  yang lain, yang menyajikan kutipan-kutipan pidato Bung Karno tentang Revolusi, yang tidak atau belum bisa disajikan kali ini, mengingat banyaknya atau panjangnya. Dalam tulisan kali ini baru bisa didahulukan kutipan mengenai enam hukum-hukum Revolusi, yang lengkapnya  adalah seperti yang di bawah ini :

Enam hukum-hukum Revolusi menurut Bung Karno

Kata Bung Karno : »Apa hukum-hukum revolusi itu ? Hukum-hukum Revolusi itu, kecuali garis-besar romantika, dinamika, dialektika yang sudah kupaparkan  tadi, pada pokoknya adalah :

Pertama,  Revolusi mesti punya kawan dan lawan, dan kekuatan-kekuatan Revolusi harus tahu siapa kawan dan siapa lawan ; maka harus ditarik garis-pemisah yang terang dan harus diambil sikap yang tepat terhadap kawan dan terhadap lawan Revolusi ;

Kedua, Revolusi yang benar-benar Revolusi bukanlah « revolusi istana «  atau « revolusi pemimpin », melainkan Revolusi Rakyat ; oleh sebab itu, maka Revolusi tidak boleh « main atas » saja, tetapi harus dijalankan   dari atas dan dari bawah ;  (huruf miring dari teks aslinya  Pen.)

Ketiga, Revolusi adalah simfoninya destruksi dan konstruksi, simfoninya penjebolan dan pembangunan, karena destruksi atau penjebolan saja tanpa konstruksi atau pembangunan adalah sama dengan anarchi, dan sebaliknya ;  konstruksi atau pembangunan saja tanpa destruksi atau penjebolan berarti kompromi, reformisme

Keempat, Revolusi selalu punya tahap-tahap ;  dalam hal Revolusi kita :  tahap nasional-demokratis dan tahap Sosialis,  tahap yang pertama meretas jalan buat yang kedua, tahap yang pertama harus dirampungkan dulu, tetapî sesudah rampung harus ditingkatkan ke tahap yang kedua  -- inilah dialektik Revolusi ;

Kelima, Revolusi harus punya Program yang jelas dan tepat, seperti dalam Manipol kita merumuskan dengan jelas dan tepat : (A) Dasar/tujuan dan Kewajiban-kewajiban  Revolusi Indonesia ; (B) Kekuatan-kekuatan sosial Revolusi Indonesia ; (C) Sifat revolusi  Indonesia, (D) Hari depan Revolusi Indonesia ; dan (E) Musuh-musuh Revolusi Indonesia. Dan seluruh kebijaksanaan Revolusi harus setia kepada Program itu ;

Keenam, Revolusi harus punya sokoguru yang tepat dan punya pimpinan yang tepat, yang berpandangan jauh–kemuka, yang konsekwen, yang sanggup melaksanakan tugas-tugas Revolusi sampai pada akhirnya, dan Revolusi juga harus punya kader-kadernya yang tepat pengertiannya dan tinggi semangatnya.

Demikianlah hukum-hukum Revolusi » (Kutipan panjang selesai, dari DBR halaman 572-573)



Seruan Bung Karno masih relevan untuk sekarang



Hukum-hukum Revolusi yang diungkapkan Bung Karno seperti tersebut di atas menunnjukkan dengan jelas sekali kepada kita semua bahwa ia benar-benar memang tokoh politik revolusioner, yang mempunyaii fikiran-fikiran progressif dan mengandung ciri-ciri Marxisme.

Ungkapannya bahwa Revolusi yang benar-benar Revolusi bukanlah « revolusi istana » atau « revolusi pemimpin » dan bahwa  Revolusi tidak boleh « main atas » saja, tetapi harus dijalankan   dari atas dan dari bawah adalah  fikiran revolusioner yang contoh-contohnya bisa dilihat  sekarang (antara lain ) di Revolusi  China, Vietnam, Kuba, Venezuela, Bolivia, Ekuador

Ketika Bung Karno mengatakan bahwa Revolusi adalah simfoninya destruksi dan konstruksi atau simfoninya penjebolan dan pembangunan ia dengan tepat dan juga  bagus sekali memakainya untuk menghadapi situasi negara dan bangsa kita pada waktu itu, yang memang  masih memerlukan banyak sekali penjebolan dan pembangunan.

Alangkah masih relevannya ungkapan Bung Karno  itu kalau dihubungkan dengan situasi negara dan bangsa kita dewasa ini !!! Begitu banyak yang harus sama-sama di-destruksi (dijebol) dan di-konstruksi (dibangun) oleh bangsa kita  Terlalu banyak kebrengsekan dan kebusukan yang melanda hampir di seluruh bidang yang harus dijebol untuk kemudian dibangun kembali demi kepentingan rakyat banyak.

Mengenai tahap-tahap Revolusi, Bung Karno juga amat jelas merumuskannya dengan  mengatakan bahwa  untuk  Revolusi kita perlulah dirampungkan dulu  tahap yang pertama , yaitu Revolusi nasional demokratis yang meretas jalan buat tahap yang kedua , yaitu tahap Sosialis. Inilah jalan yang harus kita tempuh bersama, menurut pandangan jauh Bung Karno.
Jadi, seperti yang sering dinyatakannya dalam berbagai kesempatan, Bung Karno adalah - pemimpin Indonesia yang jelas-jelas menganjurkan jalan Sosialisme untuk hari kemudian bangsa dan negara Indonesia.



Situasi dewasa ini  yang penuh dengan kebejatan moral

Seperti sudah disebutkan diatas, pidato « Tahun Vivere Pericoloso » adalah pidato Bung Karno yang penuh dengan kata-kata Revolusi, yang diutarakannya secara menggebu-gebu, menggeledeg, menggelora dan berkobar-kobar. Meskipun dalam pidato-pidatonya yang lain ia juga bicara banyak tentang Revolusi, namun yang kali ini adalah yang paling menonjol.

Oleh karena itu adalah sangat penting bagi kita semua, terutama yang mau ikut dalam usaha bersama dalam perjuangan besar-besaran yang oleh Bung Karno dirumuskan dengan  kata-kata Revolusi (yang berarti penjebolan dan pembangunan) untuk menghayati ajaran-ajaran revolusionernya, dan berusaha bersama-sama mewujudkannya dalam praktek.

Seruan Bung Karno kepada bangsa Indonesia (waktu itu) untuk mengobarkan terus-menerus Revolusi, pada dasarnya berlaku juga bagi kita semua dewasa ini, kalau kita memang benar-benar ingin melakukan perubahan besar-besaran dan fundamental di segala bidang, dengan terus-menerus mengadakan penjebolan dan pembangunan.

Situasi negara dan bangsa kita dewasa ini sudah demikian rusaknya  dan busuknya, dengan melandanya kebejatan moral dan kemerosotan akhlak serta kebusukan iman (terutama di kalangan elite atau atasan) , sehingga tidak mungkin bisa diatasi atau diperbaki dalam waktu dekat dan dengan tindakan-tindakan yang hanya merupakan tambal sulam saja.



Revolusi rakyat dari dari atas dan dari bawah

Begitu besarnya kerusakan dan parahnya pembusukan itu sehingga tidak adalah pemikiran  atau tindakan, dari pemimpin Indonesia yang mana pun atau partai politik yang mana pun yang mampu memperbaikinya. Inilah yang sudah terjadi selama ini, dan juga akan tetap terjadi di masa depan,  kecuali dengan Revolusi, Yaitu Revolusi yang berupa penjebolan dan pembangunan (destruksi dan konstruks), yang dilakukan dengan rakyat, atau dari rakyat, oleh rakyat, dan untuk rakyat, dan juga dari atas dan dari bawah, seperti yang diajarkan oleh Bung Karno.

Namun, Revolusi yang harus dilancarkan  bersama-sama oleh sebagian terbesar rakyat kita itu bukanlah soal yang gampang dan  bisa dilakukan sembarangan saja. Enam hukum-hukum revolusi yang dikemukakan Bung Karno tersebut di atas merupakan pedoman penting bagi kita semua, di samping perlunya kita terus mempelajari juga ajaran atau pengalaman dari tokoh-tokoh  revolusioner besar dunia lainnya beserta pelaksanaan perjuangan mereka.

Kiranya kita semuanya menyadari bahwa  pekerjaan atau tugas  Revolusi yang harus diselesaikan bangsa adalah berat dan besar, dan juga rumit sekali. Sebab,  yang perlu dijebol, diganti, atau dirobah adalah amat banyak. Di antaranya adalah bertumpuknya  segala hal-hal negatif dan menyengsarakan rakyat yang sudah dilakukan rejim militer Suharto selama 32 tahun, dan sisa-sisanya  masih diteruskan sampai sekarang.

Beratnya dan besarnya serta rumitnya pekerjaan Revolusi yang harus diselesaikan bangsa kita ini disebabkan karena segala kebusukan dan keburukan yang merugikan kehidupn rakyat itu dibela atau dipertahankan oleh  berbagai macam sisa-sisa Orde Baru beserta simpatisan-simpatisannya yang masih terdapat di mana-mana beserta segala macam kekuatan reaksioner lainnya yang anti-Bung Karno dan anti-kiri.  Karenanya, kekuatan anti-Revolusi ini adalah amat besar dan kuat sekali.



Penggalangan front yang seluas-luasnya untuk Revolusi

Mengingat  itu semuanya, maka segala jalan dan usaha perlu ditempuh oleh semua kalangan dan golongan yang menginginkan terjadinya perubahan besar dan fundamental di negeri kita, untuk menggalang bersama-sama berbagai macam front atau persekutuan atau aliansi (walaupun sementara)   yang luas dan kuat guna menghadapi kekuatan anti-Revolusi.

Semua peserta dalam perjuangan besar ini harus berusaha mentrapkan enam hukum-hukum Revolusi yang sudah dirumuskan Bung Karno dan memperhatikan semua unsur dan syarat-syaratnya (siapa kawan dan lawan, sifat Revolusi rakyat, dilaksanakannya Revolusi dari atas dan dari bawah, simfoninya penjebolan dan pembangunan, tahap-tahap Revolusi sampai tahap Sosialis, Program Revolusi yang tepat, pentingnya sokoguru dan pimpinan yang tepat  dan berpandangan jauh, dan kader-kader yang bersemangat tinggi.

Untuk bisa terbangunnya kekuatan besar yang berupa pendukung, pendorong atau penggerak Revolusi yang demikian ini dibutuhkan waktu yang panjang , banyak tenaga dan fikiran, dan pengorbanan-pengorbanan  yang tidak sedikit. Penggalangan kekuatan  revolusioer ini bisa dilakukan melalui  berbagai cara atau jalan dan dengan macam-macam bentuk, antara lain  mengadakan aksi-aksi untuk membela kepentingan rakyat dan memperjuangkan tuntutan massa luas demi kehidupan lebih baik, dan dengan gerakan-gerakan untuk melawan segala macam penindasan oleh bangsa sendiri maupun bangsa asing,.

Segala macam aksi atau kegiatan revolusioner untuk membela kepentingan rakyat ini merupakan cara penting untuk meningkatkan kesedaran massa rakyat untuk mendukung revolusi dan juga langkah penting sekali untuk mempersiapkan pelaknsanaan revolusi. Kesedaran politik revolusioner yang tinggi dari massa rakyat banyak (terutama dari kalangan buruh, tani, rakyat miskin umumnya) adalah syarat bagi berhasilnya revolusi.  

Jumlah rakyat yang sudah dirugikan  -- dengan  berbagai bentuk dan cara -- oleh pemerintahan Orde Baru adalah amat besar, yang terdiri dari macam-macam golongan (jadi  bukan hanya golongan komunis saja,  melainkan juga dari golongan-golongan lainnya, termasuk golongan Islam). Demikian juga mereka yang jadi korban politik neo-liberalisme yang dianut pemerintahan dewasa ini, termasuk kalangan menengah (pengusaha kecil dll). Mereka ini semualah yang perlu dipersatukan di bawah bendera revolusi.

Sekali lagi, berdasarkan pengalaman jatuh-bangun  dan kegagalan yang sudah berkali-kali, yang sudah dilalui selama  lebih dari 40 tahun sejak pemerintahan Suharto, dan tidak adanya perspektif lainnya untuk menyetop proses kemerosotan dan pembusukan lebih lanjut, maka makin jleaslah bahwa tidak ada jalan lain kecuali jalan Revolusi yang sudah ditunjukkan oleh Bung Karno.

Yaitu jalan destruksi dan konstruksi atau jalan penjebolan dan pembangunan yang dilakukan terus-menerus, demi kepentingan rakyat banyak dan demi  Negara yang berdasarkan Pancasila, Bhinneka Tunggal Ika dan ajaran-ajaran revolusioner Bung Karno lainnya.
