---
layout: post
title: DPR kita dan masalah kemiskinan
date: 2008-04-06
---

Berita koran Tempo tanggal 2 April 2008 seperti di bawah ini menunjukkan, untuk kesekian kalinya, kepada kita semua bahwa berbagai lembaga negara di Indonesia memang sudah kelewatan brengseknya. Yang berikut ini adalah hanya secuwil kecil saja dari kebobrokan yang membikin pemerintahan RI penuh dengan berbagai macam penyelewengan dan penyalahgunaan kekuasaan, termasuk korupsi yang merajalela. Silakan baca sambil urutlah dada Anda, untuk menahan kejengkelan atau meredam kemarahan :

“ Di tengah gencarnya penyidikan Komisi Pemberantasan Korupsi atas dugaan suap Bank Indonesia, sejumlah dokumen yang kemarin diterima Tempo menunjukkan praktek serupa diduga kembali terulang pada periode 2006-2007. Salah satunya berupa aliran dana bank sentral kepada beberapa anggota Badan Legislasi Dewan Perwakilan Rakyat.

Dalam beberapa dokumen memorandum dan disposisi itu disebutkan, sehubungan dengan berakhirnya pembahasan Rancangan Undang-Undang Mata Uang, Bank Indonesia mengajak empat anggota Badan Legislasi DPR melawat ke London dan New York. Kunjungan dilakukan selama 10 hari, dari 3 hingga 12 Maret 2007.

Selain menanggung ongkos perjalanan, bank sentral memberikan uang saku kepada empat anggota Dewan itu. Jumlahnya Rp 1 juta dan US$ 13.960 (setara dengan hampir Rp 130 juta).

Anggaran pembiayaan lawatan itu disetujui Deputi Gubernur Budi Rochadi, atas permintaan Direktur Perencanaan Strategis dan Hubungan Masyarakat Budi Mulya melalui surat tertanggal 19 Februari 2007.

Menurut dokumen-dokumen itu, para anggota Badan Legislasi yang ikut melawat ke dua kota di Inggris dan Amerika Serikat itu adalah Bomer Pasaribu, Ali Masykur Musa, Andi Rahmat, dan Ganjar Prastowo.

Kecuali Ganjar, yang tak bisa dilacak keberadaannya di DPR (yang ada hanya nama anggota yang mirip dengan itu), ketiga wakil rakyat yang namanya tercantum dalam dokumen mengeluarkan bantahan.

Sementara itu, dua LSM melaporkan bahwa selama 2006-2007 terdapat dana di Bank Indonesia yang kucurannya dicurigai bermasalah. Dana itu adalah untuk anggota DPR sebesar Rp 2,5 miliar dan US$ 145 ribu Sebanyak 52 anggota, mayoritas anggota Komisi Keuangan dan Perbankan, menerima dana ini, dari untuk biaya perawatan kesehatan, dana partisipasi, sampai uang saku perjalanan dinas. (Harap baca berita selengkapnya dalam website http://kontak.club.fr/index.htm.).

Banyak mengecewakan rakyat

Mohon diperhatikan bahwa berita tersebut menyangkut terlibatnya anggota-anggota DPR “yang terhormat” dan pejabat-pejabat tinggi dalam Bank Indonesia, yang dua-duanya adalah instansi atau lembaga negara yang penting. Dengan membaca berita tersebut di atas mungkin saja di antara kita ada yang bisa mempunyai pendapat dan juga pertanyaan yang macam-macam, dari yang wajar-wajar saja sampai ke yang sinis atau, bahkan, yang aneh-aneh.

Umpamanya : mengapa Bank Indonesia sampai menganggap perlu membiayai perlawatan anggota-anggota DPR ke London dan New York, dengan beaya yang begitu besar? Apakah ini bukan sebagai “suapan” dalam bentuk lain, dalam rangka berakhirnya pembahasan Rancangan Undang-Undang Mata Uang? Apa perlunya anggota-anggota Badan Legislasi DPR itu melawat ke London atau New York? Kalau mau “studi banding” apa yang bisa dipelajari oleh para anggota DPR di Lodon atau New York dalam waktu yang begitu singkat? Apakah anggota-anggota DPR tersebut di atas tidak merasa “risi” atau “segan” mendapat pelayanan yang demikian dari Bank Indonesia di tengah-tengah banyaknya berita tentang kelaparan dan kemiskinan di antara rakyat kita?

Tulisan ini ikut memuntahkan kemarahan penulis, karena mendengar tingkah laku atau perangai para anggota DPR, seperti yang dipertontonkan oleh mereka yang “pesiar” atau foya-foya ke London dan New York, atas beaya uang rakyat. Kemarahan atau kejengkelan ini bukan hanya karena peristiwa ini saja, melainkan karena sudah lama DPR kita memperlihatkan banyak hal-hal lainnya yang sangat mengecewakan banyak orang.

Kita semua masih ingat bahwa sudah sejak lama banyak kritik diajukan oleh berbagai kalangan dan golongan masyarakat terhadap plesiran yang sering dilakukan oleh anggota-anggota DPR ke berbagai negara, yang sebetulnya hanya jalan-jalan atau pesiar ke luarnegeri, dengan diberi label yang gagah dan mentereng “studi banding”. Banyak orang marah waktu itu, karena “studi banding” ini ternyata sering merupakan penghamburan uang rakyat, dan hasilnya hanya sedikit sekali, kalaupun ada. Kita ingat juga bahwa ada usul untuk menaikkan gaji anggota DPR dengan angka-angka yang membikin banyak orang kaget, karena gaji mereka sudah cukup tinggi.

Yuk ramé-ramé jadi anggota DPR .......yuuuk ......

Sebagai bahan untuk pemikiran bersama, di bawah ini disajikan satu tulisan yang disiarkan oleh mailing-list cfbe@yahoogroups.com tanggal 25 Februari 2008, yang mengutip dari tulisan Agus Susanto di Kompas tanggal 21 Januari 2008. Judul tulisan itu berbunyi : “Yuk ramé-ramé jadi anggota DPR ......yuuuk..........”, dan selengkapnya adalah sebagai berikut :

"Dulu modal saya untuk jadi anggota Dewan Perwakilan Rakyat itu Rp187 juta. Enam bulan pertama sudah BEP, break even point." Sembari makan siang di kantin, seorang anggota Dewan menceritakan pengalamannya secara blak-blakan kepada wartawan. Dia juga menceritakan bagaimana praktik-praktik politik uang yang terjadi di DPR yang tidak bisa diceritakan dalam tulisan ini. Karena itu, dia termasuk yang tidak setuju dengan berbagai kebijakan anggaran di DPR yang arahnya terus menguras uang negara demi mempertebal ”kantong” anggota Dewan. Dia merasa berbagai fasilitas yang selama ini dia terima sudah lebih dari cukup.

Pemberian insentif legislasi Rp 1 juta ke semua anggota Dewan yang tidak terlibat dalam pembahasan setiap kali pengesahan rancangan undang-undang, menurut dia, salah satu kebijakan yang tidak tepat.

Dua tahun terakhir Seorang anggota Dewan lain secara blak-blakan menunjukkan seluruh catatan penghasilan yang dia terima dari negara selama dua tahun terakhir. Dari catatan itu diketahui, penerimaan anggota DPR terbagi menjadi tiga kategori. Ada yang bersifat rutin bulanan, ada yang rutin nonbulanan, dan ada juga yang sesekali.

Yang sifatnya rutin bulanan adalah gaji paket Rp15.510.000; bantuan listrik Rp5.496.000; tunjangan aspirasi Rp7,2 juta; tunjangan kehormatan Rp3,15 juta; tunjangan komunikasi intensif Rp12 juta; Dan tunjangan pengawasan Rp2,1 juta.. Total berjumlah Rp46,1 juta perbulan.

Jadi,setahun mencapai lebih dari setengah miliar, Rp 554 juta.

”Pendapatan bulanan ini semua anggota DPR sama,” katanya. Penerimaan nonbulanan banyak jenisnya, mulai dari penerimaan gaji ke-13 setiap Juni Rp 16,4 juta dan dana penyerapan aspirasi setiap masa reses Rp 31,5 juta. Dalam satu tahun sidang ada empat kali masa reses.

Ada juga dana perjalanan dinas komisi, perjalanan dinas ke luar negeri,atau perjalanan dinas saat reses. Total keseluruhan dalam setahun sekitar Rp 188 juta.

Sementara itu, penghasilan yang sifatnya sewaktu-waktu adalah insentif pembahasan rancangan undang-undang dan honor Melakukan uji kelayakan dan kepatutan yang besarnya Rp 5 juta per kegiatan. Dengan adanya kebijakan baru berupa uang insentif legislasi Rp 1 juta per-RUU, semakin menambah lagi pemasukan anggotaDPR. Uang insentif legislasi yang dia terima Rp 39,7 juta. Apabila keseluruhan penerimaan negara itu dihitung, total uang yang diterima seorang anggota DPR dalam setahun hampir Rp 1 miliar. Sebagai anggota DPR yang tidak terlalu aktif saja, selama tahun 2006, dia menerima Rp 761,3 juta, sedangkan tahun 2007 Rp 787, 1 juta. Anggota Dewan yang merangkap anggota badan selain komisi juga mendapat tunjangan khusus. Demikian pula anggota yang merangkap pimpinan alat kelengkapan, banyak melakukan studi banding ke luar negeri, memimpin panitia-panitia khusus pembahasan RUU, serta menjadi pimpinan fraksi, atau pimpinan DPR. Dengan uang yang diberikan negara itu, dia yakin semua anggota DPR bisa menjadi profesional, independen, dan bersungguh-sungguh memperjuangkan aspirasi rakyat. Namun, kalau ditanya soal cukup, menurut dia, setiap orang akan memiliki pandangan yang berbeda. ”Ibarat minum air, ada yang merasa cukup, ada juga yang malah semakin haus,” ucapnya sambil tertawa.

Idealisme 550 anggota DPR yang duduk di Senayan memang beragam. Mereka tidak bisa begitu saja digeneralisasi. Terkait pemberian insentif legislasi Rp 1 juta saja, misalnya, ada fraksi yang menolak dan ada fraksi yang menerima dengan sejumlah alasan. Anggota yang memiliki idealisme seperti tadi sesungguhnya tak hanya satu, dua. Namun, karena jumlahnya kalah banyak, suara mereka sering kali tertelan. Seorang anggota Dewan yang dulu bergelut di dunia akademisi dan sekarang terjun ke politik praktis malah mengaku sempat juga terkena getahnya. Saat dia ke kampus, rekannya menyesalkan dirinya terjun ke dunia politik praktis karena menjadi ikut ”kotor”. Tidak semua kotor

Menilai anggota DPR seluruhnya ”kotor” tentu tak tepat karena pada kenyataannya ada juga yang berusaha untuk ”bersih” di tengah kekeruhan. Yang perlu dilakukan adalah memberikan dukungan kepada mereka yang bersih agar mereka tak tercemar, tetapi malah membawa warna jernih. DPR yang bersih akan membawa pemerintahan juga menjadi bersih karena salah satu fungsi DPR adalah bidang pengawasan. Anggaran di eksekutif juga beratus-ratus kali lipat anggaran di DPR. Siapakah anggota DPR yang perlu didukung itu? Tentunya, mereka yang bisa merasakan cukup dan lebih memprioritaskan orang yang kerongkongannya kering karena dahaga............

Semakin Diberi "Air", Semakin Haus" (kutipan tulisan habis di sini).

Apakah sebagian terbesar dari isi tulisan tersebut di atas mencerminkan hal-hal yang benar atau akurat atau tidak, perlulah kiranya ada tulisan lainnya (dari siapa saja) yang bisa memberikan konfirmasi, atau juga koreksi, atau tambahan penjelasan mengenai berbagai soal yang berkaitan dengan gaji, atau berbagai pendapatan anggota-anggota DPR kita. Sebab, ini ada sangkut-pautnya juga dengan masalah moral, ethiek, martabat, kehormatan, kewibawaan, dan mutu anggota-anggota DPR kita, yang sudah makin banyak dipersoalkan oleh banyak orang.

Banyaknya cacian dan hujatan terhadap DPR
Banyaknya cacian atau hujatan berbagai kalangan terhadap tingkah-laku atau pekerjaan para anggota DPR yang tidak beres adalah hal yang baik sekali. Ini manifestasi yang menggembirakan bahwa rakyat ikut aktif mempersoalkan berbagai urusan negara, dan juga mengawasi pekerjaan anggota-anggota DPR. Sebab, persoalan negara sama sekali bukanlah hanya urusan pejabat-pejabat pemerintahan atau angggota-anggota DPR saja, melainkan adalah urusan rakyat banyak juga. DPR hanyalah mewakili rakyat, yang tugas utamanya adalah membela kepentigan rakyat.

Jadi, banyaknya protes atau kritik, atau seringnya demonstrasi dan aksi-aksi dari berbagai kalangan rakyat mengenai berbagai ketidakberesan pengurusan negara, yang ditujukan kepada pemerintah dan DPR, adalah suatu hal yang perlu didukung secara positif sekali oleh semua kekuatan demokratis. Sebab, aksi-aksi atau berbagai macam kegiatan untuk menentang segala kebobrokan atau ketidakberesan pengelolaan negara sangatlah dibutuhkan secara mutlak, karena DPR sudah sering sekali menunjukkan sikap atau tindakan yang tidak menguntungkan atau tidak membela kepentingan rakyat.

Marilah kita simak dan bandingkan gaji ditambah pendapatan-pendapatan lainnya yang diterima para anggota DPR (tidak semuanya) yang bisa berjumlah sampai Rp 1 miliar setahun itu dengan banyaknya kemisikinan yang menimpa secara luas rakyat kita. Menurut Suara Pembaruan (11 Juli 2007) , Badan Dunia yang menangani masalah pangan , World Food Programme (WFP) memperkirakan anak Indonesia yang menderita kelaparan akibat kekurangan pangan saat itu berjumlah 13 juta.

Kita juga masih ingat bahwa ada sekitar 4 juta anak balita yang kekurangan gizi bahkan banyak yang busung lapar. Di samping itu kira-kira ada separo rakyat Indonesia yang tergolong miskin, dan banyak yang terpaksa hidup dibawah 1 dollar ( kurang lebih 10.000 Rupiah) sehari per orang. Orang-orang miskin ini sekarang harus menderita lebih parah lagi, dengan naiknya harga-harga sembako (beras dan bahan pangan pokok lainnya) dan dengan banyaknya bencana (banjir, hujan yang terus-menrus, gempa, jalan rusak))

Indonesia membutuhkan kekuasaan politik tipe baru

Menghadapi kesulitan-kesulitan besar dan parah yang dialami sebagian besar rakyat kita itu, berbagai kalangan dan golongan sudah bangkit, dengan mengadakan macam-macam aksi. Banyaknya dan beraneka-ragamnya aksi-aksi ekstra-parlementer ini menunjukkan bahwa kesadaran rakyat terhadap masalah politik, sosial dan ekonomi, makin meningkat. Mereka makin melihat bahwa berbagai politik atau tindakan pemerintahan dan DPR, seperti yang dilakukan selama ini, sudah gagal untuk tercapainya kesejahteraan bagi sebagian terbesar rakyat Indonesia.

Sekarang makin jelas bagi banyak orang bahwa negara dan bangsa kita tidak bisa terus-menerus dikangkangi oleh kekuasaan politik yang seperti sekarang ini. Juga tidak bisa oleh kekuasaan politik dari hasil Pemilu 2009, yang pada dasarnya toh akan sama saja. Rakyat sudah terlalu banyak yang menderita, sudah terlalu lama, dan sudah terlalu parah pula ! Berdasarkan pengalaman negatif pemerintahan Orde Baru yang selama 32 tahun ditambah dengan lebih dari 10 tahun pemerintahan-pemerintahan pasca-Suharto, Indonesia membutuhkan kekuasaan politik tipe baru, tipe yang betul-betul mengutamakan kepentingan rakyat di atas segala-galanya.
