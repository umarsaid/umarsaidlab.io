---
layout: post
title: Memperingati HUT Bung Karno di bawah bendera revolusi
date: 2001-04-07
---

Renungan dan catatan tentang BUNG KARNO (4)

Sekarang ini, sulit ditaksir berapakah kiranya jumlah buku "Dibawah Bendera Revolusi", yang berisi karya-karya Bung Karno, masih beredar di seluruh Indonesia. Mungkin, tidak banyak lagi. Juga sulitlah agaknya menebak-nebak, apakah mudah untuk mendapatkannya secara lengkap, baik dalam toko-buku maupun lewat saluran-saluran lainnya. Sebab, buku ini sudah lama sekali diterbitkan (lebih dari 35 tahun). Di samping itu, selama itu pula, banyak orang merasa takut untuk ketahuan bahwa memilikinya. Sistem politik (dan kebudayaan) Orde Baru telah membikin banyak orang takut atau segan berbicara tentang dirinya, tentang sejarah perjuangannya, dan tentang karya-karyanya. Oleh karenanya, banyak sekali orang (apalagi generasi yang sekarang) yang tidak tahu tentang adanya buku ini. Kalaupun pernah mendengarnya, maka tidak mengetahui isinya.

Begitu hebatnya kampanye "de-sukarnoisasi" yang dilakukan secara permanen dan sistematis terhadap sejarah bangsa kita, dan begitu parahnya trauma yang diakibatkan oleh tindakan para pendiri Orde Baru terhadap Bung Karno, sehingga banyak orang tidak berani menyimpan (atau menjual) buku-buku yang berisi karya-karyanya (pidato, ceramah, tulisan dll). Bahkan, konon, tidak sedikit yang terpaksa menyembunyikannya rapat-rapat, membakarnya atau menghancurkannya secara diam-diam. Oleh karenanya, amatlah sulit sekarang ini, untuk bisa mendapatkan buku "Dibawah Bendera Revolusi" ini, baik dikota-kota besar Indonesia, maupun (atau, apalagi!) dikota-kota kecil. Dalam banyak perpustakaan umum maupun perpustakaan universitas dan lembaga-lembaga penting pun, buku ini juga sudah "menghilang" atau dihilangkan.

Setelah sama-sama melampaui masa Orde Baru, maka nyatalah bahwa dalam rangka penghayatan sejarah perjuangan bangsa, dan pendidikan patriotisme dan nasionalisme, atau juga pemupukan moral bangsa dan semangat pengabdian kepada kepentingan rakyat, maka penyebarluasan buku-buku yang memuat ajaran atau gagasan Bung Karno adalah penting sekali. "Dibawah Bendera Revolusi" adalah salah satu di antaranya. Dari buku inilah kita akan dapat menelusuri kembali perkembangan fikiran atau gagasannya yang berkaitan erat dengan problem-problem nasional dan internasional yang terjadi baik sebelum Republik Indonesia diproklamasikan maupun sesudahnya. Dalam buku inilah kelihatan betapa cemerlangnya pemikiran-pemikirannya di bidang politik, betapa agungnya wawasannya di bidang persatuan bangsa, dan betapa besarnya kecintaannya kepada rakyat, dan terutama kepada rakyat "kecil".

## Buku Buku Bung Karno Perlu Diperbanyak

Zaman memang sudah berobah, waktu pun sudah bergulir panjang dan lama. Banyak problem yang dihadapi bangsa Indonesia kita dewasa ini juga jauh berbeda dengan yang dihadapi sebelum berdirinya Orde Baru. Di samping itu, situasi internasional pun sudah berobah, yang mendorong juga terjadinya perobahan-perobahan di Indonesia, termasuk perobahan dalam cara berfikir banyak orang. Namun, ketika membaca kembali karya-karya atau gagasan-gagasan Bung Karno, antara lain yang tertera dalam buku "Dibawah Bendera Revolusi", maka akan nampaklah bahwa banyak pandangan-pandangannya yang, sekarang ini, masih tetap relevan untuk dijadikan referensi. Dengan kalimat lain, bisalah kiranya dikatakan bahwa banyak pemikiran-pemikiran dasarnya yang masih tetap bisa dijadikan pegangan atau patokan untuk memandang berbagai persoalan yang sedang dihadapi bangsa kita dewasa ini. Mengingat itu semuanya, maka terasa sekalilah sekarang ini, betapa pentingnya untuk menerbitkan kembali karya-karya Bung Karno sebanyak mungkin dan juga menyebar-luaskannya. Dengan usaha bersama untuk menyebar-luaskan gagasan-gagasan besar Bung Karno, maka kita akan memberikan sumbangan kepada bangsa untuk :

* Menemukan kembali arah tepat yang perlu bersama-sama ditempuh menuju cita-cita masyarakat adil dan makmur, mengingat bahwa Orde Baru telah membawa bangsa kita ke arah yang sesat selama puluhan tahun
* Membangkitan kembali bangsa, sehingga kesadaran akan pentingnya kesatuan dan persatuan bangsa menjadi pulih kembali, dan terhindar dari perpecahan yang berbau SARA
* Memupuk semangat kerakyatan dan semangat mengabdi kepada rakyat, patriotisme dan nasionalisme (yang tidak sempit)
* Menggugah semangat revolusioner seluruh bangsa, untuk bisa terus memperjuangkan tercapainya cita-cita revolusi 17 Agustus di bidang politik, ekonomi, sosial dan kebudayaan.

Sekarang ini, suasana politik di Indonesia sudah memungkinkan untuk mngumandangkan lagi suara Bung Karno. Setelah kepengapan udara selama lebih dari 32 tahun, bangsa kita butuh menghirup angin segar atau hawa baru. Berbagai karya asli atau gagasan otentik Bung Karno yang bisa disebar-luaskan dalam berbagai bentuk juga akan merupakan vitamin bagi banyak orang untuk melanjutkan revolusi. Karya-karya ini seyogyanya bisa diterbitkan dalam brosur-brosur kecil, sehingga terjangkau oleh banyak orang. Di samping itu, berbagai ceramah atau seminar (atau tukar-fikiran dalam bentuk-bentuk lainnya) perlu bersama-sama digelar sebanyak mungkin di mana-mana.

## Sumber Ilham Yang Tiada Kering Keringnya

Dalam merenungkan pengalaman selama Orde Baru, maka terasa sekalilah bahwa bagi banyak orang terjadi kekosongan kepemimpinan moral yang berbobot dan kekosongan guru bangsa yang bisa menjadi sumber ilham perjuangan. Dengan tetap menghormati sejumlah tokoh di berbagai bidang yang berhak untuk mendapat penghargaan dan penghormatan, maka patutlah diakui bersama bahwa Bung Karno, bagaimana pun juga, adalah pemimpin bangsa yang terbesar selama ini. Ia besar bukan hanya berkat pandai berpidato atau mengobarkan semangat banyak orang, melainkan (dan, bahkan, terutama sekali!) berkat kebesaran ajaran-ajarannya. Sebagian besar dari ajaran-ajarannya yang penting itu terhimpun dalam buku "Dibawah Bendera Revolusi".

Sampai akhir tahun 1965, "Dibawah Bendera Revolusi" yang sudah beredar luas terdiri dari dua jilid. Jilid pertama terdiri dari 650 halaman, yang memuat dokumen-dokumen penting karya Bung Karno. Antara lain, yang berjudul :

* Nasionalisme, Islam dan Marxisme
* Indonesia Menggugat
* Demokrasi politik dan demokrasi ekonomi
* Memperingati 50 tahun wafatnya Karl Marx
* Indonesia versus fasisme
* dll. dll.

Sedangkan jilid kedua (598 halaman), memuat 20 pidato kenegaraan Bung Karno setiap tahun (dalam rangka memperingati 17 Agustus) sejak tahun 1945 sampai 1964. Dalam jilid kedua ini dapat kita baca antara lain :

* "Tahun tantangan",
* "Penemuan kembali Revolusi kita",
* "Jalannya revolusi kita",
* "Revolusi-Sosialisme Indonesia-Pimpinan Nasional",
* "Tahun kemenangan",
* "Genta suara Republik Indonesia",
* "Tahun Vivere Pericoloso".

Dalam kata pendahuluan buku jilid kedua (diterbitkan 20 Agustus 1964), ketua Panitia Penerbit Dibawah Bendera Revolusi, H. Mualliff Nasution (pembantu terdekat Bung Karno), menulis antara lain sebagai berikut :

"Seiring dengan tekad bangsa Indonesia untuk mengganyang terus proyek Neo-Kolonialis "Malaysia", khusus untuk jilid kedua ini dihimpun 20 buah pidato 17 Agustus dari Presiden Soekarno, dengan maksud bukan sekadar untuk memudahkan penelitian dan peninjauan kembali jalannya Revolusi Indonesia serta bahan perbandingan kemajuan antara babak yang satu dengan lainnya, tetapi dengan maksud utama, adalah agar himpunan 20 pidato 17 Agustus ini, tetap memberi api baru - memberi dorongan dan kekuatan baru - bahkan menjadi pusaka keramat untuk menjadi bimbingan yang menyeluruh ke arah tujuan Revolusi Indonesia, yaitu terbentuknya masyarakat yang adil dan makmur berdasarkan Panca Sila.

Sesuai dengan penjelasan pada jilid pertama, maka himpunan 20 buah pidato 17 Agustus ini, bukan hanya sekadar untuk penghias lemari-buku dan memperkaya khasanah perpustakaan dengan sebuah buku yang bernilai dokumenter yang menjadi sumber bagi penulisan sejarah Revolusi Indonesia, tetapi adalah agar menjadi pegangan hidup yang senantiasa segar - menjadi sumber ilham yang tiada kering-keringnya - baik bagi generasi sekarang maupun generasi yang akan datang, demikian pula agar menjadi bahan penelitian ilmiah bagi ahli politik, ahli sosiologi, ahli hukum dan lain sebagainya.

Karena itu, persembahan himpunan 20 buah pidato 17 Agustus ini kepada rakyat Indonesia, adalah dengan maksud agar kita semua mempunyai pegangan yang sama menggerakkan Revolusi Indonesia menuju kemenangan" (kutipan habis).

## Pusaka Untuk Meneruskan Revolusi

Mengingat besarnya kerusakan-kerusakan yang telah ditimbulkan oleh Orde Baru di berbagai bidang selama lebih dari 32 tahun, maka patutlah kiranya pada kesempatan Peringatan 100 Tahun Bung Karno sekarang ini, seluruh kekuatan pro-reformasi dengan berani - dan dengan lantang pula - bersuara kepada siapa saja bahwa Bung Karno adalah milik bersama seluruh bangsa Indonesia. Seluruh kekuatan pro-reformasi perlu dengan tegas menyatakan bahwa pusaka (warisan) berharga yang berupa ajaran-ajarannya adalah jalan untuk mempersatukan kembali bangsa kita, yang sekarang ini sedang di ambang perpecahan yang serius.

Perlulah diusahakan - dengan berbagai jalan dan bentuk - berkembangnya kesadaran di kalangan sebanyak mungkin golongan dalam masyarakat bahwa gagasan-gagasan Bung Karno (sejak muda sampai wafatnya) adalah demi kepentingan rakyat banyak, demi revolusi menuju masyarakat adil dan makmur. Oleh karenanya, dalam menghadapi situasi politik yang ruwet dewasa ini, ajaran-ajaran Bung Karno dapat dijadikan ikatan bersama untuk menyelesaikan reformasi. Pusaka Bung Karno ini dapat digunakan oleh seluruh kekuatan pro-reformasi untuk meneruskan revolusi, meneruskan pembangunan kekuatan bangsa di berbagai bidang.

Dengan makin dikenalnya ajaran-ajaran Bung Karno secara luas, maka akan makin nyatalah bagi banyak orang bahwa situasi yang diwariskan oleh Orde Baru - dan juga sistem politik yang dilakukan oleh rezim militer selama lebih dari 30 tahun - adalah bertentangan sama sekali dengan ajaran-ajarannya. Pengenalan atau penghayatan secara baik ajaran-ajaran Bung Karno akan memungkinkan bagi banyak orang untuk lebih mengerti mengapa ia telah digulingkan oleh para pendiri Orde Baru dan kekuatan-kekuatan asing (waktu itu). Dengan membeberkan sejarah perjuangannya dan menyampaikan dengan gamblang ajaran-ajarannya, maka banyak orang akan makin yakin bahwa Bung Karno adalah pejuang besar nasionalis yang dimusuhi oleh Orde Baru.

Mungkin saja, bahwa dalam rangka Peringatan 100 Tahun Bung Karno ini, akan muncul suara-suara yang serba negatif terhadapnya. Bahwa ada sikap yang bersifat kritis terhadap kesalahan-kesalahan Bung Karno, adalah wajar, atau bahkan baik. Tetapi, kalau ada orang-orang yang bersikap semata-mata memusuhi ajaran-ajaran Bung Karno dan meniadakan sama sekali jasa-jasanya terhadap perjuangan bangsa, adalah sesuatu yang patut disayangkan! Sebab, ketika kesalahan para pendiri Orde Baru terhadap Bung Karno sekarang ini sudah makin terbelejedi, maka patutlah disayangkan bahwa mereka itu masih mau meneruskan kesalahan-kesalahan itu. (Tentang soal ini ada catatan tersendiri).

## Mengangkat Bung Karno Demi Rekonsiliasi Nasional

Agaknya, yang berikut ini bisa kita jadikan renungan bersama. Yaitu : mengangkat kembali Bung Karno bukanlah hanya masalah sejarah. Memang, sejarah tentang Bung Karno telah diusahakan untuk dibikin gelap oleh para pendiri Orde Baru beserta para pendukungnya, yang terdiri dari segala macam orang dan dari berbagai golongan ( bersyukurlah kita, hendaknya, bahwa sekarang ini banyak juga di antara mereka yang sudah mengobah pandangan salah mereka terhadapnya). Tetapi, usaha bersama untuk mengangkat kembali Bung Karno pada tempat yang selayaknya juga merupakan kebutuhan AKTUAL dan URGEN bagi bangsa kita dewasa ini, khususnya dalam merajut kembali kerukunan bangsa dalam rangka rekonsiliasi nasional.

Sebab, perlulah sama-sama kita sadari - dan juga tidak usah kita tutup-tutupi, - bahwa masalah Bung Karno pernah dijadikan sumber pertentangan oleh para pendiri Orde Baru beserta para pendukung setia mereka (yang, sekali lagi, sudah mulai banyak yang berobah). Bung Karno, yang seluruh hidupnya disumbangkan untuk mempersatukan bangsa demi perjuangan melawan kolonialisme Belanda dan imperialisme - dan membangun Republik Indonesia - telah dijadikan faktor perpecahan bangsa, dan, juga, dalam jangka yang lama sekali. Kesalahan mereka inilah yang sekarang ini harus dibetulkan bersama-sama. Demi persatuan kembali bangsa, demi kerukunan antar-golongan, antar-suku, antar-agama, antar-pandangan politik, maka Bung Karno perlu dijadikan lagi sebagai simbul persatuan.

Dalam kaitan betapa pentingnya Bung Karno sebagai simbul persatuan bangsa adalah menarik untuk dikemukakan dalam tulisan kali ini sikap yang diambil oleh wartawan senior Haji Mahbub Djunaidi, yang pernah menjabat sebagai anggota DPR-GR/MPRS, pemimpin redaksi suratkabar NU "Duta Masyarakat" (1960-1970), Ketua Umum PWI Pusat (1965-1970) dan Ketua DPP NU (1986). Ketika masa mudanya ia juga menjabat sebagai Ketua Umum yang pertama kali PMII (organisasi mahasiswa di bawah NU) dalam tahun 1960-1967.

Dalam buku "Mahbub Djunaidi, Asal Usul" ( diterbitkan oleh Kompas, 1996) wartawan senior M. Said Budairy (bekas sekjen PWI Pusat dan pimpinan Duta Masyarakat) menulis berbagai aspek tentang perjalanan hidup kawan dekatnya itu, dan juga tentang pandangan atau sikap Mahbub mengenai Bung Karno, Pancasila, Pramoedya Ananta Toer dll. Dalam tulisannya itu, bung Said Budairy antara lain menulis :

"Dalam suatu tulisan di harian Duta Masyarakat Mahbub mengemukakan pendapatnya bahwa Pancasila mempunyai kedudukan yang lebih sublim dibanding Declaration of Independence susunan Thomas Jefferson yang menjadi pernyataan kemerdekaan Amerika Serikat tanggal 4 Juli 1776, maupun dengan Manifesto Komunis yang disusun oleh Karl Marx dan Friedrich Engels tahun 1874. Tulisan itu dibaca oleh Bung Karno, yang kemudian minta kepada KH Saifudin Zuhri agar Mahbub diajak ke istana" (kutipan habis).

Dalam buku "Sketsa Kehidupan dan surat-surat pribadi sang pendekar pena MAHBUB DJUNAIDI" (1996, penyunting Ridwan Saidi dan Husein Badjerei), ada tulisan Ketua Umum Yayasan Pendidikan Soekarno, Rachmawati Soekarnoputri, yang berupa sambutan terhadap terbitnya buku tersebut. Di situ dapat kita baca, antara lain, sebagai berikut :

Bapak H. Mahbub Djunaidi memang salah seorang pendekar Indonesia. Beliau dikenal sebagai pendekar pena, karena ketajaman analisanya atas peristiwa, sementara arah tulisannya tetap bertolak dari keyakinan cita-cita sebagai landasan ideologi. Beliau juga dikenal sebagai pendekar politik dari partai agama melalui lingkungan Nahdlatul Ulama, yang taat kepada azas hablum minannas, yakni hubungan yang setara antara manusia dengan manusia.

Namun di sisi lain, saya juga mengenal Bapak Haji Mahbub Djunaidi oleh sikap ksatrianya yang hampir tidak mungkin ditemui pada siapa pun dalam dekade setelah tujuhpuluhan. Suatu sikap tulus yang mengakui kebenaran ajaran Bung Karno, dengan menamakan sebuah beranda depan rumahnya di Bandung sebagai "Soekarno House". Dan di beranda itulah beliau kerap kali bertukar pikiran dengan para tamu, termasuk sesama pengurus Yayasan Pendidikan Soekarno (Y.P.S.) mengenai Bung Karno dan ajarannya.

Memang Bapak Haji Mahbub Djunaidi adalah Ketua Majelis Pendidikan Yayasan Pendidikan Soekarno, sampai pada akhir hayatnya. Oleh karena itu, saya juga menilai beliau sebagai salah seorang pendekar pelaksana ajaran Bung Karno" (kutipan singkat habis disini).

Beberapa kutipan tentang sikap rekan Mahbub Djunaidi ini (penjelasan : penulis pernah sama-sama menjadi Pengurus PWI Pusat selama periode 1963 sampai September 1965) patutlah kiranya menjadi renungan bagi banyak orang dewasa ini. Mengapa ia, sebagai seorang tokoh Islam dan wartawan terkemuka (yang juga pernah mengemban tugas penting dalam berbagai organisasi yang berafiliasi dengan NU) telah menjadi Ketua Dewan Pendidikan Yayasan Pendidikan Soekarno ? Mengapa ia menjadikan bagian depan rumahnya di Bandung sebagai "Soekarno House"? Dan, juga, mengapa ia yakin bahwa ajaran-ajaran Bung Karno perlu dipelajari oleh banyak orang?
