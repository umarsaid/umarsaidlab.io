---
layout: post
title: Dominasi Asing Dalam Ekonomi Indonesia Akibat Pengkhianatan Para Maling
date: 2011-06-21
---

Perkembangan situasi di Indonesia akhir-akhir ini menunjukkan bahwa negara dan bangsa sudah mengalami krisis multidimensional dan bersegi banyak yang berbahaya sekali , dan merupakan ancaman nyata bagi hari kemudian tanair kita bersama. Krisis besar ini tercermin dengan jelas sekali dalam makin hilangnya kepercayaan rakyat terhadap kepemimpinan nasional (presiden SBY), makin bobroknya berbagai bidang pemerintahan, makin rusaknya kehidupan perlementer, makin membusuknya dunia peradilan.

Krisis besar dan multidimensional di berbagai bidang kehidupan bangsa dan negara sekarang ini bersumber – pada pokoknya --   dari parahnya krisis moral atau hebatnya krisis akhlak yang sudah melanda banyak kalangan, dan terutama kalangan atas

Bagi kita yang mengamati sejarah Republik Indonesia sejak tahun 1945 sampai sekarang nyatalah bahwa krisis moral yang meluas sekarang ini merupakan yang terbesar dan terparah. Seperti yang disaksikan oleh banyak orang, kehidupan moral publik di jaman rejim militer Suharto adalah sangat rusak. Namun, krisis moral atau krisis akhlak yang sedang kita saksikan bersama dewasa ini tidaklah kalah dengan situasi waktu itu.

Krisis moral inilah yang menyebabkan  timbulnya banyak persoalan parah negara dan bangsa, yang diakibatkan oleh tindakan atau kelakuan para pemimpin atau tokoh-tokoh, Persoalan parah ini mencakup seluruh bidang kehidupan bangsa, di antaranya dan terutama ( !)  bidang ekonomi atau  kesejahteraan rakyat. Persoalan parah di bidang ekonomi bangsa ini, ada kaitannya dengan dominasi asing, yang sudah mencengkeram banyak sekali sektor-sektor vital ekonomi Indonesia. Demikian besarnya dominasi asing ini sehingga mengurangi, atau melumpuhkan, bahkan menghilangkan kedaulatan nasional !!!

Kerusakan moral yang berkaitan dengan ekonomi negara dan bangsa adalah  luar biasa parahnya, sehingga sudah  beredar julukan (penamaan) atau cemooh bahwa negara Indonesia adalah negara kleptocratie atau negaranya  para maling. Dan maling-maling ini, yang umumnya berdasi, dan banyak yang mempunyai gelar sarjana, dan terkenal sebagai ahli di berbagai bidang, telah membikin negara kita menjadi negara yang terkorup di Asia.

Para maling atau penjahat kelas berat yang terdiri dari para intelektual dan tokoh-tokoh yang korup, reaksioner, rusak moralnya inilah yang telah menjerumuskan negara kita dalam jurang kehancuran, yang disebabkan oleh banyaknya persoalan-persoalan besar dalam ekonomi dan dominasi asing di banyak bidang atau sekstor yang vital.

Dalam tulisan kali ini masalah dominasi asing dalam kehidupan ekonomi negara dan bangsa merupakan topik yang diangkat, mengingat makin parahnya masalah ini, yang memerlukan perhatian dari seluruh bangsa. Untuk memberikan gambaran sedikit saja, atau sebagian saja, dari besarnya dominasi asing dalam bidang ekonomi Indonesia, maka disajikan di bawah ini angka-angka yang bisa membikin kita semua kaget terbelalak,  atau membikin banyak orang bertanya-tanya « mengapa negara kita sudah jadi begini ? ». Silakan simak baik-baik.

Angka-angka dominasi asing di bidang ekonomi yang « mengerikan »

Menurut Kompas (23 Mei 2011), « Dominasi pihak asing kini semakin meluas dan menyebar pada sektor-sektor strategis perekonomian. Pemerintah disarankan menata ulang strategi pembangunan ekonomi agar hasilnya lebih merata dirasakan rakyat dan berdaya saing tinggi menghadapi persaingan global.
Dominasi asing semakin kuat pada sektor-sektor strategis, seperti keuangan, energi dan sumber daya mineral, telekomunikasi, serta perkebunan. Dengan dominasi asing seperti itu, perekonomian sering kali terkesan tersandera oleh kepentingan mereka.
Per Maret 2011 pihak asing telah menguasai 50,6 persen aset perbankan nasional. Dengan demikian, sekitar Rp 1.551 triliun dari total aset perbankan Rp 3.065 triliun dikuasai asing. Secara perlahan porsi kepemilikan asing terus bertambah. Per Juni 2008 kepemilikan asing baru mencapai 47,02 persen.
Hanya 15 bank yang menguasai pangsa 85 persen. Dari 15 bank itu, sebagian sudah dimiliki asing. Dari total 121 bank umum, kepemilikan asing ada pada 47 bank dengan porsi bervariasi.
Tak hanya perbankan, asuransi juga didominasi asing. Dari 45 perusahaan asuransi jiwa yang beroperasi di Indonesia, tak sampai setengahnya yang murni milik Indonesia. Kalau dikelompokkan, dari asuransi jiwa yang ekuitasnya di atas Rp 750 miliar hampir semuanya usaha patungan. Dari sisi perolehan premi, lima besarnya adalah perusahaan asing.
Hal itu tak terlepas dari aturan pemerintah yang sangat liberal, memungkinkan pihak asing memiliki sampai 99 persen saham perbankan dan 80 persen saham perusahaan asuransi.
Pasar modal juga demikian. Total kepemilikan investor asing 60-70 persen dari semua saham perusahaan yang dicatatkan dan diperdagangkan di bursa efek.
Pada badan usaha milik negara (BUMN) pun demikian. Dari semua BUMN yang telah diprivatisasi, kepemilikan asing sudah mencapai 60 persen.
Lebih tragis lagi di sektor minyak dan gas. Porsi operator migas nasional hanya sekitar 25 persen, selebihnya 75 persen dikuasai pihak asing. Pemerintah melalui Direktorat Jenderal Migas Kementerian ESDM menetapkan target porsi operator oleh perusahaan nasional mencapai 50 persen pada 2025. » (kutiban dari Kompas selesai)

Mereka adalah pengkhianat rakyat, penjahat-penjahat besar bangsa



Ketika membaca tulisan dalam Kompas di atas itu saja banyak orang sudah mendapat gambaran bahwa ekonomi negara dan bangsa kita sudah didominasi asing. Dan dalam proporsi yang besar, atau persentase yang betul-betul menggambarkan bahwa negara kita sudah dijadikan jajahan tipe baru, atau dalam bahasa Bung Karno  « neo-kolonialisme » (nekolim).

Padahal, apa yang ditulis dalam Kompas itu hanyalah baru sebagian saja dari seluruh dominasi asing di bidang ekonomi negara dan bangsa kita. Namun, dari itu saja sudah dapat kita telaah bahwa dominasi asing dalam sektor-sektor vital ekonomi negara dan bangsa adalah akibat juga kerjasama, atau kongkalikong, atau kolusi, atau permufakatan jahat, antara fihak asing dengan berbagai kalangan atas di Indonesia  yang korup dan bermoral busuk.

Dominasi asing yang sudah begitu besar itu adalah, pada hakekatnya, pengkhianatan atau kejahatan berbagai pejabat  atau tokoh-tokoh (dalam pemerintahan, DPR, MPR, DPRD, atau lembaga-lembaga penting lainnya). Dominasi asing telah diijinkan, dipermudah masuknya, diberikan fasilitas-fasilitas, oleh para pembesar di Pemerintahan Pusat dan daerah-daerah, dengan macam-macam cara haram. Ini berbentuk segala rupa « tender » palsu atau direkayasa, segala jenis transaksi atau MOU (memory of understanding), yang diadakan lewat agen atau broker dan pengacara, yang umumnya tidak bermoral.

Karena dominasi asing ini sudah begitu luas, dan sudah menimbulkan kerugian dan penderitaan yang sangat besar pula bagi bangsa dan negara, maka dosa mereka itu sekali-kali tidak bisa dimaafkan atau dibiarkan begitu saja.  Dengan melakukan berbagai tindakan yang menyebabkan terjadinya dominasi asing di bidang ekonomi bangsa maka mereka ini telah menodai atau melanggar UUD 45 pasal 33, yang  berbunyi :


(1) Perekonomian disusun sebagai usaha bersama berdasar atas asas kekeluargaan.
(2) Cabang-cabang produksi yang penting bagi negara dan yang menguasai hajat hidup orang banyak dikuasai oleh negara.
(3) Bumi dan air dan kekayaan alam yang terkandung didalamnya dikuasai oleh negara dan dipergunakan untuk sebesar-besar kemakmuran rakyat. »

Pengkhianatan berat terhadap Konstitusi

Dalam mengamati situasi negara dan bangsa kita dewasa ini, maka jelaslah bahwa sekarang ini sedang terjadi banyak pelanggaran-pelanggaran, termasuk yang sangat serius atau parah, terhadap berbagai dasar negara dan bangsa,  antara lain UUD 45, Pancasila, Bhinneka Tunggal Ika.
Banyaknya pelanggaran-pelanggaran yang bermacam-macam itu telah banyak disajikan dalam  tulisan-tulisan yang terdahulu.

Tulisan dalam Kompas ((23 Mei 2011) menunjukkan kepada kita semua bahwa dominasi asing di bidang ekonomi kita sudah merupakan pengkhianatan besar, karena bertentangan sekali dengan pasal 33 UUD 45 kita, yang berbunyi : « Bumi dan air dan kekayaan alam yang terkandung didalamnya dikuasai oleh negara dan dipergunakan untuk sebesar-besar kemakmuran rakyat. »

Pelanggaran serius terhadap Konstitusi kita ini kelihatan jelas sekali kalau kita lihat persoalan dalam industri minyak dan gas. Porsi operator migas nasional hanya sekitar 25 persen, selebihnya 75 persen dikuasai pihak asing. Berbagai daerah di Nusantara kita sudah dibagi-bagi dalam kaveling  maskapai-maskapai asing.

Negara kita kaya dengan minyak dan gas bumi. Namun, kita sering melihat adanya antri bensin yang panjang dimana-mana, karena kurangnya persediaan dalam negeri. Sebagian besar hasil minyak  dan gas kita dikuasai oleh maskapai-maskapai asing.  Demikian juga halnya dengan tambang mas dan bahan-bahan berharga lainnya Freeport di Papua. Juga berbagai macam tambang di Sulawesi, Maluku, Nusa Tenggara, Kalimantan, Sumatera dan Jawa.

Jadi selama ini, pasal 33 UUD 45 telah dirobah oleh para pencilok (pencuri, bahasa Minang), para penipu, para perampok , para penjahat, yang bersarang dalam lembaga-lembaga  pemerintahan (Pusat dan daerah),  dan dewan-dewan perwakilan rakyat, yang  bersekongkol dengan konco-konconya di partai-partai politik   menjadi pasal yang berbunyi « bumi dan air dan kekayaan alam yang terkandung didalamnya dikuasai oleh modal asing dan dipergunakan  untuk sebesar-besar penderitaan dan kesengsaraan rakyat » !!!!! (harap perhatikan, tanda seru lima kali).

Pancasila adalah kiri dan anti-kapitalis

Besarnya dominasi asing dalam perekonomian Indonesia  dewasa ini adalah perusakan harga diri bangsa, pengkerdilan kedaulatan rakyat, dan pelangggaran pasal-pasal 33 UUD 45 kita, yang berarti juga pelanggaran atau pengkhianatan terhadap Pancasila. Dan inilah yang harus kita lawan besar-besaran dan  secara bersama-sama dewasa ini

Mengenai masalah perlawanan terhadap dominasi asing, atau penetrasi dan intervensi kekuatan asing di Indonesia Bung Karno sudah menunjukkan sikap yang bisa menjadi inspirasi besar dan contoh yang bagus sekali. Boleh dikatakan bahwa sebagian penting dari hidup Bung Karno adalah justru untuk melawan dominasi asing, baik yang berbentuk kolonialisme, neokolonialisme (nekolim), kapitalisme, liberalisme dan imperialisme .

Jadi, dalam situasi ekonomi bangsa dan negara yang sekarang dalam dominasi asing ini adalah  
baik dan berguna sekali bagi kita semua untuk menghayati sedalam-dalamnya isi jiwa Pancasila, seperti yang dijelaskan Bung Karno sendiri. Sebab isi Pancasila yang asli, dan isi pasal 33 UUD 45 adalah pegangan yang cocok  bagi bangsa Indonesia untuk menghadapi berbagai persoalan besar bangsa sekarang ini, termasuk  persoalan dominasi asing.

Bung Karno mengatakan : «  Kiri adalah juga menghendaki satu masyarakat yang adil dan makmur, di dalam arti tiada kapitalisme, tiada exploitation de l’homme par l’homme, tetapi kiri. Oleh karena itu maka saya berkata tempo hari, Pancasila adalah kiri.  Oleh karena apa ? Terutama sekali oleh karena di dalam Pancasila adalah unsur keadilan sosial.  Pancasila adalah anti kapitalisme. Pancasila adalah anti exploitation de l’homme par l’homme. Pancasila adalah anti eploitation de nation par nation. Karena itulah Pancasila kiri »  (kutiban dari buku « Revolusi belum selesai » halaman 77).

Kelihatannya, apa yang dikatakan Bung Karno seperti dikutib di atas, adalah jawaban yang tepat untuk menghadapi situasi ekonomi negara  dan bangsa yang dibelenggu oleh kapitalisme yang anti rakyat, yang penuh dengan penghisapan (pemerasan)  manusia oleh manusia, penghisapan (pemerasan)  bangsa  yang satu oleh bangsa lainnya.

Dominasi asing dalam bidang ekonomi Indonesia yang sudah menggurita sekarang ini  tidak bisa diberantas baik oleh pemerintah yang sekarang maupun oleh  pemerintahan yang mana pun juga, selama masih dipertahankan sistem berbagai politik yang ditrapkan oleh rejim militer Orde Baru sejak berlakunya UU Penanaman Modal Asing dalam tahun 1967.

Dominasi asing hanya bisa dibrantas oleh gerakan besar-besaran seluruh kekuatan demokratik, yang terdiri dari gerakan extra parlementer, yang  menyatukan kekuatan pemuda, mahasiswa, intelektual, buruh, tani, perempuan dan golongan-golongan lainnya dalam masyarakat. Kita tidak boleh lagi dan tidak bisa seterusnya menggantungkan harapan kepada  pejabat dan anggota-anggota DPR dan partai-partai politik, yang justru terdiri dari  calok-calok gelap modal asing.

Dan, guna melakukan  gerakan besar-besaran untuk melawan dominasi asing di bidang ekonomi ini tidak ada jalan lainnya yang bisa ditempuh melainkan jalan yang sudah ditunjukkan oleh ajaran-ajaran revolusioner Bung Karno.
