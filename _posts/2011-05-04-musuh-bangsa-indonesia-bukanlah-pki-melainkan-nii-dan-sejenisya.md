---
layout: post
title: Musuh Bangsa Indonesia Bukanlah PKI, Melainkan NII dan Sejenisya
date: 2011-05-04
---

Kalau kita semua sekarang sedang dibanjiri berita-berita tentang ditembaknya Osama Ben Laden oleh pasukan militer AS, dan tentang makin bergejolaknya situasi di negara-negara Arab, di samping berita-berita yang menggambarkan keruwetan di negara kita yang disebabkan korupsi, pelecehan hukum, dan hiruk-pikuk soal NII maka ketika membaca pernyataan Ketua Majelis Ulama Indonesia (MUI) tentang PKI yang seperti disajikan di bawah ini, maka kita bisa bertanya-tanya apa sajakah  tujuan peryataannya itu ???

Harap pembaca menyimaknya dengan baik-baik, dan juga membaca komentar (atau tanggapan) terhadap pernyataan yang mengandung racun bagi bangsa dan negara kita itu. Isi pernyataan ketua MUI itu persis sama saja dengan apa yang selama 32 tahun telah dicekokkan oleh Orde Baru kepada bangsa kita. Dan sikap yang ditunjukkan oleh Ketua MUI ini memperlihatkan dengan jelas bahwa MUI adalah masih satu dan sejiwa dengan Orde Barunya Suharto, tetap seperti yang dulu-dulu juga.

 Tanggapan terhadap pernyataan Ketua MUI ini bisa juga diartikan sebagai bagian dari peringatan hari ulangtahun PKI tanggal 23 Mei yang akan datang.

 Berita tersebut selengkapanya adalah sebagai berikut :



MUI: Waspadai PKI Baru!

Rakyat Merdeka 28 April 2011 . « Selain gerakan NII, pemerintah juga harus mewaspadai gerakan PKI baru di Indonesia. Sebab, saat ini geliat gerakan komunis baru ini sudah sangat terasa.

Pernyataan ini dikemukakan Ketua Majelis Ulama Indonesia Amidhan. Dia mengatakan, tanda-tanda geliat PKI baru tersebut bisa dilihat dari mulai anak-anak mantan PKI yang berani menyatakan diri sebagai keturunan PKI.

"Saat ini, mereka sudah berani bilang, saya anak PKI. Mereka juga sudah mulai melakukan pertemuan-pertemuan secara berkala dalam kelompoknya," kata Amidhan di gedung DPR, Kamis (28/4).

Bukan itu saja, lanjut Amidhan, saat ini para PKI baru ini mulai membantah kejadian 30 September bukanlah ulah PKI melainkan perselisihan antara tentara. Kelompok berusaha memanfaatkan kesalahan Presiden Soeharto untuk menutupi kesalahannya.

"Saat ini mereka sudah bekerja sama dengan dunia internasioal. Dengan alasan HAM, mereka ingin menentang keputusan pelarangannya," jelas Amidhan.

Jumlah mereka, lanjut Amidhan, sudah relatif banyak dan menyebar di semua wilayah Indonesia. "Kantung utamanya ada di Madiun dan wilayah Jawa Timur," terangnya.

Saat ini, tambah Amidhan, gerakan ini baru sekadar penyebaran paham-paham dan ideologi. Namun, kalau dibiarkan, mereka akan tumbuh besar dan membahayakan negara.

"Kalau sudah kuat, mereka akan berusaha menumbangkan negara seperti pada 1965 dulu. Karena itu, pemerintah harus bertindak. Ini berbeda dengan NII. Negara kita sudah mengatur bahwa PKI dilarang tumbuh di Indonesia," tandasnya. (Kutipan berita selesai)

Geliat gerakan PKI baru adalah baik bagi bangsa

Ketua MUI mengatakan bahwa « Selain gerakan NII pemerintah juga harus mewaspadai gerakan PKI baru di Indonesia. Sebab, saat ini geliat gerakan komunis baru ini sudah sangat terasa. »
Bahwa pemerintah  (dan seluruh bangsa !!!) harus mewaspadai  gerakan NII, itu adalah hal yang jelas, karena NII sudah dengan  terang benderang melakukan hal-hal yang merusak dan membahayakan NKRI. Sedangkan geliat gerakan komunis baru ini (kalau pun ada)  adalah hal yang baik bagi NKRI dan juga sangat perlu  bagi seluruh kehidupan bangsa.

Karena itu, Bung Karno dalam seluruh ajaran-ajaran revolusionernya sejak masih umur muda (dua puluh tahunan) sudah menganjurkan persatuan NASAKOM. NASAKOM adalah jiwa Bung Karno, pengejowantahan gagasan besarnya untuk mempersatukan bangsa. Karenanya, PKI tidaklah harus diwaspadai, melainkan harus disambut dengan hangat dalam persatuan dan kesatuan  bangsa.

Ketika kita membaca pernyataan Ketua MUI tentang PKI seperti yang disajikan di atas, maka sebenarnya ia tidak saja mengutarakan hal-hal yang hanya menjelek-jelekkan PKI, melainkan juga serangan hebat (walaupun tidak langsung) terhadap  Bung Karno. Ini jugalah yang sudah dilakukan oleh Orde Baru (Suharto bersama Golkar) terus-menerus selama puluhan tahun

Karena itu, kita patut memandang pernyataan Ketua MUI tentang PKI ini sebagai bagian dari kelanjutan kampanje anti Bung Karno (dan anti PKI) ketika di seluruh tanah air sedang dihebohkan oleh segala macam siaran mengenai NII atau Al Zaitun, atau Wiranto, Hendropriyono dan Panji Gumilang, atau tentang FPI dan sejenisnya.

Sikap PKI terhadap Pancasila dan Bhinneka Tunggal Ika

Ketua MUI, seperti halnya sebagian pimpinan Angkatan Darat (dan pimpinan berbagai gerakan Islam radikal) berusaha melindungi atau menyembunyikan segala macam kejahatan, pelangggaran, pengkhianatan  kelompok-kelompok Islam radikal (termasuk NII) dengan mengumbar segala hal yang dianggap negatif tentang PKI.

Padahal, PKI atau sisa-sisanya (dan simpatisan atau para pendukungnya) sejak dulu sampai sekarang ini sama sekali tidak merupakan bahaya bagi negara dan bangsa, tidak merusak persatuan, dan tetap merupakan pembela Pancasila dan Bhinneka Tunggal Ika, dan pendukung ajaran-ajaran revolusioner Bung Karno

Sikap setia terhadap persatuan bangsa, terhadap Pancasila dan Bhinneka Tunggal Ika  ini  juga tetap dipegang oleh sisa-sisa PKI, atau oleh simpatisan dan para pendukungnya, walaupun sudah selama lebih dari 45 tahun ditindas secara kejam, dipersekusi, dan dilarang melakukan berbagai macam kegiatan.

Tujuan yang dioerjuangkan NII berlainan sama sekali dengan yang diperjuangkan PKI.  Tujuan NII adalah merombak atau menghancurkan NKRI dan mendirikan Negara Islam Indonesia sebagai gantinya. Sedangkan tujuan perjuangan PKI adalah mempertahankan NKRI tetapi dengan isi sosialisme atau masyarakat adil dan makmur, seperti yang dicita-citakan oleh Bung Karno sejak lama sekali.

Tujuan PKI bertentangan sama sekali dengan tujuan NII

NII berusaha menjadikan Syariat Islam sebagai hukum yang berlaku di Indonesia, sedangkan PKI (seperti halnya Bung Karno) berusaha supaya kehidupan bangsa dan negara dipedomani oleh Pancasila dan Bhinneka Tunggal Ika di samping berlakukanya segala undang-undang yang betul-betul demokratis bagi semua warganegara Indonesia, dan juga bagi semua manusia.

NII sejiwa atau searah (atau seiring) dengan jiwa berbagai kalangan Islam yang sudah menentang Pancasila, Bhinneka Tunggal Ika, demokrasi dan Deklarasi Universal HAM, sedangkan PKI membelanya, dengan menyatukan diri dengan berbagai politik dan ajaran-ajaran revolusioner Bung Karno dalam menentang imperialisme dan berjuang untuk sosialisme a la Indonesia.

Jadi, adalah salah sama sekali kalau NII diperlakukan sama dengan PKI. NII adalah gerakan makar yang mengkhianati terhadap NKRI, sedangkan PKI adalah kekuatan pokok pendukung Bung Karno dalam mempersatukan bangsa dalam rangka NKRI.

NII yang sekarang adalah kelanjutan atau perpanjangan darii NII-DI-TII nya Kartosuwirjo, sedangkan PKI adalah sahabat seperjuangan Bung Karno. Itulah sebabnya mengapa Bung Karno digulingkan oleh Suharto. Bung Karno tetap tidak mau membubarkan ¨PKI walaupun didesak-desak, sampai wafatnya, akibat siksaan ketika ia sudah sakit parah dalam tahanan.

Gerakan-gerakan di negara Arab tidak untuk negara Islam

Sikap  NII untuk mendirikan Negara Islam Indonesia adalah berbeda atau bertentangan dengan sikap NU dan Muhammadiyah yang dua-duanya  -- seperti halnya sebagian terbesar bangsa dan rakyat Indonesia – menganggap bahwa NKRI dengan Pancasilanya adalah sesuatu yang sudah final, dan tidak bisa diperdebatkan lagi

Kalau kita bandingkan dengan apa yang terjadi di sebagian besar negara-negara Arab akhir-akhir ini, tujuan kelompok-kelompok Islam radikal di Indonesia adalah suatu hal yang bertolak belakang. Gerakan-gerakan besar dan kecil, untuk memperjuangkan perubahan-perubahan dalam sistem politik, ekonomi, sosial dan kebudayaan, yang dipelopori oleh kaum muda, para intelektual, dan kaum buruh di banyak negeri Arab ini justru tidak untuk mendirikan negara Islam.

Malahan sebaliknya !!! Gerakan untuk perubahan yang terjadi di Mauritania, Maroko, Aljazair, Libia,  Tunisia, Mesir, Sudan, Somalia, Bahrein, Jordania, Siria, Saudi Arabia, Yamen, Oman, adalah gelombang besar untuk perubahan dalam negara-negara yang sudah puluhan tahun (ada yang lebih dari 40 tahun !) diperintah oleh penguasa-penguasa yang korup, reaksioner, feodal, despotik, yang sering menggunakan (atau menyalahgunakan !!!) agama atau ayat-ayat suci untuk menindas rakyat masing-masing, dan hidup secara mewah-mewah di tengah-tengah kemiskinan dan keterbelakangan.

Oleh karena itu, adalah akan baik sekali bagi MUI atau berbagai kalangan radikal Islam di Indonesia untuk mengikuti dengan cermat perkembangan pergolakan-pergolakan di sebagian besar negara-negara Arab, supaya tidak ketinggalan jaman. Di banyak negara Arab ini Islam akan mengalami perubahan atau pembaruan, sesuai dengan tuntutan dan kebutuhan jaman yang berobah terus-menerus. Pergolakan-pergolakan besar di negara-negara Arab sekarang ini bisa memberi sumbangan berupa pencerahan yang berharga sekali bagi berbagai gerakan radikal Islam di Indonesia.

Islam yang diperjuangkan oleh gerakan-gerakan di banyak negara-negara Arab sekarang ini (termasuk di Saudi Arabia dan Mesir sendiri) adalah bukan « kejayaan dan kebesaran » Islam di abad-abad pertengahan (atau bahkan sebelumnya) melainkan Islam yang lebih demokratis, toleran, menghargai hak-hak manusia, dan yang melawan obscurantisme.

Bung Karno : « PKI itu sanak saudara, kalau meninggal saya ikut kehilangan »

Dalam rangka besar Pancasila dan Bhinneka Tunggal Ika inilah kita bisa melihat betapa sesatnya pernyataan Ketua MUI mengenai PKI. Sebab, seperti sikap yang dipegang teguh oleh Bung Karno (sampai wafatnya !!!), PKI adalah bukan musuh bangsa Indonesia. « PKI kuwi yo kadang yo sanak, yen mati aku melu kelangan » kata Bung Karno. (terjemahan dari  bahasa Jawa : « PKI itu sanak saudara, kalau meninggal saya ikut merasa kehilangan ».

Singkat kata, PKI adalah asset bangsa Indonesia. Dan sebaliknya, Orde Baru-nya Suharto dan pendukung-pendukung setianya adalah sampah bangsa. Sejarah sudah membuktikannya. Dan akan dibuktikan lebih jelas lagi di kemudian hari !!!
