---
layout: post
title: Tentang Mahkamah Konstitusi dan PKI
date: 2004-02-26
---

Para bekas anggota (atau yang masih menganggap diri mereka sebagai anggota PKI), atau para simpatisan PKI beserta ormas-ormas yang pernah bernaung di bawah PKI - atau para simpatisan Bung Karno - sekarang bisa bergembira-ria dengan adanya putusan Mahkamah Konstitusi tentang larangan bagi mereka untuk menjadi anggota DPR, DPD atau DPRD. Seperti banyak diberitakan oleh media massa di Indonesia, Mahkamah Konstitusi pada tanggal 24 Februari telah membuat keputusan penting, yaitu mencabut larangan bagi anggota PKI untuk menjadi calon legislatif.

Putusan Mahkamah Konstitusi ini merupakan sumbangan yang besar bagi bangsa Indonesia untuk usaha keluar dari kedunguan dan kepicikan yang selama lebih dari 32 tahun Orde Baru sudah mengungkung Republik Indonesia. Majlis hakim Mahkamah Konstitusi, yang terdiri dari 9 orang, telah menyatakan bahwa Pasal 60 Undang Undang Pemilu bertentangan dengan UUD 1945 dan telah melanggar hak azasi manusia yang terkandung dalam UUD 1945.

## Uud Melarang Diskriminasi

Pertimbangan hukum yang dipakai oleh Mahkamah Konstitusi adalah bahwa UUD 1945 melarang adanya diskriminasi berdasarkan agama, suku, ras, etnik, kelompok, golongan status sosial, status ekonomi, jenis kelamin, bahasa dan keyakinan politik. Di samping itu, article 25 dari International Convenant on Civil and Political Right (ICCPR) tahun 1966 yang mengatur hak-hak sipil politik juga menyebutkan bahwa warga negara di setiap negara mempunyai hak untuk memilih dan dipilih. Article ini telah diratifikasi atau disetujui oleh 92 negara dari 160 negara anggota PBB, termasuk Indonesia.

Jadi, putusan Mahkamah Konstitusi tersebut merupakan koreksi terhadap kesalahan monumental dan parah rezim militer Orde Baru dalam jangka waktu yang begitu panjang, terutama yang dilakukan terhadap para anggota, simpatisan PKI dan sanak-saudara mereka. Mereka ini jumlahnya puluhan juta di seluruh negeri, dan umumnya terpaksa menanggung berbagai penderitaan dan siksaan, hanya karena mereka adalah anggota atau simpatisan PKI atau golongan kiri lainnya, termasuk pendukung politik Bung Karno.

Rezim militer Orde Baru (yang tulang-punggungnya adalah TNI-AD dan Golkar) telah mengucilkan dan mempersekusi mereka yang tidak bersalah apa-apa ini, dan mentrapkan berbagai diskriminasi politik, ekonomi, sosial dan kebudayaan, dengan cara-cara yang kejam dan tidak beradab (antara lain : surat bebas G30S, « bersih lingkungan », litsus, wajib lapor, TAP MPRS no 25/1966, tanda E.T pada kartu penduduk, berbagai peraturan Kopkamtib, peraturan Kementerian Dalamnegeri No 32/1981)

## Seperti Warga Negara Lainnya

Putusan Mahkamah Konstitusi dapat diartikan bahwa semua anggota dan simpatisan PKI, termasuk para eks-tapol dan para korban Orde Baru lainnya, sejak sekarang bisa secara terang-terangan menuding para pentolan dan pendukung rezim militer Orde Baru sebagai pelanggar konstitusi dan perusak HAM. Artinya, seperti yang sudah dibuktikan oleh sejarah selama berpuluh-puluh tahun, yang ternyata jelas salah bukanlah anggota dan simpatisan PKI, yang jumlahnya jutaan itu, melainkan justru yang memusuhi PKI (dan memusuhi Bung Karno).

Karenanya, berdasarkan putusan Mahkamah Konstitusi ini para anggota dan simpatisan PKI (atau golongan kiri lainnya) dapat dengan kepala tegak mengatakan kepada siapapun bahwa mereka adalah warganegara RI yang sepenuhnya sama dengan warganegara RI lainnya. Terhadap mereka tidak boleh diadakan diskrimanasi yang bagaimana pun. Mereka juga bisa dengan lantang menggugat ketidak-adilan yang selama puluhan tahun telah mereka alami, hanya karena akibat kepicikan nalar dan kebusukan fikiran atau kebutaan hati-nurani para pendukung Orde Baru, yang umumnya anti-komunis atau anti Bung Karno.

Jelaslah bahwa sekarang ini, mereka tidak patut lagi untuk merasa rendah diri, merasa takut-takut atau malu-malu, mengakui sebagai orang kiri. Bung Karno sendiri, sesudah peristiwa G30S berkali-kali mengatakan bahwa revolusi Indonesia adalah revolusi kiri, dan bahwa Pancasila adalah kiri dan revolusioner, dan bahwa Nasakom adalah syarat yang dibutuhkan oleh negara dan bangsa Indonesia untuk mencapai persatuan dan kesejahteraan (baca : buku kumpulan pidato Bung Karno « Revolusi belum selesai »).

## Putusan Yang Bikin Heboh

Putusan Mahkamah Konstitusi tentang pencabutan larangan anggota PKI menjadi caleg telah menimbulkan berbagai reaksi di dalam negeri. Ini adalah wajar dan juga baik sekali. Dengan begini makin jelas kelihatan siapa-siapa saja, atau golongan dan kalangan yang mana saja, yang menjunjung tinggi konstitusi dan hukum atau HAM, dan yang mana saja yang tidak. Perdebatan yang ramai mengenai soal ini akan merupakan pendidikan politik bagi umum, yang sekaligus juga merupakan pemblejedan terhadap berbagai pandangan busuk dari para pendukung Orde Baru.

Sekarang makin jelas bahwa sikap sejumlah pimpinan TNI-AD masih « komunisto-phobi » (istilah Bung Karno), seperti yang didemonstrasikan oleh Panglima TNI Jenderal Endriartono Sutarto, yang « menyayangkan » keputusan tersebut, karena « PKI sudah beberapa kali memberontak dan mengkhianati bangsa », katanya. (Liputan 6, 25/2/04). Ucapan Panglima TNI ini masih seperti ucapan-ucapan berbagai tokoh TNI-AD sebelumnya ketika mereka mau menggulingkan Bung Karno. Jadi, sekarang masih kelihatan jelas juga sikap anti-komunis (dan anti-Bung Karno) dari fihak sejumlah pimpinan TNI-AD.

Tetapi, sebaliknya, reaksi dari sebagian besar tokoh-tokoh dan organisasi-organsai non-pemerintah (termasuk di kalangan NU dan Muhammadiyah) adalah positif, karena melihat bahwa ketidakadilan yang sudah berlangsung begitu lama dan merupakan aib besar bangsa, harus dikoreksi atau dihentikan sama sekali.

«Heboh » tentang putusan Mahkamah Konstitusi ini sangat berguna, untuk lebih memblejedi lebih telanjang lagi kebusukan rezim militer Orde Baru. Ini juga bisa merupakan sumbangan untuk mematangkan opini umum, bahwa akhirnya TAP MPRS 25/1966 (larangan terhadap PKI dan ajaran Marxisme) juga perlu dicabut, karena juga bertentangan dengan UUD 1945. Pada waktunya, siapa saja yang masih menganggap baik tetap diberlakukannya TAP MPRS no 25/1966, akan dipandang sebagai orang yang tidak waras nalar, atau cupet fikiran, atau memang busuk hati-nuraninya.

Putusan Mahkamah Konstitusi juga bisa dilihat dari segi lain. Melalui putusan ini kita bisa lebih mengerti lagi, secara gamblang pula, bahwa rezim militer Orde Baru (yang ditulang-punggungi TNI-D dan Golkar) bukan hanya mengkhianati Bung Karno saja, melainkan juga telah melanggar secara serius atau menginjak-injak UUD 1945. Sikap politik rezim militer Orde Baru terhadap PKI dan Marxisme adalah sama sekali bertentangan dengan UUD 1945. Dan, sikap politik TNI-AD terhadap Bung Karno adalah pengkhianatan serius terhadap pemimpin besar bangsa, yang telah mengantar Republik Indonesia ke gerbang kemerdekaan.

Nanti, pada waktunya, dan itu entah kapan, akan datang saatnya bagi TNI-AD (dan para pendukung setia Orde Baru) untuk menyadari bahwa mereka pernah berbuat pelanggaran konstitusi, dengan bersikap tidak benar atau tidak adil terhadap jutaan (bahkan puluhan juta) anggota dan simpatisan PKI beserta sanak-saudara mereka. Kalau sudah datang perkembangan yang demikian, maka rekonsiliasi nasional bisa diharapkan bisa terjadi. Tetapi, selama TNI-AD (dan pendukung setia Orde Baru lainnya) masih belum merobah sikapnya terhadap PKI dan Bung Karno, maka persatuan nasional yang betul-betul kompak tidak akan pernah tercapai.

(Dalam kaitan ini, patut dicatat bahwa dari 9 hakim Mahkamah Konstitusi yang mengambil putusan pencabutan larangan bagi anggota PKI jadi caleg hanya seorang yang tidak menyetujui putusan itu, yaitu Letnan Jenderal (Purn) TNI Ahmad Rustandi. Suatu indikasi yang cukup jelas untuk mengetahui bagaimana sikap TNI-AD !).

## Koreksi Kesalahan Besar

Dalam sejarah Republik Indonesia, putusan Mahkamah Konstitusi tentang caleg anggota PKI ini bisa digolongkan sebagai peristiwa penting, yang akan bisa mempunyai dampak positif untuk pendidikan bagi seluruh bangsa di bidang HAM dan hukum. Tidak bisa disangkal oleh siapa pun juga, bahwa rezim militer Orde Baru sudah selama 32 tahun menjalankan indoktrinasi anti-komunisme dan kampanye de-Sukarnoisasi secara intensif dengan menggunakan segala cara. Indoktrinasi yang besar-besaran dan dalam jangka waktu yang begitu panjang itu telah meracuni fikiran dan membutakan hati nurani banyak orang, terutama mengenai HAM.

Putusan Mahkamah Konstitusi ini mengingatkan kepada seluruh bangsa bahwa anggota dan simpatisan PKI, yang umumnya juga terdiri dari pendukung politik Bung Karno, mempunyai hak yang sama seperti warganegara RI lainnya, dan sama sekali tidak boleh di-diskriminasi. Karenanya, bisa pula diartikan bahwa Mahkamah Konstitusi ini telah melakukan koreksi atas kesalahan rezim militer Orde Baru, yang dalam waktu begitu lama (32 tahun !!!) telah melanggar UUD 1945, yang mengakibatkan penderitaan dan siksaan bagi jutaan orang tidak bersalah.

Karena putusan Mahkamah Konstitusi itu sifatnya final, dan mengikat secara menyeluruh menurut hukum, maka bisa diperkirakan bahwa akhirnya akan membawa dampak yang positif bagi usaha untuk memperjuangkan rehabilitasi hak para korban Orde Baru, terutama para keluarga yang sanak-saudaranya dibunuh secara besar-besaran dalam tahun 1965, para eks-tapol beserta keluarga mereka yang disiksa selama puluhan tahun, dan puluhan juta orang yang selama ini diperlakukan sebagai warganegara kelas kambing.

Berdasarkan putusan Mahkamah Konsitusi ini mereka ini sekarang bisa menuntut hak mereka sebagai waraganegara RI sepenuhnya, umpamanya untuk menjadi pegawai negeri atau militer, atau untuk dengan leluasa hidup dengan hak-hak penuh sebagai waganegara RI yang normal. Sudah terlalu lama (38 tahun !!!) para anggota dan simpatisan PKI ini terpaksa harus mengalami berbagai penderitaan akibat pelanggaran konstitusi oleh Orde Baru.

Putusan Mahkamah Konstitusi adalah bukan hanya kemenangan para anggota dan simpatisan PKI dan Bung Karno saja, melainkan kemenangan nalar yang waras, atau kemenangan fikiran yang jernih dan sehat.
