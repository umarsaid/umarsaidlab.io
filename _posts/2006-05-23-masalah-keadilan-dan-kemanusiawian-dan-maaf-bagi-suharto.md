---
layout: post
title: Masalah keadilan dan kemanusiawian dan ma’af bagi Suharto
date: 2006-05-23
---

Akhir-akhir ini ada banyak orang-orang yang mengusulkan direhabilitasinya Suharto, diampuninya dari segala dosa-dosanya, dan dihentikannya proses pemeriksaan dan pengadilan terhadapnya. Di antara berbagai alasan atau pertimbangan mereka adalah berdasarkan keadilan dan kemanusiawian karena Suharto sudah lanjut usia (84 tahun), dan sakit parah sehingga tidak bisa diajukan di depan pengadilan. Ada juga orang-orang yang mengusulkan pengampunan kesalahan Suharto mengingat jasa-jasanya terhadap negara dan bangsa sebagai presiden dan bapak pembangunan. Bahkan, ada orang-orang yang menganjurkan dihargainya atau dihormatinya sebagai pahlawan, karena ia “telah menyelamatkan bangsa dari bahaya PKI dan telah menghancurkan kediktatoran Bung Karno”. Dan, keluarga Suharto, lewat pernyataan anaknya Siti Hediati Haryadi (Titik) juga menyampaikan permintaan ma’af kepada rakyat Indonesia atas kesalahan yang telah dibuat ayahnya.

Menghadapi suara-suara atau usul-usul yang seperti itu, seyogianya kita semua merenungkan sedalam-dalamnya dan seluas mungkin, persoalan Suharto ini dari banyak segi. Segala usaha perlu sama-sama kita tempuh, untuk mencari cara atau jalan yang sebaik-baiknya, atau setepat-tepatnya, untuk menyelesaikan persoalan Suharto ini berdasarkan keadilan dan kemanusiawian. Keadilan dan kemanusiawian baginya, tetapi juga keadilan dan kemanusiawian bagi orang banyak, bagi bangsa atau rakyat. Sebab, persoalan Suharto bukanlah hanya persoalan pribadinya saja, dan bukan pula terbatas urusan keluarganya saja. Persoalan Suharto mempunyai hubungan yang erat sekali dengan berbagai masalah besar bangsa dan negara kita.

Apa yang tercantum berikut ini adalah hanya sebagian kecil saja dari bahan-bahan renungan yang bisa sama-sama kita telaah. Bahan-bahan lainnya untuk menjadi pemikiran bersama terdapat dalam kumpulan berita dan tulisan yang terdapat dalam website http://perso.club-internet.fr/kontak) baik yang dalam rubrik “Kasus pemeriksaan dan pengadilan Suharto” atau rubrik “Harta haram Suharto” atau dalam tulisan yang sinis “Jasa-jasa besar Suharto”.


Perbuatan Suharto selama 32 tahun

Untuk bisa secara jujur, atau secara objektif, atau secara adil - dan dengan fikiran yang jernih atau nalar yang sehat pula ! – menilai kepemimpinan Suharto selama 32 tahun mengendalikan rejim militer Orde Baru, maka kita perlu menelaah kembali apa yang telah dilakukannya di bidang politik, ekonomi, sosial, kebudayaan, dan juga di bidang moral dan hak-hak manusia, bagi negara dan bangsa. Dengan menelaah dengan baik-baik dan agak menyeluruh bidang-bidang itu, kita akan secara gamblang, atau jujur dan adil, memberi penilaian yang tepat kepada persoalan Suharto. Namun, karena luasnya persoalan di tiap bidang, maka tulisan yang kali ini hanya membatasi diri dalam rangka masalah keadilan dan kemanusiawian, yang dihubungkan dengan masalah Suharto dan rakyat atau bangsa.

Sebab, akhir-akhir ini, dalam pembicaraan yang ramai sekitar masalah Suharto sering disebut-sebut soal keadilan dan kemanusiawian. Ada orang-orang yang mengangkat soal keadilan dan kemanusiawian (atau perikemanusiaan) ini dengan maksud yang baik atau tujuan yang luhur, namun kita sama-sama lihat juga adanya mereka yang bicara soal keadilan dan perikemanusiaan ini hanya untuk membela kepentingan Suharto. Dan mereka membela kepentingan Suharto ini adalah sebagian besar dengan tujuan untuk membela kepentingan mereka sendiri. Bukan untuk kepentingan rakyat banyak.


Ketidak-adilan di bidang politik.

Kiranya, kita semua perlu ingat bahwa selama 32 tahun Suharto telah banyak melanggar atau merusak keadilan di bidang politik. Entah berapa banyak orang dari berbagai kalangan yang telah menjadi korban dari ketidak-adilan Suharto di bidang politik ini. Rejim militer Orde Baru yang telah mencekik kehidupan demokratis selama puluhan tahun telah menimbulkan banyak ketidak-adilan di bidang politik.

Pemilu yang diadakan berkali-kali, yang pernah disebut dengan gagah sebagai “pesta demokrasi”, merupakan tontonan dari puncaknya ketidak-adilan politik ini. Dalam pemilu yang berulangkali itu, kekuasaan militer telah memainkan peran yang besar sekali (dan kotor sekali !!!), sehingga Golkar selalu menang dengan angka sekitar 70% suara. Sebagai akibatnya, DPR yang dihasilkan oleh pemilu yang berkali-kali itu, tidaklah lebih dari perwakilan dari ketidak-adilan ini. Selama Orde Baru berkuasa sepanjang 32 tahun, pemerintahan di bawah Suharto tidak hanya mencerminkan ketidak-adilan dalam politik, melainkan juga kekuasaan yang otoriter atau despotik.

Bung Karno beserta para pendukungnya, yang berjumlah puluhan juta, diperlakukan tidak adil secara besar-besaran, dan dalam bentuknya yang macam-macam pula. Dalam rangka politik “de-Sukarnoisasi” maka yang berbau Sukarno,disingkirkan atau dikucilkan. Keadilan dalam politik tidak berlaku bagi para pendukung setia Bung Karno.

Seperti yang masih kita saksikan akibatnya sampai sekarang di mana-mana di Indonesia, ketidak-adilan dan ketidak-manusiawian yang paling menyolok adalah tindakan Suharto dengan rejim militernya Orde Baru terhadap orang-orang yang anggota PKI atau yang dituduh sebagai komunis atau golongan kiri. Para penguasa rejim militer mengetahui Mereka yang jadi korban ketidak-adilan dan ketidak-manusiawian selama puluhan tahun ini tidak bersalah apa-apa sama sekali. Dan jumlahnya banyak sekali di seluruh negeri.


Ketidak-adilan di bidang ekonomi

Ketidak-adilan di bidang ekonomi adalah ciri pemerintahan di bawah Suharto yang sangat menyolok secara gamblang. Boleh dikatakan bahwa rejim militer Orde Baru adalah pada hakekatnya manifestasi yang gamblang atau perwujudan yang kongkrit tentang ketidak-adilan di bidang ekonomi ini. Sebagian kecil sekali dari masyarakat, yang terdiri dari pejabat-pejabat negara, jenderal-jenderal, konglomerat-konglomerat hitam, dan “tokoh-tokoh” berbagai kalangan, menikmati kekayaan-kekayaan (yang haram !!!) secara berlimpah-limpah. Sedangkan sebagian terbesar dari rakyat hidup dalam kemiskinan yang akut sekali.

Dalam rangka ketidak-adilan di bidang ekonomi ini, Suharto beserta keluarganya merupakan kasus yang istimewa. Melalui penyalahgunaan kekuasaan yang luar bisa besarnya sebagai penguasa tertinggi (untuk tidak mengatakan sebagai diktator) rejim militer, Suharto telah menjalankan KKN yang paling besar dan paling luas, yang memungkinkannya menumpuk kekayaan melalui cara-cara haram dan bathil. Suharto (dan keluarganya) adalah bukan saja simbul yang “gemilang” ketidak-adilan di bidang ekonomi, melainkan juga simbul yang gamblang dari kejahatan ekonomi.

Suharto bukan saja merupakan koruptor yang paling besar (dan paling lihay) di Indonesia, melainkan juga koruptor nomor satu di dunia (menurut Transparency International, lihat rubrik “Harta haram Suharto” di website).

“Jalur-jalur pemerataan kesejahteraan” yang pernah digembar-gemborkan bertahun-tahun oleh rejim militer Suharto ternyata sekarang bahwa bukan saja slogan yang palsu, melainkan juga topeng untuk menutupi kejahatan-kejahatan dan ketidak-adilan di bidang ekonomi yang dilakukan olehnya beserta pendukung-pendukung setianya, baik dari kalangan militer, maupun dari kalangan sipil dan pengusaha besar.


Ketidak-adilan di bidang sosial

Dalam sejarah Republik Indonesia, kesenjangan sosial dalam masyarakat Indonesia menjadi tidak pernah selebar atau separah ketika di bawah pemerintahan Suharto. Jurang antara si kaya dan si miskin makin kelihatan menganga lebar sekali. “Masyarakat adil dan makmur” yang pernah jadi slogan dan idam-idaman banyak orang ketika pemerintahan ada di bawah Bung Karno telah semakin menjauh ketika zaman Suharto. Jiwa “gotong-royong” pun dicampakkan oleh rejim militer Suharto. Keadilan sosial malah menjadi suatu hal yang menjijikkan bagi para penguasa Orde Baru beserta pendukung-pendukung setianya.

Kebudayaan dan kebiasaan feodal yang sudah diperangi secara besar-besaran di era Sukarno telah dihidupkan kembali dan dipupuk secara keterlaluan oleh Suharto beserta keluarga dan pendukung-pendukungnya.

Ketidak-adilan di bidang sosial juga tercermin dalam berbagai bidang lainnya, yang merupakan jurang pemisah yang makin menajam antara kelompok atau golongan. Jumlah pengangguran yang sekarang 40 juta orang sekarang ini adalah akibat buruk dari sistem politik, ekonomi dan sosial pemerintahan Suharto, yang terpaksa diteruskan oleh berbagai pemerintahan sesudah Suharto dapat dijatuhkan oleh gerakan mahsiswa dalam tahun 1998.
Singkatnya, Suharto selama 32 tahun tidak peduli terhadap keadilan sosial.


Ketidak-adilan dalam bidang HAM dan moral

Perlu kiranya terus-menerus diingat oleh bangsa kita bahwa rejim militer Suharto dkk dibangun atas tumpukan mayat berjuta-juta orang tidak bersalah yang dibantai dengan sewenang-wenang oleh aparat militer di bawah perintah Suharto. Kebiadaban ini perlu sekali dicatat dengan jelas - dan dengan hruf tebal - dalam sejarah bangsa kita, sehingga anak cucu kita bisa menarik pelajaran dari kesalahan dan dosa besar Suharto dan pendukung-pendukungnya ini.

Pembunuhan begitu banyak warganegara sesama bangsa ini, yang ditambah lagi dengan pemenjaraan atau penahanan ratusan ribu orang tidak bersalah lainnya dalam jangka waktu yang lama sekali (belasan tahun) tanpa proses pengadilan adalah bukti yang tidak dapat disangkal lagi oleh siapa pun bahwa pelanggaran hak-hak manusia telah dilakukan oleh Suharto dan pendukung-pendukungnya. Saksinya atau buktinya masih dapat dijumpai di banyak tempat di seluruh negeri, sampai dewasa ini. Di antara mereka terdapat para eks-tapol yang meringkuk di Pulau Buru, dan banyak penjara lainnya.

Ketidak-adilan di bidang HAM ini juga dihadapi oleh puluhan juta orang keluarga (anak, istri, suami, bapak ibu atau saudara-saudara dekat) para korban pembantaian tahun 65 dan keluarga para eks-tapol, yang sampai sekarang --- harap catat : sesudah 40 tahun !!! – masih menghadapi berbagai penderitaan akibat adanya peraturan-peraturan yang “gila”. Jelaslah, kiranya, bahwa Suharto (dan pendukung-pendukungnya) tahu tentang penderitaan begitu banyak orang akibat tindakannya itu, tetapi ia toh masih meneruskan ketidak-adilan yang sudah berlangsung begitu lama itu. Ketidak-adilan ini juga dihadapi oleh banyak sekali orang-orang Indonesia yang terpaksa bermukim di negara-negara di berbagai benua, karena dicabut paspor mereka atau mendapat perlakuan-perlakuan buruk lainnya.

Kejahatan besar terhadap HAM adalah ciri utama rejim militer Orde Baru. Peristiwa terbunuhnya sejumlah mahasiswa di Semanggi, dan hilangnya anak-anak muda PRD, dan dibunuhnya pejoang HAM yang terkenal Munir, adalah bagian dari rentetan panjang kejahatan di bidang HAM. Namun, tidak bisanya diambil tindakan hukum terhadap para pelaku kejahatan-kejahatan ini adalah bukti bahwa memang ada banyak ketidak-adilan dan ketidak-manusiawian di bidang hukum selama ini. Dan, karenanya, ketidak-adilan dalam bidang HAM terhadap puluhan juta orang tidak bersalah ini adalah juga aib besar atau dosa yang berat Suharto, yang kemudian diwariskan kepada penerus-penerusnya.

* * *


Apa yang disajikan di atas adalah sekadar garis besar atau secara pokok-pokok sejumlah kecil masalah kejahatan terhadap keadilan dan kemanusiawian yang telah dilakukan oleh Suharto lewat kekuasaasannya selama memimpin rejim militer Orde Baru. Semuanya ini sudah diketahui oleh banyak orang – termasuk di luar negeri – dan sudah sejak lama pula.

Dari sedikit bahan renungan ini saja sudah cukup kiranya bagi setiap nalar yang waras, atau setiap hati yang bersih, untuk menilai Suharto dalam masalah keadilan dan kemanusiawian.
Dari apa yang telah dilakukannya selama 32 tahun Orde Baru tidaklah bisa dikatakan bahwa ia berjasa besar terhadap negara dan bangsa. Bahkan sebaliknya, ia telah menimbulkan kerusakan-kerusakan yang besar dan pembusukan yang parah di berbagai bidang kehidupan bangsa. Karena itu, Suharto tidak patut sama sekali dianggap sebagai pahlawan bangsa.

Mengingat itu semuanya, pantaslah kiranya kalau ada orang yang mengatakan bahwa kehadiran Suharto dalam sejarah bangsa Indonesia merupakan halaman hitam yang penuh dengan noda, dosa dan aib. Dari banyak dosa dan aib ini di antaranya berupa pengkhianatan terhadap Bung Karno dan rakyat Indonesia, dan pelanggaran HAM secara besar-besaran terhadap puluhan juta orang tidak bersalah, serta juga penumpukan harta kekayaan lewat penyalahgunaan kekuasaan dan korupsi.

Apakah semua ketidak-adilan dan ketidak-manusiawian Suharto yang begitu besar dan parah itu dapat dima’afkan begitu saja, tanpa ada proses hukum sama sekali? Sudah jelaslah kiranya bahwa bagi mereka yang berhati-nurani yang bersih - dan peduli dengan rasa keadilan serta rasa kemanusiawian - jawabannya adalah : TIDAK !
