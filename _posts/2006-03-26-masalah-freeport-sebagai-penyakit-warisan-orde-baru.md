---
layout: post
title: Masalah Freeport sebagai penyakit warisan Orde Baru
date: 2006-03-26
---


Ramainya reaksi dari berbagai kalangan terhadap berita-berita tentang Freeport di Papua (dan tambang minyak Cepu yang pengurusannya diserahan kepada Exxon) menunjukkan kepada kita semua bahwa memang terdapat banyak sekali ketidakberesan atau persoalan yang harus segera dibenahi oleh negara dan bangsa kita mengenai masalah tersebut. Bahwa banyak orang dari berbagai kalangan ikut beramai-ramai mempersoalkan masalah ini adalah suatu hal yang baik sekali bagi kehidupan bangsa kita dewasa ini, dan bagi generasi kita yang akan datang.

Sebab, masalah Freeport di Papua berkaitan erat dengan berbagai aspek dari pandangan politik dan sikap kita semua (terutama sikap para « tokoh ») terhadap masalah-masalah kepentingan negara dan rakyat, dan terhadap hari kemudian bangsa kita. Masalah Freeport di Papua mengandung persoalan, antara lain, bagaimana seharusnya sikap bangsa dan negara kita menghadapi kebutuhan kerjasama dengan modal asing tanpa merugikan kepentingan rakyat.

Dalam merenungkan berbagai persoalan parah yang timbul di sekitar masalah Freeport dewasa ini, agaknya perlu bagi kita semua untuk selalu ingat bahwa kehadiran Freeport di Papua adalah hasil « kerjasama » antara kekuasaan Suharto dkk di Angkatan Darat (dengan dukungan penuh dari berbagai kalangan reaksioner yang anti presiden Sukarno dan anti-PKI) dan kekuatan imperialis AS yang diwakili oleh maskapai raksasa Freeport McMoRan Copper & Gold, Inc.


KONTRAK KARYA TAHUN 1967 ADALAH PENGKHIANATAN

Freeport masuk ke Indonesia dengan fasilitas Suharto bersama-sama « orang-orang »-nya. Kontrak karya atau persetujuan pertama ditandatangiani dalam tahun 1967, ketika Suharto sudah mengkhianati presiden Sukarno dengan Supersemarnya tanggal 11 Maret 1966. Kontrak karya 7 April tahun 1967 itu dibikin ketika Suharto berhasil didudukkan (dengan berbagai rekayasa ) sebagai pejabat Presiden (sejak 7 Maret 1967) dan Bung Karno masih belum sepenuhnya didongkel secara resmi oleh MPRS tahun 1968.

Sebenarnya, kontrak karya dengan Freeport dalam tahun 1967 yang ditandatangani oleh kekuasaan di bawah Suharto juga merupakan soal yang bisa menimbulkan pertanyaan tentang keabsahannya, mengingat bahwa antara tahun 1963 sampai 1969, Papua Barat (waktu itu kita sebut Irian Barat) sedang menjadi daerah perselisihan internasional (international dispute region), sedangkan dari 1 Oktober 1962 hingga 1 Mei 1963 merupakan daerah perwalian PBB di bawah United Nations Temporary Execituvie Authority (UNTEA).

Bagaimanapun juga, tindakan Suharto (dan konco-konconya di Angkatan Darat dan tokoh-tokoh yang anti Bung Karno dan pro-nekolim waktu itu) menyetujui kontrak karya dengan maskapai raksasa Freeport dalam tahun 1967 adalah tambahan bukti bahwa politik Suharto dan konco-konconya adalah pengkhianatan terhadap tujuan revolusi 45 dan berbagai politik yang digariskan oleh presiden Sukarno bersama-sama MPRS (antara lain, UUD 45, Manipol, Trisakti, singkatnya : Panca Azimat Revolusi), yang orientasi besarnya adalah anti-imperialisme atau anti-nekolim serta mengutamakan pengabdian kepada negara dan rakyat.

Apa yang telah dikerjakan oleh pemerintahan Orde Baru dan diteruskan oleh berbagai pemerintahan sesudahnya (sampai yang sekarang) mengenai Freeport dan rakyat Papua adalah bertentangan sama sekali dengan tujuan Trikoranya Bung Karno. Tujuan perjuangan untuk membebaskan Papua (Irian Barat) dari penjajahan dan menyatukannya dengan RI, yang digelorakan secara besar-besaran dan bertahun-tahun oleh Bung Karno, adalah berlawanan seratus delapan puluh derajat dengan politik Suharto dan konco-konconya.

Agaknya, salah sekali kalau ada orang yang beranggapan bahwa Bung Karno, yang sejak masa mudanya sudah terkenal sebagai pejuang besar anti-imperialisme dan anti-kolonialis Belanda, ingin menjalankan kolonialisme dan menyengsarakan rakyat di Papua (Irian Barat). Dalam pidato-pidatonya Bung Karno dengan jelas sekali, dan sering sekali, mengatakan bahwa perjuangan rakyat Indonesia mengenai Irian Barat adalah justru untuk melaksanakan prinsip menghentikan « exploitation de l’homme par l’homme » dan « exploitation de la nation par la nation ». (pemerasan manusia oleh manusia dan pemerasan suatu bangsa oleh bangsa lainnya).

Dari segi ini pulalah kelihatan sekali bahwa banyak sekali politik Suharto (dan konco-konconya di Angkatan Darat waktu itu) yang berrangkulan dengan imperialisme AS dan kekuatan nekolim lainnya bertolak-belakang dengan jiwa revolusi rakyat Indonesia di bawah pimpinan Bung Karno.


SEANDAINYA BUNG KARNO TIDAK DIKHIANATI ........

Karena itu, jelas pulalah bahwa seandainya (sekali lagi, « seandainya » !) presiden Sukarno tidak dikhianati dan digulingkan oleh Suharto dan konco-konconya di Angkatan Darat, tidak akan muncul persoalan Freeport, seperti yang kita sama-sama saksikan dewasa ini. (Juga tidak akan terjadi pelanggaran HAM secara besar-besaran selama puluhan tahun. Juga korupsi tidak akan merajalela secara ganas di banyak bidang dan di banyak tingkatan. Juga tidak akan menjalar dekadensi moral atau degenerasi mental di kalangan « atas » masyarakat kita seperti yang dapat kita sama-sama kita saksikan selama ini.)

Freeport di Papua adalah simbul yang jelas atau manifestasi terpusat dari kerusakan besar yang telah dibikin oleh Suharto bersama-sama kawan-kawan pendukungnya terhadap negara dan rakyat Indonesia. Karena itu, kalau kita sekarang bicara tentang Freeport perlu sekali kita ingat juga bahwa pokok pangkal masalahnya yalah kontrak karya yang dibuat tahun 1967 di bawah kekuasaan politik Suharto dkk.

Kontrak karya tahun 1967 telah dengan mudah dibikin oleh Suharto (dan pendukung-pendukungnya) karena Angkatan Darat sudah menggulung habis-habisan kekuatan pendukung politik presiden Sukarno, terutama PKI, dan makin tersisihnya Bung Karno sebagai tokoh besar revolusi Indonesia. Kekuatan kiri di Indonesia, yang terutama terdiri dari anggota dan simpatisan PKI, merupakan tanggul besar atau tameng raksasa bagi rakyat Indonesia untuk mencegah hadirnya neo-kolonialisme, termasuk Freeport. Justru tanggul besar atau tameng raksasa inilah yang telah dihancur-leburkan oleh pengkhianatan Suharto dkk, dengan membunuhi dan memenjarakan jutaan orang tidak bersalah.

Dilihat dari sudut sejarah dapatlah kiranya ditamsilkan bahwa penandatanganan kontrak karya dengan Freeport oleh Suharto dkk dilakukan di atas fondamen yang terdiri dari tumpukan jutaan mayat orang kiri pendukung politik Bung Karno dan disuburkan dengan pengkhianatan yang tidak tanggung-tanggung terhadap pemimpin besar rakyat kita, Bung Karno.


BOM WAKTU YANG DITANAM OLEH SUHARTO DKK

Sekarang ini, masalah Freeport muncul lagi sebagai bom waktu yang ditanam oleh Suharto dkk , tetapi dalam skala yang lebih besar, dan dengan berbagai aspek yang makin lebih serius dari pada yang sudah-sudah. Kelompok-kelompok dari berbagai suku sudah mengobarkan aksi-aksi untuk menuntut perlakuan yang lebih baik dan lebih adil dari pemerintah RI dan Freeport, meneruskan perjuangan yang sudah bertahun-tahun mereka lancarkan. Perjuangan berbagai suku ini dewasa ini diperkuat oleh aksi-aksi yang dilancarkan para mahasiswa Universitas Cenderawasih di Papua (juga di berbagai kota besar Indonesia).

Juga berbagai LSM di Indonesia telah dan sedang mengadakan kegiatan atau aksi-aksi untuk mengangkat masalah kejahatan, pelanggaran HAM, ketidak-adilan bagi rakyat Papua, dan korupsi di kalangan TNI, untuk « pengamanan » Freeport. Sejumlah warga Papua, termasuk para mahasiswa sudah terpaksa minta suaka politik di Australia dan Papua NG. Ini merupakan tamparan yang memalukan terhadap muka pemerintah Indonesia yang memang sudah lama terkenal buruk. Pemanggilan pulang Dubes Indonesia dari Australia sebagai protes terhadap pemberian suaka politik warga Papua tidak bisa menutupi berbagai kesalahan dan keburukan yang terjadi di Papua yang dilakukan sejak rejim militer Orde Baru.

Hiruk-pikuk sekitar masalah Freeport ini adalah akibat politik rejim militer Orde Baru, yang selama puluhan tahun telah membiarkan maskapai besar Amerika ini menjalankan pengurasan secara besar-besaran kekayaan bumi Papua dan menarik keuntungan yang besar dari pertambangan emas, tembaga dan bahan-bahan mineral lainnya. Selama puluhan tahun Freeport hanya mendatangkan keuntungan dan kenikmatan bagi pemegang sahamnya, dan sejumlah orang-orang penting Indonesia yang terlibat dalam kerjasama dalam projek raksasa ini.

Dalam kontrak karya tahun 1967, yang rupanya dibikin terlalu buru-buru dan sembrono, karena sudah « kebelet » ingin menunjukkan bahwa kekuasaan di bawah Suharto berlainan dengan kekuasaan presiden Sukarno, telah disetujui bahwa pembagian saham dalam PT Freeport Indonesia adalah 81,28% untuk Freeport, pemerintah Indonesia 9,36% dan PT Indocopper Investama juga 9, 38%. Pembagian saham tersebut sudah menunjukkan kejanggalan yang menyolok, mengingat bahwa justru pemilik buminya, yaitu negara Indonesia, hanya mendapat saham yang begitu kecil.


HANYA MENGUNTUNGKAN SEGOLONGAN KECIL ORANG

Selama puluhan tahun, karena begitu kecilnya saham pihak Indonesia, maka pendapatan negara, yang berupa royalti serta pajak (dan lain-lainnya) pun dengan sendirinya juga kecil sekali, kalau dibandingkan dengan seluruh keuntungan Freeport. Dan, yang lebih menyedihkan lagi, pendapatan negara yang relatif kecil ini pun banyak sekali yang digerogoti oleh korupsi yang merajalela di kalangan atas dan menengah, baik sipil maupun militer. Tidak bisa dibayangkan lagi betapa besar jumlah dana yang berkaitan dengan projek Freeport ini yang « hilang « masuk kantong pejabat-pejabat rejim militer Orde Baru, dan selama puluhan tahun pula !

Pada masa Orde Baru, berbagai hal buruk dan janggal tentang Freeport ini tidak bisa banyak diungkap atau dibongkar berhubung banyak orang (juga di kalangan Orde Baru sendiri) menganggap bahwa maskapai raksasa Amerika ini punya hubungan yang erat sekali dengan Suharto dan keluarganya atau konco-konco dekatnya (ingat, antara lain Ibnu Sutowo, Bob Hasan, Tommy Suharto, Prabowo, Humpuss, Nussamba.)

Tetapi, sekarang, bom waktu yang ditanam tahun 1967 oleh Suharto dan konco-konconya di Angkatan Darat (wakktu itu) mulai meledak, yang sulit diramalkan sampai sejauh mana buntutnya atau apa saja yang bisa menjadi akibatnya di kemudian hari.


PERLU PENINJAUAN KEMBALI KONTRAK KARYA

Penyelesaian masalah Freeport di Papua dewasa ini membutuhkan ikut sertanya masyarakat Papua secara sungguh-sungguh dan aktif (antara lain lewat: MRP-Majelis Rakyat Papua, DPRP- Dewan Perwakilan Rakyat Papua, organisasi mahasiswa Papua). Di samping itu pemerintah perlu memperhatikan pendapat dan suara-suara yang dilontarkan oleh berbagai organisasi dan LSM Indonesia non-Papua yang selama ini memperjuangkan kepentingan rakyat Papua dan mengkritik berbagai kebijakan yang tidak adil atau buruk tentang Freeport.

Gencarnya perlawanan masyarakat Papua dan tajamnya kritik berbagai kalangan di Indonesia mengenai Freeport mengharuskan pemerintah SBY mengambil tindakan-tindakan yang mendasar untuk memperbaiki kesalahan-kesalahan yang sudah dilakukan sejak puluhan tahun oleh Orde Baru. Perlakuan yang lebih adil bagi kepentingan masyarakat Papua adalah kunci untuk penyelesaian masalah yang sudah menjadi semakin rumit sekarang ini. Untuk ini pemerintah SBY perlu berani memaksakan peninjauan kembali kontrak karya dengan Freeport sehingga kehadirannya di Papua betul-betul ikut mendatangkan keadilan dan kemakmuran bagi masyarakat Papua dan juga bagi negara dan rakyat Indonesia lainnya.

Masalah Freeport tidak bisa diselesaikan dengan pola dasar fikiran Orde Baru, dan juga oleh orang-orang yang bermental seperti Suharto dkk. Sebab, tokoh-tokoh Berkely Maffia, CSIS, ICMI serta banyak kalangan intelektual dari berbagai lembaga yang menyokong rejim militer Orde Baru ( seperti DPR dan MPR), telah diam seribu bahasa dan berpangku tangan saja selama puluhan tahun Orde Baru terhadap berbagai keburukan kebijakan pemerintah yang berkaitan dengan Freeport.

Dalam arus besar perlawanan sedunia menentang neo-liberalisme dan globalisasi, perjuangan kita bersama menentang ketidak adilan Freeport merupakan satu bagian yang penting. Pengalaman revolusioner presiden Hugo Chavez di Venezuela dan presiden Evo Morales di Bolivia dalam membela kepentingan rakyat kecil melawan penghisapan modal-modal raksana Amerika (dan negara-negara lainnya) menunjukkan bahwa di bawah pimpinan politik yang benar-benar untuk kepentingan rakyat banyak, modal raksasa seperti Freeport pun akhirnya bisa dilawan.

Agaknya, arah inilah yang harus sama-sama kita tempuh !

Tambahan : untuk mendapat informasi yang agak lengkap tentang masalah-masalah Freeport dan Exxon harap disimak kumpulan berita ; “Sekitar masalah Freeport di Papua” dan “Tentang Exxon dan harga diri bangsa”.
