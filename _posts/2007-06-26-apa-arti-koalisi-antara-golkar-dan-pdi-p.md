---
layout: post
title: Apa arti koalisi antara Golkar dan PDI-P
date: 2007-06-26
---

“Silaturahmi nasional Partai Golkar dan PDI-P” yang diadakan di Medan pada tanggal 20 Juni 2007, yang menurut berita-berita pers disebutkan sebagai langkah akan tergalangnya koalisi jangka panjang antara kedua partai tersebut menarik perhatian banyak orang, tetapi juga mengejutkan atau membingungkan banyak kalangan. Bahkan, mungkin juga, ada berbagai kalangan yang mengkuatirkannya atau sangat memprihatinkannya .

Hal yang demikian itu adalah wajar sekali. Sebab, walaupun berita-berita tersebut belum tentu mengandung kebenaran akan terjadinya koalisi sesungguhnya, maka sudah merupakan persoalan yang banyak dibicarakan oleh berbagai kalangan politik di Indonesia.

Karena, kemungkinan, adanya koalisi Partai Golkar dan PDI-P ini akan menimbulkan dampak yang besar bagi baik-buruknya negara dan bangsa kita pada dewasa ini (dan di kemudian hari). Sebab itu, seyogianyalah masalah ini -- sejak sekarang -- menjadi topik kajian bagi berbagai golongan dan kalangan, yang terdiri dari aktivis berbagai partai, aktivis ornop atau LSM, para intelektual di semua bidang, pimpinan organisasi massa (buruh, tani, pemuda, mahasiswa), tokoh-tokoh berbagai agama, pegawai-pegawai negeri, dan pengusaha. Singkatnya, makin banyak orang yang mempersoalkan pro dan kontra-nya, akan makin bergunalah untuk kepentingan rakyat kita, baik yang generasi sekarang maupun yang akan datang.

Sebab, kalau kita simak berita berbagai suratkabar mengenai perkembangan tersebut, maka nampaklah bahwa memang banyak sekali hal-hal yang perlu kita telaah dalam-dalam, atau banyak soal-soal serius yang bisa kita coba pertimbangkan dengan pandangan sejauh mungkin dan wawasan seluas mungkin. Untuk bisa sama-sama dihayati oleh kita semua, berikut ini adalah cuplikan dari berita-berita tersebut:

Menurut harian Kompas 21 Juni 2007 : “ Ketua Dewan Pertimbangan Pusat PDI-P Taufik Kiemas mengatakan, pertemuan itu dirancang selama hampir dua tahun. “Jika dijinjing berdua, rasanya lebih ringan dibanding dibawa sendiri”, katanya

“ Ketua Dewan Penasehat Partai Golkar Surya Paloh menambahkan, silaturahmi ini bukan hanya menuju Pëmilu 2009, tetapi lebih panjang lagi, hingga dua atau tiga generasi. “Mulai hari ini antara Partai Golkar dan PDI-P terjadi kesepakatan saling asah, asuh, dan asih”, katanya.

“ Surya mengatakan, jatuh bangunnya bangsa ini tidak terlepas dari tanggungjawab bersama Golkar dan PDI-P. Pengalaman empiris menunjukkan keberlangsungan negara membutuhkan keadaan yang stabil. Indonesia punya pengalaman tahun 1967 hingga 1998, saat ada kestabilan politik, pembangunan bisa berlangsung meski kurang memberi ruang bagi demokrasi.Demokrasi bukan tujuan, melainkan sekadar cara untuk mencapai kesejahteraan rakyat. “Demokrasi tak ada gunanya jika tidak ada kesejahteraan”, katanya.

“ Surya menegaskan Golkar dan PDI-P merasa ikut bertanggungjawab atas maju mundurnya pembangunan bangsa ini. “Langkah konkret kami, koalisi. Tetapi, itu belum dilakukan”, katanya. (kutipan dari Kompas selesai).

Sedangkan menurut Suara Merdeka (21 Juni 07 juga) Surya mengatakan bahwa Partai Golkar dan PDI-P harus mampu berperan serupa demi kepentingan pembangunan dan masa depan bangsa dan negara. Ia mengakui bahwa Indonesia merupakan negara multipartai. Namun demikian konsep multipartai itu terbukti telah membuat bangsa ini jatuh bangun. Dia mencatat sudah 18 kali pemerintahan jatuh-bangun dengan konsep multipartai. “Bahkan itu pula kiranya yang menjadi alasan kenapa presiden Sukarno mengeluarkan Dekrit Presiden 5 Juli”, katanya.

Pada bagian lain ia juga mengungkapkan era reformasi unik, di mana setiap orang memiliki kebebasan yang nyaris tanpa batas, baik untuk berbicara, bereaksi, menulis maupun berbuat apa saja yang diinginkan. Namun demikian ia mengingatkan sudah terjadi empat kali pergantian kepala negara selama era reformasi yang semua menunjukkan ketidakstabilan politik di negara ini.

Taufik Kiemas menyatakan Partai Golkar dan PDI-P menargetkan perolehan suara hingga 60 persen pada Pemilu 2009. Menurut suami mantan presiden Megawati Sukarnoputri itu, setelah target raihan suara itu tercapai baru kemudian akan menentukan siapa yang akan jadi presiden pada pilpres. “Jika kita sudah menang pemilu, baru kita akan tentukan siapa presidennya. Bisa dari Golkar dan bisa juga dari PDI-P”, tambahnya (kutipan dari Suara Merdeka selesai).

Menurut Suara Pembaruan (22 Juni 2007), Partai Golkar dan PDI-P sepakat membangun Aliansi Nasionalis Kebangsaan. Kebersamaan dua partai besar itu sudah terlihat sejak Surya dan Taufiq tiba di Bandara Polonia, Medan. Surya dikalungi bunga oleh kader PDI-P, dan Taufiq mendapat kalungan bunga dari kader Golkar. Di setiap sudut kota tampak bendera kuning Partai Golkar dikibarkan berdampingan dengan bendera merah PDI-P.

Spanduk ucapan selamat datang disertai foto Surya dan Taufiq juga dipasang di sejumlah lokasi strategis. Tidak ketinggalan terpampang pula foto Ketua Umum Partai Golkar Jusuf Kalla dan Ketua Umum PDI-P Megawati Sukarnoputri.

Menurut Tempo Interaktif (24 Juni 2007), Taufik Kiemas menyatakan bahwa antara PDI-P dan Partai Golkar memiliki kesamaan ideologi. Sehingga sangat wajar jika di antara keduanya bergabung membentuk koalisi untuk membangun bangsa. Kiemas sendiri mengungkapkan bahwa pertemuan yang dilakukan dirinya dengan Surya Paloh di Medan bukanlah pertemuan antara individu. Namun merupakan pertemuan antara lembaga politik yaitu antara PDI-P dan Partai Golkar.

Kiemas mengungkapkan bahwa pihaknya akan segera memanggil ketua DPD dan DPC PDI-P di seluruh Indonesia untuk menindaklanjuti hasil pertemuan di Medan. “Akan kami panggil mereka, dan mereka berkewajiban untuk mensosialisasikan dan menindaklanjutinya di daerah masing-masing”..

Persamaan ideologi antara PDI-P dan Golkar ?

Seperti yang bisa kita simak dari berita-berita tersebut di atas, memang banyak hal penting yang patut menjadi perhatian kita dan juga perlu dipertanyakan. Umpamanya : ketika digambarkan oleh Taufik Kiemas dan Surya Paloh bahwa ada persamaan ideologi antara Golkar dan PDI-P. Menyatakan bahwa antara kedua partai ada kesamaan ideologi ini berarti bahwa melupakan fakta sejarah bahwa Golkar telah didirikan oleh tokoh-tokoh Angkatan Darat sebelum peristiwa G30S untuk menentang politik Presiden Sukarno dan mengimbangi PKI, yang merupakan gabungan kekuatan besar anti-imperialis (terutama As).

Sedangkan selama 32 tahun Orde Baru, Golkar merupakan tulang-punggung yang luar biasa kuatnya dan besarnya, dan sekaligus alat yang maha ampuh, dalam mendukung diktatur militer yang telah melakukan banyak kejahatan/pelanggaran HAM, mempraktekkan pencekekan kehidupan demokratis selama puluhan tahun, melakukan korupsi dan nepotisme, merusak moral bangsa, dan melemahkan dan membusukkan semangat pengabdian kepada rakyat dan bangsa.

Bahwa, secara luarnya (!), antara kedua partai (Golkar dan PDI-P) terdapat sejumlah persamaan-persamaan tertentu adalah masuk akal atau, bahkan, wajar-wajar saja. Umpamanya saja, bahwa kedua belah fihak sudah sama-sama menyatakan :

“menjaga keutuhan Negara Republik Indonesia dengan mempertahankan Negara Kesatuan Republik Indonesia (NKRI), mempertahankan Pancasila sebagai ideologi Negara Republik Indonesia. mempertahankan Undang-undang Dasar RI 1945. dan mempertahankan Bhinneka Tunggal Ika dengan menjaga pluralisme”.

Tetapi, banyak sekali di antara kita yang ingat dan mencatat bahwa mengenai Pancasila, umpamanya, Golkar selama Orde Baru telah ikut-ikut pimpinan TNI-AD secara aktif memalsu isi atau merusak jiwa Pancasila, dan bahkan mengkhianati penciptanya, yaitu Bung Karno. Orde Baru secara terus-menerus, dan dengan lantang pula, telah menggembar-gemborkan Pancasila yang sudah di”vermaak”. Dalam prakteknya yang selama 32 tahun, nyatanya Pancasila telah dilecehkan atau diinjak-injak, juga oleh Golkar pula.

Mengenai Bhinneka Tunggal Ika dan menjaga pluralisme, Golkar juga selama puluhan tahun sudah menunjukkan dan mempraktekkan -- dengan cara-cara yang tidak demokratis pula -- politik yang telah mendominasi kekuasaan secara mutlak dan menguasai seluruh kehidupan politik (ingat : sekitar 70 % suara pemilu dan juga sekitar dua pertiga kursi parlemen dikuasai Golkar) dan tidak membolehkan berkembangnya kekuatan kiri (apalagi kekuatan golongan komunis).

Karenanya, sudah pastilah bahwa banyak di antara pendukung politik Bung Karno, atau para anggota keluarga korban peristiwa 65, atau juga para simpatisan PDI-P, yang tidak bisa mengerti kalau dinyatakan adanya kesepakatan antara Golkar dan PDI-P untuk membangun Aliansi Nasionalis, atau “silaturahmi”, yang bahkan menjangkau dua sampai tiga generasi yang akan datang.

Sejarah munculnya PDI-P

Sebab, pastilah banyak orang ingat bahwa PDI-P dilahirkan justru sebagai manifestasi perlawanan terhadap Orde Baru (yang intinya adalah TNI-AD dan Golkar), yang dipersonifikasikan waktu itu oleh sosok Megawati. Karenanya, banyak dari anggota-anggota keluarga orang-orang yang mendukung politik Bung Karno -- baik yang nasionalis, komunis atau yang dari kalangan agama – yang menaruh simpati kepada PDI-P hanya karena melihat sosok Megawati Sukarnoputri.

Boleh dikatakan bahwa sebagian terbesar para korban peristiwa 65, dan sebagian terbesar eks-tapol, beserta kalangan-kalangan luas lainnya yang anti-Orde Baru, adalah – tadinya -- pendukung atau simpatisan PDI-P. Dengan adanya berita-berita tentang adanya koalisi atau aliansi, atau “pendekatan” dengan Partai Golkar, maka mudah diperkirakan adanya kekecewaan (dan kebingungan) yang besar sekali di kalangan mereka ini. Sebab, dengan perkembangan yang begini ini, maka banyak orang menganggap bahwa PDI-P sudah tidak lagi bisa dijadikan penyalur aspirasi perlawanan berbagai golongan terhadap sisa-sisa Orde Baru. Banyak orang yang kuatir juga bahwa dengan “koalisi” ini PDI-P terikat sekali dan menjadi tidak bebas untuk mempersoalkan kesalahan dan kejahatan yang telah dilakukan Orde Baru.

Padahal, mempersoalkan kesalahan atau kejahatan Orde Baru adalah mutlak perlu sekali, untuk bisa mengadakan pembetulan atau menjalankan reformasi, demi kebaikan bangsa dan negara. Dan perlu sekalilah kiranya kita ingat bahwa berbagai golongan yang dirugikan oleh kesalahan atau kejahatan rejim militer Orde Baru adalah besar dan luas sekali. Mereka ini terdiri dari semua suku, semua agama, semua kalangan, dan semua bidang pekerjaan serta kehidupan. Berbagai macam akibat dari kesalahan dan kejahatan rejim militernya Suharto dkk ini sampai sekarang masih nampak nyata secara jelas dan luas sekali.

Memang, tentunya, ada berbagai alasan atau latar-belakang bagi kedua belah fihak untuk mengadakan koalisi antara Golkar dan PDI-P ini. Dan juga ada berbagai pandangan atau analisa mengenai kemungkinan adanya pendekatan atau kesepakatan antara kedua partai tersebut. Sebagian orang hanya melihat adanya kepentingan untuk menghadapi pemilu dan pemilihan presiden dalam 2009. Ada yang mentafsirkan sebagai usaha untuk mencegah terpilihnya kembali SBY sekali lagi. Tetapi ada juga yang menduga bahwa ini adalah untuk mencegah golongan-golongan Islam tertentu menguasai sepenuhnya kekuasaan negara, dan juga mencegah adanya bahaya separatisme serta menjaga kelangsungan Negara Kesatuan RI.

Golkar sebagai bagian utama Orde Baru

Apapun latar belakang atau pertimbangan-pertimbangan yang mungkin terdapat di belakang gagasan untuk mengadakan koalisi ini, satu hal yang harus diamati dengan waspada oleh kita semua ialah hilangnya kemandirian PDI-P dan berobahnya partai yang (tadinya atau awalnya) didambakan sebagai pengemban hati nurani para penentang rejim militer Orde Baru menjadi sekutu atau “tawanan” dari Golkar. Sebab, perlu diingat bahwa sejarah bangsa kita sudah mencatat Golkar sebagai sebuah partai yang menjadi bagian yang utama dari rejim diktatorial yang sudah dinajiskan oleh sebagian terbesar rakyat kita. Dan banyak bukti atau indikasi yang menunjukkan bahwa Partai Golkar yang sekarang adalah pada pokoknya, atau pada dasarnya, masih Golkar yang dulu-dulu juga.

Selama Golkar tidak mau terang-terangan dan belum berani secara tulus mengakui kesalahan-kesalahannya yang besar sekali dan dosanya yang banyak sekali (!!!) ketika menjadi tulang-punggung Orde Baru, maka banyak orang akan memandang tidak pantaslah baginya untuk menjalin koalisi dengan PDIP-P. Kiranya, kalau Golkar sudah terang-terangan -- dan dengan tulus pula --menyatakan putus segala macam hubungannya secara politik dan moral dengan Orde Baru beserta sisa-sisanya, maka barulah banyak orang bisa melihat dengan lega hati kepada terjalinnya koalisi dengan PDI-P.

Sebab, kalau melihat komposisi atau perimbangan kekuatan politik di Indonesia dewasa ini, maka kelihatan bahwa Golkar dan PDI-P memang menduduki posisi yang penting (Golkar punya 128 kursi di DPR, dan PDI-P 109 kursi, PPP 58, Partai Demokrat 57, PKB 52, PAN 52). Berdasarkan angka-angka itu, kalau terjadi koalisi antara Golkar dan PDI-P, maka kemungkinan sekali bahwa kedua partai tersebut akan bisa menguasai lebih dari 50% kursi di DPR.

Citra dan kewibawaan PDI-P

Jadi, dalam menjalin pendekatan dengan Golkar, kiranya banyak orang mengharapkan bahwa PDI-P tidak takut-takut untuk menyatakan terang-terangan perlawanannya terhadap sisa-sisa Orde Baru, berani menyuarakan aspirasi politik para pendukung Bung Karno yang anti-imperialis, dan bersedia bekerjasama atau bahkan membantu macam-macam gerakan atau golongan yang berjuang melawan neo-liberalisme dan globalisasi.

Kiranya, sejarah bangsa kita akan membuktikan di kemudian hari bahwa besarnya kewibawaan PDI-P justru terletak pada kesetiaannya mengabdi (dengan sungguh-sungguh dan tulus !) kepada kepentingan rakyat Indonesia, yang sebagian terbesar masih miskin dan menderita. Juga, generasi kita, yang sekarang dan di kemudian hari, akan melihat keluhuran citra PDI-P pada keberpihakannya kepada golongan kiri atau yang betul-betul (!!!) menjunjung tinggi dan mempraktekkan Pancasila dan Bhinneka Tunggal Ika.

Sebaliknya, PDI-P akan jatuh merosot di mata banyak orang dan kewibawaannya akan anjlog, kalau dalam pendekatan dengan Partai Golkar, partainya Megawati ini kehilangan kebebasannya untuk benar-benar menjalankan politik yang betul-betul mandiri dalam memperjuangkan kepentingan rakyat banyak. Kebesaran PDI-P bisa dilihat oleh banyak orang dengan jelas hanya kalau partai ini dengan berani dan gigih memperjuangkan reformasi terhadap segala keburukan dan kebusukan di segala bidang yang diakibatkan oleh Orde Baru. Bukan sebaliknya !!!
