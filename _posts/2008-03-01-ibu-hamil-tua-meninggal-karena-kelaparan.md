---
layout: post
title: Ibu hamil tua meninggal karena kelaparan
date: 2008-03-01
---

Berita yang dimuat dalam harian Fajar (Makassar) 1 Maret 2008 tentang meninggalnya seorang ibu yang hamil 7 bulan bersama bayinya karena kelaparan setelah tiga hari tidak makan, merupakan peringatan bagi kita semua bahwa rakyat Indonesia memang sedang menghadapi banyak hal yang parah sekali. Sebab, berita ibu hamil yang mati kelaparan bersama bayinya itu hanyalah satu dari ratusan juta rakyat Indonesia yang sedang dirundung penderitaan karena parahnya kemiskinan dan pengangguran. (Harap selalu diingat bahwa lebih dari 60 juta orang di antara rakyat kita hidup dalam kemiskinan, dan lebih dari 40 juta dalam pengangguran, ditambah lagi dengan besarnya penderitaan karena banjir di banyak daerah, gempa yang berkali-kali, kasus lumpur Lapindo dll dll)

Berita dari Makassar itu juga mengingatkan kepada kita semua bahwa sebagian ( yang amat besar !!!) dari rakyat Indonesia itu menderita karena jeleknya penyelenggaraan negara, yang disebabkan oleh rusaknya moral para pejabat dan « kaum elite » dalam masyarakat lainnya, sebagai akibat dari sistem yang dijalankan oleh Orde Baru selama 32 tahun dan diteruskan, sampai sekarang, oleh berbagai pemerintahan yang menyusulnya.

Kematian ibu hamil dengan bayinya karena sudah tiga hari tidak makan itu tambah meyakinkan kita semua bahwa kehidupan yang sulit sekali sebagian terbesar rakyat kita betul-betul sudah menuntut adanya perubahan besar-besaran di bidang politik, ekonomi, sosial. Sebab, penderitaan yang dialami ibu hamil yang mati itu, yang notebene tinggal di Sulawesi Selatan, yang biasanya terkenal sebagai « lumbung padi » bukanlah gejala yang langka atau yang « istimewa », kalau kita ingat bahwa di Jawa, Sumatra, Kalimantan, Indonesia Timur, juga ada banyak sekali daerah yang penduduknya miskin sekali. Begitu miskinnya, sehingga banyak rakyat tidak bisa hidup dengan layak dan sehat. Itulah sebabnya, sering di dalam pers kita dimuat adanya anak-anak sekolah yang kurang gizi, bahkan ada anak-anak yang gantung diri atau mati karena kurang makan, disebabkan kemiskinan orang tua mereka.

Kasus ibu hamil (dengan bayinya yang umur 7 bulan) yang mati kelaparan di propinsi daerah asal Wakil Presiden Jusuf Kalla (yang juga ketua umum partai Golkar), dengan keras dan jelas memperingatkan kita semua bahwa berbagai persoalan besar dan parah bangsa dan negara kita sudah tidak bisa lagi diharapkan dapat ditangani atau diselesaikan dengan sistem politik, ekonomi dan sosial seperti yang dianut oleh Orde Baru jilid II sekarang ini.

Sebenarnya, kasus ibu hamil yang mati kelaparan di Sulawesi Selatan ini adalah cermin dari wajah sebagian kehidupan bangsa kita, wajah yang bopeng-bopeng karena rusaknya moral atau wajah yang buruk karena banyaknya berbagai penyelewengan (antara lain korupsi yang merajalela). Wajah yang buruk ini tidaklah mungkin diperbaiki oleh kekuatan-kekuatan politik, yang selama ini mendukung politik Orde Baru jilid II.

Karena besarnya kerusakan moral dan parahnya pembusukan mental di kalangan partai-partai politik selama ini, maka tidak banyaklah yang bisa diharapkan dari hasil Pemilu 2009 nantinya.Sudah bisa diramalkan bahwa Pemiu 2009 tidak akan menghasilkan perubahan-perubahan besar atau mendasar di berbagai bidang, yang bisa memperbaiki kehidupan rakyat banyak. Perubahan yang sejati, perbaikan yang mendasar, hanyalah bisa dibikin oleh kekuasaan politik yang pro-rakyat, dan bukan oleh kekuasaan politik yang berbau-bau Orde Baru. Sebab, pengalaman Orde Baru selama 32 tahun ditambah pengalaman Orba Jilid II (artinya pemerintahan SBY-JK) sudah cukup jelas negatifnya bagi sebagian terbesar rakyat kita.

Kiranya, bagi banyak orang sudah jelas bahwa orang-orang seperti Jusuf Kalla, Aburizal Bakrie, (atau Prayogo Pangestu, atau lebih-lebih lagi Tommy Suharto, dan tokoh-tokoh lainnya yang pro-Suharto) tidak mungkin mau mendorong lahirnya kekuasaan politik yang pro-rakyat. Karena, politik pro-rakyat adalah justru sama sekali tidak mengutungkan kepentingan mereka.

Kekuasaan politik yang betul-betul pro-rakyat adalah tujuan perjuangan banyak golongan dalam masyarakat melalui pengembangan berbagai kekuatan yang demokratis dan anti Orde Baru, dengan menggunakan segala cara dan bentuk.

Sekarang ini, di seluruh tanah-air sudah dan sedang bergejolak berbagai perjuangan yang dilakukan dengan berani oleh golongan pemuda dan mahasiswa, golongan buruh atau pekerja di berbagai bidang, golongan tani, golongan perempuan, yang menuntut bermacam-macam perubahan atau perbaikan dan melawan ketidakadilan dan berbagai penyelewengan atau kejahatan. Akan datang saatnya, nantinya di kemudian hari, bahwa berbagai kekuatan yang masih berjuang secara terpisah-pisah sekarang ini, akhirnya akan mempersatukan diri, sehingga merupakan kekuatan yang bisa merebut kekuasaan politik, atau merobahnya, atau menggesernya.

Jelaslah bahwa di negeri kita, Indonesia, hanya kekuasaan politik yang betul-betul pro-rakyatlah yang akan bisa menjamin tidak adanya lagi ibu-ibu hamil yang mati kelaparan bersama bayi dikandungnya, seperti yang terjadi di Sulawesi Selatan ini.

Paris, 1 Maret 2008

A. Umar Said

======



Berita yang sangat mengharukan yang dimuat di Harian Fajar tanggal 1 Maret 2008 adalah sebagai berikut :



Judul : Kelaparan, Ibu Hamil Meninggal



MAKASSAR--Suasana di salah satu rumah yang terletak di Jalan Dg Tata I Blok 5, tampak lain dari biasanya. Puluhan warga di sekitar lorong itu, berkumpul dan tampak larut dalam suasana duka berbalut kesedihan.Salah seorang tetangga mereka, Dg Basse, 35, yang sedang hamil tujuh bulan, meninggal dunia bersama jabang bayi yang dikandungnya, sekira pukul 13.00 Wita. Tragisnya lagi, hanya berselang lima menit, Bahir, 5, anak ketiganya, juga menyusul meninggal.

Ibu dan anak ini meninggal akibat kelaparan setelah tiga hari tidak pernah menelan sebutir nasipun. Hal sama nyaris menimpa, Aco, 4, anak bungsu mendiang Basse. Untung saja, sebelum ajal datang menjemput, warga sekitar bergegas membopongnya ke ruang Unit Gawat Darurat Rumah Sakit Haji.

Pada saat itu, kondisi Aco (si anak bungsu) sudah sangat parah. Jangankan bergerak, mengedipkan kelopak matanya terlihat susah.

Basri, 40, suami dan ayah korban, yang sehari-harinya bekerja sebagai pengayuh becak, tak mampu berbuat apa-apa. Ia hanya bisa tepekur menyaksikan orang-orang terdekat dan amat dicintainya, telah meregang nyawa satu persatu setelah ia tiba di rumah.

Satu-satunya yang dia mampu lakukan hanya “memboyong” jazad istri dan seorang anaknya ke kampung halamannya di Kassi, Kabupaten Bantaeng, dengan ditemani Baha, 7, anak keduanya. Sementara untuk menjaga anak bungsunya yang dibawa ke Rumah Sakit Haji, Basri hanya memercayakan kepada anak sulungnya, Salma yang baru berusia 9 tahun.

“Sehari-hari memang sering terdengar suara anak-anak itu menangis. Kalau keluar rumah, biasanya kita tanya kenapako menangis nak. Katanya, mereka lapar,” tutur Mina, 42, tetangga korban yang sempat ditemui, kemarin.

Penuturan Mina ini juga diperkuat dengan hasil pemeriksaan tim medis RS Haji yang menangani anak bungsu mendiang Basse. Dokter jaga UGD RS Haji, dr Putu Ristiya mengatakan, Aco positif menderita gizi buruk.

Saat baru tiba di rumah sakit, kata Putu, kondisi kesehatan anak itu mengalami dehidrasi berat. Beratnyapun hanya 9 kg. Nanti setelah diberi cairan dua botol, kondisinya agak membaik. “Padahal untuk anak seusia ini (Aco, red) berat idealnya 15-20 kg. Jadi, ini positif marasmus (gizi buruk),” kata Putu Ristiya.

Berpenghasilan Rp5 Ribu

Bagaimana sebenarnya kehidupan Basri dan mendiang Basse? Menurut penuturan Mina, selama ini, ekonomi keluarga pengayuh becak itu memang sangat memprihatinkan. Penghasilan yang diperoleh tiap hari rata-rata hanya Rp5 ribu hingga Rp10 ribu saja. Akibatnya, untuk membeli beras amat kesulitan.

“Kalau mereka beli beras satu liter, biasanya itu untuk mencukupi makan selama tiga hari. Sehari semalam mereka cuma bikin bubur satu kali,” tutur Mina.

Sedangkan rumah kos ukuran 4x10 meter yang ditempatinya juga hanya dibayarkan orang lain, Dg Dudding yang merupakan sahabat Basri. Untuk satu tahun, Dg Dudding membayar sewa ruma itu Rp1,6 juta. Lantai bawah ditempati Dg Dudding dengan istri dan anaknya. Sedangkan Basri dan keluarganya tinggal di lantai atas.

“Mereka juga baru lima bulan tinggal di sini. Sebelumnya mereka tinggal di Jl Bonto Duri,” lanjut Mina.
Hanya saja, Mina mengaku belum terlalu akrab dengan tetangganya itu. Sebab, suami korban sangat pendiam dan cenderung tertutup.

“Sebenarnya kami juga selalu ingin membantu, cuma suami dan istrinya itu jarang bicara. Jadi yang kita ajak bicara biasanya cuma anak-anaknya,” terang Mina.

Herman, tetangga korban lainnya juga menuturkan, warga sekitar masih jarang yang akrab. Selain pendiam, mereka juga sangat jarang bergaul. “Bahkan selama ini, mereka masih menggunakan KTP Bantaeng,” katanya.
Sementara istri Dudding, Hasna mengungkapkan, kehidupan keluarga Basri memang sangat memilukan.

Bayangkan, mereka makan tanpa sayur. “Paling kalau makan kuahnya pakai minyak bekas penggorengan. Karena tidak punya uang untuk beli ikan, mereka juga hanya makan garam. Saya tahu, sebab saya sering melihat mereka makan dan memberinya ikan,” ungkap Hasna.

TRAGIS dan menyedihkan, memang. Kalimat itulah yang terasa pas disematkan untuk keluarga Basri. Dalam sehari, dia kehilangan istri, anak, dan jabang bayi dalam kandungan istrinya; Basse.

Rumah kayu yang berdiri di ujung lorong blok 4 Jl Dg Tata I, menjadi saksi bisu begitu beratnya hidup di Kota Makassar. Rumah tersebut tampak sangat kontras dengan rumah di sekelilingnya. Letaknya juga agak tersembunyi, sehingga tak tampak jelas dari luar.

Di atas rumah panggung itulah Basri dan keluarganya tinggal sejak lima bulan lalu. Sebagai pengayuh becak dengan berpenghasilan pas-pasan, kondisi tempat tinggal Basri begitu memprihatinkan. Bagian atas rumah yang ditempatinya dibagi empat petak. Untuk dapur, ruang tengah, ruang tidur, serta gudang.

Di tempat itu, jangan berharap mencari lemari atau perabot mahal lainnya. Sebab di situ hanya ada karung-karung berisi pakaian, rak piring, satu kompor, satu tungku, serta sejumlah peralatan masak, seperti panci dan piring tua, serta dua kasur usang.

Di atas kasur itulah, Basse dan anaknya, Bahir, meninggal karena kelaparan dan sakit. “Dia pertama sakit pada Kamis, sore. Saat itu, sepanjang malam ia menangis dan berteriak kesakitan. Ia sempat tidur saat Jumat subuh, namun hanya beberapa menit lalu terbangun lagi dan menangis kembali,” tutur Hasna, tetangga Basri yang menemani Basse hingga ajal menjemput. (Berita agak disingkat sedikit).
