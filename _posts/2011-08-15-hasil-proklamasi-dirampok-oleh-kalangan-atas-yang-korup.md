---
layout: post
title: Hasil Proklamasi Dirampok oleh Kalangan Atas Yang Korup
date: 2011-08-15
---

Setiap peringatan 17 Agustus merupakan kesempatan yang baik bagi banyak orang untuk mengenang kembali perjalanan panjang perjuangan rakyat Indonesia menuju kemerdekaan tahun 1945.Perjalanan panjang ini telah melalui, antara lain : Boedi Oetomo, Serikat Islam, Serikat Rakyat, pembrontakan PKI melawan Belanda tahun 1926,  terbuangnya ribuan orang di  kamp di Digoel, Sumpah Pemuda tahun 1928,  gerakan di bawah tanah melawan fasisme Jepang, dan revolusi 1945.

Memperingati dan merayakan 17 Agustus adalah kesempatan juga untuk mengingat kembali jasa-jasa Bung Karno (dan Bung Hatta) dan pemimpin-pemimpin lainnya dari berbagai golongan dan aliran politik dalam perjuangan untuk kemerdekaan bangsa kita. Karena itu, sudah sepatutnya, bahkan seharusnya, rakyat kita memperingati 17 Agustus ini dengan gembira dan bangga.

Namun, di samping itu,  adalah satu kenyataan, yang bisa sama-sama kita saksikan juga,   bahwa sebagian dari bangsa kita ada  yang merayakan 17 Agustus kali ini dengan berbagai perasaan yang penuh kesedihan, kekecewaaan, keresahan, bahkan kemarahan.

Perasaan yang demikian ini telah dimanifestasikan oleh banyak kalangan (buruh, tani, nelayan, perempuan, pemuda dan mahasiswa, rakyat miskin, penganggur, korban peristiwa 65, eks-tapol) dengan berbagai cara dan bentuk, selama setahun ini,  di banyak tempat di seluruh Indonesia. Bermacam-macam organisasi atau LSM telah banyak menyuarakan  perasaan berbagai kalangan itu.

Berbagai krisis parah membahayakan negara dan bangsa

Salah satu contohnya, yang patut mendapat perhatian dari kita semua, adalah pernyataan 45 tokoh nasional, yang baru-baru ini menyebutkan bahwa negara kita sudah dalam bahaya dan menjurus menuju ke jurang kehancuran. Bahkan mereka juga menuntut kepada DPR untuk  mengambil tindakan supaya presiden SBY mundur dari jabatannya.



Sebab, menurut mereka presiden SBY terbukti tidak mampu dan secara moral sudah tidak patut untuk menyelenggarakan negara dan kekuasaan pemerintahan. Sungguh, suatu pernyataan keras yang tidak tanggung-tanggung !

Kekecewaan atau kemarahan mereka, yang pada hakekatnya adalah juga kekewaan dan kemarahan sebagian terbesar rakyat kita disebabkan oleh berbagai krisis nasional yang sedang melanda negara dan bangsa kita. Yaitu  krisis kewibawaan kepala pemerintahan, krisis kewibawaan kepala negara, krisis kepercayaan terhadap parpol, krisis kepercayaan kepada parlemen, krisis efektifitas hukum, krisis kedaulatan sumber daya alam, krisis kedaulatan pangan, krisis kesehatan, krisis pendidikan, krisis integrasi nasional.

Banyaknya krisis parah yang melanda di begitu banyak bidang kehidupan yang penting-penting itu memang menunjukkan bahwa negara kita dalam bahaya. Kita semua tidak bisa meramalkan peristiwa-peristiwa besar dan penting apa saja (dan yang bagaimana) yang akan terjadi dalam masa depan yang dekat ini. Tidak  saja kasus Nazaruddin-Anas Urbaningrum bisa memunculkan surprise-surprise besar (dan sangat mengherankan) , tetapi juga banyak kasus-kasus besar lainnya.

Perbedaan sekarang dengan masa pemerintahan Bung Karno

Karena itu, perayaan 17 Agustus tahun ini akan dilangsungkan ketika negara sedang digoncang oleh berbagai persoalan-persoalan besar dan  kumuh (kotor), yang menunjukkan bahwa kerusakan atau kebobrokan yang diakibatkan oleh berbagai krisis nasional itu sudah memuncak sekali. Dan berbagai krisis nasional itu, bersumber kepada krisis moral atau krisis akhlak, yang terutama sekali sudah melanda secara luas di kalangan atas bangsa kita. Kasus Nazaruddin hanyalah salah satu contoh dari bagian kecil dari krisis moral itu.

Kehidupan bangsa  dan negara yang serba begitu rusak sekarang ini mengingatkan kita semua bahwa banyak kebobrokan (terutama kebobrokan moral kalangan atas) yang begitu parah itu tidak terlihat selama Bung Karno memimpin negara selama 20 tahun antara 1945 dan 1965. Sejarah mencatat bahwa sepanjang masa pemerintahan Bung Karno  itu moral patriotisme dan nasionalisme bangsa kita tinggi sekali. Tidak pernah dalam sejarah Republik Indonesia  (sampai sekarang !) semangat untuk mengabdi bangsa dan negara sebegitu tinggi seperti di zamannya Bung Karno.

Semangat revolusioner yang berkobar karena perjuangan 45 tersebar dimana-mana. Jiwa gotong-royong dan kesediaan untuk mengabdi kepada rakyat menjadi basis moral bagi sebagian besar masyarakat. Kejujuran dalam kehidupan politik, sosial dan ekonomi menjadi pedoman hidup banyak orang. Slogan « berbakti untuk nusa dan bangsa » bukanlah omong-kosong bagi sebagian besar rakyat kita serta pemimpin-pemimpinnya.

Itulah sebabnya, walaupun revolusi berlangsung lama sekali (antara 1945 dan 1949)  dan disusul dengan berbagai peristiwa besar (antara lain : pemberontakan RMS, DI-TII , Konferensi Bandung, PRRI-Permesta, pembangunan stadion Gelora Bung Karno , perjuangan Irian-Barat, Ganefo, berbagai konferensi Asia-Afrika di Indonesia) tidak terdengar adanya korupsi atau penyalahgunaan kekuasaan secara besar-besaran.

Situasi bangsa yang penuh dengan patriotisme yang begitu tinggi, dan nasionalisme  revolusioner yang begitu meluas di seluruh tanahair waktu itu adalah berkat kepemimpinan politik dan moral Bung Karno. Sejak dikhianatinya  Bung Karno oleh pimpinan Angkatan Darat (waktu itu), sampai sekarang tidak pernah lagi bangsa kita melihat adanya kepemimpinan (politik dan moral)  yang begitu besar dan luhur.

Mengenang kebesaran Bung Karno dan pengkhianatan Suharto

Oleh karena itu, ketika kita semua memperingati Hari Besar 17 Agustus, perlulah kiranya kita di samping mengenang jasa-jasa dan kebesaran Bung Karno juga sekaligus mengingat kembali juga kepada pengkhianatan Suharto beseta konco-konconya (di dalam dan luar negeri) terhadap bapak persatuan bangsa beserta pendukung-pendukung revolusionernya.

Sebab, sejarah sudah membuktikan dengan jelas, bahwa pengkhianatan pimpinan Angkatan Darat (waktu itu) terhadap Bung Karno telah menimbulkan kerusakan-kerusakan besar dan banyak sekali kepada kehidupan bangsa, dan terutama sekali telah melumpuhkan kekuatan demokratik dan revolusioner Indonesia. Sekitar tiga juta orang tak bersalah apa-apa telah dibunuhi secara biadab, dan ratusan ribu orang  kiri lainnya dipenjarakan dengan sewenang-wenang di berbagai tempat di Indonesia.

Kerusakan-kerusakan yang dibikin oleh Orde Baru selama 32 tahun itu akibatnya masih kelihatan sampai sekarang, meskipun  berbagai pemerintahan lainnya sudah menggantikannya. Kerusakan dan kebobrokan itu bahlan kelihatan lebih menghebat lagi di bawah kepemimpinan SBY. Jadi, segala kebobrokan atau kebusukan yang kita saksikan dewasa ini ada hubungannya yang erat dengan kerusakan-kerusakan yang telah ditimbulkan Orde Baru.

Kebobrokan atau kebusukan yang melanda negara dan bangsa sejak Orde Baru sampai sekarang ini sulit bisa diperbaiki oleh pemerintahan yang mana pun atau partai politik yang apa pun, sampai kapan pun,  dengan sistem politik, sosial dan ekonomi yang bagaimana pun, kalau  bertentangan ajaran-ajaran revolusioner Bung Karno, dan  mengkhianati Pancasila dan Bhineka Tunggal Ika.

Pemerintahan di bawah kepemimpinan  SBY adalah  bukti yang jelas tentang kegagalan sistem politik, sosial, dan ekonomi,  yang makin menjauhkan negara dan bangsa dari tujuan revolusi 17 Agustus 45.

Karena itu, ketika memperingati ultah ke 66 Republik Indonesia tahun ini kita perlu merenungkan bersama, jalan apakah yang harus kita tempuh bersama, dan dengan cara apa, dan dengan apa (dan juga dengan siapa-siapa saja) bangsa kita perlu berusaha terus untuk menciptakan cita-cita kemerdekaan, yaitu masyarakat adil dan makmur.

Dunia terus berubah, tetapi Indonesia mandeg dan bobrok.

Peringatan ke-66 hari kemerdekaan tahun ini kita adakan ketika dunia berubah terus dengan cepatnya. Amerika Serikat, yang dalam jangka lama sekali menjadi « pemimpin » dunia kapitalisme sekarang mengalami krisis ekonomi yang parah, dan menjadi negara yang hutangnya paling besar di dunia. Eropa Barat juga  mengalami berbagai problem ekonomi dan sosial yang parah.

Tiongkok, yang dalam masa silam termasuk negara dan bangsa yang terbelakang, sekarang sudah menjadi kekuatan ekonomi yang terbesar sesudah AS, dan bahkan akan melampauinya tidak lama lagi. Uni Soviet yang sudah runtuh digantikan oleh Rusia yang bangkit menjadi kekuatan ekonomi (juga politik dan militer) yang memainkan peran yang tidak kecil di bidang internasional. India, Vietnam, juga sudah meraih kemajuan-kemajuan besar sekali.

Amerika Latin, yang dalam masa lalu menjadi daerah setengah jajahan AS sudah berubah menjadi kekuatan yang tidak tunduk begitu saja kepada kemauan Washington. Peran Kuba, Venezuela, Bolivia, Peru, Ekuador, Brazilia, Argentina, untuk mendorong perubahan-perubahan besar di Amerika Latin masih tetap bisa dilanjutkan terus.

Pergolakan-pergolakan  di negara-negara Arab, yang dipelopori oleh gerakan-gerakan di Tunisia, Mesir, Libia, Yaman, Siria dan lain-lainnya lagi, sedang berlangsung terus dan bahkan meluas di negara-negara Arab lainnya,  dengan bentuk dan isi yang berbeda-beda. Pergolakan-pergolakan ini akhirnya akan bisa memberikan sumbangan dan dorongan penting kepada pembaruan Islam di dunia.

Perubahan-perubahan dan kemajuan-kemajuan besar di banyak negara di dunia itu semua membikin hati banyak orang di antara bangsa kita menjadi marah ketika membandingkannya  dengan keadaan negara kita dewasa ini yang serba mandeg, kacau, dan bobrok.

Hasil proklamasi dirampok oleh kalangan atas yang korup

Dengan jumlah 240 juta orang penduduk Indonesia adalah nomor empat terbesar di dunia, sesudah Tiongkok, India, dan Amerika Serikat. Tetapi, yang bisa dikatakan sudah betul-betul merasakan hasil kemerdekaan adalah hanya sekitar 20% saja atau sekitar 45 juta saja. Sedangkan yang 80%  (sekitar 200 juta) termasuk mereka yang masih belum merasakan hasil kemerdekaan.

Sebagian terbesar hasil Proklamasi kemerdekaan 17 Agustus telah dirampok secara besar-besaran oleh banyak kalangan atas (bidang eksekutif, legislatif, judikatif, pemuka-pemuka masyarakat, pimpinan partai-partai politik dan agama, pengusaha besar),  yang bersekongkol dengan kepentingan asing.

Kalangan  atas yang mengkhianati rakyat inilah yang selalu dengan rhetorika muluk-muluk « membela kepentingan rakyat », « menghormati UUD 45 », «menjunjung tinggi Pancasila dan Bhinneka Tunggal Ika » sebetulnya dalam praktek melakukan berbagai macam kejahatan secara berjemaah, untuk melakukan korupsi, penyalahgunaan dan pelecehan hukum, dan merekayasa kekuasaan, untuk memperkaya diri sendiri dan golongan mereka masing-masing.

Kasus Nazaruddin mempertinggi kesedaran politik banyak orang

Gonjang-ganjing besar kasus Nazaruddin-Anas Urbaningrum-Partai Demokrat  yang sedang membikin porak-poranda pemerintahan SBY sekarang ini akan berbuntut panjang selama berbulan-bulan, bahkan bisa bertahun-tahun. Karena, terlalu banyak kalangan atasan yang busuk yang tersangkut, sehingga timbul kecurigaan atau kewaswasan bahwa kasus besar ini akan diintervensi oleh berbagai rekayasa dari berbagai fihak.

Semua itu memberikan petunjuk bahwa perayaan 17 Agustus tahun 2011 berlangsung dalam suasana politik dan opini publik yang didominasi oleh kasus besar Nazaruddin. Situasi yang demikian ini adalah baik sekali sebagai pendidikan politik rakyat, yang memungkinkan banyak orang melihat dengan jelas bahwa negara dan bangsa kita memang betul-betul memerlukan perubahan besar-besaran dan fundamental.

Dan juga bahwa untuk perubahan besar-besaran dan fundamental, yang hanya mungkin melalui jalan revolusi itu,  diperlukan adanya pemimpin yang berjiwa revolusioner yang didukung oleh kekuatan atau gerakan yang luas dan revolusioner juga.

Munculnya tokoh atau pemimpin revolusioner di Indonesia mungkin memerlukan proses yang berliku-liku dan panjang. Pimpinan yang sedemikian ini akan tumbuh dari dan di tengah-tengah gerakan massa yang luas dalam meneruskan revolusi berjangka panjang dan terus-menerus, seperti yang selalu dianjurkan Bung Karno.
