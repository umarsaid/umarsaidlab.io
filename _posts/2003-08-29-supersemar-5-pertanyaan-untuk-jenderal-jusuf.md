---
layout: post
title: 'Supersemar: 5 Pertanyaan untuk Jenderal Jusuf'
date: 2003-08-29
---

Oleh : Asvi Warman Adam

SAMPAI hari ini naskah asli Supersemar (Surat Perintah Sebelas Maret) 1966belum ditemukan. Sementara itu pelaku yang terlibat dalam peristiwa tersebutsemakin sedikit. Soeharto sudah sakit-sakitan dan ingatannya makinberkurang. Harapan satu-satunya barangkali pada Jenderal (pur) Jusuf yangsampai sekarang belum mau berbicara dengan terbuka mengenai persoalan ini.Ada beberapa pertanyaan yang perlu dijawab oleh mantan Pangab itu.

Pertama, bagaimana sesungguhnya proses penyusunan dan penyerahan surattersebut yang terkesan tidak wajar.

Sudah diketahui umum bahwa surat tersebut dibuat bukanlah atas inisiatif dankemauan Soekarno sendiri. Tahun 1998, anggota Tjakrabirawa Letnan Dua (purn)Soekardjo Wilardjito mengaku bahwa Jenderal Panggabean menodongkan pistolnyakepada Presiden Soekarno, sementara Jenderal Jusuf menyodorkan map suratuntuk ditandatangani.

Kesaksian Soekardjo di atas didukung pula oleh Kaswadi (77 tahun) dan Serka(purn) Rian Ismail yang kini bermukim di Klaten, Jawa Tengah. Mereka melihatbahwa tamu yang datang ke Istana Bogor berjumlah empat orang, bukan tigaorang seperti yang diketahui selama ini. Bahkan Kaswadi mengakui bahwa "Padawaktu itu, 11 Maret 1966, saya melihat Panggabean ada di Istana Bogor. Saatitu sekitar pukul 01.00 WIB dinihari. Panggabean datang mengendarai mobiljip dan berpakaian dinas militer. Ia kemudian berjalan masuk menuju IstanaBogor", tutur Kaswadi kepada LBH Jogyakarta. Kesaksian tersebut perludipertegas, kejadiannya tanggal 11 Maret atau 12 Maret 1966 (karena sudahmasuk 01.00 pagi). (Wilardjito sendiri kemudian sempat diperiksa polisibahkan dihadapkan ke pengadilan negeri Yogyakarta. Namun belum jelasbagaimana keputusan hakim sampai sekarang).

Jabatan Mayjen M Panggabean pada waktu itu, seperti diungkapkan dalamtulisan Moerdiono "Di antara Para Sahabat: Pak Harto 70 tahun", adalah ketuaTim Umum yang dibentuk oleh Soeharto. Letnan Satu Infantri Moerdiono menjadisekretaris Tim Politik yang diketuai oleh Mayjen Basuki Rakhmat dan MayjenAshari menjadi ketua Tim Ekonomi.

Jumat pagi 4 September 1998, Jenderal M. Jusuf mengatakan bahwa yang menemuiSoekarno tanggal 11 Maret 1996 hanya 3 jenderal, dan mereka di sana hanyasampai pukul 20.30 Tidak ditanyakan wartawan, berapa lama ketiga jenderalitu di Istana Bogor dan apa saja yang dibicarakan mereka dengan PresidenSoekarno? Kalau betul mereka baru pulang pukul 20.30 malam, itumemperlihatkan bahwa pembicaraan dengan Soekarno berjalan alot.

Di dalam buku 70 tahun Soeharto itu, dimuat pula penuturan Sudharmono, iamenerima telpon dari Mayjen Sutjipto, Ketua G-5 KOTI, 11 Maret 1966 sekitarpukul 10 malam. Sutjipto meminta agar konsep tentang pembubaran PKIdisiapkan dan harus selesai malam itu juga. Permintaan itu atas perintahPangkopkamtib yang dijabat oleh Soeharto. Sudharmono sempat berdebat denganMoerdiono mengenai dasar hukum teks tersebut. Beberapa jam kemudian, 12Maret 1966 pukul 01.00 datanglah Sekretaris MBAD Brigjen Budiono membawadokumen yang kemudian dikenal sebagai Supersemar. Menurut Sudharmono suratperintah tersebut "diperbanyak (difotokopi) di kantor kami". Kurang jelasapakah pada waktu itu sudah ada mesin fotokopi di Jakarta.

Pertanyaan pertama di atas mengenai proses hanya tentu bisa dijawab olehJenderal Jusuf, salah satu saksi kunci yang hidup sampai sekarang. Soehartosendiri sudah berkurang ingatannya.

Pertanyaan kedua tentang siapa pengetik Supersemar? Beberapa waktu lalumuncul lagi pengakuan Letkol (pur) TNI-AD Ali Ebram, staf Asisten IIntelijen Resimen Cakrabirawa bahwa dia yang mengetik surat tersebut. Surattersebut diketik dalam waktu satu jam dengan didiktekan oleh Bung Karno. Iamengetik dengan gemetar dan mengatakan bahwa konsep itu berasal dariSoekarno sendiri. Yang diingatnya sekarang bahwa "dalam surat itu disebutajaran, koordinasi, terus laporan dan menyangkut empat poin: soal keluarga,melindungi keluarga yang tidak ada. Yang keempat itu memberi laporan".Sebagai orang yang tidak biasa mengetik, ia mengerjakannya dengan gemetardan berkata kepada Soekarno "Pak, saya mohon ampun kesesa (Pak, saya mohontidak tergesa-gesa). Sebelum ditandatangan diketik kotanya yaitu Bogor.

Betulkah Ali Ebram yang mengetik surat tersebut? Pasti Jenderal Jusuf tahupersis.

Pertanyaan ketiga, benarkah apa yang disampaikan oleh pakar AS Ben Andersonbahwa mungkin saja surat perintah yang asli itu dihilangkan karena diketikdengan kop Markas Besar Angkatan Darat. Jadi jika dipertahankan tentu sangatlucu, surat kepresidengan ditulis dengan kertas berkop MBAD. Jadi surat itu"dihilangkan" bukan karena isi tetapi karena kop suratnya. Ben mengutippengakuan seorang tentara yang mengaku waktu itu bertugas di Istana Bogor.Tidak dijelaskan nama dan pangkat tentara tersebut. Jika benar hal ini makaproses keluarnya surat perintah itu sebetulnya sudah direncanakan denganmatang di Jakarta, paling tidak oleh ketiga Jenderal tersebut.

Jawaban pertanyaan di atas jelas dapat dijawab oleh Jenderal Jusuf karenaialah yang membawa surat tersebut ke Jakarta dan menyerahkannya kepadaJenderal Soeharto.

Pertanyaan keempat, di mana naskah asli Supersemar berada? Konon, menurut KHYusuf Hasyim, dari Tebuireng Jombang, yang menyimpan naskah asli Supersemaritu adalah Mas Agung (almarhum). Bila ini benar, kenapa surat tersebutsampai jatuh ke tangan tokoh yang dekat dengan Bung Karno? Yusuf Hasyimdiberi salinan dua naskah Supersemar, yang satu berjumlah dua halaman,sedangkan yang satu lagi hanya satu halaman. Naskah asli itu konon kabarnyadisimpan di sebuah bank di luar negeri, diperkirakan di Singapura.

Jenderal Jusuf diduga mengetahui keberadaan surat tersebut. Paling sedikitia dapat mengungkapkan setelah dibaca oleh Soeharto, surat itu diserahkankepada siapa. Tentu saja, Sudharmono dan Moerdiono dapat dimintaiketerangan.

Pertanyaan kelima, apa yang dibicarakan oleh Jenderal Jusuf, Basuki Rachmaddan Amir Machmud di rumah Soeharto. Seperti sudah diketahui bahwa PresidenSoekarno meninggalkan sidang kabinet dan berangkat dengan helikopter keBogor, setelah mengetahui berkeliaran pasukan tidak dikenal di sekitaristana tanggal 11 Maret 1966. Jusuf, Basuki Rachmad dan Amir Machmud segerapergi ke rumah Soeharto di jalan Agus Salim. Bagaimana skenario yangdibicarakan mereka saat itu. Jawaban pertanyaan ini sangat penting dankrusial bagi penulisan sejarah Indonesia, khususnya sejarah terbentuknyaOrde Baru.

Mengenai Supersemar meskipun banyak kisah yang kontroversial di situ tetapisecara umum dapat disimpulkan bahwa surat tersebut bukanlah dibuat PresidenSoekarno dengan sukarela. Meskipun tidak ada todongan senjata, dapatdipahami bahwa penulisannya dilakukan dengan tekanan. Pada Supersemar,mungkin saja ia bisa berdalih tidak memaksa Soekarno, tetapi kenyataanketiga Jenderal pembantunya telah membuat Soekarno terpaksa untuk membuatSurat Perintah tersebut. Apalagi pada pagi 11 Maret 1966 berkeliaran pasukanyang tidak memakai tanda pengenal di sekitar Istana, sehingga Soekarnomemutuskan meninggalkan Istana dan pergi ke Bogor.

Perlu diingatkan kembali bahwa Undang-Undang no 7 tahun 1971 tentangKetentuan-Ketentuan Pokok Kearsipan, fasal 11 berbunyi: Barangsiapa dengansengaja dan dengan melawan hukum memiliki arsip sebagaimana dimaksud dalamfasal 1 huruf a Undang-Undang ini dapat dipidana dengan pidana penjaraselama-lamanya 10 (sepuluh) tahun.

Orang yang terbukti menyimpan naskah asli Supersemar dan tidakmenyerahkannya kepada negara ia bisa dijatuhi hukuman maksimal 10 tahunpenjara. Hal ini juga berlaku bagi siapa, termasuk seorang Jenderalsekalipun. Wahai Jenderal Jusuf berbicaralah. Sebelum terlambat.

Asvi Warman Adam, peneliti LIPI, doktor sejarah dari Ecole des Hautes Etudesen Sciences Sociales, Paris.

Sambungan yang lalu, bersambung.......

Koran Tempo, 29 Agustus 2003.
