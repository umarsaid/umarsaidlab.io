---
layout: post
title: Manifesto Politik Mahasiswa Indonesia
date: 2004-01-09
---

Menurut suratkabar Suara Pembaruan (6 Januari 2004) delapan organisasi kemahasiswaan mendeklarasikan "Manifesto Politik Mahasiswa Indonesia". Isi manifesto itu antara lain menolak masuknya politisi bermasalah ke lembaga legislatif dan eksekutif.

« Kedelapan organisasi itu adalah Dewan Pimpinan Pusat Ikatan Mahasiswa Muhammadiyah (DPP-IMM), Pengurus Besar Pergerakan Mahasiswa Islam Indonesia (PMII), Presidium Gerakan Mahasiswa Nasional Indonesia (GMNI), Pengurus Besar Himpunan Mahasiswa Islam (PB-HMI), Pimpinan Pusat Kesatuan Mahasiswa Hindu Dharma Indonesia (KMHDI), Kesatuan Aksi Mahasiswa Muslim Indonesia (Kammi) Pusat, Pengurus Pusat Perhimpunan Mahasiswa Katolik Republik Indonesia (PMKRI), dan Pimpinan Pusat Gerakan Mahasiswa Kristen Indonesia (GMKI).

« Menurut Ketua PMII A Malik Haramain, Senin (5/1), di Jakarta, mereka telah menetapkan sejumlah kriteria untuk menentukan bermasalah atau tidaknya seorang politisi. Kriteria itu antara lain terlibat dalam kasus korupsi, pelanggaran hak asasi manusia, perusakan lingkungan, kejahatan seksual, narkoba, tindak pidana, malas dan berkinerja buruk, serta penyalahgunaan kekuasaan.

« Berdasarkan kriteria tersebut, Malik menyebutkan Akbar Tandjung dan Wiranto digolongkan sebagai politisi bermasalah. "Kami meminta kedua politisi itu legowo untuk tidak mencalonkan diri menjadi anggota legislatif atau duduk di lembaga eksekutif, sampai ada putusan final terhadap perkara yang kini dihadapi kedua politisi itu," katanya.

« Organisasi mahasiswa yang mengeluarkan manifesto itu, lanjutnya, masih akan terus mengumpulkan data dan membuka posko pengaduan dari masyarakat menyangkut politisi bermasalah, kemudian mengumumkannya secara terbuka kepada masyarakat. "Kami berharap dalam pemilu 2004, masyarakat tidak memilih politisi bermasalah," tegasnya.

« Dalam manifesto itu, mahasiswa juga menolak bercokolnya kembali kekuatan lama, yakni militer, Orde Baru, dan kelompok yang tidak mendukung reformasi. Selain itu, para mahasiswa juga akan melakukan pendidikan politik kepada masyarakat dengan menginformasikan kriteria kelompok reformis dan antireformis, kelompok orang bermasalah dan Orde Baru, serta mengajak publik untuk menolak dan melawan upaya partai politik melakukan politik uang dan teror politik ». (kutipan dari Suara Pembaruan selesai)

## Manifesto Politik Yang Bersejarah

Diumumkannya Manifesto Politik Mahasiswa Indonesia oleh delapan organisasi mahasiswa tersebut merupakan perkembangan penting dalam kehidupan kemahasiswaan, yang pada waktu akhir-akhir ini mengalami semacam « pasang surut », dan karenanya suaranya terasa kurang bergema dalam kehidupan nasional kita. Ini berlainan dengan tahun-tahun 1997 dan 1998, ketika gerakan mahasiswa secara besar-besaran telah merupakan motor pendobrak untuk menggulingkan rezim militer Suharto dkk dan melancarkan reformasi di semua bidang kehidupan negara dan bangsa yang telah dirusak atau dibusukkan oleh Orde Baru.

Manifesto politik mahasiswa Indonesia ini dikeluarkan pada saat yang amat tepat, yaitu ketika bangsa kita sedang menghadapi tugas besar pemilu, dan ketika situasi negeri kita makin menunjukkan keprihatinan yang serius di berbagai bidang, terutama di bidang moral atau akhlak. Ketika bangsa kita sedang menghadapi ekonomi yang sulit, atau utang yang banyak dan pengangguran yang membengkak , para « tokoh » dan para politisi kita kelihatan sibuk sekali mencari segala peluang untuk memperkaya diri dengan korupsi atau kejahatan-kejahatan dalam bentuk lainnya.

Berbagai kesulitan atau kerusakan yang dihadapi oleh bangsa dan negara kita dewasa ini ada hubungannya yang erat dengan membusuknya akhlak atau moral para tokoh dan para politisi kita, yang kebanyakan terdiri dari orang-orang yang dididik atau « dibina » selama puluhan tahun oleh rezim militer Orde Baru. Dari segi ini dapat dilihat betapa pentingnya Manifesto PolitikMahasiswa Indonesia. Dapatlah kiranya dikatakan bahwa Manifesto Politik tersebut adalah termasuk peristiwa yang bersejarah bagi angkatan muda kita.

## Investasi Hari Depan Indonesia

Sesudah tergulingnya Suharto dari tampuk kekuasaan, banyak orang mengharapkan bahwa berbagai pemerintahan yang menggantikannya akan bisa memperbaiki kerusakan-kerusakan berat yang dibikin oleh rezim mikliter Orde Baru selama 32 tahun. Tetapi, nyatanya, pemerintahan Habibi, Abdurrahman Wahid, dan Megawati-Hamzah sekarang iniu tidak bisa berbuat banyak sekali untuk mendatangkan perobahan-perobahan yang mendasar. Kerusakan dan pembusukan yang ditimbulkan rezim militer ternyata begitu besarnya, sehingga tidak mudah untuk diperbaiki dalam tempo singkat. Di samping itu, sisa-sisa kekuatan Orde Baru masih begitu kuatnya atau begitu luasnya, sehingga kekuatan pro-reformasi atau pro-demokrasi menghadapi banyak kesulitan dalam menjalankan tugasnya.

Dalam situasi menjelang pemilu, di mana partai-partai mulai sibuk mempersiapakan diri dalam perebutan suara, atau dagang sapi yang penuh dengan kongkalikong dan kompromi, atau pencarian dana dengan cara-cara haram dan bathil (korupsi, selingkuh, penyelewengan atau pencurian) itulah dikeluarkan Manifesto Politik inj. Manifesto ini adalah suara generasi muda yang sudah muak (dan marah !) melihat situasi bangsa dan negara yang begitu rusak oleh karena kelakuan atau perbuatan para politisi kita.

Arti penting dan bersejarah Manifesto ini adalah bahwa ini dilahirkan oleh angkatan muda dari berbagai aliran politik dan agama. Ini menunjukkan bahwa gagasan-gagasan besar yang dituangkan dalam Manifesto ini merupakan platform bersama angkatan muda dewasa ini, dan sekaligus juga program kegiatan atau program perjuangan bersama. Tekad bersama yang dicantumkan dalam Manifesto Politik ini merupakan investasi politik (dan moral) yang amat penting dan berharga bagi HARI DEPAN negara dan bangsa kita.

## Manifesto Politik Yang Besar Artinya

Dalam Manifesto Politik ini angkatan muda Indonesia – melalui 8 organisasi mahasiswanya – telah secara tegas dan jelas menolak masuknya politisi yang bermasalah dalam lembaga legislatif dan eksekutif, yaitu orang-orang yang terlibat dalam kasus korupsi, pelanggaran hak asasi manusia, perusakan lingkungan, kejahatan seksual, narkoba, tindak pidana, malas dan berkinerja buruk, serta penyalahgunaan kekuasaan.

Isi yang juga amat penting dalam Manifesto ini adalah penegasan bahwa mahasiswa juga menolak bercokolnya kembali kekuatan lama, yakni militer, Orde Baru, dan kelompok yang tidak mendukung reformasi. Selain itu, para mahasiswa juga akan melakukan pendidikan politik kepada masyarakat dengan
