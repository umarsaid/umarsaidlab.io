---
layout: post
title: Selamatkan generasi yad dari wabah korupsi
date: 2003-01-27
---

Tidak banyak di antara tokoh-tokoh bangsa kita, yang telah mempersoalkan korupsi setajam atau seberani Kwik Kian Gie. Isi pidatonya pada tanggal 21 Januari di depan rapat CGI di Nusa Dua (Bali), sebagai Menteri Perencanaan Pembangunan Nasional/Kepala Bappenas, merupakan salah satu contohnya. Ia mengatakan di Bali bahwa Indonesia seharusnya tidak perlu mengemis pada komunitas internasional bila serius dalam pemberantasan korupsi, yang telah menggerogoti keuangan negara sampai 28,4 milyar dollar AS setiap tahun (Kompas, 22 Januari, 2003).

Dalam pertemuan tersebut, Kwik menyebutkan bahwa Indonesia juga kehilangan pendapatan sekitar 9 milyar dollar AS akibat pencurian dan penyelundupan kayu gelondongan, ikan dan pasir serta kehilangan 8 milyar dollar AS akibat kebocoran pajak.

"Kami tidak memperhatikan dana sebesar 28,4 milyar dollar AS tersebut, tapi hari ini kami mengemis pada anda semua untuk mendapatkan hampir 3 milyar dollar AS," sebut Kwik (idem, Kompas).

Ucapannya ini menarik dan sangat penting untuk diperhatikan, baik bagi mereka yang mendukung politik kepemimpinan Megawati-Hamzah maupun yang menentangnya. Karena, masalah korupsi adalah urusan besar bangsa. Urusan kita semua, yang masih peduli terhadap keadaan bangsa dan negara, yang sekarang sedang menghadapi beraneka-ragam persoalan berat dan gawat.

## Bukan Persoalan Baru

Sebenarnya, apa yang dikemukakan oleh Kwik Kian Gie ini bukanlah soal baru. Soal korupsi ini sudah puluhan tahun jadi masalah. Tetapi, bahwa ia dengan bahasa jang terang-terangan dan cara jang blak-blakan mengumbar kebobrokan bangsa di depan masyarakat internasional adalah suatu keberanian. Dan ini adalah tindakannya yang untuk kesekian kalinya. Tahun yang lalu, juga di depan masyarakat internasional yang serupa, ia telah menyatakan malunya bahwa Indonesia terpaksa mengemis minta pinjaman. Pada waktu itu ia sudah mengatakan bahwa kunci dari segala persoalan yang timbul di negeri kita adalah K O R U P S I .

Jadi, masalah korupsi adalah bukan soal baru bagi para peserta rapat CGI, yang terdiri dari 21 wakil negara dan 9 lembaga keuangan multilateral atau internasional itu. Banyak di antara mereka yang sudah tahu banyak tentang “isi perut” kita. Mereka kenal betul kebobrokan sistem pemerintahan kita dan kebejatan mental tokoh-tokoh kita. Sebab, mereka selama ini berhubungan langsung dengan para pejabat di berbagai bidang dalam rangka misi mereka. Di samping itu mereka juga mengadakan berbagai studi yang serius atau riset yang mendalam tentang persoalan-persoalan penting yang kita hadapi.

Orang bisa mempunyai kesan bahwa Kwik Kian Gie lebih banyak mengarahkan luapan kejengkelannya dan kekesalannya ke dalamnegeri. Ia berontak atau muak terhadap praktek-praktek kejahatan para pejabat dan tokoh-tokoh negeri kita yang setiap tahunnya menggerogoti kekayaan negara sampai sekitar 28, 4 milyar dollar US.

## Seharusnya : Tidak Perlu Utang!

Menurut Kwik Kian Gie, kalau tidak ada kebocoran-kebocoran dana pembangunan dan penggrogotan berbagai projek besar, maka kita tidak perlu mengemis-ngemis mencari utang lagi. Uang yang dicuri, dana yang diselewengkan, pajak yang digelapkan, tender yang digelembungkan, projek yang difiktifkan, laporan yang dipalsukan, anggaran yang di-“mark-up” dan cara-cara durjana lainnya, telah menyebabkan kerugian negara besar-besaran. Kejahatan ini dilakukan oleh sebagian kecil dari bangsa kita, yang menggunakan - dan menyalahgunakan – kekuasaan atau pengaruh mereka untuk memperkaya diri. Mereka ini tidak peduli lagi apakah utang negara kita sudah menggunung atau tidak, juga masa-bodoh saja apakah tindakan mereka itu merugikan kepentingan umum atau tidak.

Penggerogotan keuangan negara sampai sebesar 28 milyar dollar AS tiap tahun (!!!) adalah sungguh-sungguh kejahatan besar! Ini perampokan besar-besaran, yang dilakukan secara sembunyi-sembunyi atau beramai-ramai dalam berbagai cara dan bentuk, oleh para tokoh di berbagai tingkat. Oleh karena perampokan atau pencurian yang 28 milyar dollar (atau sekitar 280 trilyun Rupiah) itulah maka negara kita harus mencari utang. Walaupun “hanya” (harap perhatikan tanda kutip) 3 milyar dollar AS saja (sekitar 30 trilyun Rupiah). Artinya, oleh karena perbuatan sejumlah kecil “tokoh-tokoh” kita itulah seluruh rakyat kita harus menanggung beban berat utang itu. Inilah ketidak-adilan yang tidak bisa diterima oleh nalar jang sehat. Inilah kejahatan besar-besaran yang tidak boleh kita biarkan terus-menerus.

## Perlu Gerakan Besar-Besaran

Mengingat parahnya penyakit atau wabah korupsi yang sudah merusak akhlak banyak orang selama puluhan tahun, maka sudah tibalah waktunya bagi semua golongan bangsa untuk bersama-sama memikirkan dan kemudian mengadakan langkah-langkah untuk melahirkan gerakan nasional memberantas korupsi. Gerakan ini seyogyanya dijalankan oleh semua golongan, yang melakukan kegiatan atau aksi-aksi, baik bersama-sama maupun sendiri-sendiri. Harus disadari oleh semua golongan bahwa korupsi bukan saja mendatangkan kerugian material (keuangan atau dana) bagi rakyat, melainkan menimbulkan kerusakan mental atau kebobrokan akhlak banyak orang. Dan seperti yang bisa kita saksikan dewasa ini, pembusukan moral di kalangan elite ini sudah sangat parah.

Sekarang main jelas, bahwa rakyat kita tidak bisa menaruh harapan lagi - dan tidak perlu percaya lagi - kepada segala janji-janji atau segala peraturan, atau segala perundang-undangan, tentang pemberantasan korupsi. Selama ini sudah banyak komisi dan badan yang dibentuk untuk membendung korupsi, tetapi tidak membawa hasil yang berarti. Para koruptor tetap bisa terus mencuri kekayaan publik dengan berbagai cara. Ini terjadi baik di pusat pemerintahan di Jakarta, maupun di propinsi dan kabupaten bahkan kecamatan. Ini dilakukan oleh kalangan eksekutif, legislatif maupun judikatif.

Karena tidak bisa menaruh harapan lagi kepada badan-badan resmi, atau aparat-aparat negara, termasuk dewan perwakilan rakyat kita, maka rakyat sendirilah harus bertindak, untuk melancarkan perjuangan melawan korupsi. Kalau gerakan besar-besaran telah bisa dilakukan oleh berbagai golongan masyarakat, maka baru ada kemungkinan untuk memaksa aparat dan badan-badan pemerintah untuk bertindak tegas terhadap para koruptor. Tanpa desakan yang kuat dan aksi-aksi spektakuler dalam beraneka bentuk dan cara, yang dilancarkan oleh berbagai golongan, maka akan tidak ada tindakan terhadap para penjahat di kalangan elite ini.

## Selamatkan Generasi Yang Akan Datang

Gerakan nasional besar-besaran memberantas korupsi ini juga mempunyai tujuan strategis yang penting, yaitu : menyelamatkan generasi bangsa yang akan datang. Kita sudah melihat sendiri, dari kenyataan dan praktek sehari-hari, bahwa kita sudah tidak bisa punya harapan dari para tokoh generasi sekarang. Sebagian besar generasi yang dewasa ini sedang pegang peran aktif di bidang eksekutif, legislatif, judikatif, wiraswasta, dan masyarakat, sudah rusak akhlaknya oleh racun kebudayaan Orde Baru. Banyak di antara mereka dididik dengan pola berfikir Golkar, atau berkebudayaan KORPRI, atau bermental militer (TNI-AD) yang anti-demokrasi. Karenanya, kebanyakan di antara mereka bermental korup, tidak peduli kepada kepentingan rakyat, anti-reformasi, reaksioner, konservatif.

Tipislah sudah kemungkinan kita mengajak sebagian besar tokoh-tokoh atau “para terkemuka” negeri kita yang sekarang untuk ikut memberantas korupsi. Sebab, kebanyakan di antara mereka adalah justru bagian dari korupsi ini. Mereka ini bisa mempermainkan kekuasaan, merekayasa pengaruh, memelintir hukum dan undang-undang, menyalahgunakan peraturan, dengan tujuan untuk memperkaya diri dengan cara-cara durjana.

Gerakan besar-besaran memberantas korupsi ini perlu mengikutsertakan sebanyak mungkin golongan masyarakat, terutama di kalangan generasi muda. Umumnya, generasi muda ini belum kena racun busuk Orde Baru. Kita semua berkewajiban untuk menyelamatkan generasi bangsa yang akan datang, supaya jangan ketularan penyakit parah, yang telah merusak akhlak banyak orang ini. Mengikutsertakan sebanyak mungkin anak muda dalam gerakan nasional besar-besaran memberantas korupsi adalah investasi. Dan investasi ini adalah sangat vital bagi kelanjutan kehidupan bangsa. Generasi bangsa yang akan menggantikan kita janganlah sampai rusak dan bobrok seperti yang sekarang.

Segala jalan perlu ditempuh oleh semua golongan supaya anak muda kita mulai belajar bahwa korupsi adalah kejahatan besar terhadap kepentingan umum. Ini bisa dilakukan lewat sekolah, madrasah, pesantren, universitas, atau badan-badan pendidikan lainnya. Organisasi agama pun bisa memainkan peran aktif dan penting dalam masalah pendidikan moral ini. Seruan yang telah dikeluarkan oleh PB NU mengenai perlunya pemberantasan korupsi adalah contoh penting. Seruan ini perlu dikumandangkan terus-menerus sehingga bergema kemana-mana.

##Kembangkan Kebudayaan Anti-Korupsi

Dalam gerakan nasional besar-besaran anti-korupsi peran bidang kebudayaan sangatlah penting. Segala media dan sarana perlu digunakan, sehingga perjuangan anti-korupsi menjadi topik umum. Wartawan dianjurkan menulis artikel-artikel dan novelis mengarang cerita-cerita yang mengagungkan sikap jujur dan anti-korupsi. Sekadar contoh, tulisan “Aku punya papa yang tidak pernah korupsi” yang dibuat oleh,Ferona Yulia dan disiarkan Wanita Muslimah Yahoogroups, adalah salah satu di antara cerita yang rupanya menarik perhatian banyak kalangan.

Dalam menggelar kegiatan kebudayaan anti-korupsi perlu didorong artis-artis menciptakan lagu-lagu, pelawak-pelawak dan para dalang mengejek-ejek para koruptor.

Oleh karena korupsi sudah merajalela juga di daerah-daerah, maka gerakan ini perlu dikembangkan juga di berbagai propinsi, kabupaten, dan bila mungkin juga di tingkat kecamatan. Untuk melancarkan gerakan semacam ini berbagai ornop, LSM, gerakan, persatuan, paguyuban, serikat buruh, serikat tani, organisasi wanita, organjsasi pelajar dan mahasiswa, organisasi pemuda bisa mengadakan aksi-aksi bersama atau sendiri-sendiri.

Oleh karena wabah korupsi sudah merusak tubuh kebanyakan partai politik, kalangan agama, dan banyak organisasi, maka gerakan nasional besar-besaran ini seyogianya tidak bersifat partisan. Korupsi adalah musuh bersama seluruh golongan dan seluruh aliran. Korupsi sudah merusak akhlak banyak tokoh-tokoh PDI-P, Golkar, PPP, PBB, PKB, PAN, NU, Muhamadiyah, Ornop atau LSM, di samping kalangan pejabat di berbagai bidang dan tingkat.

Dengan tujuan strategis untuk menyelamatkan generasi yang akan datang itulah kampanye nasional anti-korupsi perlu digelar oleh berbagai golongan. Mengikutsertakan generasi muda sebanyak mungkin dalam gerakan ini adalah amat penting, sebagai sarana pendidikan politik dan moral bagi penerus bangsa kita. Kita harus persiapkan generasi yang akan datang untuk bisa menyelenggarakan “good and clean governance” . Generasi kita yang akan datang haruslah berbeda dari pada generasi yang sekarang. Generasi yang sekarang adalah generasi yang membusuk karena wabah korupsi.
