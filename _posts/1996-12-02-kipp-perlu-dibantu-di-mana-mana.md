---
layout: post
title: KIPP perlu dibantu di mana-mana
date: 1996-12-02
---

(Sumbangan dari : Samir Amin)

Di negeri kita Indonesia sedang terjadi macam-macam peristiwa. Buntut meninggalnya Ibu Tien Suharto masih menimbulkan macam-macam komentar dalam percakapan-percakapan. Peristiwa pembunuhan sejumlah mahasiswa dan masuknya panser-panser ke dalam kampus universitas di Ujung Pandang masih terus mencetuskan berbagai gejolak di berbagai kota dan daerah.
Di Mahkamah Agung ada persoalan yang dihebohkan. Mahasiswa-mahasiswa di berbagai kota mengadakan aksi-aksi yang meliputi macam-macam masalah. Kaburnya Eddy Tansil dari penjara Cipinang membangkitkan kemarahan banyak orang. Vonnis yang dijatuhkan oleh Pengadilan Negeri Jakarta terhadap Sri Bintang Pamungkas juga memenuhi halaman-halaman suratkabar.

Semua itu menarik perhatian kita, di-tengah-tengah kehidupan se-hari-hari yang tidak mudah bagi banyak orang. Karena, terlalu banyak hal yang harus dirombak di Indonesia, atau yang harus diperbaiki, dibongkar, diluruskan, dinormalkan, dibrantas, dan juga yang harus ditegakkan dan dibangun kembali. Di berbagai bidang : politik, sosial, ekonomi, kebudayaan, kebiasaan, dan moral.

Tetapi, di tengah-tengah segala macam masalah itu, yang semuanya penting dan mendesak, ada satu soal yang perlu mendapat perhatian secara khusus dari kita semua. Kita semua yang ada di di Indonesia maupun yang sedang tinngal di luarnegeri. Yaitu : pemilihan umum yang akan datang. Sebab, pemilihan umum yang akan diadakan dalam tahun 1997 nanti, akan dapat mempengaruhi, banyak atau sedikit, jalannya perkembangan situasi di Indonesia. Kalau pemilihan umum yang akan datang dapat dilaksanakan betul-betul secara bebas dan rahasia, dan dengan cara-cara yang jujur, maka diharapkan akan membawa udara yang segar di tanahair kita, yang selama ini terasa pengap dan menjempitkan nafas, karena polusi yang macam-macam.

Selama ini sudah dirasakan oleh banyak orang di Indonesia, bahwa beberapa kali pemilihan umum di masa-masa yang lalu (kecuali yang dalam tahun 1955) telah melahirkan DPR dan MPR yang tidak mencerminkan perwakilan rakyat yang benar-benar. Dua instansi tinggi ini didominasi, selama ber-puluh-puluh tahun, oleh GOLKAR dan « mesin militer » yang mempunyai jaring-jaringan yang luas dan dalam berbagai cara. Sudah ber-tahun-tahun pula banyak cendekiawan-cendekiawan, mahasiswa, organisasi-organisasi kemasyarakatan (LSM dan sebagainya) menyuarakan bahwa sistim politik di Indonesia perlu dirombak atau dibongkar kembali, diperbarui atau diganti. Sebab, banyak bukti dan pengalaman selama ini bahwa borok-borok yang terdapat di segala bidang di Indonesia disebabkan macetnya demokrasi, penyalahgunaan kekuasaan yang macam-macam bentuknya, dan pelanggaran hak-hak azasi manusia. Singkatnya : Pancasila dan demokrasi hanya jadi pajangan palsu atau omong-kosong, dan dijadikan alat untuk menutupi kebusukan-kebusukan yang sudah menjalar ke-mana-mana.

Ada orang-orang atau golongan-golongan di Indonesia yang menginginkan adanya perobahan atau perbaikan-perbaikan. Tetapi, ada juga berbagai golongan yang ingin mempertahankan status quo. Mereka ini, yang mau mempertahankan status quo, adalah golongan-golongan (hanya sebagian kecil dari rakyat seluruhnya) yang diuntungkan oleh sistim politik Orde Baru. Mereka berkepentingan untuk mencegah adanya perobahan-perobahan fundamental dalam sistim politik dan praktek-praktek kepemerintahan di Indonesia. Bahkan banyak juga yang ingin mempertahankan berlangsungnya situasi yang bobrok ini, demi kepentingannya sendiri, atau kepentingan golongannya.

Karena itu, banyak yang mengharapkan (ataukah hanya ilusi saja ?) bahwa kalau pemilihan umum yang akan datang ini dapat dijalankan dengan baik, maka dapatlah dilahirkan di DPR atau MPR « suara » atau « kekuatan politik » yang mencerminkan pelaksanaan demokrasi yang benar, jujur, dan adil. Dari segi ini pulalah dapat dilihat pentingnya peran Komite Independen Pemantau Pemilihan (KIPP).

Seperti kita ketahui, berdirinya KIPP telah menjadi pembicaraan di mana-mana. Sambutannya juga macam-macam. Ada yang positif dan ada yang negatif. Maklum-lah, pendapat yang ber-beda-beda ini adalah juga cermin dari berbagai kepentingan, dan ber-macam-macam pendirian. Di banyak propinsi, daerah dan kota-kota di pulau Jawa, Sumatra, Kalimantan, Sulawesi, Bali dan lain-lain, telah berdiri KIPP dalam berbagai tingkat dan skala. Ada yang besar dan mendapat dukungan dari berbagai organisasi, ada yang masih dalam masa persiapan atau baru melangkah. Ber-macam-macam rapat, pertemuan,, forum, atau pertemuan-pertemuan dalam bentuk yang beraneka-ragam telah diselenggarakan. Tidak jarang, pertemuan-pertemuan ini diselenggarakan dengan menghadapi kesulitan-kesulitan, mendapat ancaman atau tekanan melalui berbagai cara. Baik dari fihak « resmi » (alat-alat pemerintahan) maupun dari fihak-fihak « non-resmi » yang tidak ingin adanya perobahan sistim politik di Indonesia.

Dari apa yang dapat kita baca dari koran-koran, maka jelaslah bahwa KIPP ini akan mendapat jegalan-jegalan yang tidak sedikit untuk menjalankan tugasnya. Segala cara akan ditempuh oleh pendukung-pendukung pemerintahan Suharto (GOLKAR, sebagian dari ICMI, sebagian dari PPP dan banyak golongan lainnya) untuk mengadakan adu-domba, fitnah, operasi-operasi OPSUS gaya baru, intrik-intrik yang selama 30 tahun ini sudah pernah dijalankan.

Fihak penguasa sudah mulai operasinya dengan mengungkit-ungkit, dengan bukti-bukti dan dasar yang benar atau tidak, bahwa Sekjen KIPP, W. Kusumah, pernah ada sangkut-pautnya dengan organisasi pelajar IPPI, suatu organisasi pelajar yang sah dan besar dalam jaman Sukarno.

Padahal, seperti yang sudah disiarkan dalam berbagai media di Indonesia, tujuan KIPP adalah jelas, yang antara lain adalah : (1) agar Pemilu berlangsung secara Luber dan memenuhi standard Internasional tentang pemilihan umum yang demokratis. (2) Mendokumentasikan berbagai pelanggaran, pra hingga paska Pemilu yang selanjutnya dilaporkan ke masyarakat. (3) Pendidikan politik bagi rakyat dan (4) meningkatkan partisipasi rakyat dalam gerakan demokratisasi.

Tujuan yang baik ini sudah dapat dukungan dari berbagai tokoh, dari organisasi yang macam-macam (PMKRI, LBH, PRD, SMID, PPBI, LP3ES, dan banyak lagi lainnya), yang tersebar di banyak tempat dan daerah di Indonesia. Dari daftar nama pendukung KIPP yang tersebut di bawah ini, tergambar pulalah bahwa masalah ke-LUBER-an pemilihan umum yang akan datang ini menjadi pemikiran yang serius bagi banyak orang, yang antara lain adalah sebagai berikut (daftar tidak lengkap, baru sebagian) :

## Dewan pertimbangan

* Adnan Buyung Nasution, DR
* A Gaffar Rahman
* Aldentua Siringoringo, SH
* Ali Sadikin, H, Letjen TNI Mar. [purn]
* Amartiwi Saleh, SH
* Amir Husin Daulay
* Antonius Donie
* Apul Batubara
* Arbi Sanit, drs
* Arief Budiman, DR
* Aries Arief Mundayat, DR
* Bursah Zarnubi, SE
* Chandra Nainggolan
* Dahlan Ranuwihardjo
* Deddy S Triawan
* Ita Fathia Nadia Daino
* JC Princen, H
* Kartjono SW
* Loekman Soetrisno, DR
* M M Billah
* Marsilam
* Simandjuntak, DR
* MS Zulkarnen, Ir
* Muchtar Pakpahan, DR
* Nurcholish Madjid, DR
* Nursyahbani Katjasungkana, SH
* Permadi, SH
* Ridwan Saidi, H, drs
* Romo Hardoputranto
* Romo Pudjosumarta
* SAE Nababan, DR
* SK Trimurti
* Sri Bintang Pamungkas, DR
* Sukardjo Adijojo, SH
* Victor Matondang, SH
* Zumrotin KS, SH

## Dewan presidium

* Goenawan Mohammad [ ketua ]
* Mulyana W Kusumah, drs [ sekertaris jenderal ]
* Andi Arief [ anggota ]
* Bambang Beathors Suryadi [ anggota ]
* Budiman Sudjatmiko [ anggota ]
* Chatibul Umam Wiranu [ anggota ]
* FX Dodi Geger [ anggtota ]
* Muhammad Nadjib [ anggota ]
* Saut Sirait [ anggota ]
* Standar Kiaa [ anggota ]
* Tohap Simanungkalit [ anggota ]

## Alamat Sekretariat

* Jalan Kramat Kosambi II No. 39 C, Rawamangun, Jakarta Timur, Indonesia
* Telepon : 62-21 4711856
* Faksimili : 62-21 4897940
* Rekening KIPP lewat bank : Bank Internasional Indonesia, Cabang Diponegoro Jakarta
* Account number : 2-072-257190
* c/o : Rambun Tjajo


Melihat perkembangan situasi di Indonesia akhir-akhir ini, maka sudah dapat dipastikan bahwa KIPP akan mengalami banyak serangan, kesulitan, fitnahan, « rekayasa » busuk dan licik , yang dilancarkan oleh pendukung-pendukung Orde Baru, yang selama ini telah mencekik demokrasi. Oleh karena itu, segala jalan perlu ditempuh oleh kita semua, baik yang di Indonesia mau pun yang di luarnegeri, untuk membantu terlaksananya misi KIPP ini, demi tegaknya demokrasi di Indonesia.
