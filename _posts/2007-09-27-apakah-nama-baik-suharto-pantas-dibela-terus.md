---
layout: post
title: 'Apakah "nama baik" Suharto pantas dibela terus ?'
date: 2007-09-27
---

Pemerintah RI dan Bank Dunia bersepakat untuk kerjasama

Agaknya, berita penting Antara dari New York, yang dikirim tanggal 26 September 2007 ini, merupakan hal yang menarik perhatian banyak sekali orang di Indonesia, tetapi yang juga akan menimbulkan reaksi yang cukup hangat dari banyak kalangan. Berita tersebut menyebutkan, antara lain, sebagai berikut :

« Pemerintah Indonesia menyatakan keinginan untuk berpartisipasi dalam initiative StAR/Stolen Asset Recovery guna lebih memperkuat kemampuannya melaksanakan ketentuan Bab V Konvensi PBB mengenai pemberantasan korupsi (United Nations Convention Against Corruption/UNCAC) 2003 mengenai pengembalian aset, khususnya dalam hal melacak, membekukan dan mengembalikan aset yang berada di luar wilayah yurisdiksinya.

« Hal tersebut dikemukakan dalam pertemuan dwipihak antara Presiden Susilo Bambang Yudhoyono dan Presiden Bank Dunia, Robert B Zoellick, di sela-sela sidang umum ke-62 PBB, di New York.

« Dalam pernyataan bersama yang dikeluarkan kedua belah pihak, disebutkan kedua belah pihak menggarisbawahi StAR sebagai sebuah program unik dan inovatif yang memungkinkan negara berkembang dan negara maju mendapatkan manfaat dalam konteks implementasi UNCAC 2003.Disebutkan bahwa dalam beberapa tahun terakhir Indonesia telah mengambil langkah-langkah penting dan mendasar dalam upaya memberantas korupsi.

« Oleh karena itu, sebagai negara pihak dari Konvensi UNCAC 2003 dan tuan rumah penyelenggaraan pertemuan ke-2 negara-negara pihak dari UNCAC 2003 di Bali, 28 Januari-1 Febuari 2008, Indonesia menyatakan keinginan untuk berpartisipasi dalam initiative StAR.

Sebagai tindak lanjut, maka misi bersama Bank Dunia dan UNODC akan berkunjung ke Indonesia guna mengembangkan lebih lanjut program bantuan teknis spesifik di bawah inisiatif StAR. Kedua pemimpin juga mendesak negara-negara maju untuk mengambil langkah-langkah yang diperlukan guna memastikan bahwa pusat-pusat keuangan dunia tidak menjadi tempat penyimpanan dana hasil korupsi yang dilarikan dari negara berkembang.(baca teks berita selengkapnya dalam Kumpulan berita Masalah Suharto dan PBB-Bank Dunia)

Bagaimana akhirnya nasib Suharto di kemudian hari?
Dikeluarkannya pernyataan bersama antara presiden SBY dan presiden Bank Dunia mengenai kesediaan pemerintah Indonesia untuk berpartisipasi dalam program StAR merupakan perkembangan penting dalam usaha memberantas korupsi di Indonesia, termasuk masalah kemungkinan menindak korupsi Suharto.

Dengan diluncurkannya program StAR Initiative oleh PBB dan Bank Dunia, dan disebutkannya dalam dokumen-dokumen kedua badan internasional itu bahwa Suharto adalah pencuri terbesar di dunia, agaknya banyak orang mungkin bertanya-tanya : bagaimanakah akhirnya nasib mantan presiden dan yang juga pemimpin Orde Baru itu di kemudian hari ?

Barangkali, sekarang ini, masih tidak begitu mudah untuk mendapat jawaban yang segera atas pertanyaan yang demikian itu. Karena, kasus Suharto menyangkut berbagai persoalan yang rumit atau kompleks, dan yang juga dilatarbelakangi oleh faktor-faktor politik dan sejarah yang sarat dengan banyak masalah berat. Namun begitu, sudah adalah kiranya sejumlah aspek-aspek yang bisa dipakai sebagai ancer-ancer tentang arah perkembangan kasus Suharto ini, mengingat berbagai hal yang sudah terjadi sekarang ini dan yang mungkin akan terjadi di kemudian hari.

Dalam tulisan yang kali ini disajikan berbagai hal untuk sekadar direnungkan atau ditelaah bersama-sama, dengan mencoba mendekati persoalan Suharto ini dari berbagai sudut pandang dan berangkat dari berbagai titik tolak.

PBB dan Bank Dunia membuka perspektif baru
Meskipun masih terdapat berbagai hal yang belum jelas betul tentang inisiatip PBB dan Bank Dunia dengan programnya StAR (Prakarsa Pengembalian Uang Negara yang Dicuri) namun banyak orang mengharapkan, atau, bahkan, sudah melihat adanya kemungkinan bahwa StAR ini akan membuka perspektif baru dalam penanganan masalah korupsi Suharto. Seperti sama-sama kita ketahui, kebanyakan orang di Indonesia tadinya sudah putus harapan akan adanya kemungkinan diambilnya tindakan tegas dan tuntas terhadap korupsi Suharto. Karena, masih ada terlalu banyak orang-orang yang bersimpati kepada Suharto yang menyelinap di berbagai lembaga negara, antara lain di bidang eksekutif, legislatif, dan judikatif.

Tetapi, perkembangan terakhir menunjukkan tanda-tanda adanya perubahan-perubahan atau kemajuan di kalangan pemerintahan. Contohnya, menurut Jawapos (25/9/07) :”Kejaksaan Agung serius menindaklanjuti dokumen Bank Dunia berisi aset mantan Presiden Soeharto di luar negeri. Selain mengajukan request ke Bank Dunia, kejaksaan bakal minta dukungan kepada seluruh jaksa agung sedunia yang akan hadir di pertemuan The 2nd Annual Conference and General Meeting of The International Association of Anti-Corruption Authorities (IAACA) di Nusa Dua, Bali ( 28 Januari-1 Februari 2008)

Jaksa Agung Hendarman Supandji mengatakan, dalam pertemuan di Bali tersebut para jaksa agung akan membicarakan konvensi untuk merumuskan mekanisme penelusuran aset, agar tidak melanggar prinsip-prinsip kerahasiaan bank. "Kami juga membuat kesepakatan dengan lembaga internasional, untuk berkomitmen membantu pelacakan (aset Soeharto)," kata Hendarman (harap baca selengkapnya di “Kumpulan berita Masalah Suharto dan PBB-Bank Dunia)

Yang penting : ada kemauan politik

Dengan adanya program StAR Initiative dari PBB dan Bank Dunia sekarang ini tergantung kepada sikap pemerintah RI, apakah akan menunjukkan – dengan sungguh-sungguh – political will (kemauan politik) dalam menyelesaikan secara tuntas masalah korupsi Suharto. Sebab, meskipun presiden SBY sudah ketemu dengan presiden Bank Dunia di AS, dan sudah mengeluarkan pernyataan bersama tentang StAR Initiative, tetapi kalau pada dasarnya memang tidak ada kemauan politik untuk mengambil tindakan tegas terhadap korupsi Suharto, akan ada saja berbagai dalih atau macam-macam alasan yang bisa dikarang-karang untuk tidak menepati kesepakatan yang sudah diambil bersama.

Demikian juga, dengan masalah keberanian dan keteguhan Jaksa Agung. Sekali lagi, perlu diulangi di sini, bahwa ia bernjanji untuk minta bantuan para Jaksa Agung dari seluruh dunia yang akan bersidang di Bali untuk menindaklanjuti dokumen Bank Dunia yang berisi aset Suharto di luarnegeri. Selain itu Kejaksaan Agung juga mengajukan request (permintaan) kepada Bank Dunia untuk membantu melacak harta Suharto yang berasal dari korupsi. Kalau semua itu hanya janji kosong atau permintaan bantuan yang pura-pura saja, korupsi besar-besaran tidak akan bisa ditindak dan Suharto pun masih tetap bisa enak-enak lenggang-kangkung terus.

Dari perkembangan ini maka kita semua akan mengetahui, tidak lama lagi, atau lambat-laun, apakah pemerintahan RI (khususnya Kejaksaan Agung) akhirnya akan sungguh-sungguh dan berani mengambil tindakan tegas terhadap koruptor terbesar di dunia ini, atau tidak.

Martabat bangsa dan negara dipertaruhkan

Agaknya, kita semua perlu mendorong -- dan, bahkan, menuntut dengan keras! -- supaya presiden SBY beserta pembantu-pembantunya di berbagai bidang betul-betul menjaga martabat bangsa atau melindungi kehormatan negara RI. Hendaknya, janganlah kita mengecewakan harapan banyak orang, baik di Indonesia maupun di dunia, atas terlaksananya StAR Initiative, juga yang berkaitan dengan kasus Suharto. Apalagi, presiden SBY sudah mengeluarkan pernyataan bersama dengan presiden Bank Dunia. Ditambah lagi, para Jaksa Agung dari seluruh dunia, yang akan bersidang di Bali permulaan tahun depan, tentunya akan bicara tentang StAR Initiative. Jadi, kasus korupsi Suharto akan menjadi masalah yang berkaitan dengan erat sekali -- melebihi dari yang sudah-sudah -- dengan martabat bangsa dan citra penegakan hukum di Indonesia.

Sebab, dapatlah kiranya diduga oleh banyak orang bahwa perwakilan Bank Dunia dan PBB di Jakarta, dan juga kedutaan-kedutaan asing di Indonesia, selama ini sedikit banyaknya mengetahui apa sebab-sebabnya mengapa Suharto sampai sekarang masih belum disentuh oleh hukum dan pengadilan, meskipun sudah ada bukti-bukti atau tanda-tanda yang kuat bahwa ia (bersama keluarganya) sudah mencuri uang rakyat secara besar-besaran. Tidak atau belum bisa diadilinya Suharto berkaitan dengan harta haramnya yang bertumpuk-tumpuk adalah aib besar bangsa (kecuali yang berkaitan dengan 7 yayasannya yang sudah mulai disidangkan).

Apalagi, Indonesia akan menjadi tuan rumah pertemuan Jaksa Agung seluruh dunia di Bali, dan akan minta bantuan mereka untuk melacak harta haram Suharto di luarnegeri. Kalau ternyata kemudian bahwa sikap pemerintah RI atau Kejaksaan Agung hanya setengah-setengah atau tidak jujur, atau “memblé” saja mengenai kasus Suharto, maka citra hukum dan peradilan di Indonesia, yang sudah buruk selama ini, akan makin anjlok lebih dalam lagi.

Jadi, pertemuan dan pernyataan bersama presiden SBY dengan presiden Bank Dunia dan juga pertemuan para Jaksa Agung seluruh dunia di Bali menjadi pertaruhan besar bagi martabat bangsa dan kehormatan negara.

“Nama baik dan kehormatan” Suharto

Walaupun ada tanda-tanda yang menimbulkan optimisme bagi banyak orang tentang tindakan terhadap masalah korupsi Suharto, namun seyogianya kita semua punya perhitungan bahwa jalan yang harus ditempuh masih panjang dan mungkin juga akan makan waktu lama sekali. Itu disebabkan oleh selain adanya berbagai masalah-masalah yang berkaitan dengan pentrapan hukum dan hubungan antar negara dll dll, juga disebabkan oleh masih adanya banyak orang yang mau menjaga “nama baik dan kehormatan “ Suharto.

Mereka yang masih mau “menjaga nama baik dan kehormatan “ Suharto adalah pada umumnya, dan pada hakekatnya, orang-orang yang merasa “diuntungkan” untuk bersikap pro-Suharto dan pro-Orde Baru, karena berbagai sebab dan perhitungan. Karenanya, bisalah dimengerti bahwa mereka ini juga cenderung untuk tidak menyetujui -- atau bahkan memusuhi -- program PBB dan Bank Dunia, yang berkaitan dengan pengusutan korupsi Suharto. Mereka ini akan terus berusaha menentang atau menyabot – dengan berbagai cara dan bentuk -- kesediaan pemerintah RI untuk berpartisipasi melaksanakan program StAR Initiative.

Mereka ini ( yang banyak terdapat di kalangan pimpinan Golkar dan sebagian dari pimpinan TNI-AD) boleh dikatakan tidak mau tahu, juga tidak mau mengerti, bahwa Suharto adalah pencuri besar uang rakyat dan negara, yang sudah tidak perlu dan tidak pantas dihormati sama sekali. Sebab, walaupun Suharto pernah menjadi tokoh paling tinggi dan paling berkuasa di Golkar ( Ketua Dewan Pembina) dan pernah menjabat panglima tertiggi Angkatan Bersenjata Republik Indonesia sekaligus presiden RI selama 32 tahun, tetapi kenyataannya ia adalah maling terbesar di Indonesia, dan bahkan di skala dunia pula

Bagi mereka-mereka yang bersikap begitu itu, baiklah kiranya mengetahui bahwa arah perkembangan situasi nasional dan juga internasional tidaklah menunjukkan tanda-tanda yang menguntungkan Suharto. Jadi, usaha membela “nama baik dan kehormatan” Suharto akhirnya akan terbukti sia-sia belaka.

“Kehormatan” Suharto bukanlah kehormatan bangsa

Dengan dicantumkannya Suharto dalam daftar 10 koruptor besar di dunia (yang paling atas pula!) oleh badan-badan internasional yang penting (PBB, Bank Dunia, dan Transparency International atau yang lain-lain) maka sulit kiranya untuk bisa mengatakan bahwa Suharto masih punya “nama baik atau kehormatan”, seperti yang dinyatakan oleh pengadilan kasasi Mahkamah Agung yang dipimpin Mayjen TNI (Pur) German Hudiarto. Oleh karena itu, dimenangkannya gugatan Suharto terhadap TIME atas dasar tuduhan bahwa majalah itu sudah merugikan “nama baik dan kehormatan” Suharto sebagai mantan jenderal TNI dan presiden RI, adalah suatu hal yang bisa dianggap “lucu”.

Sekarang ini, makin jelas bahwa membela “kehormatan” Suharto sama sekali bukanlah berarti membela kehormatan bangsa, dan juga bukan pula menjaga nama baik TNI.. Bahkan sebaliknya, membela “kehormatan” Suharto berarti justru membikin aib bangsa, atau merendahkan martabat TNI. Sebab, yang dikatakan oleh pendukung-pendukung setia Orde Baru sebagai “kehormatan” Suharto adalah sebenarnya, atau pada hakekatnya, k e j a h a t a n, dan lebih-lebih lagi, kejahatan yang luar biasa besarnya di dunia.

Kalau kita renungkan dalam-dalam, maka kita akan sampai pada kesimpulan bahwa untuk menjaga martabat bangsa dan kehormatan TNI, kita perlu menghilangkan atau menghapus aib besar yang dibikin oleh Suharto (beserta keluarganya).. Bangsa kita atau TNI kita tidak akan dihormati oleh bangsa-bangsa lain, kalau Suharto masih bisa menongkrongi terus harta yang sudah dirampoknya secara besar-besaran.

Untuk kepentingan bersama

Dalam kaitan itu semuanya, perlulah kita sadari bersama, bahwa dihapuskannya aib besar bangsa yang berupa kasus korupsi Suharto itu adalah untuk kepentingan seluruh bangsa. Adalah fikiran yang sama sekali keliru kalau ada yang berpendapat bahwa yang senang dengan ditindaknya Suharto adalah terutama golongan kiri, atau hanya mantan anggota atau simpatisan PKI, atau pendukung Bung Karno saja. Memang, wajarlah kalau para korban rejim militer Ode Baru akan senang dengan diambilnya tindakan terhadap Suharto, mengingat apa yang mereka alami dimasa-masa yang lalu.

Tetapi baik juga sama-sama kita ingat bahwa kalangan atau golongan yang dirugikan kepentingannya oleh Suharto dengan Orde Barunya sangatlah luas dan banyak sekali, dan bukan hanya orang-orang dari golongan kiri atau pendukung Bung Karno saja. Juga orang-orang dari kalangan Islam banyak sekali yang telah menjadi korban kerakusan Suharto yang ia praktekkan lewat KKN, dan berbagai pelanggaran HAM. Kalau kita lihat dengan cermat, maka nyatalah bahwa orang-orang yang miskin, atau yang menganggur, atau yang hidup sengsara akibat berbagai politik Suharto sebagian terbesar adalah justru dari kalangan Islam.

Dosa-dosa besar Suharto akibat banyak kejahatannya di bidang korupsi dan pelanggaran HAM menunjukkan dengan jelas bahwa Suharto adalah orang yang tidak patut dihormati dan tidak pantas dibela sama sekali. Dan, kiranya, baiklah kita sama-sama renungkan yang berikut ini : pada hakekatnya, adalah juga merupakan kejahatan juga kalau ada orang membela hanya satu orang yang begitu besar dosa dan kejahatannya, tetapi tidak membela kepentingan ratusan juta rakyat kita.
