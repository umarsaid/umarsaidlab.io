---
layout: post
title: Korban rejim militer Suharto merindukan pemimpin yang seperti Bung Karno
date: 2010-09-21
---

Supaya para pembaca jangan salah tafsir atau salah tanggap ketika membaca judul tulisan seperti tersebut di atas baiklah kiranya dijelaskan terlebih dulu bahwa yang dimaksud dalam hal ini bukanlah hanya para korban yang terdiri  dari anggota atau simpatisan PKI beserta puluhan jutaan anggota ormas-ormas yang berafiliasi dengannya, seperti antara lain : Pemuda Rakyat, Gerwani, BTI, SOBSI, HSI, CGMI, dan puluhan serikat buruh lainnya (umpamanya SBKA, SARBUPRI, SBPU, SBPP, PERBUM, SEBDA, SB Unilever dll dll).

Dan bukan pula hanya para pendukung politik revolusioner Bung Karno, yang terdiri juga dari orang-orang kiri non-komunis. Melainkan seluruh orang  (sekali lagi : seluruh orang) dari berbagai kalangan dan golongan, yang pernah menjadi korban  -- dalam macam-macam bentuk dan berbagai kadar  -- politik rejim militer Suharto yang berkuasa selama puluhan tahun itu. Jadi, jumlahnya besar sekali di seluruh Indonesia, dan juga di luar negeri.

Patutlah kiranya sama-sama kita ingat bahwa rejim miiliter Suharto telah menjalankan politik anti-demokrasi yang menyeluruh di segala  bidang kehidupan negara dan bangsa, dan dalam jangka waktu yang lama sekali pula. Karenanya, yang menjadi korban politik anti-demokrasi rejim militer Suharto ini sebenarnya adalah luas sekali. Jadi, sekali lagi,  bukan hanya golongan kiri saja.

Namun, perlu dicatat bahwa di antara para korban politik anti-demokrasi dari diktatur militer Suharto selama 32 tahun  itu yang paling berat, dan paling lama serta paling besar adalah golongan anggota dan simpatisan PKI dan golongan pendukung aktif politik Bung Karno.

Perbedaan besar antara Indonesia di bawah Bung Karno

dengan di bawah Suharto

Para korban diktatur militer yang jumlahnya besar ini sekarang sedang menunggu perkembangan situasi negara dan bangsa yang makin kelihatan semrawut, seperti kehilangan arah, penuh dengan masalah-masalah besar yang tidak terselesaikan (untuk disebutkan sejumlah kecil saja di antaranya : korupsi yang tetap merajalela, Bank Century, kekusutan di kalangan pimpinan POLRI, masalah gedung baru DPR, pertentangan antar agama, utang negara yang makin membubung tinggi, kelakuan para pejabat dan anggota DPR untuk « studi banding » ke luar negeri dll dan seterusnya).

Jelaslah bahwa banyaknya masalah dewasa ini tidak bisa dipisahkan dari kerusakan-kerusakan, pembusukan, yang dibikin oleh rejim militer Orde Baru. Bahkan,  bisalah dikatakan bahwa banyak sekali di antara banyak hal-hal yang buruk dewasa ini adalah produk dari segala politik diktatur militer yang selama 32 tahun telah merusak sendi-sendi negara, membusukkan jiwa bangsa, menbobrokkan moral sebagian rakyat.

Para pengamat sejarah, dan para politisi dari angkatan 45, atau golongan «angkatan lansia « umumnya sekarang  bisa melihat perbedaan yang jauh sekali antara Indonesia di bawah pimpinan Bung Karno dan Indonesia di bawah Suharto dan pemerintahan-pemerintahan yang menyusulnya. Banyak sekali kebusukan moral, kebejatan mental, dan kebobrokan iman yang sekarang kelihatan merajalela begitu luas dan begitu parah di « kalangan atas » bangsa tidak pernah nampak  waktu Indonesia ada di bawah pimpinan Bung Karno.

Kalau melihat begitu besar kerusakan moral, atau kebejatan mental, atau kesesatan iman seperti yang sama-sama kita saksikan dewasa ini dan kita bandingkan dengan situasi bangsa dan negara di bawah pimpinan Bung Karno, maka nyatalah dengan jelas sekali betapa besar  dosa Suharto dengan Orde Barunya dalam membikin Indonesia yang akibatnya seperti sekarang ini. Padahal, jiwa bangsa Indonesia di bawah pimpinan Bung Karno adalah jiwa yang penuh dengan moral revolusioner, moral perjuangan, moral mengabdi kepada kepentingan rakyat, moral persatuan bangsa, dan moral gotong royong.

Arti hilangnya Bung Karno dari kepemimpinan bangsa

Sejarah sudah membuktikan, dengan disaksikan juga oleh golongan  progresif seluruh dunia,  bahwa jiwa bangsa Indonesia di bawah pimpinan Bung Karno terkenal sebagai jiwa yang didominasi oleh elan (semangat) revolusioner selain untuk mengabdi kepada kepentingan rakyat Indonesia juga untuk kepentingan rakyat Asia-Afrika-Amerika Latin serta rakyat-rakyat tertindas lainnya. Untuk semuanya itulah, jiwa rakyat Indonesia waktu itu penuh dengan patriotisme kerakyatan, dan nasionalisme revolusioner anti-kapitalisme, anti-kolonialisme dan anti-imperialisme.

Jiwa bangsa Indonesia yang pernah begitu terhormat di bawah pimpinan Bung Karno inilah yang sudah dirusak parah oleh Suharto beserta para jenderalnya , sehingga  membusuk secara luas, dan membikin bangsa menjadi sakit parah, seperti yang kita saksikan dimana-mana dewasa ini.

Keadaan negara dan bangsa yang serba kacau dan menyulitkan kehidupan rakyat kecil dewasa ini, menunjukkan bahwa dibutuhkan akan adanya pimpinan nasional yang berwibawa, yang memiliki integritas moral yang tinggi, yang jelas mempunyai jiwa pengabdian kepada rakyat, yang dicintai dan dihormati oleh bangsa, seperti halnya Bung Karno.

Karena itu, hilangnya Bung Karno dari pucuk pimpinan bangsa dan  negara akibat pengkhianatan Suharto dan para jenderalnya (beserta pendukung-pendukungnya di berbagai kalangan masyarakat)  mengakibatkan Indonesia dalam keadaan kehilangan pimpinan yang sudah terbukti memiliki kualitas yang tinggi dalam berbagai bidang.

Kita semua tahu, bahwa Bung Karno, meskipun menjadi Presiden sejak proklamasi sampai digulingkannya oleh Suharto tidak melakukan korupsi untuk menumpuk harta bagi keluarga dan anak-anaknya. Ia wafat dalam keadaan keluarganya tidak memiliki harta yang melimpah ruah seperti halnya keluarga Suharto. Bahkan, pernah ada cerita bahwa Ibu Fatmawati terpaksa minta bantuan keuangan kepada  pengusaha Hasyim Ning sesudah Bung Karno tidak menjabat lagi sebagai presideni.

Banyak orang menyesal telah menyokong Suharto

Karenanya, dapatlah dimengerti bahwa banyak orang yang tadinya menyokong Suharto (dan konco-konconya) dalam mendongkel Bung Karno, umumnya sekarang menjadi kecewa atau ikut menyesalinya. Mereka tadinya mengira atau mengharapkan bahwa Suharto akan bisa membawa negara dan bangsa lebih baik dari pada Bung Karno. Mereka tadinya menyokong Suharto karena berpendapat bahwa ajaran-ajaran revolusioner dan politik kiri Bung Karno (pro-sosialisme, anti-kolonialisme dan anti-imperialisme) harus dibuang dan digantikan dengan yang lain.

Namun, ternyata apa yang terjadi adalah yang serba kebalikannya.  Suharto ternyata jauh tidak lebih baik dari pada Bung Karno, jauh sekali, sangat jauh !!! Bahkan, dalam banyak hal, sangat bertentangan !!!!! (tanda seru lima kali). Sejarah hidup Bung Karno yang sejak muda sudah berjuang menentang kolonialisme Belanda bertentangan dengan sejarah hidup Suharto yang dimulai dengan menjadi serdadu kolonial Belanda.

Bung Karno adalah penggagas atau penggali Pancasila, sedangkan Suharto adalah pemalsu atau perusak Pancasila, seperti yang sudah dibuktikannya selama 32 tahun memimpin rejim militernya. Bung Karno sejak mudanya sudah terbukti sebagai pemersatu bangsa, sedangkan Suharto telah mencabik-cabik persatuan bangsa secara parah sekali, sejak terjadinya peristiwa 30 September 1965.

Tetapi, yang lebih penting dan lebih serius lagi adalah perbedaan yang besar sekali, atau  bertentangan sama sekali antara  cara Bung Karno memimpin negara dan bangsa  dan cara Suharto mengangkangi dan mengendalikan diktatur militernya. Kediktatoran Suharto yang dibarengi dengan kerusakan moral yang parah, telah membikin kerusakan-kerusakan yang hebat sekali hampir di segala bidang.

Kerusakan paling besar : pembusukan jiwa bangsa

Kerusakan yang paling besar adalah yang terdapat di bidang jiwa bangsa, termasuk jiwa generasi muda yang sekarang banyak menduduki tempat-tempat penting di bidang eksekutif, legislatif, dan judikatif. Di antara pejabat-pejabat penting ini (atau tokoh-tokoh lainnya dalam masyarakat) sebagian terbesarnya  adalah mereka yang waktu mudanya diberi pendidikan oleh Orde Baru.

Oleh karenanya, kita dapat mengatakan bahwa pada dasarnya, atau pada pokoknya,  birokrasi  (dan elite bangsa pada umumnya) di Indonesia sekarang ini adalah produk atau hasil kultur diktatur militer Orde Baru dengan segala sistemnya yang serba korup, anti-rakyat, anti-Bung Karno atau anti-kiri (termasuk anti-PKI). Hal yang demikian ini terdapat di Jakarta, dan juga di seluruh Indonesia.

Mereka-mereka itulah yang juga menjadi tokoh-tokoh berbagai partai politik, menjadi anggota DPR di pusat dan didaerah-daerah, atau menjadi pemuka-pemuka masyarakat di berbagai tingkat, termasuk di kalangan swasta atau pengusaha.

Itu semua merupakan  faktor-faktor mengapa meskipun rejim militer Suharto sudah ambruk, namun sisa-sisa Orde Baru masih kelihatan kuat di sana-sini. Ini nampak dari masih besarnya peran yang dimainkan Golkar dalam percaturan politik dewasa ini, dan juga adanya partai-partai politik lainnya yang masih menentang ajaran-ajaran revolusioner Bung Karno.

Namun, situasi kongkrit dalam negeri (dan juga situasi internasional) menunjukkan perkembangan yang tidak menguntungkan lagi bagi kekuatan-kekuatan yang mau mempertahankan politik anti-keadilan, anti-kesejahteraan, anti-perubahan bagi kepentingan rakyat, terutama rakyat miskin.

Kemiskinan yang masih makin terus menganga lebar, pengangguran yang makin membengkak, penderitaan sebagian terbesar rakyat yang makin parah, membikin sadarnya banyak  kalangan bahwa negara dan bangsa kita sudah memerlukan sekali perubahan besar dan fundamental.
Perubahan besar dan fundamental ini tidak mungkin dilakukan dengan  birokrasi atau kalangan  atas  bangsa yang masih terkontaminasi oleh penyakit-penyakit Orde Baru.

Tidak seorang pun bisa menyamai keagungan Bung Karno

Dengan aparat pemerintahan yang terdiri dari pejabat-pejabat yang reaksioner, anti-rakyat, dan korup dan bejat moralnya seperti kebanyakannya dewasa ini,  situasi negara dan bangsa akan tetap dalam keadaan terus membusuk di segala bidang.

Seperti kita ketahui bersama, keadaan yang serba menyedihkan bagi sebagian terbesar rakyat kita sekarang ini adalah akibat hilangnya Bung Karno dari tampuk kepemimpinan bangsa oleh pengkhianatan Suharto dan para jenderalnya dengan bantuan imperialisme AS.

Dan sejak hilangnya Bung Karno dari kepemimpinan bangsa, maka tidak seorang pun  -- sekali lagi : tidak seorang pun ! -- dari tokoh-tokoh Indonesia lainnya yang bisa menggantikan kebesaran sosoknya yang agung.   Tidak seorang pun dari pemimpin-pemimpin Indonesia  (dari partai politik yang mana pun, atau dari golongan apa pun) yang bisa menandingi apalagi melebihi berbagai keunggulan Bung Karno, yang setara dengan keunggulan banyak tokoh-tokoh besar seperti Mao Tse-tung,  Chou En-lai, Ho Chi-minh, Nehru, Gamal Abdul Nasser, Kwame Nkrumah, Tito atau pemimpin-pemimpin besar lainnya di dunia.

Oleh karena itu wajarlah kalau banyak kalangan dan golongan di negeri kita sekarang ini mendambakan munculnya seorang pemimpin yang memiliki sikap perjuangan atau sikap politik yang bisa seperti Bung Karno. Sebab, sudah terbukti bahwa keadaan yang serba dekaden sekarang, atau yang serba membusuk dewasa ini, adalah karena berbagai pemerintahan  telah menjalankan politik yang serba bertentangan dengan ajaran-ajaran revolusioner Bung Karno, yaitu Trisakti (berdaulat secara politik, berdikari dalam bidang ekonomi, dan berkepribadian dalam kebudayaan).

Namun, sayangnya, kiranya adalah sulit sekali  -- bahkan tidak mungkin!  -- bagi bangsa Indonesia untuk  mempunyai lagi pemimpin yang seagung Bung Karno. Sebab, Bung Karno adalah sosok yang terlalu besar, untuk bisa disamai oleh siapa pun di Indonesia, sekarang ini, dan mungkin juga untuk masa-masa yang akan datang.  Bangsa Indonesia, bersama seluruh anak cucunya di kemudian hari patut  merasa bangga dan bertermakasih pula sudah pernah mempunyai seorang puteranya yang bernama Sukarno.

Dalam seribu tahun  akan hanya ada seorang Soekarno

Seperti yang pernah ditegaskan oleh Subadio Sastrosutomo (mantan pimpinan terkemuka PSI)  dalam bukunya  bahwa  “Sukarno adalah Indonesia, dan Indonesia adalah Sukarno”, untuk menggambarkan arti kebesaran Bung Karno bagi bangsa Indonesia. Penegasan atau pengakuan Subadio ini sangat penting, kalau diingat bahwa dalam jangka lama tadinya PSI digolongkan sebagai penentang politik Bung Karno yang cukup « mbandel ».

Dalam kaitan ini, baik juga kita ingat kembali apa yang ditulis oleh mantan Dubes RI di Argentina Jusuf Ronodipuro dalam  New York Times tanggal 4 Juni 2002,  yang menyebutkan “In a thousand years, there will only be one Sukarno » (dalam seribu tahun  akan hanya ada seorang Soekarno)

Mengingat itu semua, dan juga mengingat situasi negara dan bangsa dewasa ini, wajarlah kalau banyak di kalangan rakyat Indonesia merindukan adanya pemimpin yang bisa seperti bung Karno. Dan wajar juga atau bisa dimengerti pula, kalau di antara yang merindukan pemimpin seperti Bung Karno itu adalah para korban Orde Baru, yang terdiri dari banyak golongan dalam masyarakat. Namun, kiranya lebih-lebih dapat dimengerti lagi bahwa para korban Orde Baru dari golongan kiri-lah yang paling merindukan tampilnya seorang pemimpin yang seperti Bung Karno.

Kalaupun tidak mungkin tampilnya seorang yang seperti  Bung Karno, maka akan sudah sangat besar artinya bagi rakyat kalau ada orang yang bisa melaksanakan isi dan jiwa ajaran-ajaran revolusioner Bung Karno dengan sungguh-sungguh. Dan orang seperti ini, pada suatu saat pasti akhirnya akan lahir juga  dari rahim perjuangan revolusioner rakyat Indonesia , walaupun mungkin akan makan waktu dan pengorbanan tenaga yang tidak sedikit.

Sebab, situasi negara dan bangsa tidak akan mungkin bisa diperbaiki oleh siapa pun dan bagaimana pun serta dengan cara apa pun, kalau menentang isi atau jiwa ajaran-ajaran revolusioner Bung Karno !!!
