---
layout: post
title: Suharto membangun atau merusak citra Islam ?
date: 2007-06-12
---

Hari ulang tahun mantan presiden Suharto yang ke-86, yang jatuh pada tanggal 8 Juni 2007, telah dirayakan di rumahnya di jalan Cendana, Jakarta, dengan cara-cara yang tidak seramai atau semeriah tahun-tahun sebelumnya. Walaupun ada mantan-mantan pejabat tinggi yang berdatangan, tetapi jumlahnya tidak sebanyak yang sudah-sudah. Tetapi karangan-karangan bunga masih banyak yang diterima.

Dalam rangka peringatan hari ulang tahun Suharto ke-86 ini, yang patut mendapat perhatian dari kita semua adalah diterbitkannya buku yang berjudul “HM Suharto membangun citra Islam”. Menurut berita Antara (8/7/07) dalam peluncuran buku tersebut, selain AM Fatwa, juga tampak hadir pejabat tinggi di masa kepemimpinan Soeharto, yaitu mantan menteri agama Tarmizi Taher.. Buku yang ditulis oleh Miftah H Yusufpati ini secara resmi diluncurkan sehari sebelum peringatan ulang tahun Suharto. Menurut Miftah : « Banyak perjuangan yang sudah dilakukan Soeharto terhadap Islam. Pak Harto terbukti telah menegakkan prinsip-prinsip ajaran Islam dan memperjuangkan toleransi beragama »

Ketika membaca judul yang berbunyi « HM Suharto membangun citra Islam » dan kalimat bahwa « Pak Harto terbukti telah menegakkan prinsip-prinsip ajaran Islam » maka mungkin saja di antara kita ada yang tercengang, atau mungkin juga ada yang tertawa terkekeh-kekeh, atau, ada pula yang jengkel dan marah-marah. Sebab, mungkin saja, ada orang yang menganggapnya sebagai lelucon, atau sebagai bualan alias omong-kosong, atau bahkan sebagai hinaan yang amat besar kepada Islam.

Dalam kaitan ini, tulisan ini mengajak para pembaca untuk merenungkan secara dalam-dalam apakah sebutan « HM Suharto membangun citra Islam » itu memang mengandung kebenaran, dan apakah sebutan itu akan mengangkat Islam, ataukah sebaliknya, malahan memperburuk atau merusak citra Islam. Sebab, terbitnya buku dengan judul yang demikian itu untuk memperingati hari ulangtahun Suharto mencerminkan bahwa di kalangan sebagian golongan Islam memang terdapat anggapan bahwa Suharto adalah tokoh pembangun citra Islam dan “penegak prinsip-prinsip ajaran Islam”. Hal yang demikian ini adalah sangat serius sekali. Dan menyesatkan sekali !

Oleh karena itu, alangkah baiknya, kalau masalah ini dibicarakan dan ditelaah dengan teliti oleh para tokoh agama Islam di Indonesia, dan dijadikan bahan studi atau riset di universitas-universitas dan lembaga-lembaga ilmiah Islam. Singkatnya, masalah ini perlu sekali menjadi kajian secara luas oleh ummat Islam di Indonesia.

Sebab, masalah apakah Suharto telah « membangun citra Islam » atau malahan merusak citra Islam, adalah masalah penting yang bisa dipakai untuk menilai berbagai politik rejim militer Orde Baru, dan juga untuk menelaah sikap berbagai golongan Islam terhadap Suharto dan Orde Baru. Sikap berbagai golongan Islam terhadap Suharto dan Orde Baru adalah satu hal yang mempunyai pengaruh penting dalam persoalan politik dan sosial di negara kita, yang buntutnya masih sama-sama kita saksikan sampai sekarang ini.

Sebagai bahan-bahan untuk renungan bersama, maka di bawah berikut ini disajikan sejumlah pandangan mengenai masalah tersebut, yang dicoba dilihat dari berbagai sudut pandang :

Apakah untuk melaksanakan ajaran-ajaran Islam ?

Suharto memang pernah memberikan bantuan kepada sejumlah pesantren, memberikan sumbangan untuk membangun mesjid-mesjid, dan memberikan dana kepada sejumlah kyai-kyai dan para pemuka agama Islam. Suharto juga sering memberikan sedekah kepada anak-anak yatim piatu, atau memberi beasiswa kepada pelajar-pelajar.

Tetapi, perlulah sama-sama kita renungkan dalam-dalam, apakah itu semua telah dilakukan Suharto oleh karena memang ingin melaksanakan ajaran-ajaran Islam dengan sungguh-sungguh dan secara tulus, ataukah karena sebab-sebab dan pertimbangan lainnya. Selama pemerintahan rejim militer Orde Baru, memang Suharto berusaha merangkul, menina-bobokkan, merekayasa, menggunakan, mensalahgunakan, bahkan menipu atau menjerumuskan sebagian golongan Islam

Sejauh mana atau sebesar apa, dan juga bagaimana saja cara-caranya Suharto telah “merangkul” dan “menggunakan” sebagian golongan Islam ini, barangkali berbagai kalangan Islam sendiri dapat menganalisanya atau menelitinya berdasarkan pengalaman-pengalaman pahit mereka masing-masing. Yang sudah jelas yalah adanya kebutuhan atau keharusan bagi Suharto (artinya rejim militer Orde Baru) untuk “merangkul” sebanyak mungkin golongan Islam, demi kelanggengan diktatur militernya, dan untuk melawan lawan-lawan politiknya.

Merangkul golongan Islam untuk bersekutu dengan AS
Seperti kita ketahui, stabilitas politik dan sosial telah dapat diciptakan rejim militer Orde Baru, dengan menggunakan tangan besi atau berbagai praktek otoriter, di samping merangkul, membujuk, menyiasati sebagian terbesar golongan Islam. Ada yang mengatakan bahwa Orde Baru telah berhasil « membeli » berbagai golongan Islam, dan menjadikan mereka sebagai pendukung berbagai politik rejim militer Suharto, yang dalam jangka lama adalah pro-Barat dan mengabdi kepada kepentingan politik AS.

Dengan « merangkul » sebagian golongan Islam, Suharto dkk telah berhasil menjadikannya sebagai sekutu dan sekaligus “tunggangan” untuk menghancur-leburkan kekuatan politik Bung Karno beserta pendukung-pendukungnya yang terdiri dari golongan kiri (antara lain PKI), yang melawan kekuatan-kekuatan nekolim (neo-kolonialisme dan imperialisme). Untuk lebih jelas lagi : pada masa-masa yang panjang selama Orde Baru, sebagian golongan Islam telah berhasil dibawa atau digiring oleh Suharto untuk memihak nekolim (baca : AS) dalam menghancurkan kekuatan anti-imperialis yang digalang Bung Karno beserta pendukung-pendukungnya.

Kalau dilihat dengan kaca-mata sekarang, dengan apa yang terjadi di Timur Tengah dan peran AS dalam berbagai masalah internasional dewasa ini (antara lain : masalah Israel-Palestina, masalah perang Irak, ancaman terhadap Iran) maka kelihatanlah dengan jelas bahwa Suharto dkk pada akhirnya -- atau pada dasarnya -- telah membawa golongan Islam di Indonesia pada jalan yang salah. Dengan kalimat lain, bisalah kiranya dikatakan bahwa Suharto bukanlah “pembangun citra Islam” yang baik.

Golongan Islam yang paling banyak dirugikan

Suharto telah melakukan penindasan besar-besaran , dan dalam jangka waktu yang lama pula, terhadap sebagian terbesar rakyat Indonesia, yang agamanya Islam. Memang, penindasan ini juga dilakukan terhadap semua golongan dalam masyarakat Indonesia. Tetapi yang diberangus suaranya, atau yang dirugikan kepentingannya, terutama adalah dari kalangan Islam. Ketika terjadi pelanggaran HAM secara besar-besaran (ingat: peristiwa pembunuhan tahun 65-66, peristiwa Lampung, peristiwa Tanjung Priok, peristiwa di Madura, peristiwa Trisakti, peristiwa Semanggi) bukanlah hanya orang-orang kiri yang menjadi korban, melainkan sebagian terbesar juga orang-orang yang beragama Islam.

Dalam kaitan ini perlu diketahui oleh banyak orang (terutama oleh para pendukung Suharto) bahwa banyak sekali (untuk tidak mengatakan sebagian terbesar) di antara orang-orang yang ditahan selama puluhan tahun di penjara-penjara dan dibuang di Pulau Buru adalah orang-orang Muslim, yang rajin sembahyang, bahkan mungkin melebihi apa yang dikerjakan Suharto beserta anak-anaknya. Pertanggunganjawab Suharto atas kedholiman terhadap ratusan ribu orang-orang Muslim yang ditahan atau jutaan yang dibunuhi secara ganas dan sewenang-wenang adalah besar sekali. Karena itu amat tidak patutlah kiranya untuk menempelkan embel-embel merek “pembangun citra Islam” atau “penegak ajaran-ajaran Islam” kepada namanya, Haji Mohamad Suharto.

Ketika Suharto (beserta anak-anaknya) menjadi maling-maling terbesar dalam sejarah Republik Indonesia, maka sebenarnya (atau pada hakekatnya) harta yang ditumpukkannya secara haram adalah milik rakyat, yang sebagian terbesar beragama Islam pula. Karena jumlah harta haram yang sudah dirampoknya dari rakyat dan negara adalah amat luar biasa besarnya (baca majalah Times 24 Mei 1999 dan tulisan George Aditjondro), maka dosanya terhadap ummat Islam di Indonesia juga amat luar biasa besarnya!

Kalau menurut ajaran Islam tangan seorang pencuri bisa dipotong, maka hukuman apakah yang sepantasnya dijatuhkan kepada Suharto, yang sudah mencuri harta milik sebegitu banyak rakyat Indonesia ? Apakah segala perbuatannya yang begitu serakah dan begitu haram itu bisa dikategorikan sebagai “menegakkan prinsip-prinsip ajaran-ajaran Islam”?

Dosanya lebih besar dari pada “amal”-nya

Suharto memang sudah memberikan dana kepada sejumlah pesantren, kepada berbagai organisasi (atau partai-partai) Islam, atau badan-badan sosial-ekonomi-kebudayaan Islam. Tetapi, mengingat bahwa uang yang sudah dicuri Suharto adalah begitu besarnya, maka dosanya adalah barangkali jutaan kali jauh lebih besar dari pada “amal”nya.

Dan, apalagi, kalau kita ingat bahwa Suharto -- bagaimana pun juga! -- harus bertanggungjawab atas dibunuhnya sekitar 3 juta orang tidak bersalah yang dituduh atau dicap Komunis (padahal ternyata kemudian bahwa sebagian terbesar di antara mereka adalah orang-orang Muslim juga !!!), maka sulit sekali untuk menganggap bahwa Suharto adalah orang yang “membangun citra Islam” dan “menegakkan prinsip-prinsip ajaran Islam”.

Haji Mohammad Suharto, yang mempunyai tanggungjawab besar sekali terhadap terjadinya banyak pelanggaran HAM yang serius, yang menyebabkan ratusan ribu eks-tapol (yang sebagian besar terdiri orang-orang Muslim juga) sampai sekarang terlunta-lunta hidupnya, yang membikin macam-macam penderitaan berlarut-larut terhadap puluhan juta keluarga korban peristiwa 65 (juga sampai sekarang), sama sekali tidaklah pantas disebut “Pembangun citra Islam”.

Sebab, kalau Haji Mohamad Suharto tetap terus disanjung-sanjung oleh sebagian kalangan Islam sebagai “pembangun citra Islam” atau “penegak prinsip-prinsip ajaran Islam” maka orang dapat mengartikan bahwa Islam adalah agama yang kejam dan tidak berperikemanusiaan. Atau, bahwa korupsi atau pengumpulan uang haram adalah citra Islam, atau bahwa Islam membolehkan segala macam praktek-praktek nista yang sudah banyak dilakukan oleh Suharto beserta keluarganya selama ini..

Suharto adalah perusak citra Islam
Dan lagi, karena Suharto sudah terkenal di Indonesia (dan juga di kalangan internasional) sebagai koruptor kaliber raksasa, yang jarang tandingannya di dunia, maka penamaan “Suharto pembangun citra Islam” adalah penamaan yang sungguh-sungguh merugikan sekali citra Islam. Karena, dengan begitu bisa saja diartikan bahwa citra koruptor besar adalah citra Islam. Jadi, kiranya, julukan yang paling tepat baginya justru adalah “Suharto perusak citra Islam”.

Di samping itu semuanya, kita bisa bersama-sama menilai apakah cara hidup dan kegiatan sehari-hari Suharto beserta keluarganya (antara lain : Tutut, Bambang, Sigit dan Tommy) di kompleks Cendana patut dinamakan “membangun citra Islam” dan “menegakkan prinsip-prinsip ajaran Islam ». Ketika sebagian terbesar rakyat Indonesia (yang kebanyakan terdiri dari orang-orang yang beragama Islam !!!) menderita sekali karena kemiskinan dan berbagai macam kesulitan atau kesusahan, Suharto dan keluarganya hidup mewah sekali dan foya-foya dengan uang najis yang diperolehnya dengan cara-cara haram.

Sekarang makin banyak bukti-bukti bahwa Suharto adalah sampah bangsa, karena kejahatannya yang banyak dan besar-besar di bidang korupsi, kolusi dan nepotisme (KKN) dan bidang HAM. Juga makin jelaslah bahwa Suharto adalah pengkhianat besar terhadap Bung Karno dan terhadap Republik Indonesia (ingat, antara lain Supersemar, dan pelecehan Pancasila).

Jadi, kalau ada orang-orang dari golongan Islam yang masih tetap bersikukuh menjunjung tinggi Suharto dengan mengatakan bahwa ia adalah “pembangun citra Islam” walaupun mereka sudah mengetahui segala dosa dan aibnya (akibat berbagai kejahatannya di bidang politik, ekonomi, sosial, moral dan agama), maka bisalah dikatakan bahwa orang-orang yang demikian adalah orang-orang yang sesat imannya. Jelasnya, orang-orang semacam itu pada hakekatnya juga ikut merusak citra Islam !!!
