---
layout: post
title: Suara keras Gus Dur
date: 2007-12-31
---

Ketika negeri dan rakyat kita sekarang sedang menghadapi berbagai masalah politik, ekonomi, sosial, moral dan agama yang parah, maka suara keras dan berani yang dikeluarkan oleh Gus Dur seperti yang berikut ini patut mendapat perhatian yang besar dari kita semua. Suara keras dan berani ini tercermin dalam berita yang disiarkan Tempo Alternatif tanggal 30 Desember 2007, yang berbuyi sebagai berikut :

“Ketua Dewan Syuro Partai Kebangkitan Bangsa Abdurrahman Wahid menyatakan Indonesia kehilangan orientasi pembangunan nasional. Akibatnya, rakyat tidak percaya pada pemerintah yang berkuasa saat ini.

"Orientasi pembangunan kita nggak jelas. Kita harus mampu membuat orientasi pembangunan nasional secara tepat," katanya dalam orasi catatan akhir tahun di Hotel Santika, Minggu (30/12).

Orasi akhir tahun Gus Dur tersebut dihadiri oleh beberapa tokoh nasional dan politik. Di antaranya Franz Magnis-Suseno, H.S. Dillon, Agum Gumelar, Soetrisno Bachir, dan Mohammad Sobari.

Selama ini, pembangunan ditujukan untuk kalangan atas. Akibatnya, jumlah rakyat miskin bertambah dan pengangguran tetap tinggi. "Pemerintahan SBY didikte oleh super power. Sama seperti pemerintahan orde baru yaitu pembangunan untuk kalangan atas saja," katanya.

Pemerintahan Yudhoyono-Kalla mengukur hasil pembangunan berdasarkan pertumbuhan atau growth. Sehingga, pembangunan selama ini dinilai sukses. Angka kemiskinan pun tinggal 16 persen. Sedangkan, angka pengangguran 49 persen. Seharusnya, dia menambahkan, pembangunan diukur berdasarkan pemerataan. "Yang kaya, tambah kaya. Yang melarat, tambah melarat," ujarnya.

Hilangnya orientasi pembangunan, katanya, tak lepas dari pengaruh Bank Dunia, Dana Moneter Internasional, dan Organisasi Perdagangan Dunia. Selama ini, dia melanjutkan, tiga organisasi dunia itu memaksa Indonesia berutang. Sehingga, nilai utang luar negeri saat ini mencapai US$ 600 miliar. Bahkan, ujarnya, ada pihak yang berpendapat nilai utang luar negeri Indonesia mencapai US$ 1,3 triliun. "Pemerintah lupa bahwa yang harus membayar utang adalah anak cucu kita," ujarnya.

Utang luar negeri itu, ujarnya, dibiayai dengan penjualan komoditas ke luar negeri. Padahal, komoditas Indonesia dibeli dengan harga murah. Ketidakseimbangan itu, dia menambahkan, akibat dari globalisasi perdagangan. "Nilai-nilai dasar yang kita anut pun berubah," katanya. Dampak susulan dari perubahan itu adalah lahirnya golongan fundamental atau radikal yang ingin mempertahankan nilai-nilai Islam.

Gus Dur punmenyoroti soal kekerasan yang masih digunakan untuk menyelesaikan masalah. Tindak kekerasan itu diterapkan dengan mengatasnamakan agama. "Ini harus dibongkar habis-habisan," ujarnya.

Menurut dia, konstitusi menjamin kebebasan berpikir dan berpendapat warga negara Indonesia. Aturan itu seharusnya menjadi landasan kehidupan berbangsa. Majelis Ulama Indonesia pun berkontribusi atas tindak kekerasan yang mengatasnamakan agama. Selama ini, ujarnya, MUI mengeluarkan fatwa yang dijadikan landasan bagi sebagian besar umat Islam di Indonesia. "MUI jangan sembarangan berpendapat tentang Ahmadiyah sebagai aliran sesat. Saya minta MUI tidak menggunakan kata sesat," ujarnya.

Persoalan Ahmadiyah, katanya, sebaiknya ditangani oleh PAKEM (penganut aliran kepercayaan terhadap Tuhan Yang Maha Esa). Alasannya, dalam PAKEM terdapat unsur kepolisian dan kejaksaan agung. Selain Ahmadiyah, orang-orang yang tergabung dalam gerakan shalawatan Wahidiyah di Tasikmalaya Jawa Barat pun dinyatakan sesat oleh pengurus MUI setempat. "Orang sudah melupakan Republik Indonesia bukan negara Islam tapi nasional," katanya.

Dia meminta MUI dibubarkan karena mengeluarkan fatwa sembarangan. Apalagi, ujarnya, MUI bukan satu-satunya organisasi massa Islam yang ada di Indonesia. "Bubarin ajalah MUI kalau begini. MUI bukan satu-satunya ormas Islam karena itu jangan gegabah mengeluarkan pendapat yang bisa membuat kesalahpahaman," ujarnya. (kutipan dari Tempo Interaktif selesai).

Pukulan keras bagi pemerintah SBY

Dari berita tersebut di atas dapat kiranya diangkat berbagai hal penting, karena justru diungkapkan oleh seorang tokoh politik nasional dan sekaligus juga agamawan Islam yang besar. Karena itu, ketika ia mengatakan bahwa “Indonesia sudah kehilangan orientasi dalam pembangunan nasional, dan karenanya rakyat tidak percaya pada pemerintah yang berkuasa saat ini” maka bisa diartikan bahwa ucapannya ini merupakan pukulan yang berat sekali terhadap pemerintah SBY-JK.

Pukulan keras yang diucapkan Gus Dur terasa lebih keras lagi ketika ia juga mengatakan : “Selama ini, pembangunan ditujukan untuk kalangan atas. Akibatnya, jumlah rakyat miskin bertambah dan pengangguran tetap tinggi. Pemerintahan SBY didikte oleh super power. Sama seperti pemerintahan Orde Baru yaitu pembangunan untuk kalangan atas saja," katanya.

Sebab, dengan mengatakan yang demikian itu Gus Dur menunjukkan degan jelas-jelas bahwa pembangunan yang digembar-gemborkan selama ini hanya menguntungkan kalangan atas. Dan pembangunan yang menguntungkan kalangan atas itu sudah berlangsung sejak pemerintahan Orde Baru, seperti yang kita saksikan dewasa ini. Banyaknya korupsi dan penyalahgunaan kekuasaan, yang dilakukan kalangan atas, merupakan bagian dari pembangunan yang salah orientasi, yang membikin rakyat miskin bertambah miskin dan jumlah pengangguran sangat tinggi.

Pernyataan Gus Dur yang lain yang sangat penting sebagai tokoh politik nasional dan tokoh terkemuka ummat Islam ialah ketika ia mengatakan bahwa pemerintahan SBY didikte oleh super power dan bahwa hilangnya orientasi pembangunan tak lepas dari pengaruh Bank Dunia, Dana Moneter Internasional, dan Organisasi Perdagangan Dunia. Selama ini tiga organisasi dunia itu memaksa Indonesia berutang, sehingga ada yang mengatakan bahwa utang luar negeri Indonesia mencapai US$ 1,3 triliun. “Pemerintah lupa bahwa yang harus membayar utang adalah anak cucu kita”, ujarnya

Adalah juga sangat penting (dan menarik sekali ! ) yang dikatakan Gus Dur bahwa pemerintahan SBY didikte oleh superpower dan bahwa hilangnya orientasi pembangunan tak lepas dari pengaruh Bank Dunia, Dana Moneter Internasional dan Organisasi Perdagangan Dunia, Ini menunjukkan bahwa pandangannya mengenai hal-hal ini adalah sejalan dengan pandangan berbagai tokoh negeri kita, dan seiring dengan kegiatan-kegiatan atau aksi-aksi bermacam-macam gerakan yang menentang neo-liberalisme dan globalisasi, baik yang di Indonesia maupun yang ada di banyak negeri di dunia. Patutlah diingat bersama bahwa kesadaran umum terhadap akibat-akibat buruk dari banyaknya operasi modal besar asing di Indonesia akhir-akhir ini makin membesar, terutama di kalangan generasi muda.

Bubarkan saja Majlis Ulama Indonesia

Di samping hal-hal penting itu semua, ucapannya mengenai berbagai masalah yang berkaitan dengan ummat atau agama Islam di Indonesia mempunyai arti yang amat besar bagi situasi negeri kita dewasa ini. Gus Dur menyoroti soal kekerasan yang masih digunakan (oleh kalangan Islam) untuk menyelesaikan masalah. Tindak kekerasan itu diterapkan dengan mengatasnamakan agama. "Ini harus dibongkar habis-habisan," ujarnya.

Kecaman yang tajam sekali telah dilontarkan oleh Gus Dur terhadap Majlis Ulama Indonesia (MUI) yang telah mengeluarkan fatwa yang dijadikan landasan bagi sebagian ummat Islam Indonesia, antara lain bahwa Ahmadiyah sebagai aliran sesat. Dia meminta MUI dibubarkan karena mengeluarkan fatwa sembarangan. Apalagi, ujarnya, MUI bukan satu-satunya organisasi massa Islam yang ada di Indonesia. "Bubarin ajalah MUI kalau begini. MUI bukan satu-satunya ormas Islam, karena itu jangan gegabah mengeluarkan pendapat yang bisa membuat kesalahpahaman," ujarnya.

Sebagai tokoh besar golongan Islam, ucapan Gus Dur yang seperti itu mempunyai bobot dan arti penting tersendiri. Ketika banyak tokoh-tokoh Islam lainnya hanya diam (atau takut-takut) saja terhadap aksi-aksi kekerasan yang dilakukan oleh sebagian kalangan Islam, maka apa yang dikatakan oleh Gus Dur mengenai hal ini merupakan keberanian yang menyejukkan hati bagi sebagian besar kalangan masyarakat.

Sebagai tokoh nasional yang terkemuka, pandangan Gus Dur mengenai negara, konstitusi, kebebasan berfikir dan kehidupan berbangsa juga amat penting untuk dicermati oleh kita semua, dan terutama oleh golongan Islam. Menurut dia, konstitusi menjamin kebebasan berpikir dan berpendapat warga negara Indonesia. Aturan itu seharusnya menjadi landasan kehidupan berbangsa. . "Orang sudah melupakan Republik Indonesia bukan negara Islam tapi nasional," katanya.

Gus Dur dihormati kalangan internasional

Dari apa yang diucapkan oleh Gus Dur seperti tersebut di atas nyatalah bahwa pandangannya mengenai berbagai masalah penting negara kita mencerminkan ketidakpuasan atau kekecewaan, dan menghendaki perubahan besar demi kepentingan rakyat banyak. Dan berlainan dengan para pejabat atau “tokoh-tokoh” kita yang kebanyakan masih berilusi tentang “kebaikan” sistem yang dianut oleh neo-liberalisme, maka ia menyatakan bahwa justru karena ulah Bank Dunia, Dana Moneter Internasional, dan Organisasi Perdagangan Dunia yang membkin rakyat kita yang miskin tambah miskin dan yang kaya makin kaya.

Berbagai pandangan Gus Dur yang positif tersebut di atas perlu sekali diketahui dan disebar-luaskan sebanyak mungkin di kalangan rakyat, dan terutama di kalangan Islam. Memang, pandangan yang hampir serupa atau searah dengan pandangan Gus Dur sudah juga muncul di sana-sini, terutama di kalangan muda Islam, umpamanya di kalangan Ikatan Pelajar NU (IPNU), Kaum Muda NU (KMNU), Jaringan Islam Liberal (JIL), Syarikat Indonesia, Santri Kiri, PMII dll dll. Dengan kadar berbeda-beda, dan pendekatan yang tidak sama, pada umumnya kelompok-kelompok atau kalangan tersebut di atas menyuarakan hal-hal yang berbeda (bahkan bertentangan sama sekali) dengan kalangan Islam lainnya yang dekat dengan FPI, Majlis Mujahidin, Komando Jihad, Jamaah Islamiah dan lain-lain kelompok atau organisasi yang sehaluan dan searah.

Berbagai pandangan Gus Dur mengenai persatuan nasional dan persaudaraan Islam digolongkan oleh banyak orang sebagai pandangan yang mengutamakan kebebasan dan toleransi antara berbagai golongan, termasuk golongan minoritas. Itu sebabnya, ketokohan Gus Dir juga diakui dan dihormati di dalam negeri dan di kalangan internasional.

Kita perlu banyak tokoh seperti Gus Dur

Banyak peristiwa-peristiwa dalam masyarakat yang disebabkan tindakan atau kegiatan sebagian kalangan Islam (semacam FPI, Komando Jihad dll), yang dibantu oleh kekuatan-kekuatan gelap dari sisa-sisa Orde Baru, telah menimbulkan citra yang jelek bahwa kalangan-kalangan itu umumnya bersikap tidak toleran terhadap adanya perbedaan keyakinan, mempunyai pandangan picik dan menyukai kekerasan.

Menghadapi kekisruhan atau kekacauan yang ditimbulkan oleh anasir-anasir yang tidak toleran seperti tersebut di atas itu semunya, terasalah pentingnya bagi negeri kita mempunyai banyak tokoh seperti Gus Dur. Kita butuhkan tokoh-tokoh yang mempunyai gagasan-gagasan besar untuk memajukan bangsa, dan bukannya orang-orang yang menggiring bangsa kita ke arah kemunduran.

Dan kalau kita lihat bahwa bangsa-bangsa lain di berbagai bagian dunia sudah mengalami perubahan-perubahan besar demi kemajuan rakyatnya (contohnya : Tiongkok, Vietnam, India, Kuba, Venezuela, Bolivia, Argentina, bahkan juga akhir-akhir ini negara-negara di Eropa Timur) maka keterpurukan negara kita Indonesia kelihatan makin sangat menyedihkan.

Bukan hanya karena korupsi yang merajalela di seluruh negeri saja, tetapi juga karena kemiskinan dan pengangguran yang menimpa sebagian besar sekali rakyat kita. Pembusukan moral terjadi dimana-mana, termasuk di kalangan agama. Penderitaan rakyat yang sudah sangat berat itu ditambah lagi dengan adanya bencana alam, gempa bumi, dan banjir.

Dengan latar belakang itu semuanya maka nyatalah bahwa suara Gus Dur yang dilontarkan dalam orasi akhir tahunnya itu mempunyai arti dan bobot yang perlu mendapat perhatian dari kita semuanya, termasuk (bahkan, terutama ! ) dari kalangan Islam.
