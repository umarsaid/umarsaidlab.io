---
layout: post
title: Mantan tapol jadi presiden perempuan di Cili
date: 2006-01-17
---

Kemenangan yang diperoleh tanggal 15 Januari 2006 oleh Michelle Bachelet, seorang perempuan untuk menduduki jabatan presiden terpilih Cile, kiranya menarik perhatian banyak orang di Indonesia. Tidak hanya di Indonesia saja, bahkan pers di seluruh dunia juga menjadikan peristiwa ini sebagai topik yang penting. Karena, berbagai hal menarik bisa diungkap dengan terpilihnya Michelle Bachelet sebagai presiden.

Bukan saja bahwa ia merupakan tokoh perempuan yang pertama kali dalam sejarah Cile yang terpilih secara langsung melalui pemilihan umum. Tetapi, juga bahwa ia adalah anak seorang jenderal Angkatan Udara (Alberto Bachelet) pendukung presiden Salvador Allende, yang ditangkap sebagai tapol oleh diktator Pinochet, dan disiksa sampai meninggal.

Dapat diperkirakan bahwa di Indonesia, bagi mereka yang menentang diktator rejim militer Suharto beserta Orde Barunya, berita ini disambut dengan gembira. Sebab, sudah lama sejak jenderal Pinochet melakukan kudeta (dalam tahun 1973) terhadap presiden Allende dan membunuhnya, diktator Pinochet ini disejajarkan dengan diktator Suharto. Seperti diketahui dari sejarah, kudeta di Cile untuk menggulingkan presiden Allende, yang sebagian pemikirannya condong sebagai Marxist (seperti Bung Karno) juga disokong CIA, seperti halnya sokongan CIA kepada klik Suharto dalam menggulingkan Presiden Sukarno.


Jenderal Pinochet juga membunuhi dan memenjarakan banyak sekali orang-orang komunis Cile dan orang kiri lainnya yang mendukung Presiden Allende, seperti halnya Suharto membunuhi dan memenjarakan banyak orang komunis dan orang kiri lainnya yang mendukung Presiden Sukarno. Karena itu, sangat menarik untuk diperhatikan bahwa slogan-slogan yang terdapat di jalan-jalan atau gedung-gedung di Santiago (ibukota Cile) waktu kudeta adalah “Jakarta,Jakarta”.


ARTI POLITIK KEMENANGAN MICHELLE BACHELET

Kemenangan Michelle Bachelet (sebagai calon Partai Sosialis dalam koalisi tengah-kanan, dengan hasil 53,22%) melawan Sebastian Pinera (pengusaha kaya, calon golongan kanan, dengan hasil 46,77%) bisa disoroti dari berbagai segi.

Terpilihnya perempuan (usia 54 tahun) yang sejak mudanya sudah aktif menentang jenderal Pinochet, menandakan bahwa pengaruh imperialisme AS yang pernah mendukung secara besar-besaran rejim militer Pinochet di Cile sudah jauh sekali makin melemah. Rejim militer Pinochet berkuasa di Cile cukup lama, yaitu 17 tahun sejak 1973 sampai 1990, melalui kudeta yang menyebabkan dibunuhnya sekitar 3000 orang dan ditahannya 27 000 orang pendukung Presiden Allende dan simpatisan Partai Komunis Cile (Bandingkan dengan Suharto yang mengangkangi kekuasaan selama 32 tahun, membunuh 3 juta orang kiri dan menahan ratusan ribu orang tidak bersalah).

Berlainan dengan situasi dalam tahun 1973, ketika CIA secara terang-terangan menggulingkan Presiden Allende, maka sejak jatuhnya diktator Pinochet dalam tahun 1990 sebagai pimpinan rejim militer, imperialisme AS tidak bisa lagi berbuat semau-maunya di Cile. Sejak 1990 Cile berada di bawah pemerintahan koalisi partai-partai yang beraliran kristen-demokrat dan sosialis. Presiden Cile yang terakhir (yang akan digantikan Michelle Bachelet), adalah seorang sosialis juga, yang bernama Ricardo Lagos.

Kemenangan Michelle Bachelet di Cile merupakan pertanda tambahan lagi bahwa Amerika Lain sedang mengalami transisi politik yang tidak menguntungkan kepetingan imperialisme AS. Dalam waktu singkat sebelum terpilihnya Michelle Bachelet, juga terpilih Evo Morales, seorang pemimpin gerakan petani sebagai presiden Bolivia. Yang menarik dari Evo Morales bahwa ia merupakan orang Indian yang pertama kali terpilih sebagai kepala negara di Amerika Latin, dan yang juga terkenal sebagai tokoh yang anti-Amerika.

Dapat diperkirakan bahwa sebagai akibat pengaruh revolusi Kuba dengan Fidel Castronya, dan sikap politik anti-Amerika dari Presiden Venezuela Hugo Chavez, dan situasi politik di Brasilia dengan presidennya Luiz Inasio Lula Da Silva(ditambah dengan terpilihnya Evo Morales di Bolivia) , terpilihnya Michelle Bachelet akan bisa mendorong terjadinya perubahan-perubahan di negeri-negeri Amerika Latin lainnya dalam perjuangan menentang neo-liberalisme atau globalisasi ekonomi, yang dimotori oleh kepentingan imperialisme AS. Kalau ini terjadi, maka akan merupakan perkembangan penting sekali.


“FENOMENA” MENARIK DARI MICHELLE BACHELET

Terpilihnya Michelle Bachelet sebagai presiden perempuan merupakan “fenomena” yang menarik sekali. Sebab, negeri Cile selama ini (sejak berabad-abad) terkenal sebagai negeri yang sebagian terbesar penduduknya beragama katolik dan bertradisi kolot. Pada umumnya, kedudukan perempuan tidak begitu dihargai. Contohnya, baru dua tahun yang lalu dibolehkan adanya perceraian di antara suami-istri. Dan hanya sejak permulaan tahun 2005 ada undang-undang yang melarang adanya gangguan terhadap perempuan dalam pekerjaan.

Banyak orang memandang Michelle Bachelet sebagai orang sosialis yang lebih kiri daripada presiden Rirardo Lagos yang juga sosialis.Ia sudah menunjukkan ke-kiri-annya sejak muda ketika ia masih menjadi mahasiswa; Pada tahun 1970 ia masuk fakultas kedokteran di Universitas Cile. Pada masa pemerntahan Presiden Allende, bapak Michelle Bachelet (Alberto Bachelet, yang jenderal Angkatan Udara) diangkat untuk mengepalai badan yang mengurusi bahan makanan.

Setelah terjadi kudeta jenderal Pïnochet, dalam tahun 1973 Alberto Bachelet ditangkap dan dipenjarakan. Ia meninggal dalam penjara pada tahun 1974 akibat siksaan dan perlakuan tidak manusiawi. Istri dan anak perempuannya (Michelle) juga akhirnya ditahan dan disiksa dalam rumah tahanan yang namanya Villa Grimaldi yang terkenal sebagai tempat penyiksaan. Dalam tahun 1975 istri jenderal Alberto Bachelet dan anak perempuannya mengungsi ke Australia di mana terdapat saudara Alberto almarhum. Kemudian Michellle pergi ke Leipzig untuk belajar bahasa Jerman (Jerman Timur, waktu itu) dan seterusnya ke Berlin (Timur) untuk melanjutkan studi kedokterannya di Universitas Humboldt.

Dalam tahun 1979 ia kembali ke Cile, dan menyelesaikan diplomanya sebagai ahli bedah. Dari tahun 1983 sampai 1986 ia menjadi spesialis dokter anak-anak dan kesehatan publik. Dari 1986 sampai 1990 ia aktif dalam organisasi yang membantu keluarga orang-orang yang disiksa dan orang hilang.di Cile.

Sesudah jatuhnya Pinochet, ia bekerja dalam Kementerian Kesehatan, dan kemudian menjadi Menteri Kesehatan dalam pemerintahan Presiden Ricardo Lagos. Michelle Bachelet yang melihat bahwa dalam kalangan sosialis di negerinya masalah pertahanan dan militer merupakan hal yang lemah sekali, maka ia memutuskan untuk melakukan studi tentang soal-soal strategi militer. Studi ini dilakukannya di Santiago (Cile) dan kemudian di Washington. Dalam tahun 2002 Michelle diangkat oleh presiden Lagos sebagai Menteri Pertahanan, suatu kedudukan yang pertama kali dipegang oleh perempuan di benua Amerika Latin.

Michelle dikenal sebagai seorang yang cerdas, suka bekerja keras, sosial sekali, dan karenanya mempunyai karisma yang tinggi. Oleh karena itu di negeri yang pada umumnya masih konservatif terpilihnya sebagai presiden perempuan merupakan kejadian yang bersejarah. Ia sendiri pernah berseloroh :” Saya adalah orang yang mempunyai dosa yang banyak. Saya adalah perempuan, saya juga sosialis, saya juga perempuan yang bercerai, dan saya orang yang agnostik (tidak beragama atau tidak mengenal Tuhan ” (menurut suratkabar Le Monde 14 Januari 2006).

Michele Bachelet yang lahir tahun 1951 di Santiago (Cile) sudah bercerai dari dua suami, dan mempunyai tiga orang anak. Ia merupakan presiden perempuan yang ketiga di benua Amerika Latin. Yang pertama adalah Violeta Chamorro, presiden Republik Nicaragua dari 1990 sampai 1996. Yang kedua ialah Mireva Moscoso, Presiden Panama antara 1999 sampai 2004.


PERJALANAN HIDUP POLITIK MICHELLE BACHELET

Michlle Bachelet menjadi anggota Partai Sosialis dalam tahun-tahun 70-an; Ia dipilih sebagai anggota CC dalam tahun 1995, dan kemudian anggota politbiro CC antara 1998 sampai 2000. Setelah diangkat oleh presiden Lagos sebagai Menteri Kesehatan dan kemudian Menteri Pertahanan dan makin popular dalam tahun 2004 ia dipilih sebagai calon presiden, dengan mendapat dukungan kuat dari presiden Lagos sendiri.

Platform pemilu Michelle Bachelet digalang bersama dalam koalisi tengah kiri yang diberi nama “Concertation” (artinya : persetujuan bersama atau perembukan bersama). Koalisi inilah yang mengalahkan gabungan partai-partai kanan dan ultra-kanan (pendukung Pinochet). Pemilu yang diikuti kira-kira 8 juta pemilih ini merupakan langkah lebih jauh bagi negeri Cile dalam menegakkan demokrasi, sesudah junta militer di bawah Pinochet berkuasa lama sekali.

Dengan terpilihnya Michelle Bachelet sebagai presiden (akan dilantik 11 Maret yad), banyak orang meramalkan bahwa tidak akan terjadi perubahan yang radikal sekali di Cile. Politik ekonomi sosial yang berpedoman pada garis liberal selama pemerintahan di bawah presiden Lagos, akan diteruskan, walaupun mungkin ada perbaikan di sana-sini. Selama ini pertumbuhan ekonomi Cile termasuk yang tinggi untuk negeri-negeri Amerika Latin, yaitu sekitar 6 % setahunnya. Tetapi, kesenjangan antara si kaya dan si miskin masih tetap besar.

Program Michel Bachelet justru bertititik berat pada mengurangi kemiskinan, perbaikan kesehatan penduduk, kebebasan yang lebih besar bagi kaum perempuan dan persamaan hak antara laki-laki dan perempuan. Apa yang sudah dicapai dengan sukses selama pemerintahan presiden Lagos akan diteruskan oleh Michelle Bachelet.


BEDANYA 11 MARET CILE DAN SUPERSEMAR

Dapat dimengerti bahwa terpilihnya Michelle Bachelet, yang pernah menjadi tapol dan disiksa oleh polisi rahasia junta militer Pinochet, menjadi presiden perempuan di Cile menarik perhatian banyak orang di Indonesia. Karena, apa yang dilakukan oleh Pinochet tahun 1973 adalah ulangan dari apa yang dilakukan Suharto dkk dalam tahun 1965, atau 8 tahun sesudah peristiwa G30S. Dan apa yang dilakukan junta militer Pinochet sama jahatnya atau sama kejamnya dengan apa yang dilakukan Suharto dkk, walaupun skalanya jauh lebih kecil dari apa yang terjadi di Indonesia. Dalam jangka waktu yang lama sekali, junta militer Cile di bawah Pinochet “menaruh respek” terhadap Orde Baru. Maklum, dalam hal-hal tertentu Suharto adalah “guru” Pinochet, di bawah kontrol CIA.

Ada persamaan nasib antara diktator anti-komunis yang dua-duanya menjadi sekutu imperialis AS waktu itu. Dalam masa tua mereka, Pinochet dan Suharto, sama-sama menjadi hujatan banyak orang, akibat kesalahan-kesalahan berat atau kejahatan mereka, terutama pelanggaran di bidang hak-hak manusia dan demokrasi.

Bisa diartikan sebagai hal yang ironis, bahwa pelantikan Michel Bachelet akan dilakukan tanggal 11 Maret. Pada hari itulah perempuan yang pernah menjadi tapol Pinochet akan diresmikan sebagai presiden Cile, Kalau 11 Maret di Indonesia merupakan hari pengkhianatan para pimpinan TNI-AD terhadap Bung Karno (ingat : Supersemar yang pernah diagung-agungkan oleh pendukung Orde Baru), maka 11 Maret di Cile akan merupakan hari bersejarah untuk merayakan kemenangan seorang perempuan yang menentang diktator Pinochet.

Kejadian-kejadian terakhir di berbagai negeri Amerika Latin dan di Timur Tengah (juga di berbagai negeri di benua lainnya) menunjukkan bahwa perjuangan melawan imperialisme AS berkembang terus, dengan berbagai bentuk dan cara. Inilah yang perlu disadari oleh mereka yang sampai sekarang masih berfikiran dengan pola rejim militer Orde Baru.
