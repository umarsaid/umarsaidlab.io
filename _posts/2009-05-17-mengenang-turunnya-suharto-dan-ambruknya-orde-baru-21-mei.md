---
layout: post
title: Mengenang turunnya Suharto dan ambruknya Orde Baru (21 Mei)
date: 2009-05-17
---

Mengenang 21 Mei 1998


Turunnya Suharto dan

ambruknya Orde Baru



Tulisan ini mengajak para pembaca untuk mengenang kembali tanggal 21 Mei 1998, ketika diktator yang super-korup dan sudah memerintah Indonesia dengan tangan besi -- secara bengis dan kejam selama 32 tahun -- terpaksa turun dari jabatannya, berkat perjuangan besar-besaran berskala nasional oleh gerakan generasi muda (terutama mahasiswa dan pelajar) dengan mendapat dukungan luas dari masyarakat.

Mengingat berbagai hal, maka bisalah kiranya dikatakan dengan tegas bahwa memperingati turunnya Suharto sebagai presiden tanggal 21 Mei 1998 adalah maha penting bagi bangsa Indonesia, baik bagi generasi yang sekarang maupun generasi yang akan datang. Begitu pentingnya, sehingga peristiwa yang sangat bersejarah ini perlu diperingati atau dikenang oleh masyarakat -- dengan berbagai cara dan bentuk -- setiap tahun di kemudian hari, dalam rangka “nation building and character building”, yang sudah lama dilupakan sejak Bung Karno digulingkan secara khianat.oleh Suharto

Memperingati 21 Mei 1998 bisa merupakan pelajaran politik yang sangat diperlukan dan pelajaran sejarah yang sangat berharga bagi rakyat Indonesia, untuk bisa melihat dengan jelas bahwa era pemerintahan jenderal Suharto betul-betul adalah bagian yang paling gelap, yang paling busuk, yang paling rusak dari sejarah Republik Indonesia dan bangsa Indonesia. Rejim militer Orde Baru adalah perusak besar jiwa perjuangan rakyat, yang telah dipupuk dan dikobarkan puluhan tahun oleh berbagai tokoh perintis kemerdekaan, terutama Bung Karno.

Orang-orang yang tidak waras nalarnya

Kalau dalam tahun 1998 hanyalah sebagian kecil saja dari masyarakat (contohnya : kalangan generasi muda) yang berani bersuara atau bergerak melawan Suharto dengan Orde Baru-nya, maka sekarang ini -- kebalikannya ! – makin sedikit orang atau kalangan yang masih berani terang-terangan memuji-muji Suharto dan apalagi membela Orde Baru. Sebab, sekarang ini makin terbukti dengan jelas bahwa begitu busuknya Suharto dan begitu buruknya rejim militernya, sehingga kalau ada orang atau kalangan yang masih suka koar-koar memuji-mujinya, maka bisa dianggap tidak waras fikirannya atau sakit jiwanya.

Dan orang-orang semacam itu tidak hanya terdapat di kalangan Golkar atau petinggi-petinggi militer (yang masih aktif maupun yang sudah pensiun), tetapi juga di kalangan partai-partai lainnya dan berbagai “tokoh” masyarakat. Dan, sekarang ini, kelihatan makin jelas juga bahwa hanyalah orang-orang yang perlu dipertanyakan kualitas moralnya (atau kalangan yang pantas diragukan kesehatan jiwanya) yang masih berani sesumbar tentang kebaikan sistem politik Orde Baru. Atau, hanyalah orang-orang yang sakit nalarnya -- ma’af, kalau kata-kata ini dianggap terlalu kasar ! -- masih ngotot menganggap bahwa Suharto adalah tokoh agung yang perlu dihormati sebagai “pahlawan pembangunan “ atau sebagai bapak bangsa yang berjasa besar.

Nyatanya, Suharto adalah pengkhianat besar terhadap Bung Karno, dan mengkhianati Bung Karno adalah sama saja dengan mengkhianati revolusi rakyat Indonesia. Sebab, Bung Karno adalah pengejowantahan dari perjuangan revolusioner rakyat Indonesia. Jadi, jelaslah bahwa Suharto sebenarnya adalah musuh dari revolusi rakyat Indonesia di bawah pimpinan Bung Karno.

Suharto adalah panutan bangsat

Bahwa generasi muda Indonesia dalam tahun 1998 sudah berhasil secara besar-besaran dan serentak di seluruh negara memaksa turunnya Suharto dari jabatannya sebagai presiden, adalah satu bukti yang jelas sekali bahwa negara dan bangsa kita sudah mencampakkannya. Itu adalah wajar, bahkan sudah seharusnya !!! Sebab, sudah ternyata selama 32 tahun Orde Baru bahwa sistem politik dan kekuasaan Suharto adalah sumber dari banyak ketidakadilan, sumber penderitaan, sumber penganiayaan bagi sebagian terbesar rakyat kita.

Suharto sudah membunuh atau menyuruh (langsung dan tidak langsung) membunuh Bung Karno beserta jutaan pendukungnya, dan membiarkan terbunuhnya atau dipenjarakannya jutaan orang lainnya yang tidak bersalah, dan membiarkan puluhan juta keluarga (istri, anak dan saudara-saudara) orang kiri menderita dan sengsara sampai lebih dari 40 tahun. Di samping itu Suharto juga sudah mencekek kebebasan demokrasi bagi sebagian terbesar rakyat kita, sambil secara sewenang-wenang mengumpulkan harta haram bagi seluruh keluarganya (harap ingat, antara lain : segala macam cerita tentang Tutut, Sigit, Bambang, Tommy, Probosutedjo, laporan majalah Time, laporan PBB tentang Suharto sebagai koruptor terbesar di dunia dll dll).

Mengingat itu semuanya, maka jelaslah bahwa nalar yang sehat tidak akan bisa menyetujui berlangsugnya terus-menerus kekuasaan politik Suharto. Karena, juga jelaslah bahwa fikiran yang waras tidak mungkin menganggap bahwa sebagian terbesar dari apa yang telah dikerjakan Suharto adalah untuk kebaikan bangsa dan negara. Lagi pula, moral yang benar pastilah tidak akan menjadikan Suharto sebagai sosok panutan bangsa, melainkan (yang lebih tepat, dan sepantasnya) sebagai panutan bangsat Sebab, sejak lahirnya Orde Baru terlalu banyak bangsat-bangsat yang mengabdi di bawah Suharto.

Karenanya, kiranya patut menjadi kesedaran bagi semua pendukung Suharto bahwa sebenarnya, (atau pada hakekatnya), penolakan terhadap Suharto adalah bukan hanya berkaitan dengan diri pribadi Suharto sebagai presiden, melainkan juga penolakan terhadap segala yang terpaut dengan politik dan praktek-praktek Orde Baru.. Penolakan terhadap Suharto (yang merupakan juga penolakan terhadap jenderal-jenderal dan pendukung setianya) berarti bahwa perlu diadakannya perubahan atau pergantian, atau perombakan, dan diganti dengan sistem atau politik kekuasaan yang berlainan sama sekali dari pemerintahan tipe Orde Baru.

Tujuan reformasi : perombakan sistem Orde Baru

Aspirasi atau kegandrungan dari sebagian terbesar rakyat terhadap adanya perubahan ini nyata sekali dari menggeloranya gerakan menuntut reformasi, yang dengan gencar sekali dilancarkan di seluruh negeri oleh banyak kalangan sebagai bagian penting dari penolakan terhadap Suharto selaku pimpinan negara. Suharto adalah perwakilan terpusat dari seluruh sistem politik dan kekuasaan Orde Baru. Suharto adalah pengejowantahan dari rejim militer, yang korup, yang berciri-ciri fasis, yang anti-rakyat, yang reaksioner atau yang anti-revolusioner.

Jadi, kalau dikaji dalam-dalam, hakekat arti reformasi adalah aspirasi banyak kalangan untuk menolak atau membuang jauh-jauh segala hal yang serba negatif dari pemerintahan era Suharto yang selama 32 tahun sudah mengangkangi atau mencengkam negara dan bangsa Indonesia, sambil mengeruk harta rakyat dengan cara-cara haram atau bathil. Sebab, sudah terlalu lama, dan juga sudah kelewatan besar pula (!) , kerusakan atau kebobrokan di banyak bidang, yang dibikin oleh Suharto bersama pimpinan militer pendukungnya. Sekali lagi, singkatnya, tujuan reformasi adalah adanya perubahan, atau penggantian, atau perombakan, atau pembaruan, atau restrukturasi segala yang berbau rejim militer Orde Baru, untuk diganti dengan yang lain.

Sayangnya, gerakan reformasi yang sangat diperlukan oleh bangsa dan negara ini ternyata kemudian mandeg atau macet di tengah jalan, karena sabotase oleh sisa-sisa kekuatan Orde Baru (bukan hanya yang di Golkar saja, tetapi juga di kalangan yang lain-lain) yang masih bisa bertahan terus di berbagai bidang penting dalam pemerintahan dan kehidupan politik (termasuk lembaga-lembaga legislatif dan judikatif) dan ekonomi. Karena itu, bolehlah dikatakan bahwa reformasi akhirnya mati begitu saja, dan bahkan tidak banyak dibicarakan orang lagi sekarang ini.

Itulah sebabnya, maka pada pokoknya dan secara garis besar situasi negara dan bangsa kita tidak berubah secara fundamental, dan masih dirundung berbagai “penyakit parah” seperti selama di bawah pemerintahan Orde Baru. Perubahan atau perbaikan yang nampak agak jelas adanya kebebasan pers, kebebasan menyatakan pendapat, dan kebebasan berserikat (walaupun masih ada pembatasan-pembatasan).

Sejak Suharto dijatuhkan dari jabatannya, sampai pemerintahan SBY-JK yang sekarang, politik pemerintahan yang berganti-ganti (Habibi, Gus Dur, dan Megawati) di bidang ekonomi dan sosial tetap anti-rakyat atau reaksioner, pro-neoliberal, mengutamakan prioritas kepada kepentingan asing dan tidak menjalankan prinsip berdikari. Sebagai akibatnya, pengangguran sangat membengkak, kemiskinan kelihatan makin meluas. Korupsi yang terus merajalela, dan kebejatan moral yang makin merata, adalah sebagian kecil dari persoalan-persoalan yang dihadapi berbagai pemerintahan, sampai sekarang.

Berdasarkan pengamatan pengalaman selama itu, maka sulitlah kiranya bagi kita untuk mempunyai optimisme bahwa situasi politik, ekonomi dan sosial di negara kita akan bisa mengalami perubahan yang fundamental, atau yang radikal dan besar-besaran. Kerusakan moral atau kebejatan akhlak di kalangan “elite” yang berjalin dengan politik reaksioner dan anti-rakyat membikin sulit adanya perubahan.

DPR yang baru tidak akan membawa perobahan

Walaupun komposisi anggota DPR diganti dengan masuknya 9 partai yang memenangkan suara dalam pemilu legislatif (Patai Demokrat, Golkar, PDI-P, PKS, PAN, PPP, PKB, Gerindra, Hanura), berdasarkan pengalaman praktek mereka di masa yang lalu, maka sudah dapat diramalkan sejak sekarang bahwa DPR yang baru ini tetap bukanlah perwakilan rakyat yang sebenarnya. Karenanya, seperti yang sudah-sudah, DPR yang baru ini akan tetap juga merupakan alat bagi kalangan elite, kalangan polykrasi, kalangan pemimpin-pemimpin gadungan, untuk menipu rakyat atas nama rakyat.

Demikian juga, dengan pemilihan presiden dan wakilnya yang akan datang, tidak peduli apakah yang akan jadi presiden nanti adalah SBY atau yang lain. Kalau SBY terpilih lagi, maka kita sudah bisa mengira-ira bahwa tidak akan bisa ada perubahan besar dan penting-penting di bidang politik ekonomi dan sosial. Bahkan dapatlah diramalkan bahwa keadaan akan bertambah sulit bagi kehidupan sebagian terbesar dari rakyat kita, sebagai akibat dari krisis global dewasa ini dan di masa datang.

Karena tidak bisa lagi menaruh harapan kepada 9 partai pemenang pemilu legislatif untuk adanya perubahan besar, maka kini tibalah waktunya bagi semua kekuatan demokratis, yang terhimpun di luar yang 9 partai itu, untuk bersama-sama berusaha membangun kekuatan alternatif yang berskala nasional dan betul-betul merakyat. Gabungan besar kekuatan alternatif ini seyogianya bisa menghimpun sebanyak mungkin segala macam unsur yang bisa dipersatukan dari banyak kalangan ormas atau ornop, dari kalangan 29 partai-partai kecil yang tidak dapat lolos pemilu (dan partai-partai kecil lainnya) serta dari kalangan golput.

Gerakan extra-parlementer yang kuat

Gabungan kekuatan alternatif, yang bisa berbentuk federasi, atau aliansi, atau koalisi dari berbagai macam kesatuan dan persatuan yang independen, bertujun untuk menjadi gerakan besar-besaran dari perjuangan extra parlementer, untuk mengontrol pemerintah dan DPR, dan memperjuangkan kepentingan rakyat yang mendesak. Seperti halnya di banyak negeri di dunia, gerakan extra-parlementer adalah sangat diperlukan bagi sehatnya kehidupan demokratik, bahkan juga di negeri-negeri yang sudah terkenal sejarah demokrasinya seperti Eropa. Khususnya, situasi di Indonesia yang begitu banyak dirongrong oleh berbagai persoalan parah lebih-lebih memerlukan gerakan extra-parlementer yang besar, kuat, dan bersatu erat dengan rakyat banyak.

Rakyat Indonesia sudah ternyata tidak bisa (dan juga tidak boleh !) terus-menerus mempunyai ilusi atau harapan kosong bahwa dari tokoh-tokoh partai-partai (dan pemimpin-pemimpin militer !) yang pro-Suharto dan anti Bung Karno akan bisa lahir pandangan dan tindakan yang benar-benar pro-rakyat banyak, yang anti-neoliberalisme, yang revolusioner, yang merupakan jalan keluar dari keterpurukan yang sudah dirundung bangsa dan negara sejak lama.

Panca Azimat Revolusi Bung Karno perlu dipelajari lagi

Dalam menghadapi situasi serba semrawut yang mendatangkan kesengsaraan bagi sebagian terbesar rakyat seperti yang kita hadapi dewasa ini, terasalah betapa benarnya ajaran-ajaran revolusioner Bung Karno. Ajaran-ajaran revolusioner Bung Karno bisa merupakan landasan berfikir, dan platform bersama bagi gerakan extra-parlementer yang luas, yang bisa menunaikan tugas reformasi. Lebih jauh lagi, ajaran-ajaran Bung Karno adalah jalan untuk meneruskan revolusi, menuju cita-cita bersama, yaitu masyarakat adil dan makmur.

Ajaran-ajaran besar Bung Karno yang terkandung dalam Panca Azimat Revolusi yang pernah mengkobarkan jiwa perjuangan rakyat Indonesia, dan yang dewasa ini tetap berguna sekali dipelajari kembali untuk dijadikan pedoman dan ditarik sari patinya adalah :


(1) Nasakom (sejak tahun 1926 dalam tulisan nasionalisme, Islamisme, marxisme).
(2) Pancasila yang lahir tahun 1945.
(3) Manipol USDEK lahir tahun 1959,
(4) Trisakti (berdaulat di bidang politik, berdikari di bidang ekonomi, dan berkepribadian di bidang kebudayaan) tahun 1964, dan
(5) Berdikari (berdiri di atas kaki sendiri) lahir tahun 1965.

Karena Suharto dan rejim militer Orde Baru sudah digulingkan, maka tugas rakyat Indonesia adalah melanjutkan terus-menerus gerakan reformasi di segala bidang, sebagai langkah penting untuk melanjutkan revolusi yang belum selesai, seperti yang digagas oleh Bung Karno.
