---
layout: post
title: Ketetapan MPRS no 25/1966 tentang larangan PKI harus dicabut
date: 2010-10-01
---

Pada tanggal 2-3 Oktober 2010, bertempat di Diemen Amsterdam telah dilangsungkan peringatan 45 tahun peristiwa 30 September 1965. Peringatan ini diadakan oleh sebuah Panitia yang didukung berbagai organisasi masyarakat Indonesia di Negeri Belanda. Di samping dari negeri Belanda, dalam pertemuan ini juga hadir perwakilan berbagai komunitas orang Indonesia dari Jerman, Perancis dan Swedia. Beberapa sambutan disampaikan dalam pertemuan penting yang meliputi beberapa  negeri di Eropa ini. Juga telah diputar film-film dokumenter yang berkaitan dengan masalah HAM di sekitar peristiwa tersebut.



 Di bawah ini adalah sambutan Sdr. A. Umar Said, yang dibacakan oleh  Sdr Sujoso, yang datang dari Paris untuk menghadiri pertemuan yang mendapat sukses besar tersebut.



Teks lengkap sambutan tersebut adalah sebagai berikut :





Ketetapan MPRS no 25/1966 tentang larangan PKI harus dicabut

Demi persatuan bangsa dan

kebaikan anak-cucu kita





« Saya merasa senang dapat ikut berpatisipasi dalam Peringatan 45 tahun Tragedi Nasional 1965 yang diadakan di negeri Belanda sekarang ini, walaupun saya tidak dapat hadir secara fisik dalam pertemuan yang amat penting ini., berhubung dengan masalah kesehatan.

Pertemuan di Holland ini sangat penting, karena memperingati peristiwa 30 September 1965 merupakan kewajiban yang perlu dilakukan, dengan berbagai cara dan bentuk, oleh  semua orang yang menjadi korban diktatur militer Suharto. Sebab, sebagai akibat  atau kelanjutan peristiwa 30 September 1965, negara dan bangsa Indonesia telah dijerumuskan oleh pimpinan Angkatan Darat ke dalam kegelapan selama 32 tahun, kegelapan yang penuh penderitaan, yang belum pernah terjadi dalam sejarah bangsa Indonesia.

Perlu diingat bersama oleh kita semua bahwa korban rejim Orde Baru sebenarnya adalah besar sekali, bahkan boleh dikatakan sebagian terbesar dari rakyat Indonesia telah menjadi korban, dalam bentuk dan kadar yang berbeda-beda, dan dalam berbagai situasi yang berlain-lainan pula.

 Sebab, rejim militer Suharto telah menjalankan diktatur yang menyeluruh di segala bidang dan dalam jangka waktu yang lama sekali, yang meliputi seluruh Indonesia, dan juga di luar negeri. Diktatur militer Suharto ini telah melakukan banyak sekali kejahatan terhadap kemanusiaan, dan telah merusak sendi-sendi negara kita, yaitu Pancasila dan Bhinneka  Tunggal Ika.

Dengan dalih menumpas Gerakan 30 September, dimana 6 jenderal Angkatan Darat terbunuh dalam pertentangan intern sesama tentara, Suharto bersama konco-konconya  -- di dalam negeri maupun di luarnegeri, terutama di kalangan imperialis AS  --  telah melakukan serentetan panjang tindakan yang penuh dengan pelanggaran kemanusiaan dan pengkhianatan terhadap presiden Sukarno, dan seluruh kekuatan revolusioner pendukung Bung Karno dan PKI.

Kejahatan atau dosa-dosa Suharto bersama jenderal-jenderalnya terhadap Bung Karno dan terhadap seluruh kekuatan revolusioner di Indonesia ini sudah mulai terbongkar sedikit demi sedikit, setelah selama kurang lebih 32 tahun digelapkan atau disembunyikan dengan berbagai cara. Selama puluhan tahun itu telah diadakan larangan terhadap tersiarnya berita-berita yang tidak menguntungkan rejim Suharto dibarengi dengan berbagai intimidasi, terror, persekusi, ancaman « bahaya laten PKI », yang semuanya dilakukan sistematis, menyeluruh, intensif, terus-menerus dan dalam jangka lama sekali.

Itu semua perlu diketahui oleh sebanyak mungkin rakyat kita, dan usaha ke arah ini, dengan mengadakan pertemuan di Holland ini adalah langkah penting untuk selalu ingat kepada kejahatan Suharto yang banyak sekali serta dosa-dosa Orde Barunya yang sangat berat. Kegiatan semacam ini sangat penting sebagai sumbangan untuk  pendidikan politik dan pendidikan moral bagi bangsa kita, termasuk terutama sekali untuk anak-cucu kita di kemudian hari.

Berbagai generasi kita di kemudian hari perlu tahu bahwa negara dan bangsa kita pernah mengalami pengkhianatan besar-besaran oleh seorang yang bernama Suharto, dan yang  telah melakukan  bermacam-macam  kerusakan dan pembusukan yang parah sekali di hampir segala bidang kehidupan.

Pertemuan-pertemuan seperti yang diadakan di Holland  ini perlu sesering mungkin diadakan selanjutnya di kemudian hari, juga di tempat-tempat lainnya, terutama di Indonesia. Sebab, melalui pertemuan semacam ini kita tidak saja dapat mengenang segala kejahatan Suharto melalui Orde Barunya, melainkan juga melawan segala kejahatan dan kebusukan yang diteruskan oleh pemerintahan di bawah SBY sekarang ini.

Sebab, banyak sekali kebobrokan, kebusukan, kerusakan yang kita sama-sama saksikan dewasa ini adalah produk dari sistem pemerintahan diktatur militer Orde Baru beserta sisa-sisanya yang masih bercokol kuat di banyak bidang ekskutif, legislatif, dan judikatif sekarang ini.

Jadi, membongkar kejahatan Orde Baru bukanlah perbuatan yang sudah kedaluwarsa untuk mengutik-utik persoalan lama dan membuka luka-luka lama saja. Sebab, luka lama bangsa kita adalah besar sekali dan parah sekali. Sisa-sisanya sampai sekarang masih terdapat banyak sekali di seluruh Indonesia. Di antara luka-luka lama ini adalah masalah korban rejim Orde Baru yang jumlahnya puluhan juta orang, berikut keluarga mereka, yang sampai sekarang masih mengalami berbagai macam  penderitaan. Dalam hal ini termasuk penderitaan para eks-tapol beserta keluarga mereka.

Membongkar terus kejahatan Suharto beserta Orde Barunya justru adalah penting untuk menghadapi masa kini, dan juga masa depan. Masa kini erat hubungannya dengan masa lampau yang telah dirusak oleh Suharto beserta para konconya, baik yang di dalam negeri maupun di luar negeri.

Di antara dosa besar Suharto yang banyak menimbulkan kerusakan itu  adalah Ketetapan MPRS no 25/1966, yang telah diputuskan oleh MPRS gadungan,  dengan penuh rekayasa yang dibarengi dengan paksaan oleh penguasa militer di bawah Suharto. Ketetapan MPRS no 25/1966 ini melarang kegiatan PKI beserta ormas-ormasnya serta melarang disebarkannya Marxisme Leninisme.

Dengan dalih melaksanakan Ketetapan MPRS 25/1966  yang melarang kegiatan PKI itu maka rejim militer Suharto telah melakukan berbagai pelanggaran HAM yang sangat berat, tidak hanya terhadap golongan PKI melainkan juga golongan revolusioner lainnya, termasuk para pendukung Bung Karno.  Bung Karno telah di-kudeta oleh Suharto beserta pimpinan Angkatan Darat lainnya,  karena dianggap terlibat dengan PKI dan tidak mau membubarkan PKI walaupun didesak keras oleh pimpinan Angkatan Darat.

Ketetapan MPRS no 25/1966 ini adalah undang-undang yang telah dipakai (dan disalahgunakan) oleh rejim militer Orde Baru untuk melakukan terror fisik dan mental secara besar-besaran, membungkem mulut rakyat banyak, dan menginjak-injak kehidupan demokratis, sambil sekaligus melakukan korupsi serta berbagai kebijakan yang merugikan  orang banyak dan hanya menguntungkan segolongan orang yang mendukung Suharto beserta Orde Barunya.

Karenanya, bisalah dikatakan bahwa ketetapan MPRS 25/1966 telah merupakan alat terror yang telah  mengekang kebebasan banyak orang,  tidak saja bagi golongan PKI serta simpatisan-simpatisannya, melainkan juga bagi golongan non-PKI yang tidak menyetujui Orde Baru. Slogan « Awas bahaya laten PKI » atau « Cegah come-backnya PKI » dipakai untuk meng-intimidasi atau menakut-nakuti semua orang yang berani mempunyai sikap kritis terhadap rejim militer Suharto.

Ketetapan MPRS no 25/1966, resminya saja hanya ditujukan kepada golongan PKI beserta simpatisan-simpatisannya, namun dalam prakteknya mempunyai dampak negatif yang luas juga bagi kebebasan berbagai golongan yang non-PKI. Undang-undang yang merugikan kebebasan demokratis atau menginjak-injak HAM ini berlangsung selama 32 tahun Orde Baru, dan diteruskan sampai sekarang, artinya sudah berlangsung selama 44 tahun.

Kalau selama Orde Baru Ketetapan MPRS 25/1966 ini bisa merupakan senjata ampuh sekali dalam terror untuk menjaga keselamatan kekuasaan Suharto, maka sejak runtuhnya Orde Baru dan meningggalnya Suharto perannya sudah mulai berubah atau berkurang. Meskipun sisa-sisa Orde Baru  dalam berbagai kalangan terkadang-kadang masih juga bisa menggunakannya untuk menghalang-halangi kebebasan demokratis (ingat, antara lain peristiwa Banyuwangi mengenai gangguan pertemuan Dr Tjiptaning oleh FPI, dan larangan Kejaksaan Agung terhadap buku « Lekra tak membakar» karya pengarang dan sejarawan  muda Rhoma Juliantri).

Ketetapan MPRS no 25/1966 adalah salah satu  di antara banyak monumen buruk yang merupakan  simbul kejahatan dan pelanggaran HAM rejim Suharto, yang masih diteruskan sampai sekarang oleh pemerintahan SBY. Karenanya, setelah Suharto dijatuhkan oleh gerakan generasi muda dalam tahun 1998, dan dijalankannya reformasi di berbagai bidang, maka masih terus dipertahankannya sampai sekarang undang-undang yang menyengsarakan begitu banyak orang itu adalah sesuatu yang harus dipersoalkan dan harus dilawan oleh sebanyak mungkin kalangan dan golongan dalam masyarakat.

Bagi sebagian terbesar dari rakyat Indonesia adanya Ketetapan MPRS 25/1966 tidak ada manfaatnya atau gunanya, atau tidak membawa kebaikan sama sekali. Situasi negara dan bangsa sekarang ini sudah berlainan sekali dengan situasi di era Orde Baru, ketika Suharto memerlukan adanya undang-undang yang bisa digunakan (atau disalahgunakan) untuk melakukan terror demi kestabilan atau keselamatan rejim militernya
.
Ketetapan MPRS 25/1966,  meskipun masih belum dicabut atau dinyatakan tidak berlaku lagi, namun dalam prakteknya sekarang sudah makin tidak ditakuti, bahkan di-« cuweki » saja oleh berbagai kalangan.  Memang, pertemuan atau kegiatan-kegiatan tidak ada (atau belum ada) yang diselenggarakan secara resmi dengan menggunakan nama PKI atau simpatisan-simpatisannya. Tetapi, siapakah  bisa melarang orang mempunyai pandangan atau fikiran yang berhaluan komunis, marxis atau sosialis ? Dan lagi, orang-orang  semacam ini bisa saja melakukan berbagai macam kegiatan, tanpa menggunakan nama organisasi PKI.

Banyak penerbit yang sejak beberapa tahun yang lalu sudah mengedarkan bahan bacaan yang isinya bisa diartikan mengandung Marxisme atau berbau Leninisme, termasuk  buku Das Kapitalnya Karl Marx. Sudah banyak juga tulisan-tulisan dalam majalah atau suratkabar yang menyajikan segi-segi positif PKI dalam perjuangan untuk bangsa di masa-masa yang lalu. Larangan penyebaran Marxisme atau Leninisme masih berlaku, namun dalam prakteknya sudah banyak dilanggar oleh banyak orang. Ini menunjukkan bahwa undang-undang yang melanggar HAM ini sudah kedaluwarsa dan sudah  tidak sesuai lagi dengan perkembangan situasi di Indonesia dan kemajuan jaman.

Larangan yang dicantumkan dalam Ketetapan MPRS no 25/1966 itu kelihatan makin tidak berfungsi dengan adanya kemungkinan untuk menggunakan Internet secara bebas bagi siapa saja yang mau menulis tentang PKI atau menyiarkan Marxime dan Leninisme. Bahkan, bahan bacaan tentang Partai-partai komunis sedunia pun bisa diperoleh dengan mudah melalui Internet, termasuk berbagai ajaran-ajarannya. Ketetapan MPRS tersebut, yang dalam masa-masa Orde Baru bisa digunakan sebagai senjata ampuh,  sekarang ini sudah tidak bisa lagi mencegah tersebarnya bahan bacaan yang bisa dianggap mengandung  propaganda PKI dan Marxime Leninisme.

Dan lagi, semboyan-semboyan anti-PKI  yang pernah terdengar gegap gempita di seluruh negeri dan dipakai sebagai alat mujarab oleh rejim militer Suharto selama puluhan tahun, antara lain « Awas bahaya laten PKI », sudah tidak banyak terdengar lagi sejak runtuhnya Orde Baru dan meninggalnya Suharto. Perkembangan situasi di Indonesia sejak pembunuhan massal di seluruh negeri dan secara besar-besaran terhadap golongan PKI beserta simpatisan-simpatisannya  menunjukkan dengan jelas sekali bahwa « Bahaya laten PKI », sebenarnya tidak ada pada waktu itu. Slogan itu terutama dipakai untuk melakukan terror dalam usaha penyelamatan diktatur militer Suharto.

Perkembangan situasi di Indonesia sekarang ini, makin menunjukkan dengan jelas sekali bahwa bahaya yang sesungguhnya bagi negara dan rakyat Indonesia bukanlah PKI atau datang dari kalangan PKI. Bahaya yang besar dan laten bagi negara dan bangsa Indonesia justru terutama datang dari kalangan atau golongan Islam fundamentalis, dan dari kalangan teroris ,  yang  s u d a h, s e d a n g   dan juga  a k a n  t e r u s   melakukan berbagai kegiatan yang merusak negara Republik Indonesia.

Golongan PKI dan simpatisan-simpatisannya pada umumnya adalah pendukung politik atau ajaran-ajaran revolusioner Bung Karno. Dan ajaran-ajaran revolusioner Bung Karno yang anti-kapitalisme, anti-kolonialisme, anti-imperialisme dan pro-sosialisme adalah sikap yang cocok dengan perjuangan rakyat menentang neo-liberalisme yang mencengkam negara dan bangsa kita dewasa ini.

Jadi jelaslah  bahwa Ketetapan MPRS no 25/1966 itu sudah kedaluwarsa dan merupakan duri besar yang menyakitkan sekali dalam tubuh bangsa. Selama undang-undang ini belum dicabut maka akan tetap menyakitkan hati banyak orang, terutama kalangan kiri pendukung ajaran-ajaran revolusioner Bung Karno. Karenanya, undang-undang yang sudah kedaluwarsa dan tidak ada gunanya  ini juga hanya merupakan sumber dendam banyak golongan, terutama golongan  kiri.

Ketetapan MPRS no 25/1966 yang tidak ada gunanya lagi ini hanya merupakan sisa-sisa yang buruk dari rejim militer Suharto, yang merusak jiwa bangsa menjadi terpecah belah dan saling membenci,  sehingga menggoyahkan persatuan dan kesatuan rakyat yang didasarkan kepada Päncasila dan Bhinneka Tunggal Ika.

Jadi, jelaslah bahwa Ketetapan MPRS no 25/1966 tidak mendatangkan kebaikan apa pun bagi bangsa dan negara, melainkan hanya menguntungkan kepentingan musuh-musuh negara dan rakyat yang sebenarnya, yaitu kaum reaksioner pendukung setia Suharto, atau kekuatan pro neo-liberalisme baik yang di dalam negeri maupun yang datang dari luar negeri.

Oleh karena itu, adalah jelas sekali bahwa undang-undang yang merupakan noda besar dan sumber penyakit jiwa bangsa ini perlu sekali segera dicabut oleh MPR sendiri bersama pemerintah dan lembaga-lembaga negara lainnya yang berkepentingan. Penghapusan Ketetapan MPRS N0 25/1966 adalah penting tidak hanya bagi golongan PKI atau eks-PKI, melainkan juga untuk kepentingan seluruh bangsa, termasuk anak-cucu bangsa kita di kemudian hari.

Kalau undang-undang yang  selama 44 tahun telah menjadi sumber penyakit  bagi persatuan rakyat ini toh tetap terus dipertahankan, maka akan menimbulkan kerusakan-kerusakan yang lebih besar lagi bagi jiwa bangsa. Juga bisa dipertanyakan oleh generasi-generasi yang akan datang, mengapa generasi pendahulu mereka bisa melakukan kesalahan yang begitu besar dan dalam jangka waktu yang begitu lama.

Sudah waktunya bagi MPR dan DPR atau berbagai lembaga perwakilan rakyat lainnya untuk bersama-sama seluruh kekuatan rakyat menghilangkan undang-undang yang dipakai oleh Suharto bersama pimpinan Angkatan Darat lainnya untuk mengangkangi negara dan bangsa dengan diktaturnya.

Mencabut atau menghapus Ketetapan MPRS 25/1966 adalah tindakan yang berdasarkan nalar yang waras, rasa keadilan, keluhuran jiwa bangsa, dan demi persatuan berbagai golongan rakyat. Sedangkan tetap meneruskannya adalah sikap politik yang keliru, sikap moral yang tidak sehat, dan juga sikap iman  yang sesat !!!
