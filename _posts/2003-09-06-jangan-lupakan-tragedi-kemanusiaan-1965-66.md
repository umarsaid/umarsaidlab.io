---
layout: post
title: Jangan Lupakan Tragedi Kemanusiaan 1965-66
date: 2003-09-06
---

Ken Gagah Pamungkas (6 September 2003)

Banyaknya fakta kelumpuhan pihak-pihak yang bertanggung jawab dalammenangani peristiwa-peristiwa tragedy di masa lalu, menimbulkan keprihatinanatas perkembangan bangsa di masa depan. Jelas akan menimbulkan pertanyaanyang sangat skeptis: masih akan bisa ditegakkankah hukum, keadilan dan HAMdi negeri ini?

Adalah sangat kita puji posisi Nurcholis Madjid dalam masalah terorismekasus Kuta yang mengorbankan 200-an nyawa manusia. Dikatakan olehnya:“Ungkapan kata-kata apalagi yang bisa diucapkan berkenaan dengan tragedikemanusiaan pembunuhan jiwa tanpa alasan seperti di Kuta, selain kutukan danlaknat. Suatu kejahatan yang merupakan pelanggaran dan tantangan terhadapajaran agama tentang kesucian nyawa, harta, dan martabat manusia”.Ditambahkan juga ayat Al Qur’an sebagai dasar pembenaran posisinya: "Barangsiapa membunuh seseorang tanpa kejahatan pembunuhaan atau perusakan di bumi,maka bagaikan dia membunuh seluruh umat manusia. "(Q5: 32). Kemudiandibulatkan pendapatnya bahwa: “Dalam ajaran itu, kejahatan terhadap satuorang, satu jiwa, lebih-lebih lagi kejahatan pembunuhan bukanlah semata-matakejahatan kepada orang per orang sebagai pribadi saja. Kejahatan itumerupakan kejahatan kepada segenap umat manusia, crime against humanity,pelanggaran terhadap kesucian hak manusia yang paling asasi dan palinginversial.”)

Saya kira tidak aada seorangpun yang akan menilai negatif apa yang diakatakan tersebut diatas. Bravo, hebat sekali.

Tetapi sangat disayangkan mengapa cendikiawan Nurcholis Madjid kurangperhatian terhadap apa yang terjadi 38 tahun yang lalu, yaitu salah satuKejahatan Kemanusiaan yang terbesar di dunia: Pembunuhan 2-3 juta manusiatak bersalah, penahanan tanpa proses hukum selama belasan tahun, pencabutanpassport orang-orang yang ada di luar negeri (karena belajar di NegaraSosialis, karena menyuarakan aspirasi demokrasi), pendiskriminasian puluhanjuta manusia karena keluarganyanya dituduh tersangkut G30S. Sangatdisayangkan, pada hal bulan September ini adalah bulan titik awal tragedykemanusian tersebut, di mana setiap orang yang peduli HAM perlumengheningkan cipta untuk mengenangkan tragedy tersebut dan berdoamudah-mudahan tidak terulang lagi atas anak cucu kita.

Semua pelanggaran HAM besar seharusnya mendapat perhatian untuk
penyelesaiannya secara tuntas. Adalah suatu diskriminasi keadilan besarapabila yang dipedulikan hanya masalah Tragedi Semanggi, Trisakti, Istiqlal,Natal, Kuta, Hotel Marriott dll. Tanpa penuntasan masalah pelanggaran HAM1965-66 berarti membiarkan bangsa ini tetap dalam suasan saling curiga,saling praduga jelek, saling simpan rasa dendam. Dengan demikian akanterganggulah segala upaya untuk memajukan bangsa dan negara ke arahdemokraasi yang berkeadilan dan berkemakmuran.

Terutama para tokoh Islam harus lebih merenungkan akan peristiwa tragedykemanusiaan 1965-66 tersebut di atas dan memelopori mencari jalanpenormalisasian situasi yang sudah puluhan tahun dijadikan abnormal olehrejim fasis Soeharto. Sebab tanpa keaktifan tokoh-tokoh Islam sendiri, akantetap melekatlah attribut “kekerasan” terhadap Islam. Padahal menurut ajaranIslam sendiri kekerasan sangat dikutuk. Bagi orang non-Islam tentu yangpertama-tama dilihat adalah faktanya: fakta terjadinya tragedy kejahatankemanusiaan 1965-66 yang tidak dapat diputar-balikkan.

Para tokoh ummat Islam harus berani jalan di depan untuk membuka jalan bagirekonsiliasi nasional. Mereka harus berani membuktikan (bukan hanya ucapandalam doa) yang benar adalah benar dan yang salah adalah salah.

(http://www.suaramerdeka.com/harian/0309/06/nas5.htm)

Kolom IBRAHIM ISA 6 September 2003.
