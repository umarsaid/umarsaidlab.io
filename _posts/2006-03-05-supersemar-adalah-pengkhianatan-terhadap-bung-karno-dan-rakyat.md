---
layout: post
title: Supersemar adalah pengkhianatan terhadap Bung Karno dan rakyat
date: 2006-03-05
---

Agaknya, bagi bangsa Indonesia adalah penting sekali untuk selalu ingat bahwa pada 11 Maret 1966 ( jadi 40 tahun yang lalu) telah terjadi pengkhianatan besar yang dilakukan oleh segolongan pimpinan TNI-AD yang dikepalai oleh Letnan Jenderal Suharto (waktu itu) terhadap presiden Sukarno. Dan melalui pengkhianatan terhadap pemimpin besar rakyat Indonesia ini Suharto beserta para pendukungnya, baik yang di kalangan militer maupun yang bukan, telah melakukan kejahatan yang besar sekali terhadap revolusi dan rakyat Indonesia. Begitu besarnya kejahatan yang dilakukan dengan pengkhianatan ini, sehingga perlu sekali (dan pantas sekali!) dimasukkan sebagai bagian yang penting dari sejarah bangsa. Semua generasi bangsa yang akan datang perlu sekali mengetahui dengan jelas tentang berbagai kejahatan yang berkaitan dengan pengkhianatan besar ini.

Tulisan yang berikut ini adalah sekadar sumbangan dan ajakan untuk menyegarkan kembali ingatan bersama kita mengenai peristiwa, yang selama puluhan tahun disanjung-sanjung sebagai Supersemar (Surat Perintah Sebelas Maret) yang, pada hakekatnya, merupakan dosa besar dan aib berat yang harus dipikul oleh Suharto beserta para pendukung rejim militer Orde Baru. Supersemar yang pernah diagung-agungkan oleh para pendiri dan pendukung rejim militer Suharto dkk (yang ditulang-punggungi oleh TNI-AD dan Golkar) adalah salah satu di antara banyak sekali dan berbagai kejahatan besar yang telah dilakukan oleh rejim militer Orde Baru.

Karenanya, kalau selama puluhan tahun Orde Baru, Supersemar telah dibangga-banggakan setinggi langit oleh kalangan militer (terutama TNI-AD), Golkar, sebagian golongan Islam, dan tokoh-tokoh “cendekiawan”, maka sejak turunnya Suharto dari singgasana kepresidenan sedikit demi sedikit terkuaklah banyak kebusukan dan kejahatan sekitar pengkhianatan terhadap Presiden Sukarno dan penghancuran PKI yang merupakan pendukung utamanya. Sekarang ini, tidak begitu banyak lagi orang yang berani terang-terangan memuji Suharto, atau berani terlalu membangga-bangggakan Orde Baru, atau menyanjung-nyanjung Supersemar.

KUDETA MERANGKAK DIMULAI 1 OKTOBER 65

Kalau dilihat secara hakiki, sebenarnya pengkhianatan dan kudeta merangkak yang dilakukan oleh Suharto dan teman-temannya di kalangan Angkatan Darat terhadap Presiden Sukarno telah dimulai, pada tanggal 1 Oktober 1965, ketika Suharto sebagai panglima Kostrad mulai bergerak untuk menghancurkan kekuatan militer yang menyokong G30S. Dengan berbagai jalan dan cara (termasuk yang tertutup) Suharto dkk di kalangan Angkatan Darat telah melakukan pembangkangan terhadap kepemimpinan presiden Sukarno.

Pembangkangan dan kudeta merangkak Angkatan Darat terhadap Presiden Sukarno ini mulai dilakukan dengan gerakan besar-besaran untuk membunuhi, atau menangkapi secara sewenang-wenang sebagian terbesar pimpinan PKI di semua tingkatan (nasional, propinsi, kabupaten, kecamatan, bahkan pedesaan) di seluruh Indonesia. Di samping itu, pimpinan Angkatan Darat juga menciptakan berbagai langkah-langkah yang berakibat terjadinya pembantaian besar-besaran (sampai sekitar 3 juta orang) terhadap para anggota dan simpatisan PKI yang tidak bersalah apa-apa, dan penahanan ratusan ribu orang (yang juga tidak bersalah apa-apa !) di banyak daerah. Semuanya ini merupakan pelanggaran HAM besar-besaran oleh suatu regim terhadap bangsanya sendiri, yang kebiadabannya jarang tandingannya di dunia.

Pembangkangan dan kudeta merangkak ini mencapai tahap yang sangat penting dengan pemaksaan ditandatanganinya Surat Perintah Sebelas Maret oleh presiden Sukarno pada tanggal 11 Maret 1966, yang memberi kekuasaan (terbatas!!!) kepada Letnan Jenderal Suharto, yang berbunyi antara lain sebagai berikut:

“Mengambil segala tindakan yang dianggap perlu, untuk terjaminnya keamanan dan ketenangan serta kestabilan jalannja Pemerintahan dan jalannya revolusi, serta menjamin keselamatan pribadi dan kewibawaan Pimpinan Presiden/Panglima Tertinggi/Pemimpin Besar Revolusi/Mandataris MPRS demi untuk keutuhan Bangsa dan Negara Republik Indonesia, dan melaksanakan dengan pasti segala ajaran Pemimpin Besar Revolusi. Supaya melaporkan segala sesuatu yang bersangkut paut dalam tugas dan tanggung jawabnya seperti tersebut di atas »

Begitulah bunyi Supersemar yang disodorkan dengan paksa kepada presiden Sukarno. Tetapi, kemudian ternyata bahwa apa yang dilakukan oleh Suharto dan kawan-kawannya di Angkatan Darat berlawanan sekali dengan isi surat perintah tersebut. Dengan adanya penangkapan sewenang-wenang dan pemenjaraan ratusan ribu anggota dan simpatisan PKI yang tidak bersalah apa-apa telah dilancarkan teror besar-besaran di seluruh negeri, dan telah dirusak ketenangan di kalangan rakyat. Pimpinan Angkatan Darat waktu itu telah mengambil berbagai tindakan yang merusak kestabilan jalannya pemerintahan di bawah pimpinan presiden Sukarno dan jalannya revolusi.

Apa yang dilakukan Suharto dkk terhadap Bung Karno sama sekali tidak menjamin kewibawaan beliau sebagai presiden/panglima tertinggi/pemimpin besar revolusi/mandataris MPRS. Bahkan, pimpinan Angkatan Darat kemudian menjadikan beliau sebagai tapol, dan mengurung beliau dalam tahanan, sampai akhir hayat beliau. Suharto dkk juga sama sekali tidak melaksanakan ajaran pemimpin besar revolusi, bahkan sebaliknya, menghancurkan segala ajaran beliau (antara lain : Pancasila, politik anti-imperialis dan anti-nekolim, Nasakom, Manipol, sosialisme à la Indonesia, semangat gotong-royong, pengabdian kepada kepentingan rakyat kecil, dan persatuan bangsa). Dan, Suharto dkk juga tidak memberi laporan kepada presiden Sukarno tentang segala sesuatu yang bersangkut-paut dalam tugas dan tanggung jawabnja seperti yang tercantum dalam surat perintah tersebut di atas, bahkan melakukan berbagai tindakan tanpa sepengetahuan presiden Sukarno.

KEMUNAFIKAN DAN PENGKHIANATAN SUHARTO DKK

Kalau ditelusuri berbagai langkah-langkah atau tindakan Suharto sebagai « pengemban » Supersemar, maka jelaslah bahwa ia telah melakukan pengkhianatan besar terhadap Bung Karno.

Dan kalau di sini disebut Suharto, maka tidak berarti sebagai Suharto seorang diri, melainkan sebagai tokoh atau perwakilan golongan yang reaksioner dan anti-Sukarno, yang mempunyai politik dan kepentingan yang sama atau sejajar dengan imperialisme, terutama imperialisme AS. Anti-Sukarno adalah bagian penting dalam politik imperialisme dalam menghadapi Perang Dingin dan perjuangan rakyat Asia-Afrika yang digelorakan oleh Konferensi Bandung.

Menurut brosur yang dikeluarkan oleh Kementerian Penerangan tanggal 28 Maret 1966 (jadi sekitar dua minggu setelah penyodoran secara paksa Supersemar) pada tanggal 12 Maret 1965 Letnan Jenderal Suharto telah mengeluarkan Perintah Harian kepada Angkatan Darat, yang antara lain berbunyi sebagai berikut :

« Bagi Rakjat, hal ini berarti bahwa suara hati nurani Rakyat yang selama ini dituangkan dalam perjoangan yang penuh keichlasan, kejujuran, heroisme dan selalu penuh tawakal kepada Tuhan yang Maha Esa, benar-benar dilihat, didengar dan diperhatikan oleh Pemimpin Besar Revolusi Bung Karno yang sangat kita cintai dan yang juga merupakan bukti kecintaan Pemimpin Besar revolusi kepada kita semua .

« Percayakan tugas-tugas tersebut kepada ABRI anak kandungmu; Insya Allah ABRI akan melaksanakan Amanatmu, Amanat Penderitaan Rakyat, yaitu melaksanakan Revolusi kiri kerakyatan Indonesia, dengan ciri-ciri anti feodalisme, anti kapitalisme, anti Nekolim dan mewujudkan masyarakat adil-makmur berdasarkan Panca-Sila, masyarakat Sosialis Indonesia, yang diridhoi oleh Tuhan yang Maha Esa dalam taman sarinya satu Dunia Baru tanpa segala bentuk penindasan dan penghisapan;

« ABRI tidak hendak meng-kanankan Revolusi Indonesia seperti jang dituduhkan oleh benalu-benalu dan cecunguk-cecunguk Revolusi; ABRI juga tidak akan membiarkan Revolusi dibawa kekiri-kirian, sebab Revolusi kita memang sudah kiri. Siapa saja, golongan mana saja, yang akan menyelewengkan garis Revolusi, mereka akan berhadapan dengan ABRI, dan akan ditindak oleh ABRI »

Alangkah jauhnya perbedaan atau jarak antara kalimat yang terdengar bagus-bagus itu dengan apa yang dikerjakan oleh Suharto dkk sebelum dan sesudah penyerahan Supersemar. Ia bicara tentang « Pemimpin Besar Revolusi Bung Karno yang kita cintai » padahal tindakan-tindakannya sejak tanggal 1 Oktober (bersama-sama kawan-kawannya di Angkatan Darat) menunjukkan pembangkangan terhadap presiden Sukarno. Ia juga bicara bahwa « Abri akan melaksanakan Amanat penderitaan Rakyat, yaitu melaksanakan Revolusi kiri kerakyatan Indonesia, dengan ciri-ciri anti-feodalisme, anti-kpitalisme, anti-Nekolim dan mewujudkan masyarakat adil-makmur berdasarkanPanca-Sila, masyarakat Sosialis Indonesia…… », padahal sudah ternyata bahwa tindakannya sebagai pimpinan Angkatan Darat telah menghancurkan Revolusi kiri kerakyatan Indonesia.

Terbukti juga kemudian bahwa dalam menjalankan kekuasaannya Suharto sama sekali tidak melaksanakan sikap anti-feodalisme, anti-kapitalisme, anti-Nekolim, melainkan kerjasama dan bersahabat dengan kekuatan-kekuatan kapitalisme dan Nekolim. Terbukti juga dengan gamblang kemudian bahwa Orde Baru yang dipimpinnya tidak mewujudkan masyarakat adil-makmur berdasarkan Panca-Sila atau masyarakat sosialis Indonesia, bahkan mengubur cita-cita untuk mewujudkannya dengan membunuhi dan memenjarakan semua orang yang berjuang untuk sosialisme Indonesia



SUPERSEMAR MEMPERLANCAR BERBAGAI KEJAHATAN POLITIK

Jelaslah kiranya bahwa Supersemar yang selama puluhan tahun diagung-agungkan oleh Angkatan Darat bersama pendukung-pendukung setia Orde Baru, adalah kelanjutan dari pembantaian dan pemenjaraan jutaan anggota dan simpatisan PKI yang tidak bersalah apa-apa dan tidak ada hubungannya sama sekali dengan peristiwa G30S. Supersemar adalah juga merupakan kunci pembuka jalan bagi Suharto dkk untuk melakukan berbagai kejahatan politik, lebih lanjut dan lebih banyak lagi, terhadap presiden Sukarno dan PKI. Beberapa di antara berbagai kejahatan politik itu adalah :

Pada tanggal 12 Maret 1966 (jadi sehari sesudah Supersemar) Suharto “atas nama presiden/panglima tertinggi ABRI/mandataris MPRS/pemimpin besar revolusi “ mengeluarkan keputusan no 1/3/1966 mengenai PKI yang antara lain berbunyi sebagai berikut:

“Bahwa demi tetap terkonsolidasinya persatuan dan kesatuan segenap kekuatan progresif-revolusioner Rakyat Indonesia dan demi pengamanan jalannja Revolusi Indonesia yang anti feodalisme, anti kapitalisme, anti Nekolim dan menuju terwujudnya Masyarakat Adil-Makmur berdasarkan Pancasila, masyarakat Sosialis Indonesia, perlu mengambil tindakan cepat, tepat dan tegas terhadap Partai Komunis Indonesia;

MENETAPKAN : Dengan tetap berpegang teguh pada LIMA AZIMAT REVOLUSI INDONESIA.

Pertama : Membubarkan Partai Komunis Indonesia termasuk bagian-bagian Organisasinya dari tingkat Pusat sampai ke daerah semua Organisasi yang seazas/berlindung/bernaung di bawahnya ;

Kedua : Menyatakan Partai Komunis Indonesia sebagai Organisasi yang terlarang di seluruh wilayah kekuasaan Negara Republik Indonesia « (kutipan selesai)

Berdasarkan berbagai keterangan dan kesaksian banyak sumber, maka sekarang makin jelas bahwa keputusan untuk membubarkan dan melarang PKI seperti tersebut di atas adalah atas inisiatif Suharto dkk di Angkatan Darat, dengan mencatut “atas nama “ presiden /panglima tertinggi ABRI/pemimpin besar revolusi. Artinya, Bung Karno tidak tahu-menahu sebelumnya, dan bahkan tidak menyetujui pembubaran dan pelarangan terhadap PKI. Hal ini dapat dimengerti kalau diingat bahwa sejak mudanya di tahun 20-an, Bung Karno sudah menunjukkan simpatinya kepada perjuangan PKI dan Marxisme. Gagasan besar Bung Karno adalah persatuan bangsa Indonesia, yang merupakan penggabungan kekuatan politik berdasarkan NASAKOM.

Pada tanggal 14 Maret 1966 Suharto mengeluarkan peraturan, juga dengan mencatut “atas nama” presiden /panglima tertinggi ABRI/pemimpin besar revolusi, larangan bagi organisasi-organisasi partai politik dan organisasi-organisasi massa, untuk sementara tidak menerima/menampung anggota-anggota ex Partai Komunis Indonesia (PKI) beserta Organisasi-organisasi massanya yang seazas/bernaung/berlindung di bawahnya.

Larangan ini disusul dengan Seruan tanggal 14 Maret 1966 agar semua anggota pimpinan, kader-kader dan aktivis-aktivis PKI serta organisasi-organisasi massanya melaporkan diri.

Pada tanggal 18 Maret 1966 Menteri/PANGAD (Suharto) mengeluarkan Pengumuman No. 1/Peng/1966, “
