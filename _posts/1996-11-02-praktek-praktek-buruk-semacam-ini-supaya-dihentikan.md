---
layout: post
title: Praktek-praktek buruk semacam ini supaya dihentikan
date: 1996-11-02
---

Surat terbuka oleh Yoso Sanyoto kepada Presiden Suharto

Permohonan disalurkan lewat Bapak Murdiono, para menteri lainnya, khususnya lewat Pangab dan Jaksa Agung, dan Ketua DPR

> Bapak Presiden yang terhormat,
>
> Lewat Internet telah disiarkan, pada tanggal 31 Oktober, berita dari Australia mengenai kemungkinan terjadinya penganiayaan terhadap Budiman Sujatmiko dan Dita Indah Sari. Menurut berita itu, yang didapat dari saluran-saluran di Indonesia, Dita Indah Sari (pimpinan PPBI) telah diambil oleh BIA (Badan Intelijen Abri) dari tahanan Kejaksaan Surabaya, dan kemudian mengalami penganiayaan yang berat. Dalam penganiayaan itu Dita Sari telah dicabut beberapa kuku-jarinya, karena ia tidak mau menjawab pertanyaan-pertanyaan selama interogasi mengenai tuduhan-tuduhan subversif.
>
> Dalam berita lewat Internet itu juga disebutkan bahwa Sujatmiko, ketua PRD, juga mengalami hal yang serupa. Ia telah diambil oleh BIA dari tahanan Kejaksaan Agung di Jakarta, dan kemudian disiksa selama interogasi. Kuku-kuku jarinya juga telah dicabut.
>
> Bapak Presiden, berita yang disiarkan lewat Internet ini sudah pasti beredar diseluruh dunia. Sebab, tidak saja telah disiarkan oleh "Apakabar" (di AS) yang mempunyai langganan lebih dari 12 000 dan tersebar di seluruh dunia, melainkan juga oleh saluran-saluran lainnya lewat E-mail, yang terdapat di banyak negeri.
>
> Berita dari Australia yang ditulis dalam bahasa Inggris ini juga menyerukan kepada grup-grup atau orang-orang di berbagai negeri untuk menuntut kepada pemerintah mereka masing-masing supaya masalah ini dipersoalkan dengan pemerintah Indonesia. Juga diserukan supaya anggota-anggota parlemen dan para terkemuka di berbagai negeri itu menuntut kepada pemerintah Indonesia untuk menyatakan bahwa berita itu tidak benar dan memperlihatkan Budiman dan Dita Sari di depan umum untuk menyangkal kebenaran berita itu.
>
> Bapak Presiden, kami berpendapat bahwa pemerintah Indonesia (entah lewat badan pemerintahan yang mana) perlu sekali untuk menanggapi berita dan seruan dari Australia ini. Kalau berita itu tidak benar, perlu sekali ada cara (entah dalam bentuk apa) untuk meyakinkan pendapat umum dunia, bahwa Budiman Sujatmiko dan Dita Indah Sari tidak mengalami penyiksaan serupa itu. Ini penting sekali, untuk mengurangi serangan-serangan, dari dalamnegeri dan terutama dari luarnegeri, mengenai banyaknya pelanggaran HAM di Indonesia. Terutama sekali, pada dewasa ini, ketika sorotan pendapat umum dunia terhadap masalah-masalah Indonesia sangat tajam, antara lain yang menyangkut kasus 27 Juli, kasus Hadiah Nobel, masalah Timor-Timur, masalah WTO.
>
> Kalau pemerintah Indonesia dapat menunjukkan kepada opini umum (di Indonesia dan juga di luarnegeri) bahwa Budiman dan Dita Sari diperlakukan secara wajar dan normal menurut ketentuan-ketentuan hukum Indonesia dan norma-norma hukum internasional, ini akan merupakan tindakan yang akan mendapat penghargaan. Sebaliknya, segala bentuk tindakan yang di luar ketentuan-ketentuan hukum, segala macam praktek kesewenang-wenangan dan melanggar HAM akan merupakan noda bagi aparat-aparat pemerintah Indonesia, bagi Orde Baru yang berlandaskan Pancasila, dan juga bagi martabat Pak Harto sendiri sebagai kepala negara.
>
> Sebab, dalam perkembangan situasi di dalamnegeri, dan dengan kemajuan teknik komunikasi (antara lain lewat Internet, fax dan kemudahan menggunakan telpon), masalah-masalah sekitar Budiman dan Dita Sari ini tidak dapat diremehkan oleh pemerintah Indonesia. Apa saja yang sudah terjadi dan apa yang akan terjadi dengan mereka, akhirnya akan diketahui juga oleh umum. Ini semua akan merupakan ukuran tentang pelaksanaan hukum di Indonesia, akan merupakan barometer penerapan jiwa Pancasila dalam praktek pemerintahan.
>
> Bapak Presiden, seandainya berita tentang penganiayaan oleh BIA atau oleh aparat-aparat pemerintahan lainnya itu benar, seyogyanyalah bahwa Bapak sebagai kepala negara dan Panglima Tertinggi ABRI menurunkan perintah yang tegas dan hukuman yang setimpal kepada pelaku-pelakunya. Sebab, sudah jelaslah bahwa praktek ini mencemarkan citra aparat negara, dan menimbulkan keraguan banyak orang tentang kebersihan pemerintahan yang Bapak pimpin. Adalah akan baik sekali bagi suasana di tanahair kita ini, kalau Bapak memperlihatkan ketidakpersetujuan Bapak terhadap praktek-praktek yang membikin noda yang tercela semacam ini.
>
> Seperti kita semua mengetahuinya, praktek penganiayaan, penyalahgunaan kekuasaan, tindakan se-wenang-wenang ini sudah terjadi selama ber-puluh-puluh tahun, di banyak tempat negeri kita, dan dilakukan oleh berbagai aparat dari macam-macam tingkat. Kita sudah mendengar cerita yang macam-macam tentang apa yang dilakukan oleh aparat-aparat Kodim, Korem dan Kodam, dan juga praktek apara-aparat kepolisian dari berbagai tingkat. Ini semua sudah menjadi rahasia umum di negeri kita.
>
> Ada aparat-aparat, yang karena kesetiaannya kepada Orde Baru atau kepada kepemimpinan Bapak, yang telah melakukan tugas mereka ber-lebih-lebihan, sehingga melewati batas-batas hukum. Ada pula yang melakukan pelanggaran-pelanggaraan karena mereka memang melakukan kejahatan dan tindakan kriminil se-mata-mata. Umpamanya, karena masalah uang. Dan ini tidak sedikit jumlahnya.
>
> Apa yang terjadi di sekitar kasus Marsinah, Hendrik Dikson Sirat, Fuad Muhammad Syafrudin (wartawan Bernas), Cece Bin Tadjudin (Bogor), adalah hanya secuwil kecil dari begitu banyak kasus-kasus serupa yang telah disiarkan dalam suratkabar. Kasus-kasus itu menunjukkan bahwa banyak pelanggaran-pelanggaran yang serius yang telah dilakukan oleh sebagian dari aparat pemerintahan. Dan ini telah menjadi pembicaraan di warung-warung kopi, dalam percakapan antara teman-teman. Orang bicara di mana-mana soal suapan, uang pelicin, atau "semua bisa diatur".
>
> Bapak Presiden, kami melihat sendiri dan juga mendengar bahwa sebenarnya citra sebagian dari aparat pemerintahan (militer, kepolisian, kejaksaan, pengadilan, imigrasi, pajak) sudah jelek atau bobrok di mata banyak orang. Sudah tentu, ini tidak semuanya. Sebab, masih ada orang-orang yang masih mau bersikap jujur dan bersih dalam menunaikan tugas mereka. Tetapi, kebudayaan korupsi dan kolusi ini sudah begitu merajalela di negeri kita. Dan penyalahgunaan kekuasaan dengan kekerasan atau pemerasan (secara halus maupun kasar) sudah menjamur di banyak tempat, termasuk masalah penganiayaan yang terang-terangan melanggar hukum dan HAM.
>
> Karena hal-hal tersebut di atas, untuk menjaga nama Republik Indonesia, kami berpendapat bahwa Bapak Presiden perlu dengan tegas dan jelas melarang praktek-praktek penganiayaan aparat negara terhadap tahanan-tahanan atau pun terhadap orang-orang yang disangka atau dituduh melakukan pelanggaran hukum negeri kita. Larangan ini perlu dilanjuti dengan hukuman atau sanksi yang berat.
>
> Sebab, kalau Bapak Presiden membiarkan praktek-praktek semacam ini berlangsung terus, maka orang bisa ber-tanya-tanya, apakah Bapak tidak mengetahui kejadian-kejadian semacam ini, atau memang ini dilakukan dengan pengetahuan dan persetujuan Bapak. Juga orang bisa ber-tanya-tanya apakah Bapak melindungi pelaku-pelaku itu.
>
> Ketegasan Bapak akan menghilangkan kesan bahwa Bapak tidak peduli atau menutup mata terhadap kebobrokan mental pejabat atau aparat, justru se-mata-mata karena Bapak membutuhkan dukungan mereka. Kesan umum semacam ini yang sekarang sudah berjangkit di mana-mana akan merugikan pemerintahan Orde Baru. Pada akhirnya, kesan negatif ini bisa mengarah kepada Bapak sendiri, sebagai orang yang paling bertanggungjawab atas baik-buruknya citra Republik Indonesia dan kebersihan jalannya pemerintahan.
>
> Akhir-akhir ini pers dunia banyak muat artikel atau komentar-komentar mengenai macam-macam soal yang terjadi Indonesia. Selama 30 tahun ini, belum pernah Indonesia mendapat sorotan opini dunia dalam skala yang begitu besar. Dan, sayangnya, sorotan ini tidak selalu dengan kacamata yang positif. Banyak tulisan atau komentar yang negatif mengenai pelaksanaan HAM dan demokrasi, dan juga mengenai masalah penanganan soal-soal Timor-Timur. Jadi, jelaslah bahwa apa yang terjadi di Indonesia akan bisa saja diikuti oleh mata dunia. Praktek penganiayaan oleh aparat-aparat pemerintahan Orde Baru, atau pelanggaran hak-hak demokrasi dan HAM, akan merugikan kita semua.
>
> Bapak Presiden, kami mohon dengan hormat, sudilah kiranya Bapak memerintahkan badan atau jawatan yang berkaitan dengan pemeriksaan Budiman dan Dita Sari (dan juga tahanan-tahanan lainnya) untuk membantah berita tentang penganiayaan terhadap mereka, dengan membuktikannya di depan umum. Seandainya berita tentang penganiayaan itu benar, maka perintah Bapak untuk memberikan hukum yang setimpal akan meyakinkan kepada umum (baik di dalamnegeri maupun di luarnegeri) bahwa Bapak memegang teguh prinsip-prinsip yang benar dan sehat dalam memimpin negara dan pemerintahan. Juga mohon kepada Bapak hendaknya dengan tegas mengumumkan larangan terhadap praktek-praktek biadab yang dilakukan oleh aparat-aparat pemerintahan, yang semuanya ini bertentangan dengan Pancasila yang sering Bapak ucapkan dalam petuah-petuah atau petunjuk-petunjuk Bapak selama ini.
>
> Hormat saya
> Yoso Sanyoto


Untuk jelasnya persoalan, di bawah ini saya lampirkan berita di Internet yang telah disiarkan tanggal 31 Oktober dari Australia :


> From: achis@igc.apc.org (Alex Chis & Claudette Begin)
> From: asiet <asiet@peg.apc.org>
> Subject: PRD LEADERS TORTURE REPORT
> Date: Thu, 31 Oct 1996 03:27 AEST
>
> BUDIMAN AND DITA TORTURED: FINGER NAILS EXTRACTED
>
> Dear friends

> Today October 30 we have received the following message from activists in the Peoples Democratic Party (PRD) in Indonesia.

> "Dita Indah Sari (president of the Indonesian Centre for Labour Struggles) was taken by BIA (military intelligence) from the Suravya Attorney General's detention centre. This time she was tortured more severely. Her finger nails were pulled off because she refused to answer wuestions in the interrogation about subversion charges. Budiman Sujatmiko (president of the PRD), whose family was also falsely accused of being communist by General Syawran Hamid, has just experienced the same thing. He was taken by BIA from the Attorney-General detention centre in Jakarta. His finger nails were also pulled out during his interrogation."
>
>  On behalf of ASIET, we appeal to you to use your connections to:
>
> (a) get your government officials to raise this with Indonesian government officials as soon as possible;
> (b) for your group, parliamentarians, or other personbalities to PUBLICLY seek assurances from the Indonesian government that these reports are untrue and to produce Budiman and Dita in public now to prove that this is not the case.
>
> If these reports are true, it is further indication of the barbaric character of the Indonesian military's methods of repression against their citizens. It also represents an escalation in the visciousness of the attacks against democratic activists. Please inform us as soon as possible of what action you intend to take,
>
> In solidarity,
>
> Max Lane
> National co-ordinator.

(Mohon kepada para pembaca tulisan ini untuk dapat menyampaikannya lewat E-mail, fax dll, ke alamat-alamat di Indonesia, dan ke alamat para pembesar Indonesia, termasuk kepada Pak Harton sendiri).
