---
layout: post
title: Peristiwa 65 pelanggaran berat HAM
date: 2003-07-01
---

Berbagai pendapat Prof. DR. Said Aqiel Siradj MA yang diungkapkan dalam tulisan di majalah RUAS edisi VIII th II, 2003 sangat penting untuk mendapat perhatian dari sebanyak mungkin orang, baik di kalangan Islam di Indonesia, maupun yang di luar kalangan Islam. Sebab, berbagai pendapatnya itu diutarakannya sebagai tokoh penting dalam NU (Ketua Pengurus Besar) dan mantan anggota Komnas Ham dan sarjana Islam terkemuka.

Seperti yang bisa disimak dalam tulisan yang berjudul “NU akan terbuka atas penyidikan kembali tragedi ’65-‘66 “ (harap baca kembali tulisannya selengkapnya di website http://perso.club-internet.fr/kontak/ ) ia mengutarakan pendapatnya bahwa (saya kutip) :” peristiwa ’65 itu ada faktor eskternal dan internalnya. Faktor eksternal tak bisa lepas dari ketegangan politik perang dingin dua negara adikuasa. Jelas dalam peristiwa itu ada rekayasa Amerika. Prolog peristiwa itu sendiri sudah mulai matang sebelum Amerika” masuk”. Sementara faktor internalnya ada perebutan kekuasaan . Presiden Soekarno jatuh dan presiden Soeharto menjadi presiden dengan rezim Orde Baru nya »

Dalam tulisan kali ini tidak (atau belum) dibahas tentang faktor eksternal dan internal yang berkaitan dengan G30S, termasuk masalah peran Bung Karno dan PKI, atau faktor CIA, TNI-AD dan lain-lain Tetapi, lebih banyak disoroti arti penting pesan yang terkandung di dalam tulisan itu untuk kalangan Islam, terutama tentang sikap terhadap kaum komunis di Indonesia. Pesannya ini, seperti halnya pesan politik dan pesan moral Gus Dur dan tokoh-tokoh NU lainnya (termasuk kalangan muda NU) merupakan angin segar yang menyejukkan bagi kehidupan bermasyarakat dan bagi suasana politik di Indonesia, terutama sesudah jatuhnya rezim militer Suharto lima tahun yang lalu.

## Orde Baru Memupuk Sentimen Anti-Komunis

Seperti sudah sama-sama kita saksikan, selama lebih dari 30 tahun, rezim militer Suharto telah menyiksa – dalam berbagai bentuk dan cara - puluhan juta orang warganegara. Mereka ini terdiri dari pendukung atau simpatisan politik Presiden Sukarno dan simpatisan PKI yang berasal dari bermacam-macam suku, ras, agama atau aliran politik. Kampanye anti-komunis telah dilancarkan berpuluh-puluh tahun oleh Orde Baru secara besar-besaran dan secara bengis, yang jarang tandingannya di dunia.

Begitu intensifnya kampanye ini, sehingga dampaknya sangat luas dan mendalam dalam masyarakat, termasuk di kalangan Islam. Rezim militer Suharto dkk telah menyemaikan dan menyuburkan sentimen anti-komunis di kalangan Islam, sesuai dengan kepentingan Orde Baru sendiri, yaitu mempertahankan kekuasaan, yang berhasil direbutnya dari tangan Presiden Sukarno. Penyebaran sentimen anti-komunis (dan anti-Sukarno) di kalangan Islam ini merupakan siasat besar Orde Baru mengingat kedudukan yang penting faktor Islam di Indonesia.

Dari segi inilah kita bisa lihat pentingnya pendapat Prof. Dr Aqiel Siradj, yang menyatakan :” Setahu saya, versi NU mengenai peristiwa ’65 itu dulu sampai sekarang tidak tunggal. Banyak versinya sesuai kesaksian masing masing tokoh NU. Kalau kita melihat Gus Dur misalnya, sewaktu jadi presiden ia sudah mohon maaf atas keterlibatan NU waktu itu. Ini karena dia humanis. Dalam hal ini, saya setuju dengan pandangan Gus Dur.

## Komisi Penyidikan Keterlibatan Suharto

“Saya mendukung apa yang dilakukan oleh Komnas HAM yang membentuk komisi penyidikan keterlibatan Soeharto pada tragedi peristiwa “65. Namun harus mempertimbangkan kondisi waktu itu. Tidak boleh semuanya digebyah uyah. Kita sebagai warga NU harus arief melihat persoalan ini, Peristiwa ’65 memang tragedi berdarah yang korbannya paling banyak PKI. Tetapi peristiwa sebelumnya juga dilihat », tegasnya.

Tentang pembunuhan orang-orang komunis ia mengatakan : « Dalam kacamata apapun , hukumnya dosa, membunuh orang tanpa melalui pengadilan. Apalagi hanya karena PKI maka mesti dibunuh itu dosa. Saya dukung sepenuhnya Komnas HAM yang akan mengangkat kembali pelanggaran berat HAM tahun ’65 itu. Tapi hendaknya kembali dengan semangat yang arief dan objektif. Semua bertanggungjawab, Penguasa atau pelaku, pertanggungjawabannya kan mesti beda ».

Tentang rekonsiliasi antara antara eks-tapol dengan kalangan Islam, dikatakannya : « Rekonsiliasi eks tapol PKI dengan kelompok Islam selain NU bisa di lakukan. Saya kira Muhammadiyyah masih bisa diajak bicara. Yang memahami Islam secara benar, masih bisa diajak berdialog. Kalau yang tidak punya nalar, memang sulit. »

## Rekonsiliasi Dengan Eks-Tapol

Pernyataan yang demikian penting ini perlu diketahui oleh banyak orang, yang menunjukkan bahwa berlainan dengan sebagian kalangan Islam yang selama ini menyokong politik dan kebudayaan berfikir rezim militer Orde Baru, ada kalangan Islam lainnya yang menganggap perlu adanya rekonsiliasi dengan para eks-tapol. Rekonsiliasi dengan para eks-tapol adalah bagian atau langkah penting bagi terselenggaranya rekonsiliasi nasional. Dan rekonsiliasi nasional adalah amat diperlukan untuk menyehatkan demokrasi di Indonesia dan memperkuat persatuan bangsa.

Suara dari kalangan NU yang mengumandangkan pandangan yang humanis dan berwawasan luas ini akan memberikan sumbangan penting bagi pencerahan bangsa pada umumnya dan kalangan Islam pada khususnya. Dengan memancarkan pandangan-pandangan yang menyejukkan hati banyak orang inilah kalangan Islam di Indonesia akan memainkan peran yang lebih positif lagi bagi kehidupan bangsa dan negara.

Sudah waktunya bagi berbagai kalangan Islam di Indonesia untuk meninggalkan sama sekali keterkaitannya dengan kebiasaan atau kebudayaan berfikir Orde Baru.

## Apa G30S Karena Ketololan PKI Dan Sukarno?

Mengenai peristiwa ’65, rupanya bukan hanya di kalangan NU saja yang tidak mempunyai versi yang tunggal, tetapi juga kalangan-kalangan lainnya. Selama ini sudah banyak tulisan mengenai peristiwa itu, baik yang dibuat oleh berbagai kalangan di Indonesia maupun oleh para ahli dan pengamat asing. Namun, pendapat atau pandangan berbagai kalangan itu (baik yang di Indonesia maupun di luarnegeri) tentang masalah G30S masih berbeda-beda bahkan ada yang saling bertentangan.

Sudah makin banyak dan beraneka-ragam pula bahan; informasi, atau data yang disajikan kepada umum selama ini dari sumber orang-orang Indonesia (antara lain : Omar Dani, Tumakaka, Subandrio; Manai Sophiaan, Kolonel A. Latief, Roeslan Abdulgani, mantan menteri Sutomo, Setiadi, Siauw Giok Tjhan, Oei Tjoe Tat, Heru Atmodjo, Sumarsono, Pramoedya Ananta Toer, A. Karim DP, Jusuf Isak, dan masih banyak lagi nama-nama lainnya yang terlalu panjang untuk disebut). Di samping itu, tidak sedikit buku yang sudah diterbitkan oleh wartawan, sejarawan atau ahli-ahli asing mengenai Bung Karno, PKI, G30S, Suharto dan rezim militer Orde Baru. Untuk sekadar menyebutkan sejumlah kecil di antaranya adalah : Bob Hering, Peter Dale Scott, Noam Chomsky, Ben Anderson, Joop Morien, Daniel Lev, John D. Legge, Prof. Wertheim, Audrey dan George Kahin, Harold Crouch, Lambert Giebels, Cindy Adams.

Walaupun sudah banyak buku yang beredar tentang Sukarno, G30S, PKI, CIA, Suharto dan Orde Baru, bangsa kita masih terus perlu terus-menerus mempelajari sejarah bangsa di masa-masa yang lalu guna membangun hari depan yang lebih baik bagi rakyat. Untuk itu berbagai macam tulisan, buku, diskusi, atau perdebatan tentang soal-soal besar bangsa ini, senantiasa amat diperlukan sebagai pendidikan politik dan peradaban bangsa, terutama bagi generasi muda kita. Umpamanya: siapakah dan apakah Sukarno itu bagi bangsa Indonesia ? Bagaimana kita patut menilai Sukarno ? Apa-apa sajakah segi-segi positif dan negatif sosok Sukarno bagi bangsa ?

Mengenai G30S banyak pula aspek-aspek yang bisa dan perlu terus-menerus dibahas. Apa itu sebenarnya G30S? Adakah peran Bung Karno dan PKI dalam G30S? Sampai di manakah keterlibatan CIA dalam usaha menggulingkan kekuasaan politik Sukarno? Bagaimana Suharto menghadapi presiden Sukarno dan G30S? Mengapa Orde Baru begitu anti Sukarno dan anti-PKI? Apa G30S itu terjadi karena ketololan PKI dan Sukarno?

## Yang Membela Politik Suharto

Mungkin saja, bahwa sebagian dari persoalan atau pertanyaan itu tidak akan segera mendapatkan penjelasan atau jawaban, atau bahwa sebagian lagi akan tetap menjadi misteri sejarah. Barangkali banyak hal akan menjadi lebih jelas kalau di masa datang arsip militer (misalnya arsip Teperpu atau arsip negara lainnya yang bersifat rahasia) tentang periode sebelum, selama dan sesudah G30S dinyatakan terbuka untuk umum.

Meskipun banyak hal tentang G30S yang diajukan berbagai fihak masih bisa dipertanyakan atau diperdebatkan kebenarannya, satu hal sudah jelas bagi makin banyak orang, yaitu bahwa Suharto dkk sudah melakukan kejahatan peri-kemanusiaan terhadap puluhan juta orang. Suharto dkk bukan saja sudah membunuhi jutaan anggota dan simpatisan PKI – dan banyak simpatisan Bung Karno - yang tidak bersalah apa-apa, tetapi sudah pula mengkhianati Bung Karno dan membunuhnya secara politik dan secara fisik. Suharto dkk dengan rezim militernya yang bernama Orde Baru (harap dicatat bahwa fasisme Hitler yang anti-komunis juga suka memakai istilah Orde Baru) telah mencekik kehidupan demokratis sesudah membasmi PKI secara biadab.

Berbagai pelanggaran, penindasan dan kejahatan yang sudah dilakukan oleh rezim militer Suharto dkk selama lebih dari 30 tahun ini sudah terlalu banyak dan juga terlalu jelas bagi banyak orang. Begitu jelasnya sehingga kalau sekarang ada orang yang masih berani terang-terangan membela atau membenarkan politik Suharto dkk maka dianggap sebagai orang aneh atau tidak sehat nalarnya. Atau jadi bahan tertawaan.

Seperti sudah dibuktikan oleh sejarah, Suharto bukanlah penyelamat Republik Indonesia dan bangsa Indonesia, tetapi perusak Republik Indonesia. Titik.
