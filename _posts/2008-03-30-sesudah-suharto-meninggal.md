---
layout: post
title: Sesudah Suharto meninggal
date: 2008-03-30
---

Jawapos, 30 Maret 2008,

Rumah Jalan Cendana 8 setelah Vonis Bebas

Anak-Anak Soeharto
Tanpa Penghuni, Ruang Dapur Tak Berasap Lagi


Pengadilan Negeri Jakarta Selatan Kamis (27/3) lalu memutus bebas anak-anak Soeharto. Mereka lolos karena sang ayah dianggap tidak bersalah dalam kasus korupsi Yayasan Supersemar. Namun, putusan itu tak direspons dengan suka cita oleh keluarga besar yang tinggal di Jalan Cendana.

RIDLWAN HABIB-M. NAUFAL., Jakarta

DITEMANI sebungkus rokok kretek, segelas kopi, dan koran terbitan ibu kota, Sarmada, Jumat (28/3) siang lalu tampak menikmati harinya. Terik matahari yang menyengat tak membuatnya gerah. Berkeringat pun tidak. Radio kecil yang menemaninya bekerja sedang mengalunkan lagu keroncong.

"Sejak peringatan 40 hari (meninggalnya Soeharto) rumah di sini semakin sepi. Hampir tidak ada tamu," ujar penjaga rumah Jalan Cendana 8 itu kepada Jawa Pos.

Pelataran rumah warna hijau pupus di kawasan elite Menteng, Jakarta Pusat, itu tampak bersih. Seorang rekan kerja Sarmada, Sriyono, tukang sapu Cendana, sedang mengumpulkan daun-daun pohon mahoni yang berguguran. Ayam jago merah di sangkar hijau yang terletak di tengah halaman berkokok berkali-kali, tak peduli pagi sudah lama berlalu.

Cendana 8 adalah episentrum yang menjadi simbol kekuatan Soeharto semasa berkuasa. Meski diberi jatah Istana Kepresidenan, penguasa 32 tahun itu lebih memilih tinggal di rumah sendiri. Termasuk untuk menerima tamu-tamu istimewa. Di rumah sederhana itu, Bu Tien Soeharto mengumpulkan koleksi suaminya. Pemberian tamu-tamu negara. Saking banyaknya, Bu Tien sampai mendirikan Museum Dharma Pertiwi di TMII.

Di sekitar rumah Cendana 8 itu pula anak-anak Soeharto tinggal. Hardijanti Hastuti Rukmana (Mbak Tutut) di Jalan Yusuf Adiwinata 14; Tommy Soeharto di Jalan Yusuf Adiwinata No 4. Di jalan yang sama pula, kakak Tommy, Sigit Harjojudanto, tinggal. Lalu, Siti Hutami Endang Adiningsih Pratikto (Mamiek) di Jalan Cendana 17, serta Bambang Trihatmodjo di Jalan Tanjung 23.

Sampai Soeharto menjelang dikebumikan di makam keluarga akhir Januari 2008 lalu di Matesih, Karanganyar, Jawa Tengah, kawasan Jalan Cendana yang panjangnya 500 meter itu dijaga ketat petugas keamanan. Polisi memasang penghalang di kedua ujungnya: Jalan Rasamala dan Jalan Tanjung. Hanya kendaraan milik warga Jalan Cendana yang bisa keluar-masuk.

Namun, sepeninggal Soeharto, penjagaan itu sudah tak ada lagi. Jalan itu kini dibuka untuk umum. Bahkan, ada kesan rumah Cendana itu sudah kehilangan magnetnya.

"Sekarang rumah kosong, suwung, hanya pembantu. Itu pun kalau malam pulang," kata Sarmada yang mengaku baru tiga tahun mengabdi di Cendana. Karena dapur di rumah itu sudah tak mengepul, untuk kebutuhan makan minum sehari-hari mereka memesan dari luar.

"Kalau dulu dimasak sendiri. Sekarang lebih sering pakai katering, " ujar Sarmada yang memakai baju satpam putih biru lengkap.

Sarmada membolehkan Jawa Pos mengambil gambar dari halaman rumah. Hanya, ketika beranjak lebih ke dalam, dia tidak memperbolehkannya. "Jangan ya Mas, saya nanti dimarahi," katanya.

Siang itu Sarmada ditemani Eman. Sejawatnya ini tampak lebih pendiam. Bahkan, terkesan takut kepada wartawan. "Sudah ya Mas, sudah ya Mas," ujarnya berulang-ulang sambil menyenggol lengan rekannya.

Bagaimana putra-putri Soeharto? Menurut Sarmada, sekarang mereka lebih banyak beraktivitas di rumah masing-masing. "Coba saja ke rumah Bu Tutut di belakang. Kalau Mas nunggu di sini, percuma," katanya seperti mengusir.

Lima menit berjalan kaki dari Cendana 8, Jawa Pos sampai di rumah tingkat dua berpagar putih di Jalan Yusuf Adiwinata 14. Sebuah mobil Honda CRV hitam berpelat B 1 TE terparkir di depan garasi.

Ada satu pos satpam berukuran 3 x 2 meter di kiri pagar. Fondasi pos itu dibuat lebih tinggi setengah meter agar pandangan bisa luas sampai ujung jalan. "Ibu (Mbak Tutut) sedang berlibur ke luar kota, Mas," ujar Husen, penjaga rumah. Putri sulung Soeharto itu pergi bersama suami dan kedua anaknya. "Sejak tiga hari lalu," tambah Husen.

Husen yang juga merangkap petugas satpam Masjid Agung At Tiin di Taman Mini Indonesia Indah itu sedang berjaga sendirian. Dia ditemani empat orang berbaju safari layaknya ajudan. Seragam seperti umum dipakai para "centeng" di kawasan Menteng yang rata-rata tampil rapi. Mereka duduk-duduk di depan kios rokok di samping rumah. "Tolong identitasnya," pintanya sambil mengajak Jawa Pos masuk ke pos jaga.

Dia menjelaskan prosedur yang harus dilalui jika ingin bertemu sang nyonya rumah. "Harus ada surat resmi dulu masuk ke sini. Nanti sekretaris Ibu akan membacanya. Kalau Ibu setuju, seminggu atau dua minggu akan ditanggapi," katanya.

Dia juga melarang Jawa Pos mengambil gambar. "Jangan, ini rumah orang. Tolong hormati," katanya dengan nada meninggi. Empat temannya yang asyik mengobrol bangkit berdiri.

"Nanti, kalau ada apa-apa, saya yang (dimintai) tanggung jawab," tambahnya sembari meminta dua kartu nama. Husen lalu menandatangani salah satunya. "Ini kalau Mas perlu bukti. Di situ ada tanda tangan saya dan jam Anda berkunjung ke sini. Tidak perlu foto-foto segala," ujarnya.

Ke mana Mbak Tutut? Husen menggeleng. "Pokoknya, bawa suratnya dulu ke sini kalau mau cari informasi," katanya sambil beralasan prosedur itu berlaku untuk semua tamu tanpa kecuali.

Penjaga warung telepon "Kalam Haq" yang terletak tepat di seberang rumah Tutut punya cerita lain. "Saya dengar Mbak Tutut ke luar negeri sama anak-anaknya," kata wanita yang minta dipanggil Bu Sholeh itu.

Dia mengaku masih sering melihat keluarga Soeharto datang ke rumah wanita kelahiran Jogjakarta 23 Januari 1949 itu. "Tapi, biasanya malam. habis magrib. Kalau siang, saya jarang mergoki," katanya.

Dihubungi secara terpisah pengacara keluarga Soeharto, Juan Felix Tampubolon, mengatakan, seluruh ahli waris Soeharto lega atas keputusan bebas majelis hakim. "Itu keyakinan sejak awal putra-putri almarhum kalau ayahandanya tidak pernah bersalah," katanya.

Berbekal keyakinan itulah putra-putri Soeharto menunjuk kuasa hukum. "Hasilnya (putusan PN Jakarta Selatan) sudah sesuai dengan harapan," katanya.

Juan Felix membenarkan, setelah Soeharto mangkat, rumah tabon (induk) Cendana 8 memang dibiarkan tanpa penghuni. "Jelas masih kosong, keluarga belum memutuskan kelanjutannya," katanya. (el)


* * *
SEMBAKO, HARTA DAN HUTANG SUHARTO

Oleh: Yoseph Tugio Taher



"Keluarga mendiang mantan Presiden Soeharto membagi-bagikan paket sembako di sejumlah tempat seperti Ndalem Kalitan, Astana Giribangun Karanganyar, Momumen Tien Soeharto dan rumah dinas Bupati Wonogiri, Sabtu (2/2).

Sebanyak 3.000 paket sembako dibagikan kepada warga yang ikut membantu keluarga dengan mengikuti tahlilan hingga tujuh hari kematian Soeharto”, menurut Tempo Interaktif Solo. 2 Pebr. 08.

“Oh, alangkah bagusnya. Alangkah indah dan mulia hatinya, “memberi makan” rakyat miskin yang memang hidup serba kekurangan, melarat dan kelaparan”, selintas tentu kita akan berpikir begitu. Namun, tunggu dulu!

Menurut pembantu Rumah Tangga Kalitan, Edy Woro Seyanto paket sembako yang masing-masing bernilai Rp 75 ribu itu dibagikan kepada warga yang ikut membantu keluarga dengan mengikuti tahlilan hingga tujuh hari kematian Soeharto.


Ohh……, rupanya, ini adalah “upah”! Upah karena ikut tahlilan hingga tujuh hari kematian Soeharto. Jadi bukannya memberi gratis kepada rakyat miskin!

Namun, menurut berita my RMnews Solo, “pembagian sembako dirumah keluarga Soeharto itu, diwarnai kericuhan, sebab, “rakyat” yang dibiarkan menunggu, antre berjam-jam, walapun mereka memegang girik (kupon) tidak dibenarkan masuk bahkan pintu gerbang ditutup oleh Petugas Keamanan Dalem Kalitan”.

Dan istimewanya, “Warga yang mendapat sembako kebanyakan mereka yang tergolong mampu. Karena girik itu kami bagikan kepada warga yang datang tahlilan semalam (1/2). Jadi tidak dipilih-pilih," terang Edy Woro Setyanto, pembantu rumah tangga Dalem Kalitan, di sela-sela pembagian sembako”, seperti yang diberitakan my RMnews Solo.

Nah, jadi pembagian sembako itu bukan kepada rakyat biasa yang miskin, lapar dan sengsara., namun kepada mereka yang tergolong mampu! Ini berarti kroni-kroni Soeharto yang menangisi kepergiannya dan tahlilan sampai 7 malam. Mereka mendapat sembako Rp. 75 ribu, kalau uang sebanyak itu dibelikan beras akan bisa dapat kira-kira 15 kilo!

Dengan demikian apakah keluarga Cendana itu bisa dikatakan telah mendermakan harta kekayaannya untuk orang miskin? Memperhatikan nasib bangsa yang kere dan sengsara? O, tidak sama sekali. Apa yang mereka lakukan itu, hanya ibarat menjatuhkan rimah-rimah makanan di bawah meja, dimana coro dan semut yang kelaparan berebutan untuk mendapatkannya. Apa yang dilakukannya itu hanyalah untuk menutupi, supaya orang tidak melihat lebih jauh, menyelidiki lebih jauh akan kekayaan dan harta keluarga Soeharto yang diperolehnya selama dia berkuasa. Supaya orang lupa akan uang puluhan ribu yang diuntil-until, dijadikan kembang untuk kado kroni-kroninya Soeharto ketika perkawinan cucunya! Begitulah “kebaikan” Soeharto dan keluarganya!

Begitu juga, ketika Soeharto sakit dan meninggal banyak tokoh, pemimpin Negara-negara tetangga yang datang menjenguk dan memberi rekomendasi akan jasa, kebaikan, kehebatan, dan hubungan erat mereka dengan Soeharto, namun menutupi kebejatan, kebrutalan dan kekejaman Soeharto dalam membunuhi bangsanya, menyiksa dan membunuh pelan-pelan Presiden pertama RI Bung Karno, setelah kedudukannya yang dengan licik dirampok oleh Soeharto. Mengapa semua tokoh luar berdatangan ketika sakit dan meninggalnya Soeharto? Bukan saja disebabkan untuk menjaga hubungan baik dengan Pemerintah Indonesia yang sekarang, yang secara berlebihan dan super istimewa melakukan perawatan dan melaksanakan penguburan Mantan Presiden Soeharto., tapi juga mengingat "kepentingan ekonomi", berhubung dengan adanya sebagian kecil harta kekayaan keluarga Soeharto, yang berada di negara mereka.


Dan catatan “Sebagian Kecil Harta Keluarga Soeharto di luarnegeri” bersama ini saya turunkan, agar bisa diketahui oleh rakyat yang selama ini diperbodoh, diplaster matanya oleh Soeharto dan kroni-kroninya hingga tidak bisa "melihat". Dan juga buat mereka yang mengagungkan dan meng-anggap Soeharto banyak “jasanya”, agar matanya juga bisa melek, bahwa mereka sebenarnya telah dikelabui oleh kepintaran dan kelicikan Soeharto!

Di bawah ini adalah daftar yang saya kutip dari http://kontak.club.fr/index.htm, yang diperoleh dari sumber yang mengumpulkannya yaitu “http://www.hamline.edu/ apakabar/index.html, sebagai berikut:

“Daftar ini baru meliputi sebagian kecil saja kekayaan keluarga besar Suharto berwujud rumah, kawasan perburuan, kapal layar mewah, serta perusahaan properti dan perusahaan tanker yang sebagian atau seluruhnya milik keluarga bekas kepala Negara, ketiga terkaya di dunia. Ini belum lagi saham mereka dalam puluhan perusahaan di luar negeri.


Di Britania Raya (UK)
Lima rumah seharga antara 1-2 juta Poundsterling (1 Poundsterling = Rp 18.000) di London, yang terdiri dari:
· Rumah Sigit Harjojudanto di 8 Winington Road, East Finchley
· Rumah Sigit Harjojudanto di Hyde Park Crescent
· Rumah Siti Hardiyanti Rukmana (Tutut) di daerah Kensington
· Rumah Siti Hediyati Haryanti (Titiek Prabowo) di belakang Kedubes AS di Grosvernor Square
· Rumah Probosutedjo di 38A Putney Hill, Norfolk House, London SW.15/6 AQ : 3 lantai, dengan basement.
(sumber-sumber: Tiara , 5 Desember 1993: 35; Forum Keadilan , 1 Juni 1996: 47; Dewi , Juni 1996; Swa , 19 Juni - 9 Juli 1997: 85; Far Eastern Economic Review , 9 April 1998; mahasiswa Indonesia serta wartawan Inggris dan Indonesia di London dan Jakarta).

Di Amerika Serikat
Dua rumah Dandy N. Rukmana dan Dantu I. Rukmana (anak laki-laki dan anak perempuan Tutut) di Boston, dengan alamat:
· 60 Hubbard Road , Weston, Massachussets (MA) 02193 (sejak Juli 1995)
· 337 Bishops Forest Drive , Waltham , MA 02154 (sejak Februari 1992)
Dua rumah anak-anak Sudwikatmono di:
· H illcrest Drive , Beverly Hills , California ,
· D oheney Drive , Beverly Hills , California


Rumah peristirahatan keluarga Suharto di Hawaii.
(sumber-sumber: Eksekutif , Maret 1990: 133-134; Tiara , 5 Desember 1993: 35; Far Eastern Economic Review , 9 April 1998; Ottawa Citizen , 16 Mei 1998; hasil investigasi aktivis pro-demokrasi Indonesia di AS)


Di Daerah Laut Karibia
Rumah-rumah peristirahatan keluarga Suharto di Kepulauan Bermuda dan Cayman (sumber-sumber: Ottawa Citizen , 16 Mei 1998; Die Welt , 23 Mei 1998)


Di Surinam
Raden Notosoewito, adik tiri Suharto dari Desa Kemusuk, Kabupaten Bantul, D.I. Yogyakarta, adalah ketua Yayasan Kemusuk Somenggalan. Yayasan ini adalah pemegang saham PT Mitra Usaha Sejati Abadi (MUSA), holding company dari satu konglomerat yang punya berbagai bidang usaha di Indonesia (Solo, Yogya, Malang, DKI Jaya), Singapura, Hong Kong, dan Surinam.


Di negeri yang tersebut terakhir itu, Surinam, konglomerat ini pada tahun 1993 mendapat konsesi hutan seluas 150 ribu hektar di Distrik Apura, Surinam bagian Barat. Konsesi itu merupakan awal dari rencana MUSA untuk menanamkan modal sebesar US$ 1,5 milyar, sebagian besar untuk sektor kehutanan. Konsesi hutan ini, serta praktek MUSA Group untuk juga memborong kayu dari daerah di luar konsesinya sendiri, telah mendapatkan serangan dari gerakan lingkungan di mancanegara.


Selain dampak lingkungan dan budayanya yang sangat merusak bagi suku-suku Amerindian Maroon di Distrik Apura, yang juga jadi sorotan adalah bagaimana konsesi itu diperoleh berkat 'diplomasi tingkat tinggi' antara Suharto, sebagai Ketua Gerakan Non-Blok waktu itu, dengan para petinggi Surinam yang keturunan Jawa, khususnya Menteri Sosial Surinam, Willy Sumita. Diplomasi tingkat tinggi, di mana konon uang sogokan sebanyak US$ 9 juta berpindah ke tangan para politisi, dikenal di sana dengan istilah "The Indonesian Connection". Salah satu pendekatan yang dilakukan oleh Yayasan Kemusuk Somenggalan, yang beroperasi di Paramaribo, Ibukota Surinam dengan bantuan Kedubes RI di sana, adalah menawarkan bantuan untuk renovasi Istana Presiden Surinam. Proyek itu ditawarkan untuk diborong oleh anak perusahaan MUSA sendiri. (sumber-sumber: Kompas , 15 Maret 1993, hal. 14 [iklan ucapan selamat atas terpilihnya Suharto dan Tri Sutrisno sebagai Presiden & Wk. Presiden RI]; EIA, 1996: 32; Skephi & IFAW, 1996; Friedland & Pura, 1996; Harrison, 1996; de Wet, 1996; Toni and Forest Monitor, 1997: 26-27, 29-30)

Di Aotearoa (New Zealand)
Kawasan wisata buru seluas 24,000 Ha bernama Lilybank Lodge di kaki Mount Cook dan di tepi Danau Tekapo di Southern Island bernilai NZ$ 6 juta (1 NZ$ = Rp 4000), yang dibeli lisensinya dari Pemerintah NZ oleh Tommy Suharto tahun 1992. (sumber: AFP , 20 Mei 1998; Australian Financial Review , 27 Mei 1998; hompage: www.lilybank.co.nz ; hasil investigasi lapangan G.J. Aditjondro ke Lilybank, bulan Februari 1998).


Di Australia
· Kapal pesiar mewah (luxury cruiser ) milik Tommy Suharto seharga Aust$ 16 juta (1 Aust$ = Rp 5.000), yang diparkir di Cullen Bay Marina di Darwin.
· Merger antara perusahaan iklan ruang asal Melbourne, NLD, dengan kelompok Humpuss milik Tommy & Sigit, tahun 1997, berbarengan dengan pembelian saham perusahaan iklan ruang terbesar di Malaysia, BTABS (BT Advertising Billboard Systems), memberikan Tommy dan partner Australianya, Michael Nettlefold, konsesi atas billboards di sepanjang freeways di Negara Bagian Victoria, Australia, serta sepanjang jalan-jalan toll NLD-Humpuss di Malaysia, Filipina, Burma dan Cina.


· Perjanjian persekutuan strategis (strategic alliance) antara Kelompok Sahid milik Keluarga Sukamdani Gitosarjono dengan Kemayan Hotels and Leisure Ltd., yang ditandatangani bulan Desember 1997, memungkinkan Sahid ikut memiliki 50 hotel milik Park Plaza International (Asia Pacific) di kawasan Asia-Pasifik serta 180 hotel Park Plaza di AS. Dengan demikian, 24 hotel milik kelompok Sahid di Indonesia dan Medinah, Arab Saudi, diganti namanya menjadi Sahid Park Plaza Hotel. Harap diingat bahwa Sukamdani Gitosardjono, sejak 28 Oktober 1968 menjabat sebagai Ketua Harian Yayasan Mangadeg Surakarta, yang didirikan dengan dalih membangun dan mengelola kuburan keluarga besar Suharto. Jadi tidak tertutup kemungkinan, bahwa ekspansi Kelompok Sahid ke Arab Saudi, AS, dan Asia-Pasifik melalui Kelompok Kemayan/Park Plaza ini, juga memperluas sumber pendapatan keluarga Suharto di berbagai negara itu.
(sumber-sumber: Tempo , 3 Desember 1977: 8-9; Info Bisnis , Juli 1994: 9-23; Kontan , 10 Maret 1997; Australian Financial Review , 17 Desember 1997, 13 Maret 1998; Weekend Australian , 10-11 Agustus 1998; Sydney Morning Herald , 17 Agustus 1996, 11 Desember 1997, 6 April 1998; The Suburban , Darwin, 11 Juni 1998; Port Phillip/Caulfield Leader , 22 Juni 1998; sumber-sumber lain).
Di Singapura
· Perusahaan tanker migas milik Bambang Trihatmodjo dkk, Osprey Maritime, yang total memiliki 30 tanker, dengan nilai total di atas US$ 1,5 milyar (US$ 1 = Rp 10.000). Sejak Juni 1996, dua tanker Osprey, yakni Osprey Alyra dan Osprey Altair, dikontrak oleh Saudi Basic Industrial Corporation untuk mengangkut minyak dan produk-produk petrokimia dari Arab Saudi ke mancanegara. Dengan akuisisi perusahaan tanker Norwegia yang terdaftar di Monaco, Gotaas-Larsen, oleh Osprey Maritime yang disepakati bulan Mei 1997, perusahaan milik Bambang Trihatmodjo ini menjadi salah satu maskapai pengangkut migas terbesar di Asia. (sumber-sumber: Economic & Business Review Indonesia , 5 Juni 1996; Asiaweek , 23 Mei 1997: 65; LNG Current News , 13 Februari 1998).
· Perusahaan tanker migas milik Tommy & Sigit, Humpuss Sea Transport Pte. Ltd., adalah anak perusahaan PT Humpuss INtermoda Transport (HIT), yang pada gilirannya adalah bagian dari Humpuss Group. Tapi dengan berbasis di Singapura, perusahaan itu -- yang berpatungan dengan maskapai Jepang, Mitsui O.S.K. Lines -- dapat mengoperasikan ke-13 tanker migas dan LNGnya, lepas dari intervensi Pertamina pasca-Reformasi. Ini setelah berhasil menciptakan reputasi bagi dirinya sendiri berkat kontrak jangka panjangnya dengan Taiwan. Perusahaan Singapura ini pada gilirannya punya anak perusahaan yang berbasis di Panama, First Topaz Inc. (sumber-sumber: Swa , Mei 1991: 45-46; Prospek , 18 Januari 1992: 40-43;Info Bisnis , November 1994: 12; Jakarta Post , 20 November 1997).
Di Malaysia, Filipina, Burma, dan Cina
Di ke-4 negara Asia ini, Siti Hardiyanti Rukmana masih menguasai jalan-jalan tol sebagai berikut :
· 166,34 Km jalan toll antara Wuchuan - Suixi - Xuwen di Cina;
· 83 Km Metro Manila Skyway & Expressway di Luzon, Filipina;
· 22 Km jalan toll antara Ayer Hitam dan Yong Peng Timur, yang merupakan Bagian dari jalan tol Proyek Lebuhraya Utara Selatan sepanjang 512 Km yang menghubungkan Singapura, Johor, sampai ke perbatasan Muangthai di Malaysia;
· ?? Km jalan toll patungan dengan Union of Myanmar Holding Co. di Burma.
(sumber-sumber: Info Bisnis , Juni 1994: 11-12; Swa , 5-18 Juni 1997: 47; AP , 21 Februari 1997; Economic & Business Review Indonesia , 5 Maret 1997: 44).
Sumber :
http://www.hamline.edu/apakabar/index.html dan http://kontak.club.fr/index.htm.


Itulah sekedar daftar “sebagain kecil” kekayaan keluarga Soeharto yang rasanya sangat perlu diketahui oleh rakyat ( dan juga kroni-kroninya yang “nggak tahu” yang cuma meng-agungkan “jasa” Soeharto!). Jadi kalau ada yang bicara soal harta kekayaan mereka di luar negeri, maka itu bukanlah sekedar omongan ngawur! Begitulah keluarga Soeharto!

Nah, Soeharto “besar jasanya”, kepada rakyat ataukah kepada keluarga dan kroninya? Coba timbang sendiri! Mereka telah mendapat “gajah” dari rakyat dan bumi Indonesia, namun mereka hanya memberikan kacang sebagai imbalannya! Begitulah jasa dan praktek Soeharto dan kroninya terhadap rakyat Indonesia!

Ada lagi “jasa” Soeharto yang lain.
“Soeharto meninggalkan utang, bukannya Rp 1.500 triliun akan tetapi Rp 1.800 triliun! Tepatnya 800 miliar dolar AS.

Jika utang tersebut dibagi-bagikan kepada 200 juta penduduk Indonesia, maka setiap kepala dibebani utang Rp 9 juta!


Nah, siapa yang menyebabkan negara ini berutang begitu besar? Tidak lain tidak bukan adalah Soeharto, karena angka itu adalah posisi awal utang Indonesia saat Soeharto dilengserkan pada Mei 1998!


Saat Soeharto turun, akhirnya terungkap sebanyak 30% utang luar negeri itu atau sedikitnya Rp 540 triliun dikorupsi oleh Soeharto dan kroni-kroninya”

Nah kita bisa lihat bagaimana ‘moral” Soeharto, orang yang dianggap ‘bapak pembangunan” Hutang Negara yang mestinya digunakan untuk kesejahteraan rakyat dan Negara, eeee…….taunya masuk kantongnya Soeharto dan kroninya! Lantas, kemudian rakyat yang mesti membayar kembali! Bapak pembangunan apaan seperti ini? Rakyat cuma dikelabui, membangun gedung pencakar langit, namun sekian persen untuk kantongnya Soehato! Inilah “jasa” Soeharto!

Tidak heran kalau pada zamannya Soeharto berkuasa, masih ada anak yang berusia 9 tahun tidak sekolah (film dok. Riding The Tiger), dan masih ada anak yang dimasa kecilnya sering mengalami kehidupan sulit, bahkan acap kali hanya makan “ondo” (umbi beracun yang tumbuh liar di hutan) karena setiap tahun dilanda paceklik.
Disaat memasuki usia sekolah, masuk sekolah dasar yang hanya berdinding bambu, meja bambu, berlantai tanah dan beratap rumbia. Ke sekolah tidak menggunakan alas kaki dan bila hujan menggunakan payung daun pisang. Begitulah “nasib” kanak-kanak dalam zaman pemerintahan Soeharto, sedang kroni-kroninya Soeharto hidup bergfoya dan berlebihan! Tidak heran kalau kemudian Indonesia dilanda kesengsaraan yang hebat, 100 juta anak bangsa menjadi miskin dan 13 juta kanak-kanak kekurangan makan! Begitulah “jasa” yang ditinggalkan Soeharto!
Pepatah mengatakan, “gajah mati meninggalkan gading”, tapi Soeharto mati meninggalkan hutang!


“Kini, setelah 10 tahun sejak diturunkan, (bahkan setelah matinyapun-pen), Soeharto, sebenarnya masih menyusahkan rakyat!. Setiap tahun, negara tetap membayar utang-utang tersebut. Data yang dilansir Bank Indonesia, posisi terakhir utang luar negeri kita adalah 176,55 miliar dolar AS atau Rp 1.589 triliun (kurs Rp 9.000 per dolar AS).
Mungkin, bahwa utang ini tidak berdampak langsung bagi anda. Tapi tahukah anda, lebih dari separuh APBN, dipakai hanya untuk membayar utang-utang tersebut plus bunganya. Artinya, anggaran yang semestinya dipergunakan untuk, misalnya, membangun jalan, memperbaiki gedung sekolah, dan segala fasilitas umum digerus habis oleh utang warisan Soeharto ini! (kutipan artikel dari Ketua Fraksi Kebangkitan Bangsa (FKB) DPR RI Effendi Choirie/http:kontak.club.fr/index.htm)
Supaya rakyat tahu dan melek, betapa “jasa besar” Jenderal Bintang Lima Soeharto dalam merusak ekonomi dan rakyat Indonesia!
Nah, setelah rakyat tahu akan sebagian kecil “dosa besar” Jenderal Bintang Lima Soeharto yang mengibuli rakyat Indonesia itu, tentu rakyat akan heran, geleng-geleng kepala dan berucap: “Masya Allah, Astaga, Busyet. Alaa mak………ataupun mungkin bercarut marut! Bagaimana dengan anda?
“Pak Harto memang orang besar. Tapi jangan lupa, kesalahannya juga besar!” ucap Ketua Fraksi Kebangkitan Bangsa (FKB) DPR RI Effendi Choirie ***


Australia, 7 Pebruari 2008


* * *

Tribun Jabar 4 Februari 2008

1.000 Nisan Korban Soeharto Dipamerkan

Merupakan simbol korban jatuh atas pelanggaran HAM Soeharto.

JAKARTA, TRIBUN - Puluhan aktivis yang tergabung dalam
Kesatuan Rakyat Adili Soeharto (KERAS) menggelar aksi protes atas
pelanggaran hak asazi manusia (HAM) Almarhum Mantan Presiden
Soeharto di Tugu Proklamasi Jakarta, Minggu (3/1).

Aksi itu ditandai dengan memajang 1000 nisan di Tugu
Proklamasi, sejejar satu dengan yang lain menyerupai taman makam
pahlawan (TMP).Nisan sebanyak itu merupakan simbol korban jatuh atas
pelanggaran HAM Soeharto selama menjadi presiden selama 32 tahun.
Misalnya korban tragedi 1965, perang Timor Timur, korban kerusuhan
27 Mei, daerah operasi militer (DOM) Aceh, Tanjungpriok, dan korban
hilang peristiwa lainnya.

"Ini adalah awal dari aksi kami. Kami ingin ada pengadilan in
absentia terhadap Soeharto, juga terhadap para kroninya jangan
dibiarkan begitu saja. Hukum harus ditegakkan dan kami mengandaikan
1000 nisan itu adalah korban Soeharto," kata Borang, Koordinator
Humas KERAS.

Aksi ini juga sebagai bentuk keprihatinan terhadap pengadilan
selama ini. Hukum dinilai masih mengambang karena tidak tuntasnya
pengusutan masalah pelanggaran HAM. Pengadilan belum memberikan rasa
keadilan dan kepuasan bagi korban pelanggaran HAM.

Aksi 1.000 nisan merupakan aksi keempat yang digelar kelompok
yang merupakan gabungan dari 28 organisasi diantaranya organisasi
kemasyarakatan (Ormas), mahasiswa, dan lembaga swadaya masyarakat
(LSM).

Menurut dia, aksi menuntut pengadilan in absentia terhadap
Soeharto dan kroninya akan berlanjut dan Selasa pekan depan akan ada
aksi pengadilan teatrikal di tempat yang sama.
"Selama dua hari dua malam kami akan melaksanakan aksi berbeda
dengan melakukan pengadilan teatrikal di tempat yang sama,"katanya.

Caranya, lanjut dia, dengan membentuk pengadilan teatrikal
layaknya pengadilan di kantor pengadilan negeri. "Ada yang pura-pura
jadi jaksa, hakim, pengacara, dan kami akan hadirkan 20 orang korban
pelanggaran HAM Soeharto,"katanya.

Sebelumnya, KERAS telah melakukan aksi dengan tujuan yang sama
di beberapa tempat. Aksi-aksi tersebut yakni unjuk rasa di RSPP,
tempat Soeharto dirawat, aksi penandatanganan di spanduk berukuran
100 meter di daerah Blok M, dan konvoi motor dari Tugu Proklamasi
menuju Taman Ismail Marzuki (TIM).

* * *
Ber-yoyo ria dengan peradilan

mengenai Soeharto

Oleh : Harry Adinegara



Membikin orang miris, kecewa, sakit hati dan pusing apabila membaca dan mengikuti aksi penuntasan kasus mantam Presiden Suharto.

Pasca Mei 1998 dimana mbah Harto lengser ke-prabon(nyentrik bener ungkapan ini), sudah terindikasi apalagi setelah, mulai dari upaya pertama kira sampai upaya ke tiga, sudah bisa diduga bahwa usaha penuntasan perkara korupsi yang dilakukan oleh Mbah Harto cuman ...window dressing saja.

Inilah namanya ber-yoyo ria dengan peradilan dan hukum.


1 September 1998
Tim Kejaksaan Agung menemukan indikasi penyimpangan penggunaan dana yayasan-yayasan yang dikelola Soeharto, dari anggaran dasar lembaga tersebut.

11 September 1998
Pemerintah Swiss menyatakan bersedia membantu pemerintah RI melacak rekening-rekening Soeharto di luar negeri. Namun belakangan investigasi ini jadi klayapan kemana mana dan luntur niatnya.

29 September 1998
Kejagung membentuk Tim Penyelidik, Peneliti, dan Klarifikasi Harta Kekayaan Soeharto dipimpin Jampidsus Antonius Sujata.

22 Oktober 1998
Andi M Ghalib menyatakan, keputusan presiden yang diterbitkan mantan presiden Soeharto, sudah sah secara hukum. Kesalahan terletak pada pelaksanaannya.

2 Desember 1998
Presiden Habibie mengeluarkan Inpres No 30/1998 tentang pengusutan kekayaan Soeharto.
Karena ini si Habibie sampai di-jootak sama mbah Harto, Mbah Harto ngambek tidak mau bicara ket alone mau ketemu.

9 Desember 1998
Soeharto diperiksa Tim Kejaksaan Agung menyangkut dugaan penyalahgunaan dana sejumlah yayasan, program Mobil Nasional (mobnas), kekayaan di luar negeri, perkebunan, dan peternakan Tapos.

12 Januari 1999
Tim 13 Kejaksaan Agung mengungkapkan, mereka menemukan indikasi unsur perbuatan melawan hukum yang dilakukan Soeharto. Setelah itu Soeharto melalui tujuh yayasan yang dipimpinnya mengembalikan uang negara sebesar Rp 5,7 triliun.
Tertarik aku sama majalah Time yang "ngomong kosong" dan mereka bilang(Time) mbah Harto menyengkelit duit sebesar 15 billion dollar. Darimana tuh angka Rp5,7 trilliun?

27 Mei 1999
Soeharto menyerahkan surat kuasa kepada Kejagung untuk mencari fakta dan data berkaitan dengan simpanan kekayaan di bank-bank luar negeri (Swiss dan Austria) .

30 Mei 1999
Andi Ghalib dan Menteri Kehutanan Muladi berangkat ke Swiss untuk menyelidiki dugaan transfer uang sebesar US$ 9 miliar dan melacak harta Soeharto lainnya. Hasilnya tidak ditemukan simpanan uang Soeharto di bank-bank Swiss dan Austria.
.......Ampun udah ngeluarin duit buat team Ghalib/Muladi hasilnya nol besar. Apa ini hanya buat window dressing saja biar kelihatan kalau peradilan di Indonesia aktip, sampai penggede2 kluyuran ke Swiss cari simpanannya Mbah Harto.

11 Oktober 1999
Kejagung melalui Wakil Jaksa Agung Ismudjoko mengeluarkan Surat Perintah Penghentian Penyidikan (SP3) terhadap kasus Soeharto karena minimnya bukti.
Mulailah yoyo kelihatan aksi proses main yoyo yakni naik turun yang cuman bisa dinikmati, tapi ngak ada gunanya.
6 Desember 1999
Pemerintahan Presiden Abdurrahman Wahid membuka kembali pemeriksaan kekayaan Soeharto. Marzuki Darusman yang ditunjuk sebagai Jaksa Agung langsung mencabut SP3 Soeharto. Pencabutan ini sempat digugat namun ditolak oleh hakim.
Mulai yoyo agak naik dikit tapi kekuatan naiknya cuman dikit bener.

14 Februari 2000
Kejagung memanggil Soeharto guna menjalani pemeriksaan sebagai tersangka tapi tidak hadir dengan alasan sakit. Untuk itu, Jaksa Agung Marzuki Darusman membentuk Tim Medis untuk memeriksa kesehatan Soeharto.
Seneng banget ya kalau bicara soal diri-in team ini team itu, ujung2nya cuman waste of money aja.

31 Maret 2000
Soeharto dinyatakan sebagai tersangka penyalahgunaan uang dana yayasan sosial yang dipimpinnya.
Yoyo kembali naik, bagi yang kurang awas, mereka sudah girang...anggep-nya wah ini Indonesia mulai memasuki era baru, era reformasi dan keadilan, ngak tahunya......

3 April 2000
Tim Pemeriksa Kejagung mendatangi kediaman Soeharto di Jalan Cendana. Baru diajukan dua pertanyaan, tiba-tiba tekanan darah Soeharto naik.
Yoyo-nya anjlok lagi...buset karena mbah Harto dapat high blood pressure.

13 April 2000
Soeharto dinyatakan sebagai tahanan kota sekaligus tahanan rumah. Tahanan kota, coba dia mau melancong ke LN siapa yang berani nyetop dia?

8 Agustus 2000
Kejaksaan Agung menyerahkan berkas perkara ke PN Jakarta Selatan.

31 Agustus 2000
Soeharto tidak hadir dalam sidang pengadilan pertamanya karena sakit.

28 September 2000
Setelah berulang kali gagal menghadirkan terdakwa, majelis hakim yang diketuai Lalu Mariyun menyatakan penuntutan perkara pidana HM Soeharto tidak dapat diterima dan sidang dihentikan.

10 November 2000
PT membatalkan penetapan PN Jaksel tanggal 28 September 2000 yang menyatakan menghentikan sidang dan mengembalikan surat dakwaan kepada JPU. Seoharto kembali dikenai status tahanan kota.

2 Februari 2001
Majelis hakim agung yang diketuai Syafiuddin Kartasasmita dengan hakim anggota Sunu Wahadi dan Artidjo Alkostar, membatalkan putusan PT DKI dan melepaskan status mantan penguasa Orde Baru itu sebagai tahanan kota.

17 Desember 2001
Soeharto masuk rumah sakit karena terserang pneunomia. Alasan ini kemudian digunakan Presiden Megawati untuk memberikan abolisi, namun banyak ditentang. Akhirnya, Mega tidak jadi memberikan abolisi.

4 Mei 2006
Soeharto dirawat di Rumah Sakit Umum Pusat Pertamina (RSPP) untuk kesekian kalinya.

10 Mei 2006
Pemerintah memutuskan menghentikan peradilan perkara dugaan korupsi di tujuh yayasan yang dilakukan oleh Soeharto. Namun, pemerintah masih mencari formulasi yang tepat untuk merumuskan kebijakan tersebut mengingat secara hukum sudah tidak memiliki jalan keluar.

11 Mei 2006
Kejaksaan Agung mencabut status cekal Soeharto.

12 Mei 2006
Kejaksaan Agung mengeluarkan Surat Keputusan Penghentian Penuntutan. SKP3 ini sempat dibatalkan oleh majelis hakim tunggal Andi Samsan Nganro atas gugatan LSM dan korban pelanggaran HAM. Belakangan putusan ini dibatalkan oleh Mahkamah Agung (MA).

9 Agustus 2007
Sidang perdana gugatan perdata terhadap Soeharto disidangkan. Gugatan yang diajukan negara tersebut dilakukan karena secara pidana Soeharto tidak dapat diadili. Sidang atas perkara itu masih berlanjut hingga kini.

Acirulkalam....sekarang Mbah Harto akan dijadikan pahlawan nasional...on what basis I ask myself. Lha gimana sih ini perkara hukumnya saja belum rampung. Lha mbok dirampungin dulu ya, baru di-timbang2 mana yang lebih berat...dosa atau jasa?
Tapi gimana tuh ya kalau orang selain ngegelapin duit dan /plus dituduh sebagai pembunuh massal kemana timbangannya akan ngejomplang ????? Again I ask myself!

Harry Adinegara

Sumber: Dari Berbagai Sumber.

* * *



JAWA POS 1 Februari 2008,

Anak-Anak Pak Harto Tolak Warisi Perkara


JAKARTA - Anak-anak mantan Presiden Soeharto tidak mau mewarisi gugatan perdata Kejaksaan Agung terkait kasus korupsi Yayasan Supersemar. "Yang dapat diwariskan hanya kepemilikan. Apakah itu utang atau piutang," kata salah seorang pengacara keluarga Cendana, O.C. Kaligis, saat ditemui di Pengadilan Negeri (PN) Jakarta Selatan kemarin.

Dia menambahkan, pewarisan gugatan dari (alm) Soeharto kepada enam anaknya tidak diatur dalam perundang-undangan, baik dalam KUH Perdata maupun Kompilasi Hukum Islam. Gugatan, kata Kaligis, tidak termasuk boedel warisan karena tidak dapat dikategorikan di dalam aktiva (aset) maupun passive (liability).

Menurut dia, pendapatnya itu didasarkan pada ketentuan pasal 833 ayat 1 KUH Perdata dan Kompilasi Hukum Islam pasal 171 huruf a yang mengatur tentang pewarisan hak milik seseorang.

Lebih detail lagi, lanjutnya, pada dua pasal tersebut yang dapat diwariskan adalah hak dan kewajiban di bidang hukum kekayaan. "Fungsi mewariskan dari yang bersifat pribadi atau yang bersifat hukum keluarga, misalnya perwalian, tidak dapat beralih," jelasnya.

Ketika dana yang ada di Yayasan Supersemar diindikasikan kuat disalahgunakan sehingga merugikan negara, bukankah itu bisa diartikan bahwa yayasan tersebut telah berutang kepada negara?

Ditanya seperti itu, Kaligis membantah. Menurut dia, yang dimaksud jaksa adalah warisan perkara, bukan kepemilikan atau harta.

Dia lantas mengutip ketentuan pasal 175 Kompilasi Hukum Islam. Sesuai pasal tersebut, kewajiban ahli waris adalah mengurus dan menyelesaikan pemakaman jenazah, menyelesaikan utang-utang berupa pengobatan, perawatan, termasuk kewajiban penagihan piutang, menyelesaikan wasiat pewaris, dan membagi harta warisan di antara ahli waris yang berhak. "Sedangkan warisan perkara tidak diatur," jelas Kaligis.

Ditegaskan, apabila perkara dapat diwariskan, maka setiap orang akan seenaknya menggugat. "Kalau perkara bisa diwarisi, saya gugat saja bapak Anda yang meninggal, kan kemudian bisa diteruskan ke Anda. Masuk akal undang-undang begitu?" tanya Kaligis.

Di tempat terpisah, Koordinator Jaksa Pengacara Negara (JPN) Dachmer Munthe menegaskan, jika enam anak Soeharto enggan mewarisi perkara ayahnya, kejaksaan punya sejumlah langkah. "Apa langkah itu, saya nggak bisa sebut sekarang. Nanti kami akan sebutkan dalam persidangan," jelas Dachmer. Soal berbagai aturan perundang-undangan yang disampaikan Kaligis, Dachmer menegaskan, kejaksaan tetap mendasarkan pada KUH Perdata bahwa sebuah perkara perdata dapat diwariskan kepada ahli warisnya, entah anak atau cucunya.

Sebelumnya, kejaksaan menggugat Soeharto dan Yayasan Supersemar yang pernah dipimpinnya sebesar USD 420 juta dan Rp 185 miliar. Pemerintah juga menuntut ganti rugi imaterial senilai Rp 10 triliun. Soeharto dianggap menyelewengkan sebagian dana yayasan. Dana yang seharusnya untuk membantu pelajar dan mahasiswa yang kurang mampu justru dialirkan ke perusahaan milik keluarga dan kroninya. (agm/kum)

* * *

Sinar Harapan, 31 Januari 2008

Prestasi Soeharto Tak Bisa Pinggirkan
Persoalan Hukum

Oleh
Sihar Ramses Simatupang/Tutut Herlina

Jakarta-Gelar kepahlawanan kepada Soeharto dipandang jelas akan memancing kontroversi di dalam masyarakat karena selama ini pemerintah dipandang belum jelas menempatkan status hukumnya.

Persoalan hukum bagaimanapun bukanlah hal sepele yang bisa dinafikan dengan memori keberhasilan-keberhasilan yang diraih mantan Presiden tersebut. Demikian benang merah pernyataan Bambang Sulistomo, Komisioner Komisi Yudisial (KY) Sukotjo Suparto, dan anggota Dewan Pertimbangan Presiden (Wantimpres) Dr. Sjahrir kepada SH, Kamis (30/1).
“Termasuk status hukum Soeharto berdasarkan data dari Bank Funia harus dijelaskan lebih dalam karena juga akan memancing kontroversi. Karena bila Pak Harto tidak salah, berarti gerakan yang menjatuhkan dia (Pak Harto) pada tahun 1998 itu salah. Kalau di zaman Soekarno itu jelas yang mana yang positif, yang mana negatif,” ujar Ketua Maklumat Pembebasan Rakyat Indonesia Bambang Sulistomo, putra Bung Tomo yang dikenal sebagai pejuang “arek Surabaya”, kepada SH , Rabu (30/1).


Bila Soeharto menjadi pahlawan tentu akibatnya adalah gerakan mahasiswa sebelum turunnya Soeharto dari tahta pemerintahan pun harus ditangkapi. Tentang Bung Tomo yang tidak dimunculkan sebagai pahlawan, Bung Tomo pada tahun 1978 tetap menyuarakan secara kritis dengan bergabung bersama gerakan mahasiswa untuk menentang Orba.
“Barangkali karena kekuasaan saat ini dari dulu sampai sekarang tidak mungkin mengidolakan tokoh kritis karena ayah saya pernah satu tahun bersama mahasiswa tahun 1978 mengkritik Orba,” ujarnya.


Sementara itu, ekonom Dr Sjahrir mengatakan bahwa beda Soeharto dan Soekarno adalah pada masa mantan Presiden Soeharto munculnya kekuatan kapital kuat yang kukuh sehingga media yang revolusioner pun akan berat melawan hujan tembakan uang dari kapital. Perlakuan terhadap Soeharto pun di media massa berbeda dengan Soekarno.
“Kenapa orang bicara Pak Harto karena Pak Harto satu-satunya yang berkuasa selama hidupnya, 32 tahun. Sehingga memaafkan adalah satu masalah namun hukum adalah masalah lain. Yang kita lupakan adalah fenomena korupsi, soal HAM. Pembangunan yang dengan tak menghitung hancurnya ekonomi 1998. Beliau (mantan Presiden Soeharto) merampok satu setengah generasi,” kata Syahrir.

Usut Kroni
Di kesempatan sama, Komisioner Komisi Yudisial (KY) Soekotjo Soeparto mengatakan bahwa reformasi hukum perlu dilakukan karena ketidakjelasan politik, termasuk pada kegamangan fungsi DPRD, banyaknya perundang-undangan yang tak optimal. Akibat hal ini, persoalan hukum Soeharto seolah terkesempingkan.


“Perlu ada prioritas penyelesaian korupsi yang diambil lembaga penegak korupsi terhadap orang yang selama ini seolah tak tersentuh korupsi pada penguasa atau mantan penguasa, tentara atau mantan tentara juga pengusaha,” katanya.


Sementara itu, Fraksi Partai Amanat Nasional (PAN) mendesak pemerintah untuk melakukan pengusutan terhadap keluarga dan kroni Soeharto dalam berbagai kasus korupsi, sekaligus menolak pencabutan Tap MPR No XI/MPR/1998 . Sikap ini diambil setelah Fraksi PAN melakukan rapat pimpinan di Jakarta, Rabu (30/1), guna menyikapi pro dan kontra mengenai penuntasan kasus Soeharto setelah yang bersangkutan meninggal.
Hal tersebut dikemukakan Ketua FPAN di MPR Patrialis Akbar di Jakarta, Rabu (30/1). Menurutnya, pencabutan Tap MPR seperti yang diusung sejumlah elite politik sangat tidak logis karena peraturan itu bukan hanya berbicara khusus tentang Soeharto. Tap itu justru menekankan koreksi terhadap sistem pada saat Soeharto, yang telah melahirkan banyak kasus korupsi yang melibatkan keluarga dan kroninya.


“Tap ini bicara tentang sistem yang bersalah. Tap ini justru menggambarkan kalau pengusutan kepada Soeharto saja bisa apalagi pada yang lain. Jadi, kami akan mendorong pemerintah agar Tap MPR tetap dilaksanakan kepada konconya termasuk dalam soal BLBI karena sangat mempengaruhi keuangan negara,” katanya.

* * *

SUARA PEMBARUAN, 31 Januari 2008

Usulan Pahlawan untuk Soeharto

Ketua MPR: Tetapkan Dulu Kepastian Hukum

[JAKARTA] Wacana pemberian gelar pahlawan nasional terhadap Soeharto seharusnya digulirkan setelah ada kepastian hukum terhadap kasus hukum mantan presiden kedua RI itu. Pemerintah juga harus terlebih dulu melaksanakan Ketetapan (Tap) MPR Nomor XI/MPR/1998 tentang Penyelenggaraan Negara Yang Bersih dari KKN, Tap MPR Nomor VIII/MPR/2001 tentang Percepatan Kebijakan Penyelenggaraan Negara yang Bersih dari KKN, serta Tap MPR Nomor I/2003.

Menurut Ketua MPR Hidayat Nur Wahid di Jakarta, Rabu (30/1), jika pemberian gelar pahlawan nasional dilakukan sebelum ada kepastian hukum, tidak sesuai dengan amanat Tap MPR tersebut. Semangat Tap MPR adalah menghadirkan penyelenggaraan negara yang bersih dari KKN. Persoalannya, kata dia, sampai saat ini belum ada pernyataan hukum yang menyatakan Soeharto bersalah atau tidak.

Di negara demokrasi memang tidak dilarang orang memunculkan usulan atau wacana apalagi untuk menghormati jasa-jasa para pahlawan. "Kalau bertentangan (dengan Tap MPR, Red) itu sesuatu yang belum kita bahas. Kalau ternyata Soeharto terbukti secara hukum, sesudah menghormati asas praduga tak bersalah, ternyata terbukti melakukan kesalahan, saya kira pemerintah akan sangat berhati-hati memberikan gelar kepahlawanan kepada pihak yang dinyatakan bersalah itu," kata Hidayat.

Dia sangat menyayangkan wacana itu muncul tidak lama setelah Soeharto wafat karena dinilai justru menimbulkan kontroversi. Pasalnya, wacana itu dimunculkan saat kasus hukum Soeharto masih berlanjut di persidangan dan belum ada kepastian hukum. Apalagi, saat ini kasus perdata tentang Yayasan Supersemar masih digelar di Pengadilan Negeri Jakarta Selatan.

Hidayat menegaskan, semua Tap MPR yang dikeluarkan terkait penyelenggaraan negara yang bersih dari KKN semuanya masih berlaku kendati Soeharto sudah wafat. "Tentu Tap ini tidak menjadi luluh, apalagi tercabut, dengan wafatnya Soeharto. Tap MPR ini tidak secara khusus hanya mengatur Pak Harto. Beliau hanya satu dari sekian hal yang diatur dalam Tap itu sehingga harus dijalankan," tegasnya.


Belum Pasti

Secara terpisah, Ketua Umum DPP Partai Golkar (PG) M Jusuf Kalla masih belum menentukan sikap tentang pemberian gelar pahlawan nasional kepada Soeharto. "Untuk menentukan itu harus ada proses," ujarnya seusai melantik pengurus pusat Badan Informasi dan Komunikasi Partai Golkar, Rabu (30/1).

Wakil Presiden RI itu mengatakan tidak pemerintah pun belum mengambil keputusan atas ide pemberian gelar pahlawan itu. Proses tersebut, katanya, bisa melalui usulan-usulan daerah dan juga mengacu pada undang-undang yang ada.

Pengamat politik Universitas Indonesia Andrinof A Chaniago mengatakan usulan pemberian gelar pahlawan kepada Soeharto harus diserahkan kepada lembaga independen. "Karena, jika usulan tersebut datang dari orang-orang yang memiliki hubungan emosional dengan Soeharto, itu tidak fair," tegas dia.

Dukungan terhadap pemberian gelar pahlawan datang dari organisasi massa yang menamakan diri Angkatan Muda Soeharto Indonesia (AMSI). Ketua Umum AMSI, Sam Tuheteru mengatakan pihaknya mendesak pemerintah untuk merehabilitasi nama baik mantan Presiden Soeharto dan keluarganya.

"Ini penting untuk mengamankan stabilitas nasional agar pemerintah lebih berkonsentrasi ke persoalan-persoalan bangsa ini dan jangan berkutat dalam isu seputar Soeharto," ujarnya. [L-10/M-16/Y-4]


* * *

Tajuk Sinar Harapan 31 Januari 2008

Pemerintah Berkepribadian Ganda

terhadap Soeharto

PRESIDEN Susilo Bambang Yudhoyono bertindak sebagai Insipektur Upacara di komplek makam keluarga Soeharto di Astana Gribangun, Karanganyar, Jawa Tengah pada saat pemakaman mantan Presiden Soeharto, Minggu, 28 Januari 2008. Dalam pidatonya Presiden mengatakan “Indonesia telah kehilangan putra terbaik bangsa, seorang pejuang yang setia, prajurit sejati, dan seorang negarawan terhormat”.

Tidak ada yang menolak Pak Harto sebagai putra terbaik bangsa dengan segala keberadaannya, juga sebagai pejuang yang setia ditunjukkannya untuk mencapai tujuan dan sebagai prajurit sejati dimulai sejak KNIL, Heiho sampai TNI. Dia terlibat dalam Serangan Umum 1 Maret sampai menjadi panglima operasi Trikora untuk membebaskan Irian Barat membawahi 200.000 prajurit dengan arsenal terkuat di Asia Tenggara, dan menumpas PKI dan para simpatisannya pasca peristiwa 30 September 1965. Kisah keprajuritannya tidak ada yang menyangkal, bahkan setelah purnawirawan pun ia menjadi jenderal besar.

Tetapi adalah pemahaman hukum yang kacau balau menyebut Pak Harto sebagai negarawan terhormat. Negarawan ya, Soeharto adalah negarawan. Perannya besar dalam membentuk dan membesarkan ASEAN. Tetapi sebutan “terhormat” adalah sulit dari segi logika hukum, karena kasus Tap MPR No. IX/1998 memasukkan Pak Harto dalam kategori melakukan Korupsi Kolusi dan Nepotisme. Dengan predikat terhormat itu menunjukkan bahwa Pemerintah berkepribadian ganda, bersikap mendua terhadap Seharto.Karena Presiden Susilo Bambang Yudhoyono sendirilah yang memberikan kuasa atas nama Pemerintah kepada Jaksa Agung sebagai Pengacara Negara untuk menggugat Soeharto, mantan presiden RI, ketua dan pendiri Yayasan Supersemar, sehingga perkaranya saat ini sedang diperiksa dan diadili di Pengadilan Negeri Jakarta Selatan.

Sebelumnya, Soeharto juga dituntut Kejaksaan di pengadilan yang sama dengan tuduhan korupsi, tetapi karena terdakwa, menurut hasil pemeriksaan tim dokter, mengalami sakit yang permanen sehingga tidak dapat memberi keterangan secara normal, maka akhirnya Kejaksaan mengeluarkan Surat Ketetapan Penghentian Penuntutan Perkara (SKP3). Upaya menyelesaikan korupsi, kolusi dan nepotisme (KKN) yang terkait dengan Soeharto serta kroni-kroninya adalah suatu keharusan sebagaimana ditetapkan MPR dalam Tap/IX/1998, mau tidak mau secara hukum harus dilakukan, proses peradilanlah yang membuktikan, dihukum atau dibebaskan. Sebenarnya sikap mendua atau kepribadian ganda dimulai Presiden BJ Habibie ketika Jaksa Agung Andi Ghalib. Soeharto mulai disidik, kemudian sasaran dialihkan ke Muhammad “Bob” Hasan, Hutomo Mandala Putra (Tommy) dan Probosutedjo.

Namun kasus yang sempat melilit Pak Harto tidak bisa diabaikan dengan adanya tekanan politik. Dari segi politik apa yang dilakukan Presiden Susilo Bambang Yudhoyono adalah suatu realita atau dalam kalkulasi politik sebagai suatu keharusan, tetapi dari logika hukum, layakkah menempatkan Soeharto sebagai negarawan terhormat yang tersangkut kasus hukum dengan KKN yang tidak tuntas? SKP3 Kejaksaan menggantung status hukumnya, dan di situlah kemisteriusan Soeharto, di satu pihak ia disanjung dan dihormati tetapi di sisi lain ia dihujat dan dimaki, memaksa banyak pihak untuk berkepribadian ganda kepadanya. Sadar atau tidak dengan sikapnya tersebut, kurang cermat atau memang ada kesengajaan?

Soeharto di satu sisi adalah orang besar sementara di sisi lain menimbulkan berbagai masalah dan persoalan bagi sebagian orang sejak ia menjadi Panglima Komando Operasi Pemulihan Keamanan dan Ketertiban (Pangkopkamtib) sampai di akhir kepemimpinannya tahun 1998, banyak orang yang mengalami dan merasakan ketidak adilan. Lebih parah bidang politik, sosial, budaya, demokrasi apalagi HAM, tidak ada yang membanggakan, semua serba terkontrol dan diatur. Pemerintah satu-satunya pemilik kebenaran, keadilan dan kepastian hukum, termasuk penentu keyakinan apa kata pemerintahan Soeharto dengan dukungan ABRI, Birokrasi dan Golkar, itulah yang benar.

Penyederhanaan partai, satu-satunya azas, wadah tunggal organisasi kemasyarakatan dan organisasi profesi, pembreidelan media cetak dengan pencabutan surat ijin cetak (SIC) dan surat ijin terbit (SIT) sampai pembatalan Surat Izin Usaha Penerbitan (SIUP), penangkapan tanpa peradilan dan pemimpin harus bersih diri dan bersih lingkungan. Khotib dan imam di mesjid-mesjid harus persetujuan pemerintah. Tetapi dari berbagai tindakan represif itu, hanya KKN Soeharto dan kroni-kroninya yang dituntut Tap MPR. Secara hukum Soeharto sudah sulit dibebaskan dari KKN karena SKP3 Kejaksan menempatkannya antara korupsi dan tidak, karena SKP3 bukanlah putusan pengadilan yang berkekuatan hukum tetap.

Di tengah pro-kontra keinginan mengampuni kesalahannya penghentian kasus hukumnya, pemberian gelar pahlawan, menimbulkan situasi yang tidak jelas dan mendorong kita menjadi tambah gamang. Kesempatan seperti ini akan banyak petualang memanfaatkan situasi untuk kepentingan golongan masing-masing apalagi menjelang Pemilu 2009 serta membersihkan diri. Kita nantikan langkah pemerintah, terhadap seruan agar dicabut Tap MPRS tentang Bung Karno dan Tap MPR No. IX/1998 tentang pengusutan KKN Soeharto termasuk kebijakan Pemerintah terhadap gugatan perdata di PN Jaksel, dikaitkan dengan ucapan Presiden Soesilo Bambang Yudhoyono bahwa Soeharto sebagai negarawan terhormat, apakah setelah tujuh hari masa perkabungan akan ada keputusan baru? n



* * *

Detiknews, 30 Januari 2008

Amien Rais: Pak Harto Tidak Usah

Diberi Gelar Pahlawan

Jakarta - Meski sempat meminta masyarakat memaafkan Soeharto, kini mantan
Ketua MPR Amien Rais berpendapat penguasa Orde Baru itu tidak usah diberi
gelar pahlawan.

Menurut Amien, pemberian gelar pahlawan untuk mendiang presiden kedua
Indonesia itu lebih baik tidak usah dilakukan pemerintah demi menghindari
konflik.

"Itu menimbulkan konflik. Lebih baik tidak usah. Bagi saya, amalan itu lebih
penting. Segala amal baik dan buruk Pak Harto sudah dicatat," ujar Amien di
Gedung MK, Jl Medan Merdeka Barat, Jakarta Pusat, Rabu (30/1/2008).

Amien mengatakan hal tersebut usai menghadiri peluncuran buku bertajuk "Dari
Konstruksi Sampai Konstitusi" karangan salah satu pendiri PAN AM Luthfi.
Menurut Guru Besar UGM ini, pemberian gelar tidak penting. Masyarakat kini
menunggu janji pemerintah untuk mengusut kasus perdata Soeharto.

"Pengakuan manusia tidak begitu penting. Sekarang masyarakat menanti-nanti
janji pemerintah. Menjelang Pak Harto meninggal, pemerintah mengatakan kasus
perdata bisa dilanjutkan. Sampai kapan pun ini yang harus ditindaklanjuti
dengan konkret," beber Amien.

Lalu apakah pantas Soeharto diberi gelar pahlawan? "Lebih baik lupakan dulu,
tidak usah dibicarakan," ucap Amien.

Soal kasus hukum Soeharto, Amien meminta Presiden SBY mengusut.
"Bilang saja Pak Amien berpesan, masyarakat menunggu janji Presiden Yudhoyono
untuk mengusut kasus perdata Pak Harto. Itu harus dilakukan. Jika tidak
terjadi, nanti masyarakat semakin tidak percaya pada pemerintah dan hukum,"
pungkas Ketua Majelis Penasihat PAN ini. ( nik / sss )

* * *

Tempo Interaktif, 31 Januari 2008

Aktivis 77/79 Tolak Gelar Pahlawan

untuk Soeharto

Sejumlah aktivis gerakan mahasiswa angkatan 1977/1978 menolak rencana pemerintah untuk memberikan gelar pahlawan kepada mantan presiden Soeharto. “Sampai sekarang kejahatan kemanusiaan Soeharto belum disentuh,” ujar Mahmud Madjid, salah seorang aktivis angkatan 1977-1978 di Bandung, Kamis (31/1).

Dalam pernyataan yang dibacakan Mahmud, Forum GerakanMahasiswa 77/78 menginginkan agar pemberianpenghargaan dan sanksi terhadap jasa dan kesalahanSoeharto dilakukan secara proporsional dankonstitusional. “Pemerintah dapat merujuk kepadaproduk hukum dan perundang-undangan yang ada,”katanya.Pengampunan dan pemberian sanksi terhadap para pelakupolitik nasional, kata dia, seharusnya dilaksanakansecara mendasar berdasarkan undang-undang tentangKomisi Kebenaran dan Rekonsiliasi. “Jadi akan tercapaikejelasan historis yang melandasi pengampunan dandimulainya rekonsiliasi nasioal yang mendasar,”katanya.

Mereka juga menilai pemberitaan media massa tentangsosok dan debut Soeharto selama ini tidak proporsionaldan berimbang, serta menjurus kepada kultus individu.“Bahkan mungkin ini tidak diharapkan oleh keluargaSoeharto sendiri,” ujar Mahmud.Pemberitaan yang berlebihan tentang prestasi dan jasaSoeharto, ujar Mahmud, dirasakan tidak adil danmelukai hati para korban kebijakan Soeharto. “Sepertidalam kasus Aceh, Papua, Lampung, Tanjung Priok, KasusGerakan Mahasiswa 77-78, 27 Juli, Haur Koneng, Gerakan 30 September, Petrus, Waduk Nipah, Tapol-Napol, dan seterusnya,” kata dia.Pemberitaan yang tidak berimbang ini, kata dia, dapatmenjurus kepada kebohongan publik dan tidak mendidik.“Ini bisa menciptakan kekacauan publik dalam menetapkan standar moral yang benar,” katanya.

* * *

Antara, 31 Januari 2008

Presiden Tak Tanggapi Gelar Pahlawan Soeharto

Presiden Susilo Bambang Yudhoyono seperti disampaikan juru bicaranya, Andi Malarangeng, menolak menanggapi perdebatan mengenai pemberian gelar pahlawan nasioanal kepada mantan presiden Soeharto.

"Kami mendengar dan membaca berita tentang wacana, usulan pihak-pihak tertentu tentang gelar kehormatan atau apa namanya. Indonesia adalah negara demokrasi, semua bisa mengusulkan ini, ada yang mengusulkan memaafkan dan ada yang menghendaki diadili," kata Andi di Jakarta, Rabu.

Menurut Andi, dalam hal ini pemerintah tidak akan berwacana dan hanya akan memutuskannya sesuai dengan tugas dan kewajibannya.

"Pokoknya pemerintah tidak berwacana. Yang berwacana silakan, mau pengamat atau anggota DPR, boleh. Silakan. Warga masyarakat mau berwacana silakan. Ini negara demokrasi. Usul ini, usul itu ya silakan. Ini negara demokrasi," papar Andi.

Perdebatan mengenai perlu tidaknya pemberian gelar pahlawan bagi Soeharto muncul setelah sejumlah pengurus Partai Golkar mengusulkan agar pemerintah memberikan gelar pahlawan atas dasar jasa dan perjuangan Soeharto terhadap bangsa dan negara.

Namun, berbagai pihak menolak usulan itu karena kesalahan-kesalahan Soeharto saat menjadi presiden selama 32 tahun juga sangat besar dan menyakiti hati rakyat. [EL, Ant]
* * *

Koran Tempo, 30 Januari 2008

Mahasiswa di Bali Demo Tolak

Gelar Pahlawan Soeharto



Puluhan mahasiswa di Bali siang ini menggelar aksi unjuk rasa menolak usulan pemberian gelar pahlawan bagi almarhum mantan Presiden RI Soeharto. Mereka justru menyebut Soeharto sebagai Bapak Pelanggaran HAM.

Aksi di jalan PB Sudirman Denpasar itu dilakukan dengan membentangkan spanduk dan mengacungkan poster kepada pengendaraan kendaraan yang lewat. Mereka juga membagi-bagikan pernyataan yang berisi tuntutan akan kasus korupsi Soeharto terus diusut hingga ke kroni-kroninya.

“Tidak ada alasan untuk menghentikan apalagi hanya karena ingin memberi gelar pahlawan,” tegas Hasan yang menjadi Korlap aksi itu.

Poster yang dibuat antara lain bertuliskan, “Adili kroni Soeharto”, “Tidak ada Kata Maaf untuk Pelanggar HAM”, dan lain-lain.

Mahasiswa yang berasal dari berbagai organisasi itu menilai, pemerintahan Presiden Susilo Bambang Yudhoyono telah bertindak represif dengan mewajibkan pengibaran bendera setengah tiang sebagai tanda duka cita atas meninggalnya Soeharto. Padahal, sepanjang kekuasaannya Soeharto telah menyakiti hati rakyat dengan rangkaian pelanggaran HAM sejak pascaperistiwa G-30S PKI hingga penerapan Daerah Operasi Militer (DOM) di Aceh.

Dalam tuntutannya, mereka meminta Presiden Yudhoyono menuntaskan pengungkapan kasus-kasus pelanggaran HAM, menetapkan rezim Orde Baru sebagai rezim pelanggaran HAM, menyatakan Soeharto sebagai penjahat HAM dan mengusut tuntas serta melacak harta hasil korupsi milik Seoharto.

“Tuntuan itu merupakan bukti adanya keadilan di negeri,” tegas mereka.

* * *

Koran Tempo, 30 Januari 2008

Mahasiswa Semarang Tolak Gelar Pahlawan untuk Soeharto



Sejumlah mahasiswa di Semarang menyatakan menolak keras jika pemerintah memberikan gelar pahlawan kepada mendiang mantan Presiden RI Soeharto.

Mahasiswa menilai, Soeharto bukanlah sosok pahlawan yang berhasil membawa bangsa ini ke arah yang benar dan mensejahterakan rakyat.

"Sebaliknya, Soeharto menjerumuskan bangsa ini ke jurang kemiskinan dan keterpurukan," kata mahasiswa Jurusan Sejarah Universitas Diponegoro Semarang Ahmad Yusuf kepada Tempo di Semarang, Rabu (29/1).

Yusuf mengakui, pada awal kekuasaannya Soeharto memang dinilai berhasil membawa Indonesia menuju ke arah yang lebih baik. Secara riil, kata Yusuf, ditandai dengan adanya situasi perekonomian yang lebih baik dan kondisi bangsa yang lebih kondusif.

"Tapi, program yang digalakkan Soeharto tersebut adalah program semu, karena duit pembangunannya berasal dari utang luar negeri," katanya.

Yusuf menambahkan, saat Indonesia mulai utang ke negara-negara donor maka mulai saat itulah bangsa ini berada di ketiak negara-negara barat. "Kedaulatan negara sudah pupus, secara tidak langsung kita sudah terjajah kembali," katanya.

Penolakan pemberian gelar kepada mendiang Soeharto juga diungkapkan Suroso. Mahasiwa Fakultas Tarbiyah IAIN Walisongo Semarang ini mengatakan, tidak ada gunanya pemerintah memberikan gelar pahlawan kepada penguasa yang otoriter seperti Soeharto.

Apalagi, masyarakat saat ini masih terbebani akibat kebijakan-kebijakan Soeharto yang menjerumuskan. "Gimana disebut sebagai pahlawan kalau ia menjerumuskan bangsa ini ke jurang kemiskinan," katanya.


* * *


SUARA PEMBARUAN, 30 Januari 2008

Gelar Pahlawan untuk Soeharto

Sakiti Hati Rakyat


[JAKARTA] Sejumlah kalangan menilai mantan Presiden Soeharto tidak layak diberi gelar pahlawan. Pasalnya, sampai ia meninggal dunia, statusnya masih sebagai terdakwa kasus korupsi.


Selain itu, begitu banyak orang yang dibunuh dan dipenjara tanpa melalui proses hukum pada masa pemerintahan Soeharto. "Kalau ia diberi gelar pahlawan, justru menyakiti hati rakyat, terutama para keluarga korban kekejaman di masa pemerintahannya," kata Koordinator Komisi untuk Orang Hilang dan Korban Tindak Kekerasan (Kontras) Usman Hamid kepada SP di Jakarta, Rabu (30/1).


Selain itu, kata Usman, sangat ironis kalau Soeharto diberi gelar pahlawan karena sejumlah mahasiswa yang tewas ditembak "anak buah" Soeharto pada 1998 karena menuntut dia mundur telah diberi gelar pahlawan reformasi. Sedangkan di sisi lain, Soeharto yang diduga kuat sebagai dalang peristiwa itu juga akan diberi gelar pahlawan.


Senada dengan itu, Koordinator Tim Advokasi dan Rehabilitasi Korban Tragedi 1965 Witaryono Reksoprodjo mengatakan kalau Soeharto diberi gelar pahlawan, selain menyakiti rakyat Indonesia, juga memalukan bangsa dan negara. Pasalnya, di mata dunia internasional Soeharto adalah mantan kepala negara yang mencuri harta negaranya paling tinggi dibanding kepala negara lain yang juga korup.
Selain itu, ketika Soeharto meninggal dunia media massa asing memberitakan Soeharto sebagai seorang mantan diktator yang kejam. "Sudahlah. Ia tidal layak diberi gelar pahlawan," kata dia.


John Pakasi, salah satu korban pelanggaran HAM berat 1965, mengatakan Soeharto adalah diktator yang kejam, bahkan lebih kejam dari Hitler. "Mana bisa orang seperti dia diberi gelar pahlawan?" ujar pria yang dipenjara selama 9 tahun tanpa melalui proses hukum oleh Soeharto dengan alasan terlibat PKI.


Ketua Badan Pengurus Yayasan Lembaga Bantuan Hukum Indonesia (YLBHI) Patra M Zen mengatakan sangat tidak patut pemerintah memberikan predikat pahlawan kepada Soeharto. Ketidaklayakan itu didasarkan pada alasan bahwa secara hukum, Soeharto tidak bisa dikatakan bersalah maupun tidak bersalah karena proses hukum atas perbuatannya tidak selesai.


Keputusan pemerintah memberikan predikat pahlawan dan memberikan penghormatan tujuh hari berkabung merupakan sikap yang gegabah dan tidak berdasar. Hal itu juga bertentangan dengan prinsip proporsionalitas dan alasan hukum yang rasional.
Patra menilai sikap dan proses politik yang mengiringi kematian Soeharto yang ditunjukkan oleh pemerintah begitu gegabah dan berlebihan. Pasalnya, Soeharto meninggal dunia tanpa pernah diadili atas perbuatan-perbuatan.


Ketua Dewan Pimpinan Pusat Partai Demokrat Anas Urbaningrum menilai tidak elok untuk membicarakan dan memperdebatkan apakah mantan Presiden Soeharto yang telah berpulang, Minggu (27/1), perlu diberi gelar pahlawan atau tidak. Lebih tidak elok lagi karena wacana itu hanya bermaksud mengambil kesempatan politik dalam suasana duka keluarga mantan penguasa Orde Baru itu. "Itu tidak ubahnya politisasi atau bahkan komersialisasi duka cita untuk kepentingan politik," kata Anas. [E-8/A-21]


* * *

Tribune Timur, 29 Januari 2008

Soeharto tinggalkan Utang Rp 1.800 Triliun

Kesalahan terbesar penguasa Orde Baru terletak pada kebijakan ekonomi yang seolah-olah begitu kuat.


KETUA Fraksi Kebangkitan Bangsa
(FKB) DPR RI Effendi Choirie jengkel bukan main. Ia melihat sikap
pejabat dan media massa di Indonesia, terutama televisi, sangat
tidak proporsional dalam memberitakan pemakaman sang penguasa Orde
Baru, Soeharto.


"Kita sangat pahami simpati dan bela
sungkawa dari para pejabat kepada almarhum Pak Harto. Tapi, yang
proposional saja-lah. Harusnya Pak Jusuf Ronodipuro juga layak
mendapatkan penghargaan yang sama," kata Effendi di Gedung DPR,
Senayan, Jakarta, Senin (28/1).

Jusuf adalah pejuang kemerdekaan
yang membacakan proklamasi kemerdekaan RI pada 1945. Lewat RRI yang
didirikan, ia menyebarkan kabar Indonesia telah merdeka.
"Pak Harto memang orang besar. Tapi
jangan lupa kesalahannya juga besar. Utang Rp 1.500 triliun itu
warisan Pak Harto," ujarnya lagi.


Effendi salah. Soeharto tidak
meninggalkan utang Rp 1.500 triliun, melainkan meninggalkan utang Rp
1.800 triliun!
Tepatnya 800 miliar dolar AS.
Artinya, jika utang tersebut dibagi-bagikan kepada 200 juta penduduk
Indonesia, maka setiap kepala dibebani utang Rp 9 juta.
Anda tentu bisa menebak siapa
menyebabkan negara ini berutang begitu besar? Ya, angka tadi adalah
posisi awal utang Indonesia saat Soeharto dilengserkan, Mei 1998
silam.
Banyak pengamat meyakini, Soeharto
digulingkan bukan semata karena gerakan mahasiswa, tetapi juga
karena ia membangun sistem ekonomi yang begitu keropos selama
berkuasa 32 tahun.
Buktinya, krisis moneter 1997, yang
menyebabkan perekonomian Indonesia porak poranda, tidak hanya
dialami RI. Seluruh negara di Asia Tenggara juga mengalaminya.
Namun, hanya dalam waktu dua-tiga
tahun, Thailand, Malaysia, serta Singapura berhasil mengatasinya.
Negara-negara jiran yang semula


berada di bawah bayang-bayang Indonesia ini akhirnya melejit pesat
dan meninggalkan Indonesia. Hingga sekarang.
Warisan utang Soeharto itu masih
menjadi beban negara hingga kini.
Utang yang jumlahnya berlipat-lipat
dari nilai APBN ini berasal dari pinjaman di International Monetary
Fund (IMF), World Bank (Bank Dunia), dan Asian Development Bank
(ADB).


Mungkin, bahwa utang ini tidak
berdampak langsung bagi Anda. Tapi tahukah Anda, lebih dari separuh
APBN, dipakai hanya untuk membayar utang-utang tersebut plus
bunganya.
Artinya, anggaran yang semestinya
dipergunakan untuk, misalnya, membangun jalan, memperbaiki gedung
sekolah, dan segala fasilitas umum digerus habis oleh utang warisan
Soeharto ini.


Kini, setelah 10 tahun sejak
diturunkan, Soeharto sebenarnya masih menyusahkan rakyat. Setiap
tahun, negara tetap membayar utang-utang tersebut. Data yang
dilansir Bank Indonesia, posisi terakhir utang luar negeri kita
adalah 176,55 miliar dolar AS atau Rp 1.589 triliun (kurs Rp 9.000
per dolar AS).


Pembayaran Utang
Pembayaran utang luar negeri
pemerintah memakan porsi APBN yang terbesar dalam satu dekade
terakhir.
Jumlah pembayaran pokok dan bunga
utang hampir dua kali lipat anggaran pembangunan dan memakan lebih
dari separuh penerimaan pajak.


Pembayaran cicilan utang sudah
mengambil porsi 52 persen dari total penerimaan pajak yang
dibayarkan rakyat sebesar Rp 219,4 triliun.
Pada tahun 2006, pemerintah
melakukan pelunasan utang kepada IMF. Pelunasan sebesar 3,182 miliar
dolar AS merupakan sisa pinjaman yang seharusnya jatuh tempo pada
akhir 2010.


Saat Soeharto turun, akhirnya
terungkap sebanyak 30 persen utang luar negeri itu atau sedikitnya
Rp 540 triliun dikorupsi oleh Soeharto dan kroni-kroninya.
Dari Dulu
IMF sebenarnya sudah lama
menancapkan kukunya di Indonesia. Itu dimulai sejak era Presiden
Soekarno.


Namun, sikap tegas Soekarno yang
mencium ada udang di balik batu lembaga moneter dari Amerika Serikat
(AS) itu memaksa IMF segera hengkang.
Saat Soeharto berkuasa, 1967, rezim
Orde Baru kembali membuka pintu lebar-lebar bagi IMF.
Pada akhir tahun 1966, IMF membuat
studi tentang program stabilitas ekonomi, dan pemerintah Orde Baru
dengan cepat melaksanakan kebijakan seperti yang diusulkan IMF dan
Indonesia secara resmi kembali menjadi anggota IMF.

Kembalinya Indonesia menjadi anggota
IMF dan Bank Dunia, menimbulkan reaksi negara-negara barat.
Mereka segera memberikan hibah
sebesar 174 juta dolar AS dengan tujuan untuk mengangkat Indonesia
dari keterpurukan ekonomi, disusul dengan restrukturisasi utang
karena 534 juta dolar AS harus dikeluarkan untuk membayar cicilan
pokok dan bunga utang.
Pembangunan pun dimulai. Selama 30
tahun ke depan, rakyat tidak tahu bahwa pembangunan yang dilakukan
adalah utang-utang berbunga yang mesti dibayar.


Puncaknya terjadi saat krisis
ekonomi menghantam Asia Tenggara pada 1997.
Soeharto mengundang IMF untuk
menyelamatkan perekonomian nasional yang sedang dalam krisis.
Kesepakatan antara IMF dan
pemerintah Indonesia terjadi pada tanggal 31 Oktober 2007 dengan
ditandatanganinya Letter of Intens (LOI) pertama yang berisikan
perjanjian tiga tahun dan kucuran utang sebesar 7,3 miliar dolar AS.


Namun kehadiran IMF justru
mengakibatkan bertambah parahnya berimbas pula terhadap ekonomi
Indonesia, tidak lebih dari satu tahun terjadi pelarian modal
(capital flight) keluar negeri besar-besaran yang menyebabkan
pengangguran, diperparah lagi dengan penurunan nilai tukar rupiah
secara drastis.


Pada Mei 1998, karena kesepakatan
antara IMF dan Soeharto, pemerintah mencabut subsidi bahan pokok,
serta menaikkan harga minyak dan listrik. Kebijakan ini menyulut
penolakan keras dari rakyat dan tak lama kemudian, Soeharto jatuh.
Anehnya, setelah itu, presiden yang
silih berganti tak berani mengambil sikap keluar dari Bank Dunia dan
IMF.


Tak satupun di antara mereka yang
seberani Presiden Venezuela, Hugo Chavez. Tahun 2007 lalu, Chavez
mengumumkan secara resmi bahwa negaranya menyatakan keluar dari
keanggotaan Bank Dunia dan IMF.


Akhirnya, hingga kini, dibanding
negara-negara yang dihantam krisis ekonomi 11 tahun silam, ekonomi
Indonesia tetap terpuruk dan tak beranjak.
Pengamat ekonomi meyakini, kesalahan
terbesar Soeharto bukan pada dugaan korupsi dan pelanggaran HAM yang
dituduhkan kepadanya.


Tetapi kesalahan terbesar penguasa
Orde Baru itu terletak pada kebijakan ekonomi yang seolah-olah
begitu kuat --karena ditopang oleh tindakan represif-- padahal
kenyataannya sangat rapuh.


Sebagian rakyat mungkin sudah
melupakan kasus korupsi Soeharto. Sebagian lainnya malah mungkin
tidak tahu kasus-kasus pelanggaran HAM berat Soeharto.
Tapi, yang pasti, disadari atau
tidak, peletakan kebijakan ekonomi Soeharto yang begitu rapuh, masih
dirasakan dampaknya hingga kini.
Setelah 10 tahun dipaksa turun dan
bahkan setelah Soeharto sudah wafat.


* * *

Tribune Timur, 29 Januari 2008

Habibie Sedih Dicuekin Soeharto...

Konflik Antara Soeharto dan Muridnya


JAKARTA kian tegang! Tekanan mahasiswa agar Soeharto lengser kian liar. Di saat yang sama elite-elite politik terus bergerilya. Misi mereka seragam. Merapatkan barisan, mendorong perubahan, dan ini berarti lengserkan Soeharto. Sehari menjelang pengunduruan diri Soeharto, Wapres Habibie meminta ajudannya, Kolonel (Mar) Djuhana, menghubungi Soeharto. Namun, Pak Harto punya pikiran lain. Dia menolak keinginan murid kesayangan yang dia kenal saat bertugas di Pare-pare, saat Habibie masih berusia 15 tahun, di tahun 1950-an.

Melalui Menteri Sekretaris Negara Saadilah Mursyid, Pak Harto hanya menyampaikan, 21 Mei 1998, dia akan menyerahkan otoritasnya ke Habibie. Itu membuat Habibie terkejut. Dia ngotot bertemu Soeharto. Ajudan Soeharto menyanggupi dan berjanji mempertemukan sebelum ke Istana Presiden.

Saat tamu pulang, Habibie memantau perkembangan Jakarta dari internet. Tiba-tiba Kol Djuhana melaporkan Panglima ABRI Jend Wiranto minta waktu bertemu. Mengaku ingin istirahat Habibie menolak. Pukul 06.45, Wiranto datang lagi. Hingga pukul 07.25 Wiranto melaporkan keadaan Jakarta dan inpres yang diteken Presiden Soeharto untuk bertindak demi keamanan dan stabilitas negara. Inpres ini semacam Supersemar (Surat Perintah Sebelas Maret). Habibie juga menegaskan agar tetap mengamankan keluarga Soeharto.
Tapi Habibie gelisah. Dia masih ingin mendengarkan penjelasan dari Soeharto, Tapi Pak Harto tetap bersikeras menolak bertemu Habibie.Habibie tak berkecil hati. Dia masih punya harapan. Sebelum pernyataan pengunduran diri, Habibie berharap Soeharto mengabulkan permohonannya.

Pukul 08.00 Habibie sudah di Istana. Tapi ajudan presiden hanya mempersilakan ketua dan anggota Mahkamah Agung ke ruang Jepara. Kepada ajudan, Habibie kembali menanyakan dia datang lebih dulu dan sudah janjian bertemu Soeharto. Tapi Soeharto kukuh menolak Habibie.


"Betapa sedih dan perih perasaan saya ketika itu. Saya melangkah ke ruang upacara mendampingi Presiden Soeharto, manusia yang saya sangat hormati, cintai, dan kagumi yang ternyata menganggap saya seperti tidak ada.Saya melangkah sambil memanjatkan doa dan memohon agar Allah SWT memberi kekuatan, kesabaran, dan petunjuk untuk mengambil jalan yang benar," tuturnya.


Satu dekade berlalu keinginan Habibie tetap sikap penolakan Soeharto. Beberapa kali, Habibie ingin bertemu Soeharto. Momen Lebaran dan hari ulang tahun Soeharto selalu jadi harapannya. Hingga akhirnya, tanggal 15 Januari 2008, saat Soeharto kritis di RSPP Habibie ajukan niat baik. Tapi Soeharto tetap diam dalam sakitnya.
"Dengan Bismillahirrahmanirrahim, pergilah pulang. Kau harus bezuk Pak Harto!" kata Fannie Habibie, adik kandung Habibie, menyemangati kakaknya untuk sekedar menyapa Soeharto saat masih Habibie masih di Jerman. "Bagaimana kalau saya ditolak lagi?" jawab BJ Habibie.

"Tidak apa, Rud. Jika memang kau ditolak lagi, terimalah dengan lapang dada. Yang penting, pergilah. Aku sebagai adik mendukung kau pulang untuk menemui Pak Harto dalam kondisi yang seperti ini," ujar Fannie, seper Dan hingga Soeharto menghembuskan nafas terakhirnya, Minggu (27/1) lalu, harapan Habibie tetaplah penolakan Soeharto. Dari Jerman, Rudy, sapaan Habibie, hanya mengutus anaknya, Thareq dan Kamal ke Cendana, tempat Soeharto disemayamkan.



* * *

Pos Kota 29 Januari 2008

Mahasiswa Demo Depan Istana Merdeka

JAKARTA (Pos Kota) –Di tengah prosesi pemakaman mantan Presiden HM Soeharto, 87, di Astana Giribangun, Jawa Tengah, ratusan mahasiswa dan elemen warga demo di depan Istana Merdeka, Jakarta, Senin (28/1).

Mereka mendesak pemerintah agar segera mengadili dan mengusut tuntas rentetan kasus almarhum Soeharto selama berkuasa 32 tahun. “Pak Harto, memang sudah tiada, tapi bukan berarti seluruh kasus yang merugikan rakyat itu berhenti sampai di sini, kami minta hukum harus ditegakkan seadil-adilnya,”teriak satu pendemo.

Pengunjukrasa ini sebelumnya berkumpul di Tugu Proklamasi, sekitar pukul 10.00 WIB. Kemudian, mereka beriringan dengan menggunakan sepeda motor bergerak menuju Istana, melewati Jalan Diponegoro, Jalan Salemba Raya, Kramat Raya, Tugu Tani, Jalan Medan Merdeka Timur, hingga ke depan Istana Merdeka.

Mereka mengatasnamakan Kesatuan Rakyat Adili Soeharto (Keras) tersebut di sepanjang jalan berorasi. Membagi-bagikan selebaran. Isinya antara lain menyatakan ketidakpuasan mereka terhadap kepastian hukum almarhum jenderal besar bintang lima itu.

MENOLAK
Dengan lantang, puluhan pendemo ini menolak pemasangan bendera setengah tiang sebagai petanda berkabung nasional selama 7 hari. “Kami menolak memasang bendera merah putih setengah tiang,” kata Wawan, koordinator pendemo.

Iring-iringan pendemo dijaga ketat oleh aparat kepolisian. Sesampainya di Istana Merdeka, pendemo berorasi dan mengecat jalan aspal dengan pilox warna putih dan bertuliskan “Adili Soeharto.”

Akhirnya, aksi itu dibubarkan petugas. Selanjutnya pendemo meninggalkan tempat dengan terpaksa.
* * *

Bangka Pos, 30 Januari 2008

Tutup Kasus Pak Harto

Jaksa: Tetap Dilanjutkan

Dasarnya yakni yurisprudensi MA (Mahkamah Agung).

JAKARTA, BANGKA POS - Tim kuasa hukum (alm)
Soeharto meminta agar persidangan atas gugatan perdata terhadap
mantan Presiden RI ke-2 itu dan Yayasan Supersemar ditutup.
Alasannya, UU tidak mengatur bahwa ahli waris bisa menjadi pihak
tergugat untuk mewarisi gugatan pewarisnya.

"Soal ahli waris, tidak ada dalam UU yang
mewajibkan waris untuk melanjutkan gugatan. Oleh karena itu, kami
mohon perkara ini ditutup," tegas OC Kaligis dalam persidangan kasus
gugatan perdata terhadap Soeharto dan Yayasan Supersemar di
Pengadilan Negeri (PN) Jakarta Selatan, Selasa (29/1).

Atas permohonan tersebut, jaksa pengacara
negara (JPN) asal Kejagung yakni Johanes Tanak meminta agar majelis
hakim tidak mengindahkan permintaan Kaligis. Alasannya, Kaligis saat
ini tidak lagi menjadi kuasa hukum (alm) Soeharto. Surat kuasa gugur
bersamaan dengan meninggalnya pemberi kuasa.

Namun majelis hakim yang dipimpin Wahyono
tetap menyatakan bahwa gugatan ini tetap dapat dilanjutkan. Dasarnya
yakni yurisprudensi dari Mahkamah Agung (MA). "Sesuai yurisprudensi
MA, perkara dapat dilanjutkan," tegas Wahyono.

Seusia persidangan, Kaligis menegaskan bahwa
dalam upacara pemakaman Pak Harto, Presiden SBY secara tegas
mengatakan bahwa Pak Harto adalah putra terbaik dan banyak jasanya
bagi negeri ini.

"Presiden bilang putra terbaik, kenapa masih
ada perkaranya. Harusnya ditutup dong," lanjut Kaligis.
Kuasa hukum Soeharto lainnya yakni Juan
Felix Tampubolon mengatakan gugatan terhadap Soeharto hanyalah main-
main. Alasannya, gugatan dilakukan tanpa didahului dengan putusan
pidana dari kasus yang digugat. "Harus dibuktikan dulu ada perbuatan
melawan hukumnya secara pidana, baru bisa perdata," tegas Felix.

Ditambahkan Felix, dalam persidangan yang
sudah hampir selesai ini, sama sekali tidak ditemukan kerugian
negara yang dilakukan oleh Pak Harto. Begitu pula dengan perbuatan
melawan hukum yang dilakukan mantan penguasa Orde Baru
tersebut. "Pak Harto itu tidak ada hubungannya dengan kasus dana
Yayasan Supersemar," lanjut Felix.

Menurut Felix, perintah majelis hakim kepada
JPN agar menunjuk ahli waris yang menjadi pewaris gugatan Soeharto,
tidak serta merta bisa ditunjuk begitu saja. Prosesnya harus melalui
penetapan Pengadilan Agama atau dengan pengesahan notaris.

"Pokoknya, kalau ini tembus (diputus hakim),
saya yakin ini ada unsur politis. Gugatan ini ngawur kok. Ecek-
ecek," tegas Felix.

Anggota JPN Johanes Tanak menegaskan,
majelis hakim sudah mengatakan gugatan terhadap Soeharto dan Yayasan
Supersemar tetap dapat dilanjutkan. JPN sepakat dengan sikap majelis
hakim.

"Menurut kami, sidang tetap berlanjut.
Menurut kami, ahli waris adalah anak-anaknya (Soeharto)," jelas
Johanes Tanak.

* * *

Lampung Post 30 Januari 2008

Suharto dan Orde 'Tak Tersentuh'

Andi Arief
Mantan Ketua Umum Solidaritas Mahasiswa Indonesia untuk Demokrasi (SMID)
Suharto adalah diktator yang berhasil. Apa boleh buat, dari satu iklim politik yang suka latah seperti sekarang, saya terpaksa mencatatnya begitu. Kepergiannya kemarin diiringi liputan media massa luar biasa, on the spot, menit per menit. Kentara sekali jika obituariumnya telah selesai ditulis, atau direkam, jauh hari sebelum bekas penguasa rezim Orde Baru itu meninggal dunia.

Media massa, meski tak semua, menghadirkan liputan kematiannya bak sebait ode bagi seorang sarat tanda jasa. Satu stasiun televisi bahkan memutar kembali riwayat hidupnya dengan iringan lagu Gugur Bunga. Terkesan pada kita Suharto seperti seorang pahlawan besar yang tamat berlaga di medan perang. Tapi bagi saya, lagu itu justru menyeret ke arah komplikasi sejarah, dan juga hari depan politik Indonesia. Satu momen yang tiba-tiba membuat saya terlempar ke masa sepuluh tahun silam.

Dari sudut sel gelap tempat saya ditahan aparat keamanan Orde Baru, sayup-sayup terdengar lagu Gugur Bunga menyayat. Hari-hari di bulan Mei 1998. Dari radio transistor penjaga sel, telinga saya menangkap berita tiga mahasiswa ditembak mati di Universitas Trisakti. Mereka jadi martir gerakan mahasiswa prodemokrasi dan rakyat yang tak percaya lagi dengan rezim Orde Baru. Saya dan kawan-kawan mahasiswa yang diculik, lalu ditahan itu, yakin inilah orkes pembuka bagi tumbangnya kediktatoran. Sepekan kemudian, Suharto mundur. Lakon Indonesia yang hamil tua itu akhirnya usai. Orde politik baru telah lahir.

Tapi, rupanya perubahan tak selalu menghasilkan kemurnian. Ratusan ribu mahasiswa hari itu bermimpi rakyatlah yang menang. Lalu, pemerintahan baru bisa membawa cita-cita tentang Indonesia yang makmur dan adil, yang bebas represi dari tentara bangsanya sendiri. Demokratis dan bermartabat. Pokoknya semua hal normatif, dan kini terdengar naif. Sepuluh tahun terakhir kita mencatat, bahwa Suharto adalah "sang tak tersentuh". Semangat menumbangkan sang diktator satu dekade silam, kini hampir sama kencangnya dengan gairah merayakannya kembali sebagai pahlawan.

Apa boleh buat. Mimpi reformasi itu pun kini hanya sepertiganya berhasil. Memang, ada kelegaan bahwa politik kini sudah menjadi urusan sipil. TNI dengan besar hati kembali menjadi militer profesional. Pers boleh bebas bicara, dan partai politik tumbuh seperti jamur. Ekonomi relatif lebih baik, meski banyak yang mengigau bahwa zaman Suharto jauh lebih enak. Tak soal. Mungkin mereka dari kaum yang memilih perut kenyang, tapi rela jika bermimpi pun dilarang.

Tetapi, sebagai bekas penguasa satu rezim terpanjang dalam sejarah republik Indonesia--mengalahkan Sukarno pendahulunya, Suharto adalah produser sekaligus produk dari satu tradisi politik yang berbahaya bagi demokrasi. Dia percaya rakyat tak butuh demokrasi, karena pemimpin tahu segalanya. Pancasila dipermak menjadi mantra menghidupkan kembali negara organis gaya fasisme Jepang. Suharto, dengan patrimonialisme yang ditanamkan pada pengikutnya, menjelma menjadi struktur otoriter itu sendiri. Dia tak cukup lagi dilihat sebagai pribadi. Kekuasaan otoriter, oligarki ekonomi bertopang kronisme, tradisi politik asal bapak senang, wadah tunggal, penangkapan kaum oposan dengan brutal, agaknya telah menjadi bagian dari "Suhartoisme".

Di tengah politik Indonesia yang kian liberal hari ini, ajaran itu mungkin mulai lapuk. Tapi toh, politik liberal itu juga membuat Suharto bisa menutup hari akhirnya dengan lebih terhormat. Nasibnya jelas lebih baik ketimbang koleganya bekas Presiden Filipina Ferdinand Marcos, yang digulingkan people power pada 1986. Hingga ajalnya tiba, Marcos sempat tak diizinkan pulang ke negerinya oleh penguasa baru hasil reformasi politik di sana. Di Korea Selatan, bekas presiden Chun Do Hwan, yang sebetulnya sulit disebut diktator ulung, tapi tersandung kasus korupsi. Dia akhirnya bersedia masuk bui. Chun lalu dikenang sebagai contoh moral politik bertanggungjawab dari bangsa Korea.

Perginya Suharto jelas meninggalkan warisan perkara publik yang belum tuntas. Bahkan, kita merasakan Suhartoisme seperti berdenyut kembali. Ketika dia sedang sakit berat pun, tiba-tiba segelintir elit politik melantunkan koor maaf, minta kejahatan politik dan ekonomi Suharto dihapuskan saja dari benak rakyat. Kita lalu seperti putus asa dengan hukum. Lebih parah lagi, kita lupa bahwa penyelesaian kasus Suharto adalah amanat reformasi, dan telah menjadi satu ketetapan di MPR RI.

Tentu, setelah Suharto pergi, warisan perkara itu menjadi beban negara, keluarganya dan juga masyarakat. Bagi negara, pintu rekonsiliasi bagi masa lalu lenyap sudah, padahal masih banyak perkara korban pelanggaran hak asasi manusia oleh rezim Orde Baru belum lagi tuntas.

Sepatutnya, saat Suharto minta maaf dulu, sewaktu dia turun, penguasa baru membawanya ke pengadilan. Mereka yang menjadi korban politik kejahatan atas kemanusiaan, dari tragedi 1965 sampai kasus Lampung, Aceh, Papua dan banyak lagi, bisa menemukan jawaban pasti secara legal dari negara. Kini, tentu pemerintah menjadi lebih sulit merebut kembali harta yang diselewengkan Suharto melalui berbagai yayasan. Atau mengeduk kembali timbunan uangnya di luar negeri, termasuk mengadili perannya dalam kasus BLBI yang macet itu.

Saya turut bersedih, bukan mengikuti anjuran berkabung nasional selama sepekan itu. Yang menyedihkan adalah kepergian Suharto dengan warisan perkara. Sebagai bangsa seakan kita telah gagal memberi contoh yang baik bagaimana menamatkan sebuah transisi dari rezim otoriter ke demokrasi. Tidak jelasnya status hukum Suharto sampai dia meninggal, adalah tragedi besar bagi reformasi politik dan hukum. Dia sekaligus pendidikan politik buruk bagi generasi penerus, seakan menjadi pemimpin berarti berhak menjadi yang "tak tersentuh".

Apa boleh buat. Suharto adalah diktator yang berhasil. Dia mungkin pergi dengan tenang. Tetapi rakyat yang mencatat kesalahannya, mungkin akan terus memburu keadilan dari siapa pun yang menjadi pewarisnya.

* * *

Kompas, 30 januari 2008

Amuk Rakyat Tolak Berkabung
SOLO, KOMPAS - Sehari setelah mantan Presiden Soeharto dimakamkan,
sejumlah warga kota Solo dan sekitarnya yang tergabung dalam Aliansi
Masyarakat untuk Kesejahteraan Rakyat mendatangi Kantor Kejaksaan
Negeri Solo. Mereka menolak masa berkabung nasional dan pengibaran
bendera setengah tiang.

Aliansi Masyarakat untuk Kesejahteraan Rakyat (Amuk Rakyat)—yang
terdiri atas aktivis beberapa badan eksekutif mahasiswa (BEM) dari
sejumlah perguruan tinggi dan aktivis lembaga swadaya masyarakat—juga
mendesak kroni-kroni Soeharto yang terkait dengan pelanggaran hak
asasi manusia (HAM) dan kasus korupsi segera diadili.

Para aktivis yang dipimpin Winarso, Selasa (29/1), menemui Kepala
Kejaksaan Negeri Solo Momock Bambang Soemiarso. Dalam pertemuan itu,
Winarso menyerahkan pernyataan sikap Amuk Rakyat yang intinya menolak
masa berkabung nasional tujuh hari dan pengibaran bendera setengah
tiang. Alasannya, Soeharto tidak pantas mendapatkan penghormatan itu.

"Kasus-kasus Soeharto dan kroni-kroninya sampai saat ini belum jelas
penyelesaiannya. Bagi kami, berkabung nasional tujuh hari hanya
dikhususkan untuk pahlawan. Soeharto yang penuh dengan kasus apakah
pantas disebut sebagai pahlawan," tutur Winarso.

Jangan buru-buru

Di Yogyakarta, ahli sejarah dari Universitas Sanata Dharma Yogyakarta,
FX Baskara T Wardaya, mengingatkan agar pemerintah tidak buru-buru
memberi gelar pahlawan kepada mantan Presiden Soeharto. Sebab,
ketokohan Soeharto masih menyisakan kontroversi, di samping sebagian
besar masyarakat dipastikan tidak akan mendukung pemberian gelar.
"Belum saatnya memberikan gelar kepahlawanan kepada Soeharto," ujarnya.

Menurut Baskara, Soeharto tidak bisa digolongkan dalam kategori
pahlawan nasional. Kematiannya bahkan menyisakan ketidakjelasan dalam
hal sejarah, terutama menyangkut peristiwa Serangan Umum 1 Maret 1949.

Selain itu, Soeharto juga dinilai sebagai tokoh yang bertanggung jawab
dalam peristiwa gerakan 30 September 1965. Peristiwa itu telah
menyebabkan munculnya dua tragedi, yaitu terbunuhnya sejumlah jenderal
dan pembantaian rakyat secara massal.

Astana Giribangun

Sekalipun di Solo dan Yogyakarta ada yang mempermasalahkan
"kepahlawanan" Soeharto, tempat pemakamannya kemarin dikunjungi banyak
peziarah. Ratusan warga dari berbagai daerah bergantian memadati
Kompleks Astana Giribangun, Karanganyar, Jawa Tengah. Peziarah tidak
sekadar memberi penghormatan terakhir kepada Soeharto, tetapi
sekaligus berdoa.

Di Solo, Bupati Wonogiri Begug Poernomosidi, yang masih berkerabat
dengan Ny Tien Soeharto, mengimbau masyarakat agar berkenan memaafkan
Soeharto.

Sementara di Makassar, Sulawesi Selatan, ratusan siswa dari berbagai
sekolah melakukan tabur bunga di Monumen Mandala, simbol perjuangan
merebut Irian Barat (Papua), yang dipimpin Soeharto. Monumen yang
dibangun dengan dana Rp 75 miliar itu diresmikan Soeharto pada tahun
1995.(EKI/GAL/WER/SON/KOR/WKM/REN)

* * *



Tempo Interaktif,, 30 Januari 2008 IB

Pendukung Soeharto Unjuk Rasa di UNCAC

TEMPO Interaktif, Nusa Dua:Aksi unjuk rasa kecil-kecilan sempat terjadi di sekitar hotel Westin, Nusa Dua, tempat berlangsungnya Konferensi International Anti Korupsi. Pelakunya adalah beberapa penyusup yang mengaku tergabung dalam Pemuda NKRI.

Sekitar 7 orang dari kelompok itu berhasil masuk hingga ke lobi hotel dan membagikan selebaran yang berisi tuntutan agar PBB dan Bank Dunia mencabut inisiatif StAR dan mencabut tuduhan kepada mantan Presiden Soeharto sebagai kepala negara terkorup di dunia. “Pencabutan harus disampaikan secara resmi dalam forum UNCAC,” kata mereka.

Pernyataan itu antara lain ditandatangani Adam Rumbaru (Pemuda Maluku), Mardan Muhammad (Pemuda Sulawesi), Tri Joko Susilo (Pemuda Jawa), Reningson (Pemuda Sumatra) dan Agus Abdullah (Pemuda Nusa Tenggara). PBB juga diminta menyampaikan permohonan maaf kepada seluruh rakyat Indonesia.

Bila tuntutan mereka tidak terpenuhi, mereka mengancam akan mengadakan class action kepada Mahkamah Internasional untuk menuntut Presiden Bank Dunia Robert B Zollick agar mendapat sanski hukum. Selain itu meminta ganti rugi sebesar 15 juta US Dolar sebagai pengganti laporan palsu dan penghinaan terhadap rakyat Indonesia. Sebelum mereka dapat beraksi lebih jauh, petugas keamanan telah menggiring mereka keluar dari area

* * *

Surat Dari Montmartre:

KEMATIAN SOEHARTO DI PERS PERANCIS
Sejak Soeharto naik panggung kekuasaan dan mengendalikan jalannya Republik Indonesia, sekali pun Perancis tergabung dalam IGGI dan kemudian CGI, boleh dikatakan hubungan antara kedua negara tidak hangat. Lebih-lebih ketika Mei 1981, Perancis berada di bawah kekuasaan Partai Sosialis dan partai-partai kiri. Ketika partai-partai kanan , RPR dan UDF, sekutu RPR, berkuasa menggantikan Partai Sosialis dan partai-partai kiri [kiri majemuk, la gauche plurielle], sikap Perancis pada Orde Baru Soeharto pun tidak mengalami perobahan mendasar. Sikap tidak hangat pemerintah Perancis ini, barangkali bisa diusut dari sejarah negeri ini, yang melalui beberapa kali revolusi, sampai memenggal kepala raja di depan publik, akhirnya sampai pada nilai "liberté, egalité et fraternité [kemerdekaan, kesetaraan dan persaudaraan]. Nilai-nilai ini sampai sekarang dijadikan motto Republik Perancis, dicantumkan di semua surat-surat dan dokumen segala keputusan resmi. Rangkaian nilai yang menjadi isi konsep republik, dituangkan ke dalam undang-undang dan peraturan-peraturan. Ia ditanamkan sejak anak-anak masuk sekolah.


Memang nampak juga satu paradoks ketika Perancis ikut dalam IGGI , organisasi internasional terdiri dari berbagai negara besar yang kuat ekonominya, dan sejak awal berdirinya Orde Baru boleh dikatakan penyangga utama ekonomi dan politik Orde Baru. Tapi ikutnya Perancis di dalam IGGI, barangkali bisa dipahami dari posisi Perancis sebagai negara kapitalis, yang tidak ingin membiarkan Indonesia, sebuah pasar besar dan sumber bahan mentah berlimpah, hanya dikuasai oleh negeri-negeri anggota IGGI -- kemudian berobah menjadi CGI. IGGI merupakan sebuah forum internasional untuk membagi-bagi "kueh" Indonesia.


Oleh latar belakang demikian, maka sering nampak politik pemerintah dan sikap rakyat Perancis, termasuk yang muncul di media massa, sering berbeda. Seakan di sini kita saksikan adanya dua Perancis: Perancis resmi dan Perancis rakyat.


Hal ini pun kembali tercermin dalam menghadapi kematian Soeharto. Sejauh ini, aku belum juga membaca dan mendapatkan ucapan belasungkawa resmi dari pemerintah Sarkozy dan Fillon. Bahkan Harian Le Figaro, koran yang dekat dengan kekuasaan sekarang, sejauh ini masih tidak sebaris pun memberitakan tentang kematian Soeharto. Apakah ketiadaan baris kalimat pemberitaan begini merupakan suatu sikap politik dari sebuah koran nasional penting berpengaruh dan dekat dengan penyelenggara negara di negeri ini? Memberitakan atau tidak, kukira adalah suatu sikap. Sikap politik.


Sementara koran-koran, radio dan tivi yang memberitakannya, satu dalam penilaian terhadap Soeharto. Semuanya menggunakan istilah "diktatur", "pembunuh rakyat Indonesia" [l'Humanité] , "kriminal", "pelanggar HAM", "melakukan génocide" [la Croix, 28 Januari 2008] dan "koruptor terbesar dalam zaman kita [Direct Soir, 28 Januari 2008]. Bahkan Harian Libération menggunakan istilah "kekuasaan yang setara dengan monarkhi" yang "membangun monarkhinya dengan tanpa ampun menghancurkan lawan-lawan potensialnya atau dengan membeli mereka" [28 Januari 2007].


Hampir semua media massa , baik radio, tivi, dan media cetak, tidak ada yang menggunakan istilah wafat , tapi "mati". Bahkan harian gratis "Direct Matin" yang diterbitkan oleh harian terkemuka Paris, Le Monde dengan sinis mengatakan ketika para peserta Pertemuan Anti Korupsi PBB yang berlangsung di Bali sekarang, berdiri sejenak memberi penghormatan kepada Soeharto sebagai "adegan surealis" [29 Januari 2008]."


Harian Le Monde, Paris, satu-satunya harian yang menerbitkan sehalaman penuh tulisan mantan koresponden Asia Tenggaranya, Jean-Claude Pomonti, malah mensejajarkan kekuasaan Soeharto sebagai kekuasaan"kerajaan Jawa". Dan ujar Pomonti yang lama berdiam di Bangkok: "Yang pasti "kerajaan Jawa" begini tak akan pernah berhasil". [29 Januari 2008].


Pomonti juga melihat bahwa kemelut yang dihadapi Indonesia sekarang tidak lain dari peninggalan Soeharto selama tiga dasawarsa lebih. Masalah-masalah ini tadinya seperti bara dalam sekam dan sekarang muncul ke permukaan. Mantan koresponden Le Monde untuk Asia Tenggara ini juga menyebut rezim Soeharto merupakan "salah satu pemerintahan yang paling berdarah dan paling korup pada paro kedua abad ke-XX" [l'un des gouvernants les plus sanguinaires et les plus corrompus de la deuxième moitié du XX siècle]. Rezim diktatur Soeharto jugalah, ujar Pomonti, yang menenggelamkan Indonesia ke genangan hutang.


Soeharto juga dihubungkan benar oleh media massa Perancis dengan penindasan dan pembunuhan di Timor Timur sambil mengingatkan tuntutan José Ramos Horta, sekarang presiden Timor Leste, agar diktatur Soeharto "diadili atas genosid" yang ia lakukan selama berkuasa.[La Croix, 28 Januari 2008].


Membaca pers Perancis dan mendengar siaran radio serta tivi negeri ini, tak ada sepatah kata sifat baik apa pun yang diucapkan tentang Soeharto bahkan kata wafat [décédé] pun tidak digunakan. Yang digunakan adalah kata "mati" [mort]. Tentu saja berita kematian Soeharto diketahui masyarakat melalui media massa. Dalam konteks ini aku teringat cerita Judith yang menerima sms dari saudaranya di Indonesia: "Soeharto mati, tante meninggal". Sms yang melukiskan secara spontan perasaan dan pikiran masyarakat bawahan pada Soeharto. Pengirim sms sadar benar nuansa kata "mati" dan "meninggal" atau wafat, berpulang.


Ketika aku servis malam di Koperasi Restoran Indonesia, tidak sedikit pelanggan yang bertanya: "Mantan presiden kalian baru meninggal iya?".


Tanpa mengulas pertanyaan ini, karena sedang sibuk, aku hanya menjawab singkat: "Iya".

"Mudahan kediktaturan tidak terulang lagi di negeri Anda".


"Iya, itu pun harapan rakyat negeriku, hanya barangkali jalannya masih tidak mulus sebagaimana halnya dengan jalan harapan ".


"Demikianlah hidup. C'est la vie, anak muda. Apalagi di dunia politik", ujar pelanggan tuaku, tanpa rambut dan mengenakan jas warna oranye. "Yang penting adalah bagaimana bisa belajar dari masa silam untuk kepentingan hari ini dan esok", tambahnya.


Aku mengucapkan terimakasih atas perhatian, harapan dan kata-kata baiknya. Oleh perhatian, harapan dan kata-kata baik dari seorang asing ini kepadaku yang terpental dari negeri kelahiran, membuatku merasa bahwa jarak Paris-Jakarta, antara Rue de Vaugirard dan jalan Tawes, tak terlalu jauh-jauh juga, dan betapa dunia makin menjadi sebuah "desa kecil", di mana peduduknya saling bersentuhan bagai tetangga. Kepentingan mereka pun tak terkurung pada batas geografis satu dua negara, tapi saling taut-menaut seperti yang sering dikatakan di sini "Agir ici et là" [Bertindak di sini sama dengan bertindak untuk di sana]. Mereka ada di satu "desa kecil dunia" bernama " kemanusiaan yang tunggal", jika menggunakan ungkapan Paul Ricoeur.


Ah, sungguh, aku mau jadi manusia yang manusiawi walau pun sebagai pekerja kasar biasa pada sebuah koperasi restoran.***



Paris, Musim Dingin 2008.
-----------------------------------
JJ. Kusni, pekerja biasa pada Koperasi Restoran Indonesia, Paris.

* * *


Soeharto di Giribangun, Hatta di Tanah Kusir

Oleh Farid Gaban | Madina

Meski sama-sama dari debu kembali menjadi debu, jasad Soeharto ditanam
ke tanah dengan cara yang jarang dialami manusia mati lainnya di
negeri ini. Dia dimakamkan secara istimewa. Cara penguburan ala
raja-raja. Bahkan mungkin lebih dari sekadar raja. Dan itu
mencerminkan kebingungan kolektif kita sebagai bangsa.

Meski semasa berkuasa dia disebut presiden, Soeharto memang pada
dasarnya raja. Atau setidaknya itulah citra yang ingin ditampilkannya
sebagai bagian dari watak kepemimpinannya. Soeharto dikuburkan di
Makam Astana Giribangun, atau Mangadeg, yang dibangun berdekatan makam
raja-raja Mataram, secara harafiah, dan dipersiapkan jauh ketika dia
masih hidup sehingga menimbulkan kontroversi ramai sejak 1970-an.

Perlakuan kepada tubuh mati Soeharto bahkan melebihi perlakuan kepada
petinggi keraton. Prosesi pemakamannya, yang disiarkan langsung oleh
semua saluran televisi, tak cukup melibatkan iring-iringan mobil atau
kereta kencana, tapi juga iring-iringan pesawat Hercules, Fokker dan
Boeing.

Jutaan orang, menurut sebuah berita di koran, menghormati jenazahnya
dan menangisi kepergiannya. Pemerintah menetapkan hari berkabung
nasional dan mengajak rakyat mengibarkan Merah-Putih setengah tiang
selama tujuh hari tujuh malam. Dan Partai Golkar, kendaraan politiknya
di masa Orde Baru, mengusulkannya menerima gelar pahlawan nasional.

Betapa kita semua cepat lupa. Sepuluh tahun lalu, dia dipaksa mundur
dari kursi presiden secara gegap-gempita dalam pengertian sebaliknya.
Soeharto jatuh diwarnai krisis ekonomi menyengat, dengan darah serta
ratusan kematian sebagai tumbalnya. Melalui sebuah tikungan tajam
bersejarah, sistem yang dipeliharanya selama tiga dasawarsa, yakni
sistem Orde Baru, dihujat sebagai simbol korupsi, kediktatoran, dan
sebuah orde yang usang.

Soeharto sendiri lalu diadili, baik melalui mahkamah formal (yang
tidak bisa menuntaskannya) maupun mahkamah informal di warung kopi dan
tajuk rencana koran. Kejatuhannya kala itu menandai sebuah perubahan
besar, atau reformasi, istilah yang kini kedengaran klise.

Seperti ditempatkan dalam roller-coaster liar, Soeharto pernah
dielu-elukan sebagai raja, dijatuhkan menjadi pariah dan belakangan
dipuja lagi menjadi raja. Betapa fana suasana hati, yang pada dasarnya
mencerminkan kebingungan tragis bangsa ini.

Sialnya, tragedi seperti ini bukan hal baru. Presiden sebelumnya pun,
Soekarno, pernah mengalaminya. Bung Karno dipuja hampir sepanjang Orde
Lama, bahkan belakangan dinobatkan menjadi presiden seumur. Lalu
melalui peristiwa yang sangat berdarah dia dijatuhkan ke comberan
sejarah. Namun akhirnya, kembali Bung Karno tampil in absentia dalam
panggung politik era Reformasi mengantarkan putrinya, Megawati
Soekarnoputri, menjadi presiden.

Kisah hidup Soekarno dan Soeharto adalah kisah dua raja. Dalam tragedi
yang jarang disadari, negeri berpenduduk ratusan juta ini hanya
mengenal dua presiden selama setengah abad sejak kemerdekaannya. Fakta
ini secara tepat melukiskan bahwa Indonesia pada dasarnya sebuah
kerajaan yang menyaru jadi republik.

Dalam konteks itulah Reformasi 1998 sebenarnya memiliki makna penting.
Melalui amandemen konstitusi muncul perubahan sikap mendasar dalam
kita memperlakukan para presiden. Pembatasan masa jabatan presiden
hanya dua kali, atau 10 tahun, memperkecil peluang munculnya raja
baru. Dan mempersempit peluang kita sebagai bangsa terjatuh pada
tragedi yang sama berulang-ulang.

Dan pada dasarnya kita memang tidak membutuhkan seorang raja untuk
memimpin negeri ini. Dalam sebuah tulisan pendeknya yang terkenal,
Demokrasi Kita, Bung Hatta telah lama mengingatkan kita akan bahaya
dari sebuah sistem feodalistik. Dia mengkritik baik publik yang memuja
maupun Soekarno sendiri yang menciptakan sistem kenegaraan dengan
nuansa kental kharisma pribadi. "Sistem yang menyandarkan diri pada
orang per orang," tulis Hatta, "takkan bertahan lama, mudah roboh
seperti rumah dari kartu."

Sistem feodalistik membawa kebiasaan nepotisme, perkoncoan dan
perkerabatan ke dalam pemerintahan, yang merupakan ladang subur
korupsi. Dan Soeharto, seperti kita tahu, ternyata tidak belajar dari
Bung Hatta. Dia mengulangi kesalahan Bung Karno. Rumah kartu Soeharto
roboh bahkan ketika dia masih hidup, persis seperti ramalan Hatta.

Kebiasaan lama tidak mudah luntur. Sepuluh tahun setelah reformasi,
keluarga Soeharto dan publik yang kini memuja jasadnya juga tidak
belajar dari Hatta yang menerapkan prinsip egalitarianisme baik dalam
hidup maupun matinya. Hatta menolak dikuburkan di Makam Pahlawan. Dia
berwasiat untuk dimakamkan di Tanah Kusir, makam orang kebanyakan.
Bukan makam raja-raja, baik asli maupun bikinan.***

* * *

Tempo Interaktif, 30 Januari 2008

Para Korban Orde Baru Masih Simpan Data Kejahatan Soeharto
Para korban yang merasa dirugikan atas kebijakan mendiang mantan Presiden Soeharto saat ini masih menyimpan daftar data-data kejahatan yang diduga dilakukan mantan penguasa itu. Para korban ini tergabung dalam Lembaga Perjuangan Rehabilitasi Korban Rezim Orde Baru (LPR-KROB)

"Data kejahatan kemanusiaan Soeharto sangat banyak. Kami masih menyimpan itu dan kalau ada yang kurang lengkap sangat mudah mencarinya," kata Ketua Lembaga Perjuangan Rehabilitasi Korban Rezim Orde Baru (LPR-KROB) Semaun Utomo kepada Tempo di Semarang, Rabu (30/1).

Semaun belum bisa menjelaskan secara detail data-data kejahatan Soeharto itu. Dia hanya mengungkakan sekelumit cerita tentang kejahatan Soeharto, seperti adanya pemenjaraan sebanyak 3 juta orang tanpa pengadilan yang jelas, pembunuhan korban di Aceh, dan masih banyak lagi.

"Ini rentetan sejarah yang tidak bisa dilupakan karena merupakan kejahatan kemanusiaan yang dilakukan penguasa, maka pengadilan seharusnya wajib mengadili Soeharto," katanya.

Semaun menyatakan siap memberikan data-data kejahatan yang dilakukan Seoharto itu kepada pihak-pihak yang membutuhkan. Data-data tersebut, kata dia, sangat kuat jika digunakan untuk proses pengadilan dalam rangka mengusut perkara Soeharto.

Namun, Semaun tampaknya masih ragu atas kredibilitas pengadilan yang sebenarnya wajib untuk mengadili Soeharto. "Pengadilan masih dipenuhi orang-orang berwatak orde baru," katanya.

Semaun mengatakan, meski Soeharto sudah meninggal tapi proses pengadilan terhadapnya tetap harus dilanjutkan. Proses pengadilan bisa dilakukan dengan cara pengadilan in absentia, atau tanpa kehadiran terdakwa. "Bagaimana pun Seoharto harus tetap diadili," katanya.

* * *


Jawapos , 30 Januari 2008,

Jaksa Diperintahkan Cari Surat Waris

dari Lurah


Gugatan ke Soeharto Beralih ke Putra Putrinya

JAKARTA - Mangkatnya Soeharto membawa perubahan pada sidang gugatan kejaksaan terhadap mantan presiden itu. Dalam sidang lanjutan di PN Jakarta Selatan (29/1), majelis hakim menetapkan pengalihan status Soeharto selaku tergugat kepada para ahli warisnya.

Yang penting lagi, majelis yang diketuai Wahjono memerintahkan jaksa pengacara negara (JPN) menentukan ahli waris yang menggantikan kedudukan sebagai tergugat. "Kami minta JPN mengajukan bukti-bukti untuk menentukan siapa saja ahli waris tersebut. Ini kewajiban penggugat," kata Wahjono dalam sidang.

Wahjono lantas membeberkan sejumlah dasar hukum terkait pengalihan kedudukan tergugat tersebut. Yakni, pasal 1813 KUH Perdata, pasal 1194 KUH Perdata, serta Keputusan Ketua MA No 53K/SIP/1967 dan No 429K/SIP/1971.

"Dari sejumlah ketentuan tersebut, kuasa seseorang otomatis gugur apabila pemberi kuasa meninggal dunia. Selanjutnya, gugatan dapat dialihkan kepada ahli warisnya," jelas Wahjono yang didampingi dua hakim anggota.

Menurut Wahjono, penentuan ahli waris dapat dilakukan dengan menggunakan surat keterangan dari kelurahan dan diketahui camat setempat. "Cukup lurah dan camat sebagai pencatat ahli warisnya," terang Wahjono.

Agenda sidang seharusnya membacakan kesimpulan dari kubu jaksa pengacara negara (JPN) dan kubu pengacara Soeharto. Namun, karena suasana berkabung, majelis menunda sidang hingga 12 Februari 2008.

Tak hanya kepada JPN, majelis juga meminta tim pengacara membantu penentuan siapa saja ahli waris tersebut. Namun, pengacara Soeharto O.C. Kaligis menolak permintaan majelis tersebut. Dia beralasan, kuasa tergugat berakhir dengan meninggalnya sang klien. "Kami keberatan dengan permintaan tersebut. Sebab, dalam perundang-undangan, tidak ada kewajiban kami melanjutkan gugatan. Kami justru meminta perkara ini ditutup," tegas Kaligis.

Namun, sebaliknya, JPN Johannes Tanak balik memprotes pernyataan Kaligis. Dia menegaskan apa yang disampaikan Kaligis dianggap tidak ada. "Mereka tidak berhak berbicara karena kuasa mereka gugur dengan meninggalnya tergugat I (Soeharto)," jelas Johannes.

Setelah sidang, Johannes menegaskan bahwa tim jaksa akan melaksanakan penetapan majelis dengan menyiapkan surat pernyataan berisi nama-nama ahli waris Soeharto. "Semua putra putrinya adalah ahli waris," kata Johannes. Untuk memastikan aspek legalitas para ahi waris tersebut, lanjut Johannes, JPN akan berkoordinasi dengan Pengadilan Agama (PA) Jakarta Pusat.

Ditanya kelanjutan sidang, Johannes menjawab, "Kami akan tetap bersidang selama tidak ada perintah pencabutan SKK (surat kuasa khusus) dari presiden."

Usai sidang, kuasa hukum Soeharto yang lain, Juan Felix Tampubolon mengatakan bahwa majelis seharusnya tidak mengeluarkan penetapan tentang ahli waris kliennya. Tapi, menurut dia, itu harus inisiatif penggugat. "Majelis seharusnya pasif. Ini kan perkara perdata," kata Juan Felix.

Gugatan tersebut diajukan terhadap Soeharto dan Yayasan Supersemar. Diduga, di yayasan yang pernah diketuai mantan penguasa Orde Baru itu terjadi penyelewengan. JPN menuntut pengembalian dana USD 420 juta dan Rp 185,92 miliar, plus ganti rugi imateriil Rp 10 triliun.

JPN menganggap, yayasan awalnya bertujuan menyalurkan beasiswa kepada mahasiswa kurang mampu sejak 1978. Dana yayasan itu berasal dari duit bank-bank pemerintah dan masyarakat.

Semasa diketuai Soeharto, menurut JPN, banyak dana yayasan yang dialirkan untuk kepentingan bisnis kroni-kroni keluarga Cendana. Itu jelas bertentangan dengan PP No 15 Tahun 1976 tentang Penetapan Penggunaan Sisa Laba Bersih Bank-Bank Milik Pemerintah, yang kemudian diatur dengan Keputusan Menteri Keuangan Nomor 373/KMK.011/1978, serta pasal 3 Anggaran Dasar Yayasan Supersemar, seharusnya uang yang diterima disalurkan untuk beasiswa pelajar dan mahasiswa.

Selain itu, penyelewengan dana tersebut, menurut JPN, merupakan perbuatan melawan hukum sesuai dengan pasal 1365 KUHPerdata. Nah, selanjutnya, nilai gugatan setara dengan total kerugian negara akibat kebijakan Soeharto. (agm/roy)



* * *

SUARA PEMBARUAN DAILY

Perdata Soeharto Dilanjutkan
Selain korupsi, kasus pelanggaran hak asasi manusia (HAM) berat di masa Soeharto juga harus diusut.

SP/YC Kurniantoro

[JAKARTA] Juru Bicara Kepresidenan Andi Mallarangeng menegaskan, harta kekayaan milik negara yang diambil siapa pun, termasuk mantan Presiden Soeharto, harus dikembalikan kepada negara. Karena itu, kasus perdatanya tetap bisa dilanjutkan untuk mengembalikan harta milik negara tersebut.

Penegasan tersebut disampaikannya di Jakarta, Senin (28/1), terkait sikap pemerintah terhadap kasus hukum mantan Presiden Soeharto yang meninggal dunia Minggu (27/1) lalu, di Rumah Sakit Pusat Pertamina (RSPP) Jakarta. Namun, dia tidak men-jelaskan lebih jauh sikap pemerintah tersebut.

Secara terpisah, Jaksa Agung, Hendarman Supandji, di sela-sela Konferensi PBB tentang Pemberantasan Korupsi, di Nusa Dua, Bali, Senin, secara singkat juga menegaskan, kasus perdata almarhum Soeharto tetap dilanjutkan.

Menteri Luar Negeri Hassan Wirajuda, di konferensi yang sama menolak berkomentar mengenai proses hukum mantan Presiden Soeharto, terutama terkait dengan laporan PBB mengenai inisiatif pengembalian harta negara yang dicuri penguasa. Saat konferensi pers, Menlu banyak mendapat pertanyaan dari wartawan dalam dan luar negeri tentang kelanjutan kasus hukum Soeharto. Namun, Menlu memilih untuk tidak menanggapi pertanyaan itu.

Menurut Hassan, saat ini sebaiknya tidak membicarakan proses hukum Soeharto. Ia juga menyampaikan pesan Presiden Susilo Bambang Yudhoyono yang meminta publik untuk sementara menghentikan perdebatan seputar kasus hukum Soeharto. Namun, Pemerintah Indonesia, kata Menlu, tetap akan menghargai sistem hukum yang berlaku.


Jangan Dihentikan

Terkait hal tersebut, praktisi hukum Todung Mulya Lubis mengingatkan pemerintah untuk tidak menghentikan proses hukum Soeharto melalui mekanisme gugatan perdata. Dengan meninggalnya Soeharto, bukan berarti proses hukumnya berhenti begitu saja.

Todung cenderung menyalahkan pemerintah, yang selama 10 tahun menunda proses hukum terhadap Soeharto, keluarga, dan kroninya. "Akibatnya sejarah Indonesia diwarisi pertanyaan besar mengenai letak peranan Soeharto dalam perjalanan kita sebagai bangsa," ujarnya.

Senada dengan itu, Direktur Lembaga Bantuan Hukum (LBH) Jakarta, Asfinawati mendesak Kejaksaan Agung (Kejagung) segera melanjutkan gugatan perdata terhadap sejumlah kasus korupsi yang dilakukan Soeharto. Dalam gugatan perdata itu, Kejagung, sebagai pengacara negara, harus mengajukan permohonan sita jaminan terhadap semua harta Soeharto yang diduga berasal dari hasil korupsi.

"Kalau tidak segera dilakukan penyitaan jaminan, bisa saja harta-harta itu diklaim oleh anak-anak serta kroni-kroninya," katanya, di Jakarta, Selasa (29/1).

Asfin menyayangkan Kejagung yang tidak dari dulu menyita harta Soeharto. Menurutnya, Kejagung akan berani menggugat dan menuntut Soeharto secara perdata, serta menuntut anak-anak serta kroni-kroninya secara pidana dan perdata, kalau ada dukungan penuh dari Presiden.

Selain korupsi, kasus pelanggaran hak asasi manusia (HAM) berat di masa Soeharto juga harus diusut. Banyak orang dibunuh di zaman Soeharto, bukan hanya dilakukan Soeharto, tetapi oleh kroni-kroninya.

Senada dikatakan Ketua Badan Pengurus SETARA Institute, Hendardi, mengatakan, kasus korupsi dan pelanggaran yang HAM yang dilakukan Soeharto, anak-anak, dan kroninya harus diusut tuntas. Menurut Hendardi, secara pidana kasus hukum Soeharto sudah gugur karena dia sudah meninggal. Namun, secara perdata bisa melalui ahli warisnya.

"Hingga mengembuskan napas terakhir, Soeharto tidak memperoleh kepastian hukum yang melilitnya. Kematiannya menyisakan kontroversi dan preseden buruk penegakan hukum di Indonesia," kata dia.

Menurut Hendardi, di masa pemerintahan saat ini, diskriminasi penegakan hukum telah terjadi, sebagai akibat ketidakmampuan Presiden menciptakan terobosan untuk menuntaskan kasus hukum semasa Soeharto masih hidup.


Bukan Pengacara Soeharto

Secara terpisah, salah satu kuasa hukum Soeharto, M Assegaf menegaskan, secara pidana, kasus hukum Soeharto sudah selesai, karena Soeharto sudah wafat. "Dan saya tegaskan, Tap MPR No XI yang menyatakan pengadilan terhadap Soeharto, sudah dilaksanakan. Karena Soeharto sudah diselidiki, disidik, diajukan ke penuntutan, namun dikeluarkan Surat Ketetapan Penghentian Penuntutan Perkara (SKP3). Jadi sepanjang menyangkut Soeharto, sudah selesai," tegasnya.

Assegaf mengungkapkan pula, dengan meninggalnya Soeharto, maka ia dan pengacara lain yang menerima kuasa dari Soeharto, dengan sendirinya gugur secara hukum. "Kami tidak bisa sebagai kuasa almarhum (Soeharto) lagi, karena beliau sudah meninggal dunia. Demikian KUH Perdata mengaturnya," jelas Assegaf. [A-21/137/E-8]
* * *

Jawapos 30 Januari 2008,

Bias Berita Media soal Wafatnya Soeharto

Oleh Nurudin



Mengamati pemberitaan media massa (cetak dan elektronik) kita sebelum dan sesudah meninggalnya mantan Presiden Soeharto, saya ikut merinding. Secara kuantitas, hampir semua media massa sudah kehilangan daya kritisnya. Semua pemberitaan nyaris seragam, memberitakan hal yang positif tentang mantan pengusa Orde Baru (Orba) tersebut.

Itu tak bermaksud, media massa harus memberitakan sisi negatif saja. Hanya yang agak aneh, keberpihakan media sangat terasa. Tak kalah anehnya adalah pemberitaan seputar pemakaman Soeharto.

Apakah itu hanya terjadi pada media massa dalam negeri? Tidak. Media massa luar negeri justru lebih ngeri karena hanya memberitakan sisi buruk Soeharto tanpa melihat sisi positif yang selama ini sudah diberikan kepada bangsa dan negara ini.

Washington Post menurunkan tulisan dengan judul In Indonesia’s Ex-Dictator Soeharto. Sementara itu, ada pula judul-judul yang dibuat; Buried, Soldier, Savior, Strongman, Crook (Newsweek), Suharto, Tyran of Indonesia, Dies Without Facing Justice (The Independent), President Soeharto, 86; Indonsian Ruler Left Mixed Legacy of Prosperity and Untold Deaths (LA Times), Suharto, Former President of Indonesia and Brutal Rules, Dies (Timesonline), dan lain-lain. Yang jelas, gambaran judul beberapa media asing itu terkesan negatif dan memojokkan mantan presiden ke-2 RI tersebut.

Melihat perbedaan yang mencolok pemberitaan media massa dalam dan luar negeri tersebut, kita merenung bagaimana media massa sampai terbawa arus untuk memihak salah satu kecenderungan saja? Di sinilah kita perlu mendiskusikan tentang objektivitas media.

Objektivitas

Berbicara tentang objektivitas media massa, kita bisa memakai perspektif yang pernah diajukan Westerstahl (McQuail, 2000). Dia pernah mengatakan, objektif itu harus mengandung faktualitas dan imparsialitas. Faktualitas berarti kebenaran yang di dalamnya memuat akurasi (tepat dan cermat) dan mengaitkan sesuatu yang relevan untuk diberitakan (relevansi). Sementara itu, imparsialitas mensyaratkan adanya keseimbangan (balance) dan kenetralan dalam mengungkap sesuatu.

Objektivitas selalu mengandung kejujuran, kecukupan data, benar dan memisahkan diri dari fiksi dan opini. Ia juga perlu untuk menghindarkan diri dari sesuatu yang hanya mengejar sensasional.

Apa yang dikemukakan Westerstahl tersebut tidak mudah untuk diwujudkan. Media massa tidak lepas dari subjektivitas atau subjektivitas yang objektif. Subjektivitas dilakukan jika media massa memberitakan suatu kejadian yang tidak pernah terjadi.

Subjektivitas yang objektif terjadi ketika media massa secara terang-terangan atau tidak cenderung membela salah satu pihak yang sedang diberitakan. Pemberitaannya berdasar fakta-fakta yang terjadi (objektif), tetapi penulisannya secara subjektif.

Jika kita memakai kriteria tersebut, media massa yang memberitakan kasus Soeharto nyata tidak objektif lagi. Ada nuansa bias yang mengiringi penulisan beritanya. Itu sangat terlihat ketika televisi-televisi Indonesia mulai Minggu (27/1) siang sampai Senin (28/1) menyoroti Pak Harto. Hampir semua televisi hanya memberitakan sisi positif Pak Harto.

Bahkan, beberapa stasiun televisi kita memilih narasumber yang mengulas kematiannya kebanyakan berasal dari orang yang pernah punya hubungan dekat dengan mantan penguasa Orba itu.

Artinya, kita hampir tak pernah disuguhi pemberitaan bagaimana tanggapan masyarakat Kedungombo (misalnya) ketika harus terusir dari tanah kelahirannya tentang kematian Pak Harto.

Bagaimana pula, komentar keluarga yang menjadi korban "Tragedi Trisakti 1998". Seolah semua sudah di-setting hanya memberitakan seputar permukaan dan tidak banyak menggali hal-hal yang bernuansa kritis. Bisa jadi, itu disebabkan kelemahan televisi untuk memunculkan fakta atau data yang melengkapi beritanya.

Tetapi, sebenarnya televisi bisa menugaskan reporter untuk menginvestigasi ke lapangan di luar "acara resmi" pemakaman Pak Harto. Kenyataannya, bukankah acara televisi tentang berita kematian Soeharto nyaris seragam?

Idealnya

Pamela J. Shoemaker dan Stephen D. Reese dalam bukunya Mediating The Message (1996) pernah melihat mengapa media massa bisa mempunyai perbedaan dan persamaan dalam liputannya. Ada beberapa tahap yang memengaruhinya; (1) individual level, (2) media routine level, (3) organizational level, (4) extramedia level, dan (5) ideological level.

Jadi, betapa media massa bukan sebuah lembaga yang independen jika dilihat dari pemberitaannya. Semua berita akan terpengaruh oleh banyak hal di dalam dan luar media massa itu sendiri. Apalagi jika ditambah dengan level lain, yakni kemalasan reporter untuk melakukan investigasi ke lapangan. Reporter hanya senang dengan berita-berita berdasar realitas psikologis (sumber-sumber resmi) yang memunculkan jurnalisme kutipan saja dan tidak berdasar realitas psikologis (melakukan investigasi langsung dan melihat serta menceritakan fakta-fakta di lapangan).

Lepas dari kenyataan tersebut, ada beberapa catatan yang layak untuk terus disodorkan kepada media massa kita. Pertama, dalam situasi apa pun, media massa harus tetap memegang teguh peliputan cover both sides (meliput dua sisi yang berbeda secara seimbang). Bukan menjadi all sides.

Kedua, media massa sebaiknya memosisikan dirinya sebagai (meminjam istilah Jakob Oetama, 2003) the search dan the production of meaning. Media massa dituntut untuk tak sekadar memberitakan fakta apa adanya secara linear, tetapi fakta yang mencakup. Dengan kata lain, fakta perlu dilengkapi dengan latar belakang, proses, dan riwayatnya serta tali temali yang berkaitan dengan fakta tersebut.

Ketiga, media massa harus menjadi penentu arah perubahan masyarakat dan bukan sekadar memberitakan fakta telanjang. Tentu saja, arah perubahan yang positif di masa datang. Ketika memberitakan kasus wafatnya Pak Harto, media massa tidak harus terjebak pada berita-berita seremonial, tetapi juga mampu mengarahkan, mempertanyakan, menggelitik pembaca bagaimana penyelesaian "kesalahan" Pak Harto di masa lalu. Itu tak berarti tidak ikut berduka cita, hanya media massa tetap punya tugas mulia seperti itu.


Nurudin, staf pengajar pada program pascasarjana Universitas Muhammadiyah Malang (UMM)



* * *

Lampungpost, 29 Januari 2008-01-30
Soeharto di Mata Pers


Eni MuslihahPenggiat Bengkel Jurnalisme, tinggal di Bandar Lampung

Hari Minggu, 27 Januari, sekitar pukul 13.10 WIB, dokter kepresidenan Republik Indonesia (RI) mengumumkan secara resmi wafatnya mantan Presiden RI, H. Muhammad Soeharto. Setelah kurang lebih tiga minggu Soeharto tergeletak tak berdaya di rumah sakit Pertamina Pusat Jakarta, akhirnya hari itu juga Soeharto tutup usia yang ke-87 tahun. Innalillahi wainna ilaihi rojiun.

Tak lama kemudian, Presiden RI Soesilo Bambang Yudhoyono (SBY) didampingi Wakil Presiden Yusuf Kalla, mengumumkan hari berkabung nasional selama tujuh hari atas wafatnya putra terbaik bangsa, Soeharto.Pada hari itu, hampir semua media elektronik tak henti-hentinya memberitakan perkembangan terbaru pasca wafatnya Soeharto. Rumah persemayaman di Jalan Cendana dipadati handai tolan, kerabat, dan warga yang ingin melihat secara langsung jasad terakhir Soeharto. Bahkan pagi harinya, ketika proses keberangkatan ke Solo, warga juga tampak antusias melihat secara langsung iring-iringan mobil jenazah Soeharto. Almarhum akan dimakamkan di Astana Giribangun, Solo, pemakaman yang sudah dipersiapkan bagi keluarga Seoharto.

Bisa dipastikan Jakarta macet total pada saat itu karena dipadati ribuan manusia yang hendak memberikan penghormatan terakhir pada mantan presidennya. Padahal, pada hari itu bukan hari libur nasional, bisa dibayangkan bagaimana jadinya, jika prosesi pemakaman soeharto bertepatan dengan hari libur.Antusiasme itu menujukkan bahwa masyarakat Indonesia masih bersimpati dengan mantan Presiden ke-2 RI Soeharto. Sebenarnya, kepergian Soeharto menghadap Sang Khalik sudah bisa diprediksikan publik sejak hari pertama beliau dirawat di RS Pertamina Pusat. Kondisi kesehatan beliau ketika itu naik turun. Bahkan, terakhir mengalami penurunan yang cukup drastis. Hanya tinggal menunggu waktu yang telah digariskan Sang Pencipta.Semua media menampilkan kilas balik semasa hidup beliau. Ada yang menyanjung selama kepemimpinannya, ada yang membantai tiada ampun, tetapi ada pula yang arif dan bijak memberikan maaf atas kesalahan yang telah diperbuatnya selama ia memimpin negeri ini.

Itulah memori rakyat ketika Soeharto memimpin Indonesia selama 32 tahun.Namun, dari sekian banyak kilas balik tentang Soeharto di semua media elektronik, ada yang membuat saya tertarik mendengar kesaksian yang ditayangkan Liputan 6 sore SCTV, yang bertajuk In Memoriam Pak Harto, ia seorang jurnalis kepresidenan di era Soeharto. Ia bernama Linda Djalil. Ia mengisahkan selama bertugas di istana kepresidenan, insan pers ditempatkan sebagai manusia berkelas tiga.Menurut dia, ketika itu tak satu pun wartawan berani menjulurkan tape recorder-nya di hadapan Presiden. Pernah satu ketika Soeharto didemo di negara Jerman, tetapi tak satu pun wartawan berani menanyakan bagaimana perasaan beliau yang dihina di negara orang meskipun Soeharto memberi kesempatan wartawan untuk bertanya. Rasa takut dan segan terhadap sosok Soeharto pun tampak dari menteri-menteri eranya ketika itu.Tidak ada istilah wartawan sejajar dengan narasumbernya ketika itu, tidak seperti sekarang, presiden sekalipun bebas kita kuliti selagi masih berdasarkan data dan fakta.

Ketika itu, wartawan dianggap manusia yang tak berarti. Bahkan, perlakuan yang sama pun didapatnya dari Kurawa-Kurawa Soeharto, dari sopir sampai tukang cuci piringnya pun memperlakukan wartawan tidak baik ketika itu. Ia juga mengisahkan, pernah suatu ketika para wartawan diundang pada acara buka puasa di kediaman keluarga Cendana, apa yang didapat? Para wartawan yang meliput mendapatkan kue kotak yang di dalamnya sudah dikerumuni semut. "Inilah akibat contoh yang tidak baik, sampai Kurawa-Kurawa-nya saja ikut memperlakukan kami seperti binatang," kata Linda, suaranya terdengar begitu geram. Padahal, pers merupakan ujung tombak bagi sebuah bangsa.Ingatan itu sepertinya begitu melekat dan sulit dilupakan dari ingatan Linda dan rekan-rekan jurnalis lainnya. Bagaimana bisa lupa? selain dibungkam, pers juga diperlakukan tidak manusiawi.

Itulah sekelumit cerita dari kekejaman seorang Soeharto, masih banyak cerita sedih lainnya, sepertinya jika didokumentasikan akan banyak cerita yang akan terus menghakimi Soeharto dan kelurganya yang semakin hari, semakin tidak memiliki kekuatan apa pun untuk membungkam rakyat-rakyatnya.***Hari-hari terakhir hidupnya, Soeharto meninggalkan persoalan hukum yang belum sempat ia selesaikan. Persoalan yang merugikan aset negara hingga mencapai triliunan rupiah tersebut menimbulkan kontroversi. Ada yang menghendaki Soeharto terus diadili, ada pula yang menghendaki kasus Soeharto diputihkan saja.

Secara pribadi, saya menghendaki kasus hukum Soeharto terus diusut hingga betul-betul menemui titik terang. Persoalan yang merugikan banyak orang ini, nantinya tidak akan berhenti sampai di dunia saja. Ada pengadilan yang lebih agung dan mahaadil kelak. Kesaksian di sana tidak akan terbantahkan.Maka dari itu, pengusutan perkara Soeharto di dunia sesungguhnya membawa kebaikan bagi Soeharto dan keluarganya. Artinya, hukum dunia membantu meringankan hukum akhirat. Dan terpenting adalah, negara kita sudah memberlakukan hukum yang adil pada siapa pun tanpa pandang bulu.

Dengan begitu, penguasa yang kini maupun yang akan datang berpikir dua kali untuk melakukan kesalahan yang sama. Kalaupun kasus Soeharto memang harus diputihkan, sekurang-kurangnya ada pengakuan dari pihak keluarga Soeharto dan membenarkan atas kesalahan yang telah diperbuat oleh ayah mereka dan kroni-kroninya. Syukur-syukur, kekayaan negara yang pernah diakuinya dikembalikan ke kas negara.

Tetapi, rasanya mustahil.Sekarang Soeharto sudah menghadap Ilahi, persoalan yang belum terselesaikan semoga bisa terselesaikan dengan arif dan bijaksana, baik dari keluarga maupun penyelenggara hukum negeri ini. Bantu pahlawan kita dengan cara memberikan kesaksian yang jujur. Dan, tepatkan diri kita pada titik nol. Dengan begitu, kita sebagai warga negara sudah meringankan ia dalam melaporkan pertanggungjawabannya sebagai pemimpin negeri ini kepada Sang Khalik. Selamat jalan pahlawanku, semoga pengabdianmu dalam membangun bangsa ini diterima oleh Allah swt. Terima kasih atas segalanya.


* * *

SUARA PEMBARUAN, 29 Januari 2008

Kebebasan Pers ala Soeharto


Perjalanan Pemerintahan Orde Baru di bawah kepemimpinan Soeharto memiliki sejarah tersendiri di bidang pers. Pada masa itu berulang kali pers diberedel baik secara bersama-sama maupun sendiri-sendiri. Harian Sinar Harapan, harian sore pertama yang terbit 27 April 1961 dan cikal bakal Harian Suara Pembaruan, di masa Orde Baru mengalami empat kali pemberedelan. Alasan pemberedelan beragam.


Pertama, pada 1965, tidak diizinkan terbit dari tanggal 2 sampai 7 Oktober 1965. Ketika itu situasi masih simpang siur setelah terjadi pemberontakan G30S. Agar tidak mengacaukan situasi, penguasa memerintahkan melarang terbit semua surat kabar di Jakarta, termasuk Sinar Harapan (SH). Hanya dua surat kabar, yakni Berita Yudha dan Angkatan Bersenjata, yang membawakan suara pihak Angkatan Bersenjata, tetap terbit.


Kedua, pada 1973, tepatnya 2 sampai 11 Januari, SH dilarang terbit oleh Pelaksana Khusus Panglima Komando Pemulihan Keamanan dan Ketertiban Daerah Jakarta Raya (Laksus Pangkopkamtibda Jaya) karena menyiarkan Rancangan Anggaran Pendapatan dan Belanja (RAPBN) sebelum disampaikan Presiden pada sidang DPR. pemberedelan kali ini hanya dilakukan terhadap SH.
Ketiga, 21 Januari-3 Februari 1978, karena memberitakan aksi-aksi mahasiswa tujuh koran Ibukota diberedel, yakni SH, Kompas, Pelita, Merdeka, Pos Sore, Indonesia Time, dan Sinar Pagi.


Ketika itu, Jumat 20 Januari sekitar pukul 20.21 WIB, redaktur piket SH menerima telepon dari Letkol Anas Malik, Kepala Penerangan Laksusda Jaya, pesannya singkat, "Surat kabar Sinar Harapan mulai Sabtu 21 Januari 1978 diperintahkan tidak terbit."
Keempat, 9 Oktober 1986, SH kembali diberedel, karena Menteri Penerangan (Menpen) ketika itu, Harmoko, menilai bahwa tulisan-tulisan di SH, baik sengaja atau tidak disengaja, umumnya menciptakan suasana yang serba suram, merisaukan, membingungkan, dan meresahkan masyarakat. Singkat kata, gaya pemberitaan SH dinilai tidak sesuai dengan hakikat kebebasan pers yang bertanggung jawab.


Tanggal 9 Oktober itulah keluar SK Menpen yang membatalkan SIUPP SH. Sejak pemberedelan itu SH tidak dizinkan terbit. Awal Februari 1987 pemerintah mengizinkan terbit lagi, namun tidak boleh menggunakan nama Sinar Harapan. Para pemimpin SH yang dianggap bertanggung jawab atas pemberitaan, tidak boleh duduk sebagai pemimpin di koran yang baru. Maka terbitlah harian sore Suara Pembaruan dengan pimpinan dan nama perseroan terbatas (PT) yang baru.


Kritik dan koreksi terhadap kebijakan pemerintah akan berhadapan dengan pihak keamanan atau instansi sipil, yakni Departemen Penerangan, yang sudah terbiasa mengatasnamakan keamanan untuk melakukan teguran, panggilan, interogasi, hingga koran diberedel untuk beberapa hari, minggu, bahkan bulan, atau seterusnya, seperti yang di- alami SH dan beberapa koran lain.


Pers sangat dikekang, kritik terhadap pemerintah harus pintar-pintar diukur, sehingga tidak membuat kuping penguasa menjadi "merah", sekalipun kritik disertai fakta. Penggambaran pengekangan terhadap pers dapat dilihat dengan munculnya eufemisme kata. Misalnya, saat pers mengatakan penduduk kelaparan, mengalami busung lapar, maka pemerintah akan mengatakan istilah itu tidak tepat. Yang benar adalah kekurangan gizi, dan sebagainya.


Untuk mengontrol pers, pemerintah membuat peraturan, bahwa setiap media massa harus memiliki Surat Izin Terbit (SIT) yang dikeluarkan oleh Departemen Penerangan. SIT dikeluarkan apabila mendapatkan rekomendasi dari berbagai pihak, di antaranya Dewan Pers dan Laksusda. Ancaman atas kebebasan pers yang diatur dengan istilah, "Pers yang Bebas dan Bertanggung Jawab", dengan mudah dipelintir rezim Orde Baru pimpinan Soeharto, menurut kepentingannya. Mengacu pada istilah itu pula, maka Sinar Harapan diberangus dan dipaksa lahir kembali dengan nama Suara Pembaruan. [Agnes Samsoeri]
