---
layout: post
title: SKP3 Suharto tidak sah, penuntutan harus diteruskan
date: 2006-06-15
---

Putusan Pengadilan Negeri Jakarta Selatan pada tanggal 12 Juni 2006, yang menegaskan bahwa surat penghentian penuntutan perkara atas nama Soeharto tidak sah, dan juga menyatakan bahwa penuntutan perkara Soeharto dibuka dan dilanjutkan, merupakan peristiwa penting bagi sejarah dunia hukum dan peradilan di Indonesia. Tidak dapat disangkal bahwa putusan pengadilan ini merupakan tamparan yang tidak tanggung-tanggung kerasnya bagi mereka yang selama ini masih terus memuja-muja dan memuji-muji Siharto, petinggi-petinggi Golkar dan TNI—AD, atau pendukung Orde Baru umumnya.

Kiranya, putusan Pengadilan Negeri Jakarta ini pantas dijadikan bahan renungan bagi semua orang, dan juga dijadikan bahan studi bagi kalangan hukum dan peradilan, termasuk fakultas-fakultas dalam universitas di seluruh negeri, dan bagi banyak organisasi yang peduli kepada penegakan hukum dan keadilan. Sebab, buntut putusan Pengadilan Jakarta Selatan ini akan masih panjang dan banyak liku-likunya.

Dengan putusan Pengadilan Jakarta Selatan ini berarti bahwa keinginan Kejaksaan Agung untuk menutup kasus korupsi mantan Presiden Soeharto kandas di pengadilan. Hakim menilai penerbitan SKP3 yang ditandatangani pada 11 Mei 2006 tidak tepat dan prematur. Alasan penghentian penuntutan perkara juga tidak sah menurut hukum. Penghentian penuntutan perkara yang dilakukan kejaksaan tidak berdasarkan tiga kondisi yang disyaratkan ketentuan Pasal 140 Ayat (2) Huruf a Kitab Undang-undang Hukum Acara Pidana (KUHAP).

Kejaksaan menggunakan dasar kondisi kesehatan terdakwa (Suharto) yang tidak layak disidangkan. Padahal, alasan menghentikan penuntutan perkara demi hukum adalah karena terdakwa meninggal, nebis in nidem (terdakwa disidangkan dua kali dalam perkara yang sama), serta kedaluwarsa. "Kejaksaan sebagai lembaga penuntut umum selayaknya hati-hati untuk tidak leluasa melakukan interpretasi atau penafsiran undang-undang," kata hakim. (Kompas, 12 Juni 2006)

Putusan pra-peradilan ini harus dijunjung tinggi
Permohonan praperadilan terhadap SKP3 atas nama terdakwa Soeharto diajukan Asosiasi Penasihat Hukum dan Hak Asasi Manusia Indonesia, Koalisi Gerakan Masyarakat Adili Soeharto (Gemas), dan Aktivis 98. Permohonan itu berkenaan dengan diterbitkannya SKP3 11 Mei 2006 atas nama terdakwa Soeharto dalam kasus dugaan korupsi pada tujuh yayasan yang didirikannya.

Dalam pertimbangannya, majelis hakim menyebutkan, perkara pidana atas nama terdakwa Soeharto sudah pernah dilimpahkan oleh termohon ke PN Jakarta Selatan, bahkan sudah sampai tingkat kasasi. Namun, proses hukumnya terhambat karena terdakwa sakit sehingga tidak dapat hadir dalam sidang. Putusan Mahkamah Agung Nomor 1846 K/Pid/2000 tanggal 2 Februari 2001, memerintahkan jaksa melakukan pengobatan terdakwa sampai sembuh atas biaya negara, untuk selanjutnya setelah sembuh dihadapkan ke persidangan.

"Pengadilan berpendapat, penghentian penuntutan perkara atas nama terdakwa Soeharto adalah bertentangan dengan putusan Mahkamah Agung tanggal 2 Februari 2001 sehingga penerbitan SKP3 tidak tepat dan prematur," kata hakim.

Ditemui seusai sidang, Ketua Badan Pengurus PBHI Johnson Panjaitan selaku pemohon mengatakan, putusan praperadilan ini harus dijunjung tinggi. Sidang pengadilan perkara Soeharto harus segera dibuka kembali. (Kompas, 12 Juni 2006)

Putusan yang sesuai dengan aspirasi rakyat
Dari banyaknya pernyataan oleh berbagai kalangan dalam masyarakat nyatalah bahwa permohonan praperadilan terhadap SKP3 atas Soeharto yang diajukan Asosiasi Penasihat Hukum dan Hak Asasi Manusia Indonesia, dan Koalisi Gerakan Masyarakat Adili Soeharto (Gemas), serta Aktivis 98 sesuai dengan aspirasi sebagian terbesar dari rakyat kita. Banyak golongan dalam masyarakat dan bermacam-macam organisasi menentang keputusan Jaksa Agung yang menghentikan penuntutan perkara atas Suharto ini. Termasuk berbagai aksi-aksi yang dilancarkan oleh generasi muda bangsa kita.

Putusan Pengadilan Ngeri Jakarta mengenai kasus Suharto ini patut sekali mendapat dukungan dari opini umum yang seluas-luasnya dan juga sekuat-kuatnya. Berbagai cara dan bentuk perlu terus dimanfestasikan oleh banyak kalangan untuk menunjukkan dengan tegas dan jelas bahwa tuntutan untuk mengadili Suharto atas dosa-dosa dan berbagai kejahatannya adalah sikap politik dan moral yang benar dan juga luhur.

Sikap Pengadilan Negeri Jakarta Selatan ini bertentangan dengan sikap banyak pembesar (sipil maupun militer), atau tokoh-tokoh politik, dan pemuka-pemuka agama, yang telah beramai-ramai menyuarakan secara lantang supaya perkara Suharto dihentikan, atau supaya Suharto diampuni, atau supaya Suharto tidak diajukan ke pengadilan.

Aksi-aksi perlu terus digalakkan
Meskipun Peegadilan Negeri Jakarta Selatan sudah mengambil putusan untuk menyatakan tidak sahnya tindakan Jaksa Agung yang menghentikan penuntutan perkara Suharto, tetapi tidak berarti bahwa berbagai usaha kalangan yang pro-Suharto akan berhenti begitu saja. Kejaksaan Agung sudah menyatakan akan minta banding kepada lembaga yang lebih tiggi. Pimpinan Partai Golkar, dan pimpinan TNI-AD, bersama-sama berbagai tokoh masyarakat (termasuk kalangan agama dan sebagian intelektual) akan terus berusaha mencegah diadilinya Suharto.

Dengan masih banyaknya unsur-unsur pro Orde Baru dalam berbagai lembaga kita yang setia kepada Suharto, maka usaha untuk diadilinya mantan pemimpin Orde Baru ini akan masih terus banyak mengalami halangan yang macam-macam. Kemungkinan bahwa putusan Pengadilan Negeri Jakarta Selatan akan “dibatalkan” oleh lembaga yang lebih tinggi (umpamanya, Mahkamah Agung) masih ada.

Oleh karena itu, segala macam aksi atau kegiatan untuk tetap menuntut diadilinya Suharto kiranya perlu terus dilancarkan oleh sebanyak mungkin kalangan masyarakat, bahkan perlu terus lebih digalakkan dari pada yang sudah-sudah. Karena, kalau direnungkan dalam-dalam dan juga dilihat dari banyak segi, menuntut diadilinya Suharto adalah tuntuntan yang sah secara moral, dan adil secara politik, dan dbenarkan oleh rasa keadilan. Sebaliknya, tidak meminta pertanggungan-jawab kepada Suharto atas segala kejahatan dan kesalahannya selama 32 tahun berkuasa, adalah sikap yang sama sekali salah, yang patut dikutuk oleh banyak orang, dan bisa dihujat oleh generasi-generasi yang akan datang.

Kalau ada orang yang mengatakan bahwa pengadilan terhadap Suharto hanyalah menjadi tuntutan para korban peristiwa 65, atau hanya keinginan para anggota PKI dan para eks-tapol saja, berarti bahwa orang itu menghina pendapat banyak orang, termasuk generasi muda Indonesia, yang sejak lama dan sudah bertahun-tahun mengharamkan dosa-dosa Suharto beserta para pendukung setianya. Pengadilan terhadap Suharto adalah sesuai dengan aspirasi semua golongan atau kalangan dalam masyarakat, yang mendambakan bahwa di Republik Indonesia ini semua orang dihargai setara sebagai manusia. Pengadilan terhadap Suharto adalah tuntutan fikiran yang waras, dan juga seruan hati nurani yang bersih, adil dan jujur.

Sejarah bangsa Indonesia harus dibersihkan

Dengan adanya putusan Pengadilan Negeri Jakarta Selatan, yang bisa dilihat sebagai tantangan terhadap para pendukung Suharto, banyak kalangan mengharapkan bahwa sidang perkara pengadilan Suharto akhirnya bisa dibuka kembali,. Sebab, diadilinya Suharto –entah dalam bentuk dan cara yang bagaimanapun juga, umpamanya : secara “in absentia” – adalah penting bagi sejarah bangsa Indonesia.

Halaman sejarah Republik Indonesia sudah dinodai oleh berbagai tindakan Suharto dan sejarah bangsa Indonesia juga sudah dikotori oleh banyak kejahatannya di bidang politik, ekonomi, sosial, moral dan hak-hak manusia. Anak cucu kita perlu sekali dengan jelas mengetahui bahwa Suharto, (yang ulangtahunnya masih “dihormati” oleh sebagian pimpinan Golkar dan TNI-AD) adalah sebetulnya penjahat besar di bidang politik dan ekonomi dan juga perusak moral bangsa. Seperti sudah disaksikan oleh banyak orang, kehidupan keluarganya (antara lain : anak-anaknya, dan Probo) dan oran-orang dekatnya (antara lain : Ibnu Sutowo, Bob Hasan) adalah bukan contoh moral yang gemilang.

Dari segi ini, kelihatan betapa pentingnya pengadilan terhadap Suharto. Dengan diadilinya Suharto, hari depan bangsa Indonesia tidak lagi dikotori terus oleh sosok koruptor terbesar di dunia, yang juga pengkhianat terbesar terhadap Bung Karno. Generasi yang akan datang perlu tahu juga bahwa Suharto adalah sekutu atau kaki-tangan kekuatan imperialis (terutama AS) untuk menghancurkan kekuatan anti-nekolim di Indonesia. Pengadilan terhadap Suharto adalah salah satu usaha untuk membersihkan sejarah bangsa, dan membersihkan hari depan generasi kita yang akan datang.
