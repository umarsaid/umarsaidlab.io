---
layout: post
title: 'Pernyataan HMI: Soeharto Tak Layak Jadi Pahlawan'
date: 2010-10-19
---

Pernyataan Ketua Umum PB HMI mengenai penolakan pemberian gelar pahlawan nasional kepada Suharto menunjukkan bahwa gelombang penolakan terhadap usul atau rencana yang berasal dari fikiran sinting atau jiwa yang tidak waras ini makin meluas di dalam masyarakat.

Perkembangan ini makin kelihatan lebih penting lagi dengan adanya pernyataan Ketua Umum HMI yang sangat tajam dan keras tentang kesalahan dan kebusukan Suharto  seperti yang disajikan di bawah ini.

Penyataan yang begitu tegas, jelas, dan keras terhadap berbagai ragam kejahatan dan kesalahan yang dilakukan Suharto yang dimanifestasikan oleh Ketua Umum ini bisa mempunyai dampak yang penting bagi berbagai kalangan Islam di Indonesia.

Pendapat HMI (yang diwakili oleh Ahmad Chozin Amirullah) tentang pelanggaran berat HAM dan korupsi besar-besaran oleh Suharto ini akan membuka mata banyak kalangan Islam yang masih tetap terus dibutakan oleh sisa-sisa pendukung Orde Baru, sehingga masih saja menganggap Suharto adalah « orang baik » dan, karenanya,  pantas diberi gelar pahlawan.

Sikap HMI tentang kejahatan Suharto dan dosa-dosa Orde Baru adalah sejalan atau searah dengan sikap sebagian kalangan muda di NU dan Muhammadiyah, yang berlainan (bahkan juga berlawanan) dengan sikap keliru kalangan fundamentalis yang masih  tetap menyokong politik Suharto.

Sikap yang anti Suharto beserta Orde Barunya adalah sikap yang bisa menaikkan martabat atau citra golongan Islam pada umumnya. Sebaliknya, kalangan Islam yang masih menganggap Suharto pantas dijadikan pahlawan hanya akan mengotori citra Islam, dan berada di jalan yang sesat.

Paris, 19 Oktober 2010

Umar Said


= = = =



Penyebab HMI Pecah, Soeharto Tak Layak Jadi Pahlawan

Rakyat Merdeka, 18 Oktober 2010


RMOL. Gelombang penolakan pemberian gelar pahlawan kepada mantan Presiden Soeharto terus bergulir. Kini giliran Pengurus Besar Himpunan Mahasiswa Islam yang menilai mantan penguasa orde baru itu tidak layak untuk mendapatkan gelar kehormatan tersebut.

Ketua Umum PB HMI Ahmad Chozin menjelaskan, HMI menolak pemberian gelar pahlawan kepada Soeharto karena telah menjerumuskan bangsa Indonesia kepada keterpurukan yang tiada tara. Mulai dari penindasan terhadap hak-hak kemanusiaan, uutang luar negeri yang tak terbayarkan, korupsi akut yang tak tersembuhkan, serta kebodohan multidimensional sehingga bangsa ini bermental inferior.

Dia membeberkan rezim Soeharto telah melakukan pengkhianatan terhadap nilai-nilai demokrasi dengan melalui kejahatan HAM berat atau kejahatan terhadap kemanusiaan, yang semestinya tidak bisa dihapuskan di luar pengadilan, ataupun karena kadaluwarsa. Sebut saja, pembunuhan massal 1965, penembakan misterius, kerusuhan 13-14 Mei, Daerah Operasi Militer (DOM) Aceh, Talang Sari, Tanjung Priok, Kasus 27 Juli, Trisakti 12 Mei 1998, Papua, dan lainnya.

Tak sampai di situ, berdasarkan laporan Kantor PBB urusan Obat-obatan dan Kriminal (UN Office on Drugs and Crime/ UNODC) bersama Bank Dunia, Soeharto juga masuk dalam list pertama daftar koruptor terbesar dunia. Dalam daftar itu disebutkan, Soeharto (Indonesia) korupsi sebesar US$15-35 miliar, Ferdinand E. Marcos (Filipina)  US$5-10 miliar, dan Mobutu Sese Seko (Kongo) US$5 miliar.

"Kami justru mempertanyakan, kenapa "mereka" begitu ngotot untuk mengusulkan
pemberian gelar Suharto sesegera mungkin? Ini patut  dicurigai, ada maksud
ada dibalik itu?" katanya mempertanyakan melalui keterangan pers yang diterima Rakyat Merdeka Online (Senin, 18/10).

Khusus bagi HMI, Chozin menambahkan, kebijakan Soeharto mengakibatkan HMI terpecah menjadi dua. Yaitu, HMI Dipo dan HMI-MPO. Otoritarianisme Soeharto yang memaksakan penerapan Asas Tunggal Pancasila pada tahun 1985 telah menjadikan barisan jama'ah HMI tercerai-berai dan akhirnya terpecah menjadi dua, yang sampai saat ini susah untuk dipersatukan.

"Kepada SBY yang punya wewenang akhir memutuskan pemberian gelar tersebut,
dengan ini saya menghimbau agar menolak usulan pemberian gelar tersebut.
Jika SBY meluluskan usulan tersebut dan akhirnya memberikan gelar pahlawan
kepada Suharto, maka sudah cukup bukti bagi kami bahwa rezim SBY tak lebih
dari sekedar reinkarnasi rezim Orde," tegasnya. [zul]



* * *

Pantaskah Soeharto Menjadi Pahlawan Nasional?

Rakyat Merdeka, 18 Oktober 2010 ,  


RMOL. Direktorat Kepahlawanan, Keperintisan dan Kesetiakawanan di Kementerian Sosial telah menetapkan sepuluh calon penerima gelar Pahlawan Nasional. Kesepuluh nama itu akan diserahkan ke Dewan Gelar, Tanda Kehormatan dan Tanda Jasa sebelum akhirnya diserahkan ke Presiden.

Menurut Sekretaris Kabinet Dipo Alam, belum tentu Dewan Gelar, Tanda Kehormatan dan Tanda Jasa yang dipimpin Menko Polkam akan meloloskan kesepuluh nama itu, dan memberikannya kepada Presiden SBY. Serta, belum tentu juga Presiden SBY akan menyetujui semua nama yang diserahkan Dewan.

Dus artinya, kata akhir siapa yang pantas mendapat gelar Pahlawan Nasional ada di tangan Presiden.

Kesepuluh calon penerima gelar Pahlawan Nasional tersebut adalah:

1. Mantan Gubernur DKI Jakarta Ali Sadikin dari Jawa Barat.
2. Habib Sayid Al Jufrie dari Sulawesi Tengah
3. Mantan Presiden Soeharto dari Jawa Tengah
4. Mantan Presiden Abdurrahman Wahid dari Jawa Timur
5. Hj Andi Depu Maraddia Balanipa dari Sulawesi Barat
6. Mantan Waperdam II Johanes Leimena dari Maluku
7. Yohannes Abraham Dimara dari Papua
8. Andi Makassau dari Sulawesi Selatan
9. Pakubuwono X dari Jawa Tengah
10. KH Ahmad Sanusi dari Jawa Barat.

Dari kesepuluh calon penerima gelar Pahlawan Nasional tersebut rasa-rasanya hanya mantan Presiden Soeharto yang ditentang banyak orang. Pemberian gelar Pahlawan Nasional untuk Soeharto menjadi kontroversial karena ia dinilai bertanggung jawab pada sejumlah kejahatan HAM dan kejahatan ekonomi selama berkuasa. Kalau tidak, mana mungkin ia diterjang gelombang demonstrasi dan akhirnya harus mengundurkan diri.

Soeharto dianggap sebagai pihak yang paling bertanggung jawab di balik peristiwa pembantaian massal yang terjadi terhadap simpatisan Partai Komunis Indonesia (PKI) dan orang-orang yang dituduh memiliki hubungan dengan partai itu antara 1965 hingga 1966. Tidak ada angka resmi mengenai berapa jumlah korban tewas dalam pembantaian massal tersebut. Angka yang biasa disebutkan ada di kisaran 500 ribu sampai 1,5 juta jiwa.

Tetapi, dalam artikel yang ditulis Ben Anderson sebulan setelah Soeharto meninggal dunia, Exit Soeharto: Obituary for a Mediocre Tyrant (2008), disebutkan bahwa mantan komandan RPKAD Sarwo Edhie sebelum meninggal dunia mengatakan bahwa yang tewas adalah tiga juta orang. RPKAD adalah sayap militer yang digunakan Soeharto untuk menggulung PKI dan orang-orang yang dituduh memiliki hubungan dengan partai itu.

Sementara di bidang ekonomi, Soeharto dipersalahkan karena menggadaikan sumber daya alam Indonesia kepada perusahaan-perusahaan multinasional. Freeport di Papua adalah contoh paling klasik. Perusahaan ini menjadi perusahaan multinasional pertama yang beroperasi di Indonesia setelah Soeharto mengambil alih kekuasaan dari tangan Bung Karno. Hak yang diberikan kepada perusahaan ini sedemikian besar, sehingga ia dapat mengubah bukit menjadi danau, menciptakan kerusakan lingkungan, dan di saat bersamaan meninggalkan jejak kemiskinan yang merata di Papua.

Ketergantungan ekonomi Soeharto pada dunia luar tidak hanya terbatas pada pemberian konsesi pertambangan kepada MNCs saja. Soeharto juga gemar menangguk utang luar negeri. Hebatnya lagi, selama masa Orde Baru, utang luar negeri dimasukkan ke dalam kolom penerimaan APBN; utang luar negeri diperlakukan sebagai pendapatan negara. Soeharto memang pantas disebut sebagai Bapak Pembangunan. Tetapi harus diingat, pembangunan a la Soeharto adalah pembangunan yang dibiayai utang luar negeri. Itu sebabnya di saat bersamaan juga terjadi ketimpangan yang ironisnya, setelah Soeharto tidak berkuasa, malah semakin besar.

Soeharto juga dianggap bermasalah di bidang politik. Stabilitas politik adalah unsur pertama dalam trilogi pembangunan a la Soeharto. Stabilitas mensyaratkan ketiadaan kelompok oposisi dan kelompok-kelompok yang memiliki perbedaan pandangan dengan Soeharto dan kroninya. Jumlah partai politik dipangkas paksa; pemilihan umum digelar secara berkala lima tahun sekali di bawah tekanan militer; Pancasila dan UUD 1945 dijadikan alat tekan politik sehingga sampai kini dianggap identik dengan gagasan otoriter yang usang; Dewan Perwakilan Rakyat dan Majelis Permusyawaratan Rakyat menjadi badan-badan perkoncoan yang tugasnya hanya menyetempel dan menyetujui apapun yang diinginkan Cendana.

Ada juga yang mengingatkan, bahwa Soeharto merupakan objek koreksi reformasi. Bukankah TAP MPR XI/1998 mengamanatkan agar kasus KKN yang melibatkan Soeharto dan kroninya diusut tuntas? Sampai kini upaya itu tidak pernah terealisasi. Soeharto tidak pernah didudukkan di kursi terdakwa. Ia menghembuskan nafas terakhir, akhir Januari 2008 tanpa sedikitpun tersentuh tangan hukum.

Ben Anderson menyebut Soeharto sebagai "mediocre tyrant" atau tiran yang biasa-biasa saja. Kemunculannya diawali krisis politik dalam negeri. Baik dirinya maupun krisis politik dalam negeri itu dimanfaatkan pihak asing, dalam hal ini Amerika Serikat. Karier kekuasannya pun ditutup oleh babak turbulensi politik.

Tetapi ada satu hal yang sepertinya Ben Anderson lupa. Dalam sejarah kontemporer, Soeharto adalah satu-satunya tirani yang survive setelah kekuasaannya digusur gelombang kemarahan rakyat dan kelompok elit yang memanfaatkan kemarahan rakyat itu. Hanya Soeharto yang setelah dilengserkan tidak harus melarikan diri dan meminta perlindungan dari pemerintahan negara manapun, dan tidak disentuh hukum sama sekali. Pengalaman di banyak negara memperlihatkan sebaliknya, tiran yang jatuh harus mengalami salah satu dari kedua hal itu, lari atau dihakimi.

Sebegitu keras kontroversi yang mengiringi pencalonan Soeharto sebagai Pahlawan Nasional. Menurut pihak Istana, kontroversi itu adalah hal yang wajar. Sekretaris Kabinet Dipo Alam yang dihubungi kemarin mengatakan, sebagai manusia biasa tentulah Soeharto memiliki kelemahan dan kelebihan. Dia membandingkan Soeharto dengan Bung Karno dan Abdurrahman Wahid yang juga jatuh dihumbalang protes rakyat.

Rachmawati Soekarnoputri, putri Bung Karno, berpendapat, agar tidak terbebani oleh sejarah, status hukum Soeharto harus diperjelas terlebih dahulu.

"Jangan diulang yang terjadi pada Bung Karno. Kita harus meng-clear-kan dulu status Pak Harto. Intinya direhabilitasi atau bagaimana," ujar mantan anggota Dewan Pertimbangan Presiden (Wantimpres) ini saat dihubungi Rakyat Merdeka Online, kemarin (Minggu, 17/10).

Dia curiga rencana pemberian gelar Pahlawan Nasional terhadap Soeharto hanya sekadar bagi-bagi status semata.

Sejarawan Asvi Marwan Adam menawarkan jalan keluar yang elegan. Masyarakat, menurutnya, juga harus dilibatkan dalam pemberian gelar Pahlawan Nasional, terlebih untuk tokoh kontroversial yang kerap dikaitkan dengan kasus-kasus pelanggaran HAM.

Untuk Soeharto, sebutnya pekan lalu, bila masyarakat tidak memberikan penolakan, maka Presiden SBY akan menyematkan gelar itu kepada Soeharto. Dan kalau sudah disematkan, tidak bisa lagi ditolak

"Akan sangat tragis bila di antara para pahlawan kita nantinya terdapat pelaku pelanggaran HAM," kata Asvi.

Demikianlah sidang pembaca. Mengikuti saran Asvi Marwan Adam, Rakyat Merdeka Online membuka kesempatan seluas-luasnya kepada pembaca untuk memberikan pendapat, apakah setuju atau tidak setuju dengan gelar Pahlawan Nasional untuk mantan Presiden RI Soeharto.

Klik pilihan Anda sekarang juga. Lebih cepat, lebih baik. [guh]

* * *



Komnas HAM: Sebaiknya Pemerintah Tak Berikan

Gelar Pahlawan untuk Soeharto

Rakyat Merdeka, 18 Oktober 2010

RMOL. Pemerintahan Presiden Susilo Bambang Yudhoyono diharapkan bisa mempertimbangkan pemberian gelar pahlawan kepada mantan Presiden Soeharto.

Wakil Ketua Komisi Nasional Hak Asasi Manusia, Ridha Saleh mengatakan, pemerintah sebaiknya tidak hanya melihat prestasi yang dilakukan Soeharto saja tapi juga harus melihat kejahatan-kejahatan masa lalu yang dilakukan rezim Suharto.

''Hal itu menjadi sangat penting sebab gelar pahlawan menjadi kontroversial pada Soeharto,'' kata Ridha Saleh ketika dihubungi melalui sambungan telepon di Jakarta (Senin, 18/10).

Ia menambahkan Komnas HAM juga akan memberikan fakta-fakta mengenai kejahatan HAM di masa lalu untuk dipertimbangkan oleh Dewan Gelar apakah mantan Presiden Soeharto layak atau tidak diberi gelar pahlawan.

Sementara itu politisi Partai Golkar Poempida Hidayatullah mengatakan pemberian gelar pahlawan kepada Soeharto oke-oke saja.

Menurutnya masyarakat juga harus melihat Soeharto sebagai sosok yang berkontribusi bagi negara Indonesia.

Ia melanjutkan pemberian gelar itu bisa dalam kategori lain seperti pahlawan ekonomi atau pahlawan pembangunan.

''Kita lihat jasa-jasanya yang banyak karena tidak ada gading yang tak retak,'' tegasnya. [arp]
