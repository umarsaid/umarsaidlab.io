---
layout: post
title: Kumandangkan lagilah lagu Internasionale pada Hari 1 Mei
date: 2010-04-24
---

Tidak lama lagi, kaum buruh sedunia akan merayakan hari besar kemenangannya yang jatuh pada tanggal 1 Mei. Seperti biasanya, sejak berpuluh-puluh tahun, hari buruh 1 Mei ini diperingati secara besar-besaran di berbagai benua (Eropa, Asia, Afrika, Australia dan Amerika Latin) dengan macam-macam cara. Ada yang berbentuk rapat-rapat umum besar-besaran, ada yang dilakukan dengan mengadakan demonstrasi atau pawai yang megah, ada juga yang dengan pesta-pesta kegembiraan dalam berbagai bentuk, yang semuanya diadakan kaum pekerja di macam-macam bidang lapangan dan oleh berbagai golongan di dunia.



Di banyak negeri di dunia, Hari Buruh 1 Mei merupakan peristiwa untuk memperingati kemenangan perjuangan kaum buruh  dalam membela kepentingan atau hak-haknya melawan penindasan,  kesrakahan, penghisapan, penipuan, dan pemerasan kaum kapitalis reaksioner nasional maupun internasional, yang merupakan tulang punggung neoliberalisme atau imperialisme.



Di negeri kita Indonesia sendiri, sekarang ini berbagai kalangan di macam-macam golongan buruh atau pekerja, juga sedang mempersiapkan perayaan Hari Buruh 1 Mei ini.  Sejak jatuhnya rejim militer Suharto dalam tahun 1998, Hari Buruh 1 Mei bisa dirayakan dalam skala  yang berbeda-beda dari tahun ke tahun.



Sedangkan, seperti masih sama-sama kita ingat, selama rejim Orde Baru yang 32 tahun itu Hari Buruh 1 Mei dilarang dirayakan dalam bentuk apapun juga. Karena,  para penguasa rejim militer Suharto, menuduh (dengan sembarangan atau keliru dan bodoh)  bahwa Hari Buruh 1 Mei diidentikkan dengan gerakan atau kegiatan yang menguntungkan PKI saja atau hanya menyuburkan komunisme/PKI.



Ini berlainan sekali dengan sikap politik Bung Karno, yang sebagai  nasionalis, Muslimin dan juga Marxis, sejak ia muda belia menghargai perjuangan kaum buruh Indonesia dan buruh sedunia, dan memandang Hari Buruh Sedunia 1 Mei sebagai peristiwa penting bagi rakyat Indonesia dan rakyat berbagai negeri yang menentang kapitalisme dan imperialisme. Karena itu, tanggal 1 Mei diresmikan sebagai hari libur nasional.





Bung Karno : kaum buruh adalah soko guru revolusi



Dari sejarah perjuangan rakyat Indonesia sebelum kemerdekaan RI, dan juga sesudah revolusi 45, nyatalah bagi kita semua bahwa tidak adalah pemimpin Indonesia yang mempunyai sikap pro kepentingan buruh yang begitu jelas seperti yang diemban Bung Karno sampai wafatnya. Bung Karnolah yang menganggap dan juga menjadikan kaum buruh sebagai soko guru revolusi, mendorong kaum buruh sebagai bagian utama front Nasional, dan wakil-wakilnya duduk dalam DPR-GR, MPR, DPA  dan badan-badan atau lembaga-lembaga penting lainnya.



Boleh dikatakan bahwa hati Bung Karno adalah betul-betul satu dengan hati kaum buruh Indonesia dalam perjuangan bersama menentang kapitalisme dan kolonialisme atau imperialisme. Karena itu, SOBSI (gabungan serikat-serikat buruh seluruh Indonesia) pernah menjadi kekuatan yang perkasa dan berwibawa, baik di tingkat nasional maupun internasional.



Sejak  revolusi 1945 nyanyian kaum buruh sedunia « Internasionale » telah bertahun-tahun berkumandang di tiap pertemuan-pertemuan penting kaum buruh atau kaum kiri umumnya. Namun, dalam jangka lama sekali, lagu ini tidak terdengar lagi, karena dilarang disuarakan oleh pemerintahan militer Suharto.  Kerterlaluan !!! Sedangkan lagu ini disuarakan di berbagai negeri di dunia, termasuk di Amerika Serikat dan juga di berbagai negara Arab.



Padahal, ketika zaman kolonial Belanda pun, lagu « Internasionale » (yang sudah diterjemahkan dalam bahasa Indonesia) masih bisa terdengar, meskipun secara sembunyi-sembunyi di berbagai daerah (terutama daerah Blitar, Kediri, dan Malang).



Sekarang tidak ada tokoh nasional yang pro gerakan buruh



Sekarang, dalam masa transisi sesudah jatuhnya Orde Baru, gerakan buruh Indonesia setapak-setapak mulai mendapat kebebasan untuk melakukan  berbagai kegiatan, dalam batas-batas tertentu yang dimungkinkan oleh berbagai sistem pemerintahan yang pada pokoknya masih didominasi oleh kepentingan neo-liberalisme.



Nyatalah,  bahwa gerakan buruh Indonesia, yang terdiri dari macam-macam golongan dan aliran politik (nasionalis, agama,  komunis atau sosialis) masih perlu terus berusaha menyatukan diri dalam perjuangan bersama  untuk menentang neo-liberalisme dalam bentuknya yang sekarang, baik di tingkat nasional maupun internasional.



Karena, sejak lama sampai sekarang, dari berbagai « pemimpin nasional » atau tokoh-tokoh penting macam-macam partai politik di pemerintahan atau DPR tidak bisa diharapkan ada yang betul-betul  sebagai pembela kepentingan kaum buruh/pekerja, maka tidak bisa lain, kaum buruh sendirilah – dengan dukungan berbagai gerakan demokratis lainnya – yang harus berjuang dengan segala cara dan jalan, untuk menggalang kekuatan, sehingga setidak-tidaknya mencapai kekuatan gerakan buruh di bawah pemerintahan  Bung Karno.



Seperti yang bisa kita amati bersama-sama, terutama oleh kalangan peneliti sejarah sosial, gerakan buruh Indonesa memang berkembang besar-besaran di bawah pemerintahan Bung Karno. Simpati Bung Karno kepada gerakan buruh, sebagai bagian dari kekuatan revolusioner di Indonesia, sudah sering sekali kelihatan selama ia bertindak sebagai Pemimpin Besar Revolusi.



Bung Karno menyanyikan « Internasionale » di Istana Negara



Dalam kaitan ini pulalah kita bisa melihat mengapa Bung Karno menyanyikan lagu « Internasionale » di Istana Negara pada tanggal 13  September 1966, dalam amanatnya di depan pertemuan dengan para anggota Angkatan 45. (Menurut cuplikan amanat Bung Karno tersebut, yang dimuat dalam buku « Revolusi Belum Selesai » jilid II halaman 313)



Harap diperhatikan bahwa Bung Karno menyanyikan lagu « Internasionale »  di Istana Negara ini ketika setahun sesudah terjadinya G30S, dan ketika jutaan para anggota serta simpatisan PKI, dan ratusan ribu pimpinan dan aktivis serikat buruh dari berbagai tingkat di seluruh Indonesia, dibunuhi, atau dipersekusi dan dipenjarakan.  Bung Karno menyanyikan lagu « Internasionale » di Istana Negara ketika PKI sudah dinyatakan sebagai partai terlarang berikut penyebaran ajaran-ajaran Marxismenya. Itulah Bung Karno !!!



Sekarang, lagu « Internasionale » mulai terdengar lagi di Indonesia, meskipun masih sayup-sayup dan masih sporadis di sana-sini. Kiranya, sudah waktunyalah,  mulai sekarang, lagu « Internasionale » dikumandangkan lagi dengan gegap gempita oleh seluruh  gerakan gerakan buruh Indonesia seperti sebelum 1965, tidak peduli dari golongan yang manapun juga dan aliran politik yang apapun juga.



Lagu « Internasionale » yang dikumandangkan di Indonesia akan merupakan bagian dari lagu yang bergema di banyak tempat di dunia (antara lain : Amerika Serikat, Amerika Latin, Asia, Australia, Afrika, dan Eropa), yang memanifestasikan juga rasa setiakawan besar-besaran dalam skala dunia untuk menentang exploitation de l’homme par l’homme dan exploitation des nations par les autres nations (menurut ajaran revolusioner Bung Karno)



Lagu « Internasionale » yang disuarakan Bung Karno di Istana Negara pada tanggal 13 September 1966 patut sekali (dan, bahkan, perlu sekali !!!) diperingati, dijiwai, atau dipanuti sebagai bagian dari ajaran-ajaran revolusionernya, untuk meneruskan revolusi yang belum selesai dewasa ini. Dan juga untuk menyatukan diri dengan gerakan revolusioner di dunia ( seperti di Venezuela, Bolivia,  atau di benua-benua lainnya) yang menentang neo-liberalisme dalam segala bentuknya.
