---
layout: post
title: Uskup kiri jadi presiden baru Paraguay
date: 2008-04-22
---

Apa yang terjadi di negara Amerika Latin, Paraguay, pada hari Minggu tanggal 22 April 2008 adalah peristiwa menarik, yang patut menjadi perhatian banyak orang di Indonesia. Sebagai hasil pemungutan suara dalam Pemilu di negara itu seorang mantan uskup Katolik, Fernando LUGO, terpilih sebagai presiden yang baru, dengan meraih lebih dari 43% suara. Yang menarik sekali untuk diperhatikan adalah bahwa Fernando LUGO dikenal di Paraguay sebagai uskup Katolik yang berhaluan kiri, dan mendapat julukan “Uskupnya orang miskin” (bishop of the poor). Mengingat bahwa perkembangan di Paraguay patut sekali mejadi renungan bersama kita, maka berikut ini disajikan berbagai informasi, yang dikumpulkan dari berbagai sumber :

Kemenangan Fernando LUGO diraihnya melalui Aliansi Patriotik untuk Perubahan (Patriotic Alliance for Change), suatu front luas yang berupa gabungan dari kekuatan-kekuatan patriotik, yang bersama-sama memperjuangkan terjadinya perubahan di Paraguay. Kemenangan Fernando LUGO ini menunjukkan juga bahwa politik “memihak rakyat miskin” , yang jadi pedoman Aliansinya, mendapat simpati dan dukungan besar dari rakyat. Hal yang demikian bisalah dimengerti, karena Paraguay (yang berpenduduk 6 juta orang) adalah negeri termiskin di Amerika Latin, lebih miskin dari pada Bolivianya Evo Morales.

Aliansi dari 20 partai dan gerakan
Dipilihnya mantan uskup Katolik yang berhaluan kiri (ia dikenal sebagai aktivis gerakan Teologi Pembebasan, yang memperjuangkan keadilan dan melawan kemiskinan) adalah perkembangan yang penting untuk Paraguay, yang selama 61 tahun diperintah oleh golongan reaksioner. Golongan reaksioner ini tergabung dalam satu-satunya partai, yaitu partai Colorado, yang juga pernah menyokong diktator Jenderal Alfredo Stroessner. Calon yang dijagokan partai Colorado (wanita bernama Ovelar) telah dikalahkan Fernando LUGO, karena hanya meraih suara sekitar 30 %. Calon lainnya dari kalangan militer hanya mendapat 22 %.

Delapan bulan yang lalu, Fernando LUGO (usia 56 tahun) telah membangun front luas yang terdiri dari serikat-serikat buruh, orang-orang suku Indian dan petani-petani miskin, dan partai-partai oposisi, untuk membentuk Aliansi Patriotik untuk Perubahan. Aliansi ini adalah gabungan dari 20 partai dan gerakan, yang bertekad satu untuk mengalahkan partai-tunggal Clolorado. Dalam kampanye-nya yang karismatik, ia melampiaskan kemarahan rakyat terhadap korupsi dan penyelenggaraan negara yang brengsek, yang menjadikan para petani sangat menderita berkepanjangan selama puluhan tahun.

“Sekarang datang saatnya untuk perubahan! Jangan takut!” ("Now is the hour of change! Don't be afraid!") serunya berkali-kali dengan kutipan dari Kitab Suci (Bible) dan mengenakan sandal. Seruannya ini sangat berarti bagi rakyat Paraguay, yang selama 35 tahun berada di bawah diktatur militer, yang disokong oleh Amerika Serikat. Diktatur militer ini berakhir dalam tahun 1989, dan mulailah pemerintahan sipil, tetapi sangat reaksioner juga dan korup.

Paraguay membikin Amerika Latin makin ke kiri
Dengan kemenangan Fernando LUGO maka berakhirlah sudah pemerintahan partai-tunggal Colorado, yang telah memerintah terus-menerus dengan cara-cara korup dan reaksioner sejak tahun 1947. Banyak orang memperkirakan bahwa Fernano LUGO akan membentuk pemerintahan yang “tengah kiri” (center-left). Tetapi ia sendiri menyatakan bahwa ia bukanlah orang “kiri” dan juga bukan “kanan”.

Hari Minggu tanggal 22 April 2008 itu merupakan hari yang bersejarah bagi rakyat Paraguay. Mulai hari itu mantan uskup Fernando LUGO membawa Paraguay memasuki era yang sama sekali baru dibandingkan dengan masa selama 61 tahun di bawah pemerintahan partai politik reaksioner. Partai Colorado adalah partai-tunggal, yang dengan cara-cara yang kejam telah menindas rakyatnya sendiri. Paraguay di bawah Fernando LUGO akan menjadi bagian dari kelompok negara-negara Amerika Latin yang menjalankan politik “kiri” dan “tengah kiri” dalam derajad atau kadar berbeda-beda. Paraguay di bawah Fernando LUGO memang tidak akan sekiri Venezuela di bawah Hugo Chavez dan Bolivia di bawah Evo Morales. Tetapi, bisalah kiranya dikatakan bahwa Paraguay, dengan Fernando LUGO sebagai presiden barunya akan mendorong benua Amerika Latin makin condong ke kiri. Ini merupakan tanda bahwa abad ke-21 adalah abad perubahan besar di Amerika Latin, yang akan bisa menimbulkan dampak penting dalam perkembangan dunia.

Yang penting untuk kita perhatikan di Indonesia, adalah bahwa kemenangan Fernando LUGO adalah bukti yang jelas bahwa politik neo-kolonialisme dan neo-liberalisme yang dijajakan AS di benua Amerika Latin telah dan sedang mengalami kemunduran atau kegagalan yang bertubi-tubi dan berturut-turut. Sekarang makin jelas bahwa AS, seperti halnya di Indonesia juga semasa Orde Baru, adalah pendukung atau penolong pemerintahan yang reaksioner dan anti-rakyat.

Kemiskinan meluas dan korupsi merajalela
Di bawah pemerintahan partai-tunggal Colorado (sebangsanya partai Golkar),, dan terutama ketika di bawah diktatur militer Jenderal Alfredo Stroessner, Paraguay menjadi pusat perdagangan gelap cocaine, kopi dan mobil mewah, yang umumnya hanya menguntungkan para politisi, kaum kaya, dan pembesar-pembesar mililiter. Sebanyak 77% tanah pertanian dikuasai kaum tuantanah yang hanya 1% dari jumlah penduduk. Kemiskinan di pedesaan sangat meluas, dan karenanya banyak orang Paraguay mencari hdup di daerah-daerah lain atau negeri lain sebagai emigran gelap. “Di Paraguay hanya ada maling dan korban maling” (“In Paraguay, there are only thieves and the victims of thieves,”), sering dikatakan oleh Fernando LUGO. Sebagai program yang akan dijalankan di bawah pimpinannya, Fernando LUGO akan memasukkan orang-orang dari suku Indian dalam pemerintahan

Karena letaknya yang jauh dari Indonesia, dan selama ini juga tidak banyak diberitakan dalam pers dunia, maka wajarlah bahwa banyak orang memerlukan informasi-informasi dasar untuk mengenalnya, walaupun secara sepintas lalu.

Paraguay adalah negara yang luasnya 406.752 km persegi, dengan penduduk seluruhnya 6 juta lebih. Ibu kotanya bernama Asuncion. Bahasa utamanya adalah bahasa Spanyol dan bahasa Indian Guarani. Agama utamanya Katolik. Ekspor utamanya : kedelai, kapas, daging sapi, minyak makan, kayu. Pendapatan per capita : US$ 1.280. Paraguay terletak di tengah-tengah Amerika Latin, dan diapit oleh negara-negara Argentina, Bolivia dan Brasilia.

Tugas Fernando LUGO besar dan berat
Mengingat bahwa partai politik yang dikalahkan oleh Fernando LUGO adalah partai tunggal yang sudah selama 61 tahun memegang terus-menerus pemerintahan di Paraguay maka bisa dimengerti bahwa tugas yang dihadapi mantan uskup ini besar dan juga berat sekali. Boleh dikatakan bahwa seluruh birokrasi atau aparat negara ada di tangan para pendukung rejim yang lama. Mereka merupakan kekuatan kontra-revolusi yang setiap waktu bisa merongrong pemerintahan Fernando LUGO dengan menimbulkan berbagai persoalan atau kesulitan.

Di samping itu penduduk yang sebagian termasuk golongan Katolik kolot masih bisa dipengaruhi oleh kalangan-kalangan reaksioner, yang tidak menyukai sikap mantan uskup Fernando LUGO yang kiri. Tetapi, kekuatan presiden baru ini justru terletak pada kesetiaannya membela kaum miskin dan keteguhannya untuk menjaga kejujuran dan keadilan.

“We will build a Paraguay that will not be known for its corruption and poverty, but for its honesty .” (Kita akan membangun Paraguay yang tidak akan terkenal karena korupsinya dan kemiskinannya, melainkan karena kejujurannya”) katanya berulang kali. Dan pernyataan yang demikian itu, yang diucapkan oleh mantan uskup Katolik kiri, mempunyai arti yang besar sekali.

Paraguay sebagai topik yang menarik
Tulisan ini mengangkat peristiwa di Paraguay sebagai topik yang menarik, karena berbagai pertimbangan, untuk sama-sama direnungkan, yang antara lain sebagai berikut :

Kemenangan Fernando LUGO sebagai mantan uskup Katolik kiri menjadi presiden kepala negara Paraguay menunjukkan bahwa pandangan atau politik kiri yang mengutamakan rakyat miskin bisa mengalahkan kekuatan atau kekuasaan politik reaksioner walaupun sudah bercokol terus-menerus selama 61 tahun.

Kemenangan Fernando LUGO, mencerminkan bahwa pandangan atau programnya yang pro-rakyat dan anti-neoliberalisme yang dijajakan oleh AS (beserta pendukung-pendukungnya) adalah sesuai dengan aspirasi rakyat Paraguay, yang mendambakan perubahan mendasar dari keterbelakangan politik, sosial dan ekonomi yang membikin sengsaranya sebagian terbesar rakyat.

Berhasilnya Fernando LUGO sebagai presiden Paraguay yang baru adalah berkat dibentuknya front luas yang bernama Aliansi Patriotik untuk Perubahan, yang dengan jelas dan tegas bertujan mengadakan perubahan radikal terhadap situasi yang sudah bercokol selama 61 tahun. Nama aliansi yang menyebutkan “untuk perubahan” adalah daya tarik yang kuat sekali.

Walaupun Fernando LUGO mengatakan bahwa ia bukan kiri dan juga bukan kanan, tetapi karena ia adalah penganut aktif dan setia Teologi Pembebasan, dalam kenyataannya ia bisa digolongkan sebagai penganut gagasan-gagasan kiri.

Runtuhnya kekuasaan politik partai-tunggal Colorado (yang merupakan partai politik yang tertua di benua Amerika Latin), mencerminkan bahwa dunia berubah terus, walaupun kadang-kadang terasa lambat sekali.

Berhasilnya Aliansi Patriotik untuk Perubahan menjadikan mantan uskup Fernando LUGO sebagai presiden adalah bukti bahwa sosoknya yang “kiri” bukanlah “momok” yang perlu ditakuti atau dijadikan musuh rakyat.

Kiranya, Itu semua bisa dijadikan bahan renungan bersama bagi kita semua yang di Indonesia.
