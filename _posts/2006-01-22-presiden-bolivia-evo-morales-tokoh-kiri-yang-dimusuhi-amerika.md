---
layout: post
title: Presiden Bolivia Evo Morales Tokoh Kiri yang dimusuhi Amerika
date: 2006-01-22
---

Dilantiknya pemimpin gerakan tani Bolivia, Evo Morales, sebagai presiden terpilih pada tanggal 22 Januari 2006, bukan saja merupakan peristiwa menggembirakan yang bersejarah bagi rakyat Bolivia, tetapi juga bagi rakyat berbagai negeri di Amerika Latin, bahkan juga bagi banyak orang di bagian-bagian lainnya di dunia. Peristiwa ini merupakan bagian dari serentetan perkembangan yang menarik sekali, yang menggambarkan bahwa sejumlah negeri-negeri Amerika Latin sedang bergeser “ke kiri”.

Oleh karena itu, kemenangan Evo Morales dalam pemilu di Bolivia menjadi perhatian pers dunia.

(Kiranya, bagi sebagian dari pembaca patut diingat lagi, bahwa Bolivia adalah negeri yang dijadikan Che Guevara sebagai basis permulaan - sesudah Kuba- dalam usahanya untuk menyebarkan revolusi di negeri-negeri Amerika Latin. Ia datang ke Bolivia dalam bulan November 1966. Setelah bergerilya di pegunungan dan pedesaan Bolivia, ia di bunuh dalam tahun 1967 oleh tentera Bolivia yang kerjasama dengan CIA. Sekarang, barangkali Che Guevara tersenyum di makamnya karena gembira melihat kemenangan Evo Morales).

Pelantikan Evo Morales sebagai orang suku asli Indian yang pertama kali untuk menjadi presiden Bolivia merupakan peristiwa besar dan penting, yang disambut dengan gembira oleh bermacam-macam suku Indian yang tersebar di benua Amerika Latin, Amerika Tengah dan juga Amerika Utara. Sebab, selama ratusan tahun sejarah Bolivia, pembesar-pembesar pemerintahan selalu terdiri dari orang-orang kulit putih. Karena itu, ada kalangan yang menafsirkan fenomena Evo Morales sebagai kebangkitan harga diri suku Indian, yang sudah diinjak-injak dimana-mana selama ratusan tahun.

Lagi pula, Evo Morales dipilih oleh majoritas suara (sekitar 54%), dalam pemilu yang diselenggarakan untuk penduduk sebesar 11 juta orang itu. Sejak 1982, ketika Bolivia mulai mengecap kehidupan yang agak demokratis (relatif) belum pernah ada seorang presiden yang terpilih dengan persentase suara sebesar itu. Karena itu, banyak pakar memandang pemilihan Evo Morales betul-betul mempunyai legitimitas dan popularitas yang tinggi sekali, melebihi semua pemimpin-pemimpin Bolivia lainnya di masa yang lalu.

FENOMENA MORALES YANG MENARIK

Banyak segi-segi yang menarik tentang fenomena Evo Morales (umur 46 tahun) yang bisa diungkap. Ia adalah anak dari petani miskin dari suku Indian Aymara yang hidup sengsara di kota kecil pegunungan Orinuca. Ketika kecil ia ikut bapaknya yang bekerja sebagai pekerja migran di Argentina. Kemudian ia berkelana di berbagai daerah di Bolivia sebagai pemain trompet dalam kelompok musik bar. Setelah timbul gerakan sosial di kalangan petani coca, ia kemudian menjadi salah seorang di antara aktivisnya yang terkemuka.

Ketika ia melihat bahwa perjuangan sosial di kalangan petani-petani coca ini perlu ditingkatkan menjadi gerakan politik , maka partai yang bernama MAS (singkatan dari bahasa Latin Movimiento Al Socialismo, “gerakan menuju sosialisme”) yang dipimpin Evo Morales menjadi kekuatan politik yang terbesar dan terkuat di Bolivia. Melalui kampanyenya yang terang-terangan mengutuk kejahatan-kejahatan perusahaan-perusahaan multinasional, mengkritik praktek-praktek neo-liberalisme dan globalisasi yang dilakukan oleh IMF, Bank Dunia, dan WTO, Evo Morales juga banyak bicara tentang pentingnya negara Bolivia mengkontrol pengelolaan gas bumi, yang merupakan cadangan besar sekali di benua Amerika Latin.

Selama memimpin MAS kelihatan sekali bahwa Evo Morales mempunyai sikap anti-Amerika yang kuat sekali. Karena itu, ia dianggap oleh berbagai kalangan sebagai “momok” atau “duri” bagi kepentingan AS di kawasan ini.

Morales mengatakan bahwa ia tidak menyukai kapitalisme. Sejarah penjajahan Spanyol di Bolivia menunjukkan bahwa penjarahan besar-besaran kekayaan bumi Bolivia yang berupa timah hanya untuk kekayaan kapitalis-kapitalis Spanyol, sedangkan orang-orang dari suku Indian, yang merupakan majoritas penduduk, tidak mendapat apa-apa atau sedikit sekali.

ARTI KEMENANGAN EVO MORALES

.

Kantor-berita Aljazeera Net (31 Desember 2005) menyiarkan berita yang antara lain mengatakan bahwa “Evo Morales yang berhaluan sosialis telah disambut sebagai pahlawan di Havana. Pemerintah Kuba menyambut pemilihan Evo Morales sebagai kemenangan penting atas pengaruh AS di kawasan ini. Fidel Castro mengatakan :” I think that it has moved the world. It’s something extraordinary, something historic. The map is changing” (Saya berpendapat bahwa kejadian ini telah menggerakkan dunia. Ini adalah sesuatu yang luarbiasa, sesuatu yang bersejarah. Peta bumi sedang berobah”.

Mungkin, seperti yang dikatakan oleh Fidel Castro bahwa terpilihnya Evo Morales sebagai presiden Bolivia memang merupakan peristiwa yang luarbiasa , yang bersejarah, yang menggoyang dunia dan yang sedang merobah peta bumi. Untuk mencoba mengerti tentang pentingnya peristiwa ini, kiranya kita bisa melihatnya dari berbagai sudut pandang

Banyak kalangan yang menafsirkan bahwa kemenangan dan pemilihan Evo Morales sebagai presiden Bolivia merupakan suatu perkembangan politik yang penting di Amerika Latin. Sebab, kemenangan ini diusung atau digotong oleh MAS (Movimiento Al Socialismo) yang terang-terangan anti-kapitalisme (dan, dengan sendirinya, anti-AS). Jadi, pada prinsipnya, corak atau arah politik MAS adalah jelas kiri, dan berhaluan sosialis.

Sekarang, kelihatan bahwa kemenangan Evo Morales ini disambut hangat oleh Hugo Chavez dari Venuzuela dan Fidel Castro dari Kuba. Dengan derajat yang berbeda-beda, tetapi tetap dalam rangka persahabatan atau perkawanan yang erat juga telah tergalang hubungan dengan presiden Lula dari Brasilia (negeri yang besar dan penting di benua Amerika Latin), dengan presiden Nesto Kirchner dari Argentina, juga dengan Uruguay dan Equador. Dapat diduga bahwa hubungan antara Bolivia dan Cili, yang akan dipimpin oleh presiden perempuan (mantan Tapol) Michelle Bachelet, juga akan membaik. Kemenangan Evo Morales di Bolivia mungkin sekali akan mendorong terjadinya perubahan di Peru juga.

Ini berarti bahwa sejumlah negara-negara (paling sedikit 7 negara) yang penting-penting di benua Amerika Latin sedang bersama-sama (walaupun tidak serentak dan dengan cara yang berbeda-beda) bergerak ke arah yang tidak menguntungkan AS beserta sekutu-sekutunya.

Inilah barangkali gejala yang disebut oleh Fidel Castro “The map is changing”. Dapat dimengerti bahwa perubahan penting dengan skala yang begitu besar itu akan bisa menimbulkan dampak yang tidak kecil bagi negeri-negeri lainnya, baik yang di Amerika Latin atau di benua lainnya. Kalau proses perubahan ini berlangsung terus - dalam jangka dekat maupun jauh - maka perjuangan berbagai negeri di dunia untuk melawan kapitalisme neo-liberal dan globalisasi akan memasuki tahap baru. Peta pertarungan politik (dan ekonomi) di skala dunia akan menjadi makin jauh berbeda dari ketika masih berlangsung Perang Dingin.

Kemenangan besar yang diperoleh Movimiento Al Socialismo di Bolivia, dan terpilihnya tokoh sosialis Michelle Bachelet di Cili, dan sikap anti-AS Hugo Chavez di Venuzuela, berarti bahwa sekarang ini Republik sosialis Kuba (sekitar 13 juta penduduk) di bawah Fidel Castro tidak terisolasi lagi seperti di masa yang lalu. Seperti kita ketahui, imperialisme AS telah selama sekitar 45 tahun memboikot dan memblokir ekonomi Kuba, dalam usaha untuk menghancurkan kekuasaan pemerintahan revolusionernya Fidel Castro, tetapi tanpa hasil, sampai sekarang.

Perlawanan gigih Kuba melawan segala macam subversi dan sabotase AS selama puluhan tahun ini membikin revolusi Kuba menjadi legendaris, dan menimbulkan respek dan kekaguman banyak orang di Amerika Latin (dan di berbagai negeri lainnya di dunia). Ketokohan Che Guevara, kawan seperjuangan Fidel Castro dalam tahun-tahun awal revolusi Kuba, menjulang tinggi di skala internasional, dan menjadi simbul berbagai gerakan rakyat atau idola sebagian generasi muda.

Untuk menunjukkan simpatinya yang besar terhadap kemenangan Evo Morales, pemerintah Kuba telah mengirim pesawat terbang khusus ke Bolivia untuk menjemput Evo Morales yang berkunjung ke Havana, dan menyambutnya secara besar-besaran sebagai kepala negara sahabat, meskipun ia belum dilantik sebagai presiden terpilih. Sebaliknya, dalam berbagai kesempatan Evo Morales terang-terangan mengakui sebagai seorang pengagum Fidel Castro, dan dengan respek dan kemesraan memanggilnya sebagai “El commandante”.

“POROS KEBAIKAN” MELAWAN “POROS KEJAHATAN”

Kecuali dengan Fidel Castro di Kuba hubungan dan kerjasama yang erat juga telah terjalin antara Evo Morales dengan Hugo Chavez di Venezuela. Dalam siarannya tanggal 20 Januari 2006 BBC News menyebutkan bahwa hubungan antara Venezuela dan Bolivia dinamakan oleh Hugo Chavez sebagai “axis of good” (poros kebaikan), sedangkan “axis of evil” (poros kejahatan) adalah Washington dengan sekutu-sekutunya di seluruh dunia, yang mengancam, menyerang, dan membunuh (who threaten, who invade, who kill, who assasinate).

Dengan orientasi politik luarnegerinya yang tidak mau tergantung kepada AS saja, Eva Morales sebelum dilantik sebagai presiden, sudah mengadakan tour kilat internasional di negeri-negeri penting di 4 benua. Antara lain ia telah mengunjungi Spanyol, Prancis, Belgia, Belanda, Afrika Selatan, Tiongkok dan Brasilia, untuk bertemu dengan berbagai kepala negara dan tokoh-tokoh. Ia telah bertemu dengan presiden Prancis Jacques Chirac, pimpinan Uni Eropa Javier Solana, menteri luarnegeri Belanda Ben Bot, presiden Hu Jintao dari Tiongkok, dan presiden Lula dari Brasilia.

Pers internasional banyak memberitakan tentang kunjungan kilatnya ini, termasuk “keistimewaan” Evo Morales mengenai pakaiannya selama pertemuan dengan berbagai tokoh terkemuka di banyak negeri itu. Sebab, dalam kunjungannya ini ia selalu memakai pakaian yang sederhana, yaitu jaket kulit atau pakaian yang dibuat dengan alpaca (bahan pakaian tradisional yang banyak dibuat orang-orang Indian). Begitu sederhananya, sehingga soal pakaian Evo Morales disoroti oleh sebagian media massa berbagai negeri lebih banyak daripada politiknya. Ada yang menganggap bahwa pakaiannya yang sederhana ini (pakai dasi pun tidak!) kurang menghormati protokol.

SEANDAINYA BUNG KARNO MASIH HIDUP.......!

Dengan adanya perkembangan atau perubahan yang terjadi di berbagai negeri Amerika Latin, yang makin menunjukkan sikap anti-AS, dan perlawanan yang makin meningkat terhadap kapitalisme internasional, maka sekarang orang dapat melihat bahwa berbagai pandangan politik Presiden Sukarno yang anti-imperialis adalah pada pokoknya benar. Juga bahwa sosialisme yang dicoba olehnya untuk diperkenalkan kepada bangsa Indonesia (tetapi yang ditentang habis-habisan oleh para pendukung Orde Baru atau pendiri rejim militer Suharto) ternyata bukanlah momok atau barang yang dinajiskan di berbagai negeri Amerika Latin.

Agaknya, kita patut selalu ingat bahwa presiden Sukarno yang sejak muda sudah “gandrung” kepada sosialisme, telah digulingkan dari kekuasaan dan kemudian dijadikan tapol oleh pimpinan TNI-AD - waktu itu - sampai meninggalnya karena penderitaan dalam tahanan. Presiden Sukarno merupakan tokoh anti-imperialisme dan neo-kolonialisme yang jadi inceran CIA dan sekutu-sekutunya di dalam maupun luar negeri.

Seandainya Bung Karno masih hidup sekarang, maka tentulah ia akan bergembira sekali dan menyambut hangat perkembangan di Bolivia, di Venezuela, di Kuba, di Cili, di Argentina, dan mungkin di Peru nantinya. Karena, misi perjuangan yang diembannya adalah senyawa atau searah dengan apa yang diperjuangkan oleh rakyat dan pemerintahan di berbagai negeri Amerika Latin. Seperti yang kita ingat bersama, Bung Karno pernah menjadi tokoh gerakan rakyat Asia-Afrika dan Amerika Latin (antara lain: dengan Ganefo-nya dan gagasan Conefo- nya, “Games of the New Emerging Forces”dan “Conference of the New Emerging Forces’).

Sudah jelaslah bahwa sikap hangat semacam itu tidak bisa diharapkan dari tokoh-tokoh pemerintahan Indonesia yang sekarang, yang masih didominasi oleh para pendukung Orde Baru atau penyokong rejim militer Suharto, yang mempunyai pendangan yang sama sekali bertolak-belakang dengan arus perkembangan menuju ke kiri di Amerika Latin.

Sekarang makin kelihatan bahwa ketika situasi di banyak bagian dunia sudah dan sedang berubah terus, karena Perang Dingin sudah lewat, para pendukung Orde Baru di Indonesia masih tetap mempertahankan pandangan mereka yang anti-Sukarno, anti-sosialisme atau anti-komunis. Menyedihkan !!!
