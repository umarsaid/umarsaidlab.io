---
layout: post
title: LUAR BIASA ! Medali Kota Paris untuk Umar Said
date: 2011-01-03
---

Pada tanggal 24 Januari 2011 malam mulai jam 18.30 di Paris telah dilangsungkan peringatan ulangtahun yang ke 28 restoran koperasi INDONESIA . Pada kesempatan itu telah juga diselenggarakan di restoran itu pemberian Medali Kota Paris kepada Umar Said, promotor didirikannya usaha kolektif  SCOP Fraternite dan restoran koperasi INDONESIA.

Peringatan ulangtahun Restoran INDONESIA dan sekaligus upacara pemberian medali oleh kota besar Paris ini telah dihadiri oleh sejumlah tokoh-tokoh dan sahabat-sahabat restoran INDONESIA dari berbagai golongan, kebangsaan, serta aliran politik.

Telah hadir dan ikut berbicara panjang dalam pertemuan malam itu Christian SAUTTER, mantan Menteri Budget dan wakil sekretaris jenderal Elysées (Istana Kepresidenan), Louis JOINET mantan  penasehat hukum (juridical adviser) 5 perdana menteri  berturut-turut selama pemerintahan Presiden, MITTERRAND. Madame Danielle MITTERAND yang memerlukan menghadiri pertemuan malam itu juga ikut menyampaikan pidato ucapan selamat.

Danielle DESGUEES, wanita Prancis yang memimpin organisasi La Boutique de Gestion (badan swasta yang sudah membantu pendirian 12 000 perusahaan kecil-kecil) dan merupakan teman dekat yang ikut mempersiapkan berdirinya Restoran koperasi INDONESIA,  berbicara tentang arti berdirinya usaha kolektif ini di Prancis.

Karena dalam kesempatan peringatan ulangtahun ke-28 Restoran Indonesia ini juga  ada upacara pemberian Medali Kota Paris kepada Umar Said, maka akan ada tulisan-tulisan tersendiri mengenai itu semuanya.. Dalam tulisan-tulisan tersendiri selanjutnya itu  akan disajikan singkatan-singkatan isi pidato dari berbagai  pembicara dalam pertemuan itu.

Karena,  ada sejumlah hal-hal yang menarik dalam pidato-pidato itu yang layak disimak, yang berkaitan dengan pendapat sebagian tokoh-tokoh Prancis  terhadap perjuangan melawan Orde Barunya Suharto serta persahabatan dalam membela hak azasi manusia dan  memperjuangkan keadilan.


Medali dengan nama Umar Said (bukan Andre AUMARS)

Nama yang dipilih Kotapraja  Paris untuk pemberian medali adalah nama Umar Said, dan bukannya Andre AUMARS, nama yang dipakai ketika menerima naturalisasi  dan resmi menjadi warganegara Perancis. Keputusan kotapraja Paris untuk memilih nama Umar Said ini adalah menarik, karena tentunya karena berdasarkan pertimbangan-pertimbangan tertentu. Mungkin, nama Umar Said ini untuk menegaskan lagi bahwa  mencakup berbagai kegiatan sebelum menjadi Andre AUMARS.

Christian SAUTTER, mantan Menteri Budget yang mewakili walikota kotabesar Paris,Bertrand DELANOE, mengatakan dalam upacara itu bahwa medali itu memang diberikan kepada nama asli Umar Said, yang diukir di medali  itu. Di medali itu diukir kata-kata « Paris à Umar Said, 2011 » (Paris kepada Umar Said, 2011). Dengan medali ini berarti juga bahwa Umar Said menjadi warganegara kehormatan dari kota Paris (citoyen d’honneur).

Memang, selama ini Medali Kota Paris diberikan setiap tahun kepada orang (atau beberapa orang) dari  berbagai negeri, di samping kepada orang Prancis sendiri. Medali ini diberikan  kepada mereka-mereka yang dianggap sebagai orang yang menonjol dalam bidang-bidang tertentu (ekonomi, sosial, kebudayaan, sastra, dll dll ) dan dianggap berjasa untuk memperkaya kehidupan kota Paris sebagai keseluruhan.

Seperti yang diutarakan dalam berbagai pidato dalam upacara itu Medali Kota Paris diberikan kepada Umar Said berkat berbagai kegiatannya  dalam mendirikan  usaha kolektif yang bernama SCOP Fraternité dan Restoran Koperasi INDONESIA dan juga berkat berbagai kegiatannya  dalam bidang HAM dan melawan segala macam penindasan dan ketidakadilan dan memupuk persahabatan atau persaudaraan dan memperjuangkan kebebasan (fraternité et liberté)

Dalam pidatonya dalam upcara itu, Christian SAUTTER, pejabat tinggi di Prancis yang berhaluan sosialis itu (ia adalah mantan sekretaris kepresidenan di bawah Presiden MITTERRAND) mengatakan bahwa Umar Said adalah militan (pejuang atau aktivis) untuk Fraternité (persaudaraan) dan militan untuk Liberte (kebebasan).

Umar Said katakan : « Penghargaan untuk kita semua » ;

Sesudah menerima Medali Kota Paris itu Umar Said mengucapkan pidato terimakasih atas nama seluruh anggota SCOP Fraternité dan Restoran INDONESIA dan menyatakan bahwa pemberian medali itu tidak saja merupakan kehormatan bagi dirinya dan kebanggaan bagi seluruh keluarganya , melainkan juga untuk seluruh teman-teman di berbagai negeri dan terutama di Indonesia, yang telah mendapat berbagai macam perlakuan yang tidak bermanusiawi dari rejim militer di bawah Suharto.

Medali Kota Paris ini merupakan penghormatan kepada seluruh teman-teman  serta berbagai organisasi Prancis yang telah membantu berdirinya SCOP Fraternite dan Restoran INDONESIA. Antara lain Pascal LUTZ, Paulette GERAUD , Danielle DESGUEES yang sejak semula sudah aktif membantu secara aktif. Dan Louis JOINET, penasehat hukum para Perdana Menteri selama kepresidenan François MITTERRAND.

Umar Said menegaskan bahwa keunikan restoran INDONESIA mempunyai berbagai segi. Di antaranya adalah bahwa ada buku tamu yang sekarang berjumlah lebih dari 24 jilid, dimana para sahabat dan para langganan menuliskan kesan-kesan mereka yang macam-macam. Ada juga seorang teman Prancis, Pascal LUTZ, yang berkat simpatinya yang besar kepada usaha kolektif kita ini, ia bersedia menjadi manager sukarela selama 20 tahun berturut-turut.

Umar Said mengatakan bahwa jarang di Prancis, bahkan di dunia, ada orang yang bersedia bekerja dengan  sukarela sampai begitu lama tanpa dibayar, karena mempunyai rasa persahabatan yang besar dan keyakinan kepada kebaikan ekonomi sosial dan ekonomi solider.

Restoran INDONESIA adalah restoran yang unik di Paris,  bahkan di seluruh Prancis. Restoran ini menjadi tempat untuk persahabatan dan solidaritas, dan karenanya layak untuk didukung dan juga dilindungi bersama-sama supaya bisa terus melanjutkan usaha-usahanya.Oleh karena itu Umar Said memandang secara tulus dan juga dengan keyakinan yang dalam bahwa pemberian Medali oleh Kota Paris berarti,pada hakekatnya, merupakan pernyataan termakasih kepada seluruh orang-orang yang memperjuangkan ekonomi sosial atau ekonomi solider, untuk mengembangkan persahatan dan mempertjuangkan kebebasan.

Dalam pertemuan itu Umar Said menegaskan bahwa sebagai refugee politik yang berasal dari Indonesia telah mendapat suaka di Prancis 37 tahun yang lalu. Ia menyatakan di depan tokoh-tokoh tingkat tinggi Prancis itu bahwa ia merasa bangga menjadi warganegara Prancis, namun bersamaan dengan itu ia tetap menaruh hatinya kepada Indonesia beserta rakyatnya.

Oleh karenanya, sekarang ini dalam usia 82 tahun ia ingin terus aktif dan tetap berguna, dengan kegiatan tiap harinya mengelelola website. Website ini sekarang telah dikunjungi   lebih dari 700. 000 kali, dengan tiap hari rata-rata 200 kunjungan (sebagian terbesar pengunjung dari Indonesia).

Kehidupan dan pengalaman di Paris

Umar Said menegaskan bahwa kehidupannya di Paris sebagai orang yang berasal dari negeri lain, adalah sangat kaya dengan pengalaman , yang juga sangat padat dan bersegi banyak, berkat persahabatan dengan berbagai orang yang dijalin melalui kegiatan bersama.

Di antara  berbagai kegiatan itu adalah ketika menjadi pengurus Komite Timor Timur di Paris, dan hubungan dengan expert HAM di PBB (Jenewa), Louis JOINET. Juga ketika menjadi anggota delegasi Madame Danielle MITTERRAND yang berkunjung ke Indonesia dalam tahun 2000 untuk menemui banyak sekali eks-tapol dan bertemu dengan Presiden Abdur Rahman Wahid.

Dikatakan juga oleh Umar Said, bahwa berkat kemurahan hati dan persahabatan yang diterimanya dari berbagai sahabatnya selama hampir separo umurnya, ia menganggap France Terre d’Asile (Prancis tanah suaka) sebagai tanah-airnya kedua, dimana ia ingin terus melakukan kegiatan-kegiatannya sampai akhir hidupnya.

Sebagai penutup Umar Said mengatakan bahwa Medali Kota Paris merupakan dorongan bagi seluruh anggota SCOP Fraternite dan Restoran INDONESIA untuk terus mempertahankan dan mengembangkan jiwa koperatif (l’esprit coopératif) dan bersamaan dengan itu tetap melanjutkan hasil-hasil positif yang sudah dicapai selama 28 tahun.

Pertemuan persahabatan yang penuh makna

Peringatan ulangtahun ke 28 Restoran INDONESIA kali ini, seperti halnya peringatan-peringatan di tahun-tahun yang lalu, mengingatkan kita semuanya bahwa usaha kolektif ini walaupun pokoknya adalah usaha komersial seperti perusahaan lainnya, namun tidak terpisah sama sekali dari sejarah kelahirannya yang mengandung aspek politik  dan perjuangan untuk menentang Orde Baru beserta sisa-sisanya.

Peringatan ulangtahun kali ini menjadi terasa lebih penting dengan hadinrya tokoh-tokoh tingkat tinggi di Prancis, yang memberikan indikasi bahwa usaha kolektif dengan orientasi yang sudah dijalankan selama ini mendapat perhatian dan juga penghargaan.

Untuk kali ini peringatan ini ditambah dimensinya dengan pemberian Medali Kota Paris kepada Umar Said. Bahwa kotabesar Paris, yang sudah sejak lama mempunyai arti penting dalam sejarah dunia (revolusi Prancis, Commune de Paris, dan sejarah tokoh-tokoh besar Prancis)  memberikan penghargaan yang begitu itu kepada Umar Said adalah suatu hal yang bisa dianggap sebagai hal yang « luar biasa ».

Itulah sebabnya bisa dimengerti bahwa berita tentang akan diterimanya Medali Kota Paris oleh Umar Said ini sudah menimbulkan rasa senang kepada kawan-kawannya  di beberapa negeri. Sebelum dilangsungkannya pertemuan peringatan ulang tahun restoran dan pemberian Medali Kota Paris saja sudah diterima ucapan-ucapan selamat dari Australia, Hongkong, China, Swedia,  Jerman dan Belanda.

Ucapan-ucapan selamat ini menunjukkan bahwa pemberian Medali Kota Paris ini adalah juga merupakan penghargaan atau simpati kepada semua orang yang pernah menjadi korban Orde Baru dalam bentuk dan cara-cara yang berbeda-beda dan juga kepada semua orang yang sedang berjuang untuk melawan segala macam ketidakadilan, dan untuk memperjuangkan kebebasan.
