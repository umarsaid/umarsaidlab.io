---
layout: post
title: Studi banding DPR RI ke Mesir ketika busung lapar
date: 2005-12-18
---

Di tengah-tengah banyaknya berita tentang terjadinya di berbagai daerah Indonesia orang-orang mati karena kelaparan, atau karena busung lapar, dan banyaknya anak-anak balita yang kurang gizi, atau anak pelajar SD yang gantung diri karena ketiadaan pakaian seragam, maka baru-baru ini tersiar kabar yang bisa menyakitkan hati kita semua. Pada tanggal 16 Desember (hari Jum’at) telah berangkat dari Jakarta, secara diam-diam, tidak transparan, bahkan misterius sekali, serombongan besar anggota-anggota DPR RI menuju Mesir untuk “studi banding” .

Tentang kepergian anggota-anggota DPR RI ke luar negeri ini, Wakil Ketua DPR Zaenal Ma'arif mengatakan bahwa ada 15 anggota dan dua staf BURT (Badan Urusan Rumah Tangga) yang melaksanakan “studi banding” ke Mesir. Total anggaran untuk kepergian mereka sebesar US$ 76.170, dengan rincian biaya tiket pergi-pulang US$ 2.735 dan uang saku US$ 195 per hari untuk tiap orang. (Tempo Interaktif, 16 Desember 2005).

Sedangkan menurut siaran Liputan 6 SCTV (16/12/05) : “ Walau tidak mendapat izin dari Ketua DPR Agung Leksono, 24 wakil rakyat tetap melenggang ke Mesir. Para anggota Dewan akan berada di Mesir selama 4 hari dalam rangka menyusun naskah akademi Rancangan Undang-undang Perjudian dan mengunjungi objek wisata. Adapun surat perjalanan dinas (SPJ) untuk setiap anggota DPR sebesar US$ 4.000..”

Masih terus menurut Liputan 6 SCTV:” Sementara nama ke 24 anggota DPR yang pergi terkesan dirahasiakan. Koresponden SCTV/Abdul Ros hanya bisa mewancarai Joko Edi Abdurachman anggota Fraksi Partai Amanat Rakyat sebelum berangkat dari Bandar Udara Sukarno-Hatta. Menurut Joko, seluruh anggota fraksi ikut ke Mesir. Hanya seorang anggota Fraksi Partai Kebangkitan Bangsa yang membatalkan diri karena dilarang oleh fraksinya.

“Kepergian para anggota DPR melanggar surat keputusan pimpinan DPR yang isinya meminta studi banding ke luar negeri tidak dilakukan untuk menghemat anggaran. Keberangkatan anggota Dewan ini memang mengundang kontroversi. Di Mesir, rencananya mereka melakukan penelitian untuk menyusun rancangan Undang-undang Perjudian. Menurut pandangan mereka Mesir dinilai ideal untuk studi banding karena sukses mengelola perjudian.

Masih juga menurut Liputan 6 SCTV :”Kunjungan anggota Dewan ke Mesir memang ada dalam jadwal kegiatan. Namun itu hanya berlangsung sampai siang hari. Setelah itu, para anggota DPR mulai diajak keliling kota Kairo. Bahkan, kunjungan objek wisata mendominasi hari-hari berikutnya. Di antaranya mengunjungi museum, Mesjid Alabaster, pelabuhan dan tentu saja melihat piramida. Tidak hanya itu, mereka masih dijadwalkan berkunjung ke Doha, Qatar, untuk berbelanja dan baru pada 22 Desember nanti kembali ke Jakarta” (kutipan dari siaran Liputan 6 SCTV selesai).

## Korupsi Dalam Bentuk Dan Cara Lain

Soal studi banding ke Mesir ini memang bukan kasus korupsi yang biasa. Ini korupsi dalam bentuk dan cara lain, yang dosa moralnya tidak kalah beratnya dengan korupsi kelas kakap yang berkaitan dengan BLBI, atau kasus Setneg mengenai Hotel Hilton di Gelora Bung Karno, atau korupsi Nazarudin Syamsuddin dari KPU, atau Kombes Suyitno dari Mabes Polri, atau pejabat-pejabat penting dari Mahkamah Agung dan Kejaksaan Agung, atau kasus Probosutedjo adik tiri Suharto.

Karena, kalau banyak kasus korupsi kelas kakap itu menyangkut pencurian dana sampai puluhan atau ratusan miliar Rupiah, bahkan triliunan Rupiah, maka anggaran untuk studi banding ke Mesir ini hanyalah kecil sekali, yaitu sebesar $4000 tiap orang. Jadi, kalau hanya dilihat dari segi jumlah uang, maka anggaran US$ 4000 untuk tiap anggota DPR itu masih belum apa-apanya, atau “gurem” saja, kalau dibandingkan dengan dana-dana yang sudah dihambur-hamburkan dan dikorupsi selama lebih dari 32 tahun oleh orang-orang pendukung setia Orde Baru.

Tetapi, kalau dilihat dari segi moral atau akhlak, maka apa yang dilakukan oleh rombongan “studi banding” ke Mesir oleh para anggota DPR ini sebenarnya sama saja busuknya atau bejatnya dengan moral atau akhlak para koruptor kelas kakap. Apakah ungkapan yang demikian ini tidak kelewat tajam atau tidak keterlaluan kasarnya? Mungkin, mungkin saja. Kalau memang demikian, maka penulis bersedia minta ma’af kepada mereka yang bersangkutan dan juga kepada para pembaca yang merasa tersinggung perasaannya. Tetapi, mohon dengan sangat untuk bersama-sama merenungkan hal-hal sebagai berikut :

Rombongan ke Mesir ini dilakukan oleh anggota-anggota DPR RI yang tergabung dalam Badan Urusan Rumah Tangga (BURT). Jadi missi ini adalah dalam kwalitas dan kapasitas “wakil rakyat”.

Dari berita-berita yang sudah tersiar (antara lain oleh Liputan 6 SCTV) orang mendapat kesan bahwa rencana kepergian untuk “studi banding” ini sebenarnya kurang mendapat persetujuan dari Pimpinan DPR, dengan alasan untuk penghematan anggaran, tetapi toh dipaksakan juga untuk dilaksanakan.

Dari rencana yang sudah disiapkan, tercermin bahwa “studi banding” dan kegiatan penelitian tentang undang-undang perjudian di Mesir ini hanya dilakukan satu hari saja (bahkan mungkin kurang, dalam prakteknya), sedangkan 3 hari selebihnya adalah untuk kegiatan-kegiatan objek wisata

Kalau kegiatan “studi banding” atau “penelitian akademis” tentang undang-undang perjudian ini hanya dilakukan satu hari saja (bahkan tidak sepenuhnya), maka bisa dipertanyakan apa sajakah “studi” atau “penelitian” yang bisa dicapai dalam waktu yang begitu singkat itu? Juga, bagaimanakah kiranya mutu atau kegunaannya?

“Studi banding” atau “penelitian” ke Mesir yang hanya satu hari itu (yang 3 hari lainnya untuk jalan-jalan di Mesir ditambah jalan-jalan 2 hari di Doha) memakan beaya lebih dari US$76.000 untuk seluruh rombongan. Jumlah uang ini jauh lebih besar dibandingkan dengan ongkos untuk mengundang seorang atau bahkan beberapa orang ahli Mesir tentang undang-undang perjudian untuk datang ke Jakarta dan tinggal beberapa waktu lamanya.

Pengaturan jadwal ke Doha (Qatar) untuk berbelanja adalah rencana yang tidak pantas bagi anggota-anggota DPR yang rakyatnya sedang menghadapi berbagai penyakit karena kelaparan atau kurang gizi, dan yang negaranya sedang menanggung utang luar negeri dan dalam negeri yang menggunung, pengangguran yang puluhan juta, dan kemiskinan yang masih meluas di seluruh negeri.

## Begitulah Kwalitas DPR Kita

Kepergian rombongan DPR ke Mesir untuk “studi banding” ini, menunjukkan kepada kita semua, untuk kesekian kalinya, bagaimana sesungguhnya kwalitas moral dan integritas politik dan kesehatan hati nurani para anggota DPR kita hasil pemilu yang lalu. Memang, seperti banyak dikomentari oleh para pakar dalam dan luar negeri, pemilu yang lalu telah dilangsungkan secara bebas (relatif), secara lebih demokratis (juga relatif), secara jujur (lebih-lebih relatif sekali !).

Tidak bisa disangkal, bahwa pengaruh buruk sisa-sisa Orde Baru, masih kuat sekali dalam otak masyarakat luas. Maklum, indoktrinasi yang dilakukan secara intensif oleh rejim militer Suharto dkk selama lebih dari 32 tahun, sangat besar dampaknya. Juga di kalangan para anggota dan tokoh di berbagai partai politik yang ikut pemilu. Terutama sekali di kalangan Partai Golkar, yang selama Orde Baru adalah (bersama TNI-AD) penyangga utama Orde Baru dan pendukung setia Suharto. Walaupun pemilu yang lalu sudah merupakan kemajuan dari para pemilu-pemilu gadungan yang selama 7 kali sudah diadakan di bawah kendali Suharto, tetapi tidak bisa disangkal bahwa Orde Baru dewasa ini masih mempunyai kaki dan tangan dan tulang punggung yang kuat yang mendominasi DPR, yaitu Partai Golkar (yang dipimpin oleh Agung Leksono, Wakil Ketua Golkar).

Sudah jelaslah bahwa dari DPR yang didominasi oleh Partai Golkar (dan simpatisan-simpatisannya) tidak bisa diharapkan adanya perubahan radikal (atau yang berlawanan sama sekali) dengan politik yang pernah dianutnya atau diembannya selama lebih 32 tahun mendukung Orde Baru. Artinya, reformasi yang juga sudah menjadi ketetapan MPR akan sulit dilaksanakan. Sebab, bagi Partai Golkar, reformasi adalah serba merugikan. Apa yang perlu direformasi adalah, pada hakekatnya, justru banyak yang berkenaan dengan Partai Golkar. Jelasnya, sasaran reformasi adalah pada esensinya Partai Golkar yang merupakan pengejawantahan Orde Baru.

## Studi Banding Ini Kejahatan Moral

Dilihat dari banyak segi, maka bisa dikatakan bahwa “studi banding” ke Mesir oleh para anggota DPR kali ini adalah kejahatan moral yang berat sekali dan kesalahan politik yang juga besar sekali. Sebab, ketika banyak rakyat kita yang miskin terpaksa harus menerima bantuan Rp 300.000 selama tiga bulan sebagai kompensasi kenaikan BBM, maka setiap anggota DPR kita menghabiskan uang sekitar US$ 4000 (atau sekitar Rp 40 juta) untuk kegiatan selama 4 hari (satu hari “studi banding” dan 3 hari jalan-jalan di Kairo). Sekali lagi, perlu kita ingat-ingat bersama bahwa hura-hura ke Mesir dan Doha selama lebih dari 6 hari (yang diselipkan satu hari - tidak penuh - ) untuk apa yang dinamakan dengan gagah “studi banding” ini meghabiskan anggaran sebesar sekitar Rp 749 juta.

Ketika di Depok saja banyak anak-anak balita menderita kurang gizi, dan di daerah Makasar juga banyak anak yang busung lapar, atau di Lampung sudah banyak orang terpaksa makan tiwul, maka anggota-anggota parlemen kita diatur untuk bisa berbelanja di kota Doha (Qatar) yang terkenal dengan barang-barang mewahnya.

Jelaslah bahwa “Studi banding” ke Mesir ini, adalah hanya penipuan berjemaah untuk menutupi praktek-praktek yang tidak sehat di kalangan DPR kita. Sayangnya, penipuan berjemaah ini dilakukan oleh hampir semua fraksi (artinya didukung oleh hampir seluruh partai politik) di DPR, seperti halnya banyak korupsi dan macam-macam penyalahgunaan kekuasaan yang dilakukan secara beramai-ramai dan berjemaah pula di berbagai DPRD di seluruh Indonesia.

## Tidak Perlu Ilusi Terhadap DPR Kita

“Studi banding” ke Mesir kali ini, seperti halnya kebanyakan “studi banding” yang lain-lain di masa yang lalu, menunjukkan dengan jelas kwalitas moral dan integritas politik para anggota DPR kita (dan DPRD) dan kebanyakan partai politik kita dewasa ini beserta hasil pemilu yang lalu. Sebagai salah satu sarana dalam kehidupan demokratis memang tetap diperlukan adanya DPR. Tetapi, soalnya, DPR yang bagaimana ? Karenanya, kita semua perlu membuang jauh-jauh segala ilusi bahwa kepentingan rakyat banyak bisa selalu diperjuangkan sebaik-baiknya dan sepenuhnya oleh DPR (dan DPRD). Sepakterjang para anggota DPR kita selama ini sudah membuktikan kenyataan itu.

Artinya, seluruh kekuatan demokratis dalam masyarakat Indonesia perlu - bahkan harus ! - memperjuangkan sendiri masalah-masalah politik, ekonomi, sosial dan kebudayaan yang banyak dan yang pelik-rumit dewasa ini, di samping dengan pandai dan tepat menggunakan segala peluang untuk memaksa DPR memenuhi tuntutan perjuangan itu. Seperti sudah mulai dilakukan selama ini, banyak soal harus diperjuangkan dengan jalan extra-parlementer, lewat jalan-jalan, dalam pabrik-pabrik dan perusahaan, dalam perkebunan, bahkan di kantor-kantor besar. Suara rakyat sudah lama atau sudah sering tidak didengar lagi di DPR.

Sebenarnya, dalam banyak hal, DPR kita sudah bukan dewan yang mewakili rakyat lagi, melainkan lebih berat mewakili kepentingan partai. Dan, seperti sudah kita saksikan sendiri selama ini, kepentingan partai tidaklah selalu sama dengan kepentingan rakyat, bahkan sering sekali berlawanan atau bertolak belakang. Memang, eksistensi partai politik tetap diperlukan. Tetapi kita semua tidak perlu atau tidak boleh terlalu menggantungkan harapan bahwa mereka akan mengutamakan membela kepentingan rakyat. Yang paling penting bagi mereka adalah kekuasaan politik.

Karena itu, dalam menghadapi kehidupan sehari-hari yang makin sulit akibat pengangguran yang meluas, banyaknya perusahaan kecil dan menengah yang ditutup, banyaknya tenaga kerja yang di-PHK-kan, makin besarnya kerusakan dan kerugian yang disebabkan globalisasi ekonomi dan neo-liberalisme, tidak ada jalan lain, kecuali rakyat memperjuangkan sendiri kepentingannya. Karena dalam banyak hal DPR sudah tidak bisa diharapkan berbuat banyak untuk kepentingan rakyat banyak, maka peran gerakan extra-parlementer menjadilah sangat penting. Ini bisa dilakukan dengan bermacam-macam cara dan bentuk, dan di berbagai bidang, melalui LSM, organisasi massa atau perkumpulan buruh, tani, pemuda, perempuan, pegawai,cendekiawan, seniman, sastrawan, dan pengusaha (antara lain).

Seperti dibuktikan oleh banyak pengalaman negeri lain di dunia, gerakan extra-parlementer yang maha-kuat akan bisa membikin kerja parlemen lebih baik lagi. Gerakan extra-parlementer yang besar dan luas merupakan benteng terakhir bagi rakyat dalam membela kepentingannya.

Inilah arah yang perlu kita tempuh bersama menuju Republik Indonesia, yang betul-betul menjadi milik seluruh rakyat.

---

Balipos, 20 Desember 2005

Rombongan DPR Ke Mesir Boyong Istri

Terkuak sudah teka-teki 15 anggota Badan Urusan Rumah Tangga (BURT) DPR RI ke Mesir. Meski alasan mereka ke negeri Fir’aun untuk studi komparasi, ternyata kebanyakan waktu dipergunakan untuk berwisata dan berbelanja.


Selain itu, enam anggota BURT ikut memboyong istri dalam kunjungan tersebut. Dalam aturan tata tertib DPR, jelas ditegaskan anggota dewan saat menjalankan tugasnya di luar negeri dilarang membawa istri atau keluarganya.


Dalam buku panduan program kunjungan BURT DPR-RI ke Mesir tanggal 16-20 Desember 2005 yang dikeluarkan Kedutaan Besar RI di Kairo-Mesir, menyebut jadwal lima hari perjalanan di Mesir, mereka hanya bertemu ketua parlemen Mesir Dr Ahmed Fathi Sourour pada hari ketiga yaitu Minggu (18/12) pukul 12.30 waktu setempat.


Sementara acara lain selama lima hari hanya hura-hura. Misalnya, menghadiri acara jamuan makan malam oleh kuasa hukum interim KBRI di Kairo, mengunjungi obyek wisata di Kairo dan Giza yang dipandu oleh sebuah travel, mengunjungi Museum Nasional di Tahrir Square yang dipandu travel dan berkunjung ke Alexandria.


Ke-15 anggota BURT yang hadir yaitu Roestanto Wahidi (FPD), Ebby Jauhari (FPG), Nizar Dahlan (FBPD), Soeharsojo (FPG), Mukhtarudin (FPG), Soewignyo (FPDIP), Rendhy A Lamadjido (FPDIP), Elva Hartati (FPDI), Syahrial Agamas (FPP), Uray Faisal Deny Sultany Hasan (FPD), Djoko Edhie Soetjipto Abdurrahman (FPAN), Choirul Soleh Rasyid (FKB), Abdullah Azwar Anas (FKB).


Disamping dua sekretaris dari Setjen DPR, yaitu Cholida Indrayana, ikut ke Mesir enam orang pendamping anggota BURT yaitu Noor Jannah Shomad (pendamping Nizar), Dyah Prasetyawati (pendamping (Soeharsojo), Lisa Andriani (Pendamping Deny), Iik Soelasmi (pendamping Ebby) dan Ipuk Fiestandani (pendamping Azwar Anas).


Dari buku profil anggota DPR RI 2004-2009 keluaran Komisi Pemilihan Umum (KPU), nama para pendamping itu tak lain adalah istri dari anggota BURT bersangkutan.
Adapun kemungkinan mereka membawa istri dengan biaya dinas bisa terjadi. Hal ini karena tiket yang dipakai mereka adalah kelas ekonomi. Seorang sumber di DPR mengatakan, mereka ke Mesir memakai tiket ekonomi. "Padahal selama ini, sebagai anggota DPR, mereka dibiayai oleh negara dengan tiket kelas bisnis."
Sementara Djoko Edhie yang pulang dari Mesir lebih dulu (Rabu, 21/12) mengatakan, kedatangan tim BURT ke Mesir tidak efektif karena tidak menghasilkan apa-apa.


Kata dia, pertemuan dengan ketua parlemen Mesir hanya berlangsung selama satu jam mendiskusikan bagaimana mengelola anggaran yang baik. Selain pertemuan hanya satu jam, pembicaraan juga tidak fokus, karena ketua parlemen Mesir harus melayani telepon sebanyak delapan kali dan beberapa kali diganggu oleh datangnya sekretarisnya yang meminta tanda tangan surat.


Menanggapi studi banding yang dilakukan, Djoko meminta dalam waktu mendatang nama studi banding sebaiknya diubah menjadi istilah lain agar tidak menyesatkan.
"Saya pikir istilah studi banding itu sebaiknya diganti dengan nama lain, seperti muhibah atau yang lainnya. Karena kalau studi komparatif maka itu adalah kebohongan publik," cetusnya.


Prihatin


Sementara tim BURT DPR membantah kunjungan mereka ke Mesir dalam rangka studi banding RUU Perjudian. Hal ini dikatakan oleh Roestanto Wahidi dan beberapa anggota rombongan lainnya saat beraudiensi dengan sejumlah Mahasiswa Indoenesia di Mesir, di lobi Hotel Ramsees Hilton Cairo, Senin (19/12) lalu.


Roestanto menyatakan, tujuan kunjungan mereka adalah mempelajari bagaiman parlemen di Mesir bisa hidup, sehingga BURT mendapat tambahan bahan untuk peningkatan kesejahteraan anggota dewan.
"Hal itu sesuai tugas kami membidangi kerumahtanggaan DPR," katanya seperti dituturkan Mushaffa, mahasiswa asal Banjarmasin kepada BPost, melalui milisnya, malam tadi.


Roestanto menyangkal kunjungan mereka ke Mesir tanpa sepengetahuan pimpinan DPR. Dijelaskan bahwa masing-masing anggota rombongan membawa dua surat, dari masing-masing komisi dan fraksi.
"Yang tidak diketahui Zainal Maarif (wakil ketua DPR) adalah kunjungan BURT ke Mesir dalam rangka studi banding UU Perjudian" ucap Roestanto agak emosi.


Ditanya mahasiswa tentang hasil kunjungan selama empat hari di Mesir, salah satu anggota rombongan dari FPG mengakui hanya pada 18 Desember menemui Ketua Parlemen Mesir terpilih, Dr Fathi Surur. Dalam pertemuan BURT meminta informasi tentang kehidupan parlemen dan kesekretariatan Majlis Sya’ab (DPR) Mesir.


Sayangnya, aku anggota FPG itu, mengingat Mesir baru menyelesaikan Pemilu, dan parlemen masih belum stabil, Fathi tidak bisa memberikan banyak keterangan. Dia hanya memberikan Tata Tertib DPR Mesir berbahasa Arab.


Meski banyak dalih dikeluarkan rombongan BURT, namun Persatuan Pelajar dan Mahasiswa Indonesia (PPMI) Mesir menyatakan keprihatinannya kunjungan mereka ke Mesir.
"Mereka tidak memiliki kepekaan sosial dengan kondisi bangsa yang masih belum sembuh dari trauma, busung lapar, flu burung, demam berdarah serta belum pulihnya penanggulangan bencana alam dan Tsunami yang melanda tanah air," tegas mereka dalam realisnya dikirimkan ke BPost.


Organisasi induk pelajar dan mahasiswa Indonesia di Mesir ini mendesak DPR menghentikan program studi banding yang tidak memiliki tujuan yang jelas dan bermanfaat ke luar negeri. JBP/ewa/msh
