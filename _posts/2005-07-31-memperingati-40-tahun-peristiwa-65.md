---
layout: post
title: Memperingati 40 tahun peristiwa 65
date: 2005-07-31
---
Dalam sejarah bangsa dan Republik Indonesia, peristiwa 65 merupakan peristiwa besar yang dampaknya atau akibatnya amat besar pula bagi kehidupan seluruh bangsa, seperti yang sudah disaksikan atau dialami oleh banyak orang selama 40 tahun. Banyak sekali seluk-beluk peristiwa besar ini yang perlu diketahui secara lebih baik lagi oleh bangsa kita, untuk bisa memandang segala persoalan bangsa dan negara kita secara jernih pula.

Tulisan ini (dan tulisan-tulisan lainnya dalam rangka memperingati 40 tahun peristiwa 65) dimaksudkan sebagai ajakan untuk merenungkan, dari berbagai segi, apa arti dan dampak peristiwa besar ini bagi kehidupan bangsa dan negara, baik di masa yang sudah sama-sama kita lalui selama 40 tahun maupun kemungkinan-kemungkinannya di masa yang akan datang.

Karena itu, tulisan ini tidak hanya sekadar berisi segala hujatan atau kutukan yang dimuntahkan terhadap seseorang, atau segolongan, atau sekelompok orang-orang, tetapi juga berisi pembeberan hal-hal buruk, kejahatan, kesalahan, yang sudah terjadi selama 40 tahun, dengan tujuan supaya itu semua menjadi pelajaran berharga bagi bangsa dan anak-cucu kita, untuk tidak terulang lagi. Bisa saja bahwa dalam pengungkapan persoalan-persoalan ditemukan di sana-sini kalimat-kalimat yang terdengar terlalu “keras”, atau kata-kata yang terasa terlalu kasar, atau, bahkan, ungkapan-ungkapan yang terlalu polos dan “kebablasan”. Kalau ada ungkapan yang demikian, harap dimengerti bahwa tujuannya hanyalah untuk menggarisbawahi atau lebih menonjolkan secara tajam sesuatu persoalan.

## Orde Baru Guru Hebat Soal Kejahatan Moral

Peristiwa 65 yang berbuntut atau mengakibatkan “halaman hitam” dalam sejarah bangsa selama kurun waktu yang cukup panjang, yaitu 32 tahun semasa rejim militer Orde Baru di bawah Suharto dkk, merupakan gudang pelajaran yang besar sekali bagi kita semua (bagi yang mau belajar !) di berbagai bidang, umpamanya : perikemanusiaan, moral, demokrasi, pelanggaran HAM, korupsi, penyalahgunaan kekuasaan, penindasan terhadap rakyat. Bolehlah kiranya dikatakan bahwa dalam hal pelanggaran kemanusiaan dan kejahatan moral, rejim militer Orde Baru-nya Suharto dkk adalah guru yang hebat sekali.

Banyak sekali hal-hal mengenai “40 tahun peristiwa 65 “ yang bisa ditulis dan dipelajari oleh para sejarawan kita, diteliti oleh kaum intelektual di berbagai bidang, dicernakan dan diolah oleh para sastrawan dan budayawan kita. Oleh karena itu, adalah hal yang patut disayangkan sekali, dan bahkan merupakan dosa besar kita semua, kalau masalah yang begitu besar dan amat penting itu dibiarkan lewat begitu saja, dan tidak dimanfaatkan untuk bisa menjadi khazanah yang berharga sekali bagi bangsa dan bagi generasi yang akan datang.

Bagi semua orang yang mempunyai hati nurani yang bersih, atau yang memiliki fikiran waras, atau dikaruniai nalar yang sehat, setelah bisa merenungkan dalam-dalam dan melihat secara menyeluruh masa Orde Baru yang puluhan tahun itu, maka bisalah kiranya menyimpulkan bahwa rezim militernya Suharto dkk itu adalah betul-betul merupakan bencana yang telah menimbulkan banyak kerusakan dan pembusukan bagi negara dan bangsa Indonesia..

Selama 40 tahun sejak 1965 sampai sekarang, sebagian besar rakyat Indonesia sudah mengalami dan merasakan sendiri betapa hebatnya kerusakan atau pembusukan yang ditimbulkan oleh bencana itu, yang akibatnya berupa berbagai persoalan parah bisa kita saksikan sekarang ini. Kebejatan mental atau kebobrokan moral yang menghinggapi secara luas kalangan “atas” dewasa ini adalah bukti nyata dari kerusakan yang ditimbulkan Orde Baru dan yang dilanjutkan oleh pemerintahan-pemerintahan penerusnya.

Mengingat itu semuanya, sudah sepatutnyalah bagi mereka yang sudah pernah mengalami berbagai penderitaan akibat politik rejim militer Suharto dkk untuk mengajak orang-orang yang masih tertipu oleh indoktrinasi puluhan tahun (termasuk orang-orang yang tadinya pernah mendukung rejim militer) untuk menjadi sadar tentang kesalahan dan kejahatan yang telah dilakukan oleh Orde Baru serta pendukung-pendukungnya.

## Peristiwa 65 Adalah Urusan Semua Orang

Peringatan “40 tahun peristiswa 65” ini sebenarnya, dan seharusnya, adalah urusan semua orang dari semua golongan dalam masyarakat yang menyadari bahwa selama puluhan tahun yang lalu telah terjadi banyak sekali hal-hal yang benar-benar merugikan kepentingan banyak orang, atau yang menimbulkan penderitaan bagi puluhan juta orang. Jadi, peringatan “40 tahun peristiwa 65” bukanlah melulu hanya urusan para korban 65 saja. Meskipun, di antara orang-orang yang dibikin sangat menderita itu – dan berkepanjangan dalam waktu puluhan tahun pula ! – adalah terutama para korban peristiwa 65 beserta keluarga mereka dan para eks-tapol beserta keluarga, yang diperkirakan jumlahnya mencapai 20 juta.

Pembantaian besar-besaran tahun 65 terhadap kira-kira 3 juta orang tidak bersalah dan pemenjaraan sewenang-wenang ratusan ribu orang (yang tak bersalah juga!) adalah satu di antara sejumlah kesalahan (lebih tepatnya : kejahatan) terbesar yang pernah dilakukan rejim Orde Baru, yang didirikan oleh TNI-AD dengan dukungan Golkar. Kesalahan (atau kejahatan!) monumental lainnya adalah penggulingan dan penyerobotan kekuasaan Bung Karno oleh Suharto dkk dan penghancuran PKI.

Pimpinan TNI-AD dalam tahun 1965 telah menggunakan dalih mengganyang G30S untuk menghancurkan PKI beserta ormas-ormasnya, yang merupakan pendukung utama kekuatan politik Bung Karno, dan kemudian membangun diktatur militer yang mereka beri nama Orde Baru. Diktatur militer Orde Baru inilah yang dengan tangan besi telah memerintah Indonesia dan menindas sebagian terbesar rakyat Indonesia dengan berbagai cara dan bentuk. Kalau dilihat secara jernih, dan dengan pandangan yang jauh, sejak itu pulalah yang merupakan pengkhianatan pimpinan TNI-AD (pada waktu itu) terhadap Bung Karno dan terhadap perjuangan rakyat Indonesia.

Oleh karena itu, kita semua (termasuk mereka yang bukan korban peristiwa 65) patut dan berhak untuk tiap tahun (kalau bisa) mengadakan berbagai kegiatan untuk memperingati peristiwa yang oleh Orde Baru (dan para pendukungnya) telah dipakai modal untuk menindas sebagian besar rakyat sendiri, hanya demi keuntungan dan kemewahan sebagian kecil sekali bangsa kita.

## Mengutuk Kejahatan Suharto Adalah Sah Dan Luhur

Sekarang, setelah 40 tahun kita lewati bersama, muncul kesempatan dan juga kemungkinan bagi kita semua untuk melihat sejarah Orde Baru secara lebih mendalam, dan dengan memandang berbagai persoalan bangsa kita secara hakiki. Untuk itu dalam memperingati peristiwa 65 perlu kita soroti kembali pengkhianatan Suharto (dan para pendukugnya) terhadap Bung Karno. Karena, sekarang makin jelaslah bahwa dengan dalih-dalih keterlibatan Bung Karno dalam G30S dan dengan alasan keterdekatannya dengan PKI maka ia digulingkan dan oleh tokoh-tokoh TNI-AD yang dibenggoli oleh Suharto, yang bersekongkol dengan kekuatan asing.

Oleh karena itu, mengutuk berbagai kesalahan dan kejahatan Suharto dan konco-konconya yang telah dibuatnya selama 32 tahun terhadap rakyat dan Republik Indonesia adalah merupakan kewajiban yang luhur dan juga hak yang sah bagi setiap orang yang merasa ikut memiliki negara , demi kepentingan bangsa dan demi keturunan kita yang akan datang. Tidak mengutuk kesalahan dan kejahatan politik Suharto adalah sikap politik yang salah, dan sikap moral yang keliru. Juga, bersikap “netral” adalah manifestasi dari ketidakpedulian terhadap kejahatan dan ketidakpekaan terhadap ketidakadilan.

Untuk menghilangkan salah satu di antara berbagai sumber penyakit parah dan kebusukan yang menghinggapi bangsa kita dewasa ini, dan juga untuk menyelamatkan kepentingan generasi yang akan datang bangsa kita, maka perlu sekali kita membeberkan segala kesalahan dan kejahatan Suharto.beserta para pendukungnya atau simpatisannya , yang sampai sekarang masih banyak menyelinap di berbagai bidang.

Adalah salah besar, kalau ada orang yang karena tidak senang kepada politik Bung Karno atau tidak setuju dengan apa yang berbau PKI, maka ia menganggap bahwa Suharto (yang mengkhianati Bung Karno) adalah baik. Sekarang, bagi banyak orang makin jelas bahwa banyak hal-hal yang telah dilakukan Suharto yang bertolak-belakang sama sekali, atau bertentangan secara total, dengan apa yang dilakukan oleh Bung Karno.

Karenanya, maka secara tajam kita bisa melihat, dan mengerti, bahwa Orde Baru adalah pada dasarnya, atau pada intinya, adalah suatu pengkhianatan, adalah suatu kekuatan reaksioner, yang sudah menimbulkan banyak kerusakan dan kebusukan yang parah sekali kepada bangsa Indonesia selama puluhan tahun, yang akibatnya seperti kita saksikan dewasa ini diberbagai bidang kehidupan bangsa.

## Pelanggaran HAM Yang Paling Besar

Dalam memperingati ulangtahun peristiwa 65 yang ke-40 , perlu sekali dingat-ingat oleh kita semua bahwa kira-kira 20 juta orang keluarga (dekat dan jauh) korban 65 yang masih terus - sampai sekarang, sudah 40 tahun !!! -- mengalami banyak penderitaaan yang disebabkan oleh berbagai peraturan yang dibikin rezim militer Orde Baru. Sebanyak kira-kira 30 macam peraturan, undang-undang, dan ketentuan masih terus membikin berbagai macam penderitaan bagi para eks-tapol dan para korban peristiwa 65 lainnya.

Jadi, memperingati peristiwa 65 berarti bukan hanya mengangkat terbunuhnya 6 jenderal TNI yang dilakukan oleh G30S, tetapi juga mengungkapkan berbagai macam akibat-akibat tindakan TNI-AD yang menyebabkan dibunuhnya jutaan orang-orang tidak bersalah, yang terdiri dari anggota-anggota dan simpatisan PKI, dan menahan sewenang-wenang serta membikin penderitaan bagi orang-orang dalam jangka lama, dan menyerobot kekuasaan negara dan pemerintahan dari tangan Bung Karno, dan mendirikan rejim militer yang represif sekali.

Perlu sekali selalu menjadi ingatan kita bersama bahwa sejarah peristiwa 65 adalah, pada intinya, sejarah dimulainya pelanggaran HAM secara besar-besaran dan dalam jangka yang lama sekali (puluhan tahun) oleh rezim militer Orde Baru. Dan, pelanggaran HAM dalam tahun 65 adalah pelangggaran HAM yang paling besar-besaran dan paling parah sepanjang sejarah Republik Indonesia, yang sudah menjadi pembicaraan umum di mana-mana, termasuk di mancanegara.

## Kesesatan Iman Dan Kerendahan Budi

Oleh karena itulah kita bisa –bahkan harus ! – mengatakan bahwa siapapun (dan dari kalangan yang manapun !) yang bersikap menyetujui politik atau mendukung tindakan rezim militer Orde Baru terhadap puluhan juta keluarga korban peristiwa 65 dan eks-tapol (dan terhadap Bung Karno) adalah pencerminan kerendahan budi nurani, atau tanda kesesatan iman, atau bukti kebobrokan moral.

Sebab, sudah jelaslah bagi nalar yang sehat, dan bagi budi rohani yang bersih, baik di kalangan yang tidak menyetujui politik Bung Karno maupun yang anti-komunis pun, bahwa pembunuhan besar-besaran dan pemenjaraan begitu banyak orang-orang tidak bersalah oleh rezim militer Orde Baru -- selama puluhan tahun ! -- adalah pelanggaran HAM yang serius sekali.

Membiarkan berlangsungnya terus-menerus – sampai sekarang ! - kesalahan besar dan aib atau dosa bangsa yang telah dilakukan oleh rezim militer Orde Baru adalah sikap moral yang salah, dan adalah juga iman yang sesat. Berusaha dengan segala cara dan bentuk untuk bisa dihentikannya kesalahan besar ini adalah usaha yang benar, adil, dan sah menurut ajaran agama yang manapun juga, baik Islam, Katolik, Protestan, Budha, Hindu, Konghucu maupun kepercayaan lainnya.

Dalam memperingati 40 tahun peristiwa 65 patutlah kiranya kita mengenang kembali mereka semua yang telah hilang dan menjadi korban tindakan pembantaian besar-besaran yang tidak berperikemanusiaan, dengan tekad untuk terus ikut berusaha - dengan segala daya dan cara - supaya penderitaan yang sudah berlangsung puluhan tahun itu akhirnya bisa dihentikan bersama-sama.
