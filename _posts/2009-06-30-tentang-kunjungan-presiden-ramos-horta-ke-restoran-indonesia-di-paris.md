---
layout: post
title: Tentang kunjungan presiden Ramos Horta  ke restoran INDONESIA di Paris
date: 2009-06-30
---

Bagi para pembaca yang tidak sempat (atau kelewatan) membaca tulisan « Presiden Ramos Horta mendadak mengunjungi Restoran INDONESIA di Paris » yang sudah disajikan di berbagai milis  masih bisa  menyimaknya dalam website http://umarsaid.free.fr/ . Karena dipasang dalam website, maka tulisan tersebut masih bisa terus-menerus atau selalu dibaca oleh para pengunjung website.



Dilihat dari berbagai segi, kunjungan presiden Ramos Horta ke restoran INDONESIA mempunyai arti yang sangat penting, dan patut diketahui oleh banyak orang. Oleh karenanya, dengan dipasangnya tulisan tersebut dalam website, maka sewaktu-waktu dapat dibaca atau disimak terus oleh siapa saja. Kunjungan presiden Ramos Horta ke Restoran INDONESIA kali ini, sebagai kepala negara Republik Demokratik Timor Leste, dan pertemuannya dengan sahabat-sahabat perjuangannya yang lama, merupakan peristiwa yang menarik.



Sebab, kunjungannya ini tidaklah sekadar untuk makan-makan enak, karena ia sebagai tokoh terkemuka mempunyai syarat-syarat dan fasilitas untuk bisa makan-makan enak di mana-mana. Ia juga sudah mengenal masakan Restoran INDONESIA di Paris, karena sudah sering merasakannya waktu tiap kali berkunjung ke Prancis. Kunjungannya kali ini mempunyai dimensi yang lain.



Tulisan Ramos Horta dalam buku tamu 2006



Kali ini, ia juga bisa membaca kembali tulisannya dalam buku tamu (bahasa Inggrisnya « guest book », bahasa Prancisnya « livre d’or ») yang dibuatnya dalam tahun 2006 ketika ia masih belum menjadi presiden. Beberapa tandatangannya tercantum dalam halaman-halaman buku tamu restoran, yang sampai sekarang sudah mencapai 23 jilid semenjak dibukanya restoran akhir 1982 (jadi sudah lebih dari 26 tahun yang lalu).



Ketika dalam tahun 2006 ia berkunjung lagi dan makan di restoran, ia menulis dalam buku tamu kalimat-kalimat yang berbunyi sangat indah dan juga mempunyai arti yang besar dan dalam. Tulisan itu adalah sebagai berikut : « It is always a great pleasure, an exceptional experience to revisit this great restaurant of my Indonesian brothers and sisters ». Jose Ramos Horta. 15-3-2006.



(Terjemahannya secara  bebas : «  Adalah senantiasa merupakan kegembiraan yang besar, dan pengalaman yang istimewa untuk mengunjungi kembali restoran besar ini yang dimiliki saudara-saudara saya laki-laki dan perempuan dari Indonesia ». Jose Ramos Horta).



Jelaslah bahwa yang dimaksudkannya dengan « great restaurant » bukanlah besarnya gedung atau jumlah kursi yang hanya sekitar 70 (ruangan atas 35 dan 35 juga di ruangan bawah), melainkan « great » dalam arti sejarahnya dalam perlawanan terhadap rejim militer Suharto. Juga perkataan  « my Indonesian brothers and sisters » bunkanlah sekadar basa-basi di lamis bibir yang sering digunakan (atau lebih tepat disalahgunakan) oleh banyak orang. Kata-kata Ramos Horta ini mengandung arti politik dan moral yang besar.



Sudah tentu isi buku tamu yang sebanyak itu (jarang ada restoran lainnya yang mempunyai buku tamu sebanyak itu !) berisi macam-macam tulisan orang dari berbagai kalangan yang datang dari banyak negeri. Pada umumnya, atau sebagian terbesar dari pengisi buku tamu itu menyatakan senang atau kepuasan mereka terhadap masakan yang dihidangkan, suasana yang « lain » dari kebanyakan restoran lainnya, dan pelayanan yang menyenangkan.



Itulah sebabnya, maka restoran koperasi ini masih bisa berjalan dengan baik selama begitu panjang umurnya, walaupun mengalami juga masa-masa pasang surut, seperti halnya banyak perusahaan lainnya. Rupanya, presiden Ramos Horta kelihatan juga ikut senang atau bahkan bangga bahwa restoran bisa tetap hidup dengan baik .



Pesan politik dan pesan moral yang penting



Hal yang demikian adalah wajar, karena dalam sejarah restoran banyak kaitannya dengan perjuangan bersama rakyat Timor Timur dan rakyat Indonesia melawan rejim militer Suharto. Cerita tentang tidurnya Ramos Horta di kursi restoran dan mandi di douche di ruangan bawah, yang juga dipakai untuk membersihkan  barang-barang, merupakan bagian dari sejarah persahabatan antara pejuang Ramos Horta dan restoran INDONESIA di Paris.



Namun di samping itu semua kunjungan presiden Ramos Horta kali ini ke restoran INDONESIA bisa dilihat mempunyai arti yang menjangkau jauh dan mengandung pesan (message) politik dan pesan moral yang lebih penting. Melalui keinginannya untuk bertemu santai dengan 3 sahabat perjuangannya  yang lama, dan mengenang bersama masa-masa sulit di masa yang lalu, kelihatan bahwa sekarang sebagai presiden ia tetap menghargai  mereka.



Lebih-lebih lagi, penghargaan terhadap sahabat-sahabat perjuangannya yang lama itu nampak dari diutarakannya rencananya yang penting dan besar, yaitu penggunaan bahasa Indonesia sebagai bahasa resmi (official language) di Timor Timur. Menurut rencananya, keputusan untuk menggunakan bahasa Indonesia sebagai bahasa resmi ini akan diumumkan bertepatan dengan ulangtahun ke-10 (bulan Agustus)  diselenggarakannya referendum di Timor Timur.



Seperti kita ingat bersama, setelah jatuhnya Suharto dari kursi kepresidenan tahun 1998, perjuangan rakyat Timor Timur melawan rejim militer Indonesia semakin menguat dan mendapat dukungan PBB dan kalangan luas di dunia. Dalam referendum (jajak pendapat)  pada tanggal 30 Agustus 1999.rakyat Timor Timur relah menyatakan dengan jelas keinginan mereka untuk merdeka dari penjajahan militer Indonesia. Dengan kemenangan ini, terhitung sejak 30 Agustus 1999, Timor Timur berpisah dari Republik Indonesia untuk menjadi sebuah negara independen.



Kalau rencana presiden Ramos Horta ini terlaksana, maka merupakan landasan yang kokoh sekali bagi pengembangan hubungan bersahabat antara dua rakyat yang pernah selama puluhan tahun mengalami penderitaan dan kesengsaraan akibat kesalahan pimpinan militer Indonesia di masa yang lalu. Rencana yang menguntungkan kepentingan dua rakyat ini akan menjangkau jauh sekali dan bersejarah di kemudian hari.



Pesan persahabatan antara dua rakyat yang disampaikan presiden Ramos Horta di depan 3 sahabat lamanya inilah yang menambah indahnya isi kunjungannya di restoran INDONESIA di Paris akhir bulan Juni ini. Dari sudut ini dapat dilihat bahwa presiden Ramos Horta tidak melupakan sebagian masa lalu, tetapi juga mengutamakan masa depan yang lebih baik antara dua negara Indonesia dan Timor Leste.



Berbagai reaksi tentang kunjungan presiden Ramos Horta



Dari reaksi yang sudah mulai diterima dari berbagai pembaca, maka nyatalah bahwa kunjungan presiden Ramos Horta ini menarik perhatian banyak kalangan, mengingat arti yang dikandung di dalamnya. Pesan (message) politik dan juga pesan moral yang disampaikan oleh tindakan presiden Ramos Horta melalui pertemuannya  dengan 3 sahabat-sahabatnya yang lama merupakan sesuatu yang sangat besar. Berikut adalah sebagian cuplikan dari berbagai reaksi tentang kunjungan yang mempunyai arti sejarah tersebut :



Dari Iwamardi (Jerman) : «Berita yang sangat menggembirakan, baik yang pernah ikut dikomite untuk Timor Timur maupun semua yang menaruh simpati kepada perjuangan rakyat Timor Timur melawan rejim agresor orba Suharto yang pada saatnya terlalu amat kuat dibanding Fretilin!  Pertemuan teman teman seperjuangan yang sangat mengesankan tentunya.
Restoran Indonesia adalah salah satu organisasi yang  telah menjalankan peranan yang tidak kecil  dalam menggalang persatuan dan solidaritas rakyat tertindas dikedua negeri, Indonesia dan Tim Tim  saat itu !
Teruskan peranan yang positif Restoran Indonesia di Paris.
Bebaskan dari pikiran pikiran  individualis, pengangkat derajat diri sendiri , yang  ingin menonjolkan diri sendiri , misalnya yang mengangkat dirinya sebagai  "pahlawan rekonsiliasi" ( yang semu) antara kedua kubu seni di Indonesia hanya untuk kepentingan pribadi.
Hidup Restoran Indonesia, restoran yang bertumpu pada politik yang sehat, yang membela rakyat miskin Indonesia dan ajaran Bung Karno !"



Dari Djoko  (Paris) : « Ada pepatah Inggris: "A friend in need is a friend indeed", yang terjemahannya kira-kira "teman waktu kita dalam kesulitan adalah teman sejati". Presiden Ramos Horta rupanya tidak melupakan teman-teman Indonesianya di Paris dan menganggapnya sebagai "teman sejati". Sikap demikian perlu dihargai.
Simpati dan dukungan teman-teman Indonesia yang terutama diwakili oleh Bung Umar Said memang nyata, meskipun pernah ada salah paham, yaitu waktu Rejim Suharto melakukan pembantaian di Timtim (soal ini mungkin cawapres-cawapres Wiranto dan Prabowo bisa cerita lebih terperinci), ada tilpun ke Restoran Indonesia dengan nada marah-marah dan akan membalas dendam atas pembantaian tersebut. Tetapi salah paham tersebut akhirnya bisa dijernihkan. »



 Dari Tossy Aboepriyadi S (Solo) : « Menarik cerita Bung Umar Said. Jose RH di sini napak tilas. Hormat kepada Bung Umar, Emil dan Restoran Indonesia Paris yang termasuk segelintir pertama orang Indonesia yang sejak dini konflik TimTim sudah solider dan mendukung perjuangan kemerdekaan Timor Timur, sementara kebanyakan org Indonesia - juga yg di rantau - bertahun2 terus menerus "NKRI" (mendukung nasionalisme-nya Soeharto cs. dst) is terhadap TimTim. Lagi pula musibah kemanusiaan TimTim adalah paling dahsyat setelah musibah 1965-66. »



Dari Najib (Holland) :» Terima kasih telah berbagi cerita yang menarik dan menggugah ini.

Saya menikmati membacanya: sejarah, kenangan, penindasan, perlawanan,

perjuangan, persahabatan... di atas semuanya: jalinan indah kemanusiaan! «
