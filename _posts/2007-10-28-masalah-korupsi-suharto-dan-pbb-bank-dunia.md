---
layout: post
title: Masalah korupsi Suharto dan PBB-Bank Dunia
date: 2007-10-28
---

Masalah korupsi Suharto dan

Transparency International

Transparency International, yang telah berkali-kali me-release berita atau informasi mengenai korupsi besar-besaran yang dilakukan Suharto, telah menyelenggarakan pertemuan tahunan di Bali yang berakhir pada hari Minggu tanggal 28 Oktober 2007. Dalam pertemuan ini telah dibicarakan berbagai masalah korupsi yang terjadi di dunia.

Mengenai pertemuan ini Tempo Interaktif telah menurunkan sebuah berita, seperti yang tertera di bawah ini. Walaupun dalam berita ini tidak ditulis secara jelas mengenai korupsi yang merajalela di Indonesia, terutama yang dilakukan oleh Suharto dan keluarganya, namun mudah diduga bahwa masalah Suharto juga menjadi pembicaraan di kalangan para peserta pertemuan badan internasional ini.

Sebab, ada masalah besar yang terkait -- secara langsung atau tidak langsung – dengan Transparency International. Yaitu kasus laporan majalah TIME 24 Mei 1999 mengenai kekayaan keluarga Suharto yang berasal dari korupsi dan penyalahgunaan kekuasaan. Sebagian penting dari laporan majalah TIME ini didasarkan atas hasil riset Transparency International.

Karena Suharto telah menggugat majalah TIME dengan alasan mencemarkan nama baiknya, dan gugatan Suharto tersebut dinyatakan menang oleh Mahkamah Agung, maka wajarlah kalau kalangan Transparency Internasional memberikan perhatian terhadap kasus Suharto.

Kasus Suharto ini kiranya juga menarik perhatian dalam pertemuan di Bali ini ketika dibicarakan juga program PBB dan Bank Dunia mengenai pengembalian harta yang dicuri para koruptor (StAR Initiative).

Karena berita yang disiarkan Tempo Interaktif tidak menyinggung soal-soal korupsi Suharto, maka bisa diharapkan bahwa Tranparency International Indonesia yang dipimpin oleh Todung Mulya Lubis SH akan mengeluarkan release atau informasi yang berkaitan dengan pertemuan di Bali dan yang berkaitan dengan masalah korupsi di Indonesia umumnya dan masalah korupsi Suharto khususnya.

A. Umar Said

* * * * *

Berita Tempo Interaktif tanggal 28 Oktober 2007 selengkapnya berbunyi sebagai berikut :

Transparency International Serukan Empat Resolusi


« Petemuan tahunan para anggota Transparency International yang berakhir hari ini Minggu (28/10) di Bali, menghasilkan empat resolusi. Meliputi pernyataan terhadap konvensi Perserikatan Bangsa-Bangsa untuk melawan korupsi, resolusi terhadap situasi terakhir di Burma, resolusi terhadap konvesi negara-negara yang tergabung dalam organisasi kerjasama ekonomi dan pembangunan (OECD), dan resolusi terhadap situasi terakhir di Pakistan.

Hadir dalam pleno terakhir tersebut sekitar 70 perwakilan dari 99 negara anggota. Pleno para anggota ini merupakan sesi terakhir dari rangkaian pertemuan yang diselenggarakan oleh Transparency International Indonesia sejak Rabu 25 Oktober lalu.

Dalam resolusi mengenai konvensi perserikatan bangsa-bangsa dalam melawan korupsi (UNCAC), Transparency International mendesak para negara-negara anggota untuk meluncurkan sebuah program mengenai pengembalian aset dari harta-harta negara yang dikorupsi oleh para penguasanya. Lembaga ini juga mendesak negara-negara yang menandatangani konvensi itu untuk memapankan sebuah mekanisme evaluasi terhadap penerapan program pengembalian aset itu.

Dalam resolusi menanggapi situasi di Burma Transparency International mendesak komunitas internasional, sektor bisnis, dan pemerintah Burma untuk mengambil langkah-langkah yang tegas untuk menghentikan tinadakan pelanggarn hak asasi manusia yang meluas. Lembaga ini juga mencatat bahwa pemerintahan Junta di Burma yang sangat korup terbukti tidak mampu memberikan respon terhadap kebutuhan warga, terutama dalam memastikan pemenuhan terhadapa hak-hak asasi manusia, termasuk perlakuan yang sama dihadapan hukum.

Dengan catatan itu, Transparency International meminta kepada negara-negara ASEAN dan Perserikatan Bangsa-Bangsa untuk memperkuat tekanan, baik terhadap pemerintahan Junta di Burma maupun komunitas global untuk segera menghentikan pelanggaran hak asasi manusia yang terjadi.

Resolusi ini juga ditujukan kepada perusahaan-perusahaan dan komunitas bisnis internasional yang memiliki hubungan dagang dengan Burma untuk mengambil langkah-langkah guna memastikan pembayaran mereka dilakukan secara transparan. Desakan itu diminta untuk memastikan para pebisnis ini tidak hanya akan memperkaya para pejabat korup yang pada akhirnya akan memperkuat Junta militer yang berkuasa.

Ketua Transparency International Indonesia, Todung Mulya Lubis mengatakan pihaknya juga meminta kepada pleno untuk menambahkan poin dalam resolusi kali ini yang berisi permintaan untuk membebaskan tokoh demokrasi Burma yang kini ditahan, Aung San Suu Kyi. « Kami juga mendesak pembebasan terhadap para pejuang demokrasi lainnya yang hingga kini masih ditahan Junta terkait protes mereka bebrapa waktu lalu, » katanya.

Dalam resolusi tentang konvensi OECD, Transparency International mendesak para negara anggota dan 37 pemerintahan yang menandatangani untuk memperkuat aksi mereka guna memastikan pemenuhan konvensi untuk memberantas penyuapan yang dilakukan oleh para pelaku dari negara-negara lain.

Menurut Todung, beberapa negara anggota OECD yang tergabung dalam Transparency juga mengeluhkan sikap Inggris yang dinilai kurang memberikan perhatian yang cukup terhadap pemeberantasn korupsi dinegara-negara lain. Todung mencontohkan kasus diterimanya Thaksin Sinawatra, yang membeli klub sepak bola Manchester City di Inggris dengan tangan terbuka. Padahal, dinegaranya sendiri Thaksin dituduh menjadi tersangka korupsi yang merugikan keuangan Thailand dalam jumlah jutaan dolar.

Resolusi terakhir berisi mengenai pernyataan terhadap diterimanya kembali Benazir Bhutto oleh pemerintah Pakistan. « Padahal kita tahu Benazir Bhutto adalah orang yang sangat korup ketika ia menjadi penguasa Pakistan, » ujar Todung. « Itu bisa dianggap sebagai impunitas yang diberikan pemerintah terhadap seorang koruptor. »

Setelah pertemuan di Bali ini beberapa pimpinan Transparency International dan perwakilannya di Indonesia akan menghadap presiden Susilo Bambang Yudhoyono di istana pada Senin siang. Pertemuan itu sekaligus akan meminta presiden untuk mengambil langkah-langkah yang lebih aktif dan memimpin pembentukan sebuah kaukus Transparency International di tingkat regional. « Para anggota transparency di ASEAN telah meminta Indonesia untuk memelopori pembentukan kerjasama regional yang lebih solid, » kata Todung. Y. Tomi Aryanto
