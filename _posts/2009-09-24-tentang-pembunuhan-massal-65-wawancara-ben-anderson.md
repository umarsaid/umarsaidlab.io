---
layout: post
title: Tentang pembunuhan  massal ‘65 (wawancara Ben Anderson)
date: 2009-09-24
---

Sebagai lanjutan penyajian bahan-bahan untuk renungan bersama dalam rangka memperingati  44 tahun peristiwa 1965 , berikut di bawah ini disiarkan kembali wawancara Ben Anderson, seorang profesor Amerika yang sejak 65 sudah melakukan banyak penelitian tentang berbagai hal yang berkaitan dengan peristiwa besar berdarah itu.

Wawancara ini, yang mengungkap hal-hal yang penting dan menarik sekali, telah diadakan tanggal  20 & 23 September 1996, dan telah disiarkan dalam berbagai media. Wawancara yang agak panjang ini masih dapat disimak oleh siapa saja dalam Google, dengan meng-klik « pembunuhan massal –Ben Anderson ».

Banyak hal-hal yang dikemukakan oleh Ben Anderson merupakan bahan penting untuk menambah pengetahuan kita bersama tentang pembunuhan massal yang dilakukan oleh militer di bawah Suharto, atau tentang berbagai soal yang berkaitan dengan G30S, dan tentang kudeta merangkaknya Suharto atau penyerobotan kekuasaan terhadap Bung Karno.

Berhubung dengan panjangnya wawancara ini, maka untuk berbagai mailing-list disajikan dalam dua bagian. Teks selengkapnya (termasuk bagian yang kedua) disajikan sekaligus dalam website ini.

A. Umar Said

Paris, 24 September 2009

=   =  =  =  =



Tentang Pembunuhan Massal ‘65

Ben Anderson


Ben Anderson adalah profesor ilmu politik di Cornell. Sekarang dia sedang menjadi dosen tamu di Yale. Bagaimana jenderal-jenderal itu dibunuh? “Tentara atau pimpinan tentara tahu betul bahwa jenderal-jenderal itu dibunuh oleh sesama tentara. Sama sekali tidak ada siksaan, tidak ada penganiayaan, dsb.” Mengapa media massa menyiarkan kabar bohong? “Saya yakin itu justru diperintahkan oleh Suharto bersama orang yang dekat-dekat dia seperti Ali Murtopo. Mereka dengan darah dingin sekali membuat kampanye fitnah untuk kepentingan politik.” BAGAIMANA JENDERAL-JENDERAL ITU DIBUNUH?


T: Untuk studi tentang pembunuhan 6 orang jenderal dan seorang letnan pada tgl 1 Oktober 1965, Pak Ben menggunakan laporan otopsi jenasah mereka. Studi Pak Ben diterbitkan dalam majalah Indonesia nomor 43, April 1987, berjudul “How did the generals die?” Kami ingin mengetahui hasil studi itu. Apakah laporan otopsi itu asli? Dari mana Pak Ben mendapat laporan otopsi itu?


J: Asli. Ini suatu kebetulan. Waktu itu kira-kira awal tujuh puluhan. Atas desakan dari Pak Kahin, saya, dsb, akhirnya disampaikan, oleh orang-orang CSIS-nya Benny Murdani, beberapa kilo berkas-berkas Mahmilub (Mahkamah Militer Luar Biasa) yang pernah diadakan oleh pemerintah Indonesia. Setiap proses verbal itu tebalnya bukan main. Sekarang ini saya tidak ingat persis proses verbal yang mana, tapi di dalamnya ada serentetan lampiran yang panjang sekali. Pada kira-kira halaman 700 atau 800 saya temukan laporan otopsi itu sebagai dokumen resmi yang ditanda-tangani oleh dokter-dokter Universitas Indonesia. Mereka disuruh Suharto untuk mengadakan pemeriksaan mayat-mayat yang ditemukan di Lubang Buaya. Jadi jelas sumbernya dari berkas yang diserahkan kepada kami oleh pemerintah Indonesia. Jadi itu sama sekali bukan barang palsu, tanda tangan semuanya ada di situ, dan laporannya cukup mendetail.


T: Apakah laporan otopsi ini pernah dijadikan barang bukti di pengadilan?


J: Kalau masuk berkas Mahmilub itu berarti termasuk dokumen yang dipakai di pengadilan. Tetapi saya tidak tahu apakah dokumen ini memang pernah dibacakan di pengadilan. Tapi jelas termasuk barang-barang yang seharusnya dipikirkan oleh para hakim.


T: Siapa yang memerintahkan para dokter untuk melakukan otopsi?
J: Itu jenderal Suharto. Pemeriksaan diadakan atas perintah dari pejabat Kasad saat itu, yaitu Suharto. Dan tanda-tangan Suharto memang ada di situ.


T: Tanggal 7 Oktober 65 koran Angkatan Bersenjata bilang, “Matanya dicongkel.” Apakah memang ditemukan bukti-bukti penganiayaan sebelum mereka dibunuh? Apa ada yang dicukil matanya atau disayat-sayat badannya?


J: Sama sekali tidak ada. Itu yang menarik buat saya sewaktu membaca. Karena jelas mereka semua mati karena ditembak. Dan kalau ada luka-luka lain itu karena kena pinggiran sumur atau terbentur batu-batu di dalamnya. Jenasah itu dilemparkan ke dalam sumur yang dalamnya lebih dari 10 meter. Jadi sama sekali tidak ada tanda siksaan. Dan dokter tidak pernah menyebutkan ada bekas-bekas siksaan. Jadi matanya, oke. Kemaluannya juga utuh, malah disebut 4 disunat dan 3 nggak disunat.


T: Bagaimana pemberitaan koran dan TV pada bulan Oktober dan Nopember 1965? Misalnya Angkatan Bersenjata 5 Oktober 65 itu bilang, “Perbuatan biadab berupa penganiayaan yang dilakukan di luar batas perikemanusiaan.” Berita sejenis ini muncul setiap hari selama beberapa minggu baik di koran, radio maupun di TV.


J: Itu jelas direkayasa. Karena pimpinan tentara tahu betul bahwa tidak ada siksaan. Tapi ini salah satu strategi untuk menimbulkan suasana yang tegangnya bukan main. Ini sudah biasa. Di mana saja di dunia, kalau mau diadakan pembantaian massal, harus diciptakan suasana di mana orang merasa apa saja boleh. Karena calon korban sudah berbuat hal-hal diluar peri kemanusiaan.


T: Otopsi selesai tgl 5 Oktober sekitar jam 12 siang. Karena Suharto yang memerintahkan, tentu dia orang pertama yang diberi tahu hasil otopsi itu. Suharto tahu tidak ada bukti-bukti penyiksaan di Lubang Buaya. Mengapa Suharto membiarkan koran-koran — terutama Angkatan Bersenjata dan Berita Yudha, koran Angkatan Darat yang bisa dia kontrol — menyiarkan kabar bohong tentang penyiksaan, kekejaman, dsb?


J: Saya tidak mengatakan bahwa Suharto membiarkan. Saya yakin itu justru diperintahkan oleh Suharto bersama orang yang dekat-dekat dia seperti Ali Murtopo. Ini jelas justru untuk membantu mereka dalam hal menghancurkan PKI dan pada akhirnya mengambil kekuasaan dari tangan Bung Karno. Yang bisa menjadi penyokong Bung Karno yang paling kuat dan paling terorganisir adalah PKI. Kalau PKI sudah tidak ada, Bung Karno tidak punya kekuatan konkrit yang terorganisir yang bisa melawan tentara.


T: Apakah Suharto, sebagai Pangkopkamtib pada tahun 1965 itu, pernah menjelaskan pada masyarakat umum tentang bagaimana sebenarnya jenderal-jenderal itu dibunuh, siapa yang membunuh, dsb?


J: Saya nggak yakin Suharto saat itu sudah menjadi Pangkopkamtib. Saya lupa persis kapan dia menjadi Pangkopkamtib. Yang paling penting ketika itu adalah pidato Jenderal Nasution sewaktu anaknya yang malang itu dikuburkan. Jadi Suharto sendiri pada waktu itu tidak menonjol sebagai tukang pidato. Tetapi media massa yang dikuasai oleh Suharto dkk terus menerus mengatakan itu PKI yang menjadi dalang dari peristiwa. Suharto itu sendiri juga memberi pidato. Tapi saya merasa yang penting bukan pidato dia sendiri tapi suasana yang diciptakan media massa yang direkayasa oleh kelompok Suharto.


T: Bagaimana dengan kabar ditemukannya alat-alat pencukil mata? Misalnya dalam berita di koran Api Pancasila tgl 20 Oktober, “Alat cukil mata ditemukan di kantor PKI di desa Haurpanggang di Garut.”


J: Saya yakin ini bikin-bikinan. Masa orang menyimpan alat pencukil di Garut? Juga apa yang disebut sebagai alat pencukil, saya kurang tahu persis. Rasanya ini sebenarnya cuma satu alat yang dipakai oleh petani untuk urusan sehari-hari. Atau mungkin alat untuk buruh yang kerja di perkebunan. Jadi bukan alat khusus pencukil mata. Mungkin kalau anda sendiri lihat gambarnya bisa menjelaskan sebenarnya itu alat apa.


T: Bagaimana dengan berita tentang pengakuan dari saksi mata yang dimuat dalam Angkatan Bersenjata tgl 10 Oktober, “Korban dicungkil matanya. Ada yang dipotong alat kelaminnya dan banyak hal-hal lain yang sama sekali mengerikan dan di luar perikemanusiaan.” Apakah ‘pengakuan’ saksi mata itu benar? Atau bikinan tentara?


J: Saya kira nggak. Waktu itu ada dua saksi penting. Yang paling penting itu seorang gadis berumur 15 tahun yang jelas sudah diteror habis-habisan. Buktinya kedua wanita ini, yang pernah mengaku ikut mencukil mata, tidak pernah diadili. Jadi ini jelas bikin-bikinan.


T: Jadi mereka dituduh ikut menyiksa tetapi tidak ada bukti?


J: Sama sekali tidak ada bukti. Kalau ada bukti ini akan menjadi kasus yang bagus untuk pihak pimpinan tentara. Tapi ini cuma dibikin untuk memanaskan suasana. Setelah itu gadis ini tidak pernah kedengaran namanya lagi.


T: Apa kesimpulan studi tentang pembunuhan di Lubang Buaya ini?


J: Apa yang sebenarnya terjadi di Lubang Buaya itu lain soal. Yang penting ialah sebelum kampanye media massa, tentara atau pimpinan tentara tahu betul bahwa jenderal-jenderal itu dibunuh oleh sesama tentara. Sama sekali tidak ada siksaan, tidak ada penganiayaan dsb. Jadi mereka dengan darah dingin sekali membuat kampanye fitnah untuk kepentingan politik. Dan itu bisa dibuktikan oleh dokumen yang berasal dari tokoh itu sendiri, juga oleh dokter-dokter dari fakultas kedokteran Universitas Indonesia yang kejujurannya dalam hal ini tidak disangsikan.


T: Apakah Pak Ben mendapatkan data baru yang merubah kesimpulan atau hasil studi yang ditulis tahun 1987 ini?


J: Tidak ada.
Dalam versi sejarah dan juga film yang dibuat Orde Baru, pembunuhan para jenderal itu dilakukan oleh orang-orang PKI. Tetapi sebenarnya siapa yang membunuh para jenderal itu? “Jelas oleh kelompok tentara sendiri. Sama sekali tidak ada bukti bahwa orang sipil ikut.”


SIAPA YANG MEMBUNUH


T: Siapa yang membunuh 3 jenderal (Yani, Panjaitan dan Haryono) di rumahnya?


J: Sepengetahuan kami sampai sekarang pembunuhan yang terjadi di rumah jenderal-jenderal dilakukan oleh kesatuan dari Cakrabirawa yang dipimpin langsung oleh letnan Dul Arief yang sampai sekarang nggak ada bekasnya. Bagaimana nasibnya, ke mana larinya, dibunuh di mana, itu tidak diketahui. Jadi jelas oleh kelompok tentara sendiri. Jenderal Nasution sendiri dalam pengakuannya mengatakan memang rumahnya diserang oleh tentara.


T: Siapa yang melakukan pembunuhan 3 jenderal (Parman, Suprapto dan Sutoyo) dan letnan Tendean di Lubang Buaya?


J: Kalau yang terjadi di Lubang Buaya itu saya kira sama juga. Mungkin bukan orang-orang dari Cakra saja, tetapi juga dapat bantuan dari kelompok kecil dari AURI. Sampai sekarang tidak jelas, karena bagaimanapun waktu itu Halim dalam kekuasaan AURI. Sama sekali tidak ada bukti bahwa orang sipil ikut.


T: Jadi bunuh-membunuh itu antara tentara sendiri.


J: Oh iya. Justru itu menjadi faktor yang sangat menentukan. Karena perpecahan di kalangan tentara ini justru sesuatu yang menjadi skandal besar yang bisa menghancurkan nama ABRI di mata umum. Jadi kalau ada ABRI membunuh ABRI, ya bagaimana? Sangat diperlukan dalang yang bukan ABRI.


T: Apakah ada anggota PKI, atau ormasnya seperti Pemuda Rakyat dan Gerwani, yang ikut melakukan pembunuhan di Lubang Buaya?


J: Tidak ada bukti.


T: Apakah semua pembunuh diajukan ke pengadilan?


J: Ya, satu dua. Malah masih ada satu yang dulu sersan dari Cakra. Kalau nggak salah dia sampai sekarang masih nongkrong di — yang dalam bahasa Inggris dinamakan — “Death Row.” Dia divonis hukum mati 25 tahun yang lalu. Tapi sampai sekarang belum dijalankan. Tapi sebagian besar dari kelompok Cakra itu hilang tanpa bekas. Kita nggak tahu bagaimana nasibnya mereka. Untung sendiri ditangkap dan diadili. Tapi dia sendiri tidak langsung memimpin kesatuan yang menyerbu rumah jenderal dan sebagainya.


T: Apakah kesaksian yang pernah diadili itu konsiten dengan otopsi?


J: Saya tidak ingat lagi. Kami tidak pernah menerima berkas proses verbal dari sersan-sersan Cakra. Yang kami terima cuma berkas-berkas dari perkaranya orang-orang yang dianggap tokoh dalam G-30-S dan beberapa orang sipil seperti Subandrio, dll. Kalau yang dianggap orang kecil, seperti sersan ini dan sersan itu, tidak pernah disampaikan.


T: Apakah ada orang yang mempermasalahkan tentang hilangnya para pelaku pembunuhan di Lubang Buaya yang tanpa bekas ini?


J: Tidak pernah. Yang lebih mengherankan adalah bahwa kelompok pimpinan Kodam Diponegoro yang mendukung G-30-S, Kolonel Suherman asisten satu, Kolonel Maryono asisten tiga, Letkol Usman asisten empat, dsb, untuk selama kira-kira 48 jam, menguasai hampir seluruh Jawa Tengah, kemudian mereka juga hilang. Tidak pernah diantara mereka ada yang diadili, diajukan ke pengadilan, dsb. Mereka hilang tanpa bekas. Itu tidak pernah diisukan. Malah kalau membaca laporan dari Buku Putih apa yang terjadi di Jawa Tengah sama sekali tidak menjadi masalah. Jadi semua perhatian dengan sengaja dipusatkan pada apa yang terjadi di Jakarta.


T: Sebenarnya yang tahu soal orang hilang ini siapa?


J: Ya, harus tanya pada pemerintah di Indonesia. Suherman di mana? Maryono, Usman di mana? Dsb. Banyak sekali tokoh-tokoh dari G-30-S hilang tanpa bekas. Yang tahu bagaimana nasibnya, ya tentara sendiri

.
T: Kemudian bagaimana Pak Ben menjelaskan kehadiran anggota Pemuda Rakyat dan Gerwani di Halim?


J: Ini dalam rangka konfrontasi dengan Malaysia. Memang ada kebijaksanaan dari pemerintah. Selain dari anggota ABRI ada juga orang sipil yang dilatih sebagian sebagai sukarelawan untuk dikirim ke perbatasan. Tapi ada sebagian juga yang dilatih untuk membantu seandainya Ingris melakukan serangan udara ke Jakarta sebagai pembalasan terhadap aksi dari pihak Indonesia. Dan pada umumnya Angkatan Darat tidak begitu antusias.
Tapi AURI di bawah pimpinan Omar Dhani yang dekat dengan Bung Karno, rupanya mulai suatu sistim latihan itu di beberapa pangkalan udara. Dan mereka minta dari parpol-parpol dan ormas-ormas supaya anggota-anggota mereka dilatih. Dan yang paling antusias pada waktu itu, ya PKI. Jadi kalau ada Gerwani dan Pemuda Rakyat di Halim itu karena mereka dikirim oleh pimpinan PKI di Jakarta untuk latihan. Dan karena tugasnya setengah militer, mereka ini harus yang muda-muda. Mereka bisa dilatih oleh petugas-petugas dari AURI, dalam hal ini ya mayor Suyono. Bukan PKI saja, juga ada PNI dan NU dilatih.


T: Apa yang dilatih di Halim itu tidak hanya PKI tetapi juga PNI dan NU.


J: NU saya nggak jelas apakah dilatihnya di Halim. Tetapi di pangkalan lain jelas ada.


T: Kalau begitu keberadaan mereka secara kebetulan saja?


J: Tidak kebetulan. Memang ada program dari pemerintah. Tetapi tidak dilatih untuk membunuh jenderal. Mereka dilatih untuk mempertahankan pangkalan kalau diserang oleh Inggris.


T: Bagaimana Pak Ben menjelaskan kehadiran DN Aidit, ketua PKI, di Halim? Apa yang dia lakukan selama di Halim pada tanggal 1 Oktober itu?


J: Sampai sekarang ini menjadi teka-teki yang besar. Karena sepengetahuan kami tidak ada bukti bahwa DN Aidit berbuat apa-apa. Justru ini yang aneh. Seolah-olah dia dijemput oleh kelompok tertentu, dibawa ke Halim, terus ditaroh di salah satu rumah di situ sepanjang hari. Pada akhirnya dia dapat pesan, mungkin ada kurir atau apa dari Bung Karno, dia disuruh pergi ke Jawa Tengah untuk menenangkan situasi. Jadi tidak ada bukti kalau dia ketemu dengan pimpinan G-30-S. Tidak ada bukti bahwa dia memberi instruksi apa-apa. Malah ada kemungkinan juga dia mau disembunyikan di Halim di bawah perlindungan AURI. Seandainya ada usaha menculik dia dari rumahnya atau dari kantor PKI


T: Tadi dikatakan bahwa dia disuruh pergi ke Jawa Tengah untuk menenangkan situasi. Jadi ke Jawa Tengahnya bukan inisiatif dia sendiri?


J: Tidak ada bukti. Yang jelas setelah Bung Karno melihat situasi pada malam hari tanggal 1 Oktober, dia tahu bahwa situasi itu sangat berbahaya. BK juga tahu situasi di Jawa Tengah. Dia merasa mungkin ini bisa menjadi permulaan dari suatu perang saudara. Dia juga takut jangan-jangan PKI merasa harus berbuat sesuatu, padahal mereka tidak bisa berbuat banyak. Mereka kan tidak punya senjata. Saya kira Aidit juga takut jangan-jangan ada kelompok di daerah yang tidak mengerti keadaan lantas membuat sesuatu yang bisa dipakai sebagai provokasi oleh lawan.
Kita harus sadar bahwa dalam ingatan orang-orang PKI, Peristiwa Madiun tahun 1948 itu menjadi trauma yang besar. Waktu Madiun, orang-orang tingkat lokal membuat sesuatu yang kemudian menyeret pimpinan partai dan akhirnya menghancurkan partai pada waktu itu. Jadi seperti, ya sudah ada ide dalam partai, itu jangan terjadi lagi, jangan sampai bisa diprovokasi sekali lagi.
Pada bulan Januari 1966 Ben Anderson dan Ruth McVey, waktu itu masih mahasiswa S-3, menulis makalah yang kemudian dikenal luas dengan nama “Cornell Paper.” Dalam makalah itu Pak Ben menyatakan, “Tokoh-tokoh utama dari gerakan ini baik di Jawa Tengah maupun Jakarta sendiri adalah perwira-perwira dari kodam Diponegoro. Mereka semuanya bekas bawahan dari Suharto sendiri.” Bagaimana peranan PKI dan Bung Karno? “Sampai sekarang sama sekali tidak ada bukti bahwa Bung Karno ada di belakangnya. Masalah PKI lebih ruwet. Setelah tahun 66 ada beberapa data yang masuk seolah-olah ada orang PKI yang terseret. Saya tidak bisa mengatakan bahwa sama sekali PKI tidak ada sangkut pautnya. Tapi saya masih tetap berpendapat mereka bukan pencipta utama G-30-S.”


CORNELL PAPER


T: Tiga bulan setelah pembunuhan para jenderal, Pak Ben dan Ibu Ruth menyusun hasil studi berjudul “A preliminary analysis of the October 1, 1965, coup in Indonesia.” Studi itu selesai ditulis tgl 10 Januari 1966 dan semula cuma diedarkan di kalangan terbatas. Baru diterbitkan untuk umum, tanpa perubahan, pada tahun 1971. Studi itu kemudian dikenal dengan nama “Cornell Paper.” Apa pokok pikiran dalam Cornell Paper? Dan apa bukti-bukti utama yang menunjang pokok pikiran itu?


J: Pada waktu itu kami ingin mengecek sampai kemana versi resmi dari apa yang terjadi itu masuk di akal. Versi resmi bunyinya, “Ini suatu komplotan jahat, yang didalangi oleh PKI.” Pada waktu itu kami ingin mengecek apa ini cocok dengan informasi dan data-data yang masuk. Laporan itu selesai ditulis tanggal 10 Januari. Tapi saya kira bahan-bahan itu makan waktu tiga minggu untuk penulisan. Jadi bahan-bahan yang masuk itu hanya sampai pertengahan Desember. Nah, kebetulan kira-kira tiga minggu setelah peristiwa, situasi belum 100% bisa dikontrol oleh Suharto. Jadi masih banyak koran beredar di Jawa Timur, Jawa Tengah, Medan, dsb. Dan kebetulan Cornell punya koleksi koran Indonesia yang paling lengkap di dunia. Pada waktu itu memang luar biasa kami dapat segala macam koran dari Surabaya, Semarang, Yogya, Solo, dll. Dan ternyata dari laporan-laporan itu banyak yang tidak cocok dengan versi resmi.
Selain itu kami punya arsip tentang tentara. Dari situ kami bisa juga sedikit banyak mengecek siapa sebenarnya orang-orang seperti Suherman, Usman, dsb. Dari situ kami bisa mengetahui bahwa umpamanya Suherman baru kembali dari latihan di Amerika. Dia dilatih sebagai orang inteljen oleh AS. Untuk kita tidak masuk di akal bahwa ada seorang PKI bisa menjadi lulusan latihan AS dan dikasih jabatan sebagai tokoh intel di Jawa Tengah. Selain dari itu juga ada beberapa informasi dalam bentuk surat dari teman-teman yang kebetulan jalan-jalan di Jawa Tengah dan Jawa Timur pada waktu itu. Mereka menulis apa yang mereka lihat dengan mata sendiri.


T: Cornell Paper dibuka dengan kalimat ini, “The weight of the evidence so far assembled and the (admittedly always fragile) logic of probabilities indicate that the coup of October 1, 1965, was neither the work of PKI nor of Sukarno himself.” Apa yang Pak Ben maksud dengan “logic of probabilities” itu?


J: Ini kesimpulan dari dua sudut logika. Bung Karno sama sekali tidak dapat keuntungan dari peristiwa berdarah ini. Justru beliau bisa berdiri di atas segala kelompok di Indonesia karena bisa mengimbangi kelompok ini dengan kelompok itu. Kalau politik sudah bergeser dari politik sipil, yaitu omong-omong, organisasi, dsb, ke lapangan kekerasan, BK nggak bisa apa-apa. Jadi jelas G-30-S membahayakan dia, bukan membantu dia.
Dari sudut PKI, kami merasa selain dari faktor trauma-48, PKI pada waktu tahun 1965 cukup sukses dalam politiknya. Yaitu politik damai. Di mana dia semakin lama semakin berpengaruh. Justru karena mereka tidak mengambil jalan seperti Ketua Mao dengan perang gerilya, dsb. Mereka pakai strategi sipil. Justru mereka akan jadi susah kalau mulai konflik bersenjata. Karena mereka tidak punya senjata, tidak punya kesatuan-kesatuan yang bersenjata. Jadi logika dua kelompok ini, BK dan PKI, menuntut supaya politik tetap politik normal bukan politik bedil.
Logika ini diperkuat dengan hal-hal yang kongkrit. Banyak hal yang aneh dalam G-30-S. Pertama, pengumuman-pengumuman dari G-30-S itu nggak mungkin disusun oleh tokoh-tokoh PKI yang cukup berpengalaman dalam bidang politik. Saya kasih dua contoh. Pertama, yang dikatakan Dewan Revolusi yang diumumkan G-30-S, itu suatu Dewan yang sama sekali tidak masuk di akal. Karena banyak tokoh-tokoh yang penting, seperti Ali Sastroamidjojo, yang tidak masuk. Tapi banyak tokoh-tokoh yang hampir tidak dikenal namanya justru masuk. Malahan banyak orang kananpun masuk, itu umpamanya Amir Machmud. Jadi ini bukan suatu Dewan Revolusi yang meyakinkan. Jadi rasanya kacau.
Yang kedua, yang lebih meyakinkan lagi, adalah pengumuman dari Untung kepada sesama tentara, bahwa mulai saat itu tidak akan ada lagi pangkat dalam tentara yang lebih tinggi dari pangkatnya Letkol Untung sendiri. Nah itu jelas membuat setiap kolonel, brigjen, mayjen, letjen, dsb, jengkelnya bukan main. Dan ini suatu move yang membuat semua pimpinan tentara akan anti dengan gerakan ini. Nggak mungkin ada orang yang punya otak politik akan membiarkan suatu pengumuman seperti itu. Itu jelas suatu pengumuman yang keluar dari hati nurani Letkol Untung sendiri. Karena dia jengkel dengan atasannya. Atau bisa juga itu provokasi yang diatur oleh dalang sebenarnya dari peristiwa ini. Yang memakai Untung, yang jelas bukan orang pinter, sebagai pionnya.


T: Setelah 30 tahun, apakah Pak Ben tetap berpegang pada pendapat itu?


J : Kalau Sukarno jelas. Sampai sekarang sama sekali tidak ada bukti bahwa Bung Karno ada di belakangnya. Masalah PKI lebih ruwet. Setelah tahun 66 ada beberapa data yang masuk seolah-olah ada orang PKI yang terseret.
Saya sendiri pernah ikut persidangan Mahmilub Sudisman pada tahun 1967, dan mendengar pidato uraian tanggung jawabnya. Dalam pengadilan itu yang dinamakan Ketua Biro Khusus yaitu si Kamaruzaman, atau Syam, nongol sebagai saksi. Di sana cukup jelas bahwa Syam ini adalah orang yang dikenal betul oleh Sudisman. Bagaimanapun Syam ada hubungan langsung dengan pimpinan PKI. Jadi apa ada sebagian dari orang-orang PKI ikut-ikutan, apa ada sebagian dari PKI yang dibodohin oleh kelompok ini-itu, masih tidak jelas. Jadi saya tidak bisa mengatakan bahwa sama sekali PKI tidak ada sangkut pautnya. Tapi saya masih tetap berpendapat mereka bukan pencipta utama G-30-S.


T: Kalimat terakhir dalam alinea pertama “Cornell Paper” itu, “The actual originators of the coup are to be found not in Djakarta, but in Central Java, among middle-level Army officers in Semarang, at the headquarters of the Seventh (Diponegoro) Territorial Division.” Apa bukti-bukti utama dari pernyataan ini?


J: G-30-S hanya sukses bisa menguasai daerah di Jawa Tengah. Dan tokoh-tokoh utama dari gerakan ini baik di Jawa Tengah maupun Jakarta sendiri adalah perwira-perwira dari kodam Diponegoro, kecuali Supardjo. Mereka semuanya bekas bawahan dari Suharto sendiri, termasuk Supardjo. Yang paling menonjol tentu pada waktu itu adalah fakta bahwa Untung dan Latif kedua-duanya dekat sekali dengan Suharto. Suharto sendiri pada tahun 64 pergi jauh-jauh ke salah satu desa di Jawa Tengah untuk ikut menghadiri perkawinan bawahannya yang tercinta, yaitu Untung. Jadi itu yang pertama.
Kedua, fakta bahwa apa yang terjadi di Jawa Tengah sampai sekarang 100% ditutupi oleh versi resmi. Ini aneh. Karena nggak ada pengadilan, nggak ada cerita apa yang terjadi di Semarang. Terus kesatuan-kesatuan utama yang ikut gerakan di Jakarta itu sebagian besar juga dari Diponegoro. Itu yang penting.


T: Apa motivasi perwira-perwira Diponegoro itu?


J: Ini jelas tentara yang belum berbintang. Otaknya seolah-olah pangkat mayor, letkol, kolonel, dibantu oleh kapten, letnan, sersan, dsb. Kalau kita membaca pengumuman G-30-S, seolah-olah masalahnya itu masalah intern. Mereka menuduh jenderal-jenderal ikut serta dengan CIA dalam rangka mendongkel Bung Karno. Menuduh jenderal-jenderal orang yang hidup mewah, orang yang suka main perempuan, yang tidak menghiraukan nasib dari bawahannya, dsb.
Jadi terasa sekali ada semacam konflik antara tentara yang bawahan, yang pada umumnya miskin-miskin, dan tokoh jenderal-jenderal yang berduit. Pada waktu itu di Jakarta cuma ada satu mobil Lincoln Continental yang putih. Dan pemiliknya siapa? Jenderal Yani, kan? Padahal Indonesia saat itu miskinnya bukan main. Jadi sangat menyolok. Jadi kalau motivasinya dikatakan sebagai kecemburuan sosial, juga bisa.


T: Apa ada hubungan antara motivasi itu dengan lingkungan para perwira Diponegoro pendukung G-30-S itu?


J: Sebagian karena Jawa Tengah terkenal sebagai daerah yang paling miskin dibandingkan dengan Jawa Barat dan Timur. Juga kultur di Jawa Tengah di mana patriotisme kejawa-jawaan itu paling kuat. Dari dulu ada persaingan antara Diponegoro dan Siliwangi. Perwira Siliwangi dianggap orang yang statusnya lebih tinggi, biasa pakai bahasa Belanda diantara mereka sendiri dan biasa kebarat-baratan, dan paling dekat dengan Amerika.
Perwira Jawa Tengah sebagian besar berasal dari Peta, bikinan Jaman Jepang. Waktu revolusi mereka merasa diri sebagai orang Jogya lah. Orang yang mempertahankan nilai-nilai dari revolusi 45, patriotisme Jawa, dsb. Pokoknya kalau jenderal-jenderal Bandung omong, mereka tidak pernah pakai –ken, –ken. Tapi ini bukan masalah suku. Karena tokoh utama dari semuanya itu orang Jawa dan sasaran utamanya juga orang Jawa.


T: Pada minggu pertama Oktober 1965, dari 5 pucuk pimpinan PKI, 3 orang ada di Jawa Tengah (Aidit, Lukman dan Sakirman). Nyoto sedang berada di Sumatra Utara sedangkan Sudisman di Jakarta. Kemudian Lukman dan Nyoto menghadiri Rapat Kabinet di Bogor tgl 6 Oktober. Apa yang dilakukan para pimpinan PKI selama berada di Jawa Tengah setelah 1 Oktober?


J: Dari informasi yang masuk, dari laporan sopirnya Lukman, dsb, ya mereka putar-putar. Aidit dan Lukman menghubungi cabang dan ranting-ranting PKI, memberi tahu apa yang sedang terjadi, dan diminta untuk waspada dan jangan sampai bisa diprovokasi. Dalam hal ini kita belum tahu banyak. Karena sampai sekarang kebanyakan orang PKI yang masih hidup, di dalam maupun di luar negeri, belum sempat menulis secara jujur, terang-terangan, tentang kehidupan intern partai pada saat itu.
Munculnya tokoh Syam, Ketua Biro Khusus PKI, membuat pendapat Ben Anderson bergeser. Tentang tokoh itu, “Syam ini orangnya cukup misterius.” Tentang Biro Khusus, “Banyak hal yang masih belum jelas.” Tentang peranan RPKAD, “Pembunuhan juga baru mulai di Jawa Timur setelah RPKAD berpindah dari Jawa Tengah ke Jawa Timur. Di Bali, pembunuhan massal baru mulai setelah RPKAD pindah dari Jawa Timur ke Bali.”


BIRO KHUSUS


T: Tadi Pak Ben bilang, munculnya tokoh Syam itu membuat pendapat Pak Ben bergeser?


J: Ya. Maka itu kami menamakan “Laporan Sementara.” Dan sampai sekarang status sebagai laporan sementara tetap dipertahankan. Karena bagaimanapun sebagian besar tokoh-tokoh yang paling penting sudah dibunuh, atau sudah hilang. Dan ada juga yang berkaitan langsung tetapi belum mau ngomong. Jadi harus nunggu.
T: Ini bukan bahan dari Cornell Paper, tapi dari Buku Putih yang diterbitkan Sekretariat Negara tahun 1994. Dalam Buku Putih soal Biro Khusus PKI dibahas panjang lebar. Menurut Pak Ben sendiri seberapa besar peranan Biro Khusus PKI ini dalam G-30-S?
J: Dalam hal Biro Khusus ini ada beberapa sisi yang sampai sekarang sulit dimengerti. Pertama, pada umumnya partai komunis, di manapun juga di dunia, punya format yang sama. Susunan organisasinya sangat standard. Dan sampai sekarang belum ditemukan partai komunis lain yang pernah menciptakan semacam Biro Khusus. Ini tidak mustahil. Tetapi cukup aneh.
Kedua, istilah atau bahasa yang dipakai oleh orang-orang yang dianggap tokoh Biro Khusus ada juga yang mencurigakan. Umpamanya mereka pakai istilah “pembina.” Jadi, untuk menggambarkan aktivitas mereka, ada para pembina yang mengadakan pembinaan-pembinaan. Sepanjang pengetahuan saya, ini bukan bahasa marxist dan bukan bahasa komunis. Tapi dari dulu ini bahasanya tentara. Rada aneh kalau waktu itu PKI berlawanan dengan tentara, kok yang dianggap organisasi rahasia PKI itu justru pakai istilah teknis dari tentara?
Yang ketiga, sampai sekarang identitas dan nasibnya Kamaruzaman alias Syam itu masih menjadi teka-teki. Setelah memeriksa beberapa dokumen di Amerika, di Indonesia dan di Belanda, ternyata si Syam ini pernah menjadi anggota PSI. Dalam majalah resmi dari PSI namanya pernah disebut pada tahun 1951 sebagai ketua ranting. Kalau tidak salah di Rangkasbitung, Banten. Selain itu ditemukan juga dokumen dalam arsip Belanda di mana Kamaruzaman ini pada waktu revolusi pernah diangkat sebagai orang intelnya Recomba Jawa Barat. Recomba itu pemerintah federal buatan Belanda. Itu bisa juga. Karena pada waktu itu ada juga patriot-patriot yang pura-pura jadi pegawai Belanda untuk mengetahui rahasianya Belanda. Tapi toh rada aneh. Terus di koran-koran Indonesia ada informasi bahwa pada akhir tahun 50-an Kamaruzaman nongol sebagai informan dari komandan KMK (Komando Militer Kota) Jakarta.
Jadi Syam ini orangnya cukup misterius. Jelas dia dikenal baik oleh pimpinan PKI. Tetapi dia juga pegang peranan di Recomba, di PSI, di tentara, dsb. Sampai sekarang serba misterius. Di mana kesetiaannya? Buat saya tidak jelas. Walaupun pemerintah mengumumkan bahwa Syam sudah dieksekusi, itu masih disangsikan kebenarannya. Mungkin dia cuma disimpen saja.
Yang terakhir, ini kesan saya waktu mengikuti pengadilan Sudisman. Di sana Syam diberi kesempatan untuk omong panjang lebar. Saya bisa membandingkan kesaksiannya dengan kesaksian Sudisman. Itu sangat berbeda. Kesaksian Sudisman itu mengesankan, jelas, mendalam dan bahasanya teratur. Sedangkan kesaksian Syam itu bukan main kacau-balaunya. Dia banyak memakai bahasa-bahasa dari jaman revolusi yang sudah tidak berlaku lagi. Malahan seolah-olah orangnya itu agak sinting. Jadi sulit masuk di akal kalau orang seperti ini menjadi kepala biro yang sangat rahasia dan penting. Jadi banyak hal yang masih belum jelas.


T: Pak Ben mengikuti sendiri pengadilan Sudisman. Dia satu-satunya pucuk pimpinan PKI yang diadili. Apa kesimpulan Pak Ben dari pengadilan Sudisman itu?


J: Dari kesaksian Sudisman saya dapat kesan bahwa dia merasa diri dalam keadaan di mana partai yang ikut dia pimpin itu dihancurkan secara mengerikan. Ratusan ribu yang mati. Dan dia sebagai seorang pemimpin dan sebagai seorang Jawa merasa bertanggung-jawab. Bagaimanapun, kalau pimpinannya baik dan beres seharusnya hal seperti itu tidak terjadi. Karena itu dia menamakan pembelaannya itu “Uraian Tanggung Jawab.”
Dia tidak mau debat tentang soal ini-itu. Dia cuma bilang, “Bagaimanapun juga, sebagai pimpinan tertinggi yang masih hidup, saya memakai kesempatan ini untuk meminta maaf atas apa yang terjadi.” Sudisman tidak pernah bilang bahwa dia ikut merencanakan G-30-S. Dia cuma bilang bahwa rupanya ada unsur-unsur PKI yang terseret. Dia tidak membicarakan soal Biro Khusus. Tidak membenarkan dan juga tidak membantah adanya. Waktu Syam memberi kesaksian, Sudisman tidak mau melihat mukanya dan tidak mau menjawabnya. Yang jelas, untuk sebagian besar dari saksi-saksi waktu itu informasi tentang adanya Biro Khusus itu sesuatu yang mengejutkan sekali. Jelas mereka


PERANAN RPKAD


T: Ini catatan dalam Cornell Paper. Pasukan RPKAD sampai di Semarang tgl 19 Oktober. Bentrokan pertama dengan ormas PKI terjadi di Boyolali tgl 21 Oktober. Selama 3 minggu, tgl 1 sampai 21 Oktober, tidak ada bentrokan berdarah. Walaupun pemberitaan di koran, TV dan radio tentang Lubang Buaya sangat memanaskan suasana, menyebarkan ketakutan, dst. Bagaimana Pak Ben menjelaskan 3 minggu tanpa bentrokan ini?


J: Saya kira sebagian karena kekuatan sosial pada tingkat sipil cukup seimbang. Jadi orang merasa bahwa organisasi PKI dan keluarga besar PNI itu kira-kira seimbang. Di Jawa Tengah NU itu tidak begitu berpengaruh. Pertama orang merasa tidak ada kelompok yang dominan. Dan walaupun suasana tegangnya bukan main, saya nggak yakin bahwa orang Jawa Tengah, kalau tidak ada orang yang mengipasi mereka, mau cepat-cepat membantai tetangganya.
Kita harus ingat di desa-desa, di kota-kota, orang PKI itu bukannya bergerak di bawah tanah. Mereka itu tetangga, yang saban hari ketemu, masih famili, dsb. Banyak anggota PNI yang punya saudara PKI, dsb. Kalau tidak dibikin suasana yang luar biasa tegangnya tidak akan terjadi apa-apa, dalam arti pembantaian.
Justru pentingnya kedatangan RPKAD adalah orang-orang anti PKI merasa bahwa angin sudah berada di pihak mereka. Mereka yang mau netral dapat petunjuk dari RPKAD kalau membuktikan kamu bukan orang PKI maka kamu harus membunuh PKI. Ini khususnya ditujukan kepada pemuda-pemuda, pemuda Islam, pemuda Banteng, pemuda Kristen, Katolik, dsb. Jadi kalau tidak ada pembantaian sebelum RPKAD datang. Itu justru menunjukan apa yang terjadi tidak spontan. PKI sendiri juga takut.


T: Kesatuan-kesatuan yang mendukung G-30-S itu dari Diponegoro, lalu mereka menguasai Jawa Tengah. Lalu RPKAD datang. Bukankah kedua kekuatan itu seimbang?


J: Itu benar pada hari-hari pertama. Tetapi jangan lupa bahwa Suherman, Maryono, Usman, dkk, itu hanya berkuasa selama kira-kira 48 jam. Setelah itu Pangdamnya, Suryosumpeno, sempat ambil kembali posisi sebagai panglima. Lalu orang-orang ini hilang entah ke mana. Bahwa ada kesatuan-kesatuan di Jawa Tengah yang bersimpati pada PKI, itu mungkin sekali. Karena sebagai kelompok teritorial bagaimanapun mereka berada di tengah masyarakat Jawa Tengah. Tentara ini sedikit-banyak akan membantu PKI. Tapi kira-kira mulai tanggal 3 Oktober, pimpinan Diponegoro tidak lagi di tangan perwira-perwira yang pro G-30-S. Dan kesatuan-kesatuan yang dicurigai itu langsung ditarik ke daerah lain.
Tapi, ini dapat dibuktikan, pembunuhan juga baru mulai di Jawa Timur setelah RPKAD berpindah dari Jawa Tengah ke Jawa Timur. Di Bali, pembunuhan massal baru mulai setelah RPKAD pindah dari Jawa Timur ke Bali. Kalau membandingkan tiga propinsi ini, tiga minggu yang tenang di Jawa Tengah itu aneh. Lebih aneh lagi karena ada enam minggu yang tenang di Jawa Timur. Dan malahan ada dua bulan yang tanpa pembunuhan di pulau Bali.
Bagaimana pembunuhan massal itu dilaksanakan? “Faktor yang pertama adalah policy atau kebijaksanaan dari pimpinan tentara di Jakarta yang diwujudkan dengan pengiriman RPKAD ke Jawa Tengah, Jawa Timur dan Bali. Senjata, latihan, perlindungan, kendaraan, dsb, dikasih kepada kelompok-kelompok pemuda yang mereka hubungi.” Mengapa orang mau disuruh membunuh? Mau dipakai sebagai alat? “Itu mungkin menunjukan gejala yang lebih mendasar. Buat saya faktor yang utama adalah keadaan ekonomi. Mulai kira-kira tahun 1961-62 inflasi di Indonesia melejit secara mengerikan.” Tentang konflik antara kelompok agama dengan PKI, “Ini timbul sebagai akibat Undang-Undang Pokok Agraria dan Undang-Undang Pokok Bagi Hasil.”


PEMBUNUHAN MASSAL


T: Sekarang (1996) hampir semua koran di dunia melaporkan orang PKI yang dibunuh pada tahun 1965 itu jumlahnya sekitar 500 ribu. Misalnya dalam editorial New York Times (1 Ags), The Economist (3 Ags) dan editorial Washington Post (20 Ags). Dalam pemberitaan media di Indonesia, jumlah korban ini jarang diungkapkan. Dan siapa yang jadi korban itu juga tidak dijelaskan. Tahun 1985, dalam majalah Indonesia nomor 40, Pak Ben bilang, “Probably between 500.000 and 1.000.000 Indonesians died at the hands of other Indonesians.”


J: Pertama harus dikatakan bahwa tidak ada orang yang tahu persis berapa jumlahnya orang yang dibunuh pada waktu itu. Angka 500 ribu itu diambil dari pernyataan Adam Malik dan pernah juga dari Soedomo. Tapi apa mereka sendiri tahu? Itu nggak jelas. Itu cuma perkiraan.


T: Menurut Pak Ben apa sebab terjadinya pembunuhan massal selama akhir 1965 itu?


J: Kalau sebab-sebab ada dua faktor. Faktor yang pertama adalah policy atau kebijaksanaan dari pimpinan tentara di Jakarta yang diwujudkan dengan pengiriman RPKAD ke Jawa Tengah, Jawa Timur dan Bali. Mereka ingin supaya PKI dihancurkan dan mereka ingin juga bahwa ini tidak hanya dikerjakan oleh tentara tapi juga oleh kelompok-kelompok yang mau dijadikan sekutu untuk membangun apa yang belakangan dinamakan Orde Baru. Jadi mereka menyertakan warga Banteng, NU, Katolik, Protestan, dsb. Karena itu senjata, latihan, perlindungan, kendaraan, dsb, dikasih kepada kelompok-kelompok pemuda yang mereka hubungi. Jadi kalau policy pimpinan tentara ini tidak ada, kemungkinan terjadi pembunuhan massal itu saya kira tidak besar.
Tetapi mengapa orang mau disuruh membunuh? Mau dipakai sebagai alat? Itu mungkin menunjukan gejala yang lebih mendasar. Buat saya faktor yang utama adalah keadaan ekonomi. Mulai kira-kira tahun 1961-62 inflasi di Indonesia melejit secara mengerikan. Saat itu saya sendiri ada di Indonesia. Saya lihat saban minggu itu harga barang bisa berlipat ganda. Duit tidak ada arti sama sekali. Khususnya bagi orang-orang gajian itu menimbulkan suasana yang panik. Kalau pejabat gajinya tidak berarti lagi, mereka cepat-cepat lari ke dunia korupsi, catut, dsb. Orang melarikan duitnya untuk beli tanah. Karena tanah dianggap sesuatu yang bisa mempertahankan harganya. Keadaan ekonomi waktu itu menimbulkan suatu kegelisahan di seluruh Indonesia. Orang merasa masa depannya sangat gelap, tidak normal dan serba tak tentu. Ini yang penting.
Terus, ada kemiskinan yang luar biasa. Saya ingat waktu itu jalan-jalan di Yogya-Solo, banyak orang yang geleparan di pinggir jalan. Orang yang mati karena busung lapar. Bung Karno sendiri tidak malu untuk bikin propaganda supaya orang makan tikus sawah. Dan dia sendiri mengaku pernah makan tikus sawah. Saya sendiri nggak percaya. Tapi itu penting.
Ketiga, konflik antara kelompok agama dengan PKI. Ini timbul sebagai akibat Undang-Undang Pokok Agraria (UUPA) dan Undang-Undang Pokok Bagi Hasil (UUPBH). PKI memperjuangkan Land Reform justru ketika tanah menjadi sangat penting, beras menjadi sangat penting, pembagian hasil menjadi sumber konflik yang luar biasa. Apalagi dalam hal Land Reform salah satu pengecualian yang penting adalah tanah-tanah yang menjadi milik dari lembaga agama — umpamanya mesjid, surau, gereja dsb — tidak boleh diganggu gugat. Dalam suasana seperti itu orang-orang yang punya tanah lebih, supaya bisa tetap pegang tanahnya, lebih sering justru menyerahkannya kepada wakaf, kalau Islam. Dan lembaga yang seperti itu juga untuk yang Kristen. Di mana dia bisa ikut sebagai pimpinan.
Jadi seperti dilaporkan oleh Lance Castles dalam artikelnya dalam majalah Indonesia pada tahun 1966 itu, kita ambil contoh pesantren Gontor. Dalam satu tahun tanah yang dimiliki oleh Gontor ini bisa bertambah sepuluh kali. Jadi dengan mendadak lembaga agama menjadi tuan tanah yang terbesar. Kalau orang-orang kiri, orang-orang yang pro-Land Reform, mau ribut soal tanah ini, mereka langsung berhadapan bukan dengan individu tuan tanah tapi langsung menghadapi lembaga agama. Banyak orang tergugah untuk membela. Mereka bukannya mau melindungi tuan tanah tapi bagaimanapun mereka pasti mau melindungi atau membela lembaga agama mereka.


T: Pembunuhan massal terjadi di desa-desa Jateng, Jatim dan Bali. Di Jateng yang dominan itu Islam abangan. Di Jatim Islam santri, dan di Bali itu semuanya Hindhu. Tapi di Jawa Barat, tidak ada pembunuhan besar-besaran. Padahal Jabar adalah daerah yang Islamnya paling taat, daerahnya Masyumi. Pemenang Pemilu-55 dan Pemilu Daerah-57 di Jabar adalah Masyumi. Bagaimana Pak Ben memahami hubungan antara pembunuhan massal ini dengan agama penduduk setempat?


J: Isu agama ini gampang dipakai selama PKI dapat dianggap sebagai kelompok anti agama. Dan harus diingat bahwa paman Karl Marx pernah mengatakan bahwa agama itu candu supaya masyarakat tidak menghadapi realitas siapa yang menindas. Ini sulit dicabut oleh PKI, walaupun mereka berusaha supaya tidak confrontational terhadap agama. Tapi isu agama ini gampang dipakai. Apalagi di negara komunis pimpinan agama diusir, Uni Soviet itu atheis, dsb. Jadi bagaimanapun isu agama ini salah satu alat pemukul di kalangan lawan PKI.
Tetapi waktu mengikuti pengadilan Sudisman, saya lihat banyak saksi-saksi dari pihak PKI. Dan hanya sedikit — Sudisman sendiri, mungkin satu-dua lagi — yang tidak mau ambil sumpah secara agama, menggunakan Al Qur’an, Al Kitab, dsb. Jadi saya yakin sebagian besar dari orang-orang PKI itu sebenarnya juga orang beragama. Itu faktor penting.
Soal perbedaan antara daerah-daerah memang menarik. Karena pembunuhan ini tidak terjadi di mana-mana secara merata di seluruh Indonesia. Di sini kami bisa melihat betapa menentukan faktor pimpinan tentara lokal. Ambil contoh umpamanya Jabar. Pada tahun 68 saya sempat mewawancara jenderal Ibrahim Adjie yang pada waktu peristiwa dia menjadi Pangdam Siliwangi. Pada waktu itu dia dianggap sebagai saingannya Suharto, lalu dibuang ke London jadi Dubes di sana. Saya ngomong lama sama beliau.
Saya tanya, kenapa kok tidak ada pembunuhan besar-besaran di Jawa Barat? Sebenarnya memang ada, umpamanya di Indramayu, tetapi tidak meluas. Dia bilang, “Itu sebabnya karena saya tidak ingin ada pembantaian di Jawa Barat. Karena merasa bagaimanapun ini sebagian besar orang biasa, orang-orang kecil. Akan mengerikan kalau mereka itu dibunuh. Saya sudah kasih perintah kepada semua kesatuan di bawah saya, orang ini ditangkap, diamankan. Tapi jangan sampai ada macem-macem.” Ternyata kewibawaan si Adjie yang terkenal jenderal kanan, yang dekat dengan Amerika, itu berlaku penuh.
Sebaliknya di Jawa Timur pimpinan tentara pada waktu itu lemah. Saya lupa nama panglimanya. Mungkin Sunaryadi? Tapi jelas kolonel-kolonel, komandan Korem merasa bisa bergerak dangan sendirinya. Umpamanya Danrem di Kediri yang masih famili dengan salah satu jenderal yang dibunuh di Jakarta, itu mengambil inisiatif sendiri. Sebagian timbul karena perpecahan. Kalau RPKAD sudah masuk orang merasa bahwa untuk selamat mereka harus berbuat sesuatu. Di Bali juga begitu. Ini menarik dan penting. Karena di Jawa Barat, di mana RPKAD tidak pernah putar-putar, justru tidak terjadi pembantaian.
T: Mengapa tidak ada perlawanan dari orang PKI?


J: Ya karena mereka tidak punya senjata. Mau apa?


PENGARUH LUAR NEGERI? BUDAYA


T: Bagaimana pengaruh faktor luar negeri?


J: Daftar orang PKI yang dibikin orang kedutaan Amerika itu?


T: Ya. Orang kedutaan AS itu kan bikin daftar 5 ribu orang PKI, lalu dia serahkan ke Suharto.


J: Saya juga kenal dengan orang Amerika itu. Karena waktu di Jakarta saya kadang-kadang ke Kedutaan Amerika dan ini orang memang punya obsesi yang aneh. Mungkin karena dia dididik sebagai Kremlinologist, dia sibuk bikin daftar dari orang-orang PKI. Pada waktu itu menjadi bahan ketawaan pegawai di kedutaan sendiri. Seolah-olah tentara Indonesia tidak pernah bikin daftar. Padahal mereka jauh lebih mengikuti seluk beluknya politik di Indonesia dari pada orang Amerika. Pada waktu itu orang kedutaan Amerika yang dapat berbahasa Indonesia dengan fasih, boleh dikatakan baru satu-dua. Saya tidak percaya bahwa tentara memerlukan daftar yang dibuat oleh pegawai kedutaan Amerika. PKI itu tidak di bawah tanah.


T: Salah satu studi Pak Ben yang lain adalah tentang “Mitologi dan Toleransi Orang Jawa,” terbit tahun 1965. Bagaimana Pak Ben menjelaskan pembunuhan massal itu dari pemahaman tentang budaya Jawa?


J: Saya merasa kita harus membedakan kebudayaan dalam keadaan normal, ketika tidak ada ketegangan mencekam, tidak ada suasana ketakutan yang luar biasa. Kalau melihat kehidupan sehari-hari dari orang Jawa, apalagi orang Jawa Tengah, mereka berusaha menghindari konflik yang terbuka, dsb.
Tapi dalam masyarakat apapun juga — kalau kita lihat sejarah dunia modern — pembantaian, pembunuhan yang luar biasa kejamnya, bisa timbul dalam suasana ketika orang merasa situasi sudah tidak ada ketentuan. Saya ambil contoh, umpamanya Yugoslavia. Pada akhir kesatuan Yugoslavia orang makin merasa bahwa hukum tidak berlaku, alat negara jelas terpecah, sebagian ikut ini sebagian ikut itu. Situasi ekonomi sudah mulai hancur. Dalam situasi seperti itu orang akan coba menyelamatkan diri sendiri dengan cara apapun. Mereka harus berbuat sesuatu yang dalam keadaan normal tidak mungkin terjadi.
Kita lihat umpamanya di Yugoslavia banyak sekali terjadi perkawinan antara orang Serbia dan Bosnia. Dan memang bahasanya sama. Jadi selama tiga puluh tahun sebelum terjadi huru-hara nggak ada masalah. Tapi kalau ketakutan besar sudah mulai, ya bisa terjadi bahwa si bapak yang Serbia terpaksa — supaya tidak dianggap antek karena bininya sebagai orang Bosnia calon penghianat — dia harus berbuat sesuatu yang kejem, demi keselamatan diri sendiri. Bisa juga dia malah membunuh istrinya sendiri. Dan ini bener-bener terjadi.
Kita lihat situasi yang sama umpamanya ketika terjadi pembagian India dan Pakistan oleh Inggris. Dan terjadi juga di tempat-tempat yang lain. Saya yakin di Amerika pun, kalau dalam situasi ketakutan seperti itu, akan banyak kekejaman yang bisa terjadi. Jadi saya merasa ini tidak kontradiktif dengan toleransi sehari-hari orang Jawa. Kekejaman ini harus dikaitkan dengan situasi yang serba tidak tentu.
Kita lihat bagaimana setelah peristiwa terjadi tidak pernah ada orang yang nongol di depan umum dengan bangga ngaku bahwa saya telah membunuh seratus lima puluh orang PKI. Malah propaganda dari pemerintah seolah-olah korban dari pihak pemerintah dan dari PKI kira-kira samalah. Jadi tidak ada kebanggaan atas pembantaian. Jaman sekarang istilah yang paling sering disebut adalah trauma. Itu jelas bukan trauma untuk yang mati, karena mereka sudah mati. Trauma justru untuk yang menang dan yang membunuh. Banyak orang yang membunuh akhirnya menjadi gila.


T: Orang Jawa sangat dipengaruhi cerita wayang. Dalam cerita wayang, Mahabarata itu diakhiri dengan Perang Baratayuda. Ramayana juga diakhiri dengan perang besar menyerbu Alengka. Apakah ada pengaruh dari budaya wayang ini?


J: Mahabarata dan Ramayana itu berasal dari India. Kalau orang Jawa itu pantang menggelarkan lakon Baratayuda karena sangat bahaya. Lakon itu dianggap dapat membawa malapetaka. Jadi itu pantang sekali. Setahu saya cuma ada satu desa di Delanggu, entah karena apa, setiap setahun sekali Baratayuda itu harus digelar. Tapi itupun harus dengan macam-macam upacara sebelumnya. Jadi sulit juga kalau dikatakan bahwa orang Jawa suka Baratayuda.
Siapa yang bertanggung-jawab? “Suharto dan pimpinan Angkatan Darat itu bertanggung jawab atas pembunuhan ini, itu jelas.” Tentang kesalahan PKI, “Bisa dikatakan bahwa pimpinan PKI yang bertanggung-jawab atas politik partai itu mungkin memang ada salahnya.” Lalu Pak Ben membuat catatan panjang tentang kesalahan pimpinan PKI. Tentang anak muda sekarang, “Kalau anak muda mengerti apa yang terjadi, mereka tidak bisa melihat situasi secara hitam-putih. Mereka harus melihat ke depan, jangan cuma menengok ke belakang.” Pelajaran apa yang bisa ditarik, “Kita sebagai manusia dalam situasi tertentu bisa menjadi pembunuh. Jadi kita harus berusaha keras supaya tidak timbul situasi di mana sifat binatang di dalam diri kita masing-masing bisa ke luar.”


PELAJARAN SEJARAH


T: Pelajaran apa yang bisa kami tarik dari pembunuhan jenderal-jenderal itu?


J: Saya tidak tahu pelajaran apa yang diambil dari pembunuhan jenderal. Kecuali bahwa orang yang hidup dari kekerasan akan dihancurkan oleh kekerasan. Jadi Untung, Supardjo, Latif, dll, harus bertanggung jawab atas apa yang mereka kerjakan. Bagaimanapun kalau orang tidur di rumah tahu-tahu dibrondong mitraliyur itu mengerikannya bukan main.


T: Kalau orang Indonesia tahu kebohongan tentang Lubang Buaya, lalu tahu ada pembunuhan massal, bagaimana sejarah akan menilai Suharto?


J: Banyak sejarah yang hilang. Ratusan ribu orang yang jadi korban itu tidak bisa bicara lagi. Berapa sebenarnya jumlah yang dibunuh, itu mungkin kita tidak akan bisa tahu dengan pasti. Dan dalam sejarah memang banyak peristiwa pembunuhan atau kekejaman yang kemudian tidak bisa diungkapkan. Misalnya saja sejarah perdagangan budak di Afrika. Kita tidak tahu berapa yang dibunuh, bagaimana dibunuhnya, siapa yang membunuh, dst. Jadi jangan terlalu yakin bahwa seluruh sejarah 1965 ini akan bisa diungkapkan.
Sekarang memang banyak sejarah yang dihapus. Kita juga harus ingat bahwa Orba selama ini justru melakukan apa yang suka saya sebut “kebijaksanaan pembodohan” masyarakat Indonesia sendiri. Bukan hanya tentang pembunuhan tahun 65. Tetapi juga tentang bagaimana munculnya kesadaran nasional, sejarah Jaman Pergerakan yang sebenarnya, tentang Jaman Jepang, Jaman Revolusi, dsb. Karena yang mau dijadikan pahlawan itu cuma ABRI. Padahal ABRI kan belum lahir waktu orang lain sudah berjuang puluhan tahun. Tapi toh sekarang mulai terlihat usaha anak-anak muda untuk mencari informasi, untuk menggali kembali sejarah bangsanya. Seperti terlihat dalam buku “Bayang-Bayang PKI” itu, antara lain. Dan memang ini tugas anak muda, kan? Untuk tidak mau dibodohi.
Tapi bahwa Suharto dan pimpinan Angkatan Darat itu bertanggung jawab atas pembunuhan ini, itu jelas. Dan bukan pembunuhan ini saja. Mereka juga bertanggung jawab atas ratusan ribu korban pembunuhan di Timtim, dan ribuan lagi orang yang dibunuh dalam kasus-kasus Irian Jaya, Aceh, Petrus, dsb. Sepanjang sejarah Indonesia, termasuk selama Jaman Belanda dan Jaman Jepang, belum pernah ada kelompok penguasa yang tangannya begitu berlumuran darah. Ya, itu fakta.


T: Tadi Pak Ben bilang soal pembunuhan di Kediri yang luar biasa kejam. Tapi ada juga propinsi seperti Jabar, di mana pembunuhan tidak meluas. Apa yang bisa dipelajari dari fakta-fakta seperti itu?


J: Laporan Cornell itu topik utamanya bukan pembunuhan massal. Tetapi apa yang terjadi dalam Gerakan 30 September. Apa sebabnya terjadi pembunuhan massal? Itu soal lain lagi. Untuk tahu tentang pembunuhan massal itu kita sudah tahu politik dari tentara di pusat. Tapi itu tidak selalu dilaksanakan pada tingkat lokal. Mengapa di Kediri pembunuhan meluas, tadi sudah dibicarakan, karena Danremnya saudara dari jenderal yang dibunuh. Kenapa di Jabar pembunuhan tidak meluas, juga sudah kita bicarakan. Pembunuhan di Aceh juga sangat kejam. Dan di Aceh juga banyak sekali orang Cina yang dibunuh. Itu juga karena inisiatif Pangdam Aceh waktu itu, Ishak Juarsa. Jadi ada dua tingkat. Kebijaksanaan pusat dan kebijaksanaan lokal dari pangdam, danrem, dsb. Studi yang lengkap memang belum ada.


T: Ini soal media. Sebelum pembunuhan massal, media massa — koran, radio, TV — dipakai Suharto untuk menyebarkan kabar bohong tentang kekejaman PKI di Lubang Buaya. Akibatnya masyarakat jadi tegang, ketakutan, saling curiga. Apa yang bisa kami pelajari dari pengalaman dengan media ini?


J: Kita harus belajar menghadapi media massa. Apalagi kalau sudah dimonopoli oleh suatu kelompok. Kita harus skeptis, curiga, jangan cepat percaya. Harus bisa membandingkan informasi dari media yang dikontrol penguasa itu dengan informasi lain, dengan pengalaman diri sendiri, dengan media luar negeri, internet, dll.
Karena media massa itu jelas alat penguasa yang dipakai untuk kepentingan penguasa. Bukan hanya di Indonesia, di Amerika juga begitu. Kalau lihat TV di sini selalu saya punya sikap curiga. Pokoknya belum tentu benar. Apalagi kalau media itu condong menghasut si penonton untuk jadi fanatik, untuk membenci, dsb. Kita harus skeptis. Belajar menjaga diri supaya nggak ikut terseret.
Saya sudah mengalami keadaan demikian di beberapa negara. Bukan cuma di Indonesia saja. Saya lihat bagaimana media massa di Inggris waktu Perang Malvinas. Media massa di Amerika waktu Perang Vietnam juga begitu. Jadi ini bukan sifat khusus media massa di Indonesia. Tetapi sifat khusus dari penguasa yang kalang kabut atau yang punya maksud jelek.
Kalau sekarang, kita bisa lihat umpamanya dalam Peristiwa 27 Juli. Tahu-tahu dicarikan kambing hitam. Sekelompok anak muda yang nggak sampai 200 orang jumlahnya. Ini jelas suatu usaha untuk bikin suasana panik. Untuk menutupi sebab-sebab sebenarnya dari peristiwa itu.


T: Menurut Pak Ben apa kesalahan PKI?


J: Saya tidak mau bilang bahwa PKI punya salah. Karena PKI itu suatu keluarga besar yang isinya jutaan orang. Saya tidak percaya kalau suatu organisasi yang begitu banyak manusianya, yang begitu beraneka warna, itu punya salah. Tapi bisa dikatakan bahwa pimpinan PKI yang bertanggung-jawab atas politik partai itu mungkin memang ada salahnya.
Pertama, pimpinan PKI mungkin tidak betul-betul mengerti atau tidak mempertimbangkan dengan matang kontradiksi yang ada antara strategi politik — yang berdasarkan pemilihan umum, aktivitas terbuka yang legal, keanggotaan partai yang jutaan — dengan retorika yang sangat radikal. Retorika PKI waktu itu cocok untuk dipakai dalam perang gerilya. Di mana pada akhirnya perjuangan akan ditempuh dengan jalan kekerasan.
PKI menganjurkan banyak sekali orang-orang biasa untuk ikut. Dan mereka jelas tidak pernah ada pikiran bahwa sewaktu-waktu bakal ada pembunuhan besar-besaran. Kalau mau mengajak jutaan orang biasa yang kerja di kantor, di desa, di kampung, untuk ikut suatu partai yang legal, di atas tanah, memakai lembaga-lembaga parlemen, pemilu, dsb. Tetapi sekaligus juga memakai retorika bahwa seolah-olah sewaktu-waktu bakal terjadi krisis yang revolusioner, di mana ini mau dihancurkan, itu mau dihancurkan, maka itu menimbulkan kontradiksi yang lumayan bahayanya.
Karena lawan politik yang mendengar retorika seperti itu — bahwa sekali waktu kamu akan dihancurkan, akan dibeginikan-dibegitukan — tentu merasa pada akhirnya akan terjadi suatu konflik fisik yang luar biasa dahsyatnya. Jadi dengan sendirinya mereka akan mempersiapkan diri. Dan pada waktu itu justru lawan PKI lah yang punya senjata.
Saya pernah dengar-dengar, Mao Tse Tung pernah mengatakan begini kepada pimpinan PKI, “Ini nggak bener. Kalau kamu mau menghancurkan ini, menghancurkan itu, ya itu nggak bisa di kota-kota, nggak bisa di parlemen, nggak bisa di tengah orang ramai. Itu harus di gunung, harus bikin perang pembebasan rakyat. Dan sebaliknya kalau kamu betul-betul mau pakai cara parlementer, maka retorika dan analisamu harus cocok dengan situasi dan strategi yang kamu pakai.” Jadi dengan demikian ada kemungkinan pimpinan PKI juga ikut membuka kesempatan untuk apa yang kemudian terjadi. Tapi, ya tentu ini cuma pendapat orang luar, seorang outsider. Saya juga merasa, kadang-kadang PKI juga terlalu mencari musuh yang nggak perlu. Umpamanya kampanye terhadap Manikebu (Manifesto Kebudayaan). Itu bukan kelompok yang penting, cuma beberapa orang yang sama sekali tidak berbahaya. Padahal lawan sebenarnya dari PKI itu, ya konglomerat, pimpinan tentara, dsb. Tetapi PKI tidak berani langsung menghadapi mereka. Lalu dicarikan target yang lebih gampang. Secara taktis ini tidak baik. Dan menimbulkan kesan PKI selalu ingin menang sendiri.
Pada waktu ada pemilihan umum yang bebas di Indonesia, partai yang paling besarpun tidak bisa dapat pendukung lebih dari 25% dari masyarakat. Tidak mungkin ada suatu organisasi yang bisa menang secara mutlak. Bagaimanapun Indonesia baru akan bisa maju dengan baik kalau ada persekutuan, ada aliansi antara kelompok-kelompok yang beraneka ragam.
Selain itu saya juga dapat kesan begini. Ini kesan yang timbul ketika mengikuti pengadilan Sudisman. Pada waktu Sudisman menghadapi kematiannya sendiri, dia tahu bahwa pengadilan itu adalah saat terakhir dia bisa ngomong. Dia banyak melepaskan diri dari bahasa resmi, bahasa formal, bahasa standard dari partainya. Lalu dia mulai ngomong sebagai manusia. Ya tentu bahasa partainya juga cukup banyak. Tetapi pidatonya itu mengesankan justru karena tidak pakai bahasa resmi. Dia ngomong sebagai orang Jawa, sebagai orang Indonesia, orang yang pernah ikut revolusi, yang ikut berjuang, dsb. Itu mengesankan. Jadi sepertinya PKI terlalu mengajak orang untuk ngomong dengan bahasa yang terlalu distandarisasikan, terlalu monoton, terlalu uniform. Sehingga bisa dibilang itu membunuh atau mengurangi kreatifitas.


T: Apakah rasa dendam dari mereka yang keluarganya dibunuh itu mungkin bakal muncul sekali waktu nanti?


J: Ada kemungkinan bakal muncul individu-individu yang demikian. Tapi jangan lupa bahwa peristiwa ini terjadi 30 tahun yang lalu. PKI sudah hancur selama 30 tahun. Dan kemenangan tentara adalah kemenangan yang mengerikan. Tetapi, dan ini perlu dimengerti juga, bahwa dalam proses membunuh, menyiksa, dsb, ternyata ada juga sebagian anggota partai, malahan juga di kalangan atasnya, yang kemudian ikut jadi penyiksa dan pembunuh. Jadi PKI tidak hanya dihancurkan oleh lawannya. Tetapi sebagian juga dihancurkan oleh anggotanya sendiri. Nah ini suatu luka yang sangat dalam.
Kalau anak muda mengerti apa yang terjadi, mereka tidak bisa melihat situasi secara hitam-putih. Dan toh Indonesia sudah banyak berubah. Masalah Indonesia sudah lain dan dunia juga sudah lain. Mereka harus melihat ke depan, jangan cuma menengok ke belakang. Harapan ada di depan. Tidak pernah ada di belakang.


T: Dalam periode sejarah yang serem ini, apakah Pak Ben melihat orang-orang yang bisa dianggap pahlawan?


J: Saya bukan orang yang gampang mencari pahlawan. Tapi saya tahu ada orang-orang yang semangat dan achlaknya memang saya kagumi. Saya kasih contoh 3 orang. Pertama, Pak Pram tentunya. Yang dengan segala penderitaannya toh bisa menciptakan karya sastra yang luar biasa dan tidak pernah mau tunduk kepada penindasnya. Saya juga angkat topi kepada teman saya yang sudah lama meninggal, si Soe Hok Gie. Yang walaupun aktif melawan PKI, tetapi pada waktu pembantaian massal, penangkapan dan pengiriman ke Buru, dia satu-satunya orang yang pada waktu itu berani mengatakan di pers bahwa ini salah. Dia satu-satunya orang yang menyatakan begitu pada tahun 60-an.
Yang ketiga, tentunya almarhum Pak Yap. Dia bersedia membela orang-orang yang sudah pasti akan dihukum mati. Yang notabene adalah lawan politiknya. Tapi dia berusaha keras membela mereka sebagus mungkin. Walaupun karena itu dia sendiri tentu rugi. Karena jadi dibenci oleh penguasa. Apakah mereka ini bisa dianggap pahlawan? Mungkin belum tentu. Tetapi sebagai orang yang punya karakter, punya moralitas, punya integritas, saya mengagumi mereka.


T: Kalau di antara politikus?


J: Sorry ya, nggak ada yang saya kagumi.


T: Kalau di kalangan orang-orang biasa? Pahlawan saya itu orang-orang biasa, pak. Tentu banyak juga orang-orang biasa yang sudah berani ambil resiko untuk menolong korban waktu itu. Atau menolong keluarga dan anak-anak orang yang dibunuh, dipenjara atau dibuang. Atau orang seperti Ibu Pram, dalam memoar Pak Pram di P. Buru, “Nyanyi Sunyi Seorang Bisu.” Atau seperti Ibu Oey dalam “Memoar Oey Tjoe Tat.” Mereka itu istri yang setia luar biasa dan tabah bukan main. Mereka bisa terus mengurus keluarga ketika suaminya dibuang belasan tahun, waktu anaknya masih kecil-kecil. Mereka kan orang biasa. Mungkin lebih gampang buat jadi contoh.


J: Ya. Tentu banyak orang yang bisa kita jadikan contoh. Cuma kita tidak tahu nama mereka. Tapi saya tahu dua contoh. Waktu pengadilan Sudisman saya masih ingat banyak saksi dari PKI, dari atas sampai bawah. Banyak sekali dari golongan atas yang sebenarnya memalukan sekali kesaksiannya. Mereka jelas-jelas mau mencoba mencuci tangan, melarikan diri dari tanggung-jawab sebagai atasan.
Tapi ada dua orang yang sikapnya bagus. Yang pertama Sri Ambar dari Gerwani. Dan yang paling mengesankan itu seorang anak Cina yang masih muda. Saya tidak ingat namanya. Dia menjadi kurirnya Sudisman waktu dalam persembunyian. Anak muda itu orangnya polos, berani, sopan, dan tidak pernah mau bertekuk lutut terhadap pengadilan. Tentu dia bukan orang yang penting, bukan orang yang dikenal namanya. Tetapi sikapnya hebat.


T: Secara umum, pelajaran apa yang bisa kami tarik dari pembunuhan massal-65?


J: Kalau tentang pembunuhan massal, ada perasaan umum di Indonesia yang seolah-olah peristiwa serem semacam itu jangan sampai terjadi lagi. Ini suatu yang mengerikan dan memalukan. Pelajaran satu lagi, bahwa kita sebagai manusia dalam situasi tertentu bisa menjadi pembunuh. Jadi kita harus berusaha keras supaya tidak timbul situasi di mana sifat binatang di dalam diri kita masing-masing bisa ke luar (Habis).

CatatWawancara tgl 20 & 23 September 1996
