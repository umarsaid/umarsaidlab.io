---
layout: post
title: NII Adalah Gerakan Makar Yang Harus Ditindak Tegas !
date: 2011-04-28
---

Hiruk-pikuk yang terdengar di negeri kita akhir-akhir ini tentang makin berkembang biaknya kegiatan-kegiatan NII (Negara Islam Indonesia), dan makin maraknya radikalisme di sebagian kalangan Islam, memerlukan perhatian dan kewaspadaan dari kita semua, yang mencintai keutuhan dan keselamatan negara dan bangsa kita.

Sebab, para pendukung NII dan berbagai kelompok radikal kalangan Islam ini telah terang-terangan menyatakan di berbagai kesempatan dan melalui berbagai cara bahwa mereka sudah tidak mengakui NKRI sebagai negara hukum yang sah, atau menyatakan bahwa mereka bukanlah lagi warganegara NKRI.

Sebagian dari mereka bahkan menyatakan bahwa mengibarkan dan menghormati bendera Sang Merah Putih serta Pancasila adalah perbuatan terlarang menurut agama. Di antara mereka  juga ada yang melarang dinyanyikannya  lagu kebangsaan Indonesia Raya dan menghormati lambang Bhinneka Tunggal Ika.

Persoalan lama yang sengaja dibiarkan saja

Tentang meluasnya gerakan radikalisme di Indonesia, ketua Umum PB NU secara tegas menyatakan  bahwa sekarang ini sudah mencapai tingkat « lampu merah » yang sangat membahayakan negara. Apa yang diungkapkan dengan jelas olehnya itu digarisbawahi oleh Din Syamsudin, Ketua Umum PP Muhammadiyah, dan oleh Amien Rais, (mantan pimpinan Muhammadiyah juga).

Dengan membaca isi pernyataan ketiga tokoh kalangan agama itu,  maka kita dapat memperoleh sedikit  gambaran bahwa masalah NII atau radikalisme ini memang sebenarnya sudah merupakan persoalan lama, namun sengaja dibiarkan tidak terselesaikan oleh pemerintahan Orde Baru di bawah Suharto, dan diteruskan oleh berbagai  pemerintahan yang menggantikannya.

Bahkan, sekarang banyak terbongkar bahwa pimpinan Orde Baru, terutama dari kalangan Angkatan Darat, sudah lama memupuk kerjasama atau persekongkolan dengan berbagai kelompok radikal di kalangan Islam, untuk kepentingan politik. Pimpinan Angkatan Darat, melalui aparat-aparatnya seperti (antara lain ; intelijen, Bakin atau BAIS, atau badan-baan lainnya) menggunakan kelompok-kelompok radikal di kalangan Islam, untuk memperkuat dominasi kekuasaan rejim militer atau melindungi « kestabilan » rejim militernya.

Kerjasama atau persekongkolan pimpinan Angkatan Darat dengan kelompok-kelompok Islam radikal ini ditujukan untuk tetap (atau terus-menerus)  menghidupkan berbagai sentimen anti ajaran-ajaran revolusioner Bung Karno dan mencegah bangkitnya kembali segala macam manifestasi pro-PKI.

Terlibatnya tokoh-tokoh Orde Baru dengan pesantren Al Zaitun

Kiranya,  kita semua masih ingat bahwa karena besarnya obsesi anti ajaran-ajaran revolusioner Bung Karno dan anti-PKI inilah sebagian dari pîmpinan Angkatan Darat telah merangkul, atau membujuk (dan menggunakan !)  semua golongan reaksioner atau kotra-revolusioner, termasuk kelompok-kelompok penerus DI-TII, yang tergabung dalam NII atau FPI atau bermacam-macam kelompok-kelompok Islam radikal lainnya.

Berdirinya dan banyaknya kegiatan « pesantren modern » Al Zaitun di Jawa Barat adalah salah satu contoh kecilnya. Selama ini sudah cukup banyak tulisan atau berita yang mengungkap keterlibatan « tokoh-tokoh » Orde Baru , seperti (antara lain !) : Wiranto, Hendropriyono, Tutut Suharto, Habibi, dalam kegiatan-kegiatan Al Zaitun. Padahal, pimpinan Al Zaitun adalah Panji Gumilang, anak-buah setia « pemimpin besar » pemberontak Darul Islam, Kartosuwirjo.

Seperti sama-sama kita ingat, Kartosuwiryo telah dijatuhi hukuman mati oleh Mahkamah Militer dalam tahun 1962 karena dituduh memimpin pembrontakan bersenjata DI-TII dan melakukan usaha pembunuhan terhadap Presiden Sukarno. Jadi, « hubungan historis » antara DI-TII dengan pesantren Al Zaitun dapat dilihat dari segi ini juga. Sekarang ini, Al Zaytun mempunyai jaringan-jaringan yang erat dengan berbagai gerakan Islam radikal, termasuk dengan NII.

Dari apa yang telah diberitakan oleh banyak pers dan televisi selama ini sudah jelaslah bagi kita semua bahwa gerakan di bawah tanah yang dilakukan oleh kelompok-kelompok NII-KW9 adalah  gerakan makar terhadap negara dan pemerintahan Republik Indonesia. Artinya, sudah jelas pula bahwa NII-KW9 adalah musuh negara dan bangsa Indonesia.

NII KW 9 adalah kelanjutan dari DI-nya Kartosuwirjo

Sekarang ini, NII-KW 9 adalah kelanjutan atau perpanjangan dari DI-TII-nya Kartosuwirjo, yang sampai tahun 1962 memusuhi Bung Karno dan anti-PKI. Dari segi ini dapatlah dimengerti bahwa sebagian pimpinan Angkatan Darat atau sebagian aparat-aparatnya, sekarang ini, mempunyai kesamaan pandangan politik (dan « bekerja-sama » secara sembunyi-sembunyi )  dengan sebagian golongan Islam radikal.

Pimpinan Angkatan Darat pada umumnya  -- sampai sekarang !  --  begitu antinya kepada Bung Karno dan kepada PKI (dan kepada semua golongan kiri pada umumnya)  sehingga mau merangkul kelompok atau kalangan yang sebenarnya adalah musuh-musuh negara dan bangsa, Padahal, musuh-musuh negara yang sebenarnya ini, yang terdiri dari golongan Islam radikal, sudah terbukti melakukan berbagai pengkhianatan terhadap Pancasila, Bhinneka Tunggal Ika, dan terhadap Bung Karno dan pendukungnya yang utama, yaitu PKI.

Sesungguhnya, musuh-musuh negara dan bangsa ini sejak lama sudah menentang Pancasila, Bhinneka Tunggal Ika, bendera Sang Merah Putih, dan lagu kebangsaan Indonesia Raya, dan melawan NKRI,  karena mereka menginginkan  berdirinya Negara Islam Indonesia dan berlakunya Syariat Islam. Mereka adalah pengkhianat proklamasi 17 Agustus 45 dan pengkhianat terhadap para pejuang kemerdekaan bangsa kita.

NU dan Muhammadiyah : NKRI tak bisa diotak-atik lagi

Keinginan mereka ini pastilah tidak akan diterima oleh rakyat Indonesia. Seperti yang sudah dinyatakan berkali-kali oleh pimpinan organisasi Islam yang terbesar Nahdatul Ulama dan Muhammadiyah, Negara Kesatuan Republik Indonesia beserta Pancasilanya adalah adalah satu-satunya bentuk yang sudah tidak bisa ditawar-tawar lagi atau diotak-atik kembali.

Pendirian NU dan Muhammadiyah ini sesuai dengan perkembangan Islam di Indonesia sekarang ini, dan juga seiring atau sejiwa dengan apa yang terjadi dewasa ini di banyak negara-negara Arab atau negara muslim di dunia pada umumnya. Seperti kita ketahui masing-masing, di sebagian besar negara-negara Arab di dunia, yang jumlahnya 20 negara dengan seluruh penduduk sebesar 250 juta sekarang ini, sedang terjadi pergolakan-pergolakan besar dan penting untuk menuntut adanya perubahan atau pembaruan dalam sistem pemerintahan dan kehidupan sosial, ekonomi dan kebudayaan.

Perkembangan ini sekarang sedang  terjadi di Mauritania, Maroko, Aljazair, Libia, Tunisia, Mesir, Yaman, Oman, Bahrein, Saudi Arabia, Jordania, Siria, dengan skala yang berbeda-beda, dan isi yang juga tidak selalu sama, dan tujuan yang bisa berlain-lainan pula. Namun,  semuanya berusaha untuk memperjuangkan perubahan atau pembaruan. Semuanya ini akan merupakan sumbangan kepada terjadinya situasi baru di negara-negara Arab dan dunia Islam di kemudian hari.

Barangkali di negeri kita, Indonesia, ada saja orang-orang atau kalangan yang bertanya-tanya  mengapa terjadi pergolakan-pergolakan di begitu banyak negara Arab secara beruntun-runtun atau sekaligus bersamaan, padahal semuanya ada di bawah pemerintahan yang  -- sedikit banyaknya dan walaupun  berbeda-beda -- mentrapkan ketentuan-ketentuan yang menganut agama Islam.

Tentunya ada hal-hal dalam sisitem pemerintahan, sosial, ekonomi dan kebudayaan, yang tidak disukai atau ditentang oleh rakyat berbagai negara itu sehingga begitu banyak orang bersedia berjuang sampai mengorbankan jiwa mereka, seperti yang  terjadi Siria, Yaman, Mesir, Tunisia, Libia, Aljazair dan Maroko dll. Padahal, kebanyakan pemerintahan-pemerintahan di negara Arab itu  (yang umumnya mengatur kehidupan  masyarakat secara Islam itu) ada yang berkuasa sudah  puluhan tahun, dan yang ternyata gagal.

Dengan adanya pergolakan-pergolakan  di banyak negara Arab sekarang ini, yang justru menuntut adanya perubahan atau pembaruan dalam kehidupan bernegara dan bermasyarakat, maka kelihatan sekalilah betapa kelirunya atau betapa sesatnya tujuan perjuangan NII KW9 untuk mendirikan Negara Islam Indonesia dan menggantikan NKRI.

Tujuan perjuangan NII merupakan kemunduran bangsa dan negara

Apa yang diperjuangkan NII KW9 adalah bertentangan sama sekali dengan tujuan kebanyakan pergolakan-pergolakan  yang terjadi di negara-negara Arab dewasa ini. Negara Islam Indonesia yang mereka perjuangkan tidaklah mungkin  --  sama sekali tidak mungkin !!! -- lebih maju, lebih terbuka, dan lebih toleran dari NKRI yang sekarang, walaupun masih banyak sekali persoalan-persoalan parah yang harus ditangani (antara lain : korupsi, penegakan hukum, keadilan sosial, pelanggaran  HAM dll dll). Negara Islam Indonesia adalah kemunduran bangsa dan kerusakan Pancasila dan Bhinneka Tunggal Ika.

NII adalah musuh rakyat Indonesia, dan bangsa Indonesia haruslah berjuang sekuat-kuatnya  dengan berbagai jalan, untuk menumpasnya. Dalam hal ini pemerintah haruslah berani bertindak dengan tegas. Namun, pemerintah (yang mana pun !!!) tidak akan bisa bertindak tegas dan menumpas NII ini, selama masih ada pejabat-pejabat atau  tokoh-tokoh yang karena anti ajaran-ajaran Bung Karno dan anti-PKI tetap terus bersekongkol dengan unsur-unsur Islam radikal, yang merupakan musuh dan pengkhianat bangsa dan negara.

Paris, 28 April 2011               

A. Umar Said

* * *               

Lampiran

PBNU : Radikalisme di Indonesia Sudah "Lampu Merah"

Jakarta 26 April 2011 (ANTARA News) - Pengurus Besar Nahdlatul Ulama (PBNU) menilai radikalisme di Indonesia sudah pada tingkatan "lampu merah" atau sangat membahayakan sehingga negara harus berani menindak tegas.

"Ini sudah `lampu merah`, sudah `emergency`. Negara harus tegas, segera ambil tindakan," kata Ketua Umum PBNU KH Said Aqil Siroj kepada wartawan di kantor PBNU, Jakarta.

Dikatakannya, terungkapnya pelaku teror bom yang berasal dari kalangan terpelajar dan memiliki perekonomian yang baik menunjukkan radikalisme telah menyentuh kalangan menengah.

Said Aqil juga merujuk hasil survei Lembaga Kajian Islam dan Perdamaian (LaKIP) yang menunjukkan lebih dari 40 persen pelajar di Jakarta dan sekitarnya cenderung setuju menempuh aksi kekerasan untuk menyelesaikan masalah agama dan moral.

Survei yang digelar selama Oktober 2010-Januari 2011 tersebut dilakukan di 59 sekolah swasta dan 41 sekolah negeri dengan responden 590 guru pendidikan agama Islam di SMP dan SMA, 993 siswa SMP umum kelas XIV dan XIX, serta siswa SMA kelas X, XI, XII.

Selain itu, menurut Said Aqil, jaringan kelompok radikal juga sudah sampai ke tingkat desa, tidak hanya terkonsentrasi di kota.

"Ini jelas bahaya sekali. Radikalisme sudah sempurna, punya sistem, orang, pelatih, dan sumber dana," kata Said Aqil.

Menurutnya, merebaknya radikalisme bukti kegagalan Kementerian Agama menjalankan tugasnya membangun, mengawal, dan meningkatkan moralitas dan spiritualitas bangsa.

Said Aqil mengatakan perlu penanganan secara komprehensif untuk menanggulangi radikalisme, mulai dari pendekatan konstitusi khususnya memberikan pemahaman kepada masyarakat bahwa Indonesia bukan negara agama, pendekatan ekonomi, sosial budaya, hingga keamanan.

"Ini butuh `political will` pemerintah dan DPR," kata alumni Universitas Ummul Qura, Arab Saudi tersebut.

Yang tidak kalah penting dilakukan, menurut Said Aqil, adalah mengembalikan kepercayaan masyarakat terhadap pemerintah dengan dihilangkannya ketidakadilan di bidang hukum, politik, ekonomi, dan lainnya.

"Selama masih seperti itu, pemerintah tidak akan dipercaya. Nasihat, arahan, khutbah tidak ada artinya. Suara NU sampai habis pun tidak ada artinya," katanya.

Dikatakannya, radikalisme agama memang bukan asli Indonesia, tetapi datang dari luar dan mendapat sokongan dari luar.

Dikatakannya, setidaknya ada 12 organisasi atau yayasan di Indonesia yang mengajarkan teologi radikal dan mendapat dukungan dana dari Timur Tengah terutama Arab Saudi.

Menurut Said Aqil, kepentingan politik, terutama pihak yang mencoba meraih simpati dari kalangan Islam, turut memiliki andil bagi tumbuh suburnya radikalisme. Demi kepentingan politik, kelompok-kelompok radikal justru "dilindungi".

"Kita tuntut keberanian pemerintah untuk menindak gerakan radikal atas nama apapun," katanya.(*)
