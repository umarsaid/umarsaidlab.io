---
layout: post
title: Stop projek keblinger gedung baru DPR (yang megah dan mewah)
date: 2010-09-04
---

Rencana pembangunan gedung baru DPR yang akan memakan beaya 1, 8 triliun Rupiah telah  menuai banyak sekali reaksi negatif dari berbagai kalangan. Banyak yang menyatakan kemarahan yang meluap-luap, ada yang sampai setengah memaki-maki, (bahkan ada yang mengungkapkannya dengan kata-kata kotor dan kasar). Semua itu dapat dimengerti, dan juga adalah wajar-wajar saja, kalau kita ingat bahwa masalah rencana pembangunan gedung baru DPR ini memang  menyangkut banyak soal yang menyakitkan hati rakyat.

Masalah ini  juga menimbulkan pertanyaan tentang kewarasan nalar serta kejernihan cara berfikir pejabat-pejabat yang bertanggung-jawab. Rencana pembangunan gedung baru DPR ini betul-betul meragukan kebersihan hati-nurani para pejabat.   Juga menimbulkan kecurigaan apakah di belakang rencana  yang serba besar dan mewah ini tidak tersembunyi maksud-maksud haram dan tujuan untuk maling secara besar-besaran dan berjemaah pula ?

Bagaimana tidak ? Sebab, menurut rencana, gedung baru DPR ini akan bertingkat 36 lantai. Untuk pembangunan fisiknya saja direncanakan 1,1 triliun Rupiah. Sungguh, suatu jumlah yang tidak kecil, kalau diingat bahwa negara sebenarnya tidak punya uang lagi untuk memenuhi kebutuhan rakyat, dan bahkan harus mencari  utang kesana-kemari. Apalagi, anggaran itu belum termasuk untuk pengadaan perlengkapan. Ditambah dengan pemasangan jaringan teknologi informasi, furniture, dan keamanan, diperkirakan biaya seluruhnya Rp 1,8 triliun.

Hati kita bertambah besar brontaknya, kalau kita dengar bahwa angka 1,8 triliun Rupiah untuk pembangunan gedung baru DPR ini mencakup pembangunan kamar ruang kerja bagi 560 anggota DPR, yang direncanakan 120 meter persegi luasnya tiap ruangan bagi tiap anggota DPR. Sudah tentu, seperti biasanya, ruangan seluas 120 meter persegi ini akan diperlengkapi dengan furniture  dan perlengkapan-perlengkapan lainnya (air condition, wc, kamar mandi, televisi dll dll).

Apakah DPR sungguh-sungguh memerlukan pembaruan gedung ?

Sebetulnya, apakah para anggota DPR memerlukan ruangan kerja yang begitu luas dan begitu mewah ?  Wakil Ketua DPR Pramono Anung (dari PDI Perjuangan) justru mempertanyakan  urgensi pembangunan gedung mewah 36 lantai itu. Ia juga mengkritik keras rencana penyediaan fasilitas relaksasi bagi anggota Dewan. "Kenapa enggak sekalian dibuka panti pijat? Tidak perlu ada fasilitas relaksasi, apalagi kalau ada fitness segala. Berlebihan," ujar Pramono, menanggapi rumor bahwa fasilitas spa dan kolam renang juga tersedia bagi anggota Dewan.

Oleh karena itu, Pramono mengatakan, ia akan mengusulkan agar rencana pembangunan gedung dievalusi kembali. Secara keseluruhan, pembangunan gedung itu dinilai mencederai rasa keadilan masyarakat.  "Apalagi kalau luasnya sampai 120 meter persegi. Ruangan saya saja 70 meter persegi sering merasa kedinginan, apalagi 120 meter?" kata Pramono.  (Kompas, 1 September 2010)

Ruangan 120 meter persegi untuk tiap-tiap anggota DPR ini diperkirakan akan menghabiskan anggaran 2,8 miliar Rupiah per ruangan, dan sekitar Rp 500 juta untuk interior dan furniture. Besarnya ruangan ini sama dengan luas lima rumah sederhana sehat bersubsidi, yang masing-masing hanya 21 meter persegi. Dalam ruangan ini direncanakan akan bekerja 7 orang,  yaitu 5 staf ahli dan 1 staf pribadi di samping anggota DPR sendiri. Ruangan itu akan dilengkapi dengan ruang rapat dan istirahat.

Bahwa gedung DPR yang akan dibangun itu betul-betul merupakan gedung yang mewah sekali,  dapat dilihat dari rencana untuk dibangunnya tempat rekreasi, restoran-restoran, kolam renang, spa, apotek, minimarket, ruang istirahat untuk anggota DPR, lift sebanyak 14, taman-taman dan helipad di lantai atas.


Astagafirulah, hebat benar DPR kita itu !!!


Astagafirullah, hebat benar DPR kita ini, mungkin ada orang-orang tertentu yang berceloteh demikian, Bisa saja juga ada orang lainnya yang bergumam Audzubillah. Barangkali, ada juga lainnya lagi yang menyebut Masya Allah, sambil mengelus-elus dada karena keheranan dan kegeraman. Kalau ada orang-orang yang  melontarkan ungkapan-ungkapan serupa itu maka bisa diartikan bahwa masalah pembangunan gedung baru DPR itu memang menarik perhatian dan memunculkan kejengkelan atau kemarahan banyak kalangan. Sebagian kecil dari contoh-contohnya, adalah sebagai berikut :


Sekretaris Jenderal Forum Indonesia untuk Transparansi Anggaran Yuna Farhan menilai, anggaran pembangunan gedung baru DPR terlalu besar. Jika dihitung rata-rata, harga satu ruangan anggota DPR itu Rp 2,8 miliar. Nilai itu sangat mahal, bahkan untuk perkantoran di Jakarta.


”Bukan memikirkan kepentingan rakyat, DPR malah mikirin diri sendiri. Rasanya tidak rela jika segelintir orang yang menjadi wakil rakyat bermewah-mewahan saat rakyat masih miskin,” ujar Suwoko HS, pensiunan TNI berpangkat sersan kepala, saat ditemui di Jakarta,

Bagi rakyat miskin di Jakarta, Gedung DPR saat ini sudah mewah sehingga niat untuk membangun gedung baru yang lebih mewah tak masuk akal mereka. Bayangkan, untuk membangun gedung berlantai 36 itu, pemerintah harus menyisihkan anggaran Rp 1,6 triliun.
Anggaran sebesar itu sebenarnya cukup untuk membiayai bantuan iuran jaminan kesehatan masyarakat (Jamkesmas) bagi lebih dari 22 juta warga miskin selama satu tahun. Tahun ini pemerintah mengalokasikan dana bantuan iuran jamkesmas sebesar Rp 6.000 per bulan atau Rp 72.000 per tahun untuk satu warga miskin.


Melanggar rasa kepantasan, kata Mahfud MD (Ketua Mahkamah Konstitusi)


Sementara itu, sikap para anggota DPR dikritik Mahfud M.D. yang kini menjabat ketua Mahkamah Konstitusi. Menurut dia, pembangunan gedung dewan itu melanggar rasa kepantasan. ''Saya pernah di DPR. Menurut saya, gedung yang sekarang masih sangat baik,'' ujar Mahfud di gedung MK



Dia menilai, kengototan dewan melanjutkan pembangunan gedung baru tersebut sebenarnya hanya dilandasi semangat gagah-gagahan. ''Okelah kalau dibuat gagah-gagahan. Itu tidak melanggar hukum, tapi melanggar rasa kepantasan,'' imbuh Mahfud. Karena itu, dia mengharapkan sebaiknya proyek pembangunan gedung baru itu dipertimbangkan lagi lebih lanjut.

Bahkan, Mahfud menyindir, walaupun gedung DPR dibangun sehingga menjadi lebih mewah dan gagah, itu tetap tidak akan memengaruhi kemampuan anggota dewan. ''Kemampuan mereka tidak akan meningkat. Pasti masih begitu-begitu saja,'' sindirnya kembali.

Jadi, menurut dia, percuma fasilitas gedung dibenahi sedimikian rupa. Sebab, hal itu tidak akan berdampak signifikan kepada produktivitas kerja penghuninya. ''Asal mau kerja keras, produktivitas anggota sebenarnya tak perlu terganggu hanya karena gedung,'' tandasnya.

Lebih lanjut, menteri pertahanan era Presiden Abdurrahman Wahid itu menambahkan, anggaran yang akan digunakan sebaiknya dialihkan untuk keperluan lain yang mendesak. Misalnya, untuk membantu korban bencana alam dan membangun gedung pengadilan di luar Jawa. ''Gedung pengadilan di luar Jawa itu masih sangat jelek. Atau, kalau tidak, untuk membangun rumah prajurit, itu lebih pantas. Mereka lebih memerlukan,'' tegasnya. (Jawapos, 1 September 2010)


Apa mau mengalahkan gedung parlemen di banyak negara ?


Mohon parhatian para pembaca tentang yang berikut ini :  kalau rencana pembangunan DPR yang baru ini kemudian terlaksana menjadi kenyataan, maka gedung yang megah dan mewah ini akan mengalahkan gedung parlemen di banyak negara di dunia !!!  Sebab, gedung parlemen Prancis yang sudah termashur di seluruh dunia sejak lama sekali saja tidak mempunyai fasilitas-fasilitas yang berlebih-lebihan mewahnya seperti yang direncanakan di Jakarta itu. Demikian juga kiranya gedung parlemen Eropa, Jerman, Belanda, Belgia, Inggris, Spanyol, dan Italia.


Apakah yang dimaksudkan untuk dicapai dengan projek yang begitu megah dan mewah itu supaya kelihatan gagah dan mengalahkan banyak negara di dunia ? Apakah supaya  Indonesia bisa dibanggakan karena mempunyai  gedung parlemen yang begitu indah ? Sungguh betul-betul keblinger, kalau begitu (untuk tidak menggunakan istilah kasar  « sinting ») Sebab kegagahan  dan kebanggaan yang begitu itu jelas hanyalah semu atau palsu saja, kalau untuk pengeluaran yang begitu besar itu negara terpaksa cari utang yang lebih besar lagi  (yang sekarang saja sudah lebih dari  Rp 1667 triliun, di samping harus diterlantarkannya terus rakyat miskin yang jumlahnya melebihi 40 juta, dan pengangguran puluhan juta pula.


Projek semegah dan semewah ini sama sekali tidaklah menunjukkan tanda kehebatan negara dan bangsa Indonesia, melainkan sebaliknya !!! Ini menunjukkan   ketidakwarasan cara berfikir  (dan sakitnya hati nurani) para tokoh atau pejabat yang berkongkalikong dengan  para pengejar keuntungan (yang tidak selalu halal) yang terdiri dari begitu banyak berbagai « calok », konsultan, promotor, designer, arsitek, dan seabreg-abreg orang lainnya.


Gedung DPR yang sederhana di bawah pimpinan Bung Karno


Projek pembangunan gedung baru DPR yang begitu itu tidak menaikkan citra bangsa dan negara, melainkan malahan merusaknya. Kehebatan citra bangsa tidak bisa dibangun atas penderitaan rakyat banyak, yang sebagian besarnya masih dalam kehidupan yang serba kekurangan dan menyedihkan.


Citra bangsa dan negara Indonesia pernah menjulang tinggi di dunia ketika dipimpin oleh Bung Karno. Pada waktu itu DPR, yang gedungnya adalah sangat sederhana sekali kalau dibandingkan dengan yang sekarang, mempunyai martabat yang tinggi dan kewibawaan yang besar, berkat politik dan ajaran-ajaran revolusioner Pemimpin Besar Revolusi, Bung Karno.. DPR waktu itu adalah salah satu dari alat revolusi rakyat Indonesia, yang suaranya didengar dan dihormati oleh banyak orang di dalam dan luar negeri.
Kalau pembangunan gedung baru DPR toh akan terjadi juga nantinya, maka tidak akan mendatangkan kemajuan yang berarti banyak bagi perubahan besar-besaran dan fundamental untuk negara dan bangsa. Sebab, dengan kualitas, sikap moral dan politik anggota-anggota DPR seperti yang sekarang ini, maka banyak soal akan tetap begitu-begitu juga

.
Alasan-alasan  yang dikemukakan oleh para pendukung rencana pembangunan gedung baru DPR bahwa gedung baru itu perlu karena untuk lebih meningkatkan efektifitas kerja anggota DPR, bahwa gedung yang sekarang sudah tidak memadai kebutuhan DPR, bahwa fasilitas-fasilitas yang serba lengkap dan mewah itu untuk menjaga kenyamanan kerja anggota DPR dan supaya tetap dalam segar bugar, adalah alasan-alasan yang aneh, yang tidak masuk akal, yang menyakitkan hati rakyat, dan juga yang ……. ;terlalu mahal !
Supaya rencana keblinger ini dibatalkan saja !
Karena pembangunan gedung baru DPR ini akan tetap menjadi masalah yang ramai dipersoalkan, banyak orang, maka wajarlah kalau seluruh kekuatan demokratis di Indonesia juga bersuara lantang untuk melawan rencana keblinger ini, dan menuntut supaya dibatalkan atau dievaluasi kembali.


Rakyat Indonesia tidak membutuhkan suatu DPR atau parlemen yang bergedung semegah dan semewah seperti yang direncanakan itu, tetapi yang tidak bekerja untuk kepentingan seluruh rakyat seperti selama ini. Rakyat Indonesia akan bangga dan senang dengan DPR yang gedungnya memadai atau selayaknya, tetapi  mempunyai anggota-anggota yang betul-betul pro-rakyat, dan yang sungguh-sungguh berhak dan pantas disebut sebagai wakil rakyat.


Mengingat bahwa pembangunan gedung baru DPR ini akan menjadi masalah yang akan lama ramai dibicarakan banyak orang dan karena pentingnya bagi kehidupan bangsa, maka kita semua patut mengikuti seteliti mungkin perkembangannya selanjutnya. Kita semua tidak boleh membiarkan bebas begitu saja segala praktek-praktek terselubung para musang berbulu ayam di belakang projek raksasa ini yang mau  mengeruk harta haram mereka, atas kerugian  para pembayar pajak.


Jelaslah kiranya bahwa pembangunan gedung baru DPR ini bukanlah projek yang betul-betul dicita-citakan oleh rakyat (walaupun disetujui oleh sejumlah « wakil rakyat »), melainkan adalah gabungan kongkalikong antara pejabat atau tokoh-tokoh dalam pemerintahan dengan para pengusaha atau berbagai oknum-oknum yang bermental korup dan anti-rakyat.
Kengototan orang-orang yang mau meneruskan projek yang begitu keblinger ini harus dilawan.
