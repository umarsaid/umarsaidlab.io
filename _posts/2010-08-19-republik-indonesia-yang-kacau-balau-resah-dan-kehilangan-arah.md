---
layout: post
title: Republik Indonesia yang kacau-balau, resah, dan kehilangan arah
date: 2010-08-19
---

Jalan revolusi rakyat adalah  satu-satunya cara

untuk mengadakan perubahan besar-besaran





Peringatan Hari Kemerdekaan 17 Agustus sudah lewat beberapa hari.  Kiranya selayaknyalah  kalau kita semua berusaha merenungkan bersama-sama tentang berbagai hal yang berkaitan dengan Negara  dan Bangsa, baik di masa-masa yang lalu, masa sekarang, maupun di masa-masa yang akan datang. Selanjutnya, sesudah umur 65 tahun, apa jadinya Republik kita yang mempunyai penduduk sebanyak lebih dari 238 juta ini ?

 Sungguh banyak betul hal-hal besar dan penting mengenai negara dan bangsa yang perlu dan bisa kita renungkan bersama-sama atau kita telaah. Bahkan, terlalu banyak, terlalu beragam, dan terlalu parah, juga terlalu ruwet, sehingga sering sekali timbul kebingungan untuk memilih mana sajakah atau apa sajakah  yang lebih urgen untuk ditelaah dan ditangani lebih dulu.  

Kiranya, berdasarkan pengalaman yang sudah-sudah -- dan sekarang juga  --kita sudah bisa meramalkan bahwa situasi tidak akan banyak berubah di masa dekat ini. Banyak soal besar dan parah akan tetap sami mawon, alias masih akan tetap begitu-begitu juga. Kerusakan moral dan kebejatan akhlak atau kebusukan iman akan tetap merajalela dan menjadi sumber segala macam penyakit parah bangsa. Korupsi yang meluas di seluruh negeri akan tetap sulit diberantas, dan penegakan hukum akan terus dilecehkan dan diinjak-injak oleh banyak kalangan.



Negara  kita sekarang dalam keadaan kacau-balau

Pada macam-macam kesempatan banjak tokoh Indonesia sudah menyatakan dengan berbagai cara, dan beragam nuansa (dan isi) tentang seriusnya keadaan bangsa dan negara kita dewasa ini. Umpamanya, mantan Presiden Megawati mengecam secara pedas sekali bahwa negara kita sekarang kacau balau.

Dalam memperingati Hari Kemerdekaan 17 Agustus sekarang ini perlulah kiranya kita perhatikan apa yang ditulis oleh Kwik Kian Gie, seorang ekonom Indonesia, yang pernah menjabat Menko Ekonomi dalam pemerintahan Presiden Abdurrachaman Wahid, mantan Ketua Bappenas, dan Ketua Litbang PDI-P. Antara lain ia menyatakan bahwa Indonesia dewasa ini sudah kembali menjadi JAJAHAN ASING. Populernya sering disebut sebuah koloni baru kaum imperialis. Ia memberikan analisis yang sangat tajam tentang sebab-musabab mengapa negeri kita sampai jadi begini. Dipaparkannya dengan jelas tentang: Sejarah Penguasaan Ekonomi Indonesia oleh Kekuatan Asing dan Kelompok Berkeley Mafia.

Pengamat ekonomi Faisal Basri mengatakan bahwa majoritas bangsa Indonesia masih belum menikmati kemerdekaan. Menurutnya, para petani dan buruh yang jumlahnya sekitar 82 persen dari seluruh rakyat Indonesia, hidupnya masih dipenuhi kecemasan.

Mantan Menko Perekonomian Rizal Ramli menilai, setelah 65 tahun kemerdekaan, bangsa Indonesia kini sedang berada di ujung tanduk, dipenuhi ketidakpastian, dan ketidakmenentuan yang mengombang-ambingkan masa depan seluruh bangsa Indonesia.  “Negeri Indonesia adalah negeri yang kaya raya, negeri yang dipenuhi oleh segala macam sumber daya alam. Tapi kita masih saja terpuruk bahkan akhir-akhir ini rakyat Indonesia yang berjumlah 230 juta jiwa ini hanya dijadikan pasar bagi produk-produk bangsa lain” ungkapnya. «Sekarang ini kita memiliki pemimpin, tetapi tidak ada kepemimpinannya,” Kata Rizal Ramli,

Solahudin Wahid mengungkapkan bahwa Indonesia adalah negara yang lemah, terutama tatkala presiden Soekarno digulingkan oleh Soeharto. “Negara telah banyak abai terhadap rakyat. Lumpur menyembur, pemerintah diam. Kekerasan atas nama keyakinan terjadi, pemerintah diam. Pendidikan kita tertinggal, pemerintah juga hanya diam saja,”

 Frans Magnis Suseno menyoroti banyaknya ketidakberesan dalam kebijakan ekonomi dan politik yang diambil pemerintah. Hal ini tercermin dari fakta masih banyaknya anggota masyarakat yang belum terlindungi dalam menjalankan keyakinannya, serta munculnya banyak kasus korupsi yang muncul secara terus menerus setiap hari. “Setiap orang belum memperoleh jaminan untuk menjalankan keyakinannya bebas dari rasa takut,” ungkap Romo Magnis.

Koordinator Gerakan Indonesia Bersih (GIB), Adie Massardie, mengatakan  selain sekitar 82 persen rakyat masih cemas memikirkan kebutuhan sehari-hari, kesejahteraan dan keadilan masih sangat jauh.



Negara kita sebenarnya belum merdeka sepenuhnya

Yang tersebut di atas itu adalah hanya baru sebagian kecil sekali saja dari pendapat tokoh-tokoh dari berbagai kalangan dan golongan, tentang situasi negara dan bangsa ketika kita semua merayakan Hari Kemerdekaan 17 Agustus yang ke 65. Dapatlah kiranya dikatakan bahwa banyak sekali kalangan dalam masyarakat  yang menganggap bahwa negara dan bangsa kita sebenarnya belum merdeka sepenuhnya. Ada yang berpendapat bahwa tujuan proklamasi 17 Agustus sudah diselewengkan atau dikhianati sejak digulingkannya presiden Sukarno oleh Suharto dan konco-konconya dan didirikannya rejim militer Orde Baru.

 Peran, tanggungjawab, dan dosa Suharto bersama jenderal-jenderalnya, yang sudah menjadikan negara kita menjadi jajahan tipe baru oleh imperialisme atau neo-liberalisme (terutama AS) adalah maha besar dan maha berat.  Suharto-lah bersama konco-konconya dari kalangan militer, Golkar, partai-partai reaksioner yang anti Bung Karno (terutama dari golongan Islam waktu itu) telah membuka pintu lebar-lebar untuk mengundang modal asing besar-besaran sejak tahun 1967. Akibatnya, adalah seperti yang sama-sama kita saksikan dewasa ini di seluruh negeri..

Banyak sekali para pakar, cendekiawan, dan tokoh-tokoh yang mengatakan  bahwa hampir semua sumber daya alam dan aset bangsa dikuasai oleh asing. Yang tersisa adalah kemiskinan dan kita jadi penonton di negeri sendiri. Jelaslah sekali  bahwa keadaan negara dan bangsa yang seperti sekarang ini sama sekali bukanlah yang dinginkan oleh Bung Karno dan bukan pula yang dicita-citakan oleh proklamasi 17 Agustus 45.



Sosok Bung Karno sebagai pemimpin besar bangsa menonjol kembali

Yang layak untuk kita perhatikan bersama yalah bahwa dalam peringatan hari kemerdekaan kali ini, sosok agung Bung Karno menjadi menonjol kembali sebagai proklamator (bersama Bung Hatta) , dan sebagai pemimpin besar rakyat Indonesia. Sebaliknya, nama atau sosok Suharto, yang selama 32 tahun pernah disanjung-sanjung setinggi langit,  boleh dikatakan tidak terdengar sama sekali pada kesempatan yang sepenting itu.

Dan perkembangan semacam itu bisalah dimengerti dan wajar pula. Sebab, kelihatannya, sekarang makin banyak orang yang lebih mengenal perbedaan antara Bung Karno Suharto, yang boleh digambarkan sebagai perbedaan antara gunung Himalaya dengan secuwil kecil batu kerikil yang bernama Suharto. Atau Bung Karno sebagai raksasa sedangkan Suharto sebagai makluk kerdil yang kecil sekali.

Sekarang menjadi makin jelas juga bagi banyak orang bahwa Suharto adalah pengkhianat besar terhadap Bung Karno, pengkhianat besar terhadap revolusi rakyat yang dipimpin oleh Bung Karno, pengkhianat besar terhadap seluruh kekuatan revolusioner Indonesia, pengkhianat besar terhadap  jiwa yang asli Pancasila, pengkhianat besar  terhadap dasar-dasar Bhinneka Tunggal Ika (ma’af, para pembaca, atas pemakaian kata « pengkhianat besar » yang berkali-kali, namun begitu itulah hakekat yang sebenarnya !)

Kejahatan, kesalahan besar, dan dosa berat Suharto terhadap itu semuanya, merupakan aib bangsa Indonesia, yang tidak bisa  - dan tidak boleh sama sekali  -- dihilangkan dari sejarah bangsa, dan perlu diketahui dan diingat-ingat terus-menerus oleh kita semua, termasuk anak-cucu kita di kemudian hari.



Pengkhianatan Suharto terhadap Bung Karno melumpuhkan revolusi rakyat

Sebab, pengkhianatan Suharto (bersama-sama konco-konconya di dalam dan luar negeri) telah merusak, melumpuhkan, atau membunuh jiwa perjuangan revolusioner rakyat Indonesia yang telah dipupuk, dikobarkan dan dipimpin oleh Bung Karno selama puluhan tahun. Pengkhianatan Suharto terhadap Bung Karno telah menyebabkan lumpuhnya atau berhentinya (walaupun untuk sementara) revolusi Rakyat Indonesia, sehingga keadaan negara dan bangsa Indonesia menjadi serba semrawut, serba membusuk, dan serba rusak seperti yang kita saksikan sekarang ini.

Jelaslah bagi kita semua bahwa keadaan yang begini menyusahkan atau memprihatinkan sebagian terbesar rakyat kita ini tidak bisa dan tidak boleh kita biarkan berlangsung terus-menerus. Sebab, sudah terlalu lama, sudah lebih dari 45 tahun,  rakyat kita tetap hidup sengsara, sedangkan segolongan kecil  sekali dari bangsa kita hidup mewah, dan bermandikan harta haram dari berbagai macam kejahatan.



Usul untuk memperpanjang jabatan SBY sebagai presiden

Apalagi, (sekali lagi : apalagi ! ) sekarang ada seorang politikus DPR  dari Partai Demokrat (Ruhut Sitompul) yang mengusulkan supaya masa jabatan SBY sebagai presiden bisa diperpanjang sampai tiga masa jabatan. Kalau usul ini diterima oleh lembaga-lembaga yang bersangkutan, maka itu berarti bahwa kekuasaan SBY yang sekarang saja sudah mendapat kecaman-kecaman pedas dari banyak fihak, akan terus mengangkangi kursi kepala negara selama 15 tahun !!! Kalau begitu, maka harapan akan terjadinya perubahan-perubahan besar dan mendasar akan amblas sama sekali. Nantinya, kita akan sama-sama menyaksikan bahwa usul semacam ini akan berbuntut panjang dan menuai perlawanan yang besar dari banyak fihak.

Di samping itu semua, pakar atau ahli atau tokoh yang mana sajakah yang masih bisa diharapkan mengatasi kebobrokan yang sudah begitu meluas dan menyembuhkan penyakit-penyakit yang begitu parah, yang sudah puluhan tahun menyerang rakyat kita ? Pemerintah atau partai-partai manakah yang selama ini sudah memerintah bertahun-tahun masih bisa terus diharapkan lagi akan berbuat sesuatu untuk mengadakan perubahan besar-besaran dan fundamental ? Program baru  apa sajakah atau rencana baru macam  apakah atau sistem bagaimanakah  yang bisa diajukan oleh orang-orang yang  selama ini sudah bekerja untuk berbagai pemerintahan sejak Orde Baru sampai sekarang ?

Tidak mungkin ada perubahan besar dan fundamental di negeri kita selama sistem pemerintahan dan segala sistem yang berkaitan dengan sosial ekonomi masih tetap berbau-bau sisa-sisa Orde Baru, dan dan masih tetap dikeloni oleh kekuatan neo-liberalisme. Dan keadaan yang menyedihkan bagi kepentingan rakyat itu bisa masih bisa berlangsung  terus-menerus, entah sampai kapan. Segala macam jalan dan cara yang sudah ditempuh selama 45 tahun sejak masa Orde Baru sampai sekarang sudah menunjukkan kegagalan besar.



Perlu ditempun jalan lain untuk merubah keadaan

Sekarang makin jelaslah kiranya bahwa untuk bisa mengadakan perubahan-perubahan besar dan fundametal perlu ditempuh jalan lain, sebab semua jalan lama sejak ORDE Baru adalah jalan buntu untuk mencapai tujuan masyarakat adil dan  makmur yang dicita-citakan proklamasi 17 Agustus 45 .

Jalan lain itu adalah jalan revolusi rakyat, yang sudah pernah digerakkan dan dikobarkan di bawah pimpinan Pemimpin Besar Revolusi (PBR) Bung Karno  dengan gemilang dan mendapat dukungan besar dari seluruh kekuatan revolusioner di Indonesia pada waktu itu. Revolusi rakyat Indonesia yang pernah menjadi kebanggaan rakyat Indonesia, dan rakyat-rakyat Asia-Afrika serta rakyat progresif sedunia inilah yang telah dilumpuhkan oleh Suharto beserta konco-konconya di dalam negeri dan luar negeri.

Jalan revolusi rakyat yang ditunjukkan Bung Karno adalah jalan untuk menjebol secara besar-besaran segala hal yang tidak menguntungkan rakyat dan menggantikanya dengan yang baru yang menguntungkan rakyat. Revolusi rakyat menurut ajaran-ajaran revolusioner Bung Karno adalah serentetan destruksi untuk digantikan dengan konstruksi, atau penjebolan dan diganti dengan pembangunan, dalam segala hal yang berkaitan dengan bangsa dan negara.

Sekali lagi, dan unruk mengulangi kesekian kalinya, mengingat banyaknya hal-hal yang perlu dijebol dan kemudian dibangun (destruksi dan konstruksi) yang terdapat dalam situasi negara dan bangsa kita dewasa ini,  maka tidak mungkin dipakai jalan lain kecuali jalan meneruskan revolusi rakyat yang sudah pernah dilakukan oleh rakyat Indonesia di bawah pimpinan Bung Karno.

Untuk itu, seluruh kekuatan demokratik di Indonesia, atau seluruh kekuatan progresif, yang menginginkan tercapainya masyarakat adil dan makmur, atau semua kalangan dan golongan yang melihat adanya kebenaran-kebenaran dalam ajaran-aajaran revolusioner Bung Karno, perlu bersama-sama mengusahakan tergalangnya kekuatan untuk meneruskan revolusi rakyat.

Bermacam-macam kegiatan dan perjuangan perlu dilakukan dan berbagai cara dan jalan perlu dicari untuk ditempuh guna menyusun atau membangun kekuatan revolusi rakyat ini, yang bisa dilakukan oleh semua kalangan dan golongan  yang menginginkan adanya perubahan besar dan fundamental di negeri kita.



Membangun kekuatan rakyat untuk menggerakkan revolusi

Dari aksi-aksi atau kegiatan revolusioner yang bisa dilakukan oleh sebanyak mungkin orang dari berbagai kalangan yang manapun, maka akan bisa didapat berbagai pangalaman yang berharga, penting dan tepat untuk memilih pimpinan gerakan masing-masing beserta para kader atau aktivis yang diperlukan. Kegiatan atau perjuangan revolusioner  -- yang biasanya makan waktu lama dan melalui proses yang tidak mudah juga -- adalah ibu yang akan melahirkan pimpinan  serta para kader untuk revolusi rakyat, sesuai dengan ajaran-ajaran revolusioner Bung Karno.

Mengingat besarnya pekerjaan penjebolan dan pembangunan terhadap apa-apa yang lama yang sudah busuk dan penyakitan (antara lain sisa-sisa Orde Baru) maka dalam penyusunan persiapan revolusi rakyat ini  perlu diutamakan tempat bagi kalangan progresif dan revolusioner dari golongan generasi muda (umpamanya mahasiswa, pemuda dan pemudi) , kaum buruh, kaum tani, kaum wanita, dan golongan rakyat miskin lainnya pada umumnya.

Gerakan atau perjuangan penjebolan dan pembangunan (atau revolusi rakyat) menurut ajaran-ajaran revolusioner Bung Karno   -- secara pelan-pelan dan bertahap – akhirnya akan mendapat simpati dan dukungan dari berbagai kalangan dan golongan  dalam masyarakat, seiring dengan makin naiknya penghormatan terhadap kewibawaan integritas moral dan luhurnya gagasan politik Bung Karno, seperti yang mulai kita saksikan bersama-sama dalam memperingati Hari Kemerdekaan yang ke 65 baru-baru ini.

Itu semua menujukkan arah bahwa jalan revolusi rakyat, menurut ajaran-ajaran Pemimpin Besar Revolusi (PBR) Bung Karno,  pada akhirnya akan ditemukan kembali dan diteruskan oleh rakyat Indonesia,
