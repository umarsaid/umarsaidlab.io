---
layout: post
title: 'Arti "kemenangan" gugatan Suharto terhadap TIME'
date: 2007-09-13
---

Dimenangkannya gugatan Suharto terhadap majalah TIME oleh Mahkamah Agung baru-baru ini, yang merupakan kejutan bagi banyak kalangan telah mulai mendapat banyak reaksi dari berbagai fihak. Dan kiranya, sudah dapat diperkirakan bahwa kasus ini akan berbuntut amat panjang, dan akan menjadi masalah yang menarik perhatian besar sekali di Indonesia maupun di luarnegeri. Sebab, berbagai masalah Suharto (dan keluarganya) memang sudah lama menjadi persoalan yang dianggap serius oleh opini publik.

Adalah wajar kalau ada berbagai pendapat atau reaksi dari banyak fihak terhadap dimenangkannya gugatan Suharto oleh Mahkamah Agung yang memerintahkan kepada majalah TIME untuk membayar ganti kerugian sebesar 1 triliun Rupiah (129,6 juta US$). Majalah TIME telah dianggap telah mencemarkan “nama baik” Suharto, karena telah menyiarkan pada tanggal 24 Mei 1999 suatu laporan panjang mengenai harta kekayaan keluarga Suharto beserta jaring-jaringannya yang diduga berasal dari hasil korupsi.

Sebagian dari banyak reaksi atau pendapat dari berbagai kalangan itu dapat dibaca dalam “Kumpulan berita tentang gugatan Suharto lawan TIME”. Untuk dapat mengikuti dan mendapat gambaran yang agak lengkap mengenai kasus ini, diharapkan kepada para pembaca, sudilah kiranya untuk sering-sering menyimaknya.

Dari yang sudah diungkap oleh berbagai fihak itu nyatalah sekali bahwa dimenangkannya oleh Mahkamah Agung gugatan Suharto terhadap majalah TIME ini mengandung banyak masalah yang patut sekali dipertanyakan, dan banyak pula hal-hal yang perlu ditelaah bersama-sama atau dipersoalkan. Dalam tulisan yang kali ini, diusahakan untuk disajikan sebagian dari pandangan terhadap kasus yang penting ini, yang dicoba dilihat dari berbagai segi atau sudut pandang.

Siapa-siapa majelis hakim yang membela Suharto

Patutlah agaknya kita renungkan bersama mengapa ada berbagai reaksi keras dari banyak kalangan di Indonesia terhadap putusan Mahkamah Agung yang memenangkan Suharto dalam gugatannya terhadap majalah TIME. Sudah tentu, ada berbagai alasan atau sebab. Tetapi yang menonjol di antaranya adalah bahwa putusan MA yang demikian itu dianggap tidak sesuai dengan rasa keadilan masyarakat luas.

TIME telah didakwa telah mencemarkan nama baik Suharto, karena telah membuat laporan panjang berjucul “Suharto Inc”, yang membeberkan dengan jelas dan rinci jaringan bisnis serta kekayaan keluarga Suharto, yang diduga berasal dari hasil korupsi atau penyalahgunaan kekuasaan selama ia menjabat sebagai presiden.

Para hakim majelis kasasi MA menganggap pemberitaan TIME 24 Mei 1999 yang tersiar luas itu “melampaui batas kepatutan, ketelitian, dan sikap hati-hati, sehingga menyebabkan perbuatan melawan hukum yang mencemarkan nama baik penggugat (maksudnya: Suharto) sebagai jenderal besar TNI dan mantan presiden RI “ (dari Suara Merdeka,11 September .2007)

Patut dicatat di sini bahwa majelis hakim agung yang memenangkan gugatan Suharto itu diketuai oleh Mayjen TNI (Pur) German Hoediarto (Ketua Muda MA Bidang Pengadilan Militer) dengan anggota M. Taufik dan Bahaudin Qaudry. Mungkin karena ada kalangan yang mempersoalkan ditunjuknya Ketua Muda MA Bidang Pengadilan Militer sebagai Ketua Majelis Hakim Kasasi yang memeriksa gugatan Suharto itulah makanya Ketua MA, Bagir Manan, mengatakan bahwa ia “sudah memilih hakim yang tepat, walau ia memiliki latar belakang karier di bidang militer”. Barangkali, masalah penunjukan Mayjen TNI (Pur) German Hoediarto ini masih akan menimbulkan berbagai persoalan atau pertanyaan di kemudian hari.

Hukuman yang dijatuhkan kepada TIME

Soeharto menggugat 7 pihak dari Time Asia yakni Time Inc, editor Time Donald Marison, John Colmay, Davit Liephold, Lisa Rose Weaver, Zamira Lubis, dan Jason Tejasukmana. Hakim kasasi MA menghukum mereka (tergugat 1 sampai 7) secara tanggung renteng membayar kerugian imateril sebesar Rp 1 triliun. Selain membayar kerugian imateril, para tergugat juga harus mengajukan permintaan maaf secara terbuka di lima media cetak nasional, majalah Time di seluruh dunia, dan lima majalah terbesar di Indonesia dalam tiga kali penerbitan secara berturut-turut. (dari Suara Merdeka 12 September 2007)

Hukuman terhadap TIME yang seperti tersebut di atas ini, merupakan hukuman yang skalanya belum pernah terjadi dalam sejarah pers, dan yang sudah jelas tidak akan diterima begitu saja oleh TIME. Karenanya, sudah dapat diperkirakan bahwa fihak TIME akan melakukan perlawanan, yang effeknya atau gemanya akan luas sekali di dunia internasional. Sebagai akibatnya, putusan para hakim agung tersebut di atas akan membikin Mahkamah Agung Republik Indonesia menjadi sorotan banyak fihak, baik di Indonesia maupun di luarnegeri.

Banyak kalangan di Indonesia, termasuk kalangan pers, yang sudah memberikan reaksi yang keras terhadap putusan Mahkamah Agung. Di antara reaksi itu ada yang mengartikan putusan itu sebagai bahaya yang bisa mengancam kebebasan pers, bahkan menyebabkan kematian usaha penerbitan pers. Di antara reaksi-reaksi itu ada yang mempersoalkan besarnya hukuman yang sampai 1 triliun Rupiah yang harus dibayar TIME kepada Suharto. Ada yang mengatakan bahwa putusan Mahkamah Agung semacam itu mengandung ciri-ciri fasisme, yang mengancam kehidupan pers.

Sumbangan besar majalah TIME

Segi lain yang juga layak untuk sama-sama direnungkan atau dipersoalkan ialah anggapan para hakim majelis kasasi MA bahwa pemberitaan TIME yang menyajikan bahan-bahan mengenai harta kekayaan Suharto itu “melampau batas kepatutan, ketelitian, dan sikap hati-hati”. Sebab, dalam laporan tentang harta kekayaan Suharto -- yang diduga berasal dari korupsi atau penyalahgunaan kekuasaan -- apa sajakah dan bagaimanakah “batas kepatutan” yang dimaksudkan oleh majelis hakim itu. ?

Dan ketika membaca bahwa laporan TIME itu “melampaui batas ketelitian dan sikap hati-hati” orang pun bisa mengingatkan para hakim bahwa laporan itu sudah dibuat oleh orang-orang yang keprofessionalannya cukup tinggi, dan sudah mengumpulkan bahan-bahan selama 4 bulan di 11 negeri. Mereka pun sudah berusaha menge-cek informasi-informasi yang mereka peroleh dengan sumber-sumber yang terpercaya.

Walaupun, katakanlah, di sana-sini bisa saja ada data atau informasi yang tidak benar atau kurang tepat, tetapi bisalah kiranya dikatakan bahwa tujuan yang mau dicapai oleh laporan itu tetap bisa dibenarkan, yaitu : membongkar berbagai kejahatan Suharto yang berupa korupsi besar-besaran dan berbagai penyalahgunaan. Dan jelaslah bahwa tujuan yang demikian itu sesuai dengan harapan sebagian besar rakyat Indonesia, yang sudah dimanifestasikan oleh berbagai golongan (terutama oleh generasi muda sejak sebelum jatuhnya Suharto) dan oleh keputusan MPR.

Dari sudut pandang inilah kita bisa memandang bahwa laporan majalah TIME itu telah berjasa dan memberi sumbangan besar sekali kepada perjuangan berbagai kalangan di Indonesia yang menuntut diadilinya Suharto. Laporan majalah TIME iru merupakan salah satu di antara tulisan-tulisan yang secara lengkap membongkar kejahatan korupsi Suharto, di samping tulisan-tulisan George Aditjondro yang juga dengan berani sekali telah mengekspose berbagai praktek buruk Suharto beserta keluarganya.

“Nama baik” Suharto sudah tercemar sejak lama

Di antara berbagai persoalan yang berkaitan dengan dimenangkannya Suharto lawan TIME adalah anggapan para hakim MA bahwa majalah tersebut telah bersalah “mencemarkan nama baik Suharto”. Karena, sebenarnya, sejak lama “nama baik”-nya Suharto sudah tercemar, baik di Indonesia maupun di luarnegeri. Sejak jauh sebelum dijatuhkan dari kedudukannya sebagai Presiden dalam tahun 1998, nama Suharto sudah penuh dengan kecemaran, baik karena kejahatan-kejahatannya di bidang politik dan HAM, maupun karena korupsi, kolusi dan nepotisme (KKN) yang dilakukannya secara besar-besaran dan dalam jangka lama pula.

Contohnya, kalau kita buka Google lewat Internet, dan kita ketik kata kunci “Suharto –korupsi” (dalam Google versi Indonesia) maka akan tersedia macam-macam bahan tentang korupsi keluarga Suharto sebanyak 37.500 halaman.. Kalau diketik dalam Google versi Inggris kata-kunci “Suharto – corruption” maka tersedia berbagai bahan dalam 249.000 halaman !!! Dengan menyimak bahan-bahan yang bisa banyak didapat dari Google, maka jelas sekalilah bagi siapa pun bahwa “nama baik” Suharto sudah sangat tercemar di dunia internasional, dan sejak lama sekali.

Jadi, mengingat itu semua bisalah kiranya disimpulkan bahwa dakwaan bahwa TIME sudah mencemarkan nama baik Suharto adalah keliru sama sekali. TIME hanya memperkuat atau menambah informasi tentang kejahatan Suharto yang sudah dibeberkan oleh banyak kalangan, dan berbagai media, baik di Indonesia maupun di banyak negeri di dunia. Dan perlu dicatat di sini, bahwa Suharto adalah satu-satunya presiden di dunia yang paling banyak diberitakan korupsinya. Dalam hal yang satu ini, dialah yang pemegang rekord dunia.

Aksi-aksi rakyat dan keputusan MPR : adili Suharto!

Untuk lebih menjelaskan arti penting laporan TIME tentang kasus Suharto, kiranya perlu kita semua (termasuk para hakim MA) ingat bahwa justru karena kejahatan-kejahatannya di bidang politik dan HAM dan juga KKN yang sudah keterlaluan itulah makanya Suharto telah “dicampakkan”, atau “dibuang” atau “dipinggirkan” oleh gerakan besar-besaran secara nasional dari generasi muda, dengan dukungan simpati dari rakyat banyak. Juga, bahwa karena korupsinya yang merajalela itu pulalah maka MPR sudah membuat keputusan yang memerintahkan pemeriksaan terhadap Suharto.

Dan perlulah juga kita ingat bersama bahwa sejak “lengsernya” Suharto dari kursi kepresidenan, gerakan atau aksi-aksi yang dilakukan oleh berbagai golongan dalam masyarakat untuk menuntut dengan keras diadilinya mantan dedengkot Orde Baru ini tetap terus-menerus berlangsung, sampai sekarang !

Jadi, singkatnya, sekali lagi, “nama baik” Suharto beserta keluarganya sudah tercemar sejak lama, bukan hanya di Indonesia saja, melainkan juga di luarnegeri.. Mengingat itu semua, kalau ada orang atau kalangan yang masih berani bicara tentang “nama baik Suharto” maka patutlah kiranya dipertanyakan kejujuran fikirannya, atau dipersoalkan kebersihan hati nuraninya, atau, bahkan, diragukan kewarasan nalarnya (ma’af, kalau kata-kata ini dianggap terlalu kasar). Hanya para pendukung setia rejim militer Orde Barulah yang masih mau dan berani berbicara begitu.

Perlu adanya hakim yang jujur dan adil
Memang, sampai sekarang Suharto belum pernah bisa diperiksa oleh pengadilan apakah ia sudah benar-benar telah bersalah atau tidak bersalah melakukan berbagai kejahatan dan korupsi. Ini disebabkan berbagai rekayasa atau manipulasi dalih (antara lain : “masalah kesehatan” atau kondisi fisik) sehingga ia tidak bisa – atau belum bisa -- diajukan di depan pengadilan. Ini pulalah yang menunjukkan berbagai kelemahan sistem hukum dan peradilan di Indonesia, yang diperkuat oleh membusuknya fikiran para simpatisan Suharto.

Sebenarnya, hanya pengadilan yang benar-benar independen, atau yang sungguh-sungguh menjalankan tugasnya dengan seadil-adilnya, yang dapat menilai atau menentukan kesalahan atau ketidaksalahan Suharto. Untuk itu memang dibutuhkan adanya hakim-hakim, jaksa-jaksa, dan pengacara-pengacara yang jujur, berani, tulus hati, dan benar-benar menjunjung tinggi-tinggi keadilan dan kebenaran. Dan bukannya orang-orang yang bisa “dibeli”, atau ditakut-takuti, atau dipengaruhi oleh fihak-fihak yang meremehkan keadilan atau membungkam kebenaran.

Dan, kita tidak tahu dengan pasti, apakah hakim-hakim agung di Mahkamah Agung itu terdiri dari orang-orang yang benar-benar mempunyai kejujuran dalam menunaikan tugas mereka. Tetapi, dengan dimenangkannya gugatan Suharto terhadap majalah TIME, maka kita bisa mengukur sikap para hakim yang memeriksa perkaranya, apakah mereka sungguh-sungguh menjunjung perasaan keadilan dan kebenaran, atau tidak.

Bukti dan indikasi korupsi Suharto sudah banyak

Selama Suharto tidak bisa diajukan di depan pengadilan karena adanya berbagai dalih dan dalil (yang palsu), atau karena adanya konspirasi (kasarnya, kongkalikong) di kalangan pemegang kekuasaan di bidang eksekutif, legislatif dan yudikatif, maka opini publik akan tetap terus mempunyai anggapan bahwa Suharto memang bersalah karena berbagai kejahatan, termasuk juga dalam hal korupsi.

Sebab, bukti-bukti atau indikasi yang menunjukkan adanya korupsi besar-besaran yang sudah dilakukannya selama puluhan tahun kekuasaannya itu sudah terlalu banyak dilihat oleh banyak kalangan (termasuk yang disaksikan sendiri oleh kalangan dekatnya), dan sudah banyak pula dibeberkan di pers Indonesia dan luarnegeri. Masalah yang menyangkut yayasan-yayasan Suharto dan simpanan uang di bank BNP (di Inggris) hanyalah sebagian kecil saja dari kasus kejahatan keluarga Suharto.

Sebenarnya, Suharto (beserta keluarganya) sudah terkena sanksi moral atau mendapat sanksi sosial yang berat (dan sudah selayaknya !) dari opini publik, setelah mengetahui dosa-dosa besarnya selama ini. Jadi, keputusan Mahkamah Agung, yang menuding bahwa majalah Time “mencemarkan nama baik” Suharto bisa diartikan sebagai sikap yang bertentangan sama sekali dengan opini publik, baik nasional maupun internasional.

Kemenangan Suharto tidak bisa dibangga-banggakan

Oleh karena masalah korupsi Suharto (dan keluarganya) sudah menjadi masalah besar yang dipersoalkan masyarakat luas, dan yang mengharapkan adanya tindakan hukum untuk mengadilinya, maka wajar kalau ada reaksi yang keras dari berbagai kalangan karena kecewa dengan keputusan MA tersebut.

Oleh karena itu, kita bisa memandang bahwa kemenangan yang “dihadiahkan” oleh Mahkamah Agung kepada Suharto bukanlah kemenangan yang bisa dibangga-banggakan oleh rakyat Indonesia, atau disambut dengan gembira, melainkan sebaliknya, harus diprihatinkan atau disesalkan oleh banyak orang yang mendambakan keadilan dan menjunjung tinggi-tinggi kebenaran.

Kemenangan Suharto atas majalah TIME sama sekali bukanlah kemenangan nasionalisme atau patriotisme Indonesia lawan arogansi kepentingan asing, atau lawan alat imperialisme AS. Dengan kalimat lain, bisalah dikatakan “kemenangan” Suharto ini sama sekali bukannya kebanggaan bangsa, melainkan sebaliknya, aib bangsa yang memalukan !

Kemenangan Suharto lawan TIME berati kemenangan (sementara) fikiran yang menginjak-injak perasaan keadilan, yang bersemayam di kepala dan hati para hakim di MA yang mengadili kasus gugatan Suharto ini. Dimenangkannya Suharto atas TIME akan lebih merusak lebih parah lagi citra dunia peradilan di Indonesia yang memang sudah terkenal rusak, atau bobrok atau busuk. Sayang, seribu kali sayang, bahwa lembaga peradilan tertinggi negara kita sudah begitu merosot citranya dengan memenangkan gugatan Suharto.

Merosotnya citra Mahkamah Agung
Kalau kita renungkan segi-segi lainnya dengan dalam-dalam, maka nyatalah bahwa dimenangkannya Suharto dalam kasus ini akan membikin makin terbongkarnya kebusukan dan kebobrokan citra keluarga Suharto, di samping makin merosotnya -- secara dalam-dalam pula -- citra Mahkamah Agung Republik Indonesia. Dan merosotnya citra Mahkamah Agung ini bisa mengakibatkan jatuhnya juga citra penegakan hukum di Indonesia secara keseluruhan.

Dapat diduga sejak sekarang, bahwa kasus dimenangkannya Suharto ini akan berbuntut panjang, dan gemanya akan memantul di banyak tempat di dunia. Karena, sudah dapat diperkirakan bahwa majalah TIME akan membalasnya dengan perlawanan yang setimpal. Dan, akan kita sama-sama saksikan bahwa “kemenangan” yang diperoleh Suharto dari Mahkamah Agung ini akhirnya akan bisa berubah nantinya menjadi kekalahannya secara moral, karena adanya perlawanan dari seluruh gerakan anti-korupsi dan anti-kejahatan HAM, baik yang di Indonesia maupun yang di luarnegeri.

Secara keseluruhan dapatlah kiranya disimpulkan bahwa maksud para hakim di Mahkamah Agung untuk membela kehormatan atau nama baik Suharto malahan berakibat membikin lebih hancurnya citranya dan kehormatan atau nama baiknya, yang memang sudah lama membusuk itu. Karena, sekali lagi, patutlah sama-sama kita ingat bahwa sejak jauh sebelum dijatuhkan dari kedudukannya sebagai Presiden dalam tahun 1998, nama Suharto sudah penuh dengan kecemaran, baik karena kejahatan-kejahatannya di bidang politik dan HAM, maupun karena korupsi,kolusi dan nepotisme (KKN) yang dilakukannya secara besar-besaran dan dalam jangka lama pula.

Kita masih ingat, dan para hakim agung di Mahkamah Agung pun semestinya juga masih ingat, bahwa justru karena kejahatan-kejahatannya di bidang politik dan HAM dan juga KKN yang sudah keterlaluan itu semualah makanya Suharto telah “dicampakkan”, atau “dibuang” atau “dipinggirkan” oleh gerakan besar-besaran secara nasional dari generasi muda, dengan dukungan simpati dari rakyat banyak.

Untuk kesekian kalinya, perlu juga diulangi di sini, bahwa juga karena korupsinya yang merajalela itu pulalah maka MPR sudah membuat keputusan yang memerintahkan pemeriksaan terhadap Suharto. Dan sejak “lengsernya” Suharto dari kursi kepresidenan, gerakan atau aksi-aksi yang dilakukan oleh berbagai golongan dalam masyarakat untuk menuntut diadilinya mantan dedengkot Orde Baru ini masih juga terus-menerus berlangsung, dengan tetap menggebu-gebu, sampai sekarang !
