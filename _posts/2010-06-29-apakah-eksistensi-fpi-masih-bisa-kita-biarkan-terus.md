---
layout: post
title: Apakah eksistensi FPI masih bisa kita biarkan terus ?
date: 2010-06-29
---

Peristiwa pembubaran secara paksa oleh FPI pertemuan para anggota DPR (dari Komisi IX, bidang Kesehatan dan Tenaga Kerja) yang dipimpin Dr Ribka Tjiptaning dengan berbagai elemen masyarakat di Banyuwangi baru-baru ini ternyata berbuntut panjang. Dr Ribka Tjiptaning telah mengadukan pembubaran sejara paksa --  yang disertai kekerasan dengan berbagai ancaman – kepada Komnas HAM dan Mabes Polri. Menurut Dr. Ribka Tjiptaning, pengaduan ini merupakan pengaduan pribadi sebagai warga negara. Meski begitu, Dewan Pimpinan Pusat PDI Perjuangan juga mendukung langkah Dr. Ribka. PDI Perjuangan akan mengusulkan kepada Komisi Hukum DPR untuk memanggil Kepala Kepolisian RI. "DPR sebagai institusi seharusnya merasa dilecehkan," kata Dr. Ribka.



Kalau dilihat dari berbagai segi, maka jelaslah bahwa serentetan langkah-langkah itu merupakan tindakan penting sekali untuk mengangkat masalah terror dari segolongan kecil dan fanatik di kalangan  Islam ini untuk menjadi  pembahasan seluas-luasnya di kalangan bangsa kita. Sebab, apa yang dilakukan oleh FPI Banyuwangi dalam menterror, mengancam dengan kekerasan, dan membubarkan pertemuan para anggota DPR ini dengan berbagai elemen masyarakat (sekitar 300 orang) di satu rumah makan di Banyuwangi adalah tindakan yang terang-terangan   -- dan secara kasar sekali, bahkan buas --  melanggar HAM, merusak demokrasi, bertentangan dengan segala undang-undang, dan juga  melecehkan Pancasila serta meludahi Bhinneka Tunggal Ika.



Karena seriusnya ancaman atau bahaya yang  terkandung dalam peristiwa FPI Banyuwangi terhadap kehidupan bangsa kita, maka sudah sepatutnya  -  dan bahkan seharusnya ! – bahwa masalah-masalah yang berkaitan dengan peristiwa terror FPI Banyuwangi ini dihadapi dengan serius oleh kita semua. Sebab, peristiwa FPI Banyuwangi ini bukanlah soal kecil, yang boleh kita anggap remeh begitu saja ;



Sekilas tentang peristiwa terror FPI

Seperti yang sudah pernah diberitakan,  pada tanggal 24 Juni FPI Banyuwangi dan sejumlah organisasi Islam lainnya (Forum Umat Beragama dan LSM Gerak) membubarkan secara paksa pertemuan acara sosialisasi kesehatan gratis oleh Komisi IX DPR yang diselenggarakan oleh suatu panitia yang terdiri  dari wakil-wakil PDI-P dan sejumlah organisasi masyarakat seperti Perpeni dan LSM Layar Ku Mendung dan dihadiri oleh sejumlah eks-tapol.  FPI menuduh acara tersebut merupakan « ajang temu kangen » mantan anggota Partai Komunis Indonesia.

 Acara tersebut dihadiri juga oleh dr Ribka Tjiptaning (Ketua Komisi IX DPR RI dan Ketua DPP PDI Bidang Kesehatan dan Tenaga Kerja), Rieke Dyah Pitaloka (anggota Komisi IX Fraksi PDIP), Nursuhud (anggota Komisi IX DPR RI, Fraksi PDIP).



 Massa FPI dan ormas Islam lainnya memaksa acara tersebut dibubarkan dengan teriakan dan acungan senjata tongkat bambu. Mereka berteriak, "Komunis ayo bubar dan keluar”. Walau banyak aparat polisi berpakaian preman mereka diam saja. Bahkan dua orang panitia dipanggil Kapolres dan memaksa mereka agar meminta Ribka Tjiptaning keluar dari acara tersebut.

 Ketiga anggota Komisi IX DPR RI dan peserta yang jumlahnya 300 orang membubarkan diri karena  merasa terancam keselamatan dan jiwanya. Tindakan massa tersebut sangat intimidatif. Bila  tidak dituruti pasti terjadi tindak kekerasan. Bahkan Dr Ribka Tjiptaning harus dilarikan ke Kantor Cab. PDIP Banyuwangi, karena massa terus mengejar (Dikutip dari berbagai sumber)

FPI mengkhianati HAM, Pancasila, Gus Dur dan Bung Karno.

Untuk mengetahui lebih banyak berbagai sikap FPI Banyuwangi tentang pertemuan itu di bawah ini disajikan kutipan-kutipan dari berita Antara,  yang antara lain sebagai berikut :



"Ini ada komunitas anggota PKI (Partai Komunis Indonesia). Kenapa ada di sini?" kata Ketua FPI Banyuwangi, Aman Faturahman, kepada sejumlah peserta pertemuan yang terkejut melihat kehadiran anggota FPI itu. Menurut Ketua FPI Banyuwangi, pertemuan itu merupakan acara temu kangen bekas anggota PKI dan keturunannya, sehingga pertemuan tersebut harus dibubarkan.



"Sosialisasi kesehatan gratis dari Komisi IX hanya sebagai kedok. Saya curiga acara itu merupakan kegiatan terselubung untuk menumbuhkan semangat komunisme lagi karena banyak peserta dari luar Kabupaten Banyuwangi yang datang," kata Aman. Untuk itu, lanjut dia, FPI bersama organisasi masyarakat Islam di Banyuwangi membubarkan acara tersebut untuk menjaga kondusivitas keamanan di kabupaten paling timur Pulau Jawa itu.

"Kami mengantisipasi tumbuhnya bibit PKI baru karena gerakan PKI pada tahun 1965 berawal dari Kabupaten Banyuwangi," katanya menambahkan. (kutipan dari Antara selesai)

Kiranya bagi kita semua yang membaca ucapan-ucapan yang demikian itu jelas sekali bahwa FPI adalah organisasi kecil kalangan Islam yang fanatik, dan cupet atau sesat pandangannya  terhadap berbagai sendi-sendi bangsa dan dasar-dasar negara kita, dan bertentangan sama sekali dengan sikap Gus Dur dan Bung Karno.

Jumlah mereka bisa puluhan juta !

Sikap FPI Banyuwangi seperti yang tercermin dalam ucapan-ucapan tokph-tokohnya seperti tersebut di atas kelihatan sekali sesatnya terhadap sesama waganegara Republik Indonesia, atau sebagai sesama ummat manusia, atau sebagai penganut Islam yang benar dan baik.

Sebab,  apakah para bekas anggota PKI dan keturunannya tidak boleh mengadakan  pertemuan yang berupa « temu kangen » di antara mereka, sehingga harus dibubarkan dengan paksa atau dengan  berbagai ancaman kekerasan oleh FPI (atau organisasi-organisasi Islam lain sejenisnya) ? Dan, lagi pula, mengapa tindakan oleh FPI itu dibiarkan saja oleh polisi Banyuwangi ?

Kalau dituruti cara berfikir yang sesat (dan menyerupai fasisme) dari  orang-orang FPI seperti itu,  maka bangsa dan negara kita betul-betul merupakan bangsa dan negara yang paling biadab dan paling hina di seluruh dunia. Kalau cara berfikir seperti itu  dibiarkan meluas dan dipraktekkan oleh sebagian terbesar bangsa kita, maka akan hancurlah sendi-sendi  kehidupan bangsa kita, antara lain Pancasila, Bhinneka Tunggal Ika, NKRI, dan tujuan proklamasi 17 Agutus 45..

Sebab, bisa kita perkirakan bersama, bahwa jumlah bekas anggota PKI beserta anak keturunannya atau saudara-saudara dekatnya adalah besar sekali. Walaupun sudah jutaan di antara mereka telah dibunuh secara besar-besaran oleh militer di bawah pimpinan Suharto, maka sisanya masih banyak sekali, paling tidak beberapa puluh juta orang (termasuk keturunan mereka)

Ratusan ribu para bekas anggota PKI atau simpatisan PKI dan ormas-ormasnya  (umpamanya SOBSI, BTI, Gerwani, Pemuda Rakyat, HSI, berbagai serikat buruh) telah ditahan berpuluh-puluh tahun tanpa pengadilan dan kemudian diterlantarkan begitu saja berpuluh-puluh tahun  setelah dibebaskan.

Karena sesudah dibebaskan mereka tetap diperlakukan tidak manusiawai oleh rejim Orde Baru (dan diteruskan oleh pemerintahan-pemerintahan pasca Suharto) maka banyak di antara mereka yang  berusaha sendiri dengan sudah payah untuk hidup terus dengan mengalami  perlakuan yang tidak manusiawi dan menghadapi berbagai kesulitan.

Arti pertemuan-pertemuan dan usaha-usaha kolektif

Di antara mereka ada yang mencoba mengatasi berbagai perlakuan yang tidak manusiawi itu dengan mendirikan organisasi-organisasi persaudaraan sesama korban Orde Baru, atau menciptakan bersama-sama berbagai macam usaha  kolektif untuk saling bantu dalam bidang sosial dan ekonomi, atau  kegiatan untuk memperjuangkan  -bersama-sama pula  -- hak-hak mereka  sebagai warganegara Republik Indonesia yang penuh.

Semua kegiatan itu  mempunyai tujuan yang baik sekali bagi kalangan mereka sendiri dan juga baik bagi kehidupan bangsa . Oleh karena itu, walaupun mendapat rintangan atau kesulitan-kesulitan tertentu dari fihak-fihak yang anti-komunis atau pro-Orde Baru, kegiatan-kegiatan para eks-tapol atau para korban Orde Baru itu pada umumnya bisa dilaksanakan terus  sejak jatuhnya rejim militer Suharto.

Berbagai macam kegiatan para eks-tapol atau korban Orde Baru ini yang telah dikembangkan sejak lama lewat berbagai organisasi atau LSM (antara lain Pakorba, YPKP, LPR KROB dan lain-lainnya) merupakan kegiatan yang mendatangkan kebaikan bagi bangsa secara keseluruhan, ketika selama ini pemerintah tidak bisa (atau tidak mau !!!) berbuat sesuatu kepada para korban Orde Baru. Sayang sekali, bahwa apa yang dikerjakan oleh berbagai organisasi para korban Orde Baru ini terbatas sekali, atau kecil sekali, dibandingkan dengan besarnya dan luasnya penderitaan mereka yang sudah berlangsung berpuluh-puluh tahun.

Adalah wajar sekali, bahkan sudah seharusnya,  bahwa untuk bisa mengadakan berbagai kegiatan di atas itu, kebanyakan para eks-tapol atau para korban Orde Baru (di antara mereka ada juga para bekas anggota atau simpatisan PKI) perlu mengadakan rapat-rapat atau pertemuan antara sesama mereka atau dengan berbagai elemen masyarakat lainnya. Dan juga adalah wajar, dan bahkan bagus sekali bahwa di antara mereka ada pertemuan-pertemuan persaudaraan  atau « temu kangen », karena sebagian dari masyarakat selalu masih memusuhi atau mengucilkan  mereka, dan sudah selama berpuluh-puluh tahun pula.Dan pertemuan-pertemuan dengan tujuan seperti tersebut di atas juga dijamin atau dibolehkan oleh konstitusi negara kita.

Bekas anggota PKI pun adalah warganegara seperti lainnya

Jadi  sikap sesat orang-orang FPI yang melarang orang-orang komunis atau eks-PKI dan keturunannya untuk mengadakan pertemuan « temu kangen » , seperti yang diadakan di Banyuwangi adalah sikap yang jelas-jelas melanggar HAM, melecehkan demokrasi, dan tidak manusiawi.  Apakah mentang-mentang mereka eks-PKI maka mereka tidak boleh mengadakan pertemuan, walaupun hanya untuk « temu kangen », atau untuk hal-hal yang berkaitan dengan usaha untuk memperbaiki secara bersama-sama kehidupan sosial-ekonomis mereka. ? Mereka adalah juga warganegara biasa seperti lainnya, bahkan banyak sekali yang juga pemeluk Islam, meskipun berhaluan kiri atau punya simpati kepada komunisme atau sosialisme.

Kalau fikiran sesat atau sikap merusak persatuan bangsa yang dianut oleh FPI ini dibiarkan berkembang maka berarti bahwa puluhan juta bekas anggota PKI atau simpatisan-simpatisannya beserta anak cucu mereka akan kehilangan hak-hak mereka sebagai warganegara RI dan bahkan akan tetap terus diperlakukan sebagai musuh masyarakat.

FPI adalah adalah pada hakekatnya Front Perusak Islam

Jelaslah bahwa sikap FPI yang demikian ini merupakan racun yang membikin rusaknya atau sakitnya kehidupan bangsa. Banyak sekali praktek-praktek yang dilakukan selama ini oleh FPI sudah menunjukkan – dan dengan bukti-bukri yang jelas pula  -- akibat yang hanya menciderai ummat Islam atau mengotori citra Islam di mata banyak orang, termasuk di mata kalangan Islam sendiri (terutama di berbagai kalangan NU dan Muhamadiyah)  Oleh sebab itu ada orang-orang yang karenanya memberikan arti (sebagai cemooh atau ejekan) FPI sebagai  Front Perusak Islam atau Front Penghancur Islam.

Dengan dalih membela Islam kalangan FPI telah melakukan ancaman, dan intimidasi, atau berbagai macam tindakan  kekerasan,  yang bersifat kriminal dan berciri-ciri  premanisme atau hooliganisme (umpamanya sweeping, pengroyokan terhadap suatu golongan yang dituduh kafir, perusakan gereja atau tempat ibadah lainnya) terhadap berbagai kalangan yang mereka anggap bertentangan  dengan Islam. FPI sudah terbukti sebagai organisasi yang tidak menghargai kebebasan mempuyai faham politik atau keyakinan  agama, hak berserikat dan berkumpul, yang dijamin oleh konstitusi yang merupakan hukum dasar dan juga tertinggi negara kita. Bahkan, lebih dari itu, FPI merupakan bahaya bagi negara dan bangsa kita yang .berdasarkan Pancasila dan Bhinneka Tunggal Ika.

Dalam jangka lama di masa lalu, FPI (dan berbagai organisasi Islam lainnya yang sejenis atau sealiran) telah digunakan sebagai alat pimpinan Angkatan Darat di bawah Suharto untuk menghancurkan, memecah belah atau melemahkan lawan-lawan politik Orde Baru, termasuk (bahkan terutama sekali) golongan kiri yang dipimpin Bung Karno dan PKI. Sekarang ini, sisa-sisa kekuatan Orde Baru yang  anti-komunis dan anti Bung Karno itu masih punya hubungan  atau masih terus « main mata » dengan kalangan Islam sejenis FPI ini.

Dengan terjadinya peristiwa aksi pembubaran pertemuan di Banyuwangi baru- baru ini, maka persoalan FPI menjadi pembicaraan lagi di berbagai kalangan ( termauk di DPR dan pemerintahan  dan di kalangan pers serta televisi). Suara-suara yang menuntut supaya ada tindakan terhadap praktek-praktek atau tingkah laku  FPI yang melanggar konstitusi dan HAM makin banyak terdengar   (termasuk dari  kalangan muda NU seperti kalangan  Ulil Abshar Abdullah dan kawan-kawannya).

Berdasarkan pengalaman selama ini, maka makin  jelaslah  sekarang ini bahwa eksistensi FPI (dan organisasi-organisasi.sejenisnya) bukan saja tidak mendatangkan kebaikan bangsa dan negara kita, bahkan sebaliknya ( !!!), mendatangkan berbagai penyakit parah yang  bisa membahayakan kelangsungan pluralitas kehidupan bangsa, yang secara padat dan juga tepat dirumuskan dalam semboyan Bhinneka Tunggal Ika.

Mengingat itu semua, seyogianyalah kita renungkan bersama, apakah untuk selanjutnya masih bisa kita biarkan terus eksistensi FPI (dan organisasi sejenisnya) di tanah air kita tercinta ini ?
