---
layout: post
title: Surat Mahkamah Agung kepada Presiden
date: 2003-07-29
---

Akhir-akhir ini ada perkembangan penting yang menggembirakan bagi puluhan juta keluarga para korban Orde Baru di seluruh Indonesia (terutama para korban peristiwa ’65). Berbagai organisasi yang berkecimpung dalam menangani masalah para korban peristiwa ’65 telah mengadakan sejumlah kegiatan dan aksi bersama. Di antara berbagai tujuan kegiatan bersama itu antara lain adalah : memperjuangkan rehabilitasi, pemulihan hak-hak sipil dan kewarganegaraan secara penuh serta penghapusan segala bentuk diskriminasi atas para korban peristiwa 1965, yang jutaan jumlahnya.

Organisasi yang sudah melakukan kegiatan bersama itu adalah, antara lain :

1. Solidaritas Korban Pelanggaran HAM/SKP-HAM.
2. Paguyuban Korban Orde Baru/PAKORBA.
3. Tim Advokasi untuk Rehabilitasi Ex Anggota TNI/AL
4. Tim Advokasi untuk Rehabilitasi Ex Anggota TNI/AD
5. Tim Advokasi/Rehabilitasi POLRI.
6. Tim Advokasi Persatuan Purnawirawan TNI/AU.
7. Lembaga Perjuangan Rehabilitasi Korban Rezim Orde Baru/LPR-KROB.
8. Lembaga Penelitian Korban Peristiwa 1965/LPKP’65.
9. Yayasan Penelitian Korban Pembunuhan 1965-1966/YPKP ‘65-’66.
10. Tim Advokasi Ex Menteri Kabinet Dwikora Korban Penyalahgunaan Surat Perintah 11 Maret 1966.

Daftar organisasi tersebut di atas menunjukkan betapa luasnya platform bersama yang telah dibangun untuk mengangkat - secara gotong-royong - masalah perikemanusiaan yang sudah terpendam selama puluhan tahun ini. Platform bersama ini merupakan wadah yang bersifat lintas-agama, lintas-suku, lintas-partai, yang mewakili kepentingan para korban ’65 secara umum. Lahirnya kerjasama antara berbagai organisasi ini merupakan langkah kemajuan yang penting dan besar, yang perlu mendapat sambutan hangat dan dukungan dari seluruh para korban peristiwa ’65 beserta keluarga mereka.

Bahkan, bukan hanya para korban peristiwa ’65 saja yang perlu mendukung kegiatan oraganisasi-organisasi tersebut di atas, melainkan juga seluruh bangsa Indonesia. Artinya, masalah ini patut mendapat perhatian dan dukungan dari Presiden, dari DPR, dari lembaga-lembaga negara yang penting-penting lainnya (umpamanya : Mahkamah Agung, Kementerian Kehakiman, Kementerian Dalamnegeri dll), dan masyarakat umum. Mengapa ?

## Mereka Sudah Terlalu Lama Menderita

Masalah para korban ’65 adalah aib bangsa yang harus dihilangkan, dan makin cepat makin baik. Sebab, sekarang sudah makin jelas bagi banyak orang bahwa para korban peristiwa ’65 adalah akibat suatu politik yang salah, yang tidak berperikemanusiaan, yang dijalankan Orde Baru. Politik ini, yang sudah dijalankan lebih dari 32 tahun, tidak boleh diteruskan lebih lama lagi. Membiarkan terus berlangsungnya kesalahan yang amat serius ini tidaklah mendatangkan kebaikan bagi siapa-siapa. Meneruskan politik yang tidak berperikemanusiaan ini adalah melestarikan penyakit di tubuh bangsa. Tidak ada yang bisa dibanggakan dengan meneruskan kejahatan negara ini. Melainkan sebaliknya, membikin citra bangsa yang sudah buruk menjadi lebih buruk lagi.

Marilah sama-sama kita renungkan berbagai hal seperti berikut ini : Kebanyakan dari para eks-tapol dan para korban ’65 adalah orang-orang yang sudah lanjut usia (lebih dari 60 tahun; bahkan banyak sekali yang di atas 70 tahun). Sebagian dari mereka telah dipenjarakan atau “diasingkan” puluhan tahun, tanpa kesalahan apa pun. Selama dalam tahanan mereka umumnya mendapat perlakuan yang tidak sepatutnya. Banyak yang meninggal karenanya, atau fisik menjadi lemah, setelah dibebaskan dari tahanan, sampai sekarang.

Di antara mereka ada pegawai negeri, tentara, polisi, atau pegawai-pegawai perusahaan umum, yang dipecati begitu saja, tanpa mendapat ganti kerugian, atau uang pensiun yang mejadi hak mereka. Mereka kebanyakan hidup dalam kekurangan atau pas-pasan saja, tanpa perawatan kesehatan yang memadai. Sebagian dari mereka sudah uzur, sebagian lagi sudah tidak bisa berbuat apa-apa lagi, kecuali menunggu ajal mereka, dalam keadaan yang sering menyedihkan. Tidak sedikit di antara mereka yang jadi beban moril (dan materiil) para sanak saudara mereka. Karena, mereka masih dikenakan berbagai stigma (cap atau stempel), dan mendapat perlakuan yang menyakitkan hati.

## Rehabilitasi Tidak Merugikan Siapa Pun Juga

Rehabilitasi para korban ’65 adalah suatu hal yang perlu direalisasikan oleh kita semua sebagai bangsa yang beradab. Sebab, rehabilitasi berarti mengkoreksi kesalahan monumental yang pernah dilakukan oleh negara terhadap sejumlah besar orang-orang tidak bersalah, yang dampaknya menyangkut puluhan juta keluarga dekat (dan jauh) mereka. Rehabilitasi para korban ’65 tidak merugikan siapa-siapa, dan tidak membahayakan masyarakat dan negara. Rehabilitasi adalah hak para korban ’65, sebagai manusia dan sebagai warganegara RI.

Kalau direnungkan dalam-dalam, rehabilitasi para korban ’65 bisa diumpamakan menghilangkan duri atau penyakit dari tubuh bangsa, yang sudah berpuluh-puluh tahun ditanam secara salah oleh Orde Baru. Dengan kalimat lain, rehabilitasi para korban ’65 adalah sama dengan menghilangkan aib atau noda bangsa.

Oleh karena itu, segala kegiatan atau aksi-aksi yang sudah dilakukan berbagai organisasi atau kalangan yang berkaitan dengan rehabilitasi para korban ’65 patut disambut dan didukung oleh semua fihak.

## Delegasi Ke DPR

Di antara kegiatan-kegiatan terbaru yang sudah dilakukan berbagai organisasi tersebut di atas adalah mengirimkan (pada tanggal 14 Juli 2003) delegasi untuk menemui Dewan Pimpinan DPR. Delegasi Forum Koordinasi Tim-Tim Advokasi /Lembaga Perjuangan Rehabilitasi Korban Peristiwa 1965 ini (yang diketuai oleh Karim. D.P.) telah diterima oleh beberapa anggota dewan , termasuk Wakil Ketua DPR-RI Soetardjo Soerjoguritno.

Kepada pimpinan DPR, delegasi para korban ’65 menyampaikan bahwa pada tanggal 12 Juni 2003, masing-masing telah menerima tembusan surat dari Mahkamah Agung RI kepada Presiden Republik Indonesia, yang pada pokoknya memberikan pandangan/pendapat hukum/rekomendasi, bahwa dengan dilandasi keinginan untuk memberikan penyelesaian dan kepastian hukum yang dapat memulihkan status dan harkat kewarganegaraan yang sama, serta didorong oleh semangat rekonsiliasi bangsa Indonesia; maka Mahkamah Agung meminta kepada Presiden Republik Indonesia untuk mengambil langkah-langkah konkrit ke arah penyelesaian hukum dan pemberian rehabilitasi umum bagi para korban rezim Orde Baru, khususnya para korban peristiwa ’65.

Delegasi para korban ’65 menegaskan kepada DPR-RI, bahwa yang perlu diberikan rehabilitasi, dikembalikan hak-hak sipil dan kewarganegaraannya secara penuh serta dihapuskannya stigma maupun segala bentuk diskriminasi atas para korban peristiwa ’65, dalam hal ini mencakup :

1. Presiden pertama Republik Indonesia, almarhum Bung Karno.
2. Para menteri serta pembantu utama Bung Karno yang menjadi korban penyalahgunaan Surat Perintah 11 Maret 1966.
3. Kaum nasionalis progresif pendukung Bung Karno.
4. Para anggota Partai Nasional Indonesia, anggota Partindo dan anggota partai non PKI lainnya
5. Para anggota Partai Komunis Indonesia
6. Para cendikiawan, budayawan, sastrawan, seniman, prajurit/anggota TNI, petani, pedagang, buruh, guru, pelajar, pemuda, mahasiswa, serta masyarakat umum di seluruh Indonesia maupun di luar negeri.
7. Keluarga, anak, cucu serta keturunan dari para Korban Peristiwa ’65.

Delegasi para korban ’65 mendesak Dewan Perwakilan Rakyat Republik Indonesia, untuk memberikan dukungan politik kepada Presiden Republik Indonesia dalam mengambil langkah-langkah konkrit guna melaksanakan pandangan/pendapat hukum/rekomendasi Mahkamah Agung Republik Indonesia.

## Perkembangan Penting Dan Menggembirakan

Adalah satu isyarat yang amat penting bahwa organisasi-organisasi yang menangani persoalan para korban ’65 menerima tembusan surat ketua Mahkamah Agung kepada Presiden Megawati mengenai langkah-langkah konkrit ke arah penyelesaian hukum dan pemberian rehabilitasi umum bagi para korban rezim Orde Baru, khususnya para korban peristiwa ’65. Ini menunjukkan bahwa, pada akhirnya, setelah berpuluh-puluh tahun terpendam, masalah korban ’65 menjadi perhatian Mahkamah Agung, sebagai instansi tertinggi kehakiman dan peradilan.

Perkembangan yang menggembirakan keluarga para korban ’65 ini ( juga bagi yang lain-lain) patut mendapat sambutan hangat dan dukungan kuat dari semua fihak, karena merupakan langkah besar atau kemajuan penting dalam bidang hukum. Berbagai bentuk kegiatan dan aksi patut diadakan sebagai sambutan terhadap perkembangan yang baik ini (umpamanya : seminar, ceramah, pengiriman delegasi , surat-surat ke Mahkamah Agung dan ke Presiden). Pertemuan-pertemuan internasional di Indonesia juga bisa diusahakan untuk diselenggarakan.

Hubungan dengan berbagai organisasi internasional atau pengiriman delegasi ke berbagai pertemuan internasional ( seperti yang dilakukan oleh delegasi para korban ’65 yang terdiri dari Dr Tjiptaning, Ir Setiadi dan Heru Atmodjo ke Belanda, Jenewa, Brussel, Paris, Berlin, Stockholm) perlu dikembangkan di kemudian hari.

## Surat Mahkamah Agung Kepada Presiden

Dalam persoalan para korban peristiwa ’65 pengiriman surat Mahkamah Agung yang ditandatangani ketuanya (Bagir Manan) kepada Presiden Megawati merupakan peristiwa yang sangat penting. Karena, ini menunjukkan bahwa Mahkamah Agung menaruh perhatian terhadap masalah yang menyangkut banyak orang ini. Dalam surat itu dinyatakan bahwa : “ Mahkamah Agung banyak menerima surat-surat baik dari perorangan maupun dari berbagai kelompok masyarakat yang menyatakan diri sebagai korban Orde Baru, yang pada pokoknya mengharapkan agar memperoleh rehabilitasi. Dengan dilandasi keinginan untuk memberikan penyelesaian dan kepastian hukum yang sama, serta didorong oleh semangat rekonsiliasi bangsa kita, maka Mahkamah Agung memberikan pendapat dan mengharapkan Presiden untuk mempertimbangkan dan mengambil langkah-langkah konkrit ke arah penyelesaian tuntutan yang sangat diharapkan tersebut » (kutipan selesai).

Surat Mahkamah Agung kepada Presiden Megawati merupakan masukan berupa pandangan dari instansi tertinggi di bidang hukum, yang tentunya sudah menjadi sikap kolektif dari para Hakim Agung. Dalam konteks politik di tanah-air seperti yang dewasa ini, sikap Mahkamah Agung ini mencerminkan keberanian mengedepankan perasaan keadilan terhadap masalah korban peristiwa ’65. Surat tersebut juga secara implisit menyatakan bahwa para korban peristiwa ’65 tidak terbukti bersalah secara hukum dan karenanya harus diberikan rehabilitasi umum oleh Presiden sebagai pemegang hak prerogatif dalam hal pemberian rehabilitasi.

Surat Mahkamah Agung kepada Presiden Megawati mencerminkan sikap yang sejiwa yang telah dimanifestasikan oleh keputusan Pengadilan Negeri Kendal yang memenangkan gugatan terhadap Kodam Diponegoro dalam kasus tanah PT NV Seketjer Wringinsari. Seperti pernah diberitakan, tanah seluas 615 hektar yang dimiliki 97 warga telah dirampas oleh Kodam Diponegoro dengan dalih terlibat G3OS. Sikap Mahkamah Agung ini juga sejalan dengan keputusan Pengadilan Tinggi Tata Usaha Negara Jakarta yang memenangkan gugatan Nani Nurani, seorang eks-tapol yang ditolak haknya mendapat KTP Seumur Hidup.

Meskipun sudah ada kemajuan-kemajuan yang cukup menggembirakan dalam memperjuangkan rehabilitasi para korban ’65, tetapi usaha untuk mendorong Presiden Megawati perlu dikembangkan melalui berbagai jalan. Dalam hal pemberian rehabilitasi tersebut, sesungguhnya Presiden RI sudah dapat memberikan rehabilitasi, karena telah mendapatkan rekomendasi dari Mahkamah Agung.

Jalan masih panjang, tetapi titik terang mulai nampak
