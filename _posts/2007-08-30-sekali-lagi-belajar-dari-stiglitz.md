---
layout: post
title: 'Sekali lagi : belajar dari Stiglitz'
date: 2007-08-30
---

Tulisan tentang kritik tajam Joseph E. Stiglitz, pakar ekonomi Amerika yang terkenal sekali di dunia, terhadap berbagai politik pemerintah Indonesia mengenai modal asing, rupanya mendapat perhatian dari banyak kalangan di Indonesia. Agaknya, perhatian yang besar sekali terhadap kritik Stiglitz ini disebabkan karena masalah penanaman modal asing ini sedang menjadi persoalan hangat yang besar sekali di berbagai kalangan, terutama di kalangan organisasi massa dan para intelektual. Juga, karena kritik tajam ini diucapkan oleh seorang tokoh penting Amerika yang mempunyai bobot yang besar sekali.

Joseph E. Stiglitz adalah professor dalam ekonomi, yang pernah menjabat sebagai penasehat ekonomi terkenal Presiden Bill Clinton, dan dipilih sebagai Wakil Direktur Bank Dunia, serta menduduki jabatan-jabatan penting di berbagai badan ilmiah dan organisasi Amerika dan internasional, yang berkaitan dengan masalah-masalah ekonomi dan permbangunan. Ia telah menulis banyak buku yang berkaitan dengan masalah-masalah ekonomi di dunia, dan telah memperoleh Hadiah Nobel karena keahliannya.

Mengenai Indonesia Stiglitz sudah sering mengemukakan pendapatnya dalam berbagai ceramah atau tulisannya, baik selama kunjungannya di Indonesia di masa-masa yang lalu, maupun dalam berbagai kesempatan di banyak negeri. Tetapi, pernyataannya yang terakhir di Jakarta baru-baru ini, adalah sangat menarik, karena ia telah mengangkat masalah politik pemerintah Indonesia di bidang penanaman modal asing dengan bahasa yang cukup kritis.

Untuk dapat bersama-sama menelaah kembali - dengan lebih teliti lagi - kritiknya yang tajam tentang politik pemerintah Indonesia di bidang penanaman modal asing, maka kita sajikan sekali lagi interviewnya dengan Tempo (16 Agustus 2007). Mengingat arti penting interwiew-nya ini bagi kita maka patutlah kiranya kita dalami, sekali lagi, pokok-pokok fikirannya, yang antara lain berbunyi sebagai berikut :

“Pemerintah diminta menegosiasi ulang kontrak-kontrak pertambangan yang terindikasi merugikan kepentingan rakyat. Jika pemerintah Indonesia berani melakukan ini maka akan memperoleh keuntungan jauh lebih besar dibandingkan yang diperoleh para investor asing.

"Mereka (para perusahaan tambang asing) tahu kok bahwa mereka sedang merampok kekayaan alam negara-negara berkembang," kata Stiglitz

”Negosiasi ulang kontrak karya ini juga sangat mungkin dilakukan dengan Freeport McMoran, yang memiliki anak perusahaan PT Freeport Indonesia. Freeport merupakan salah perusahaan tambang terbesar di dunia yang melakukan kegiatan eksplotasi di Papua.

”Stiglitz mencontohkan ketegasan sikap Rusia terhadap Shell. Rusia mencabut izin kelayakan lingkungan hidup yang dikantongi Shell. Ini karena perusahaan minyak itu didapati melanggar Undang-Undang Lingkungan Hidup dengan melakukan pencemaran lingkungan. "Kalau melanggar undang-undang, ya izinnya harus dicabut dong," kata dia.

”Seperti ramai diberitakan beberapa waktu lalu, Freeport Indonesia melakukan pencemaran lingkungan selama mengebor emas dan tembaga di Papua. Namun, kasus ini tidak pernah sampai ke pengadilan. Pemerintah hanya meminta perusahaan tambang asal Amerika Serikat itu memperbaiki fasilitas pengolahan limbahnya (kutipan dari Tempo Interaktif selesai).

Mereka sedang merampok kekayaan alam kita

Sebagai seorang ahli di bidang ekonomi, yang pernah menjabat Wakil Direktur Bank Dunia, dan anggota terkemuka dewan ekonomi presiden Clinton maka menarik dan penting sekali ketika ia mengatakan “Pemerintah Indonesia diminta menegosiasi ulang kontrak-kontrak pertambangan yang terindikasi merugikan kepentingan rakyat. Jika pemerintah berani melakukan ini maka akan memperoleh keuntungan jauh lebih besar dibandingkan yang diperoleh para investor asing. Perusahaan tambang asing tahu kok bahwa mereka sedang merampok kekayaan alam negara-negara berkembang”.

Anjuran Stiglitz supaya pemerintah Indonesia menegosiasi ulang kontrak-kontrak pertambangan yang terindikasi merugikan kepentingan rakyat merupakan pembenaran atau penggarisbawahan tuntutan banyak kalangan di Indonesia, termasuk organisasi seperti : ABM (Aliansi Buruh Menggugat) , Koalisi Anti Utang, WALHI (Wahana Lingkungan Hidup Indonesia), Debt Watch, FSPI (Federasi Serikat Petani Indonesia), INFID (International NGO's Forum for Indonesian Development), JATAM (Jaringan Advokasi Tambang), KPKB (Kelompok Perempuan untuk Keadilan Buruh), KoAge (Koalisi Anti Globalisasi Ekonomi), KPA (Konsorsium Pembaruan Agraria), LBH Apik , PBHI (Perhimpunan Bantuan Hukum dan Hak Asasi Manusia Indonesia), Perkumpulan Bumi, Sekretariat Bina Desa, SP (Solidaritas Perempuan) SEKAR, Aliansi Perempuan untuk Keterwakilan Politik, AKATIGA, STN (Serikat Tani Nasional), SPOI (Serikat Pekerja Otomotif Indonesia), Lapera Indonesia, The Institute for Global Justice, FPPI (Front Perjuangan Pemuda Indonesia), Serikat Mahasiswa Indonesia,), LS-ADI (Lembaga Studi dan Aksi Untuk Demokrasi), LBH-Jakarta, PRD, Papernas, Perhimpunan Rakyat Pekerja, Indonesian Centre for Environmental Law (ICEL), Federasi Serikat Buruh Jabotabek, (dan banyak organisasi lainnya).

Stiglitz menegaskan bahwa kalau pemerintah Indonesia berani melakukan negosiasi ulang tentang perjanjian-perjanjian atau kontrak-kontrak maka akan memperoleh keuntungan jauh lebih besar dibandingkan yang diperoleh para investor asing. Pernyataan Stiglitz ini sangat penting, sebab sejak puluhan tahun, para investor asing di Indonesia telah mengeruk keuntungan yang besar, sedangkan hasil yang diperoleh pemerintah Indonesia adalah kecil sekali. Yang lebih-lebih menyedihkan lagi ialah kenyataan bahwa sebagian (yang tidak kecil!) dari hasil kontrak-kontrak ini tidak masuk ke kas negara, melainkan dikorupsi oleh pejabat-pejabat di berbagai tingkat, baik di Pusat maupun di daerah.

Stiglitz dianggap “pengkhianat” kepentingan World Bank dan IMF

Pernyataan Stiglitz mengenai pentingnya negosiasi ulang kontrak-kotrak dengan para investor asing ini juga tercermin dalam kalimatnya yang mengatakan bahwa perusahaan tambang asing itu pada umumnya tahu bahwa mereka sedang merampok kekayaan alam negara-negara berkembang Bahasa yang digunakan Stiglitz, sebagai ahli ekonomi yang terpandang di dunia, yang mengatakan bahwa investor-investor asing itu “merampok kekayaan alam negara-negara berkembang” adalah ucapan yang terlalu terus-terang dan tidak tanggung-tanggung, dan langsung menusuk jantung hati para investor skala dunia itu.

Itulah sebabnya maka sebagai seorang yang pernah menjabat wakil Direktur Bank Dunia ia dijuluki oleh sebagian kalangan sebagai “pengkhianat”. Sikapnya yang kritis sekali terhadap politik dan praktek-praktek yang dilakukan IMF, dan yang menentang akibat-akibat negatif globalisasi, membikin dirinya terkenal sebagai seorang yang membela kepentingan negara-negara miskin dan dunia ketiga umumnya. Ia juga termasuk seorang di antara tokoh-tokoh yang melawan pencemaran lingkungan hidup.

Masalah Freeport : akibat politik yang salah Orde Baru

Juga, sebagai orang yang pernah menduduki jabatan yang begitu tinggi dan penting dalam pemerintahan Amerika pernyataannya mengenai perlunya ada negosiasi ulang dengan PT Freeport Indonesia adalah satu hal sangat menarik. Sebab, hal ini bertentangan sama sekali dengan sikap pembesar-pembesar Amerika lainnya (termasuk Henry Kissinger) yang selalu berusaha membela kepentingan PT Freeport.

Sikap Stiglitz yang demikian penting ini kiranya perlu mendapat sambutan dari banyak kalangan, baik dari kalangan pemerintah maupun tokoh-tokoh masyarakat, para intelektual dan organisasi masyarakat. Karena, kasus PT Freeport adalah salah satu di antara kasus-kasus yang paling parah yang dihadapi negara Indonesia, sebagai akibat politik yang salah selama puluhan tahun dari rejim militer Orde Baru sejak tahun 1967.

Tetapi, masalah investasi asing yang dihadapi negara Indonesia bukanlah hanya PT Freeport Indonesia, melainkan juga sebagian terbesar investasi asing lainnya. Ini juga berlaku bagi Exxon, Newmont, Rio Tinto dan banyak lagi lainnya. Sebab, jumlah investasi asing di bidang pertambangan di Indonesia adalah besar sekali. Kira-kira 70 % dari pertambangan di Indonesia didominasi oleh modal asing. Dan sebagian besar dari investasi asing ini sudah menimbulkan bermacam-macam akibat yang negatif terhadap masyarakat setempat di sekelilingnya, baik di bidang sosial maupun ekonomi, dan akibat buruk yang berkaitan dengan lingkungan hidup.

Bukan hanya Freeport saja!

Ini terjadi dengan kasus investasi Exxon Mobil di Aceh (NAD), Laverton Gold di Sumatera Selatan, Chevron, Rio Tinto dan KPC di Kalimntan Timur, Arutmin di Kalimantan Selatan, Aurora Gold di Kalimantan Tengah , PT Inco di Sulawesi Selatan, Expan Tomori di Sulawesi Tengah, Antam Pomalaan di Sulawesi Tenggara, Newmont di Sulawesi Utara dan Sumbawa, PT Arumbai di Nusa Tenggara Timur, Newcrest, PT Anggal dan PT Elka Asta Media di Maluku, Beyond Petroleum (BP) Tangguh di Papua.

Dalam menganjurkan kepada pemerintah Indonesia untuk menegosiasi ulang kontrak-kontrak karya dengan para investor asing yang menguasai pertambagan minyak dan gas, Stiglitz mengambil contoh keberhasilan Bolivia. “”Negara miskin Amerika Latin itu sekarang memperoleh keuntungan yang jauh lebih besar. "Jika sebelumnya hanya memperoleh keuntungan 18 persen, sekarang sebaliknya mereka yang mendapat 82 persen," ujarnya. Dan para investor asing itu, kata dia, tetap disana.

Yang juga sangat menarik dari interview Stiglitz ialah anjurannya supaya akibat praktek-praktek buruk para investor asing di Indonesia dibeberkan dalam media massa. “Masyarakat pasti akan sangat marah ketika mengetahuinya, sehingga kontrak-kontrak itu akan dinegosiasi ulang”, katanya. Rupanya, Stiglitz cukup mengenal garis-garis besar situasi di Indonesia, sehingga ia menganjurkan adanya pembeberan di media massa segala oraktek-praktek buruk perusahaan besar asing serta akibatnya yang merugikan.

Anjuran Stiglitz semacam itu adalah penting sekali kalau kita ingat kepada sikap para pejabat negara sejak pemerintahan Orde Baru yang membuka pintu lebar-lebar bagi masuknya investasi asing secara besar-besaran di berbagai bidang. Investor diberi segala macam pelayanan dan kemudahan-kemudahan, walaupun ternyata banyak menimbulkan masalah bagi rakyat dan merugikan kepentingan negara.

Gerakan extra-parlementer : tugas patriotik

Oleh karena DPR atau DPRD (atau DPD) tidak bisa diharapkan banyak untuk mengontrol berbagai politik pemerintahan mengenai investasi-investasi asing, maka peran berbagai organisasi non-pemerintah (ornop) dan media massa menjadi sangat penting sekali. Sebab, korupsi dan kolusi melalui dalam bentuk “suapan” yang macam-macam tidak hanya telah dilakukan oleh pejabat-pejabat penting negara, melainkan juga oleh anggota-anggota DPR atau DPRD.

Jadi, segala macam aksi-aksi extra-parlementer yang dilakukan oleh berbagai kekuatan dalam masyarakat untuk melawan segala politik buruk pemerintah di bidang penanaman modal asing adalah tugas atau kewajiban yang patriotik, yang perlu mendapat dukungan seluas mungkin dari segala fihak. Gerakan atau aksi-aksi extra-parlementer yang dilakukan berbagai tokoh masyarakat, ornop, dan ormas mahasiswa dan pemuda, adalah sangat mutlak perlunya untuk menghadapi dominasi modal asing di Indonesia beserta kakitangan mereka

Kiranya, kita semua patut selalu ingat bahwa dari 230 juta penduduk Indonesia sekitar separonya (atau sekitar 115 juta) hidup dengan kurang 2$US seharinya, dan bahwa ada pengangguran lebih dari 40 juta orang, dan juga lebih dari 40 juta orang hidup dalam kemiskinan, ditambah lagi dengan 13 juta anak-anak yang kurang makan.

Dengan banyaknya masalah-masalah parah yang sedang dihadapi bangsa dan negara kita dewasa ini, maka makin nyatalah bahwa dengan sistem pemerintahan dan konstelasi politik seperti yang sekarang ini tidak mungkin diadakan perubahan-perubahan radikal yang bisa membawa perbaikan hidup bagi sebagian besar rakyat. Apalagi, berbagai masalah besar dan parah seperti tersebut di atas dibikin lebih parah lagi dengan korupsi yang sudah merajalela dan pembusukan akhlak di berbagai kalangan masyarakat.

Sejumlah negeri-negeri di Amerika Latin (umpamanya Venezuela, Bolivia, Argentina dan juga Kuba) sedang menunjukkan kepada dunia bahwa jalan lain untuk mendatangkan perubahan besar dan perbaikan hidup rakyat banyak adalah mungkin, dan bukannya jalan yang ditunjukkan oleh Washington.

Pengalaman di berbagai negeri Amerika Latin ini patutlah sekali diperhatikan oleh semua golongan dan kalangan di Indonesia yang menginginkan adanya perubahan-perubahan besar serta perbaikan sejati di negeri kita.
