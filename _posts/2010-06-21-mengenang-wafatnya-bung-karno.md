---
layout: post
title: Mengenang Wafatnya Bung Karno
date: 2010-06-21
---

Dalam rangka memperingati hari wafatnya Bung Karno pada tanggal 21 Juni 1970, berikut di bawah ini kita sajikan tulisan Sdr Y. T. Taher (Australia) yang berjudul « Mengenang Wafatnya Bung Karno ». Ia adalah bekas tapol yang ditahan dan disiksa bertahun-tahun oleh kekuasaan militer di Riau sejak terjadinya G30S.



Untuk mengenang segala pengalaman pahit yang dideritanya selama jadi tahanan Orde Baru serta  segala  tindakan yang tidak manusiawi terhadap para tahanan yang dibunuh serta disiksa dengan berbagai cara ia telah menulis buku « Riau Berdarah » (Kisah perjalanan  hidupku), yang diterbitkan oleh Hasta Mitra dan didistribusikan oleh Ultimus (Bandung), toko buku Kalam (Utankayu, Jakarta), CV Doea Lentera (Cikoko, Jakarta).



Buku « Riau Berdarah » (273 halaman)  ini yang ditulisnya di Australia, dimana ia tinggal sekarang bersama istri dan anak-anaknya,  merupakan dokumentasi yang paling lengkap  (dan menarik sekali) tentang keganasan rejim militer Suharto terhadap golongan kiri di provinsi Riau.



Dalam bukunya ini, ia juga menggambarkan kekagumannya atau penghormatannya kepada Bung Karno, yang tercermin juga dalam tulisannya yang dibawah ini.



Paris, 21 Juni 2010

A. Umar Said

= = =    

Mengenang Wafatnya Bung Karno



Oleh : Y. T. Taher  (Australia)



Hari ini, 21 Juni 2010. Empat puluh tahun yang lalu, seorang pemimpin Besar Bangsa, pejuang kemerdekaan dan revolusi yang sejak mudanya mengorbankan masanya demi bangsa yang dicintainya, Presiden Pertama Republik Indonesia Bung Karno, wafat dalam derita yang mengenaskan.



Wafatnya Bung Karno merupakan kehilangan yang besar sekali bagi bangsa

Indonesia, terutama bagi yang mencintainya, menghormatinya, mengaguminya,

sebagai bapak bangsa, dan sebagai pemersatu bangsa yang paling agung

sepanjang sejarah Indonesia..



Ketokohan Bung Karno sebagai Pemimpin, bukan saja diakui oleh bangsa Indonesia, namun juga dunia mengakuinya karena banyak karya dan ide-idenya, ajaran-ajarannya  yang tersebar di dunia luar dan diakui terutama oleh negara-negara Asia Afrika yang berjuang dan berhasil memutuskan rantai penjajahan atas negerinya. Bahkan, Bung Karno tidak segan-segan berbicara di depan Sidang Majelis Umum Perserikatan Bangsa-Bangsa dan bertekad membangun dunia baru, "To Build a World Anew" dengan memperkenalkan dan menganjurkan agar dunia juga menganut Pancasila (Five Principles).



Akan tetapi, sayang sekali, wafatnya Bung Karno tidak seperti wafatnya pemimpin atau tokoh-tokoh Indonesia lainnya. Bung Karno meninggal karena dizalimi dan dianiaya oleh seorang manusia militer yang bernama Suharto.



Jenderal Suharto, dengan sangat licik dan keji, menggunakan  Bung Karno untuk menghabisi semua pendukung-pendukungnya.  Melalui Sarwo Edhi Wibowo, Sudomo, Kemal Idris, Sumitro, Jasir Hadibroto dan petinggi militer yang bisa dan gampang diperintahnya, melakukan pembunuhan massal terhadap jutaan bangsa Indonesia pendukung Bung Karno, sebagai dalih mengganyang G30S. Jutaan bangsa Indonesia mati dibunuh, ratusan ribu dipenjarakan, 12 ribu dibuang ke pulau Buru, dan ribuan yang berada di luar negeri dicabut paspor Indonesianya. Sedang keluarga dan sanak saudara kaum kiri yang dimusnahkan, dijadikannya sebagai golongan yang tidak bersih dan dikucilkan dari masyarakat dan dianggap kaum pariah melalui peraturan-peratruran diskriminatif.



Setelah semua pengikut dan pembela Bung Karno dipreteli dan dimusnahkan, dengan segala dalih dan akal licik, Bung Karno sendiri dizalimi. Dengan menggunakan istitusi di bawah bedil dan bayonet Jenderal Suharto, Bung Karno dicabut kekuasaannya dan didepak keluar dari Istana Merdeka dan dijadikan tahanan rumah di Wisma Yaso. Bung Karno dijadikan Tahanan Politik Ordebaru/Suharto, persis seperti ratusan ribu rakyat yang ditahan di kamp-kamp tahanan yang berserakan disegenap penjuru Indonesia. Suharto menjadi orang nomor satu di Indonesia, berkat kelicikan dan akal busuknya serta politik dedil dan bayonet yang dilakukannya. Dia menjadi "the smilling general" karena dia berhasil melenyapkan jutaan kaum kiri dan PKI serta menjatuhkan Bung Karno, dimana Amerika Serikat sendiri tidak berdaya melakukannya!



Wafanya Bung Karno menimbulkan duka cita yang luas dan mendalam bagi segenap bangsa Indonesia, secara terang-terangan maupun secara sembunyi (karena takut ditangkap dan dibui oleh nazinya Suharto). Rakyat marah, dendam dan sakithati terhadap segala perlakukan keji, biadab dan tidak berperikemanusiaan yang dilakukan Suharto terhadap Bung Karno hingga menyebabkan wafatnya. Kita kehilangan Bung Karno, Pemimpin Bangsa yang susah untuk dicari gantinya. "Belum ada yang menandingi Soekarno sampai sekarang.. Dialah yang mengerti 'nation' Indonesia. Karena itu juga, Soekarno-lah yang tidak jemu-jemunya menganjurkan tentang 'nation and character building' (Ucapan Pramoedya Ananta Toer dalam film dokumen Shadow Play)



Bung Karno telah tiada. Hari ini tepat 40 tahun yang lalu dia mati mengenaskan, dizalimi dan dianiaya oleh seorang militer yang pernah diselamatkannya. Bung Karno pergi, meninggalkan bangsa Indonesia yang dicintainya. Kita kehilangan seorang tokoh agung, seorang bapak, seorang penyambung lidah rakyat dan pemimpin besar revolusi Indonesia Bung Karno. Bung Karno, lahir, hidup, berjuang dan mati untuk rakyat dan bangsa Indonesia yang dicintainya! "Yo sanak yo kadang yen mati aku sing kelangan!" Bangsa Indonesia berhutang padanya dan rakyat akan senantiasa mengenangnya!



Untuk mengenang hari-hari akhir Bung Karno, dibawah ini dikutip beberapa cuplikan berita tentang hari-hari akhir Bung Karno, Pemimpin Besar, Bapak Bangsa dan Presiden Pertama Republik Indonesia. Salah satu cuplikan adalah dari anak Bung Karno sendiri, Rachmawati, yang dikutip dari Harian Jawa Pos, sbb.:



1. Me-nurut informasi yang ditulis pada 12 April 2003 oleh A. Karim D. P., mantan Ketua Persatuan Wartawan Indonesia, dikatakan bahwa "Jenderal Soeharto, memerintahkan kepada Bung Karno supaya meninggalkan Istana Merdeka sebelum tanggal 17 Agustus 1967.  Bung Karno beserta anak-anaknya pergi dari istana dengan berpakaian kaos oblong dan celana piyama dengan kaki hanya beralaskan sandal, menompang mobil Volkswagen Kodok, satu-satunya mobil milik pribadinya yang dihadiahkan oleh Piola kepadanya, pergi ke Wisma Yaso, di mana kemudian menjadi tempat tahanannya sampai wafat. Semua kekayaannya, ditinggalkan di istana, tidak sepotong pun yang dibawa pergi kecuali Bendera Pusaka Merah Putih yang dibungkusnya dengan kertas koran. Anak-anaknya pun tidak diperbolehkan membawa apa-apa, kecuali pakaian sendiri, buku-buku sekolah, dan perhiasannya sendiri. Selebihnya, ditinggalkan semua di istana....." (A. Karim DP., Apa Sebab Bung Karno Bisa Digulingkan, http://www.progind.net)



2. Selama ditahan di Wisma Yaso, Bung Karno diperlakukan sangat tidak manusiawi sekali. Bung Hatta, mantan wakil presiden pertama RI, sahabat Bung Karno, menceritakan bagaimana permintaan Bung Karno kepada Soeharto untuk sekadar mengizinkan mendatangkan seorang dukun pijat, ahli pijat langganan Bung Karno dan juga langganan Bung Hatta, ditolak oleh Soeharto! Bung Karno mengharapkan dengan bantuan pijatan dukun ahli itu, penderitaannya bisa sedikit berkurang. Penolakan Soeharto itulah yang kemudian mendorong Bung Hatta menulis surat pada 15 Juli 1970 kepada Soeharto yang mengecam betapa tidak manusiawinya sikap Soeharto itu! Bung Hatta minta kepada Soeharto lewat Jaksa Durmawel, S.H., agar dilakukan pengadilan untuk memastikan apakah Bung Karno bersalah atau tidak. Sebab, jika Bung Karno meninggal dalam statusnya sebagai tahanan politik karena tidak diadili, rakyat yang percaya bahwa Bung Karno tidak bersalah, akan menuduh pemerintah Soeharto sengaja membunuhnya, kata Bung Hat

 ta." (Deliar Noer, Mohammad Hatta Biografi Politik, http://www.progind.net)



3. BUNG KARNO yang penyakitnya tambah parah dalam tahanan rumahnya, tiada mendapat pengobatan yang wajar, bahkan resep pengobatan hanya disimpan di laci oleh seorang pejabat tinggi militer. Untuk ini, mari kita telusuri kisah putri Bung Karno, Rachmawati, "Mengenang Saat Terakhir Mendampingi Bung Karno" seperti yang diceritakan oleh Harian Jawa Pos.



AIR mata Rachmawati membasahi pipi. Meski sudah 38 tahun berlalu, anak ketiga Bung Karno dengan Fatmawati itu belum bisa melupakan kekecewaannya terhadap perlakuan pemerintahan Soeharto terhadap sang ayahanda di penghujung hayatnya.



"Bung Karno saat itu seperti dibiarkan mati perlahan-lahan," katanya kepada Jawa Pos sambil mengusap air matanya dengan tisu saat ditemui di kediaman di kawasan Jakarta Selatan kemarin (19/1).



Menurut perempuan kelahiran Jakarta, 27 September 1950, presiden pertama RI itu dilarikan ke RSPAD Gatot Subroto, Jakarta, dalam kondisi kritis pada 11 Juni 1970.



Berbeda dengan Soeharto yang mendapat perawatan supermaksimal dari tim dokter kepresidenan beranggota 25 dokter, Bung Karno mendapat perawatan minimal pada hari-hari terakhir menjalani opname di rumah sakit milik TNI-AD itu. Kondisi ginjal salah satu proklamator RI waktu itu sudah sangat parah.



"Tapi, tak ada perawatan maksimal. Alat hemodialisis (cuci darah) untuk pengidap gagal ginjal pun tak diberi," kenang wanita bernama lengkap Diah Pramana Rachmawati Soekarno yang kini menjadi anggota Dewan Pertimbangan Presiden itu.



Saat Bung Karno sakit kritis, lanjut dia, Soeharto melakukan konsolidasi politik pasca G 30 S/1965. Tak heran bila saat itu dia sedang membersihkan tubuh birokrasi dan militer. Tidak hanya dari unsur-unsur PKI, tapi juga dari orang-orang Soekarnois.



Asupan makan untuk Soekarno yang juga disebut pemimpin besar revolusi itu pun seadanya. Kendati juga didiagnosis mengidap darah tinggi, menu makanan untuk Bung Karno terasa asin saat dicicipi Rachmawati.



Adik kandung Megawati pun langsung protes dan baru setelah itu menu makanan diganti. "Bung Karno seperti dibiarkan mati perlahan-lahan," imbuhnya.



Gadis yang saat itu berusia hampir 20 tahun dan kuliah di Fakultas Hukum UI memang paling rajin membesuk di hari-hari terakhir sang Putra Fajar tersebut. Namun, soal membesuk juga bukan urusan mudah dan jangan dibayangkan seperti keluarga Cendana yang kini bisa hilir mudik ke Rumah Sakit Pusat Pertamina setiap saat.



Tak gampang mengakses tempat perawatan Bung Karno yang saat itu statusnya diambangkan laiknya tahanan rumah. Bahkan, tak ada kolega Soekarno yang datang mengalir seperti yang terlihat pada Soeharto hari-hari ini.



Menjenguk Soekarno memang sulit. Sebab, ruang perawatan intensif RSPAD Gatot Subroto dipenuhi tentara. Serdadu berseragam dan bersenjata lengkap bersiaga penuh di beberapa titik strategis rumah sakit tersebut. Bahkan, petugas keamanan berpakaian preman juga hilir mudik di koridor rumah sakit hingga pelataran parkir.



Yang repot, kata Rachmawati, obat-obatan pun tak datang dari langit. Mereka harus membeli sendiri. "Pokoknya beda, beda banget. Ini perjalanan sejarah yang pahit," sambung Rachmawati yang harus tidur di mobil jika hendak menunggu ayahnya yang dirawat di RSPAD.



Bahkan, Rachmawati yang pada 6 Juni 1970 sempat memotret Bung Karno yang saat itu diasingkan di Wisma Yaso (sekarang Museum Satria Mandala, Red), Jakarta Selatan, harus berurusan dengan polisi militer di Jl Guntur, Jakarta Pusat.



Pasalnya, foto yang menunjukkan kondisi terakhir Bung Karno itu dikirimkan Rachmawati ke Associated Press (kantor berita yang berpusat di Amerika) yang kemudian memublikasikannya ke seluruh dunia.



Salah seorang yang bisa menjenguk Bung Karno saat itu adalah mantan Wakil Presiden Mohammad Hatta. Bung Karno sempat memegang tangan Bung Hatta, sahabat seperjuangan yang juga proklamator RI itu, kemudian menangis. Sehari kemudian, tepatnya Minggu pagi, 21 Juni 1970, beberapa saat setelah diperiksa dr Mardjono, salah seorang anggota tim dokter kepresidenan, Bung Karno akhirnya mengembuskan napas terakhir pada usia 69 tahun.



Setelah Bung Karno meninggal itulah, baru Soeharto datang ke RSPAD untuk melihat kondisi jenazah Soekarno. Pertemuan itu, menurut Rachmawati, adalah yang pertama antara ayahnya -yang telah tiada- dan Soeharto semenjak ayahnya jatuh dari kursi presiden dalam Sidang Istimewa 7 Maret 1967.



Orang kuat Orde Baru itu kembali datang ke Wisma Yaso saat jenazah Soekarno hendak diterbangkan ke Blitar melalui Malang, Jawa Timur.



Di Blitar, upacara pemakaman Soekarno dilaksanakan dengan sederhana dan singkat dipimpin Jenderal M. Panggabean. "Rasa-rasanya, hari itu begitu mencekam. Kami hanya menurut saja saat pemerintah memakamkan Bung Karno di Blitar. Bukan sesuai permintaannya di Batu Tulis, Bogor, Jawa Barat," kenang mantan ketua umum Partai Pelopor itu.



Bendera setengah tiang pun berkibar di seluruh tanah air kendati setelah itu tak semua orang bebas datang ke kubur Bung Karno.



Rachmawati mengaku sama sekali tak menaruh dendam atas tindakan Soeharto kepada ayahnya. Secara pribadi dan sebagai manusia, dia malah memaafkan Soeharto. Tapi, dia tetap menuntut penyelesaian hukum kepada jenderal bintang lima yang berkuasa selama 32 tahun itu. Kalaupun pemerintah memutuskan untuk mencabut Tap MPR No XI/MPR/1998 tentang Penyelenggara Negara yang Bersih dan Bebas KKN yang menyangkut Soeharto, Rachmawati juga meminta pencabutan Tap XXXIII/MPRS/1967 tentang Pencabutan Kekuasaan Pemerintahan Negara dari Presiden Soekarno.



Apakah karma Bung Karno yang kini menimpa Pak Harto? "Wallahu 'alam. Tapi, alhamdulillah, kalau Pak Harto kembali sehat sekarang," jawabnya.



Kendati belum sempat membesuk, Rachmawati telah menghubungi dua putri Soeharto, Mamik dan Titik, serat mendoakan kesembuhan ayah mereka. "Kami bangga diwarisi Bung Karno dengan bekal ilmu kehidupan. Bukan harta berlimpah," katanya lalu menyeka air mata."



Demikian wawancara dengan Rachmawati sebelum Jenderal Soeharto meninggal.



Bung Karno telah tiada, bapak bangsa dan rakyat Indonesia, pejuang kemerdekaan Indonesia telah pergi. Dia meninggal, mati dalam mempertahankan rakyatnya, bangsa dan negaranya agar tidak terpecah-belah.  Bung Karno pergi dengan meninggalkan warisan besar kepada Soeharto berupa pangkat jenderal setelah menyelamatkannya dari pengadilan militer karena korupsinya, kemudian Soeharto mengkhianatinya. Bung Karno mati mengikuti jalan yang telah dirintis oleh jutaan pengikutnya, yaitu dibantai Soeharto. Kita kehilangan seorang tokoh agung, seorang bapak, seorang penyambung lidah rakyat dan pemimpin besar revolusi Indonesia Bung Karno. Bung Karno, lahir, hidup, berjuang dan mati untuk rakyat dan bangsa Indonesia yang dicintainya! "Yo sanak yo kadang yen mati aku sing kelangan!" Bangsa Indonesia berhutang padanya dan rakyat akan senantiasa mengenangnya!



Australia, 21 Juni 2010
