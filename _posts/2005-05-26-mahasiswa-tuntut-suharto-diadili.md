---
layout: post
title: Mahasiswa tuntut Suharto diadili
date: 2005-05-26
---

Menurut berbagai media pers Indonesia, « Ratusan aktivis dan elemen mahasiswa dari berbagai perguruan tinggi di Jakarta dan Bogor pada tanggal 21 Mei turun ke jalan, memperingati 7 tahun lengsernya Presiden Soeharto, penguasa Orde Baru. Adian Napitupulu dari Forum Kota menyatakan bahwa para aktivis dan mahasiswa ini masih konsisten dengan tuntutannya agar Soeharto diadili.

"Kami semua menolak diberlakukannya UU Komisi Kebenaran dan Rekonsiliasi (KKR). Bagi kami tak mungkin ada rekonsiliasi tanpa adanya pertanggungjawaban terlebih dahulu. Konsekuensinya, Soeharto diadili terlebih dulu," kata Adian, seraya menambahkan aksi bersamaan juga digelar unsur-unsur massa-mahasiswa serentak di berbagai kota. (Sinar Indonesia Baru, 22/5)

« Di Medan, mahasiswa yang tergabung dalam Himpunan Mahasiswa Islam (HMI) Komisariat FISIP USU melakukan aksi refleksi dan aksi lilin mengenang jatuhnya rezim Soeharto di Bundaran Gatot Subroto dan DPRD SU.

« Dalam pernyataan sikapnya, mahasiswa meneriakkan agar mantan Presiden Soeharto beserta kroni-kroninya ditangkap dan diadili. Mereka mendesak agar kasus-kasus pelanggaran HAM berat, kasus Semanggi, Trisakti dan kasus lain diusut secara tuntas.

« Mahasiswa dalam aksinya menilai bahwa selama kekuasaan Orde Baru rakyat selalu mengalami pemaksaan, intimidasi, perampokan dan perkosaan hak-hak azasinya oleh penguasa. Fakta tersebut terlihat di wilayah Aceh, Papua, Lampung dan Tanjung Priok, serta Maluku. Tahun 1997 ketika Indonesia masuk dalam krisis moneter rakyat menjerit yang muaranya secara bersama-sama dengan mahasiswa bergerak untuk menumbangkan kekuasaan yang akhirnya terwujud dengan munculnya orde reformasi.

« Namun pada masa orde reformasi telah timbul reformis-reformis gadungan dan kroni-kroni pejabat Orde Baru bangkit kembali. Untuk menghindari munculnya kembali kekuasaan rezim tersebut mahasiswa kembali meneriakkan agar elemen mahasiswa bersatu meneruskan agenda reformasi.

« Juga di Makassar sejumlah organisasi mahasiswa memperingati turunnya Soeharto 7 tahun lalu. Sekitar 200 mahasiswa memenuhi perempatan Tol Reformasi. Mereka terdiri dari sejumlah organisasi mahasiswa, antara lain Liga Mahasiswa Nasional untuk Demokrasi (LMND), Front Makassar (FM), BEM Universitas Negeri Makassar (UNM) dan Universitas Indonesia Timur (UIT).

"7 tahun sudah Soeharto lengser, namun tak kunjung diadili. Kami menuntut agar Soeharto, biang dari kehancuran negeri ini, diseret dan disita hartanya," ujar para demonstran saat berorasi. Selain berorasi, para mahasiswa juga membawa poster dan membentangkan spanduk. Salah satu isinya "Tuntaskan agenda reformasi" dan "Sita harta Soeharto, untuk membayar utang negara". Mahasiswa Makassar menuntut pemerintah agar tidak menghentikan pengusutan kasus korupsi Soeharto, KPU dan Bank Mandiri.

« Di Jambi, mahasiswa dari Kesatuan Aksi Mahasiswa Muslim Indonesia berorasi dan memaksa untuk melakukan siaran langsung di RRI setempat. Dalam orasinya mereka menyatakan meski rezim Soeharto telah 7 tahun jatuh, korupsi belum berhasil dimusnahkan pemerintah. Bahkan semakin mewabah seperti yang terjadi di KPU. Sebagai simbol tidak berdayanya pemerintah mahasiswa membakar keranda dan patung pejabat yang korupsi. (Detikcom)

## Teruskan Aksi Seluas Dan Sesering Mungkin

Aksi-aksi para mahasiswa di berbagai kota Indonesia ini (yang sebagian di antaranya diberitakan oleh pers seperti tersebut di atas) merupakan gugatan, atau ekspressi kemarahan dan kejengkelan terhadap situasi di negeri kita dewasa ini. Dalam aksi-aksi ini mereka telah menyuarakan isi hati-nurani banyak orang, antara lain : menuntut diadilinya Suharto, menangkap para koruptor, mengusut secara tuntas kasus pelanggaran-pelanggaran berat HAM, mensita harta Suharto untuk membayar utang negara, memerangi korupsi yang makin mewabah, dan meneruskan reformasi.

Dalam situasi di tanah air seperti sekarang ini, aksi-aksi para mahasiswa – yang terdiri dari berbagai aliran politik, golongan ideologi atau agama – adalah suatu hal yang diperlukan sekali dan sangat baik bagi kehidupan seluruh bangsa. Berbagai aksi para mahasiswa ini mengingatkan kita semua bahwa banyak sekali urusan-urusan besar negara dan rakyat yang masih belum (atau tidak sama sekali !) diselesaikan secara baik oleh pemerintah dan lembaga-lembaga negara kita, termasuk oleh DPR dan DPRD. Oleh karena itu, aksi-aksi atau berbagai kegiatan mahasiswa perlu dilakukan terus seluas mungkin dan sesering mungkin, demi kepentingan seluruh bangsa.

Di antara urusan-urusan besar itu adalah masalah diadilinya mantan presiden Suharto, yang menjadi tuntutan utama dari aksi-aksi mahasiswa di berbagai kota, dalam rangka peringatan 7 tahun turunnya dari kekuasaan yang dipegangnya dengan tangan besi selama 32 tahun. Suara yang dilantunkan oleh generasi muda kita tentang mantan pemimpin tertinggi rejim militer Orde Baru ini merupakan jawaban (atau penolakan ) terhadap ucapan atau sikap yang akhir-akhir ini dimanifestasikan oleh sejumlah « tokoh-tokoh » (dan pimpinan negara) kepada Suharto.

Seperti yang diberitakan oleh media massa, ketika mantan presiden Suharto dirawat di rumahsakit baru-baru ini, berbagai « tokoh » -- yang kebanyakan di masa lalu adalah pendukung Orde Baru – telah mengunjunginya untuk menunjukkan respek terhadapnya. Bahwa ada orang-orang yang merasa « berhutang budi » dan karenanya menghormati Suharto, ini adalah masih « masuk akal ». Tetapi kalau ada yang mengatakan bahwa Suharto tidak mempunyai « perkara » apa-apa dan, karenanya, tidak perlu dan tidak bisa diajukan di depan pengadilan, inilah yang keterlaluan !!! Dan ini jugalah yang telah dibantah oleh para mahasiswa dengan aksi-aksi mereka.

## Dosa-Dosa Suharto Banyak Sekali

Sebab, jelaslah sudah bagi banyak orang, bahwa Suharto perlu diajukan di depan pengadilan, untuk memberikan pertanggungan jawab terhadap begitu banyak dosa-dosanya karena berbagai kejahatan dan kesalahannya selama 32 tahun berkuasa sebagai diktator rejim militer. Di antara dosa-dosanya (atau kejahatannnya) itu yang sangat menyolok adalah : pembangkangan terhadap Presiden Sukarno dan kemudian menggulingkannya, penyalahgunaan Supersemar, pembantaian kira-kira 3 juta orang tidak bersalah, penahanan sewenang-wenang ratusan ribu orang tidak bersalah dalam jangka yang lama, perlakuan terhadap para eks-tapol beserta keluarga mereka, agresi terhadap Timor Timur dan dibunuhnya ratusan ribu penduduk Timor Timur.

(Sebagian dari kesalahan-kesalahan Suharto ini dapat disimak dalam surat gugatan LBH Jakarta kepada Pengadilan Negeri Jakarta Pusat dalam rangka sidang class action mengenai korban 65. Tentang surat gugatan ini harap buka Website http://perso.club-internet.fr/kontak/ )

Walaupun perjuangan para mahasiswa Indonesia mengalami pasang surut sejak gerakan nasional yang bersejarah dalam menjatuhkan Suharto dari kekuasaannya dalam tahun 1998, tetapi perannya tetap sangat penting dalam kehidupan bangsa. Apalagi ketika bangsa dan negara kita menghadapi proses pembusukan di berbagai bidang seperti yang kita alami dewasa ini, terasa sekali bahwa peran yang aktif generasi muda adalah tumpuan harapan banyak orang.

## Golkar Dan Militer Adalah Penyangga Orde Baru

Sebab, dengan terpilihnya SBY-Jusuf Kalla sebagai pucuk pimpinan negara, masa depan bangsa dan rakyat Indonesia masih banyak mengandung segi-segi gelap. Memang, Presiden SBY selama ini telah menunjukkan sikap yang positif di berbagai bidang, yang bisa menimbulkan harapan besar kepada banyak orang bahwa ia betul-betul ia adalah mantan jenderal TNI « yang lain » dari pada lainnya, dan yang bisa memperbaiki apa yang sudah dirusak atau dibusukkan oleh Orde Barunya Suharto selama 32 tahun. Tetapi, apakah dalam perjalanan selanjutnya ia tidak akan meneruskan Orde Baru dalam bentuk baru dan cara yang lain, inilah yang kita semua harus waspadai terus-menerus.

Yang betul-betul perlu kita kuatirkan adalah bahwa dengan terpilihnya Jusuf Kjalla sebagai Ketua Umum Golkar maka bahaya restorasi Orde Baru adalah makin kelihatan nyata. Sebab, Jusuf Kalla adalah Wakil Presiden RI, yaitu orang yang menurut jabatan resminya adalah orang terdekat Presiden SBY. Sebagai Ketua Umum Golkar, partai yang mempunyai suara dominan dalam parlemen, Jusuf Kalla mengantongi kekuasaan atau pengaruh yang besar sekali dalam percaturan politik negeri kita Apalagi, ditambah dengan kedudukan Agung Leksono - yang orang Golkar juga- sebagai ketua parlemen, maka Orde Baru edisi kedua sudah hadir di ambang pintu.

Tidak boleh kita lupakan bahwa dalam jangka 32 tahun Golkar (bersama-sama TNI-AD) adalah penyangga utama rejim militer Orde Baru, dan bahwa Suharto adalah pimpinan tertinggi Golkar (ia tadinya adalah ketua dewan pembinanya). Bolehlah dikatakan bahwa Golkar dan Orde Baru adalah satu, dan bahwa TNI-AD (setidak-tidaknya, sebagian dari TNI-AD) adalah jiwa Orde Baru. Karena Orde Baru baru 7 tahun ditumbangkan, maka wajar sekalilah bahwa sisa-sisanya masih banyak atau masih kuat. Meskipun sejak 1998 telah dinyatakan dimulainya reformasi, tetapi nyatanya ada banyak sekali « reformis-reformis gadungan » yang masih menyelinap di bidang eksekutif, legislatif, judikatif negara kita.

## Jusuf Kalla Dan Restorasi Orba

Orang melihat bahwa Wakil Presiden Jusuf Kalla mempunyai kekuasaan (atau pengaruh) besar untuk menempatkan orang-orangnya (atau orang Golkar) di banyak pos penting, termasuk pimpinan BUMN dan pejabat-pejabat tingkat eselon satu di berbagai bidang. Jaring-jaringan Golkar (tersembunyi atau terang-terangan) di banyak alat negara dan aparat pemerintahan, yang sudah dibangun selama puluhan tahun Orde Baru, akan tetap tersebar luas dan kuat dengan dilangsungkannya Pemilihan Kepala Daerah tidak lama lagi. Jusuf Kalla sudah menyatakan bahwa untuk Pilkada yang akan datang target Golkar adalah meraih 60 % suara. Kalau target untuk Pilkada ini terlaksana maka munculnya Orde Baru jilid II menjadi semakin kongkrit. Dan dengan makin terkonsolidasinya Orde Baru jilid II maka kemungkinan Jusuf Kalla menjadi presiden untuk menggantikan SBY dalam tahun 2009 makin menjadi lebih besar. Kalau sudah begitu, maka akan 100 % komplitlah restorasi Orde Baru !!!

Kemungkinan akan « come back »-nya Orde Baru (dengan wajah baru, atau gaya baru, tetapi dengan « jati-diri » yang lama atau hakekat dan isi yang dulu-dulu juga) adalah keadaan yang sungguh-sungguh mengerikan dan sekaligus memuakkan. Kita semua masih ingat dengan jelas segala kebusukan dan segala kejahatan atau kesalahan yang dilakukan selama puluhan tahun oleh tokoh-tokoh rejim Orde Baru, yang akibat parahnya atau kerusakan-kerusakan besarnya masih kita saksikan dengan jelas sampai sekarang.

Sulitlah kiranya disangkal bahwa kerusakan moral atau dekadensi akhlak besar-besaran yang melanda bangsa kita dewasa ini adalah, pada dasarnya, produk « kebudayaan » rejim Orde Baru. Dan, kalau kita tilik dalam-dalam, nyatalah bahwa kebejatan budi pekerti yang telah menyerang secara parah kalangan « atas » ( baik militer maupun sipil), yang kebanyakan adalah tokoh-tokoh Golkar dan bekas pendukung atau simpatisan Orde Baru, merupakan akibat buruk sistem rejim Suharto.

Mengingat itu semuanya, maka kelihatanlah dengan jelas, bahwa aksi-aksi para mahasiswa yang menuntut diajukannya Suharto di depan pengadilan adalah benar, adil, dan sesuai dengan harapan banyak orang. Diajukannya Suharto di depan pengadilan yang independen dan transparan adalah salah satu di antara langkah-langkah penting untuk mencegah terulangnya Orde Baru. Dan, mencegah restorasi Orde Baru adalah untuk menyelamatkan bangsa dewasa ini dan generasi yang akan datang dari kerusakan, atau pembusukan, atau dekadensi.
