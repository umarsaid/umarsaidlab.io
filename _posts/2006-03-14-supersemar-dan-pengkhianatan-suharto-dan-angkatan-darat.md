---
layout: post
title: Supersemar dan pengkhianatan Suharto dan Angkatan Darat
date: 2006-03-14
---

Dalam kesempatan untuk mengenang kembali Supersemar, yang selama 32 tahun Orde Baru telah dipamerkan oleh pendukung-pendukung rejim militer Orde Baru sebagai peristiwa besejarah untuk “menyelamatkan negara dan bangsa Indonesia”, berbagai orang telah mengemukakan pendapat dan perasaan mereka.

Di antara mereka itu terdapat tokoh-tokoh korban peristiwa 65 dan eks-tapol seperti Ibrahim Isa (dari Nederland), Gutav Dupe (dari Jakarta) , Utomo S (pimpinan LPR-KROB). Sejarawan Asvi Warman Adam membuat tulisan yang amat menarik sekali yang berjudul « Supersemar dan kudeta merangkak MPRS », yang mengurai berbagai persoalan dan mengajukan bahan-bahan baru untuk renungan kita bersama. Harian Kompas dan Sinar Harapan juga menyiarkan tulisan-tulisan yang menarik mengenai masalah ini.

SUPERSEMAR MENCELAKAKAN BANGSA DAN NEGARA

Dari tulisan-tulisan yang baru-baru ini dapat dibaca, jelaslah kiranya bahwa Suharto dkk (artinya : Angkatan Darat dengan dukungan berbagai golongan reaksioner dalamnegeri dan luarnegeri) telah menjadikan Surat Perintah Sebelas Maret sebagai puncak pembangkangan, pemboikotan dan pengkhianatan terhadap presiden Sukarno.

Pembangkangan, pemboikotan dan pengkhianatan terhadap presiden Sukarno ini didahului dengan pembunuhan besar-besaran terhadap 3 juta anggota dan simpatisan PKI, dan penahanan sewenang-wenang terhadap sekitar 2 juta orang tidak bersalah apa-apa, serta penyebaran terror di seluruh negeri. Ini semua dilakukan oleh golongan militer (terutama Angkatan Darat), tanpa persetujuan presiden Sukarno.

Sesudah peristiwa Supersemar (11 Maret 1966) pembangkangan dan pengkhianatan terhadap presiden Sukarno ini dilanjutkan dengan langkah-langkah Suharto dkk lainnya, dengan « membersihkan » MPRS dan DPR-GR dari golongan pro-PKI dan pro-Bung Karno, sehingga MPRS bisa sepenuhnya dikuasai dan dimanipulasi oleh Suharto dkk. MPRS yang sudah dikebiri atau dibikin loyo oleh Angkatan Darat inilah yang kemudian bisa didesak untuk mencabut kedudukan Bung Karno sebagai presiden/panglima tertinggi ABRI/pemimpin besar revolusi/mendataris MPRS.

Seperti sudah sama-sama kita saksikan sendiri, dengan diangkatnya Suharto sebagai presiden dalam tahun 1968 oleh MPRS, maka negara dan bangsa Indonesia telah dijerumuskan oleh Angkatan Darat yang dipimpin Suharto dalam masa gelap selama puluhan tahun yang penuh dengan pelanggaran HAM, kebejatan moral, kerusakan perikemunusiaan, kehancuran kehidupan demokratis, dan hancurnya persatuan bangsa. Dari segi ini dapatlah kiranya kita katakan dengan tegas bahwa Supersemar telah mencelakakan bangsa dan negara.

ANGKATAN DARAT MENGKHIANATI BUNG KARNO DAN REVOLUSI

Berbagai tulisan yang sudah disiarkan di Indonesia dan di luarnegeri menunjukkan dengan jelas tentang pengkhianatan golongan Angkatan Darat yang dipimpin Suharto ini terhadap presiden Sukarno, terutama sekali dengan menyalahgunakan Supersemar. Kejahatan Angkatan Darat ini tidak saja karena pembantaian besar-besaran terhadap anggota dan simpatisan PKI dan simpatisan Bung Karno, melainkan karena telah meneruskan berbagai kejahatan dan pelanggaran HAM selama lebih dari 30 tahun.

Kalau dihitung jumlah orang yang jadi korban pembunuhan, dan yang ditahan sewenang-wenang, dan orang-orang dari berbagai kalangan yang menjadi korban peristiwa 65, ditambah dengan kejahatan-kejahatan lainnya selama Orde Baru, maka tidak salahlah kalau ada orang-orang yang mengatakan bahwa Angkatan Darat di bawah pimpinan Suharto merupakan golongan bangsa yang telah mendatangkan malapetaka dan kesengsaraan bagi rakyatnya sendiri.

Sejarah dan praktek-praktek Orde Baru menunjukkan dengan jelas bagi banyak orang bahwa dengan menyalahgunakan Supersemar, Angkatan Darat di bawah pimpinan Suharto bukan saja telah mengkhianati Bung Karno, tetapi juga merusak cita-cita revolusi rakyat Indonesia. Angkatan Darat di bawah pimpinan Suharto bukan saja telah menghancurkan PKI dan kekuatan kiri lainnya, tetapi juga merusak secara serius dan besar-besaran banyak tatanan demokratis dari kehidupan politik.

Singkatnya, di bawah pimpinan Suharto, Angkatan Darat telah merusak Republik Indonesia, yang akibat parahnya masih kita saksikan sampai sekarang di berbagai bidang kehidupan bangsa. Kerusakan yang disebabkan berbagai kejahatan ini sudah demikian banyaknya dan juga demikian besarnya sehingga sulit untuk diperbaiki dalam jangka dekat dan waktu singkat. Banyak dari masalah-masalah parah dan rumit yang kita saksikan dewasa ini adalah warisan atau akibat dari rejim militer Orde Baru, yang dibangun oleh Angkatan Darat di bawah pimpinan Suharto.

TNI TELAH DIRUSAK OLEH SUHARTO

Peran busuk dan khianat yang sudah dimainkan oleh Angkatan Darat di bawah pimpinan Suharto yang menyalahgunakan Supersemar untuk menggulingkan presiden Sukarno dan kemudian mendirikan Orde Baru telah berakhir (secara resminya !!!) dengan jatuhnya Orde Baru. Tadinya, banyak orang mengira atau berharap bahwa TNI bisa mengubah dirinya, dan tidak berjiwa dan bertindak lagi seperti selama rejim militer Orde Baru, setelah Suharto tidak lagi menjadi presiden dan panglima tertinggi.

Tetapi, kerusakan di kalangan militer (terutama Angkatan Darat) yang disebabkan pimpinan Suharto sudah sedemikian parahnya dan pembusukan sudah sedemikian jauhnya, sehingga hanya sedikit sekali (kalau ada!) perubahan dalam sikap mental atau moral mereka. Selama 32 tahun Suharto telah memanjakan golongan militer, dan menjadikan mereka sebagai “kelas istimewa” dalam kehidupan bangsa, yang berada di atas segala golongan lainnya dalam masyarakat.

Perlakuan istimewa Suharto terhadap golongan militer ini adalah untuk “membeli” kepatuhan atau kesetiaan mereka kepadanya. Oleh karena itu, walaupun terjadi banyak kesalahan atau pelanggaran yang dibuat oleh kalangan militer selama Orde Baru , Suharto tidak (atau jarang sekali !) bertindak. Asal mereka patuh kepadanya. Itu sebabnya, maka banyak pelanggaran HAM atau penyalahgunaan kekuasaan atau korupsi, yang banyak dilakukan oleh pimpinan militer dari berbagai tingkatan dibiarkan saja dan tidak ditindak.

Sekarang, ketika resminya Orde Baru sudah gulung tikar, dan Suharto sudah dipaksa turun, maka adanya pimpinan militer seperti yang dipertontonkan panglima Kodam Jaya,Mayjen TNI Agustadi Sasongko Purnomo, adalah pertanda bahwa pada pokoknya TNI-AD masih belum mengadakan perubahan seperti yang dituntut oleh gerakan reformasi.

Menurut Detikcom 7 Maret yang lalu, “ia meminta masyarakat waspada bangkitnya kembali gerakan komunisme. Hal itu bisa dilihat dari kegiatan mereka yang belakangan ini makin nyata. Kegiatan seperti pameran budaya di TIM yang digelar korban stigma Partai Komunis Indonesia (PKI) pada 22 Februari lalu dan bedah buku sejarah BTI dan PKI karangan Pramoedya Ananta Toer, dinilainya sebagai bentuk konsolidasi partai berlambang palu dan arit itu.

“Konsolidasi PKI itu dalam rangka menyusun kekuatan dan memulihkan nama baik sebagai persiapan menghadapi Pemilu 2009. Targetnya, PKI bisa masuk dalam percaturan politik Indonesia. Karena itu, imbuhnya, semua pihak harus waspada dengan kemungkinan bangkitnya komunisme yang makin intensif melakukan kegiatan, baik terbuka maupun tertutup.

“Selain pameran budaya dan bedah buku, indikasi bangkitnya PKI bisa dilihat dari banyaknya aksi demo yang dilakukan para buruh tani dan sejumlah aktivis mahasiswa dari kelompok kiri yang intinya minta pencabutan Tap MPR 25/1966 tentang Pembubaran PKI, menghidupkan kembali organisasi komunis dan membubarkan koter. Kodam Jaya dalam hal ini intelijen tetap melakukan pemantauan terhadap aktivitas kelompok-kelompok yang dianggap garis kiri. » (kutipan selesai).

Gaya dan isi pidato Mayjen Agustadi itu mengingatkan kita kepada berbagai pernyataan dan pidato yang sering diucapkan tokoh-tokoh militer selama 32 tahun Orde Baru. Dengan terus-menerus menyebarkan racun anti-komunisme dan menjadikan PKI sebagai momok, rejim militer telah melakukan terror untuk memberantas perlawanan dan membungkam oposisi terhadap berbagai politik dan praktek mereka yang merugikan rakyat dan demokrasi.

PERINGATAN BUNG KARNO KEPADA SUHARTO

Pidato Mayjen Agustadi seperti tersebut di atas menunjukkan juga bahwa TNI-AD yang sekarang masih banyak dipengaruhi garis politik, jiwa atau semangat TNI di bawah pimpinan Suharto, yang melakukan pengkhianatan terhadap presiden Sukarno, panglima tertinggi ABRI pada waktu itu.

Dalam kaitan ini, adalah menarik untuk menyimak kembali amanat presiden Sukarno dalam upacara pelantikan Mayor Jenderal Suharto (pada waktu itu) menjadi Menteri/Panglima Angkatan Darat di Istana Negara pada tanggal 16 Oktober 1965 (jadi, 15 hari sesudah peristwa G30S).

Dokumen yang berisi amanat presiden Sukarno ini tidak banyak dipublikasikan dan bahkan sengaja “disembunyikan” oleh rejim militer Orde Baru. Teks lengkapnya, yang cukup panjang, dapat dibaca dalam buku “Revolusi Belum Selesai”, yang berisi kumpulan pidato-pidato presiden Sukarno sesudah peristiwa G30S. Berikut di bawah ini adalah sedikit cuplikan dari amanat yang panjang itu.

“Mayor Jenderal Soeharto,

Saya angkat Saudara menjadi Menteri/Panglima Angkatan Darat. Saudara bersedia mengucapkan sumpah atau janji?

Sumpah. (Jawab Mayjen Soeharto-red)

Menurut ajaran agama?

Islam. (Jawab Mayjen Soeharto-red)

Ikuti perkataan-perkataan saya.

(Sumpah diucapkan-red .)

(Sumpah selesai diucapkan –red.)

Syukur alhamdulillah, sumpah Menteri telah Saudara ucapkan.

(Kemudian presiden Sukarno bicara panjang lebar tentang revolusi Indonesia, tentang G3OS, tentang peran nekolim yang mau mengganggu jalannya revolusi rakyat Indonesia. Teks lengkapnya dapat dibaca pada halaman 21 sampai 26 buku tersebut. Dalam amanatnya itu presiden Sukarno memberi pesan kepada Mayor Jenderal Suharto sebagai berikut: )

“Saya perintahkan kepada Jenderal Mayor Soeharto, sekarang Angkatan Darat pimpinannya saya berikan kepadamu, buatlah Angkatan Darat ini satu Angkatan dari pada Republik Indonesia, Angkatan Bersenjata daripada Republik Indonesia yang sama sekali menjalankan Panca Azimat Revolusi, yang sama sekali berdiri diatas Trisakti, yang sama sekali berdiri diatas Nasakom, yang sama sekali berdiri diatas prinsip Berdikari, yang sama sekali berdiri atas prinsip Manipol-USDEK.

“Manipol-USDEK telah ditentukan oleh lembaga kita yang tertinggi sebagai haluan negara Republik Indonesa. Dan oleh karena Manipol-USDEK ini adalah haluan daripada negara Republik Indonesia, maka dia harus dijunjung tinggi, dijalankan, dipupuk oleh semua kita. Oleh Angkatan Darat, Angkatan Laut, Angkatan Udara, Angkatan Kepolisian Negara. Hanya jikalau kita berdiri benar-benar di atas Panca Azimat ini, kita semuanya, maka barulah revousi kita bisa jaya.

“Soeharto, sebagai panglima Angkatan Darat, dan sebagai Menteri dalam kabinetku, saya perintahkan engkau, kerjakan apa yang kuperintahkan kepadamu dengan sebaik-baiknya. Saya doakan Tuhan selalu beserta kita dan beserta engkau!.” (kutipan selesai)

Jadi, peristiwa ini menunjukkan bahwa Mayor Jendral Suharto sudah mengucapkan sumpah di hadapan presiden Sukarno, yang berarti bahwa sebagai Menteri dan Panglima Angkatan Darat ia seharusnya patuh kepada presiden Sukarno yang juga panglima tertinggi Angkatan Bersenjata, dan bahwa Suharto seharusnya menjalankan Panca Azimat revolusi, dan menjunjung tinggi Manipol-USDEK yang menjadi haluan negara.

Kita semua tahu bahwa justru sumpah inilah yang telah dilanggar secara khianat oleh Suharto, dan kemudian melanjutkan pengkhianatannya dengan menyalahgunakan Supersemar selama 32 tahun dalam melaksanakan politik busuk Orde Baru. Kita sekarang juga mengetahui bahwa perintah presiden Sukarno telah dikentuti saja oleh Suharto. Yaitu perintah presiden Sukarno kepada Suharto yang berbunyi “ buatlah Angkatan Darat ini satu angkatan dari pada Republik Indonesia, Angkatan Bersenjata daripada Republik Indonesia yang sama sekali menjalankan Panca Azimat Revolusi, yang sama sekali berdiri diatas Trisakti, yang sama sekali berdiri diatas Nasakom, yang sama sekali berdiri diatas prinsip Berdikari, yang sama sekali berdiri atas prinsip Manipol-USDEK”.

Suharto, dengan mendapat dukungan penuh dari Angkatan Darat, Golkar, dan berbagai kalangan reaksioner dalamnegeri (dan kekuatan nekolim luarnegeri !!!), telah membuat Angkatan Bersenjata Republik Indonesia menjadi sasaran kebencian rakyat. Suharto juga membikin Orde Baru mencampakkan Panca Azimat Revolusi, melecehkan Trisakti, membuang Nasakom, mengingkari prinsip Berdikari, dan memusuhi Manipol-USDEK.

BANYAK YANG HARUS DIROBAH DAN DIBETULKAN

Mengingat banyaknya berbagai kejahatan dan pelanggaran HAM yang dilakukan oleh golongan militer (terutama Angkatan Darat) di bawah pimpinan Suharto selama Orde Baru, maka sewajarnyalah (bahkan seharusnya !!!) bahwa teks dalam buku-buku sejarah yang dipakai untuk pelajaran di sekolah dasar, lanjutan dan perguruan tinggi yang bersangkutan dengan Suharto, Supersemar, Orde Baru dan juga Angkatan Darat perlu dirobah atau dibetulkan, menurut kebenaran sejarah.

Demikian juga segala teks dalam dokumen-dokumen negara yang masih secara tidak benar tetap memuji-muji Supersemar, dan menyanjung-nyanjung Suharto dan Orde Baru harus dibetulkan menurut kenyataan yang sudah terjadi. Begitu juga halnya dengan segala monumen atau museum atau tugu peringatan yang secara tidak benar menyajikan Suharto sebagai pemimpin Angkatan Darat yang berjasa untuk negara dan bangsa. Sebab, kenyataannya, Suharto dengan Angkatan Darat yang dipimpinnya, telah menimbulkan kerusakan-kerusakan yang parah terhadap negara dan juga menimbulkan berbagai penderitaan bagi rakyat.

Mengingat itu semua, maka perlulah kiranya kita semua sadar dan yakin bahwa, untuk selanjutnya di kemudian hari, Angkatan bersenjata Republik Indonesia harus sepenuhnya menjadi alat negara di bawah supremasi sipil, seperti kebanyakan negara demokratis lainnya di seluruh dunia. Seluruh kekuatan demokratis di Indonesia harus mencegah atau melawan bersama-sama kembalinya Dwifungsi ABRI dalam bentuknya yang bagaimanapun juga.

Kita semua (termasuk golongan-golongan yang demokratis dalam kalangan militer sendiri) harus mencegah terulangnya masa gelap Orde Baru, ketika golongan militer yang jumlahnya tidak melebihi 500.000 orang telah mengangkangi negara - secara otoriter atau despotik - dan ratusan juta rakyat (yang sekarang berjumlah lebih dari 230 juta orang), selama lebih dari 32 tahun! Pengalaman pahit bangsa ini tidak boleh berulang lagi, dalam bentuknya yang bagaimana pun juga!
