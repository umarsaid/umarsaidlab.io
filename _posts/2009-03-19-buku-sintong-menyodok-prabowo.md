---
layout: post
title: Buku Sintong menyodok Prabowo
date: 2009-03-19
---

Berhubung makin menariknya persoalan buku Letjen (Pur) Sintong Panjaitan dan persoalan kasus Prabowo maka berikut di bawah ini disajikan dua tulisan yang disiarkan oleh majalah Gatra dan Viva News. Dua tulisan yang agak panjang ini juga disajikan dalam website http://kontak.club.fr/index.htm di bawah judul « Buku Letjen Sintong Panjaitan yang membikin heboh ».

Umar Said
= = =

Majalah Gatra 19 Maret 2009]

Buku Sintong Menyodok Prabowo
Setidaknya dua kali Prabowo Subianto kena sodokan keras lewat karya buku tokoh penting di negeri ini. Yang pertama, lewat buku Detik-detik yang Menentukan karya mantan Presiden B.J. Habibie, yang diluncurkan dua setengah tahun silam. Habibie, antara lain, menceritakan kedatangan Prabowo ke Istana Merdeka, 22 Mei 1998, memprotes pencopotannya sebagai Panglima Komando Cadangan Strategis Angkatan Darat (Pangkostrad).

Pada waktu itu, Habibie baru beberapa jam menjabat sebagai Presiden RI, menggantikan Soeharto yang lengser. Habibie menulis, ia mencopot Prabowo karena mendapat laporan dari Panglima ABRI (Pangab), Jenderal Wiranto, tentang adanya pergerakan pasukan Kostrad dari luar kota menuju Jakarta tanpa sepengetahuan Pangab.

Buku Habibie itu sontak membuat Prabowo geram. Lewat konferensi pers yang digelar sepekan berselang, 29 September 2006, Prabowo membantah tudingan bahwa ia ingin melakukan kudeta. Ia juga membantah bahwa "dirinya marah-marah dan menyebut Habibie sebagai presiden naif".

Kini sodokan serupa datang lagi lewat biografi Letnan Jenderal (purnawirawan) Sintong Panjaitan, berjudul Perjalanan Seorang Prajurit Para Komando, yang diluncurkan di Balai Sudirman, Jakarta Selatan, Rabu malam pekan lalu. Bedanya, kali ini Prabowo bereaksi relatif kalem. Mungkin dia sengaja sedikit jaim alias jaga image.

"Terserah orang mau bicara apa. Ini kan negara demokrasi, setiap orang berhak menulis apa saja. Tapi dia tidak berhak mencemarkan nama baik orang lain," kata calon presiden dari Partai Gerindra itu usai meluncurkan buku Membangun Kembali Indonesia Raya di Jakarta, Kamis pekan lalu, atau sehari setelah peluncuran buku Sintong.

Dalam buku biografi setebal 520 halaman yang ditulis wartawan senior Hendro Subroto itu, Sintong memblejeti sekaligus menohok sosok Prabowo. Mulai seputar upaya Prabowo mengejar posisi KSAD, pencopotan Prabowo dari jabatan Pangkostrad, sampai penculikan sejumlah aktivis semasa Prabowo menjadi Danjen Kopassus.

Sintong, 68 tahun, juga mengungkap cerita terpendam tentang upaya counter coup d'etat ala Prabowo pada 1983. Pendeknya, Prabowo disebutkan beberapa kali melawan perintah atasan, melanggar prosedur kemiliteran, dan melakukan langkah-langkah yang bukan wewenangnya.

Toh, Prabowo tak terpancing. Ia menyatakan, pada saat ini rakyat sudah pandai menilai secara objektif. "Saya penganut falsafah Jawa: sing becik ketitik, sing olo ketoro. Artinya, yang baik akan ketahuan, dan yang buruk juga akan terlihat," ucap Prabowo.

***

Sintong, mantan Pangdam IX/Udayana yang juga mantan penasihat Presiden Habibie bidang hankam, mengisahkan dengan lugas apa yang dialami, didengar, dan dilihatnya. Misalnya soal kedatangan Kepala Staf Kostrad, Mayor Jenderal (Mayjen) Kivlan Zen, dan Danjen Kopassus, Mayjen Muchdi PR, ke kediaman Presiden Habibie di Patra Kuningan, 22 Mei 1998.

Habibie meminta Sintong menemui dua pejabat militer itu. Ternyata Kivlan dan Muchdi mendapat tugas dari Pangkostrad, Letnan Jenderal Prabowo, untuk menyampaikan surat yang diteken Jenderal Besar (purnawirawan) A.H. Nasution kepada Habibie. Surat itu sebenarnya ditulis tangan oleh Kivlan karena Nasution sedang sakit. Nasution tinggal meneken.

Surat itu berisi saran agar KSAD Jenderal Subagyo HS diangkat menjadi Pangab, sedangkan Pangab Jenderal Wiranto diangkat menjadi Menteri Hankam. Adapun Prabowo diusulkan menjadi KSAD. Selain itu, juga disarankan agar diadakan pemisahan antara jabatan Pangab dan Menteri Hankam.

Pada hari yang sama, Sintong mengisahkan, Wiranto melaporkan kepada Habibie bahwa telah terjadi pergerakan pasukan Kostrad dari luar Jakarta menuju Jakarta. Juga adanya konsentrasi pasukan di sekitar kediaman Habibie di Patra Kuningan. Semua mobilisasi pasukan itu tanpa sepengetahuan Pangab Wiranto.

Habibie memutuskan bahwa keberadaan pasukan di luar sepengetahuan Pangab itu tak dapat dibiarkan karena akan mempengaruhi para komandan lainnya untuk bertindak sendiri-sendiri dengan alasan apa saja, tanpa koordinasi lagi dengan Pangab. Lantas Habibie memerintahkan, jabatan Pangkostrad harus diserahterimakan hari itu juga, sebelum matahari terbenam. Prabowo diberi jabatan sebagai Komandan Sesko ABRI.

Sintong mengaku tidak ikut campur dalam masalah pencopotan Prabowo itu. Alumnus Akademi Militer Nasional (AMN) 1963 ini malah merasa kaget. Prabowo sendiri baru mengetahui dirinya dicopot dari jabatan Pangkostrad ketika berkunjung ke kantor Fanny Habibie (adik kandung B.J. Habibie) di kantor Otorita Batam pada 22 Mei itu, pukul 13.30.

Satu setengah jam kemudian, Prabowo meluncur ke Istana Negara dengan membawa 12 pengawal, mengendarai tiga mobil Landrover. Sintong mendapat laporan dari ajudannya bahwa Prabowo langsung naik lift ke lantai IV, tempat Habibie berkantor, tanpa diperiksa dan disterilkan. Sintong memerintahkan seorang pengawal presiden berpakaian preman melucuti senjata Prabowo secara baik-baik.

Ia lega, karena Prabowo bersedia menanggalkan kopelrim dengan pistolnya, magasin peluru, dan sebilah pisau rimba. Prabowo pun diterima Presiden Habibie, berbicara empat mata. Sintong tidak memaparkan apa isi pembicaraan itu. Sedangkan dalam buku Detik-detik yang Menentukan, Habibie memaparkan ucapan Prabowo yang bernada keras, memprotes pencopotannya sebagai Pangkostrad.

"Ini suatu penghinaan bagi keluarga saya dan keluarga mertua saya, Presiden Soeharto. Anda telah memecat saya sebagai Pangkostrad," kata Prabowo dengan nada tinggi. Lalu ada kalimat lebih ketus: ''Presiden apa Anda? Anda naif!" kata Prabowo, seperti tertulis dalam buku biografi Habibie.

***

Selain kisah tentang penculikan aktivis, ada kisah lain lagi mengenai Prabowo yang diungkap Sintong. Prabowo disebutkan pada 1983 sempat berupaya melakukan counter kudeta dan hendak menculik sejumlah jenderal yang ia duga akan melakukan kudeta. Pada waktu itu, Prabowo berpangkat kapten, menjabat sebagai Wakil Komandan Detasemen 81/Antiteror Kopassandha.

Detasemen 81 merupakan satuan yang dikehendaki L. Benny Moerdani dalam menghadapi teroris. Ketika itu, Letjen Benny menjabat sebagai Asisten Hankam/Kepala Intelstrat/Asintel Kopkamtib. Hubungan Detasemen 81 dengan pihak intelijen hankam sangat dekat. Satuan elite ini dipasok informasi oleh staf intelijen hankam, demikian pula sebaliknya.

Pada Maret 1983, menjelang Sidang Umum MPR, Komandan Detasemen 81/Antiteror, Mayor Luhut Pandjaitan, dikejutkan oleh laporan anak buahnya bahwa Detasemen 81 sedang siaga atas perintah Kapten Prabowo. Mereka sudah membuat rencana menculik Letjen Benny Moerdani dan beberapa perwira tinggi lainnya. Yaitu, Letjen Sudharmono, Marsdya Ginandjar Kartasasmita, dan Letjen Moerdiono.

Luhut mengaku tidak mengerti tentang rencana tersebut. "Kok, aneh. Ada soal begini, saya sebagai komandan kok nggak tahu," katanya. Akhirnya Luhut memberikan perintah tegas, "Nggak ada itu. Sekarang kalian semua kembali siaga ke dalam. Tidak seorang pun anggota Den 81 yang keluar pintu tanpa perintah Luhut Pandjaitan sebagai komandan."

Luhut segera memanggil Prabowo. Namun ia langsung ditarik Prabowo keluar dari kantor. "Ini bahaya, Bang. Seluruh ruangan kita sudah disadap," kata Prabowo. Pak Benny mau melakukan coup d'etat," Prabowo memberikan informasi rahasia.

"Coup d'etat apa?" tanya Luhut.

"Pak Benny sudah memasukkan senjata...," kata Prabowo.

"Senjata untuk apa?" tanya Luhut lagi.

"Ada, Bang. Senjata dari anu mau dibawa ke sini untuk persiapan coup d'etat," jawab Prabowo.

Luhut membenarkan bahwa Benny Moerdani memasukkan senjata, antara lain AK-47, SKS, dan senjata antitank. Tetapi senjata itu adalah senjata dagangan untuk Pakistan, yang selanjutnya akan disalurkan kepada pejuang Mujahiddin Afghanistan untuk melawan Uni Soviet. Operasi intelijen oleh Benny Moerdani ini dalam rangka mencari dana dan menunjukkan peran Indonesia dalam perjuangan di Asia.

Luhut kemudian melaporkan hal itu kepada Kolonel Sintong Panjaitan, Komandan Grup 3/Sandiyudha di Kariango, Makassar. Sintong dalam bukunya itu sampai menyimpulkan bahwa counter-coup d'etat yang direncanakan Prabowo merupakan rekayasa yang mirip dengan rekayasa ala Letkol Untung, Komandan Batalyon 1/Kawal Kehormatan Resimen Tjakrabirawa, ketika menculik sejumlah jenderal Angkatan Darat pada 1965.

Atas saran Sintong, Luhut kemudian melapor kepada Wakil Danjen Kopassandha, Brigjen Jasmin. Pada waktu itu, Danjen Kopassandha, Mayjen Yogie S.M., telah menjadi Pangdam III/Siliwangi, tapi jabatan Danjen Kopassandha belum diserahterimakan. Luhut dan Prabowo menghadap Jasmin.

Prabowo marah-marah karena Jasmin tidak percaya perihal rencana Benny Moerdani melakukan coup d'etat. Luhut sampai menurunkan tangan Prabowo yang menuding muka Jasmin. Setelah Luhut dan Prabowo keluar ruangan, Jasmin memanggil Luhut kembali masuk ruangan.

"Hut, untung kamu ada di sini. Ada apa dengan Prabowo? Coba kamu amati. Kayaknya dia sedang stres berat," ucap Jasmin.

Persoalan ini sampai juga ke Menhankam/Pangab, Jenderal M. Jusuf. Setelah mendengar penjelasan sejumlah jenderal, Jusuf menyimpulkan, isu rencana coup d'etat itu tidak ada dan persoalan dianggap selesai.

Namun Sintong sendiri percaya, kabar tentang kudeta 1983 itu berperan menjatuhkan karier Benny Moerdani, yang diikuti tersingkirnya sejumlah perwira yang dinilai sebagai orangnya Benny. Sintong dan Luhut merasa termasuk di antaranya.

***

Sintong menegaskan, buku biografinya tidak bernuansa politis, tidak pula bertujuan mencari popularitas. Menurut Sintong, buku itu dimaksudkan agar kebenaran diungkapkan dan ditegakkan. "Buku ini sama sekali bukan merupakan manipulasi politik untuk menghakimi atau menyalahkan seseorang atau pihak tertentu," kata Sintong.

Ia juga menampik anggapan bahwa buku itu ada yang mensponsori untuk menjegal langkah pihak tertentu, mengingat peluncurannya menjelang pemilu Toh, kesan menjegal pihak tertentu tetap saja sulit dihilangkan. Apalagi, usai peluncuran buku itu, Sintong kembali menegaskan bahwa Prabowo, juga Wiranto, harus bertanggung jawab atas sejumlah kasus pada saat mereka menjabat.

Wiranto, yang kini calon presiden (capres) dari Partai Hanura, dinilainya harus bertanggung jawab atas insiden kerusuhan Mei 1998. Dalam bukunya itu, Sintong menganggap Wiranto gagal menangani kerusuhan. Sedangkan Prabowo, capres dari Partai Gerindra yang popularitasnya tengah melesat, harus bertanggung jawab atas kasus penculikan sejumlah aktivis.

Terlepas dari apakah keduanya terlibat langsung atau tidak dalam kasus itu, sebagai pemimpin, menurut Sintong, mereka harus bertanggung jawab. "Pertanggungjawaban mereka belum selesai," katanya. Soal pertanggungjawaban ini, Prabowo mengatakan, "Terkait penculikan aktivis, saya sudah mempertanggungjawabkan di Dewan Kehormatan Perwira," ujarnya.

Wiranto juga enggan berkomentar banyak. Ia menilai, buku Sintong hanya melihat dari satu sisi, sehingga tidak mungkin mendapat kebenaran. "Saya tidak akan mempermasalahkan buku Pak Sintong. Itu sekadar menambah khazanah sejarah bangsa. Saya tidak mau capek-capek mengurusi hal itu," katanya.

Dalam pengamatan Soetojo Darsosentono, ahli komunikasi dari Universitas Airlangga, Surabaya, selama ini banyak sejarah perjalanan bangsa ditulis para pelaku sejarah. Karena bersifat pribadi, tentu ulasannya sesuai dengan pengamatan dan penilaian mereka masing-masing.

Karena itu, mengenai kebenaran sejarah dalam buku Sintong, menurut Soetojo, masih perlu kajian mendalam. "Buku itu merupakan sejarah pribadi Sintong. Soal benar atau tidaknya, biar masyarakat yag menilai, sebab buku itu sudah milik publik," Soetojo menegaskan.

Taufik Alwie, Cavin R. Manuputty, dan M. Nur Cholish Zaein

[Nasional, Gatra Nomor 19 Beredar Kamis, 19 Maret 2009]

* *
VIVAnews 20 Maret 2009

Mimpi besar Prabowonomics
Konsep ekonomi Prabowo mengundang kontroversi.

Ekonom berhaluan liberal meragukannya.

GAYANYA di panggung mirip-mirip Soekarno. Tangannya mengepal dan diangkat-angkat. Orasinya tegas dan lugas. Sindirannya pun tajam. Meski suara serak, calon presiden Prabowo Subianto tetap berusaha berteriak lantang, meledak-ledak.

”Saudara-saudara, elit di Jakarta lupa. Negara kita punya kekayaan alam. Kaya, kaya. Tetapi rakyat tidak mengalami perbaikan nasib. Sistem ekonomi kapitalis saat ini hanya dinikmati oleh segelintir orang saja. Sebagian besar tidak merasakannya. Orang tak punya uang tidak boleh hidup negeri ini.

Saya tahu isi hatimu. Kau inginkan pekerjaan yang baik dan halal. Kau ingin beri makan istri dan anakmu. Betul Ingin sekolahkan anakmu. Betul Apa Saudara mau jadi kacung terus Mau jadi bangsa miskin terus Mau anak-anak tak sekolah

Saudara-saudara, mari buat perubahan besar. Perubahan untuk masa depan anak-anakmu. Beri kesempatan pemimpin baru. Yang tak mampu minggir saja. Kembali ke rumah, ajak saudara-saudara, teman-teman, semua, untuk perubahan”

Peluh membasahi baju mantan Komandan Pasukan Khusus yang tengah berkampanye di Kota Padang tersebut. Di depan panggung, di bawah terik matahari, massa berteriak, “Hidup Gerindra. Prabowo presiden! “ Ribuan orang berseragam merah putih tumpek blek di lapangan Cimpago yang berada di bibir pantai.

***

Perubahan sistem ekonomi adalah misi besar yang digadang-gadang sang Ketua Dewan Pembina Partai Gerakan Indonesia Raya (Gerindra). Prabowo beralasan kapitalisme- liberal adalah sistem ekonomi yang salah sehingga harus dirombak. Resesi ekonomi global adalah bukti kegagalan pasar bebas tanpa kendali, sistem kapitalisme tanpa kendali, katanya di seminar yang digelar Himpunan Pengusaha Pribumi Indonesia, pada Rabu, 11 Maret 2009.

Ketua Himpunan Kerukunan Tani Indonesia (HKTI) itu sedang berada di atas angin. Krisis keuangan dan resesi ekonomi global telah menimbulkan sorotan tajam terhadap sistem kapitalis. Yang pedas dikritik bukan cuma kejatuhan bursa saham Wall Street, simbol kapitalisme dunia. Di dalam negeri, kelompok penentang kapitalisme- liberal semakin mendapat panggung.

Saat Prabowo meluncurkan buku “Membangun Kembali Indonesia Raya” pada Kamis lalu, 12 Maret, suasana Hotel Dharmawangsa terasa marak. Sejumlah rektor, profesor, elit partai dan wakil asosiasi binaan Prabowo hadir di ball- room hotel yang disulap penuh nuansa merah itu. “Saya ingin mengubah vonis bahwa negeri ini akan terus miskin” kata Prabowo.

Tepuk tangan membahana. Jenderal Prabowo yang dulu pernah dijauhi setelah dinyatakan terlibat penculikan sejumlah aktivis pro-demokrasi, kini menjadi magnet yang menyedot perhatian sementara kalangan.

Enam hari kemudian, 18 Maret, giliran kelompok Indonesia Bangkit meluncurkan buku "Ekonomi Konstitusi" di Hotel Four Seasons, Jakarta. Di sini sejumlah ekonom juga berkumpul. Terlihat ada Iman Sugema, Hendri Saparini, Revrisond Baswir, Ichsanuddin Noorsy dan lainnya. Indonesia jangan pakai tim ekonomi teh botol (teknokrat bodoh dan tolol), ujar Iman mengejek ekonom yang berhaluan neoliberal mereka yang pro pasar bebas, rezim perdagangan tanpa sekat negara, serta peran pemerintah yang minimal dalam sistem ekonomi.

Para ekonom ini dikenal menganut paham yang cenderung sosialis, nasionalistis, dan menginginkan peran negara yang lebih besar sebagai lokomotif perekonomian nasional.

Endang S Thohari dari Institute Garuda Nusantara kelompok-pemikir yang didirikan Prabowo turut hadir di sana. Menurut Endang, mereka tengah bahu membahu menggusur paham neoliberal. Berjuang bisa di mana saja, yang penting tujuannya sama. Dibekingi Prabowo, upaya kelompok ini terus bergulir.

Prabowo menyatakan tak main-main dengan gagasan besarnya. Ia mengisahkan, tekadnya menggebu setelah dia dipensiun paksa pada 1998. Saat itu ia banting setir jadi pengusaha membantu adiknya, Hashim Djojohadikusumo, yang berkibar sebagai pengusaha minyak di Kazakhstan.

Saat tinggal di Amman, Yordania, dia terperangah membaca sebuah laporan Van Zorge, konsultan politik dan bisnis di Jakarta mengenai kekayaan Indonesia yang menguap dari Bumi Pertiwi. Menurut taksirannya, dalam tempo 10 tahun sejak 1997, tak kurang dari US$ 250 miliar devisa ekspor telah terbang ke luar negeri.

Prabowo seperti mendapat amunisi kembali. Sejak 2003, dia sibuk berkeliling mengkampanyekan dampak buruk sistem kapitalisme- liberal. Setahun kemudian dia menulis buku berjudul “Kembalikan Indonesia” yang mengecam habis-habisan sistem ekonomi liberal.

Putra begawan ekonomi Sumitro Djojohadikusumo itu terus merangsek. Dia lalu menghimpun para ekonom, ahli pertanian, pengusaha dan pakar industri. Selama belasan bulan sejak 2007, Prabowo terlibat dalam berbagai diskusi intensif dengan kalangan ini. Dia kerap mengundang Kwik Kian Gie, Sri Edi Swasono, Bungaran Saragih (mantan menteri pertanian), Prasetyantoko (ekonom Atmajaya), Hendri Saparini, dan lainnya.

Kwik dan Prasetyantoko mengaku memang sering diundang Prabowo. “Saya beberapa kali datang ke rumahnya untuk diskusi dan memberi masukan” ujar Kwik kepada VIVAnews, “Apa yang diiklankan Prabowo itu sama dengan pemikiran saya”

Untuk menerjemahkan pandangannya, Prabowo dibantu Hashim, Rachmat Pambudy (ekonom IPB), Endang S Thohari (doktor Prancis ahli pedesaan), Widya Purnama (mantan Direktur Utama Pertamina), dan Rauf Purnama (mantan Direktur Utama PT Asean Aceh Fertilizer). Mereka semua tergabung dalam Institut Garuda Nusantara.

***

Konsep ekonomi ala Prabowo ini kini populer disebut Prabowonomics kemudian dituangkan dalam buku “Membangun Kembali Indonesia Raya” setebal 209 halaman. Isinya mengelu-elukan konsep pembangunan ekonomi berbasis ketahanan pangan, kedaulatan energi, serta industri nasional yang bernilai tambah. Prabowo memimpikan perekonomian yang berlandaskan sumber daya domestik seperti sumber alam, sumber daya manusia, dan sumber dana serta pasar domestik yang besar, 230 juta penduduk Indonesia.

Di atas itu, Prabowo menjanjikan sejumlah program maha ambisius. Di bidang pangan, dia berikrar akan membuka sawah dan kebun jagung masing-masing sejuta hektare, membangun pabrik pupuk urea, menambah pasokan bahan bakar gas, serta membangun infrastruktur desa.

Di bidang energi, dia berpromosi bakal mengganti bahan bakar minyak fosil dengan sumber energi nabati. Belum habis, dia juga berjanji akan membuka 4,4 juta ha kebun aren untuk bahan baku produksi bio-etanol, membangun pabrik bio-etanol berbahan baku singkong, serta mendirikan pembangkit tenaga panas bumi.

Untuk sektor industri, dia bilang bakal menggeber industri makanan, tekstil, sepatu, agroindustri, serta sumber alam yang bernilai tambah, seperti migas, tambang, energi dan komoditas. “Kita jangan cuma ekspor buah coklat dan biji sawit mentah-mentah, tetapi sudah dibuat pabrik bernilai tambah di sini” kata Endang.

Tim Prabowo percaya sektor-sektor itu mampu menggenjot pertumbuhan. Berdasarkan data Badan Pusat Statistik 2008, sektor pertanian menyumbang 14,68 persen produk domestik bruto (PDB). Ini masih kalah dari sektor industri pengolahan. Namun, jika agroindustri digabung, maka sektor pertanian akan menjadi penyumbang terbesar kue ekonomi nasional.

Supaya program itu berjalan, Prabowo mengajukan sejumlah resep. Di antaranya adalah mengerahkan BUMN sebagai lokomotif pembangunan di sektor-sektor yang menyerap banyak tenaga kerja. Kebijakan fiskal dan moneter akan dipusatkan ke sana. Pembayaran hutang luar negeri dijadwal ulang untuk menambah persediaan dana untuk melumasi berbagai program raksasa itu. Selain itu, ini dia, lahan kritis akan dibagi-bagikan ke petani.

”Pemerintah jangan cuma jadi wasit, tetapi harus turun tangan jadi lokomotif ekonomi” kata Prabowo. Dia memberi contoh pemimpin China Deng Xiaoping yang menjadikan lembaga pemerintah dan BUMN sebagai motor penggerak, sehingga ekonomi mereka bertumbuh di atas 10 persen.

Dengan berbagai konsep ini, tim Prabowo hakulyakin pada 2011 ekonomi nasional bakal tumbuh 8-9 persen. Tak cuma itu, dua tahun kemudian mereka bermimpi angka pertumbuhan akan melesat ke level di atas 10 persen. Jika itu terjadi, begitu mereka bermimpi, saat Republik berulang tahun ke-100 pada 2045, pendapatan per kapita Indonesia akan mencapai, jangan kaget, US$ 60 ribu atau Rp 720 juta per tahun.

***

Bagi kubu ekonom pro-Prabowo, ambisi itu mereka nilai realistis. Hendri Saparini, Iman Sugema, Dradjad Wibowo, Kwik Kian Gie, dan Revrisond Baswir, menilai Prabowonomics bisa dilaksanakan asal ada perubahan paradigma ekonomi.

”Argentina yang penduduknya lebih sedikit bisa tumbuh 8 persen” kata Dradjad. Meski juga mengaku bersepakat, Revrisond toh buru-buru mengingatkan, Yang penting, jangan cuma jadi jargon kampanye saja.

Tanggapan berbeda datang dari kubu ekonom neo-liberal. Mereka mengritik program Prabowo bak mimpi di siang bolong. Para ekonom jebolan Universitas Indonesia, seperti Muhammad Ikhsan, Chatib Basri, Adrian Panggabean, serta Purbaya Yudhi Sadewa dari Danareksa, meragukan target pertumbuhan ekonomi 10 persen itu. Terlalu ambisius, kata Ikhsan. Adrian dan Chatib mempertanyakan bagaimana angka itu dihitung.

Yudhi juga mewanti-wanti rencana Prabowo merestrukturisasi utang luar negeri. Jika dilakukan, menurutnya itu akan jadi pertaruhan besar bagi Indonesia. Resikonya besar. Pasar modal, obligasi dan kurs rupiah akan hancur, kata Kepala Ekonom Danareksa Research Institute ini. Jadi, kalau tak bayar utang, ekonomi Indonesia akan hancur.

Untuk sementara ini, Prabowonomics masihlah sebatas mimpi yang dianggap menjanjikan oleh sementara kalangan, dan dikecam sebagai ilusi oleh sejumlah pihak yang lain. Buktinya masih harus ditunggu. Itu pun jika purnawirawan jenderal berbintang tiga ini berhasil menang pemilu. Maka tak ada yang lebih tepat ketika Prabowo, masih dengan suara serak, merayu para pemilih di Kota Padang, “Agar konsep ini jalan, perlu kehendak politik. Karena itu, saya minta mandat dari rakyat”
VIVAnews (Heri Susanto, Elly Setyo Rini, Nur Farida Ahniar, Umi Kalsum)
