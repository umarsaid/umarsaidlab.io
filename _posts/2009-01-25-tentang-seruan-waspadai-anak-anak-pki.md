---
layout: post
title: 'Tentang seruan "Waspadai anak-anak PKI!"'
date: 2009-01-25
---

Tulisan ini dimaksudkan sebagai ajakan kepada para pembaca untuk sama-sama merenungkan secara dalam-dalam tentang pernyataan Asisten Intelijen Kasdam 1/Bukit Barisan, kolonel (Inf) Arminson., yang menekankan perlunya upaya sistematis dari “segenap komponen bangsa mencegah anak-anak PKI menyusup masuk birokrasi, apalagi sampai menjadi pejabat” dan ucapan-ucapan lainnya tentang bahaya laten komunis yang sekarang mulai muncul dengan terang-terangan dalam berbagai bentuk..

Pernyataannya tersebut disiarkan oleh harian Duta Masyarakat (18 Januari 2009) sebagai bagian dari berbagai tulisan mengenai PKI, seperti yang disajikan oleh website http://kontak.club.fr/index.htm

(harap baca “Bung Karno : sumbangan dan pengorbanan PKI besar sekali !).

Agaknya, setelah membaca dan merenungkan isi pernyataan petugas (yang cukup penting) dari Intel ini maka banyak orang bisa menarik kesimpulan bahwa selama golongan militer didominasi oleh orang-orang yang berfikiran sejenis kolonel intel tersebut di atas, maka negara dan bangsa kita akan tetap terus menanggung berbagai penyakit yang parah. Sebab, apa yang dinyatakan oleh petugas intel ini, sama sekali bukanlah pendapat atau sikap pribadinya seorang diri saja, melainkan ia berbicara dalam kualitasnya sebagai perwira militer.

Jelaslah kiranya bagi kita semua bahwa ucapan-ucapan kolonel intel yang demikian ini mencerminkan sebagian sikap atau garis politik pimpinan militer (terutama Angkatan Darat) yang selama puluhan tahun sudah bersikap anti Bung Karno dan anti-PKI, yang juga dengan kekuasaan Orde Barunya telah melakukan banyak dan bermacam-macam kejahatan dan pelanggaran HAM.

Pernyataannya itu merupakan tambahan bukti lainnya lagi, bahwa golongan militer pendukung Suharto masih tetap merupakan perusak persatuan rakyat dan bangsa Indonesia, seperti yang telah dilakukan selama 32 tahun kekuasaan rejim militer. Karena itu, kiranya pantaslah disebutkan bahwa, pada hakekatnya, pimpinan militer pendukung Suharto sebenarnya bisa dipandang sebagai musuh bangsa Indonesia.

Marilah sama-sama kita coba telaah isi pernyataan tersebut dari berbagai segi dan sudut pandang, yang selengkapnya adalah sebagai berikut :

“Asisten Intelijen Kasdam I/Bukit Barisan, Kolonel (Inf) Arminson, mengatakan, pemerintah dan segenap komponen bangsa perlu mengambil langkah-langkah antisipatif untuk mencegah agar anak-anak PKI tidak bisa mengulang kembali sejarah gelap bangsa Indonesia. Dia menilai modus perjuangan PKI agar bisa hidup lagi di Indonesia hingga saat ini juga tidak pernah berubah. Yakni selalu memanfaatkan isu kemiskinan, ketidakadilan di bidang sosial, ekonomi dan hukum, serta berupaya mendiskreditkan kelompok atau institusi yang dianggap menghambat atau mengancam perjuangannya. PKI dengan ideologi komunisme selalu berusaha menyebarluaskan kebohongan untuk mencapai tujuannya.

“Anak-anak PKI juga dididik menghalalkan segala cara untuk meraih tujuan tersebut. "Mereka pelaku berbagai tindakan kekejaman di masa lalu sehingga tak boleh diberi kesempatan untuk mengulang lagi kekejamannya itu," katanya. Langkah-langkah antisipasi untuk mencegah PKI bangkit lagi antara lain konsistensi pemerintah dalam memperjuangkan peningkatan pendidikan, kesehatan, kesejahteraan, dan rasa keadilan masyarakat guna mencegah upaya penggalangan dari kelompok komunis yang selalu memanfaatkan isu keterbelakangan, kemiskinan dan ketidakadilan yang terjadi di tengah-tengah masyarakat.

“Ditekankannya, bahaya laten komunis dengan segala tipu muslihat, kebohongan, dan kekejamannya bukan semata musuh TNI tapi musuh seluruh bangsa Indonesia dan semua pihak harus mencegah setiap upaya pihak mana pun yang ingin mencabut TAP MPRS No. XXV Tahun 1966 yang menetapkan PKI sebagai partai terlarang di Indonesia. Langkah antisipasi lain adalah dengan membangkitkan kesadaran masyarakat untuk mencegah hidupnya kembali komunis melalui sarana diskusi, seminar, penyuluhan, ceramah, kemudian mewaspadai upaya penyusupan ideologi tersebut ke tubuh berbagai komponen bangsa baik pemerintah, TNI/Polri, ormas maupun komponen bangsa lain. Harus ada upaya sistematis dari semua komponen bangsa agar anak-anak PKI tidak bisa menyusup masuk birokrasi, apalagi sampai menjadi pejabat.

"Langkah berikutnya adalah mencantumkan kembali materi pelajaran tentang bahaya laten komunis di semua lembaga pendidikan, sementara masyarakat luas harus ikut serta mewaspadai, memantau serta melaporkan kepada pihak berwajib jika melihat adanya kegiatan berkaitan dengan penyebaran ajaran atau paham komunis," katanya. Lebih jauh disebutkan, era reformasi dan keterbukaan dewasa ini telah dimanfaatkan kelompok/kalangan komunis untuk bangkit kembali. Beberapa indikasi ke arah itu antara lain dapat disimak dari pernyataan salah seorang generasi muda keturunan PKI pada 17 April 1996, bahwa "Partai sudah berdiri, 31 tahun terkubur, sekarang dibangun lagi".

“Lalu dapat pula dilihat dari bermunculannya berbagai macam organisasi massa, baik oleh generasi tua maupun generasi muda keturunan PKI dengan berbagai cover, antara lain untuk memperjuangkan hak asasi manusia. Dia juga mengingatkan agar bangsa Indonesia tidak lengah karena saat ini banyak upaya membuat jaringan nasional dan internasional dengan gerakan-gerakan terselubung dalam berbagai bentuk untuk menggalang kader PKI di seluruh Indonesia dan menyusup ke kalangan generasi muda, mahasiswa, dan pelajar dengan menggunakan jaringan baru komunis di Indonesia. Maka, sekarang kuncinya hanya tiga kata: Waspadai Anak PKI! “ (kutipan dari Duta Masyarakat selesai).

Fikiran jahat dari nalar yang tidak waras !

Adanya ucapan bahwa harus diusahakan supaya “anak-anak PKI tidak mengulangi sejarah gelap bangsa indonesia” dan “harus ada upaya sistematis dari semua komponen bangsa agar anak-anak PKI tidak bisa menyusup masuk birokrasi, apalagi sampai menjadi pejabat” mencerminkan bahwa fikiran jahat dan busuk yang berlandaskan nalar yang tidak waras masih tetap merajalela di kalangan pimpinan militer pendukung Suharto.

Ucapan semacam ini jelas-jemelas merupakan kecerobohan atau kesesatan cara berfikir yang secara gebyah-uyah menggolongkan semua anak-anak PKI sebagai orang-orang yang perlu disisihkan, atau dikucilkan, atau diperlakukan tidak adil. Menganggap bahwa semua anak-anak PKI adalah orang-orang tidak baik yang perlu diwaspadai atau dicurigai mencerminkan sikap yang gegabah (dan dungu !) serta nalar yang tidak waras (untuk tidak menggunakan kata-kata “gila”)

Sebab, apakah mentang-mentang anak-anak PKI maka mereka harus mendapat perlakuan sewenang-wenang, atau harus tidak dianggap sebagai warganegara RI yang biasa, atau tidak sebagai manusia seperti lainnya?

Perlu direnungkan oleh kita semua (terutama oleh kalangan pimpinan militer) bahwa sama sekali bukanlah kesalahan mereka bahwa sudah dilahirkan sebagai anak-anak PKI. Mereka tidak bisa memilih sendiri siapakah sebaiknya orang tua mereka.Dan, karenanya, juga bukanlah merupakan “dosa” mereka (harap perhatikan tanda kutip pada kata ini) bahwa orang tua mereka dulunya menjadi anggota atau simpatisan PKI.

Memperpanjang kejahatan rejim Suharto

Dalam kaitan ini patut dicatat bahwa menurut keterangan berbagai sumber sejarah, jumlah anggota PKI (dalam 1965) ada sekitar 3 juta, sedangkan simpatisan atau pengikutnya yang tergabung dalam berbagai macam ormas (buruh, tani, nelayan, pegawai negeri, pemuda, mahasiswa,wanita, intelektual dll) ditaksir sekitar 20 juta. Karena itu, bisalah kiranya kita perkirakan bahwa jumlah anak-anak PKI itu besar sekali dan tersebar di seluruh Indonesia.

Lebih-lebih lagi, perlulah sama-sama kita ingat bahwa puluhan juta para korban rejim militer Suharto adalah pada umumnya anggota atau simpatisan PKI yang sudah terbukti tidak bersalah apa-apa atau seujung rambut pun tidak tersangkut-paut dengan G30S. Bahkan, mereka pada umumnya terdiri dari anggota atau simpatisan kekuatan utama anti-imperialis di bawah pemimpin besar revolusi rakyat Indonesia, Bung Karno. Karenanya, menjadi anggota atau simpatisan PKI, adalah pada waktu itu sikap politik yang sah, dan sikap moral yang luhur.

Anak-anak PKI adalah manusia biasa dan juga warganegara RI

Jadi, dari sudut ini nyatalah bahwa “seruan” pimpinan militer untuk mewaspadai anak-anak PKI, adalah pandangan yang sesat dan tujuan yang sama sekali tidak menguntungkan persatuan rakyat dan juga tidak memupuk rekonsiliasi nasional .Seruan pimpinan militer yang demikian ini hanyalah memperpanjang kesalahan atau kejahatan rejim militer Suharto dan memperbesar dosa-dosanya yang sudah bertumpuk-tumpuk selama puluhan tahun.

Dengan sikap yang memusuhi anak-anak PKI yang jumlahnya tidak sedikit itu, pimpinan militer (terutama TNI AD) melestarikan dendam yang sudah berlangsung selama lebih dari tahun dan bahkan mengkobarkannya terus di kalangan generasi muda dewasa ini dan juga generasi yang akan datang. Anak-anak PKI itu adalah warganegara RI dan juga manusia biasa, yang mempunyai hak-hak fundamental, seperti yang lainnya. Karenanya, adalah hak mereka yang sah, dan kewajiban mereka yang mulia untuk melawan atau berontak terhadap perlakuan yang tidak adil yang dikenakan terhadap mereka.

Sebagian terbesar anak-anak PKI (yang jumlah pastinya sulit ditaksir) sejak kecil sampai dewasa telah hidup dalam macm-macam penderitaan yang berbeda-beda kadarnya , sebagai akibat dibunuhnya ayah atau ibu mereka atau dipecati dari pekerjaan dan ditahan secara sewenang-wenang. Dari segi ini saja sudah kelewat besar dosa pimpinan militer, karena menyiksa begitu banyak anak-anak yang tidak bersalah apa pun dan dalam jangka waktu yang sangat lama pula. Sekarang, dengan seruan “waspadailah anak-anak PKI”, berarti bahwa perlakuan tidak adil (bahkan sering tidak manusiawi) terhadap anak-anak PKI ini akan ditrapkan sepanjang hidup mereka !!!

Pimpinan militer juga mengatakan bahwa telah “bermunculan berbagai macam organisasi massa, baik oleh generasi tua maupun generasi muda keturunan PKI dengan berbagai cover, antara lain untuk memperjuangkan hak asasi manusia” dan juga mengingatkan “agar bangsa Indonesia tidak lengah karena saat ini banyak upaya membuat jaringan nasional dan internasional dengan gerakan-gerakan terselubung dalam berbagai bentuk untuk menggalang kader PKI di seluruh Indonesia dan menyusup ke kalangan generasi muda, mahasiswa, dan pelajar dengan menggunakan jaringan baru komunis di Indonesia”

Pernyataan pimpinan militer yang sejenis itulah yang dengan jelas menunjukkan kekaburan cara mereka memandang - sekaligus ketakutan mereka ! – kepada adanya kebangkitan perlawanan dari banyak kalangan masyarakat terhadap situasi dalam negeri yang semrawut dan menyengsarakan sebagian terbesar rakyat kita dewasa ini. Pimpinan militer mencurigai bahwa di belakang macam-macam gerakan massa melawan kemiskinan, pengangguran, korupsi, penyalahgunaan kekuasaan, pelanggaran HAM, ada permainan unsur-unsur PKI. Bahkan mereka (pimpinan militer) menuduh atau memfitnah – dengan gegabah atau sembarangan saja – bahwa aksi-aksi yang membela kepentingan orang banyak itu digerakkan oleh antek-antek asing dan agen dari jaringan komunis internasional.

Padahal, pimpinan militer itu sendiri tahu atau menyaksikan bahwa dalam berbagai gejolak yang melanda seluruh negeri dewasa ini ikut berpartipasi secara aktif bermacam-macam kalangan dari golongan yang beraliran Islam, nasionalis, dan juga komunis. Karena makin menghebatnya krisis ekonomi dan keuangan di tingkat nasional dan internasional, maka akan makin menghebat pulalah berbagai macam gerakan atau aksi untuk menghadapi krisis politik, ekonomi dan sosial di Indonesia, seperti yang bisa sama-sama kita saksikan sejak lama dan yang masih terus berlangsung dewasa ini.

Seruan yang anti-Pancasila dan cermin fikiran tidak waras

Karena itulah, terlepas dari ada atau tidak adanya “anak-anak PKI yang harus diwaspadai”, perjuangan berbagai kalangan di Indonesia untuk menentang segala kejahatan dan ketidakadilan dan pelanggaran HAM yang dilakukan terus oleh sisa-sisa pendukung Suharto akan tetap bermunculan bahkan akan lebih menggelora dalam berbagai bentuk dan cara. Perlawanan atau perjuangan ini dilakukan oleh bermacam-macam golongan dan kalangan dalam masyarakat Indonesia, yang mendambakan adanya perubahan-perubahan besar-besaran dan mendasar di Republik kita, dan bukan hanya oleh anak-anak PKI yang harus diwaspadai dan dikucilkan.

Mengingat hal-hal itu semualah, maka makin jelas kelihatan bahwa seruan pimpinan militer pendukung Suharto mengenai anak-anak PKI adalah betul-betul mencerminkan sikap yang anti-Pancasila, yang merusak persatuan bangsa, yang melanggar HAM, yang mencerminkan fikiran sesat dan nalar yang tidak waras.

Bangsa dan negara kita tidak membutuhkan pimpinan militer yang demikian ini. Sebab, pimpinan militer sejenis ini hanyalah menjadi penghalang kemajuan dan perubahan fundamental yang menguntungkan rakyat, bangsa dan negara. Negara dan bangsa kita membutuhkan adanya pimpnan militer yang mempunyai orientasi politik dan missi pengabdian yang searah atau sejiwa dengan yang sudah ditunjukkan (antara lain) oleh Hugo Chavez dari Venezuela atau Fidel Castro dari Kuba. Dan, bukannya pimpinan militer yang berkiblat ke Suharto atau ke jenderal-jenderal lainnya pendukung Orde Baru.
