---
layout: post
title: Seribu nisan dan pengadilan rakyat tentang Suharto
date: 2008-02-15
---

Berikut di bawah ini disajikan tiga berita dari harian Kompas, harian Analisa dan Liputan 6 SCTV mengenai aksi-aksi berbagai organisasi di kompleks Tugu Proklamasi (Jakarta) yang berkaitan dengan meninggalnya Suharto, dan tuntutan untuk mengadili kroni-kroninya. Berita Kompas (11 Februari 2008) itu adalah sebagai berikut :

“Belum genap 40 hari pascameninggalnya mantan Presiden Soeharto, suara-suara gerakan anti-Soeharto kembali meraung. Mulai Senin (11/2), kelompok yang menamakan Kesatuan Rakyat Adili Soeharto (KERAS), "menduduki" Tugu Proklamasi di Jakarta.

Bersama ratusan mahasiswa dari 13 propinsi, aktivis-aktivis dan korban-korban almarhum Soeharto, mereka menginap di Tugu Proklamasi dengan mendirikan tenda-tenda rakyat.

Puluhan tenda tampak berjajar di seberang 1000 makam simbolis yang mereka bangun pekan lalu sebagai simbol korban-korban Soeharto. Mereka akan berada di sana sampai Rabu (13/2). "Agenda ini kami namakan panggung kesaksian rakyat. Para korban-korban kejahatan Soeharto akan bercerita tentang apa yang mereka alami selama zaman Soeharto," tegas Jimmy Mapitaputty, koordinator KERAS di Jakarta.

Pesan kesaksian itu kata Jimmy, penting untuk disampaikan ke khalayak umum sebagai bentuk penolakan mereka terhadap wacana pemberian gelar pahlawan bagi almarhum Soeharto. Menurutnya, wacana tersebut sangat mengada-ada jika merujuk pada kejahatan yang telah dilakukan Soeharto selama 32 tahun memerintah.

"Karena itu, kesaksian para korban ini akan mengungkap bagaimana perlakuan Soeharto. Mulai kasus 65, atau pasca 65 seperti Malari, Petrus, Priok, Trisakti," lanjut Jimmy.

Selain panggung kesaksian, Jimmy menegaskan, pertemuan tersebut juga dimaksudkan untuk kembali menyuarakan tuntutan menangkap dan mengadili kroni-kroni Soeharto. Ia mengatakan, jika pemerintahan Susilo Bambang Yudhoyono tidak bersikap tegas untuk menguak kroni-kroni Soeharto, Keras akan turun sendiri. "Kami akan tandai rumah-rumah kroni Soeharto. Bagaimanapun, uang yang telah mereka makan itu uang negara. Dan itu harus dikembalikan, " lanjut dia dengan suara lantang.

Bahkan, Jimmy mengatakan, pertemuan 300-an orang (125 berasal dari daerah dan 175 dari Jakarta) di Tugu Proklamasi itu tidak menutup kemungkinan akan membicarakan rencana datang langsung ke kediaman keluarga almarhum Soeharto di Cendana.

Jimmy menyebut, Keras ingin menyuarakan langsung tuntutan mereka ke Cendana. "Itu kalau kekuatan sudah memungkinkan. Bukan tidak mungkin besok (hari ini) kami akan ke Cendana sebagai pemanasan sebelum rencana sebenarnya kemungkinan besar bulan depan," katanya. (kutipan dari Kompas selesai)

Pengadilan Rakyat digelar di Tugu Proklamasi

Sedangkan berita dari Liputan 6 SCTV (12 Februari) yang berjudul “Korban pelanggaran HAM gelar Pengadilan Rakyat “ berbunyi sebagai berikut :

“Korban pelanggaran hak asasi manusia (HAM) Orde Baru menggelar pengadilan rakyat di Tugu Proklamasi, Jakarta Pusat, Selasa (12/2) siang. Dalam aksinya, mereka menuntut pelanggaran HAM yang dilakukan oleh almarhum mantan Presiden Soerharto diusut tuntas.

Salah satu korban pelanggaran HAM adalah Tjasman Setyo Prawiro. Pria 86 tahun ini terpaksa mendekam selama 14 tahun di Pulau Nusakambangan tanpa proses peradilan. Tjasman ditahan atas tuduhan menyebarkan ajaran komunis. Saat Tjasman di dalam penjara, sang istri harus berjuang menghidupi 10 anak.

Lain lagi kisah Lita. Cewek bertubuh gempal ini terpaksa lahir ke dunia tanpa ditemani sang ayah yang harus bersembunyi dari para penembak misterius (petrus) pada 1983. Kisah ini kemudian dituangkan Lita dalam lagu Tirai Kelahiran. Cerita Tjasman dan Lita hanyalah sepenggal rentetan kisah para korban pelanggaran HAM Orde Baru.

Namun setidaknya kegetiran hati para korban pelanggaran HAM Orde Baru untuk sementara bisa sedikit terhibur. Sebab siang tadi, majelis hakim Pengadilan Negeri Jakarta Selatan memutuskan keenam putra putri almarhum Pak Harto sebagai ahli waris akan yang meneruskan kasus gugatan perdata. Dalam kasus perdata ini, pemerintah menggugat secara materiil kepada almarhum mantan Presiden Soeharto sebesar US$ 420 juta dan Rp 185 miliar serta gugatan immateriil sebesar Rp 10 triliun atas kasus penyalahgunaan keuangan negara melalui Yayasan Supersemar (kutipan dari Liputan 6 selesai).__,_._,___

Lima dosa Suharto, menurut Sri Bintang

Harian Analisa ( 12 Februari 2008) menurunkan berita tentang pernyataan Sri Bintang Pamungkas ya mengenai kejahatan atau kesalahan Suharto dalam rangka aksi-aksi di Tugu Proklamasi. Berita tersebut adalah seperti berikut ini :

“Meski sudah wafat, bukan berarti dosa-dosa Soeharto terlupakan begitu saja. Setidaknya ada lima poin dosa penguasa Orde Baru itu yang harus diadili. Demikian disampaikan mantan Ketua Umum Partai Uni Demokrat Indonesia (PUDI) Sri Bintang Pamungkas. Dia tetap menuntut pemerintah menggelar pengadilan mendiang Soeharto.

“Setidaknya ada lima dosa yang ditinggalkan Soeharto, mulai kasus pelanggaran HAM sampai korupsinya,” kata Sri Bintang di sela-sela aksi Kesatuan Rakyat Adili Soeharto (Keras) di Tugu Proklamasi, Jalan Proklamasi, Jakarta Pusat, Rabu (13/2).

“Kelima dosa Soeharto itu, tutur dosen UI yang sempat ditahan era Soeharto ini, pertama, banyaknya korban nyawa yang melayang di zaman Orde Baru. Antara lain korban kasus Talang Sari, Tanjung Priok, DOM di Aceh dan Irian, serta Timtim.

Kedua, banyak utang Indonesia kepada luar negeri sekitar 80 miliar dolar AS, dan setiap tahun pemerintah hanya mampu bayar bunga saja.

Ketiga, banyaknya sumber daya alam yang rusak, mulai dari hutan yang telah dikavling-kavling untuk keluarga dan kroni-kroni Cendana. Bahkan saat itu, Soeharto begitu mudahnya mengeluarkan hak pengusahaan hutan (HPH).

Keempat, merajalelanya tindak pidana korupsi, mulai dari keluarga Cendana, pejabat eksekutif, legislatif, yudikatif hingga menjalar ke masyarakat. “Bahkan sejumlah tokoh militer pun ikut-ikutan korupsi juga,” tandas Bintang.

Dan terakhir, sistem pemerintahan sentralistik, militerisme, dan otoriter. Akibatnya, banyak kekayaan daerah dikuras habis oleh pusat.

“Untuk itu, Bintang setuju dengan aksi di Tugu Proklamasi tersebut. Setidaknya, bisa mengingatkan pemerintahan sekarang untuk mengusut dan menyelesaikan kasus Soeharto. Sebab sejak era Habibie hingga sekarang belum ada yang mampu mengusut tuntas kasus pelanggaran yang dilakukan Soeharto. “Bahkan SBY hanya meminta Rp10 triliun kepada Soeharto yang sebenarnya mencapai Rp400 triliun. Kalau SBY-JK tidak mampu mengusut tuntas kasus Soeharto, lebih baik mundur!” tantang Bintang.(kutipan dari Harian Analisa selesai)


Serentetan aksi-aksi yang penting

Meskipun tidak diberitakan secara luas oleh pers Jakarta, serentetan aksi-aksi yang dilakukan oleh berbagai organisasi dan kalangan masyarakat di Tugu Proklamasi ini mempunyai arti penting sekali. Sebab, serentetan aksi-aksi tersebut merupakan kelanjutan dari aksi-aksi yang telah dilakukan di Jakarta dan tempat-tempat lainnya di Indonesia, dalam rangka menolak usul (dari Golkar dan para Suhartois lainnya) untuk mmberi gelar pahlawan nasional kepada Suharto dan menuntut diadilinya para kroni Suharto.

Selain itu, bahwa aksi-aksi tersebut dilakukan di Tugu Proklamasi mempunyai arti simbolik yang berbobot politik dan sejarah yang penting. Yang menonjol dalam aksi-aksi itu adalah telah ikut sertanya para mahasiswa dari 13 provinsi di Indonesia dan kaum muda lainnya, di samping berpartisipasinya kalangan korban peristiwa 65 yang tergabung dalam Lembaga Perjuangan Rehabilitasi Korban Rejim Orde Baru (LPR-KROB) dan YPKP (Yayasan Penelitian Korban Peristiwa 65) dll dll. Artinya, aksi-aksi politik ini mendapat dukungan yang cukup luas dari kalangan kekuatan anti-Suharto (anti-Orde Baru).

Sebab, kegiatan yang dipelopori oleh KERAS (Kesatuan Rakyat Adili Suharto) merupakan gabungan berbagai organisasi dalam masyarakat, yang membikin front luas, dan bukan hanya terdiri dari golongan “kiri” atau simpatisan PKI saja. Memang, mungkin saja ada sejumlah kecil di antara mereka yang berhaluan kiri, tetapi sebagian terbesar adalah dari segala golongan (terutama kaum muda) yang tidak menyetujui politik Orde Barunya Suharto. Oleh karena itu, bisa diharapkan bahwa front luas dari organisasi-organisasi yang anti-Suharto semacam itu dapat ditiru dan dikembangkan lebih lanjut di berbagai daerah di Indonesia.

Yang menarik sebagai kegiatan yang kreatif adalah dibuatnya 1000 makam simbolis berupa “nisan” yang melambangkan besarnya pembunuhan orang-orang tidak bersalah oleh Orde Baru dan diselenggarakannya “pengadilan rakyat” dimana para korban dapat menggugat banyak kejahatan-kejahatan yang telah dilakukan oleh rejim militer Suharto;

Didirikannya kemah-kemah rakyat di kompleks Tugu Proklamasi yang berdekatan dengan 1000 “nisan” simbolik dan “panggung pengadilan rakyat” juga mencerminkan kekreatifan para penyelenggara, yang bertekad untuk mengadakan serentetan aksi-aksi politik selama beberapa hari untuk menentang wacana pemberian gelar pahlawan kepada Suharto dan menyuarakan kemarahan rakyat atas dosa-dosa Suharto di bidang HAM dan KKN

Bangkitnya kaum muda untuk melawan sisa-sisa Orde Baru, untuk menentang wacana pemberian gelar pahlawan kepada Suharto, dan menuntut diadilinya kroni-kroni Suharto, adalah penting bagi kelanjutan perjuangan bersama melawan sisa-sisa rejim militer Suharto, terutama Golkar dan sebagian kekuatan militer yang Suhartois.

Ikut sertanya Sri Bintang Pamungkas dalam aksi-aksi di Tugu Proklamasi ini menunjukkan bahwa berbagai kalangan intelektual di negeri kita juga telah mengambil sikap yang tegas terhadap masalah Suharto dan Orde Baru. Ini memberikan indikasi bahwa perlawanan terhadap berbagai kejahatan Suharto (dan keluarganya atau anak-anaknya) akan tetap berlangsung terus di kemudian hari, selama Golkar dan kaum Suhartois lainnya masih mau mengangkangi kekuasaan politik Orde Baru jilid II di negeri kita.
