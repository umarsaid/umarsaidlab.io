---
layout: post
title: Gugatan terhadap Kodam Diponegoro menang
date: 2003-06-22
---

-- Sekitar tuduhan terlibat G30S --

Dari Lembaga Perjuangan Rehabilitasi- Korban Rezim Orde Baru (LPR-KROB) didapat informasi yang cukup penting untuk diperhatikan oleh para eks-tapol dan korban Orde Baru umumnya. Informasi ini berupa sari dari tulisan yang disiarkan oleh suratkabar Wawasan (Jawa Tengah) tanggal 19 Juni 2003 tentang persoalan aset tanah dan gedung, yang dirampas oleh Kodam Diponegoro dalam tahun 1966, dengan dalih terlibat dalam G30 S. Aset tanah dan gedung ini dimiliki oleh PT NV Seketjer Wringinsari (Kendal, Jawa Tengah) yang sahamnya dimiliki 97 warga.

Informasi tersebut menjelaskan, antara lain sebagai berikut: “Surat keterangan Pengadilan Negeri Kendal dengan jelas menyatakan bahwa baik pengurus, anggota, maupun para pemegang saham PT NV Seketjer Wringinsari tidak ada satu pun yang pernah menjalani hukuman pidana dalam kasus G 30 S. Para pemegang saham maupun ahli waris PT Seketjer Wringinsari akhirnya memenangkan sebagian gugatannya terhadap Kodam IV (dulu Kodam VII) Diponegoro di Pengadilan Negeri Kendal Jawa Tengah, 17 Juni dalam kasus perampasan aset tanah milik mereka pada tahun 1966.

”Majelis hakim yang terdiri dari Magdalena Sidabutar, S.H., Mardiyono, S.H., dan Mugiyono, S.H. memutuskan untuk mengabulkan sebagian gugatan warga, yang dalam hal ini para pemegang saham PT Seketjer Wringinsari. Dengan demikian, pihak tergugat yang terdiri dari Kodam IV Diponegoro, PT Sumur Pitu Wringin, Markas Veteran Daerah Jateng, Yayasan Rumpun Diponegoro, BPN Kendal dan BPN Jateng untuk menyerahkan tanah beserta tanaman dan bangunan yang ada di atasnya kepada penggugat.

## Pembekuan Dan Penyitaan Dalam Tahun 1966

”Kasus ini bermula dari penyitaan aset tanah milik PT Seketjer Wringinsari yang sahamnya dimiliki oleh 92 orang warga oleh Kodam IV Diponegoro pada 1966. Panglima Kodam VII Diponegoro saat itu, Mayjen TNI Soerono selaku Penguasa Perang Daerah Tingkat I Jawa Tengah dan Daerah Istimewa Yogyakarta mengeluarkan Surat Keputusan Nomor Kep-PPD/00-102/7/1966, tertanggal 22 Juni 1966 yang isinya antara lain mengenai pembekuan dan penyitaan hak milik badan hukum NV Perkebunan Seketcer Wringinsari Kendal karena terlibat G 30 S/PKI.

”Dalam kenyataannya, sesuai dengan Surat Keterangan Pengadilan Negeri Kendal, disebutkan bahwa para pengurus, anggota, dan pemegang saham PT NV Seketjer Wringinsari tidak ada yang pernah menjalani hukuman pidana dalam kasus G30 S.

”NV Seketjer Wringinsari adalah perusahaan perkebunan pribumi yang didirikan pada tahun 1956 oleh 92 orang. Perusahaan ini memiliki aset berupa tanah seluas 615 hektar yang terbagi dalam lima afdeling, serta bangunan gedung sebagai sarana usaha. Dengan penyitaan tersebut, aset milik NV Seketjer Waringinsari berada dalam penguasaan Kodam VII/ IV Diponegoro, sehingga mereka merasa dirugikan, baik secara materiil maupun moril.

## Perbuatan Melawan Hukum

”Dalam pertimbangan hukumnya, Majelis Hakim menilai pendirian dan pemilikan aset PT Seketjer Waringinsari adalah sah secara hukum, sehingga tindakan penyitaan dan pencabutan aset yang dilakukan oleh pihak Kodam VII/ IV Diponegoro tersebut dinyatakan sebagai perbuatan melawan hukum. Sebab sesuai dengan perundang-undangan yang berlaku di Indonesia, satu-satunya pejabat yang berwenang menentukan pencabutan hak atas tanah untuk kepentingan umum adalah presiden. Selain itu, pencabutan hak atas tanah harus disertai dengan pemberian ganti rugi secara layak. Dalam kasus ini warga atau para pemegang saham tidak mendapatkan ganti rugi sepeserpun.

”Meski memenangkan tuntutan utama penggugat, majelis hakim tidak mengabulkan tuntutan lainnya, yaitu ganti rugi atas pengambilan aset selama masa penyitaan, serta tuntutan uang pemaksa dwangsom atas keterlambatan penyerahan aset tanah tersebut. Menanggapi keputusan tersebut, warga merasa puas dan langsung diekspresikan dengan melakukan sujud syukur di dalam ruang sidang. Sementara pihak tergugat Kodam IV Diponegoro, melalui kuasa hukumnya, Mayor CHK Kantor, di hadapan majelis langsung menyatakan banding.” (Disarikan dari Wawasan, 19 Juni 203)

## Peristiwa Menarik Di Bidang Jurisprudensi

Keputusan Pengadilan Negeri Kendal mengenai perkara NV Seketjer Wringinsari ini tergolong peristiwa menarik sekali dalam jurisprudensi (kumpulan keputusan hakim) di Indonesia. Sebab, perkara ini menyangkut kasus yang sudah terpendam selama lebih dari 30
tahun, yang rupanya tidak bisa dengan mudah dibongkar selama rezim militer Orde Baru berkuasa. Karena, perkara ini berkaitan dengan instansi militer yang amat kuat dan menakutkan banyak orang waktu itu, yaitu Kodam Diponegoro. Ditambah lagi, kasus ini adalah perkara yang berurusan dengan masalah yang juga sensitif waktu itu, yaitu tuduhan keterlibatan dengan G30S.

Peristiwa ini sangat besar artinya sebagai langkah untuk memperbaiki kesalahan-kesalahan di masa lalu, dan karenanya mengandung nilai pendidikan yang amat tinggi bagi umum tentang penegakan hukum dan perasaan keadilan. Pengadilan Negeri Kendal menunjukkan - dengan berani - bahwa tindakan Kodam Diponegoro yang menyita aset NV Wringinsari (dengan dalih terlibat G30S) adalah perbuatan melawan hukum. Dengan keputusan ini, majlis hakim pengadilan tersebut berusaha memperbaiki kesalahan Pangdam Diponegoro, yang dilakukan pada tahun 1966.

Dalam situasi di negeri kita yang baru saja (lebih kurang 5 tahun) terlepas dari belenggu rezim militer Suharto dkk, keputusan Pengadilan Negeri Kendal menunjukkan kemandiriannya dan kebebasannya sebagai institusi kehakiman dan peradilan. Sungguh, suatu tugas yang tidak mudah dilakukan!

## Sebagai Contoh Untuk Yang Lain

Berita tentang dimenangkannya gugatan para pemegang saham NV Wringinsari terhadap Kodam Diponegoro patut diketahui oleh sebanyak mungkin orang. Orang banyak perlu tahu bahwa tindakan yang sewenang-wenang, yang tidak adil, yang melawan hukum, bisa dan perlu dilawan. Perlawanan ini adalah dalam rangka besar untuk memperbaki kesalahan dan menghentikan berlangsungnya terus ketidakadilan. Memperbaiki kesalahan dan menghentikan ketidakadilan ini adalah amat diperlukan untuk bersama-sama menciptakan “good and clean governance” (pemerintahan yang baik dan bersih). Ini merupakan tugas bersama kita semua.

Kemandirian Pengadilan Negeri Kendal perlu dijadikan contoh bagi pengadilan negeri yang lain di Indonesia dalam menghadapi kasus yang serupa. Sebab, berbagai macam dan bentuk perbuatan melawan hukum, atau tindakan sewenang-wenang, telah dilakukan berbagai instansi militer semasa rezim Orde Baru. Entah berapa banyaknya tanah dan rumah (atau perusahaan) yang disita dengan tidak sah oleh oknum-oknum militer (dan instansi militer) dari orang-orang yang dengan sewenang-wenang dituduh sebagai terlibat G30S.

Kasus NV Wringinsari juga bisa dijadikan contoh bagi para korban Orde Baru lainnya untuk berani membela hak-hak mereka yang sah sebagai warganegara yang tidak bersalah apa-apa. Membela hak-hak mereka sebagai warganegara yang sah adalah kewajiban. Dan ini adalah kewajiban yang mulia. Sebab, membela hak-hak mereka yang sah berarti melawan ketidakadilan. Melawan ketidakadilan adalah perbuatan yang mendatangkan kebaikan untuk semua orang. Pada akhirnya, tidak ada fihak yang dirugikan dengan adanya perlawanan terhadap ketidakadilan.

## Dampaknya Sampai Sekarang

Kasus NV Wringinsari bisa diharapkan sebagai suatu usaha untuk bisa mendorong Kodam Diponegoro memperbaiki kesalahan (atau pelanggaran-pelanggaran) di masa lalu. Usaha ini penting sekali, karena banyaknya tindakan sewenang-wenang berbagai instansi militer (antara lain : Kodim, Korem) di berbagai bidang. Sebagian dari tindakan sewenang-wenang ini masih ada dampaknya sampai sekarang. Banyak di antara tindakan sewenang-wenang dan melawan hukum itu masih tersimpan saja dan belum dibongkar.

Kejadian yang serupa itu telah terjadi juga di berbagai Kodam di seluruh Indonesia selama rezim militer Suharto dkk berkuasa selama lebih dari 30 tahun. Karena rezim Orde Baru pada intinya (atau pada dasarnya) adalah rezim militer - dengan sokongan dari Golkar sebagai embel-embel - maka logis-logis sajalah kalau banyak kesalahan (atau kejahatan) itu dilakukan oleh oknum atau instansi militer. Karenanya, demi nama baik golongan militer, maka sudah waktunya golongan militer berusaha sekuat-kuatnya untuk mengkoreksi kesalahan-kesalahannya ini.

Dengan bantuan dan kerjasama berbagai kalangan (antara lain : organisasi eks-tapol atau para korban Orde Baru lainnya) kesalahan-kesalahan ini bisa dikoreksi bersama-sama. Kerjasama ini akan menimbulkan citra yang baru dan membangun reputasi yang baik bagi kalangan mliter (khususnya TNI-AD).

Sebab, bagaimanapun juga, negara dan bangsa kita membutuhkan adanya kekuatan militer. Tetapi militer yang profesional, yang modern, yang setia kepada prinsip demokrasi dan HAM, yang membela supremasi hukum, yang menghargai supremasi sipil, yang dekat dengan rakyat. Bukan militer type Orde Baru.
