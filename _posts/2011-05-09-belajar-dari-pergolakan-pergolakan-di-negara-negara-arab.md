---
layout: post
title: Belajar Dari Pergolakan-pergolakan di Negara-negara Arab
date: 2011-05-09
---

Tulisan ini mengajak para pembaca untuk sama-sama berusaha merenungkan atau mencoba menganalisa tentang hebohnya masalah NII, atau  kegiatan gerakan-gerakan Islam radikal di Indonesia, dan tentang makin banyaknya kalangan muda (termasuk kalangan mahasiswa) yang « tercuci otaknya » akibat praktek-praktek berbagai kalangan radikal atau fundamental,  antara lain Jemaah Islamiyah, FPI, pesantren Al Zaitun dll dll



Adalah menarik (dan juga penting !) bagi kita semua untuk mencoba menelaah bagaimana perspektif gerakan (atau golongan)  Islam radikal, Islam fundamentalis, dan Islam terroris  dalam masa-masa yang datang di Indonesia. Dan sebagai perbandingan, kiranya baik juga kita coba soroti perkembangan gerakan-gerakan  yang secara besar-besaran dan meluas sedang melanda di sebagian terbesar negara-negara Arab.



Ada masalah-masalah berat dan parah di negara-negara Arab


Apa yang terjadi di sebagian terbesar negara-negara Arab akhir-akhir ini menunjjukkan kepada dunia, bahwa ada masalah-masalah berat dan parah yang terjadi di negara-negara itu, sehingga rakyatnya – dengan dorongan angkatan mudanya ( !!!)  serta berbagai lapisan dan golongan masyarakat lainnya  -- bangkit menuntut adanya perobahan atau pembaruan atau pembongkaran.



Seandainya  masalah-masalahnya itu tidak sebegitu berat dan parah, maka tentunya rakyat-rakyat  Arab di sekian banyak negara itu tidak akan bangkit berontak atau mengadakan perlawanan besar-besaran dalam berbagai  skala seperti yang terjadi Siria, Jordania, Yaman, Bahrein, Mauritania, Maroko, Aljazair, Tunisia, Libia, Mesir dan banyak lainnya.



Kebangkitan rakyat-rakyat di begitu banyak negara Arab itu, dan lagi pula secara beruntun atau serentak, dan dalam segala macam variasi dan isi, adalah perkembangan yang dahsyat besarnya, yang  akan ada dampaknya atau pengaruhnya bagi kehidupan Islam di negara-negara itu dan juga di negara-negara lainnya.



Dalam sejarah modern selama beberapa abad yang lampau belum pernah muncul fenomena yang begitu mempesonakan banyak orang di dunia. Boleh dikatakan, bahwa tahun 2011 adalah tahun kebangkitan angkatan muda  rakyat-rakyat Arab dibanyak negara. Kebangkitan ini akan berlangsung terus, meskipun dengan mengalami pasang surut dan menghadapi macam-macam kesulitan atau berbagai  ragam masalah-masalah baru di setiap negara.



Inspirasi bagi gerakan-gerakan di negara-negara Arab lainnya



Pergolakan besar-besaran di begitu banyak negara-negara Arab sekarang ini menjadi inspirasi bagi tumbuhnya gerakan yang searah atau sejiwa bagi angkatan muda negara-negara lainnya yang menghadapi masalah-masalah yang serupa, yaitu perjuangan untuk mendatangkan perubahan atau pembaruan dalam bidang politik dan pemerintahan, sosial, ekonomi dan kebudayaan, termasuk hal-hal menyangkut  bidang kehidupan agama.



Gerakan besar-besaran rakyat-rakyat negara-negara Arab ini sekarang sedang saling melihat, saling memperhatikan, saling mengabarkan, saling membandingkan mengenai apa yang  terjadi di negara-negara masing-masing. Kemajuan-kemajuan alat komunikasi massa – televisi, Internet, telpon genggam, Face Book, SMS, twitter, You Tube – makin memudahkan proses « gethok tular » atau tukar-menukar tentang berbagai macam gerakan dan peristiwa di negara-negara Arab akhir-akhir ini.



Karena faktor-faktor itu jugalah maka peristiwa pemuda pedagang buah di kota kecil di Tunisia, Muhammad Bouazizi (26 tahun),  yang bunuh diri dengan membakar dirinya, -- sebagai protes keras terhadap sistem politik, sosial dan ekonomi yang selama 32 tahun dikangkangi oleh presiden Tunisia, Ben Ali, yang reaksioner dan koruptor besar – menjadi pemicu dan pencetus gerakan-gerakan yang sekarang sedang melanda negara-negara Arab.



Banyak orang tidak menyangka bahwa perbuatan pemuda Tunisia ini kemudian menjadi simbol kebangkitan dan trompet perlawanan rakyat-rakyat Arab lainnya. Ini semua membuktikan bahwa rakyat-rakyat di negara-negara Arab sudah bertekad untuk memerangi segala hal yang selama puluhan tahun (bahkan berabad-abad) telah membelenggu dan menindas mereka. Gerakan-gerakan ini menyerupai gunung-gunung  berapi yang meledakkan dan memuntahkan lahar yang sudah begitu lama terpendam.



Kita perlu belajar dari gerakan pembaruan di negara-negara Arab



Bagi bangsa  Indonesia apa yang terjadi akhir-akhir ini di banyak negara-negara Arab adalah penting untuk diperhatikan dan dijadikan bahan renungan atau pemikiran. Sebab, sebagian terbesar (85%) bangsa Indonesia terdiri dari pemeluk ajaran Islam dan yang menyatakan diri sebagai Muslim. Jumlah mereka ini lebih besar dari jumlah seluruh penduduk negara-negara Arab dijadikan satu. Dengan begitu Indonesia merupakan negara dengan penduduk yang beragama Islam yang terbesar di dunia.



Selama ini agama Islam di Indonesia terkenal sebagai agama yang toleran, yang moderat, dibandingkan dengan yang di negara-negara Arab pada umumnya, kecuali di Saudi Arabia, yang merupakan negara dengan sistem monarkhi mutlak. Namun, Islam di Indonesia yang dikenal sebagai toleran, telah « diganggu » oleh berbagai ragam aliran, antara lain oleh DI-TII, NII, Jemaah Islamiah, FPI dan banyak lagi lainnya.



Seperti kita ketahui semua, berlainan dengan sikap dua organisasi Islam yang terbesar di Indonesia , yaitu Nahdatul Ulama dan Muhammadiah, maka ada kalangan-kalangan Islam lainnya yang menginginkan digantinya NKRI dengan Negara Islam Indonesia, dan berlakunya Syariah Islam sebagai hukum yang mengatur kehidupan bernegara dan berbangsa di negara yang berpenduduk sekitar 240 juta orang ini, nomor ke 4 di dunia,  sesudah Tiongkok, India, Amerika Serikat.



Keinginan sejumlah kalangan untuk mendirikan Negara Islam Indonesia ini, mendapat perlawanan dari sebagian terbesar bangsa Indonesia, karena bertentangan dengan sejarah perjuangan bangsa untuk mendirikan Negara Kesatuan Republik Indonesia, dan merupakan pengkhianatan terhadap proklamasi 17 Agustus 45, serta pengkhianatan terhadap Pancasila dan Bhinneka Tunggal Ika.



Sebab,  berdasarkan pengalaman sejarah, sebagian terbesar bangsa Indonesia menganggap bahwa NKRI dengan Pancasila dan Bhinneka Tunggal Ikanya adalah bentuk negara yang paling cocok untuk negara dan bangsa kita, dan sudah tidak bisa digugat, dirombak, atau digantikan dengan bentuk dan isi yang lain lagi, apalagi dengan Negara Islam Indonesia dengan Syariah Islamnya..



(Walaupun, sekali lagi walaupun !, selama rejim militer Orde Baru sampai sekarang negara dan bangsa dirusak oleh banyaknya pelanggaran HAM, merajalelanya korupsi, rusaknya moral para tokoh-tokohnya, busuknya sistem pemerintahan, bejatnya sistem hukum dan peradilan, dan seribu satu macam penyakit parah lainnya).



Negara-negara Arab tidak lebih maju dari pada Indonesia !



Kalau kita mau berterus-terang, dan sama-sama perhatikan, maka akan nyatalah  bahwa di negara-negara Arab yang kebanyakan diperintah oleh penguasa-penguasanya yang beragama Islam  -- dan sedikit banyaknya mentrapkan unsur-unsur Islam dalam negara dan pemerintahan mereka --  tidaklah bisa dikatakan lebih baik, lebih maju, lebih berbudaya, lebih memperhatikan rakyatnya masing-masing, dari apa yang terjadi di Indonesia.



Itulah sebab-sebab utama atau latar belakang mengapa terjadi pergolakan-pergolakan besar atau perlawanan di begitu banyak negara-negara Arab. Pergolakan atau perlawanan di banyak negara-negara Arab sekarang ini umumnya bukanlah hasil rekayasa anasir-anasir subversif asing atau akibat hasutan kaum komunis Arab atau lainnya, dan bukan pula karena ulah kalangan-kalangan yang mau iseng dan hanya cari avontur tanpa tujuan yang jelas.



Gerakan besar-besaran yang melanda banyak negara Arab sekarang ini adalah gerakan yang datang dari rakyat-rakyat  Arab sendiri (terutama dari angkatan mudanya !!!). Ini bisa kita saksikan sendiri dari banyaknya tayangan di televisi atau foto-foto di media cetak. Karena datang dari sumbernya sendiri, yaitu rakyat-rakyat Arab, dan bukan barang impor (dari Barat atau dari fihak-fihak lainnya) maka pergolakan atau perlawanan ini akan bisa berlangsung lama dan menjangkau hasil-hasil yang fundamental sifatnya.



NII adalah bertolak belakang dengan tujuan gerakan di banyak negara Arab



Dari berita-berita yang banyak disiarkan selama ini, maka nyatalah bahwa gerakan atau perlawanan di negara-negara Arab sekarang ini umumnya bukanlah untuk menjadikan Islam sebagai tujuan perjuangan (antara lain negara Islam dengan Syariahnya) melainkan perjuangan untuk perubahan dan pembaruan, baik dalam sistem kenegaraan dan pemerintahan, maupun di bidang politik, sosial, ekonomi dan kebudayaan, termasuk berbagai aspek atau praktek di bidang agama Islam.



Dari sudut ini, kiranya dapat kita katakan bahwa segala impian berbagai ragam kalangan Islam radikal tentang mendirikan Negara Islam Indonesia (dengan Syariah Islamnya) adalah bertentangan sama sekali atau bertolak-belakang dengan apa yang sudah terjadi, sedang terjadi dan akan terjadi di negara-negara Arab.



Dan lagi, berbagai kalangan Islam di Indonesia, selama ini mempunyai ilusi atau kecenderungan untuk menganggap bahwa segala apa yang « berbau Arab » adalah benar, baik, dan bahkan mulia. Ini disebabkan (antara lain) :  karena bahasa Arab adalah bahasa kitab suci, dan karena Mekah terdapat  di Saudi Arabia.



Padahal sudah sejak lama banyak berita yang menunjukkan bahwa di banyak negara-negara Arab rakyat-rakyatnya hidup dalam belenggu lama dan baru, tidak mempunyai kebebasan demokratis dalam soal politik, ekonomi, sosial dan kebudayaan serta agama. Seperti dikatakan oleh Amir Musa, tokoh utama Liga Arab (organisasi besar dimana tergabung negara-negara Arab) bangsa-bangsa Arab umumnya terbelengggu oleh kemiskinan dan keterbelakangan.



Saudi Arabia bukanlah contoh yang ideal bagi Indonesia



Dalam rangka ini patutlah kiranya kita longok sedikit apa-apa yang berkaitan dengan negara Saudi Arabia, untuk pengetahuan kita bersama. Ini penting sekali, karena di Indonesia terdapat berbagai kalangan Islam yang berpendapat bahwa Saudi Arabia adalah contoh yang ideal bagaimana seharusnya negara harus diperintah atau dikelola.



Saudi Arabia bukanlah contoh yang ideal bagi bangsa Indonesia !!!!! (harap perhatikan, tanda seru lima kali). NKRI kita (walaupun masih banyak yang harus diperbaiki, karena ulah para koruptor dan orang-orang yang bejat moralnya) adalah jauh lebih baik dari pada kebanyakan negara-negara Arab, yang jumlahnya sekitar 20-an itu, apalagi kalau dibandingkan dengan Saudi Arabia. Negara yang disanjung-sanjung banyak kalangan.



Bagi kalangan Islam di Indonesia yang ingin mengetahui serba sedikit tentang Saudi Arabia dapat memperolehnya dari berbagai bahan (baik dan buruk) yang bisa dilihat melalui Google di Internet. Dengan membuka Google, maka segala macam informasi dapat didapat, tanpa sensor, tanpa pembatasan, dan dari segala fihak.



Dari Google ini, setiap orang dapat membaca bahwa Saudi Arabia adalah kerajaan monarkhi mutlak (absolute monarchy) yang tidak mempuyai parlemen seperti negara-negara lainnya di dunia, tidak mempunyai konstitusi, tidak mengadakan pemilihan umum, tidak ada partai-partai politik, hakim-hakim ditunjuk oleh raja, bahwa kedudukan wanita adalah rendah dan tidak dihargai, bahwa  raja adalah pusat besar kekuasaan, dan bahwa prinsip-prinsip demokrasi dan HAM diabaikan.



Sebagian kecil (sekali lagi sebagian kecil saja !) dari segi-segi negatif tentang kehidupan di Saudi Arabia akhir-akhir ini terbongkar dengan banyaknya kisah-kisah yang menyedihkan tentang penderitaan TKW yang bekerja dengan sengsara di negara itu dengan mendapat perlakuan yang sama sekali tidak bermanusiawi dan berkebudayaan.



Dibandingkan dengan Indonesia, Saudi Arabia adalah jauh terbelakang



Dibandingkan dengan praktek-praktek di Indonesia, yang menjalankan demokrasi (walaupun tidak atau belum sempurna) dengan dibimbing Pancasila dan dipayungi Bhinneka Tunggal Ika, maka jelaslah bahwa sistem kenegaraan atau pemerintahan di Saudi Arabia  -- dan juga di sejumlah negara-negara Arab lainnya, adalah jauh terbelakang atau sangat ketinggalan.



NKRI yang seperti sekarang ini ,bisa merupakan contoh bagi negara-negara Arab, termasuk Saudi Arabia, dan bukan sebaliknya, dalam hal sistem kenegaraan dan juga dalam mengatur kehidupan bagi rakyat, walaupun masih terdapat banyak hal yang harus diperbaiki. Oleh karena itu, angan-angan berbagai kalangan Islam radikal untuk mendirikan Negara Islam Indonesia (dengan Syariah Islamnya)  adalah bertentangan dengan aspirasi sebagian terbesar rakyat.



Dan lagi, keinginan untuk mendirikan Negara Islam Indonesia sudah kelihatan tidak sejiwa atau tidak searah dengan perkembangan gerakan-gerakan perubahan dan pembaruan di negara-negara Arab. Karena itu, mereka yang ingin mendirikan Negara Islam Indonesia patut (dan perlu sekali !!!) belajar dari isi dan tujuan gerakan-gerakan, yang dimotori oleh angkatan muda, dan yang sedang melanda di begitu banyak negara Arab.



Mungkin sekali, perubahan dan pembaruan di negara-negara Arab ini akan  memberi sumbangan dan merupakan dorongan bagi terjadinya juga perubahan di kalangan Islam di Indonesia, terutama di kalangan Islam radikal.



Di sini pulalah terletak arti pentingnya gerakan-gerakan di negara-negara Arab sekarang ini.
