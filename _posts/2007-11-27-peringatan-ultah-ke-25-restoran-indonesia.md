---
layout: post
title: Peringatan ultah ke-25 restoran INDONESIA
date: 2007-11-27
---

Peringatan ulangtahun yang ke-25 restoran koperasi INDONESIA di Paris yang diselenggarakan selama tiga malam (23, 24 dan 25 November 2007) telah berlangsung dengan sukses dan dalam suasana pesta yang meriah dan menggembirakan. Selama tiga malam itu banyak sahabat-sahabat lama restoran, termasuk berbagai tokoh penting dalam macam-macam kalangan Perancis telah berpartisipasi meramaikan tiga malam peringatan itu.

Sebagian besar keluarga besar restoran INDONESIA, yang terdiri ,dari orang-orang yang pernah bekerja dalam koperasi ini, termasuk mereka yang sudah menjalani masa pensiun, ikut merayakan ulangtahun seperempat abad restoran ini. Selama tiga malam itu juga ikut aktif dalam berbagai acara dua pendiri restoran dari 4 pendiri semula (yaitu A. Umar Said dan JJ Kusni, sedangkan yang dua lainnya, yaitu Budiman Sudharsono dan Sobron Aidit sudah wafat).

Apa yang sangat menyolok dari peringatan ulangtahun ke-25 restoran INDONESIA adalah adanya antuasiame yang sangat besar dari seluruh pegawai restoran untuk menjadikan peringatan tersebut sebagai suatu kejadian yang bersejarah dan patut menjadi kenang-kenangan kolektif dalam kehidupan dan bekerja bersama, yang penuh dengan perasaan kesetiakawanan dan persaudaraan. Seluruh pegawai restoran telah “berkiprah” di bawah pîmpinan managernya Suyoso dan wakilnya Didien.

Sambutan dari berbagai kalangan

Di antara para hadirin selama tiga malam peringatan itu terdapat tokoh terkemuka di Perancis, yaitu Louis JOINET, seorang ahli hukum yang pernah menjabat sebagai Penasehat Hukum dari 5 Perdana Menteri Perancis secara berturut-turut dan juga sebagai wakil Perancis di Komisi Hak-Hak Manusia PBB di Jenewa.

Louis JOINET dalam malam peringatan itu telah bicara di depan banyak orang tentang berbagai pengalaman yang dialami oleh restoran dalam menghadapi kesulitan-kesulitan, antara lain ketika menghadapi masalah-masalah yang berkaitan dengan politik pemerintahan Suharto waktu itu. Dari cerita-ceritanya itu kelihatanlah bahwa ia merupakan tokoh di puncak pemerintahan Perancis yang sejak awal berdirinya restoran sudah menjadi sahabat yang penting sekali.

Dari yang hadir dan juga bicara dalam malam peringatan itu terdapat berbagai tokoh masyarakat dari berbagai golongan dan aliran politik, yang selama ini menjadi sahabat restoran koperasi. Seorang wanita yang menjabat sebagai Wakil Presiden dari Dewan Ekonomi dan Sosial daerah Paris (namanya Danielle DESGUEE) menceritakan bagaimana kira-kira 26 tahun yang lalu (jadi sebelum restoran berdiri) telah membantu melakukan berbagai urusan untuk berdirinya restoran.

Juga berbicara Sergio Reggazzoni, salah seorang pimpinan organisasi humaniter Katolik yang bernama CCFD bicara tentang arti penting berdirinya restoran INDONESIA. (Catatan : CCFD adalah satu penyumbang dana yang besar untuk modal pertama restoran). Salah seorang pimpinan Liga Komunis Revolusioner (LCR), yang namanya Pierre ROUSSET telah menyatakan penghargaannya terhadap apa yang telah dicapai oleh restoran INDONESIA.

Joel LUGUERN yang sudah puluhan tahun aktif di Fondation Danielle MITTERRAND, dan beberapa teman sejawatnya, ikut meramaikan malam peringatan ini. Pimpinan Association France-Timor Leste, yang bernama Carlos Semedo, hadir dalam malam terakhir (tgl 25 November) dan menyampaikan berita bahwa Presiden Timor Leste (Jose Ramos Horta) akan berkunjung ke Paris dalam bulan Januari, dan sudah menyatakan keinginanannya untuk mengunjungi restoran INDONESIA selama kunjungannya itu.

Di antara hadirin malam peringatan itu, terdapat juga Pierre MARCI bersama istrinya, yang kedua-duanya bekerja di Comite Nasional Partai Komunis Perancis. Pierre MARCI menceritakan tentang pengalamannya di Tangerang, ketika ia dikirim oleh Partainya untuk membantu organisasi eks-tapol di bawah Ibu Sulami.

Juga patut disebut hadirnya André DUSSOLLIER yang pernah menjabat sebagai Sekretaris Jenderal dalam suatu organisasi dari Kementerian Perancis. Karena ia sudah berusia 94 tahun maka tidak bisa banyak bergerak. Namun, ia berkeras untuk bisa datang ikut merayakan ultah restoran, walaupun ia harus duduk saja di kursi rodanya.

Dari isi pidato-pidato berbagai sahabat yang ikut merayakan ultah ke-25 selama tiga malam ini, jelas sekali adanya kegembiraan mereka bahwa restoran koperasi INDONESIA di Paris sudah bisa berjalan selama seperempat abad dengan hasil-hasil yang tidak kecil. Mereka umumnya juga mengharapkan bahwa prinsip-prinsip dan orientasi yang digariskan waktu dilahirkannya SCOP Fraternité/Restoran INDONESIA, yaitu bidang ekonomi sosial atau ekonomi alternatif, bisa terus tetap dipegang dan dikembangkan.
