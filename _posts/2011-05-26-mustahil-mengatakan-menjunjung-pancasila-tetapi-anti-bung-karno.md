---
layout: post
title: Mustahil ( !!!), Mengatakan Menjunjung Pancasila Tetapi Anti-Bung Karno
date: 2011-05-26
---

Menjelang peringatan Hari Pancasila tanggal 1 Juni ada perkembangan yang amat penting dan menarik sekali.  Beberapa hari yang lalu, Presiden dan Wakil Presiden telah mengadakan pertemuan di gedung Mahkamah Konstitusi dengan 9 pimpinan berbagai lembaga tertinggi negara, untuk membicarakan soal-soal yang berkaitan dengan Pancasila.

Selain Presiden SBY dan Wapres Boediono, serta Ketua MK Mahfud MD, pertemuan dihadiri Ketua MPR Taufik Kiemas, Ketua DPR Marzuki Alie, Ketua DPD Irman Gusman, Ketua MA Harifin Tumpa, Ketua BPK Hadi Poernomo, dan Ketua KY Eman Suparman. Hadir pula sejumlah menteri KIB II, seperti Menko Polhukam Djoko Suyanto, Menko Perekonomian Hatta Rajasa, Mensesneg Sudi Silalahi, dan Seskab Dipo Alam

Pertemuan ini diadakan karena adanya gejala-gejala yang menunjukkan bahwa akhir-akhir ini perhatian atau penghayatan banyak orang terhadap Pancasila sebagai ideologi negara sudah mulai terpinggirkan, memudar atau bahkan menghilang.  Akibatnya adalah banyak sekali  permasalahan parah yang muncul dalam kehidupan bangsa, seperti yang sama-sama sering kita saksikan dewasa ini. Di antaranya adalah konflik dan kekerasan sosial yang dipicu oleh perbedaan latar belakang etnisitas, primordialisme, pertentangan  agama., merajalelanya korupsi, pelecehan hukum, dan pelanggaran HAM.

Dalam pertemuan itu telah disepakati empat poin kesepakatan, antara lain  bahwa semua lembaga negara berkomitmen untuk secara aktif mengambil tanggung jawab dalam upaya menguatkan Pancasila sebagai dasar ideologi negara, sesuai dengan peran, posisi, dan kewenangan masing-masing. Juga disepakati bahwa  Pancasila harus menjadi ideologi dan inspirasi untuk membangun kehidupan ber-bangsa dan bernegara yang rukun, harmonis dan jauh dari perilaku mendahulukan kepentingan kelompok atau golongan.

Untuk itu merekapun menggagas rencana aksi nasional. “Diperlukan rencana aksi nasional yang dilakukan oleh suatu lembaga untuk melakukan sosialisasi dan penguatan nilai-nilai Pancasila secara formal melalui pendidikan Pancasila dan konstitusi,” ujar Ketua Mahkamah Konstitusi (MK) Mahfud MD saat membacakan Joint Press Statement hasil pertemuan pimpinan lembaga Negara di Gedung MK, Jl Medan Merdeka Barat, Selasa (24/5).

Pancasila dirusak rejim militer Suharto puluhan tahun

Adanya pertemuan sebegitu banyak tokoh utama berbagai lembaga tertinggi negara (9 lembaga seluruhnya) untuk membicarakan soal-soal Pancasila itu adalah suatu perkembangan yang amat penting. Karena, pertemuan ini menunjukkan bahwa (sekarang ini !) banyak kalangan di negara kita menganggap penting sekali peran Pancasila sebagai ideologi negara dan kedudukannya sebagai pedoman kehidupan berbangsa.

Perkembangan baru semacam ini patut mendapat sambutan yang hangat dari kita semua, mengingat bahwa selama ini Pancasila hanya menjadi « pajangan » atau slogan kosong, dan tidak dipraktekkan atau tidak ada implementasinya sama sekali. Karena itulah maka moral bangsa kita sudah makin rusak atau membusuk. Ini kelihatan dalam kehidupan masyarakat sehari-hari.

Seperti yang  kita ingat kembali, selama Orde Baru di bawah rejim militer Suharto Pancasila telah disalahgunakan, dipalsu, diselewengkan, dijajakan dengan paksa, selama puluhan tahun ! Dengan nama yang gagah « Pedoman Penghayatan dan Pengamalan Pancasila » (P 4) bangsa kita telah secara paksa dicekoki indoktrinasi yang memuakkan. Selama puluhan tahun dan besar-besaran pula !

Selama 32 tahun rejim militer Suharto berkoar-koar lantang dan bicara muluk-muluk tentang Pancasila, Pancasila , dan Pancasila, namun dalam prakteknya telah melanggar, melecehkan, mengkebiri, dan memalsu Pancasila. Pada hakekatnya, rejim militer adalah perusak Pancasila. Akibatnya, selama Orde Baru itu, Pancasila telah dibenci oleh sebagian besar rakyat kita.

Karena, boleh dikatakan bahwa seluruh visi, politik, dan berbagai tindakan rejim Orde Baru adalah bertentangan sama sekali dengan isi atau jiwa Pancasila yang sebenarnya, yang asli, yang otentik, yang seperti dimaksud atau dicita-citakan oleh Bung Karno. Banyak pendukung Suharto telah  menampilkan diri sebagai penjunjung tinggi atau pendukung Pancasila, namun sebenarnya adalah justru orang-orang yang sebenarnya ( !!!) anti-Pancasila.

Rejim militer Orde Baru menyalahgunakan Pancasila

Pancasila adalah ciptaan yang besar dan gemilang Bung Karno, suatu gagasan agung yang telah menjadi pemersatu bangsa dan ideologi negara sejak proklamasi 17 Agustus 45. Tidak ada pemimpin Indonesia lainnya yang mempunyai fikiran yang sebegitu cemerlangnya seperti yang terkandung dalam Pancasila itu. Tidak ada di masa yang lalu, tidak ada sekarang dan juga tidak akan ada di masa yang akan datang.

Suharto dan para jenderalnya serta pendukungnya terpaksa menggunakan (lebih tepat menyalahgunakan) Pancasila sebagai alat untuk berusaha memupuri wajah rejimnya yang khianat dan penuh dosa itu. Oleh karena itu, selama rejim militer Suharto berkuasa, tidak henti-hentinya selalu dikoar-koarkan Pancasila, Pancasila, dan Pancasila dalam setiap kesempatan.

Namun, Pancasila yang didjajakan oleh Orde Baru beserta seluruh pendukungnya selama puluhan tahun itu bukanlah Pancasila yang asli, yang berisi semangat atau jiwa atau gagasan Bung Karno. Dan ini wajar, dan tidak bisa lain.

Sebab, hakekat atau jati-diri rejim militer Suharto adalah anti Bung Karno. Suharto bersama para jenderalnya telah mengkhianati Bung Karno, dengan menggulingkannya dan kemudian menyiksanya dalam tahanan sampai wafatnya. Padahal, Suharto dan para pendukungnya mengetahui dengan jelas bahwa Bung Karno-lah yang melahirkan Pancasila

Jadi, hanya karena terpaksa sajalah makanya rejim militer Orde Baru  menggunakan Pancasila. Keterpaksaan ini disebabkan untuk berusaha menutupi  dosa-dosa mereka dan menampilkan diri sebagai penerus atau penjunjung Pancasila.

Omong kosong : mengaku Pancasilais namun anti-Bung Karno !!!

Sikap munafik yang dipakai oleh sebegitu banyak orang (dan sebegitu lama)  semasa pemerintahan Orde Baru itu masih diteruskan oleh banyak orang dewasa ini, juga oleh pejabat-pejabat dan tokoh-tokoh, termasuk yang di kalangan eksekutif, legislatif, judikatif dan tokoh-tokoh masyarakat (partai-partai politik, agama, dan organisasi-organisasi).

Di antara mereka banyak yang mengaku Pancasilais atau menghargai Pancasila tetapi sebenarnya anti Bung Karno atau menolak ajaran-ajaran revolusionernya. Sikap yang begini adalah sikap palsu yang patut dikutuk. Sebab tidaklah mungkin seseorang bisa dikatakan menghargai sungguh-sungguh Pancasila tetapi tidak menghargai Bung Karno.

Untuk betul-betul menghargai atau menjunjung tinggi Pancasila, seseorang perlu  -- atau seharusnya -- menghargai penciptanya, yaitu Bung Karno. Orang yang anti-Bung Karno adalah,
pada hakekatnya, anti-Pacasila. Sebab Pancasila adalah pengejawantahan Bung Karno. Pancasila dan Bung Karno adalah satu dan senyawa. Bung Karno dan Pancasila tidak bisa dipisahkan satu dari lainnya.

Karena itu, bolehlah dikatakan bahwa semua pendukung setia Suharto dengan Orde Barunya adalah sebenarnya, atau pada hakekatnya, anti-Bung Karno, dan karenanya juga anti-Pancasila ! Dan orang-orang yang semacam itu pada dewasa ini masih terdapat dimana-mana.

Penguatan Pancasila dengan ajaran-ajaran Bung Karno

Mengingat itu semuanya, maka kelihatan sekali betapa pentingnya pertemuan lembaga-lembaga tertinggi negara untuk « memperkuat Pancasila » sebagai ideologi negara dan  membangun kehidupan ber-bangsa dan bernegara yang rukun, harmonis dan jauh dari perilaku mendahulukan kepentingan kelompok atau golongan. Untuk itu telah dicapai 4 poin kesepakatan yang berisi komitmen untuk melaksanakannya. Bahwa ada tekad yang begitu itu dari berbagai lembaga tinggi negara adalah suatu hal yang sangat menggembirakan.

Sudah diputuskan oleh berbagai lembaga tertinggi negara itu untuk melancarkan kampanye nasional penguatan Pancasila ini, antara lain melalui pendidikan. Ini merupakan langkah penting, karena banyak golongan-golongan dalam masyarakat sudah tidak mengenal sama sekali isi Pancasila, apalagi mengerti arti jiwanya. Selain itu, sebagian terbesar pembesar-pembesar atau tokoh-tokoh (di pemerintahan, di DPR dan DPRD maupun di berbagai organisasi) hanya mengenal saja kata          Pancasila tetapi tidak bisa  (dan tidak mau !) mengimplementasikannya, baik dalam pekerjaan mereka maupun dalam kehidupan sehari-hari.

Namun, usaha penguatan Pancasila ini akan kandas di tengah jalan, atau tidak bisa mencapai tujuannya, kalau ditangani oleh orang-orang jang masih menjadi simpatisan Suharto atau pendukung Orde Baru dan anti Bung Karno. Penguatan Pancasila tidak mungkin dilakukan oleh atau dengan oknum-oknum yang menentang atau tidak menyukai ajaran-ajaran revolusioner Bung Karno.

Pancasila, yang sekarang ini kelihatan memudar atau redup apinya, akibat pengrusakan oleh Orde Baru beserta pendukung-pendukungnya,  hanyalah  bisa dikuatkan dan dikembalikan keluhurannya sebagai ideologi negara dan bangsa, dengan sungguh-sungguh melaksanakan jiwa ajaran-ajaran Bung Karno. Pancasila tidak bisa dikuatkan tanpa menyatukannya dengan ajaran-ajaran Bung Karno.

Mempelajari  fikiran-fikiran Bung Karno untuk mengerti Pancasila

Keputusan 9 lembaga tertinggi negara tentang penguatan Pancasila beberapa hari yang lalu adalah amat penting dan juga bersejarah. Sebab, banyaknya masalah-masalah besar dan parah di bidang  politik, ekonomi, sosial, kebudayaan, keagamaan, dan moral yang sedang kita hadapi bersama dewasa ini  adalah bukti yang jelas bahwa itu semua adalah akibat tidak dipatuhinya dan tidak dipraktekannya Pancasila.

Jelaslah bahwa penguatan Pancasila adalah salah satu jalan utama untuk menanggulangsi berbagai masalah (penyakit) bangsa dan negara dewasa ini. Dan penguatan Pancasila ini tidak bisa dilakukan tanpa menggunakan ajaran-ajaran Bung Karno.  Untuk bisa menggunakan ajaran-ajaran Bung Karno secara baik, perlulah berbagai ajaran-ajarannya dipelajari atau didalami. Tanpa mempelajari ajaran-ajaran Bung Karno atau mendalami jiwanya, tidak mungkinlah seseorang bisa mengatakan bahwa ia mengerti kebesaran dan keagungan Pancasila.

Dengan akan dilancarkannya  kampanye nasional untuk penguatan Pancasila, maka usaha bersama untuk mensosialisasikan karya-karya atau fikiran-fikiran revolusioner Bung Karno adalah mutlak perlu dijalankan. Dengan menyimak isi buku-buku Dibawah Bendera Revolusi (dua jilid) atau Revolusi Belum Selesai (juga dua jilid), atau bahan-bahan lainnya, maka kita akan mengerti mengapa Bung Karno sering mengatakan bahwa Pancasila adalah kiri dan  anti-imperialis (kalau dengan bahasa yang umum sekarang anti-neo-liberalisme)

Menurut fikiran dan jiwa Bung Karno, Pancasila mencita-citakan terciptanya masyarakat adil dan makmur, atau masyarakat sosialis à la Indonesia, yang melaksanakan gotong-royong, dan berdasarkan moral publik yang luhur.

Kalau kita lihat situasi negara dan bangsa yang serba rusak dan membusuk dewasa ini, maka jelaslah bahwa Pancasila yang seperti digagas Bung Karno itulah yang merupakan alat atau senjata di tangan rakyat untuk meneruskan revolusi yang belum selesai guna mengadakan perubahan-perubahan fundamental di negeri kita.
