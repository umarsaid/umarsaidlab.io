---
layout: post
title: Kalau Tutut menjadi calon presiden
date: 2003-01-27
---

Munculnya berita dalam berbagai media massa di Indonesia mengenai dicalonkannya Tutut (Siti Hardiyanti Rukmana) sebagai presiden oleh Partai Karya Peduli Bangsa (PKPB) cukup menarik perhatian banyak kalangan sehingga menjadi topik pembicaraan yang cukup ramai dalam masyarakat. Kelihatannya, masalah pencalonan Tutut sebagai presiden ini akan bisa terus jadi pembicaraan ramai di banyak kalangan, sampai pemilihan presiden secara langsung dalam bulan Juli tahun 2004.

Adalah jelas bahwa diumumkannya Tutut menjadi calon presiden RI telah mengejutkan banyak orang, baik bagi yang menganggapnya sebagai peristiwa yang menggembirakan maupun bagi yang mendengarnya dengan perasaan kemarahan. Karenanya, bisalah dimengerti bahwa berita tentang pencalonannya sebagai presiden ini telah mengundang banyak komentar yang macam-macam. Munculnya berbagai pendapat tentang hal-hal yang berkaitan dengan masalah penting ini adalah gejala yang baik. Dan, makin ramai orang membicarakannya, makin menguntungkan bagi bangsa kita sebagai pendidikan politik, sebagai usaha untuk meningkatkan kesadaran berbagai kalangan, sebagai sarana untuk mengajak sebanyak mungkin orang ikut memikirkan persoalan-persoalan negara dan bangsa.

Karena, diumumkannya pencalonan Tutut sebagai presiden RI, adalah tambahan pertanda – atau tambahan bukti - bahwa bangsa Indonesia dewasa ini betul-betul sedang menghadapi penyakit parah, karena rongrongan sisa-sisa Orde Baru dan juga karena kegagalan kekuatan anti-Orde Baru untuk menjalankan reformasi. Marilah sama-sama kita simak sebagian dari aspek-aspeknya atau fenomenanya, yang antara lain sebagai berikut :

## Orde Baru Masih Hidup Dan Kuat

Pencalonan Tutut sebagai presiden RI oleh Partai Karya Peduli Bangsa, yang diketuai mantan Jenderal Hartono - yang juga mantan KSAD dan Menteri Penerangan dan Menteri Dalamnegeri di zaman rezim militer Suharto dkk. – adalah salah satu di antara berbagai manifestasi bahwa Orde Baru masih hidup dan juga masih kuat dewasa ini. Suharto sudah secara resmi digulingkan dalam tahun 1998, tetapi dalam lima tahun, sisa-sisa kekuatan Orde Baru masih bisa mengadakan beraneka-ragam kegiatan untuk menghalangi usaha reformasi di segala bidang, dalam rangka mempertahankan kepentingannya. Ini terjadi selama pemerintahan Habibi, Abdurrahman Wahid, dan juga Megawati-Hamzah sekarang ini.

Jaring-jaringan atau infrastruktur kekuasaan rezim militer Orde Baru, yang sudah dibangun selama 32 tahun dengan cara-cara yang luarbiasa bengisnya, ternyata tidak mudah dihancurkan - apalagi dalam waktu lima tahun saja ! - oleh berbagai pemerintahan yang menggantikannya. Infrastruktur kekuasaan ini, dalam jangka lama sekali, telah memupuk mentalitas atau membina moral Orde Baru - melalui berbagai bentuk indoktrinasi dan manipulasi - di kalangan yang luas sekali. Seperti yang sama-sama kita saksikan sendiri selama ini, mentalitas atau moral para tokoh utama Orde Baru ini tidak bisa kita golongkan sebagai mentalitas terhormat atau moral luhur dan terpuji.

## Tutut Adalah Tokoh Sisa Kekuatan Orde Baru

Sudah banyak komentar yang kita baca dalam berbagai media mengenai jatidiri Tutut. Ada yang menganggap bahwa ia benar-benar bisa menjadi tokoh nasional, kalau tidak dalam tahun 2004, ya dalam tahun 2009 atau 2014 atau 2019. Ada yang melihatnya sebagai orang yang lebih mudah berkomunikasi dengan enak daripada Megawati. Ada yang menganggapnya sebagai orang yang santun dan berjiwa sosial. (Dan banyak komentar lainnya yang menyanjung-nyanjungnya).

Bahwa Tutut dicalonkan sebagai presiden oleh mantan KSAD Jenderal R. Hartono adalah bukan sesuatu kebetulan dan mengherankan. Sebab, seperti sudah diketahui banyak orang, jenderal (purn) R. Hartono adalah termasuk di antara tokoh-tokoh TNI-AD yang paling setia kepada Suharto dan juga kepada Golkar. Kita masih ingat ketika ia sebagai pejabat tinggi militer mengatakan bahwa “militer adalah juga Golkar”. Di dalam jajaran pengurus utama partainya (PKPB) juga terdapat banyak mantan perwira tinggi yang berbintang satu, dua bahkan tiga, yang kebanyakan pernah menduduki jabatan penting di Golkar atau mempunyai hubungan erat dengan Golkar. (Sekjen PKPB adalah mantan Letnan Jenderal Ary Mardjono, yang pernah menjabat sebagai sekjen Golkar juga).

PKPB telah didirikan dengan restu Suharto. Bahkan kata “Karya” dalam nama Partai Karya Peduli Bangsa adalah usul dari Suharto Dan pencalonan Tutut sebagai presiden RI juga sudah dikonsultasikan oleh R. Hartono kepada Suharto. Jadi, jelaslah bahwa ada hubungan yang erat antara PKPB dengan keluarga jalan Cendana. Dari semua ini dapat dilihat bahwa pencalonan Tutut sebagai presiden adalah salah satu gejala dari usaha membangkitkan kembali kekuatan Orde Baru.

## Tutut Adalah Simbul (Negatif) KKN

Dicalonkannya Tutut oleh PKPB sebagai presiden RI adalah suatu petunjuk bahwa sebagian bangsa kita memang sedang menghadapi penyakit parah. Sebab, sudah jelas bahwa Tutut adalah contoh yang paling menonjol dari merajalelanya KKN selama Orde Baru (rezim militer) berkuasa. Tutut bisa menjadi “tokoh” politik (pimpinan Golkar) dan “pengusaha” yang besar dan kaya-raya, karena ia adalah anak Suharto. Tutut bukanlah apa-apa, seandainya ia adalah orang biasa, yang tidak ada sangkut pautnya kekeluargaan dengan “tokoh” yang pernah menjadi orang terkuat di Republik Indonesia.

Tutut bisa menjadi seperti sekarang, yang memiliki kekayaan dan jaring-jaringan “business” yang luas, adalah justru karena ia – dengan persekongkolan para kroni, para konglomerat hitam dan para cukong – dapat menggunakan (dan menyalahgunakan!) kekuasaan dan pengaruh bapaknya, yang kebetulan jadi presiden, panglina tertinggi ABRI, ketua Dewan Pembina Golkar, serta mendapat julukan “bapak pembangunan”.

Singkatnya, Tutut adalah contoh yang paling gemilang dari praktek korupsi, kolusi dan nepotisme (KKN) yang pernah dilakukan banyak tokoh Orde Baru selama puluhan tahun dan secara menyolok pula. Tutut adalah bukti atau produk yang paling gamblang dari busuknya sistem kekuasaan Orde Baru (rezim militer) dan buruknya moral para tokohnya. Sebab, di belakang kemegahan sukses-sukses yang dicapai Tutut dalam “business” maupun politik berjejeran sederetan panjang praktek-praktek yang mencerminkan moral yang rendah dan hina. Kekayaan yang bisa diraupnya secara tamak, sebagian terbesar telah “dijarahnya” secara halus dan lihay, tanpa menghiraukan apakah merugikan kepentingan negara dan bangsa atau tidak.

## Rakyat “Merindukan” Kembali Orde Baru?

Adalah menarik sekali mendengar penjelasan pemimpin PKPB mantan jenderal R. Hartono bahwa pencalonan Tutut sebagai presiden RI adalah, antara lain, karena ada rakyat yang merindukan kembalinya zaman Suharto, berkat adanya kestabilan situasi dan ekonomi yang baik. Ucapan mantan jenderal R. Hartono ini perlu ditanggapî secara serius oleh banyak kalangan, supaya masalahnya menjadi jelas bagi banyak orang.

Selama Orde Baru memang terasa ada kestabilan politik, dan keamanan pun bisa dijamin di seluruh Indonesia. Tetapi, kestabilan politik ini adalah kestabilan yang didasarkan sistem kekuasaan “tangan besi”, atau diktatur militer yang menjalankan cara-cara fasis selama puluhan tahun. Atas nama kestabilan politik, rezim militer Orde Baru telah mencekik kehidupan demokratis, telah membrangus kebebasan berpolitik, membatasi kebebasan pers dan menyatakan pendapat, dan menggilas oposisi secara kejam. Selama Orde Baru DPR dikuasai secara mutlak, sehingga tinggal menjadi stempel karet saja atau hanya dibikin sebagai corong rezim militer.

Selain partai Golkar, PPP dan PDI, mendirikan partai politik yang lain adalah sesuatu yang berbahaya. Demikian juga bagi kaum buruh, tani, pemuda, wanita, pegawai negeri, rezim militer sudah menyediakan organisasi mereka masing-masing. Boleh dikatakan bahwa kehidupan bangsa dan negara di zaman Orde Baru adalah kehidupan yang “digiring” dan “dibatasi” dengan cara-cara yang sama sekali tidak demokratis.

Oleh karena itu, walaupun selama rezim militer berkuasa telah terjadi banyak pelanggaran HAM dan bermacam-macam penyalahgunaan kekuasaan, maka kejahatan-kejahatan ini tidak pernah bisa dibongkar. Contoh yang menyolok adalah kasus pembunuhan besar-besaran tahun 65 dan pemenjaraan ratusan ribu tapol tidak bersalah selama bertahun-tahun. Kestabilan politik selama Orde Baru adalah kestabilan semu atau kestabilan palsu yang berdasarkan penindasan, berlandaskan kekerasan, dan berselimutkan ketakutan. Kestabilan diujung bayonet, yang harus dibayar mahal, dengan darah dan airmata, oleh rakyat Indonesia selama puluhan tahun.

## Tutut Bukan Sosok Yang Patut Jadi Presiden

Dikatakan juga oleh tokoh-tokoh PKPB bahwa ada rakyat yang merindukan kembali zaman Suharto karena ekonomi semasa itu terasa lebih baik bagi rakyat. Apa yang mereka katakan itu tidak sepenuhnya benar. Bahwa selama Orde Baru ada sebagian golongan masyarakat yang mengecap kehidupan yang enak, adalah kenyataan yang sulit dibantah. Bahkan ada juga sebagian kecil golongan elite yang bisa hidup dengan mewah. Mereka ini, merupakan benalu yang hidup dengan menghisap dan merugikan secara besar-besaran kepentingan negara dan bangsa.

Sekarang ini, dan sudah sejak beberapa tahun, keadaan ekonomi bangsa morat-marit. Utang luarnegeri dan dalamnegeri sudah bertumpuk-tumpuk menggunung, dan pengangguran sudah mencapai 40 juta orang. Tidak dapat disangkal bahwa sebagian terbesar keadaan buruk di bidang ekonomi sekarang ini adalah akibat dari sistem ekonomi dan sistem politik yang salah semasa Orde Baru, yang diwarisi dan diteruskan oleh pemerintahan-pemerintahan yang silih berganti sesudahnya. Kerusakan parah di bidang moral yang disebabkan oleh buruknya sistem politik Orde Baru merupakan fak
