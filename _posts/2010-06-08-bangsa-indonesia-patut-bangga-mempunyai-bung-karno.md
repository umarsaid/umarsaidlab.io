---
layout: post
title: Bangsa Indonesia patut bangga mempunyai Bung Karno
date: 2010-06-08
---

« In a thousand years, there will only be one Sukarno »



Hari kelahiran Bung Karno pada tanggal 6 Juni 1901 sudah berlalu.  Kita bisa duga bahwa tidak sedikit orang  ingat kepada hari yang bersejarah bagi bangsa kita itu, dan karenanya mengadakan kegiatan  dengan berbagai cara. Terutama bagi kalangan rakyat yang sedang berjuang untuk perubahan besar dan melawan segala kekuatan reaksioner di tingkat nasional maupun internasional, ingat kepada sosok revolusioner Bung Karno adalah soal wajar, benar dan, bahkan, sudah selayaknya.

Sebab, kelahiran Bung Karno pada tanggal 6 Juni 1901 di Surabaya, sebagai anak seorang guru, ternyata kemudian merupakan anugerah besar yang bersejarah bagi bangsa Indonesia. Rakyat Indonesia patut sekali bangga dan kagum serta hormat kepada satu-satunya pemimpin yang seagung itu. Sejak muda remajanya, ketika masih berumur belasan tahun, ia sudah belajar tentang perjuangan untuk rakyat dari pemimpin besar Sarekat Islam, HOS Tjokroaminoto. Dalam umur sekitar 25 tahun ia sudah sering menulis artikel-artikel penting untuk majalah dan suratkabat, antara lain tulisan besar yang berjudul « Nasionalisme, Islamisme dan Marxisme ».

Kemudian dalam kegiatannya waktu di Bandung ia mendirikan PNI  dan ditangkap dan dipenjarakan oleh pemerintahan kolonial Belanda karena dianggap berbahaya. Waktu itulah ia mengucapkan pleidooinya  yang terkenal « Indonesia menggugat » di depan pengadilan kolonial. Sesudah itu Bung Karno dibuang ke pengasingan  di Endeh  (Flores) sejak 1933 sebelum dipindah ke pengasingan di Bengkulu  antara tahun 1938-1942.

Sampai tahun 1945 Bung Karno bekerja keras bersama teman-teman seperjuangannya – selama pendudukan tentera Jepang -- untuk mempersiapkan kemerdekaan Indonesia.
Dan sejak tahun 1945 itulah Bung Karno muncul sebagai tokoh perjuangan bangsa yang lebih besar lagi dengan mencetuskan gagasannya yang cemerlang dan bersejarah, yaitu Pancasila, dan juga membacakan proklamasi (bersama Bung Hatta) kemerdekaan Indonesia pada tanggal 17 Agustus 1945.

Bung Karno paling menonjol di antara para perintis kemerdekaan

Dipilihnya Bung Karno sebagai presiden pertama Republik Indonesia adalah suatu hal yang wajar, dan juga seharusnya ! Sebab dari perjuangannya sejak muda di tahun 20-an sampai ia diasingkan ke Flores dan Bengkulu ia sudah dikenal sebagai pemimpin yang paling terkemuka dalam gerakan untuk melawan penjajahan Belanda dan memperjuangkan kemerdekaan bangsa. Karena itu, tidak ada pilihan lain, Bung Karno –lah yang dipandang oleh banyak kalangan yang paling pantas dan paling berhak untuk mengemban tugas kepemimpinan bangsa.

Sejarah sudah membuktikan bahwa Bung Karno-lah tokoh yang paling besar kewibawaannya dan paling kuat pengaruhya di antara para perintis kemerdekaan di seluruh Indonesia, yang terdiri dari banyak macam aliran politik,  golongan, suku dan agama. Kewibawaan dan pengaruh yang besar ini adalah hasil dari perjuangan Bung Karno yang dengan gigih membanting tulang selama kurang lebih 25 tahun sebelum proklamasi 1945.

Dan setelah menjabat sebagai presiden sejak 1945 pun Bung Karno masih terus melanjutkan tekadnya untuk mengabdikan diri sepenuh jiwa raganya  kepada rakyat dalam situasi yang tidak mudah sebagai akibat dari perjuangan dalam revolusi untuk kemerdekaan dan perjuangan bangsa melawan nekolim (ingat, antara lain : DI-TII, « aksi polisionil Belanda » ke-I dan ke-II, RMS, garakan tiga Selatan, PRRI, Permesta).

 Bahkan, dalam situasi yang sulit ini Bung Karno berhasil dengan gemilang telah membawa negara dan bangsa Indonesia ke gelanggang internasional dengan nama harum dan kehormatan yang tinggi, dengan diselenggarakannya Konferensi Bandung tahun 1955, dan berbagai kegiatan untuk melawan imperialisme (terutama AS). Justru di bidang internasional inilah  sumbangan Bung Karno yang paling besar untuk mengangkat nama rakyat Indonesia tinggi-tinggi di mata dunia.

Bung Karno berjasa menjunjung tinggi nama Indonesia

Bung Karno dengan visinya yang tajam mengenai imperialisme dan kolonialisme telah menjadi tokoh penting , bahkan bintang utama, dalam gerakan non-blok (bersama Tito), dan menjadi sumber inspirasi perjuangan bagi rakyat-rakyat di Asia-Afrika. Nama Bung Karno dan Indonesia mempunyai kumandang besar dalam banyak pertemuan atau kegiatan yang diadakan oleh AAPSO (Asian African People’s Solidarity di Kairo), Asian African Jurists Conferenece di Conakry, Asian-African Writers Conference di Colombo, Afro-Asian Journalists Association atau PWAA di Jakarta.

Bung Karno juga terkenal dengan gagasannya untuk menyelenggarakan GANEFO (Games of the New Emerging Forces)  dan KIAPMA (Konferensi Internasional Anti Pangkalan Militer Asing) di Jakarta. Semua kegiatan yang dilakukan dengan, atau bersama-sama, atau lewat berbagai organisasi itu adalah pada pokoknya untuk memupuk solidaritas antara rakyat di berbagai benua dalam menentang imperialisme.

Itulah sebabnya nama Bung Karno pernah dalam jangka lama menjulang tinggi dalam pandangan banyak rakyat yang berjuang di bebagai negeri. Bung Karno menjadi tokoh yang dihormati dan dicintai oleh banyak kalangan progresif di dunia, sebagai pemimpin bangsa yang mengemban politik atau visi revolusioner. Nama Bung Karno disejajarkan dengan nama-nama besar berbagai tokoh dunia seperti Tito dari Yugoslavia, Nasser dari Mesir, Kwame Nkrumah dari Ghana, Nehru dari India, Ho Chi Minh dari Vietnan, Mao Tse Tung dan Chou En Lai dari Tiongkok. Tidak adalah pemimpin Indonesia lainnya yang berhak mempunyai kehormatan untuk disejajarkan dengan tokoh-tokoh besar dari berbagai negara itu.

Di skala nasionalpun , Bung Karno sebagai presiden RI meneruskan perjuangan revolusionernya yang sudah dilakukannya sejak muda. Sesudah memimpin perjuangan besar melawan pembrontakan PRR-Permesta yang didukung AS, ia mengadakan serentetan tindakan dan politik yang penting untuk mendorong revolusi bangsa lebih bergelora. Antara lain dikembalikannya UUD 45 dalam tahun 1959, dicetuskannya berbagai gagasan besar atau ajaran-ajaran revolusionernya, yaitu Manifesto Politik, Berdikari, Deklarasi Ekonomi (Dekon), NASAKOM, Trisakti, yang menjadi panutan banyak orang dalam melanjutkan revolusi di bawah pimpinan Bung Karno.

Kata revolusi sebanyak 203 kali dalam satu pidato !!!!!

Semua gagasan besar atau ajaran-ajaran revolusioner yang dicetuskan Bung Karno baik sebelum kemerdekaan Indonesia maupun sesudah menjadi presiden RI datang dari pemikirannya yang pada pokoknya adalah kiri atau revolusioner, yang sekaligus mengandung nasionalisme, Islamisme dan marxisme. Semua orang yang betul-betul mau berjuang demi membela kepentingan rakyat melawan segala macam penindasan dan ketidakadilan akan menemukan jiwa revolusioner atau semangat kiri yang dikandung dalam tulisan-tulisan dalam buku « Dibawah Bendera Revolusi » dan « Revolusi Belum Selesai ».

Kerevolusioneran pandangan Bung Karno mengenai berbagai masalah besar bangsa adalah satu hal yang tidak bisa diungguli oleh kebanyakan pemimpin atau tokoh Indonesia lainnya. Ia menggunakan setiap kesempatan bicara di depan umum untuk mengangkat tentang pentingnya bagi bangsa untuk meneruskan revolusi yang belum selesai. Boleh dikatakan bahwa Bung Karno adalah pemimpin rakyat yang betul-betul gandrung kepada revolusi, yang selalu menginginkan perubahan-perubahan besar demi kepentingan kesejahteraan bangsa dan negara.

Sebagai salah satu contoh betapa besar idaman Bung Karno tentang revolusi yang perlu dijalankan terus menerus  oleh bangsa Indonesia adalah pidatonya untuk merayakan 17 Agustus 1964, yaitu satu tahun sebelum terjadinya peristiwa G30S. Setiap orang yang berminat dapat membacanya dalam buku « Dibawah Bendera Revolusi » jilid II dari halaman 559 sampai 598.  Dalam hanya satu pidatonya ini saja Bung Karno menyebut kata-kata revolusi atau revolusioner sebanyak 203 kali !!!!! (tanda seru lima kali). Demikian itulah begitu besar kegandrungan Bung Karno kepada revolusi.  (Karena sangat menariknya betapa besar gagasan Bung Karno tentang revolusi bagi bangsa Indonesia, akan diusahakan adanya tulisan tersendiri sebagai bahan kajian bersama).

Dari itu semua nyatalah bahwa Bung Karno adalah pemimpin rakyat yang sejak muda sekali berhaluan kiri atau revolusioner. Itulah sebabnya maka gagasan-gagasannya atau  tulisan atau pidato-pidatonya  mengandung ciri-ciri kiri, progresif, atau revolusioner. Itulah juga sebabnya ia mengatakan bahwa Pancasila adalah kiri dan juga bahwa revolusi bangsa Indonesia adalah revolusi kiri.

Bung Karno dicoba dibunuh 7 kali

Karena itu Bung Karno berusaha dengan sekeras-kerasnya untuk menyatukan diri dengan perjuangan golongan kiri atau golongan revolusioner baik dalam skala nasional maupun skala internasional melawan segala kekuatan reaksioner di dalam negeri maupun imperialisme. Dan karena itu pulalah maka Bung Karno mendapat dukungan atau simpati dari semua golongan yang kiri, progresif, atau revolusioner, termasuk golongan PKI. Tidak ada satu pun pemimpin nasional lainnya yang mendapat dukungan atau simpati dari begitu banyak golongan progresif di Indonesia – maupun di luarnegeri  --  seperti Bung Karno, dalam melawan nekolim.

Karena sejak muda Bung Karno sudah mengambil posisi yang demikian jelas bertentangan dengan kepentingan golongan reaksioner di dalam negeri dan bermusuhan dengan imperialisme di luar negeri, maka ia menjadi musuh bebuyutan mereka. Kehadiran Bung Karno sebagai pemimpin bangsa Indonesia sama sekali tidak menguntungkan golongan reaksioner dalam negeri maupun luarnegeri.

Seperti yang bisa diamati oleh para peminat sejarah Indonesia, sejak proklamasi 17 Agustus 45 sampai digulingkannya Bung Karno oleh Suharto bersama jenderal-jenderal pendukungnya (yang mendapat dukungan imperialisme AS) sudah berkali-kali diusahakan hilangnya dari kepemimpinan bangsa dan negara Indonesia, dengan berbagai macam cara, tetapi selalu gagal. Bung Karno telah dicoba dibunuh 7 kali selama menjadi presiden. Ini menunjukkan betapa besar bahaya yang dilihat oleh kalangan reaksioner  -- baik yang ada di dalam negeri maupun yang di luar negeri – pada diri Bung Karno. Karena itu, tidak bisa lain, Bung Karno harus disingkirkan, bagaimana pun juga !

Bangsa kita patut bangga mempunyai Bung Karno

Dengan terjadinya peristiwa G30S maka dapatlah bagi kekuatan reaksioner di dalam negeri (dengan sebagian pimpinan Angkatan Darat sebagai tulang-punggungnya) dan di luar negeri untuk menjadikannya sebagai dalih untuk menggulingkan Bung Karno, dengan lebih dulu menghacurkan PKI secara besar-besaran dengan cara-cara yang luar biasa biadabnya.

Pengkhianatan pimpinan Angkatan Darat (yang dikepalai oleh jenderal Suharto) terhadap Bung Karno adalah bukan saja insubordinasi terhadap panglima tertinggi ABRI, melainkan juga pengkhianatan terhadap negara dan revolusi rakyat Indonesia. Penggulingan Bung Karno secara khianat oleh Suharto merupakan juga perusakan terhadap Pancasila, karena Pancasila adalah gagasan Bung Karno. Dan karena Pancasila adalah dasar-dasar negara Republik Indonesia, maka disingkirkannya Bung Karno oleh pimpinan Angkatan Darat (waktu itu) dari kepemimpinan bangsa dan negara pada hakekatnya merupakan perusakan Republik Indonesia.

Dan, kerusakan Republik Indonesia  yang melanda sangat parah di  berbagai bidang politik, ekonomi, sosial , kebudayaan dan moral, seperti yang kita saksikan dewasa ini adalah akibat dari dirusaknya -- secara besar-besaran --  segala usaha Bung Karno dalam mengajak bangsa Indonesia melanjutkan revolusi yang belum selesai untuk membangun bersama-sama masyarakat dan makmur.

Kiranya, bangsa Indonesia – beserta anak cucu di kemudian hari -- bisa merasa banggga pernah mempunyai putera yang begitu besar sumbangannya dalam perjuangan untuk kemerdekaan bangsa dan juga sebagai presiden, kepala negara dan pemimpin besar revolusi, yang bernama Sukarno. Karena nama Sukarno tidak bisa dipisahkan dari Pancasila, maka selama bangsa dan negara kita memakainya sebagai dasar-dasar negara, maka nama Sukarno akan tetap satu dan senyawa dengan Pancasila.

Dalam seribu tahun akan hanya ada seorang Soekarno

Kini, sesudah dibenam atau disekap selama hampir setengah abad oleh rejim militer Orde Baru, nama Bung Karno mulai sedikit-sedikit serta perlahan-lahan berkumandang kembali. Sebagian dari orang-orang atau kalangan yang pernah memusuhinya atau menghinanya sudah mulai berubah pandangan. Salah satu di antara banyak contohnya adalah yang ditulis oleh Subadio Sastrosatomo, tokoh penting atau   « dedengkot » PSI,  partai yang pernah beroposisi keras dan dalam jangka lama sekali terhadap Bung Karno

Ia telah menerbitkan satu brosur yang berjudul « Soekarno adalah Indonesia, Indonesia adalah Soekarno » yang isinya berisi pengakuan tentang besarnya arti perjuangan dan jasa-jasa Bung Karno bagi bangsa Indonesia  Isi brosur ini mempunyai arti yang dalam dan jauh. (Keterangan : brosur ini diterbitkan oleh Pusat Dokumentasi Politik « Guntur 49 » Jakarta)

Dalam rangka memperingati hari lahir Bung Karno 6 Juni 1901, maka terasa sebagai ungkapan yang kuat sekali  -- dan besar artinya  -- apa yang ditulis oleh Jusuf  Ronodipuro, mantan Dubes RI di Argentina dalam New York Times tanggal 4 Juni 2002, yang antara lain berbunyi : « In a thousand years, there will only be one Sukarno.”  -- Dalam seribu tahun  akan hanya ada seorang Soekarno (kutipan dari Eddie Suroyo Sastro, Washngton DC , 6 Juni 2004)

Mengingat itu semua, maka sudah selayaknyalah  bahwa untuk selanjutnya bangsa kita  -- terutama generasi mudanya -- mengingat dan mempelajari sejarah perjuangan serta berbagai ajaran-ajaran revolusioner Bung Karno sebanyak mungkin dan juga sedalam-dalamnya. Bung Karno adalah guru besar bangsa, baik di masa lalu, maupun untuk masa depan, untuk memperjuangkan dan membangun masyarakat adil dan makmur.
