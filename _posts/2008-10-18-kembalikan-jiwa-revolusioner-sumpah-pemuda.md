---
layout: post
title: Kembalikan jiwa revolusioner Sumpah Pemuda !
date: 2008-10-18
---

Tidak lama lagi kita akan memperingati Hari Sumpah Pemuda pada tanggal 28 Oktober. Mengingat sangat pentingnya Sumpah Pemuda ini bagi bangsa Indonesia, maka tulisan ini dimaksudkan sebagai ajakan kepada para pembaca untuk sama-sama menelaah - dari berbagai segi dan sudut pandang – hal-hal yang berkaitan dengan hari bersejarah ini. Sebab, Sumpah Pemuda yang menurut sejarahnya dicetuskan dalam suasana pergolakan perjuangan melawan kolonialisme/imperialisme Belanda, selama rejim militer Suharto 32 tahun telah kehilangan "api "revolusionernya, akibat politik Orde Baru. Walaupun Hari Sumpah Pemuda juga dirayakan oleh Orde Baru tiap tahun, namun terasa sekali bahwa peringatan semacam itu terpaksa dilakukan hanya sebagai ritual yang hambar dan kering (dan karenanya kadang-kadang terasa juga membosankan, bahkan, memuakkan !), atau sebagai upacara yang isinya hanya dangkal-dangkal saja, dan tidak mengandung pesan yang dalam bagi bangsa.


Karena itu, kita semua -- termasuk terutama sekali generasi muda bangsa kita – berkewajiban untuk mengangkat kembali Sumpah Pemuda dan mendudukkannya di tempat yang semestinya atau selayaknya, sesuai dengan kebesaran sejarahnya bagi bangsa Indonesia. Kita tidak bisa membiarkan Sumpah Pemuda terus-menerus dipisahkan dari ciri-cirinya yang revolusioner sebagai pemersatu perjuangan bangsa, seperti yang telah dilakukan oleh rejim militer Orde Baru beserta para pendukungnya selama puluhan tahun.


Sumpah Pemuda adalah cikal bakal proklamasi 45


Bagi siapa saja yang mau bersikap serius dan objektif akan bisa melihat bahwa Sumpah Pemuda merupakan peristiwa besar dan maha penting bagi bangsa kita dalam perjuangan melawan kolonialisme Belanda dan merebut kemerdekaan nasional. Begitu besarnya arti atau peran yang dikandungnya, sehingga boleh dikatakan bahwa kemerdekaan yang diproklamasikan dalam tahun 1945 tidak akan diperoleh oleh bangsa kita, seandainya tidak ada Sumpah Pemuda dalam tahun 1928. Sumpah Pemuda 1928 adalah cikal bakal proklamasi kemerdekaan 1945 yang melahirkan NKRI. Sumpah Pemuda adalah sumber konsep besar persatuan bangsa yang dikenal sebagai Bhinneka Tunggal Ika. Sumpah Pemuda adalah juga landasan inspirasi gagasan besar Bung Karno yang kemudian dirumuskan dalam Pancasila. Sumpah Pemuda 1928 adalah jiwa pemersatu bangsa yang terdiri dari berbagai suku, bahasa, agama, dan kedudukan sosial.


Karena itu, Sumpah Pemuda tahun 1928 tetap mempunyai arti penting bagi bangsa kita dewasa ini ketika sedang menghadapi berbagai masalah yang ditimbulkan oleh kesukuan, adat, agama atau politik (ingat, antara lain : masalah FPI, undang-undang porno, Ahmadiyah, pembangunan gereja, ekses otonomi daerah, perda mengenai syariah Islam dll dll). Mengingat itu semua, nyatalah bahwa kita harus berusaha bersama-sama mengkobarkan kembali jiwa asli Sumpah Pemuda tahun 1928 dan menggunakannya sebagai senjata rakyat dalam menghadapi berbagai persoalan besar dewasa ini.


Selain itu, kita semua harus meninggalkan atau membuang jauh-jauh kebiasaan buruk Orde Baru yang menjadikan Sumpah Pemuda hanya sebagai hiasan yang palsu saja, yang sebenar-benarnya hanyalah melecehkannya. Sebab, Sumpah Pemuda tahun 1928 adalah suatu langkah revolusioner dari kalangan angkatan muda bangsa dalam rangka nation-building dan character-building (menurut istilah yang sering sekali dipakai oleh Bung Karno dalam berbagai pidatonya) menuju persatuan bangsa sepanjang perjuangan melawan kolonialisme Belanda, yang menjadi bagian penting dari kekuatan imperialisme. Karenanya, dalam artian tertentu, Sumpah Pemuda adalah platform pluralis yang mengandung ciri-ciri revolusioner.


Sumpah Pemuda di masa Orde Baru


Para pembaca menyaksikan sendiri bahwa justru arti atau isi atau jiwa Sumpah Pemuda yang seperti tersebut di atas itu semualah yang telah pudar (atau mandul, atau loyo) sejak digulingkannya presiden Sukarno oleh segolongan dari Angkatan Darat di bawah pimpinan Suharto dengan dukungan kekuatan imperialis (terutama AS). Kita semua masih ingat bahwa selama Orde Baru peringatan Hari Sumpah Pemuda diselenggarakan di bawah pengawasan, dan pengarahan atau bimbingan oleh pimpinan militer. Dan oleh karena pimpinan militer (terutama Angkatan Darat di bawah Suharto) mempunyai sikap anti-Bung Karno dan anti-kiri pada umumnya, maka peringatan Hari Sumpah Pemuda selama 32 tahun Orde Baru (dan juga sesudahnya) dijauhkan atau dipisahkan dari sejarah revolusioner bangsa yang melahirkan ikrar angkatan muda Indonesia dalam tahun 1928 itu.


Bagi kita semua (dan terutama bagi para tokoh-tokoh masyarakat dan para sejarawan Indonesia) adalah menarik untuk merenungkan bahwa selama masa Orde Baru peringatan Hari Sumpah Pemuda diselenggarakan tanpa banyak mengangkat kembali berbagai faktor yang melahirkan Sumpah Pemuda. Bahwa rejim militer Orde Baru berusaha « menghilangkan » peran Bung Karno dari sejarah revolusioner bangsa Indonesia melawan kolonialisme dan imperialisme adalah wajar dan bisa dimengerti, sebab Orde Baru memang merupakan musuh revolusi rakyat Indonesia.


Dalam kaitan ini, kiranya kita mengerti bahwa Suharto tidak mungkin bisa betul-betul menghayati Hari Sumpah Pemuda, karena Hari Sumpah Pemuda dilahirkan dengan jiwa atau semangat anti-kolonialisme Belanda, sedangkan Suharto adalah dulunya seorang yang telah mengabdi kepada penjajajah Belanda dengan menjadi serdadunya, yaitu KNIL (Koninklijke Nederlands Indische Leger). Karena itu, berlainan sama sekali dengan Bung Karno, bagi Suharto sulit sekali untuk berkoar-koar sebagai tokoh bangsa yang melawan kolonialisme Belanda (catatan : cerita tentang peran Suharto ketika « penyerbuan » di Jogja pun ternyata adalah cerita yang mengandung kebohongan sejarah).


Pembrontakan PKI tahun 1926 melawan Belanda


Bukan itu saja !!! Suharto dengan Orde Barunya (beserta para pendukungnya yang sampai sekarang) juga berusaha untuk memisahkan Sumpah Pemuda dari peran penting berbagai gerakan rakyat yang waktu itu berjuang melawan kolonialisme Belanda untuk merebut kemerdekaan, termasuk menghilangkan arti penting dan bersejarah pembrontakan PKI melawan kolonialisme Belanda dalam tahun 1926.


Padahal, Sumpah Pemuda dilangsungkan dalam tahun 1928 ketika suasana di kalangan berbagai gerakan anti penjajahan Belanda, terutama di kalangan angkatan mudanya, terpengaruh oleh akibat atau dampak pembrontakan PKI tahun 1926 melawan kolonialisme Belanda ketika ribuan orang komunis atau simpatisan-simpatisannya dibuang ke Digul atau tempat-tempat tahanan lainnya.


Di antara gerakan angkatan muda bangsa yang waktu itu menunjukkan kemauan untuk bersatu dalam perjuangan melawan kolonialisme Belanda – dengan cara atau bentuk dan kadar berbeda-beda – terdapat organisasi-organisasi pemuda seperti Jong Java, Jong Islamieten Bond, Jong Sumatranen Bond, Jong Batak, Jong Celebes, Jong Ambon, Minahasa Bond, Madura Bond, Pemuda Betawi dan lain-lain.


Kalau ditilik secara dalam-dalam dan jauh, maka nyatalah bahwa Sumpah Pemuda tahun 1928 adalah sumber atau cikal-bakal lahirnya pedoman-pedoman besar bangsa Indonesia, yaitu Pancasila dan Bhinneka Tunggal Ika. Dan karenanya bisa juga dikatakan bahwa Sumpah Pemuda berorientasi kiri dan revolusioner. Sumpah Pemuda tahun 1928 melanjutkan perjuangan PKI melawan penjajahan Belanda dengan pembrontakannya tahun 1926 di Jawa dan Sumatera dan tempat-tempat lainnya. Sumpah Pemuda juga mendapat dorongan dengan didirikannya Partai Nasional Indonesia oleh Bung Karno dalam tahun 1927.


Jadi, dapatlah dimengerti bahwa dampak besar di berbagai kalangan bangsa yang ditimbulkan oleh pemberontakan PKI tahun 1926 merupakan dorongan yang tidak kecil bagi terselenggaranya Kongres Pemuda Kedua dalam tahun 1928 yang melahirkan Sumpah Pemuda. Justru aspek inilah yang telah diusahakan ditutup-tutupi atau dihilangkan dari sejarah oleh Orde Baru (beserta para pendukungnya sampai sekarang !). Rejim militer Suharto dkk berusaha dengan berbagai cara dan jalan untuk memisahkan Sumpah Pemuda dari gerakan revolusioner, dari Bung Karno, dan dari golongan kiri umumnya, terutama PKI. Padahal, Sumpah Pemuda adalah bagian dari gerakan revolusioner melawan penjajahan Belanda waktu itu.


.Sumpah Pemuda bisa digolongkan sebagai peristiwa yang mengandung ciri-ciri revolusioner, karena anti penjajahan Belanda. Dalam sejarah banyak bangsa di dunia, kebanyakan perjuangan menentang penjajahan (kolonialisme dan imperialisme) mengandung ciri-ciri atau unsur-unsur revolusioner, kiri, nasionalis dan patriotik. Dalam kaitan inilah kita bisa memandang didirikannya Partai Nasional Indonesia oleh Bung Karno dalam tahun 1927 (satu tahun sesudah pembrontakan PKI melawan Belanda dan satu tahun sebelum Sumpah Pemuda) sebagai gerakan revolusioner, yang menyebabkan dipenjarakannya Bung Karno oleh pemerintah kolonial Belanda dalam tahun 1929 (ingat pleidooinya di depan pengadilan kolonial yang berjudul Indonesia Menggugat).


Bagi siapa saja yang mau bersikap serius dan objektif atau jujur, akan bisa melihat bahwa pembrontakan PKI tahun 1926 melawan penjajahan Belanda memainkan -secara langsung atau tidak langsung --peran yang penting dan tidak kecil untuk didirikannya PNI (Partai Nasional Indonesia) oleh Bung Karno dan teman-teman terdekatnya dalam tahun 1927, dan juga untuk terselenggaranya Kongres Pemuda Kedua (yang melahirkan Sumpah Pemuda dalam tahun 1928).


Sebagaimana yang dapat kita ketahui dari sejarah, Kongres Pemuda yang bersifat lintas-agama, lintas-suku, lintas-aliran poltik itu mencetuskan ikrar bersama yang amat besar artinya bagi perjuangan rakyat Indonesia kemudian, yaitu Sumpah Pemuda. Ikrar bersama yang bersejarah ini dikumandangkan tanggal 28 Oktober 1928. Sumpah Pemuda itu berbunyi :

1. Kami putra dan putri Indonesia, mengaku berbangsa yang satu, bangsa Indonesia
2. Kami putra dan putri Indonesia, mengaku bertanah-air yang satu, tanah-air Indonesia
3. Kami putra dan putri Indonesia, menjunjung bahasa persatuan, bahasa Indonesia.
Selain itu Kongres Pemuda itu juga telah mengambil keputusan penting lainnya, yaitu : menjadikan lagu Indonesia Raya (diciptakan oleh Rudolf Wage Supratman) sebagai lagu kebangsaan bagi seluruh rakyat Indonesia, dan juga menjadikan Merah Putih sebagai bendera kebangsaan.


Intisari Sumpah Pemuda adalah Bhinneka Tunggal Ika


Seperti pernah ditulis sebelumnya, kalau kita merenungkan itu semua, maka akan jelaslah kiranya bagi kita semua, bahwa kemerdekaan yang diproklamasikan oleh Bung Karno dan Bung Hatta pada tanggal 17 Agustus 1945, dan perjuangan besar rakyat selama revolusi melawan kolonialisme Belanda, adalah - secara langsung atau tidak langsung - produk atau kelanjutan perjuangan para pejuang sebelumnya, yang banyak meringkuk di penjara-penjara dan juga di tanah pengasingan Digul.


(Patut kita ingat kembali bahwa karena pembrontakan PKI melawan penjajahan Belanda itu ribuan orang dibunuh secara brutal dan sekitar 13 000 orang ditahan oleh pemerintahan kolonial. Tidak kurang dari 1 300 orang yang terdiri dari kader-kader PKI dibuang dalam kamp pengasingan di Boven Digul. Jumlah orang-orang yang dibunuh, dipenjarakan dan dibuang itu besar sekali untuk masa itu, dan merupakan goncangan yang besar sekali di seluruh Indonesia. Banyak orang-orang yang non-komunis juga ditangkapi Belanda dengan alasan untuk menindas pembrontakan PKI. Sejak 1927 PKI dinyatakan terlarang oleh Belanda, dan karenanya terpaksa bergerak di bawah tanah untuk selanjutnya, sampai proklamasi kemerdekaan tahun 1945).


Intisari Sumpah Pemuda adalah Bhinneka Tunggal Ika, yang mengandung pengertian bahwa walaupun bangsa Indonesia terdiri dari berbagai suku, agama, ras dan aliran politik, tetapi tetap merupakan kesatuan bangsa, atau bangsa yang satu. Dengan kalimat lain, tercermin di situ satu konsep besar yang indah : kesatuan dalam perbedaan, atau berbeda-beda tetapi satu, atau persatuan dalam keragaman. Alangkah sejuknya isi dan alangkah besarnya arti yang tersirat di dalamnya.

Mengembalikan Sumpah Pemuda ke sisi revolusioner


Kita semua sudah menyaksikan, dan dalam rentang-waktu yang cukup panjang, bahwa sistem politik Orde Baru telah menimbulkan perpecahan bangsa, mengompori permusuhan, mengucilkan berbagai komponen bangsa, mengkipas-kipasi permusuhan ras (ingat, umpamanya : politik terhadap minoritas keturunan Tionghoa, perlakuan terhadap kaum komunis atau eks-tapol beserta keluarga mereka, menghasut sebagian golongan Islam untuk memusuhi golongan lain dll). Ini semua adalah bertentangan dengan Sumpah Pemuda yang menegaskan bahwa kita semua adalah satu bangsa dan satu tanah air.


Mengingat itu semuanya,adalah sudah waktunya sekarang ini bagi kita semua (dan terutama sekali bagi generasi muda bangsa) untuk bersama-sama membebaskan Sumpah Pemuda dari kungkungan yang dipenuhi oleh kepalsuan dan kemunafikan oleh orang-orang simpatisan Suharto beserta Orde Barunya, dan mengangkatnya kembali ketingkat luhur dan mulia, sebagai pemersatu bangsa dan negara yang mengandung ciri-ciri revolusioner.


Generasi muda kita juga wajib ikut menjaga jangan sampai Sumpah Pemuda bisa dipisahkan dari sejarah revolusioner berbagai gerakan rakyat Indonesia. Artinya, Sumpah Pemuda juga tidak boleh dipisahkan dari sejarah perjuangan revolusioner Bung Karno. Sebab, sekarang makin terbukti dengan jelas bahwa sosok Bung Karno adalah pengejawantahan (manifestasi atau penampîlan) sekaligus Sumpah Pemuda, Pancasila, Bhinneka Tunggal Ika, dan Nasakom
