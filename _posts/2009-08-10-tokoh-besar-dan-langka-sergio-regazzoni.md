---
layout: post
title: 'Tokoh besar dan langka : Sergio Regazzoni'
date: 2009-08-10
---

Dalam kehidupan kita sehari-hari, yang dibanjiri oleh segala macam berita di media massa atau televisi tentang berbagai soal yang dihadapi oleh masyarakat jarang sekali kita membaca atau mendengar tentang seorang yang benar-benar berjiwa besar dan juga langka ditemukan bandingannya, sehingga patut diketahui oleh banyak orang dan menjadi contoh. Untuk itulah tulisan kali ini menyajikan sebagian kecil dari  kehidupan seorang teman lama dan kawan seperjuangan, Sergio Regazzoni, yang meninggal dunia di rumah sakit di Evry (pinggiran kota Paris) pada hari Kamis tanggal 30 Juli 2009, karena sakit kangker di hati dan ginjal selama beberapa bulan.

Terasa bagi saya, bahwa menulis tentang kehidupan Sergio Regazzoni, walaupun secara singkat saja, sama sekali tidaklah mudah. Sebab, kebesaran jiwa atau keluhuran perjuangan selama sejak muda sampai wafatnya, kiranya tidak bisa dituangkan dalam beberapa halaman saja, atau juga tidak cukup hanya dalam bebarapa ratus halaman. Bahkan, mungkin baru beberapa buku bisa memberikan gambaran yang layak tentang sebagian dari arti tindakan-tindakannya, sebagian dari fikiran dan pandangannya tentang berbagai soal di dunia atau tujuan dan cita-cita atau harapannya.

Tulisan ini dimaksudkan untuk diketahui oleh para pembaca di Indonesia, bahwa dalam situasi di dunia (termasuk di negara kita) yang penuh dengan berbagai konflik atau pertentangan yang berdasarkan agama, ras atau kebangsaan dan kesukuan, atau kepentingan ekonomi, dan ketika sebagian besar ummat manusia (juga di Indonesia)  sudah meninggalkan cita-cita luhur yang mengandung kemanusiaan, kasih, solidaritas, masih ada juga manusia-manusia yang seperti Sergio. Manusia-manusia yang besar jiwanya dan luhur budinya, yang memberikan indikasi bahwa dunia belum rusak semuanya.

Perkenalan pertama dengan Sergio

Saya mengenal Sergio Regazzoni, ketika ia sudah bertugas sejak 1984 sebagai penanggung-jawab urusan-urusan Asia-Pasifik dari LSM yang terbesar di Prancis, CCFD (Comité Catholique pour Développement et contre la Faim – Komite Katolik untuk Pembangunan dan melawan Kelaparan). CCFD merupakan LSM  yang sejak didirikan tahun 1961 telah bergerak dalam bidang pembangunan masyarakat di berbagai negeri di dunia (lebih dari 70 negeri) dalam usaha bersama menciptakan dunia yang lebih adil dan solider, dan membantu kaum miskin supaya menjadi aktor perubahan sosial.

Sesudah bertugas di CCFD selama lebih dari 10 tahun (1984-1994) ia menjadi Direktur Centre Lebret-IRFED International , suatu badan pusat refleksi dan tukar pengalaman bagi para aktor yang bekerja untuk kemajuan kemanusiaan, sampai tahun-tahun terakhir sebelum wafatnya.

Selama menjadi  tokoh di CCFD selama 10 tahun dan di Centre Lebret-IRFED selama sekitar 15 tahun itulah Sergio telah menunjukkan kegigihannya dalam pengabdiannya untuk melakukan hal-hal yang sesuai dengan orientasi besar organisasi dimana ia memikul tanggungjawab, dan sesuai pula dengan pandangan atau cita-citanya secara pribadi. Ia telah membikin CCFD (dan kemudian juga Centre Lebret) makin dikenal di berbagai kalangan di banyak negeri di Asia dan Pasifik, terutama di kalangan LSM  (atau badan-badan resmi berbagai pemerintahan) yang bergerak di bidang sosial untuk pembangunan dan pemberdayaan masyarakat, terutama yang miskin atau tersingkir.

Dalam kegiatannya selama 25 tahun di CCFD dan Centre Lebret , sebagai kelanjutan dari kegiatannya ketika ia masih muda sekali di JOC International (Jeunesse Ouvrière Chrétienne – Angkatan Muda Pekerja Kristen) itulah ia telah mengunjungi Korea Utara, Korea Selatan, Jepang, Tiongkok, Vietnam Utara dan Selatan, Kamboja, Laos, Thailand, Filipina, Malaysia, India, Srilanka, Indonesia, Timor Timur, dan juga banyak negeri-negeri   Afrika, Timur Tengah dan Amerika Latin.

Sahabat karib Gus Dur dan Romo Mangun

Dengan gaya kerjanya yang lincah, cara bergaul yang menyenangkan dengan siapa saja (termasuk orang-orang yang sulit), dan sikap yang rendah hati dan bersahabat, ia telah menggalang persahabatan dan kerjasama dengan banyak kalangan dari macam-macam agama dan aliran politik.

Ia tidak saja dikenal oleh banyak uskup atau pastur di banyak  negeri (antara lain dari Korea Selatan, Jepang, Tiongkok, Hongkong, Filipina, Indonesia, Timor Timur dll dll) tetapi juga menjadi sahabat karib dan mitra kerjasama Gus Dur, Romo Mangun serta tokoh-tokoh LSM Indonesia lainnya. Ia mempunyai hubungan yang erat dengan tokoh-tokoh Budha di Thailand, Kamboja dan Laos. Sergio merupakan tokoh pendukung gerakan demokratis di Birma yang menjadi oposisi sejak lama dari junta militer Birma

Dari apa yang telah dilakukan Sergio mengenai persoalan-persoalan Indonesia maka dapatlah kiranya diukur betapa besar ketulusan dan kegigihannya dalam menjalankan tugasnya, terutama ketika rejim militer Suharto masih sangat berkuasa. Ia dengan berani (tetapi cukup hati-hati) membantu dengan berbagai cara dan jalan banyak LSM yang berorientasi kiri, termasuk organisasi-organisasi yang ada di bawah pangayoman gereja Katolik maupun Protestan. Sejumlah besar beasiswa telah diberikan dan projek-projek ekonomi dan sosial juga telah dibantu. Banyak aktivis-aktivis LSM (antara lain yang tergabung dalam INFID) telah dibiayai untuk seminar atau konferensi (atau belajar) di berbagai negeri.

Dalam rangka ini pulalah Sergio pernah membantu untuk membiayai pengiriman delegasi Indonesia yang dipimpin Romo Mangun (terdiri dari 6 orang) untuk berkunjung ke Tiongkok, dalam usaha untuk menggalang hubungan bersahabat dan saling mengenal, ketika hubungan diplomatik antara Indonesia-Tiongkok masih terputus (sampai Agustus tahun 1990). Ini adalah hasil dari pemikiran jangka jauh dari Sergio, yang termasuk berani waktu itu.

Fikiran-fikirannya yang menjangkau jauh

Fikiran atau pandangan Sergio yang menjangkau jauh juga terlihat dari tindakannya untuk menggalang kerjasama dengan Korea Utara (bagian dari UNESCO Korea Utara dan Universitas Pertanian di Wonsan). CCFD juga memberikan beasiswa untuk sejumlah kecil mahasiswa Korea Utara di Prancis (dua di antaranya pernah bekerja di restoran INDONESIA di Paris sambil belajar). Bahwa Sergio telah secara berani, dan gigih, berusaha menggalang persahabatan dan kerjasama dengan berbagai kalangan di Korea Utara, demi kepentingan manusia adalah sesuatu yang patut dianggap luar biasa.

Demikian juga halnya dengan Tiongkok. Sergio sudah lama ingin bahwa CCFD mempunyai hubungan kerjasama dan bersahabat dengan kalangan-kalangan di Tiongkok, dalam rangka merealisasikan tujuan besar untuk mengabdi kepada kemanusiaan di mana pun, dan dalam situasi yang betapa pun sulitnya. Hubungan dengan Tiongkok dianggap oleh Sergio sebagai suatu hal yang perlu sekali mengingat bahwa penduduk Tiongkok berjumlah  sekitar seperlima ummat manusia di dunia.  Untuk melaksanakan tujuan inilah Sergio mengajak saya dalam tahun 1986 menjalankan missi CCFD ke Korea Utara terlebih dulu sebelum berkunjung ke Tiongkok.

Karena Sergio mengetahui sejak lama bahwa sebelum minta suaka di Prancis saya pernah bekerja di Beijing sebagai anggota sekretariat Persatuan Wartawan Asia-Afrika dan hidup di Tiongkok selama 7 tahun, dan karenanya mempunyai akses untuk hubungan-hubungan dengan berbagai fihak maka ia minta bantuan saya untuk membidani hubungan CCFD dengan Tiongkok. Sebagai langkah permulaan hubungan CCFD adalah dengan CAFIU (Chinese Association for International Understanding),  suatu badan central yang cukup penting di Tiongkok.

Sejak itu maka CCFD selama lebih dari 25 tahun sudah mengadakan kerjasama dengan berbagai kalangan di Tiongkok, dan membantu projek-projek dalam bidang pendidikan (dengan  UNESCO Tiongkok), mendirikan jembatan, membangun sekolah, klinik dan apotik, dan usaha-usaha produktif lainnya. Sergio telah berhasil memperkenalkan sebagian dari tujuan-tujuan besar CCFD kepada banyak kalangan di Tiongkok, dan membuka hubungan persahabatan yang tadinya tidak ada. Sergio telah membawa CCFD untuk menyelenggarakan seminar-seminar bersama berbagai badan atau organisasi Tiongkok, dan memberikan beasiswa untuk belajar ke Prancis.

Hubungan dengan kaum agama, marxis atau komunis

Hasil kegiatan Sergio selama 10 tahun di CCFD dan 15 tahun di Centre Lebret mengenai Tiongkok adalah termasuk trobosan yang penting, di samping pekerjaan-pekerjaan yang banyak sekali dilakukan di Vietnam, Kamboja, Thailand, Birma, Filipina, dan juga yang mengenai Indonesia dan Timor Timur.

Bukan hanya kalangan gereja Katolik di berbagai negeri itu yang mempunyai hubungan erat dengan CCFD berkat usaha Sergio melainkan juga kalangan-kalangan agama lainnya, termasuk kalangan non-agama atau marxis atau komunis. Dalam hal ini hubungan yang erat antara Sergio degan Ramos Horta dan Xanana Gusmao, dan banyaknya bantuan CCFD selama puluhan untuk berbagai kegiatan komité-komité Timor di berbagai negeri menunjukkan dengan jelas besarnya perasaan solidaritas Sergio terhadap rakyat Timor Timur dalam menentang agresi militer rejim Suharto.

Sergio menunjukkan sikap positif dan antusias terhadap berbagai macam kegiatan, dari mana pun atau oleh siapa pun, yang  menentang ke-tidak-adilan, penindasan, dan kesengsaraan, dan yang berjuang untuk kemanusiaan. Untuk itu, ia duduk bersama saya sebagai salah satu pengurus dalam Komite Timor Prancis, yang didirikan sejak 1976 di Paris (salah satu di antara Komite Timor yang tertua di dunia).

Solidaritas dengan mereka yang ditindas Suharto

Hubungan Sergio dengan kalangan yang dalam puluhan tahun menentang rejim militer Suharto, baik yang di Indonesia maupun yang di luar negeri (termasuk yang di Prancis) adalah sesuatu yang menjadi sebagian dari kebesaran Sergio, yang perlu diketahui oleh banyak orang Indonesia. Dengan berbagai cara, dan bentuk, dan dengan macam-macam jalan, Sergio telah menunjukkan persahabatan atau solidaritas dengan orang-orang Indonesia yang tidak bisa pulang. Sergio memberikan beasiswa CCFD kepada sejumlah di antara mereka atau memberikan berbagai fasilitas untuk mengembangkan diri dan berkarya.

Solidaritas dari CCFD kepada usaha kolektif yang berbentuk restoran koperasi INDONESIA di Paris adalah suatu hal penting untuk dicatat sebagai salah satu di antara berbagai contoh bagi banyak orang dan organisasi di Indonesia atau negeri-negeri lain. Sebab, ketika dalam tahun 1982 sejumlah orang-orang Indonesia yang minta suaka di Paris dalam kesulitan mencari pekerjaan untuk bisa hidup normal, dan karenanya berusaha menciptakan kerja sendiri dengan membuka restoran, maka CCFD memberikan bantuan 100.000 franc (jumlah yang cukup besar waktu itu) untuk memulai usaha kolektif ini.

Sejak mulai berdirinya (sudah 27 tahun sampai sekarang) restoran koperasi ini berjalan lancar dan baik, dan makin terkenal, sehingga menjadi kebanggaan bagi banyak kalangan, terrmasuk dari kalangan CCFD. Selama ini Sergio menaruh hubungan yang erat dengan para anggota koperasi restoran, dan juga  menjadikan restoran kita tempat pertemuannya dengan teman-temannya yang terdekat.

Restoran Indonesia banyak dikenal di kalangan Katolik dan Protestan, dan  kalangan progresif  di Prancis umumnya, juga berkat jasa Sergio yang sering memperkenalkan pengalaman restoran koperasi ini sebagai suatu contoh yang ideal dalam usaha kolektif yang berorientasi kepada ekonomi solider atau ekonomi alternatif

Suatu kejadian yang menunjukkan betapa besarnya solidaritas dan betapa eratnya hubungan batin Sergio dengan orang-orang yang tidak bisa pulang ke Indonesia adalah sikapnya terhadap Budiman Sudarsono, yang juga salah satu pendiri restoran koperasi INDONESIA. Ketika Budiman meninggal dunia karena kecelakaan kereta-api Sergio ikut menghadiri upacara pemakamannya bersama banjak orang lainnya, yang berdatangan dari berbagai negeri di Eropa. Tetapi yang termasuk luar biasa adalah ketika pada suatu hari secara diam-diam dan sendirian saja ia memerlukan ziarah ke makam Budiman. Ia lakukan itu semua walaupun ia  tahu bahwa Budiman sebelum 1965 adalah salah satu pimpinan Pemuda Rakyat (Pusat) dan juga pernah menjadi Ketua Pengurus Besar IPPI, dua organisasi pemuda yang ditindas habis-habisan oleh rejim Suharto.

Itu semua hanyalah sebagian kecil sekali dari potret kehidupan Sergio, yang menunjukkan siapa dirinya dan bagaimana atau apa saja tindakannya dan apa tujuannya. Terlalu banyak hal yang tidak bisa diungkap, mengingat banyaknya dan juga rumitnya persoalan-persoalan yang pernah dihadapinya dalam masa baktinya yang berpuluh-puluh tahun untuk banyak kalangan di berbagai negeri.

Sergio adalah jembatan bagi kalangan yang berseberangan, dan juga perantara bagi banyak fihak yang membutuhkan hubungan dan bantuan, dan juga kunci untuk membuka pintu hati banyak orang yang memerlukan perkenalan dan persahabatan. Sergio adalah arsitek agung yang ulet, teguh, dan gigih untuk membangun bersama-sama dunia yang yang lebih baik, lebih adil, dan lebih sejahtera bagi semua.

Sergio adalah pejuang kemanusiaan yang suka kehidupan « low profile », dan karenanya tidak dikenal secara menyolok sebagai selebriti dalam pers di berbagai negeri. Ia sendiri tidak banyak menulis atau memberikan interview tentang dirinya atau tindakan-tindakannya. Keunggulannya adalah bekerja keras tanpa gembar-gembor, berbicara atau berunding dengan ketulusan dan kerendahan hati yang menyenangkan, sehingga ia banyak mempunyai sahabat karib dari berbagai kalangan di banyak negeri di Asia, Afrika, Timur Tengah, dan benua-benua lainnya. Karena itu pulalah seorang pendeta Vietnam (romo Minh) terbang khusus dari Saigon ke Paris untuk menghadiri upacara pemakaman Sergio. Begitu juga seorang sahabatnya yang lama dari Thailand, Jaran Ditatichai, yang juga terbang khusus dari Bangkok untuk memberikan penghormatan kepada Sergio.

Contoh bagi banyak orang, termasuk yang di Indonesia

Saya merasa bangga menjadi salah satu di antara banyak sahabat karibnya,  yang terdiri dari banyak kalangan dari berbagai negeri itu. Dalam persahabatan sebagai kawan seperjuangan selama puluhan tahun ini terjalin saling kepercayaan yang besar dan kuat sekali, sehingga bisa menangani soal-soal ,yang rumit atau mengandung risiko, yang kadang-kadang tidak perlu atau tidak baik diketahui oleh orang banyak.

Meskipun usianya lebih muda dari pada saya (ia lahir 4 November 1943 di Swiss) , saya merasa patut belajar banyak hal dari Sergio ; yang dalam hidupnya telah berusaha berbuat sebanyak mungkin dan sebaik mungkin, bagi manusia, dimana pun mereka berada.

Hal-hal yang begitu indah dari sekelumit kehidupan  Sergio inilah yang patut diketahui oleh banyak orang, termasuk di Indonesia, yang sekarang ini sebagiannya masih menghadapi fikiran-fikiran kolot, picik, sempit, fanatik tidak karuan , dan juga sesat !!!  Sergio menunjukkan dengan jelas bahwa ia sebagai orang Italia-Swiss, dan beragama Katolik, ia sudah membuktikan diri bisa berbuat banyak dan baik dan berguna pula,  bagi banyak kalangan di berbagai negeri di dunia Betul-betul hebat dan luhur.

Sergio telah tiada. Tetapi jiwa besarnya dan keluhuran budinya tetap menyala dalam jangka lama di hati dan fikiran banyak orang yang menghargai apa yang telah dilakukannya selama puluhan tahun dalam mengabdi kepada manusia di berbagai negeri di dunia.

Selamat jalan Sergio, sahabat karib dan kawan seperjuangan.
