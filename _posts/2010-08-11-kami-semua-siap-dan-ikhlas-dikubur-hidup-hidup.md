---
layout: post
title: Kami semua siap dan ikhlas dikubur hidup-hidup
date: 2010-08-11
---

Jeritan yang mengharukan dari mereka yang dikucilkan di Lombok





Memasuki bulan puasa dan menjelang peringatan Hari Kemerdekaan 17 Agustus kami sajikan sebuah tulisan pak Djohan Effendi, mantan Sekretaris Negara di bawah Presiden Abdurrahman Wahid dan temannya yang dekat sekali, yang bisa menyentuh lubuk hati banyak orang serta menggugah fikiran banyak kalangan bangsa kita tentang masalah-masalah persatuan bangsa dan kerukunan agama, yang sekarang ini kelihatan sedang tercabik-cabik parah sekali.



Tulisan ini mengutip sebuah surat dari komunitas Ahmadiah di Mataram, Lombok, kepada Walikota Mataram,  yang merupakan curahan hati yang luar biasa kuatnya dan kerasnya, sehingga bisa mengggoncang hati dan fikiran banyak orang yang berfikiran sehat, bernalar waras, mempunyai hati nurani, dan beriman yang saleh



Surat itu antara lain berbunyi sebagai berikut : « “Berilah kami tempat, Bapak Wali Kota, di mana saja di wilayah kota Mataram ini,di pinggiran yang dianggap angker banyak setannya sekalipun, atau di pekuburan-pekuburan, yang penting kami dapat keluar dari penampungan, hidup normal, menghirup udara kebebasan dan kemerdekaan.

Atau, jika telah dianggap menodai agama, telah melanggar UU No.1 PNPS/1/1965,

sebagaimana selama ini diancamkan, jebloskanlah kami, Bapak Wali Kota, ke dalam penjara. Kami seluruh warga Ahmadi, pengungsi laki-laki, perempuan, tua, muda maupun anak-anak, lahir batin, ikhlas dipenjara, tanpa proses hukum sekalipun.

Atau jika sama sekali tidak ada tempat bagi kami, di ruang penjara tidak ada

tempat bagi kami, di pekuburan-pekuburan juga tidak ada tempat bagi kami, maka

galikanlah bagi kami, Bapak Wali Kota, kuburan. Kami seluruh warga Ahmadi

pengungsi, laki-laki, perempuan, tua, muda maupun  anak-anak, siap dan ikhlas

dikubur hidup-hidup. …”



Sungguh, luar biasa mengharukan! Dan luar biasa kuatnya juga jeritan mereka ! Dan amat luar biasa besarnya pula isi pesan surat itu untuk kita semua, untuk kita semua, sekali lagi untuk kita semua! Dan, ketika sudah membaca kutipan surat berisi jeritan yang bisa membangunkan kemarahan hati kita semua itu, maka banyak pertanyaan (dan renungan ) bisa muncul di kepala kita masing-masing.



Umpamanya, antara lain adalah yang sebagai berikut :



Apakah mereka harus mengungsi ke negara lain?



Apakah  komunitas Ahmadiah  (harap catat baik-baik :  laki-laki, perempuan, tua, muda maupun  anak-anak ) yang sudah bertahun-tahun  “ditampung” secara paksa dalam pemukiman khusus ini sudah tidak dianggap lagi sebagai warganegara Republik Indonesia ?  Atau, bukan manusia seperti orang lainnya, yang berhak hidup di bumi kita bersama yang bernama  Indonesia ini?



Apakah perlakuan terhadap mereka itu bukan seperti memperlakukan sesama wargangara sebagai pengungsi di negeri sendiri, dan dalam keadaan serba dipaksa pula ? Mereka itu sudah dianggap sebagai “golongan yang terbuang” oleh bangsanya sendiri ! Mereka adalah golongan bangsa yang dirampas hak-hak fundamental mereka seperti yang dijamin UUD 45 kita.



 Berbagai hal bisa sama-sama kita renungkan  -- secara dalam-dalam  -- mengenai kasus komunitas Ahmadiah di Lombok ini, sebagai salah satu bagian dari banyaknya masalah-masalah sejenis atau senafas yang terjadi di tanahair kita akhir-akhir ini. Kalau pertentangan soal pemahaman tentang agama itu tidak bisa diselesaikan juga, apa orang-orang semacam yang di Lombok itu harus minta suaka ke negeri lain saja (umpamanya Australia, kalau bisa !) atau mendirikan negara sendiri, yang  terpisah dari Republik Indonesia ?



Atau,  kalau tidak bisa dipecahkan dengan itu semuanya, apakah satu-satunya jalan yang harus ditempuh adalah digalikannya liang kubur untuk mengubur mereka hidup-hidup, seperti yang mereka inginkan sendiri? Sungguh, kalau sampai ada perkembangan semacam itu, maka ma’af saja, saudara-saudara pembaca, sebaiknya Republik Indonesia ini dibubarkan saja !!!!! (tanda seru lima kali).



Pelanggaran Pancasila dan kejahatan terhadap Bhinneka Tunggal Ika



Surat komunitas Ahmadiah di Mataram ini menunjukkan bahwa negara dan bangsa kita dewasa ini sedang menghadapi berbagai  krisis besar dan parah sekali. Tindakan Bupati di Lombok terhadap komunitas Ahmadiah yang sudah mengusir mereka dari rumah mereka masing-masing dan menjebloskannya dalam tempat penampungan secara paksa, dan dengan alasan agama, adalah pelanggaran berat terhadap Pancasila, dan kejahatan besar sekali terhadap pedoman agung kita Bhinneka Tunggal Ika.



Tulisan ini mengajak para pembaca sudilah kiranya memperhatikan isi tulisan pak Djohan Effendi, seorang cendekiawan Muslim tentang kasus di Lombok ini. Dari curhat pak Djohan Effendi yang mengutip surat komunitas Ahmadiah di Lombok ini  tercermin di dalamnya fikiran-fikiran besar Bung Karno yang sudah sama-sama kita kenal (umpamanya tentang Pancasila dan Bhinneka Tunggal Ika) dan juga gagasan-gagasan agung  Gus Dur (pluralisme dalam masalah agama).



Curhat pak Djohan Effendi ini bisa diartikan secara tidak langsung juga menanggapi secara bagus, tepat, mendalam mengenai keresahan-keresahan yang akhir-akhir ini ditimbulkan oleh masalah FPI, persoalan Abu Bakar Ba’asyir, persoalan puasa di bulan Ramadhan, penutupan rumah-rumah ibadah, sweeping di rumah-rumah makan dan lain-lain masalah keresahan agama di negara kita dewasa ini.



Semangat atau isi tulisan pak Djohan Effendi tentang kasus di Lombok itu pada pokoknya senafas atau sejiwa dengan pandangan sejumlah tokoh-tokoh Indonesia lainnya dewasa  ini, seperti antara lain Ahmad Syafi’i Maarif (mantan ketua umum PB Muhammadiah), Suryo Paloh (pimpinan utama Nasional Demokrat yang santer mengumandangkan “tidak ada masalah majoritas dan minoritas” di kalangan bangsa Indonesia, dan tokoh-tokoh lainnya.



* *



Untuk memungkinkan para pembaca menghayati lebih dalam atau lebih baik lagi tulisan pak Djohan Effendi tersebut, maka di bawah ini disajikan teks selengkapnya, yang berbunyi sebagai berikut :



Jakarta, 7 Agustus 2010





Kepada Yang Terhormat

Para Petinggi Negara RI !

Para Pemuka Agama !

Para Pemimpin Parpol dan Ormas !!

Para Cerdik Cendekia dan Tokoh Masyarakat !





“Berilah kami tempat, Bapak Wali Kota, di mana saja di wilayah kota Mataram ini,di pinggiran yang dianggap angker banyak setannya sekalipun, atau di

pekuburan-pekuburan, yang penting kami dapat keluar dari penampungan, hidup

normal, menghirup udara kebebasan dan kemerdekaan.

Atau, jika telah dianggap menodai agama, telah melanggar UU No.1 PNPS/1/1965,

sebagaimana selama ini diancamkan, jebloskanlah kami, Bapak Wali Kota, ke dalam penjara. Kami seluruh warga Ahmadi, pengungsi laki-laki, perempuan, tua, muda maupun anak-anak, lahir batin, ikhlas dipenjara, tanpa proses hukum sekalipun.

Atau jika sama sekali tidak ada tempat bagi kami, di ruang penjara tidak ada

tempat bagi kami, di pekuburan-pekuburan juga tidak ada tempat bagi kami, maka

galikanlah bagi kami, Bapak Wali Kota, kuburan. Kami seluruh warga Ahmadi

pengungsi, laki-laki, perempuan, tua, muda maupun  anak-anak, siap dan ikhlas

dikubur hidup-hidup. …”



           Bapak-bapak Yang terhormat!



           Kalimat-kalimat di atas saya kutip dari surat yang berisi jeritan

warga Ahmadiyah Lombok, yang sejak beberapa tahun ini terpaksa tinggal di

penampungan, terusir dari tempat tinggal mereka, hanya karena mereka difatwakan

menganut faham yang sesat. Mereka menjadi pengungsi di negeri mereka sendiri.

Padahal mereka turun temurun warga negara RI. Mereka turun temurun  tinggal di

atas bumi yang disediakan oleh Allah Tuhan Yang Maha Rahman, yang menyediakan

bumi ini bagi segenap dan seluruh anak-cucu Adam, yang rahmat-Nya dikaruniakan

kepada segenap umat manusia tanpa diskriminasi, tidak membedakan beriman atau

kufur bersikap kufur kepada-Nya, beragama atau tidak, menganut ajaran yang benar

atau ajaran yang sesat. Peristiwa pengusiran dan pengungsian ini sama sekali

bukan kisah fiktif, tapi kisah nyata yang terjadi di negara kita yang

berdasarkan Pancasila yang di antara sila-silanya adalah Ketuhanan Yang Maha Esa

dan Kemanusiaan yang Adil dan Beradab. Peristiwa ini terjadi sekarang, tidak di

masa penjajahan, tidak di masa Revolusi Kemerdekaan, tidak di masa Pemerintahan

Parlementer, tidak di masa Orde Lama dan juga tidak di masa Orde Baru. Tapi

terjadi sekarang di masa Reformasi ketika Piagam Hak-hak Asasi Manusia diterima

dan dimasukkan dalam Konstitusi kita. Lalu di mana tanggung jawab konstitusional

para Petinggi Negara RI? Di mana tanggung jawab moral para pemuka agama bangsa

kita? Di mana hati nurani tokoh-tokoh parpol, ormas, cendekiawan dan pemuka

masyarakat kita?



           Dan sekarang Bapak-bapak yang terhormat, warga Ahmadiyah di Manis

Lor, Kuningan sedang terancam, mesjid tempat mereka sebentar lagi menunaikan

ibadah tarawih, tadarus, i’tikaf, akan disegel oleh Bupati sendiri. Pengalaman

perih dihalang-halangi dan diganggu untuk menjalankan ibadah menurut keyakinan

sendiri juga terjadi di Bekasi. Dua orang umat Bahai masih ditahan di Lampung.

Dilarang membuka warung sebagai usaha mencari nafkah sehari-hari. Seorang umat

Bahai yang meninggal dunia di Pati terpaksa dimakamkan di bentaran kali karena

ditolak Kepala Desa untuk dimakamkan di Pemakaman Umum Desa, bahkan dilarang

dimakamkan di lahannya sendiri. Penganut Aliran Kepercayaan Penghayat Ketuhanan

Yang Maha Esa, masih dipinggirkan, hak-hak sipil mereka tidak terjamin dan tidak

dipenuhi. Daftar berbagai kasus penistaan hak-hak asasi dan hak-hak sipil

terlalu panjang untuk dikemukakan. Komnas HAM mempunyai data yang relatif

lengkap tentang kasus-kasus seperti ini. Kenapa masih ada warga negara kita yang

tidak menikmati kebebasan berkeyakinan dalam negara yang berusia 65 tahuin ini?



           Pernahkah kita membayangkan bagaimana kalau nasib yang dialami warga

negara yang teraniaya dan terzalimi ini justru menimpa kita sendiri? Pernahkah

kita membayangkan betapa perihnya hati kita jika kebebasan kita untuk beriman

dan beribadah menurut ajaran yang kita yakini akan menyelamatkan kita di dunia

dan di akhirat kelak direnggut hanya karena kita berbeda dengan keyakinan

mayoritas?



           Menyaksikan peristiwa-peristiwa memerihkan di atas izinkanlah saya

bertanya kepada Para Petinggi dan Penguasa di negeri ini, apakah negara dan

pemerintah sudah tidak mampu lagi menjamin, melindungi dan mempertahankan

hak-hak asasi manusia dan hak-hak sipil yang tercantum dalam Konstitusi Negara

kita bagi kelompok-kelompok minoritas? Kepada siapa lagi mereka harus

mengharapkan perlindungan?



           Kepada Para Pemuka Agama, khususnya al-Mukarrimun Para Ulama,

perkenankan saya bertanya, apakah manusia yang non Islam, atau yang menganut

ajaran yang dianggap sesat itu, tidak termasuk anak-cucu Adam yang dimuliakan

dan dianugerahi rezeki oleh Tuhan (Q. 17:70) sehingga mereka halal dilecehkan,

diusir dan diperlakukan seolah-olah mereka tidak berhak hidup di atas bumi Tuhan

yang menciptakan mereka? Andaikan mereka tersesat, apakah mereka tidak bisa

menikmati kebebasan sebagaimana mereka yang kufur kepada Tuhan (Q. !8:29)

sehingga kita merasa berhak memaksa mereka untuk mengikuti pendapat dan

keyakinan kita? Apakah tidak sebaiknya kita mengikuti metoda yang dianjurkan

Tuhan dalam menyeru manusia ke jalan Tuhan dengan cara bijaksana, nasehat yang

baik dan kalau perlu dengan dialog yang lebih baik lagi; dan akhirnya

menyerahkannya kepada Allah sendiri yang lebih mengetahui siapa yang tersesat

dan siapa yang benar-benar beroleh petunjuk? (Q. 16:7). Dan bukankah

ketidaksukaan kita terhadap suatu kelompok tidak menghalalkan kita untuk

bertindak tidak adil terhadap mereka? (Q. 5:8).  Apakah menurut al-Mukarrimun

negara atau aparat pemerintah atau kelompok masyarakat berwenang membatasi

anugerah Allah berupa hak hidup di atas bumi-Nya kepada mereka yang dianggap

sesat? Apakah negara atau pejabat yang berkuasa berwenang membatasi kebebasan

berkeyakinan yang diberikan Allah al-Khaliq kepada manusia, makhluk yang

dimuliakan-Nya? Apakah hal itu tidak berarti merampas wewenang Allah dan hak

sesama manusia?



           Bapak-bapak yang terhormat!



Dengan surat ini saya hanya ingin menyampaikan jeritan hati nurani

saudara-saudara kita yang menderita.  Hati saya merasa tidak tahan lagi melihat

penderitaan saudara-saudara yang teraniaya tersebut, dan saya merasa berdosa

kalau saya tidak melakukannya.





Hormat Takzim saya





Djohan Effendi



= = = = =





 Para pembaca yang budiman, apa yang disajikan di atas adalah bahan renungan yang baik sekali kita coba kaji (sendirian atau bersama-sama) selama  dalam menjalani bulan puasa dewasa ini, dan juga menjelang peringatan Hari Kemerdekaan 17 Agustus yad.



Bung Karno dan Gus Dur, dua-duanya  pastilah menangis dan juga marah sekali di makam mereka masing-masing, kalau membaca surat komunitas Ahmadiah kepada Walikota Mataram (Lombok).

Sebab, bukan bangsa semacam yang ditunjukkan di Mataram itulah yang dicita-citakan oleh  para perinis kemerdekaan negara kita, dan para pejoang Revolusi 17 Agutus 45 !!!



Oleh karena itu,  sebagian dari kita yang sudah membaca surat kepada Walikota Mataram di Lombok tersebut di atas itu kiranya tergugah hati nurani mereka, untuk berbuat sesuau, sebisa-bisanya, sesuai dengan kondisi atau situasi kita masing-masing.
