---
layout: post
title: Bangkitlah menjadi bangsa mandiri !
date: 2007-08-22
---


Menjelang perayaan Hari Kemerdekaan 17 Agustus yang lalu ada satu hal penting yang patut mendapat perhatian besar dari kita semua, yaitu berbagai pernyataan atau ucapan di Jakarta dari Joseph E. Stiglitz, seorang pakar ekonomi yang terkenal di dunia, dan penerima hadiah Nobel untuk keahliannya di bidang ekonomi. Pakar ekonomi yang juga pernah menjadi penasehat penting (setingkat menteri) dari Presiden Clinton ini, telah mengeluarkan pendapatnya yang kritis sekali mengenai berbagai hal yang berkaitan dengan politik pemerintahan RI dalam masalah penanaman modal asing di Indonesia.

Tajamnya kritik Joseph E. Stiglitz ini bahkan bisa diangggap sebagai “tamparan” bagi banyak “economist” (ahli ekonomi) dan para pejabat tinggi Indonesia – terutama yang termasuk dalam golongan “Berkeley Maffia” – yang selama puluhan tahun (sejak 1967 !!!) sudah menggadaikan kekayaan bumi Indonesia kepada modal asing, terutama AS.

Kritik tajam Josepf Stiglitz ( yang juga professor di bidang ekonomi di Columbia University Business School dan pernah menjadi economist terkemuka di Bank Dunia) ini, patut sekali menjadi perhatian dari kalangan intelektual dan tokoh-tokoh masyarakat serta aktifis berbagai organisasi di Indonesia, yang selama ini juga sudah menentang politik pemerintah mengenai modal asing (terutama AS).

Dengan menyimak kritik Stiglitz mengenai politik pemerintah Indonesia (sejak 40 tahun, mulai dari Orde Baru yang diteruskan oleh berbagai pemerintahan berikutnya), maka kelihatan bahwa politik mengenai modal asing di Indonesia memang perlu sekali dirubah atau diperbaiki, demi kepentingan rakyat.

Kontrak dengan modal asing perlu di-negosiasi ulang.

Sebagian dari kritik economist terkemuka Stiglitz ini dapat dilihat dalam interrviewnya dengan Tempo, yang disiarkan oleh Tempo Interaktif (16 Agustus 2007), yang antara lain adalah sebagai berikut :

“Pemerintah diminta menegosiasi ulang kontrak-kontrak pertambangan yang terindikasi merugikan kepentingan rakyat. Joseph E. Stiglitz, pemenang hadiah Nobel, mengatakan, jika pemerintah Indonesia berani melakukan ini maka akan memperoleh keuntungan jauh lebih besar dibandingkan yang diperoleh para investor asing.

"Mereka (para perusahaan tambang asing) tahu kok bahwa mereka sedang merampok kekayaan alam negara-negara berkembang," kata Stiglitz dalam wawancara eksklusif dengan Tempo.

”Negosiasi ulang kontrak karya ini juga sangat mungkin dilakukan dengan Freeport McMoran, yang memiliki anak perusahaan PT Freeport Indonesia. Freeport merupakan salah perusahaan tambang terbesar di dunia yang melakukan kegiatan eksplotasi di Papua.

”Stiglitz mencontohkan ketegasan sikap Rusia terhadap Shell. Rusia mencabut izin kelayakan lingkungan hidup yang dikantongi Shell. Ini karena perusahaan minyak itu didapati melanggar Undang-Undang Lingkungan Hidup dengan melakukan pencemaran lingkungan. "Kalau melanggar undang-undang, ya izinnya harus dicabut dong," kata dia..

”Seperti ramai diberitakan beberapa waktu lalu, Freeport Indonesia melakukan pencemaran lingkungan di selama mengebor emas dan tembaga di Papua. Namun, kasus ini tidak pernah sampai ke pengadilan. Pemerintah hanya meminta perusahaan tambang asal Amerika Serikat itu memperbaiki fasilitas pengolahan limbahnya.

”Stiglitz juga menyoroti keberhasilan Bolivia menegosiasi ulang kontrak-kontrak karya dengan para investor asing yang menguasai penambangan minyak dan gas.

”Negara miskin Amerika Latin itu sekarang memperoleh keuntungan yang jauh lebih besar. "Jika sebelumnya hanya memperoleh keuntungan 18 persen, sekarang sebaliknya mereka yang mendapat 82 persen," ujarnya. Dan para investor asing itu, kata dia, tetap disana.

”Untuk menetralisir tekanan yang muncul dari negara besar seperti Amerika Serikat yang mendukung perusahaan asing secara diam-diam, sepeti ExxonMobil, Stiglitz punya saran. Menurutnya, media massa harus mempublikasikannya. "Masyarakat pasti akan sangat marah ketika mengetahuinya, sehingga kontrak-kontrak itu akan dinegosiasi ulang."

”Ia menyesalkan sikap seorang duta besar Amerika Serikat yang sempat meminta Indonesia menghormati kontrak-kontrak pertambangan yang terindikasi korupsi. Pejabat itu akhirnya diberi posisi manajemen oleh sebuah perusahaan tambang besar asing. "Ketika dia menguliahi Indonesia tentang korupsi, justru dia sedang mempraktekkannya," kata Stiglitz yang enggan menyebut nama pejabat itu. (kutipan dari Tempo Interaktif selesai)


Stiglitz: Indonesia korban globalisasi

Apa yang diutarakan oleh Joseph Stiglitz di atas ini dibkin lebih jelas lagi dengan keterangannya yang lain yang berkaitan dengan pengalaman Argentina, yang juga menolak kebijakan-kebijakan Washington, yang disiarkan oleh harian Bisnis Indonesia tanggal 15 Agustus 2007. Dalam berita di koran tersebut, yang judulnya “Stiglitz: Indonesia korban globalisasi”, berbunyi sebagai berikut :

“ Joseph Stiglitz, peraih hadiah Nobel ekonomi 2001, berpendapat Indonesia adalah korban globalisasi yang masih menghadapi tantangan berat berkaitan dengan kemiskinan, pengangguran dan memacu pertumbuhan ekonomi, sekalipun perekonomian sudah mulai pulih dari krisis 1997/1998.

Selain itu, menurut mantan ekonom berpengaruh di Bank Dunia tersebut, Indonesia juga menghadapi tantangan kerusakan lingkungan akibat kesepakatan investasi yang lebih melindungi kepentingan investor.

"Pertumbuhan ekonomi sudah pulih dari krisis [1997/98], tetapi tidak cukup cepat dalam menciptakan cukup pekerjaan dan mengurangi kemiskinan," tutur pencetus teori informasi asimetris yang mengantarkannya sebagai pemenang hadiah Nobel pada 2001 itu. Stiglitz berbicara dalam diskusi publik sekaligus peluncuran buku Making Globalization Work, yang diselenggarakan Tempo,

Saat ini, level pertumbuhan ekonomi Indonesia masih jauh di bawah rata-rata sebelum krisis, yang pernah berada di atas 7% per tahun. Jika Indonesia gagal menerapkan kebijakan sendiri dalam mengelola dampak globalisasi, Stiglitz khawatir persoalan kemiskinan, pengangguran dan dampak lingkungan akan kian memburuk.

Mengambil contoh Argentina, yang juga terpukul krisis ekonomi dan diperburuk kebijakan IMF, pendekatan kebijakan yang berbeda membuat hasil yang berbeda. Argentina berhasil mengatrol pertumbuhan ekonomi rata-rata 8% per tahun setelah krisis.

"Penjelasan atas perbedaan itu adalah, Argentina menolak kebijakan-kebijakan berdasarkan Washington Consensus," Stiglitz menegaskan.

Konsensus Washington adalah paket kebijakan generik yang lazimnya disodorkan IMF dan Bank Dunia dengan mengencangkan ikat pinggang, seperti dianjurkan IMF terhadap Indonesia pascakrisis 1997/1998.Bahkan dengan tidak menganut Konsensus Washington, Argentina mampu membukukan surplus anggaran dan menurunkan angka pengangguran.

Stiglitz melihat negara berkembang yang patuh terhadap Konsensus Washington dengan menerapkan resep IMF dan Bank Dunia-melalui liberalisasi pasar modal- telah terjerumus ke dalam volatilitas pasar, selain terjebak pada kebijakan privatisasi yang korup. (kutipan dari Bisnis Indonesia selesai)


Pengalaman Bolivia dan Argentina

Dengan membaca dua berita yang disiarkan Tempo dan Bisnis Indonesia tersebut di atas, maka kiita bisa mendapat kesan bahwa apa yang diucapkan oleh Joseph Stiglitz ini merupakan kritik yang tajam sekali; bahkan bisa diatakan sebagai “tamparan” yang tidak tanggung-tanggung, bagi berbagai pejabat dan tokoh terkemuka masyarakat, yang sudah menjadikan RI sebagai sapi perahan bagi kepentingan asing, sedangkan ratusan juta penduduk Indonesia telah menderita karena kemiskinan dan kesengsaraan yang berkepanjangan dan 13 juta anak-anak kekurangan makan.

Dengan mengambil contoh pengalaman-pengalaman Bolivia, dan Argentina,yang dapat memperbaiki ekonomi secara radikal dan meningkatkan kehidupan rakyat karena mentrapkan secara berani sikap “tidak tunduk” kepada dikte Washington, maka Stiglitz menganjurkan kepada pejabat-pejabat Indonesia supaya berani mengambil tindakan yang sama atau searah. Ia juga menganjurkan supaya masalah praktek-praktek buruk berbagai perusahaan besar asing ini banyak diangkat dalam media massa di Indonesia, untuk menggugah kemarahan orang banyak terhadap praktek-praktek buruk modal besar asing ini.

Anjuran Stiglitz semacam ini kiranya tepat sekali. Sebab, sejak 1967 (jadi sudah 40 tahun !!!, dan itu jangka waktu yang lama sekali) Orde Baru sudah membuka pintu lebar-lebar kepada banyak perusahaan-perusahaan besar asing untuk mengeruk kekayaan bumi Indonesia secara besar-besaran, seperti Freeport, Exxon, Newmont, Shell dll. Sejak itulah para pejabat rejim militer Orde Baru telah menjadikan Indonesia sebagai klien (langganan) kepentingan AS.

Sekarang makin terbukti bahwa kehadiran perusahaan-perusahaan besar asing dalam bidang-bidang penting perekonomian (di bidang pertambangan, minyak dan gas, agro-alimentaire) di Indonesia menjadikan negara kita sangat tergantung kepada kepentingan luarnegeri, sehingga bangsa dan negara kita kehilangan kemandiriannya. Di samping itu, selama ini sudah terbukti juga bahwa kehadiran perusahaan-perusahaan asing itu telah menjadi sumber korupsi para pejabat tinggi pemerintahan melalui macam-macam penyalahgunaan (ingat, umpamanya, “setoran” Freeport yang besar jumlahnya untuk kerjasama dengan militer di Papua)


Bertolak belakang dengan sikap J. Kalla

Pernyataan Stiglitz mengenai pentingnya Indonesia meninjau kembali perjanjian-perjanjiannya (kontrak karya) dengan maskapai-maskapai besar asing internasional mencerminkan visi dan tuntutan berbagai organisasi di Indonesia, seperti Wahana Lingkungan Hidup, Jaringan Advokasi Tambang, Koalisi Anti Utang, Papernas, Kosortium Pembaruan Agraria, Partai Rakyat Pekerja (dan banyak lainnya). Juga sesuai dengan aspirasi berbagai intelektual dan ormas, yang sudah makin banyak mengangkat suara yang juga makin lama makin keras, sebagai protes mengenai kasus Freeport, Newmont, blok Cepu dll.

Pernyataan Stiglitz ini mencerminkan -- dengan amat jelas! -- sikap yang bertolak-belakang sama sekali dengan sikap yang dipasang Wapres Jusuf Kallla, yang “dengan genderang yang ramai” telah berkunjung ke AS untuk menemui penggede-penggede perusahaan besar AS dan mengundang mereka untuk berinvestasi di Indonesia.

Kritik Stiglitz tentang praktek-praktek perusahaan-perusahaan besar asing (terutama AS) di Indonesia dan anjurannya untuk melawannya merupakan dukungan yang besar sekali kepada semua golongan yang selama ini sudah melakukan berbagai macam aksi untuk menentang perampokan kekayaan negara dan bangsa kita ini.

Berhubung dengan makin bangkitnya kesadaran di kalangan intelektual, mahasiswa, dan pemimpin-pemimpin organisasi di Indonesia tentang kerugian-kerugian bagi negara dan rakyat yang disebabkan oleh modal besar asing, maka bisa diharapkan bahwa kritik tajam dan anjuran Stiglitz seperti tersebut di atas akan lebih menggalakkan perlawanan bersama terhadap akibat-akibat negatif dari globalisasi dan neo-liberalisme.

Sekarang situasi negeri sudah makin menunjukkan bahwa sudah cukuplah kita menjadi bangsa kuli. Kita harus bersama-sama bangkit menjadikan bangsa kita mandiri!
