---
layout: post
title: Benarkah bahwa presiden SBY adalah public enemy (musuh publik) ?
date: 2010-10-14
---

Hugo Chavez keluar dari Istananya untuk menampung korban banjir di dalamnya



Setelah mengikuti banyak berita-berita pers dan berbagai siaran televisi akhir-akhir ini, kita bisa bertanya-tanya apakah presiden SBY masih mendapat kepercayaan rakyat untuk menangani berbagai persoalan besar dan berat yang dihadapi bangsa dan negara dewasa ini. Sebab, terdengar makin banyak suara dari berbagai kalangan, yang mengindikasikan bahwa kepercayaan publik terhadap kepemimpinan presiden SBY sudah anjlog. Bukan itu saja, ketidakpercayaan terhadap SBY ini sudah meningkat menjadi kemarahan dari bermacam-macam kalang            an di banyak tempat di seluruh Indonesia.

Bahkan, dalam salah satu tayangan di televisi Metro TV baru-baru ini ada orang yang mengatakan bahwa presiden SBY sekarang sudah menjadi public enemy (musuh publik). Mungkin saja, kata-kata public enemy bisa dianggap keterlaluan kasarnya atau kebablasan, tetapi ini mencerminkan kemarahan publik yang sudah makin memuncak terhadap SBY akhir-akhir ini.

Sebagian dari ketidakpuasan banyak kalangan, bahkan kemarahan rakyat, telah dimanifestasikan oleh banyaknya aksi-aksi atau demo yang dilakukan oleh organisasi pemuda dan mahasiswa di banyak kota di Indonesia dalam rangka Hari Anti Korupsi Sedunia dan hari HAM sedunia. Dalam dua peringatan ini telah diangkat kembali masalah korupsi dan pelanggaran HAM yang masih tetap menjadi persoalan besar yang tidak bisa diselesaikan bangsa kita.

Korupsi sumber ketidakpercayaan kepada SBY

Terutama masalah korupsi merupakan sumber besar  ketidak-percayaan dan kemarahan rakyat terhadap presiden SBY. Kasus Bank Century yang menyangkut uang negara lebih dari RP 6 triliun yang tidak jelas juntrungnya, ditambah dengan kasus Gayus Tambunan yang menghebohkan seluruh negeri, menunjukkan bahwa presiden SBY tidak menunjukkan kepemimpinan yang diinginkan oleh rakyat banyak. Singkatnya, sangat sangat mengecewakan !!!

Baik dalam kasus Bank Century maupun kasus Gayus Tambunan dirasakan adanya hal-hal yang menimbulkan dugaan bahwa presiden SBY tidak mau, atau tidak berani, atau tidak bisa, bertindak tegas sebagai pemimpin negara dan pemimpn pemerintahan, dengan alasan « tidak mau memasuki ranah hukum », « tidak mau intervensi ».

Kalau dalam kasus Bank Century ada kecurigaan-kecurigaan adanya hal-hal yang « tidak lurus «  yang dilakukan oleh pendukung-pendukung Partai Demokrat yang dipimpin oleh SBY, maka dalam kasus Gayus Tambunan banyak orang mempertanyakan mengapa SBY berusaha supaya kasus Gayus ini ditangani oleh polri saja, yang sudah mengindikasikan bahwa Gayus akan dikenakan perkara gratifikasi saja, dan bukan perkara penyuapan.

Kelihatannya ada kalangan-kalangan yang menduga-duga bahwa keputusan SBY tentang penanganan masalah Gayus ini supaya dilakukan terutama oleh tangan-tangan polri, dan bukannya oleh KPK, adalah karena polri adalah langsung di bawah presiden SBY, seperti halnya kejaksaan agung. Dengan begitu presiden SBY bisa ikut « mengarahkan » penanganan kasus Gayus, dan dengan cara demikian  SBY beserta pendukung-pendukungnya dapat menyelamatkan penunggak pajak raksasa yang jumlahnya sekitar 150 perusahaan besar.

Dengan dalih bahwa Gayus hanya akan dikenakan perkara gratifikasi, maka meskipun ia sudah menerima uang suapan sebanyak sekitar Rp 100 miliar, ia akan dijatuhi hukuman yang ringan sekali, kalau tidak dibebaskan sama sekali. Yang paling aneh atau keterlaluan tidak masuk nalar yang waras adalah bahwa dengan dipakainya rumus « gratifikasi » maka perusahaan-perusahaan yang pernah menyuap Gayus (termasuk 3 perusahaan besar grup Aburizal Bakrie) akan bebas dari tuntutan hukum.

Kelemahan,  keragu-raguan, ketidak-beranian kepemimpinan SBY

Dengan munculnya kasus Gayus Tambunan, maka tidak saja kelihatan masih merajalelanya korupsi yang berkelas kakap di Indonesia, melainkan juga tetap terus bobroknya atau busuknya aparat-aparat hukum negeri kita. Dalam sejarah Republik Indonesia, tidak ada kebobrokan atau kerusakan di kalangan kepolisian, kejaksaan dan kehakiman (pengadilan) seluas dan separah seperti yang terjadi di masa pemerintahan SBY sekarang ini.  Hanya pemerintahan Orde Baru di bawah Suhartolah yang bisa mengalahkan atau menyamai kebobrokan pemerintahan SBY.

Banyak orang mengkaitkan kebusukan atau kebobrokan di kalangan pimpinan Polri dan Kejaksaan Agung (dan juga kehakiman atau pengadilan) dengan kelemahan, atau kelambatan, atau keragu-raguan, atau kehati-hatian, atau ketidak-beranian kepemimpinan SBY. Ada juga yang menghubungkan ketidak-tegasan SBY ini dengan kekuatirannya bahwa hal-hal yang termasuk « suram » yang berkaitan dengan kemenangannya sebagai capres dalam pemilu bisa dibongkar atau dikutik-kutik.

Di samping adanya kenyataan bahwa politik pemerintahan di bawah SBY memang menjalankan politik yang pro neo-liberal dan tidak menguntungkan rakyat, SBY juga diikat oleh koalisi yang terdiri dari partai-partai yang juga sama-sama reaksionernya.  Karena itu, koalisi partai-partai reaksioner yang duduk dalam DPR ini menjadi ajang kongkalikong dalam berbagai bentuk dan cara, dan melakukan berbagai macam kejahatan berjemaah terhadap kepentingan rakyat. Sebagian dari partai-partai ini sudah menjadi pengkianat rakyat, dan karenanya   -- pada hakekatnya !  --  juga sudah menjadi musuh rakyat..

Dengan pandangan semacam itu, maka sebenarnya,  atau pada intinya,   bukan hanya SBY saja yang menjadi public enemy, melainkan juga partai-partai yang mendukungnya. Oleh karena itu, berbagai fenomena di negeri kita menunjukkan  bahwa SBY sudah makin jauh dari rakyat. Terasa sekali bahwa tidak ada hubungan hati dan fikiran yang hangat dan erat antara presiden SBY dan rakyat banyak.

Sekarang makin kelihatan  bahwa berbagai kalangan di masyarakat tidak hanya kehilangan kepercayaan kepada aparat kepolisian, kejaksaan, dan kehakiman atau pengadilan, melainkan juga kepada pribadi presiden SBY. Ketidak-percayaan rakyat terhadap kepemimpinan SBY ini, yang sudah  turun sejak lama, akhir-akhir ini bertambah parah dengan munculnya « blunder » (kesalahan besar) mengenai hiruk-pikuk kasus keistimewaan daerah Jogyakarta. Rakyat Jogyakarta sudah memasang spanduk besar-besaran yang bertuliskan SBY= Sumber Bencana Yogya

Begitu hebatnya kemerosotan kepercayaan terhadap pemerintahan yang dipimpin SBY sehingga dalam aksi-aksi yang dilakukan baru-baru ini di berbagai kota dikibarkan bendera Merah Putih setengah tiang, sebagai tanda keprihatinan dan kemarahan. Puncak kemarahan ini terjadi pada tanggal 13 Desember ketika warga seluruh kota Jogya mengibarkan bendera setengah tiang, dan puluhan ribu penduduk secara beramai-ramai menyaksikan sidang  terbuka DPRD Jogya yang membicarakan soal keistimewaan daerah ini. Peristiwa ini merupakan « pemberontakan damai » atau tantangan penduduk Jogya terhadap pemerintahan SBY, atau setidak-tidaknya merupakan pukulan yang serius terhadap muka presiden SBY.

Perkembangan fikiran atau opini publik terhadap kepemimpinan presiden SBY ini sangat gawat dan bahkan bisa menimbulkan berbagai gejolak masyarakat yang makin lama bisa makin membesar, karena SBY beserta partai-partai koalisinya tidak akan bisa mengadakan perubahan-perubahan besar guna memperbaiki situasi politik, ekonomi dan sosial yang makin ruwet nantinya. Terutama sekali pemerintahan SBY tidak akan mungkin  dapat segera menyelesaikan masalah korupsi yang sudah merusak moral janjangan yang paling atas sampai paling bawah.

Banyak orang melihat dengan lebih terang ketokohan SBY

Kekecewaan dan kemarahan banyak kalangan terhadap SBY (dan pendukung-pendukungnya) mengindikasikan bahwa opini publik kita sudah bisa melihat lebih terang lagi kepada « ketokohan » SBY sebagai pemimpin rakyat dan negara. Walaupun SBY telah dipilih sebagai presiden secara langsung dengan perolehan suara sekitar 62% dalam pemilu yang lalu, namun sekarang ternyata bahwa banyak orang sudah tidak lagi menyukai tindakan-tindakan atau sikapnya, terutama tentang korupsi. Dewasa ini Indonesia merupakan negara yang ter-korup di daerah Asia-Pasifik.

Ketika dalam pemilu yang lalu banyak sekali orang yang mengharapkan (atau mengira) bahwa SBY akan bisa merupakan presiden yang  betul-betul bertindak sebagai pemimpin rakyat, maka mereka kemudian  merasa sebagai tertipu mentah-mentah. Ada yang berpendapat bahwa SBY ternyata bukan tokoh yang bisa menjadi contoh sebagai pemimpin rakyat. Di bawah pemerintahannya situasi negara dan bangsa tambah ruwet, atau penuh gejolak. Perdebatan panas mengenai keistimewaan daerah Jogya hanyalah salah satu bagian saja dari banyaknya persoalan parah yang harus dihadapi SBY.

Perbedaan besar kepemimpinan SBY dengan Bung Karno

Dari berbagai  tindakannya, atau sikapnya, mengenai macam-macam  soal yang berkaitan dengan negara dan bangsa, nampak sekali perbedaannya dengan kepemimpinan Bung Karno. Kalau kebesaran sosok dan keagungan ajaran-ajaran revolusioner Bung Karno sampai sekarang masih bersemayam di hati banyak sekali orang, --walaupun ia sudah wafat 40 tahun yang lalu, akibat siksaan Suharto selama dalam tahanan – maka kelihatannya nama SBY tidak mendapat tempat yang terhormat dalam hati rakyat.

Sesudah pengkhianatan besar-besaran oleh Suharto terhadap pemimpin rakyat dan bangsa, Bung Karno, maka negara kita belum mempunyai lagi pemimpin lainnya yang bisa dikategorikan sebagai pemimpin rakyat yang sebenarnya, yang seagung dan seluhur Bung Karno. Kita bisa melihat bahwa semua (atau sebagian terbesar sekali)  tokoh Indonesia yang pernah mengaku dirinya sebagai pemimpin rakyat adalah sebenarnya tidak pantas dinamakan pemimpin  rakyat. Karena mereka tidak memiliki sifat yang bisa dijadikan contoh, atau tindakan-tindakannya tidak bisa menimbulkan hormat bagi banyak orang.

Contoh dari Venezuela : presiden Hugo Chavez

Dalam kaitan ini, kiranya bisa diambil contoh dari presiden Venezuela, Hugo Chavez, seorang mantan perwira militer yang berpandangan revolusioner kerakyatan, yang terpilih secara demokratis sebagai presiden. Sejak terpilih menjadi presiden Venezuela, ia telah melakukan perubahan-perubahan besar-besaran di bidang politik, ekonomi, sosial, kebudayaan, militer, dan hubungan luar negeri, yang pada pokoknya selalu mementingkan rakyat dan anti-imperialisme (terutama AS). Karena kedekatannya dengan rakyat, maka rakyat Venezuela selalu menyokongnya dalam melawan musuh-musuh dalam negeri maupun luar negeri.

Menurut TEMPO Interaktif, (11 Des O3), Presiden Venezuela Hugo Chavez berencana untuk memerintah (berkantor) sementara dari sebuah tenda pemberian pemimpin Libya Muammar Gaddafi setelah mengundang para keluarga yang kehilangan tempat tinggal akibat hujan deras untuk tinggal di istananya.

Hujan terburuk dalam satu dekade ini telah menimbulkan malapetaka di negara eksportir minyak utama Amerika Selatan itu dengan menewaskan lebih dari 30 orang dan menyebabkan kehilangan tempat tinggal lebih dari 100 ribu orang di desa-desa pesisir dan daerah kumuh kota.

Dari Istana kepresidenan pindah berkantor ke tenda

"Siapkan hadiah Gaddafi. Anda dapat memasangnya di taman Miraflores, menaruhnya di sana karena aku akan pindah ke tenda," kata Chavez saat mengunjungi pengungsian di lingkungan miskin di belakang istana presiden Miraflores. Gaddafi dikenal karena telah memimpin sidang di sebuah tenda Badui besar ketika melakukan kunjungan luar negeri dan menggunakannya saat dalam perjalanan ke Venezuela, tahun lalu.

Presiden Hugo Chavez memindahkan 25 keluarga ke istana pada bulan November dan  mengatakan kepada pembantunya untuk mempercepat persiapan untuk menerima lagi 80 keluarga.  "Kita bisa menaruh beberapa tempat tidur di kantor utama saya," katanya. Chavez telah turun langsung ke seluruh negeri untuk mengawasi bantuan kemanusiaan. Demikian berita Tempo Interaktif.

Sudah tentu, tindakan Hugo Chavez, seorang mantan militer yang berjiwa sosialis revolusioner, untuk menampung di Istananya sebagian penduduk Venezuela yang terkena banjir dan menyediakan juga tempat-tempat tidur bagi mereka di kantor utamanya adalah sesuatu yang sama sekali « aneh » atau luar biasa bagi para penguasa di Indonesia.

Ketika membaca berita tentang tindakan-tindakan Hugo Chavez, mungkin ada di antara kita yang ingat kepada banyaknya penduduk lereng Merapi, Mentawai, Wasior dan lain-lain, yang sangat menderita karena ketimpa bencana.  Kalau seandainya para penguasa di Indonesia semuanya mempunyai sikap pro rakyat dan karakter politik seperti Hugo Chavez maka nasib rakyat kita akan jadi lain, tidak seperti sekarang ini.

Mengingat itu semua, maka kita bisa menarik pelajaran   -- dan juga mengambil kesimpulan  -- bahwa dengan pemimpin-pemimpin sejenis dan sekaliber SBY, maka negara dan bangsa kita tidak akan mungkin mengadakan perubahan-perubahan besar dan fundamental yang menguntungkan kepentingan rakyat. Artinya,  dengan tokoh-tokoh pendukung SBY yang dewasa ini mengangkangi kedudukan-kedudukan kunci di badan-badan eksekutif, legislatif,dan judikatif (dan dunia usaha !!!) , maka situasi bangsa dan negara tidak akan mungkin  meraih perbaikan, dan bahkan sebaliknya,  akan menjadi makin memburuk.

Indonesia akan bisa mengadakan perubahan-perubahan besar, hanya melalui jalan reformasi yang menyeluruh dan restorasi yang  luas sekali, yang bisa berbentuk revolusi rakyat, seperti yang ditunjukkan oleh Bung Karno, atau oleh praktek revolusioner presiden Hugo Chavez. Jalan lainnya, seperti pekerjaan tambal sulam yang dilakukan oleh SBY beserta pendukung-pendukungnya dewasa ini,  adalah jalan buntu.
