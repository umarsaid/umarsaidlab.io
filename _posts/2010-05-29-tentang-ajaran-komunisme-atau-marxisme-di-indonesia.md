---
layout: post
title: Tentang ajaran komunisme atau marxisme di Indonesia
date: 2010-05-29
---

Sesudah PKI dan marxisme dilarang di Indonesia  sejak 1966 (44 tahun), bagaimanakah perkembangannya sekarang, dan apakah komunisme atau marxisme  masih bisa hidup di Indonesia di kemudian hari ? Dan bagaimanakah perkembangan komunisme atau marxisme  di dunia sekarang ini ?

Tulisan ini mengajak para pembaca, termasuk mereka yang anti-PKI atau  yang tidak menyukai komunisme atau marxisme, untuk mencoba berusaha menelaah masalah-masalah ini dari berbagai segi, dengan mempelajari  - sebanyak mungkin dan sebisanya  -- sejarah lahirnya komunisme di Indonesia dan di dunia, berikut segala persoalan yang dihadapinya selama ini.

PKI, sebagai organisasi partai politik,  secara resminya memang sudah dilarang oleh penguasa militer di bawah pimpinan Suharto, tetapi apakah komunisme atau marxisme dapat dilarang, atau dicegah untuk dianut oleh siapapun di Indonesia ? Kiranya, untuk pertanyaan semacam itu bisa saja orang menjawab macam-macam, yang itu semua adalah hak setiap orang. Namun demikian, tulisan ini dengan tegas menyatakan : TIDAK, TIDAK BISA !!! (mohon diperhatikan, ditulis dengan huruf besar dan tanda seru tiga kali) .   Sebabya, antara lain adalah hal-hal sebagai berikut :

Bung Karno adalah satu-satunya di antara banyak pemimpin atau tokoh Indonesia yang dengan jelas, gigih, dan juga konsekwen ( ! ) mengemukakan pendiriannya atau keyakinannya bahwa isi atau jiwa yang terkandung dalam komunisme, atau marxisme atau sosialisme sudah dipunyai oleh sebagian rakyat Indonesia sejak dulu, bahkan sebelum lahirnya PKI dalam tahun 1920.

Buku « Dibawah bendera revolusi » dan «  Revolusi belum selesai »

Bung Karno telah sering membeberkan pokok-pokok isi  -- atau inti jiwa  --  yang terkandung dalam komunisme atau marxisme atau sosialisme dalam berbagai pidatonya yang dikumpulkan dalam buku « Dibawah bendera revolusi » dan « Revolusi belum selesai ». Untuk bisa mengerti mengapa Bung Karno mempunyai pandangan yang demikian itu, seyogianyalah kita semua berusaha menyimak buku-buku tersebut serta mendalami isinya

Dalam pidato atau tulisan-tulisannya itu ia berulangkali dan sering sekali menjelaskan bahwa karena sejarah bangsa yang selama 350 tahun dijajah Belanda dan sebagai akibat dari berbagai macam penindasan, maka timbullah di banyak kalangan rakyat Indonesia kemauan atau tekad untuk mengadakan perlawanan, dengan berbagai cara, bentuk dan jalan.

Api perlawanan terhadap penjajahan  Belanda yang pernah berkobar sejak permulaan abad ke 18 di Sumatera, Jawa, Kalimantan, Sulawesi, Maluku, Ambon dan tempat-tempat lainnya, telah menjadi makin besar dengan adanya pembrontakan PKI dalam tahun 1926 di Jawa dan Sumatera Barat. Karena pembrontakan PKI di tahun 1926 itulah  jiwa perlawanan terhadap kolonialisme/imperialisme yang dikandung oleh ajaran komunisme atau marxisme makin dikenal oleh kalangan perjuangan untuk kemerdekaan.

Sumbangan besar  ajaran komunisme untuk perjuangan

Para pengamat sejarah dapat melihat bahwa banyak sekali di antara para perintis kemerdekaan atau tokoh-tokoh gerakan nasionalis (termasuk yang dari golongan Islam) yang makin tergugah semangat perjuangan mereka untuk melawan kolonialisme Belanda setelah mereka mengenal atau mendengar – sedikit banyaknya – tentang jiwa perlawanan terhadap segala macam ketidakadilan yang menjadi ciri-ciri utama komunisme atau marxisme. Fenomena yang demikian ini tidak hanya terdapat di kalangan pemuda/mahasiswa yang tergabung dalam Perhimpunan Indonesia di negeri Belanda waktu itu, melainkan juga di banyak partai-partai dan macam-macam organisasi di tanahair.

Jadi, jelaslah bahwa jiwa komunisme atau isi marxisme merupakan salah satu di antara pendorong semangat atau sumber berbagai inspirasi bagi banyak  pejuang perintis kemerdekaan, terutama sekali Bung Karno. Dalam kaitan ini, baik sekali kita ingat-ingat bahwa  PKI-pun lahir dari salah satu seksi Sarekat Islam yang dipimpin HOS Tjokroaminoto. Dari sinilah kelihatan sumbangan besar dari jiwa komunisme atau marxisme untuk perjuangan kemerdekaan nasional kita.

Dengan mengingat itu semua, maka kiranya dapat dimengertilah mengapa Bung Karno tetap terus membela marxisme dan PKI dengan begitu gigihnya, ketika Suharto beserta para jenderal pendukungnya membunuhi jutaan anggota dan simpatisan PKI sesudah terjadinya G30S. Dan kita dapat mengerti pula mengapa ia tetap setia mempertahankan gagasan besarnya NASAKOM, meskipun ditentang oleh semua golongan reaksioner di Indonesia (baik sipil maupun militer)) yang bersekutu dengan fihak nekolim (terutama AS dengan CIA-nya).

Bagaimana dengan keadaan sekarang ?

Sekarang ini, sesudah 44 tahun PKI dan marxisme dinyatakan terlarang dengan Ketetapan MPRS 25/1966, ada perkembangan situasi yang menarik di Indonesia. Walaupun resminya saja larangan penyebaran marxisme masih belum dicabut, tetapi buku Das Capital bisa diterbitkan oleh Hasta Mitra, yang peluncurannya diberkahi oleh pidato Gus Dur. Di samping itu sudah banyak sekali  buku yang berisi segala macam bahan tentang PKI atau marxisme, walaupun dengan berbagai nuansa atau pandangan, yang sudah beredar sejak lama.

Walaupun Kejaksaan Agung masih juga melarang  sejumlah buku (antara lain buku Berjudul Dalih Pembunuhan Massal Gerakan 30 September dan Kudeta Suharto, karangan John Roosa, dan Lekra Tak Membakar Buku Karangan Rhoma Dwi Aria Yuliantri dan Muhidin M. Dahlan) namun perkembangan situasi (nasional dan internasional) mengindikasikan arah yang makin tidak menguntungkan golongan yang tetap ngotot bersikap reaksioner atau kontra-revolusioner, yang terus memusuhi kalangan progresif di masyarakat Indonesia, dan yang juga bersekutu dengan kaum reaksioner asing dan neo-liberalisme.

Ini kelihatan dari makin menanjaknya atau meluasnya perlawanan berbagai golongan di Indonesia terhadap segala kebobrokan moral, kebusukan mental,  dan segala kejahatan (terutama korupsi kalangan atas, umpamanya kasus BLBI, Bank Century, pengemplangan pajak Aburizal Bakri sebesar Rp 2,1 triliun, banyak kasus yang dibongkar Susno Duadji dll dll dll)

Jiwa komunisme/marxisme akan terus berkembang

Dari banyaknya aksi-aksi yang dilancarkan oleh berbagai macam kalangan  pemuda/mahasiswa dan organisasi buruh, tani dan wanita, dan dari kerasnya suara yang dilontarkan terhadap politik reaksioner oleh pemerintahan SBY yang bersekongkol dengan kekuatan neo-liberalisme, dapat tercium adanya semangat perlawanan yang terkandung dalam jiwa revolusioner marxisme. Inti atau jiwa marxisme adalah perlawanan terhadap segala macam penindasan, dan segala kejahatan  yang dilakukan oleh golongan reaksioner.

Dan karena adanya berbagai kebobrokan yang parah dewasa ini (dan  yang pastilah akan masih juga tetap parah di masa datang ! )  yang merugikan kepentingan rakyat banyak, maka perlawanan berbagai golongan dalam masyarakat akan makin banyak dan berkembang dengan macam-macam cara, jalan dan bentuk. Dalam perlawanan ini macam-macam golongan dapat menggunakan berbagai  macam pedoman atau pegangan. Karena jiwa marxisme sudah terbukti di dunia selama ini sebagai pegangan yang ideal bagi perjuangan banyak kalangan melawan penindasan dan membela keadilan, maka di Indonesiapun  jiwa marxisme akan terus tumbuh dan berkembang.

Dengan pandangan dari sudut ini dapatlah kiranya  dikatakan dengan pasti bahwa jiwa marxisme tidak bisa dimatikan di Indonesia walaupun ada ketetapan MPRS no 25/1966. Apalagi sekarang ini !!! Sebab jiwa ini bisa selalu hadir  -- dengan berbagai cara, bentuk dan jalan  --  untuk menemani atau mengantar setiap perjuangan  melawan ketidakadilan dan membela kepentingan yang sah bagi rakyat banyak. Jiwa marxisme (atau komunisme) ini bisa merangkak atau  berkeliaran kemana-mana, dan bersarang dalam fikiran atau bersemayam dalam hati banyak orang atau kalangan dan golongan yang berjuang dengan sungguh-sungguh

Marxisme/komunisme melalui Google di Internet

Kita bisa sama-sama melihat, bahwa dengan adanya Internet, maka larangan penyebaran dan menerima segala macam bahan atau informasi mengenai PKI, atau ajaran-ajaran komunisme dan marxisme, menjadi tidak ada artinya bagi banyak orang. Kasarnya, larangan itu, yang tadinya dalam masa puluhan tahun rejim militer Orde Baru telah menjadi momok bagi banyak orang, sekarang telah  menjadi bahan tertawaan, atau sesuatu yang mengandung kebodohan

Sebab, melalui Internet (terutama dengan menggunakan Google atau Ensiklopedi Wikipédia) setiap orang dapat dengan leluasa, dan setiap waktu, mendapatkan berbagai macam bacaan sebanyak-banyaknya tentang komunisme atau marxisme, baik yang di Indonesia maupun di dunia. Larangan lewat Ketetapan MPRS ini sekarang terbukti sudah tidak mampu mencegah orang untuk memperoleh atau mengedarkan segala macam bacaan tentang marxisme.

Juga larangan tentang kegiatan PKI sudah tidak lagi mempunyai arti seperti yang sudah-sudah, kecuali hanya menunjukkan watak yang reaksioner dari berbagai pemerintahan di Indonesia, dan memperlihatkan kepada seluruh dunia wajah buruk yang tidak demokratis. Sebab, para panganut ajaran  komunis atau marxis, baik yang tergabung dalam PKI maupun yang di luar PKI, masih bisa terus bergerak dan mempraktekkan ajaran-ajaran revolusioner yang terkandung dalam komunisme dan marxisme, dengan macam-macam cara, bentuk dan jalan.

Kiranya, mudahlah dapat diduga atau diperkirakan bahwa orang-orang atau kalangan yang sungguh-sungguh berhaluan komunis atau marxis akan berjuang terus, walaupun PKI dilarang, dengan berbagai jalan yang mungkin. Kita tidak tahu siapa saja dan di mana saja atau dalam gerakan apa saja para penganut komunisme atau marxisme di Indonesia sudah, sedang atau akan melakukan perjuangan mereka, untuk kepentingan orang banyak, terutama kepentingan rakyat miskin yang tertindas dan sengsara.

Karena itu, walaupun PKI dilarang secara resmi, namun bisalah  diduga bahwa isi dan tujuan perjuangan PKI sekarang ini dilakukan atau diteruskan oleh berbagai macam golongan dan kalangan yang mempunyai haluan dan tujuan perjuangan yang sama, yaitu masyarakat adil dan makmur, yang menurut Bung Karno disebut sebagai masyarakat sosialis.

Ajaran marxisme/komunisme tidak bisa lagi di-taboo-kan

Jadi, jelaslah sekarang ini bahwa berbagai informasi mengenai PKI, atau ajaran-ajaran komunisme atau marxisme, tidak bisa lagi di-taboo-kan, seperti jamannya Suharto. Sebab, melalui Internet dan membuka Google, maka setiap orang dapat memperoleh bacaan yang beraneka ragam tentang itu semua. Untuk memberi gambaran tentang besarnya dan luasnya kemungkinan untuk mendapatkannya, adalah sebagai berikut :

Kalau dibuka Google bahasa Indonesia dan ditik kata kunci  « Komunisme sebagai ideologi » maka akan tersedia 115 000 halaman. Sedangkan kalau dengan kata kunci «Komunisme di indonesia »   maka akan tersedia   575 000  halaman

Contoh-contoh lainnya adalah :

Komunisme masih hidup     --   1.230 000  halaman

Komunisme di dunia      --      468 000  halaman

Mempelajari komunisme      --    126  000  halaman

Sejarah komunisme di indonesia     --     281 000  halaman

Bahan bacaan tentang komunisme yang tersedia dalam Google tidak seluruhnya bersifat serba positif, melainkan juga bercampur dengan yang bersikap negatif, mencela atau kritis.

Sedangkan kalau dibuka Google dalam bahasa Inggris maka akan tersedia macam-macam bacaan yang jumlahnya luar biasa banyaknya, sehingga sulit untuk diperkirakan berapakah waktu yang diperlukan untuk bisa membaca seluruhnya.

Sebab, kalau ditik kata kunci : Communism today  -- maka akan tersedia 5,240, 000 halaman atau bahan bacaan ( !!!).  Kalau dengan kata kunci : Marxism today – akan tersedia 485.000 halaman.

Alat perjuangan sekarang dan di masa depan.

Mengingat itu semua, nyatalah dengan jelas bahwa larangan terhadap beredarnya ajaran komunisme atau marxisme di Indonesia adalah nonsense belaka sekarang ini. Banyak orang dapat memperolehnya melalui berbagai jalan dan cara. Di antaranya adalah melalui Internet, setiap waktu, sebanyak-banyaknya, dengan leluasa dan juga aman.,

Dan dengan perkembangan situasi di Indonesia, yang sedang dibanjiri oleh berbagai persoalan parah yang terus-menerus menyengsarakan banyak orang, dan karenanya memerlukan perjuangan revolusioner untuk mendatangkan perubahan besar-besaran dan fundamental, maka jiwa ajaran komunisme atau marxisme akan makin mendapat tempat di fikiran dan hati banyak orang.

Jiwa revolusioner yang dikandung ajaran komunisme atau marxisme adalah alat perjuangan bagi banyak kalangan di Indonesia, yang dengan sungguh-sungguh bertekad untuk mengadakan perubahan  besar-besaran dan fundamental, baik sekarang maupun  di masa depan..
