---
layout: post
title: Kasus Brigjen Kusmayadi aib besar TNI-AD
date: 2006-07-17
---

Ditemukannya 103 pucuk senapan, 42 pistol, 6 granat, dan hampir 30 ribu peluru di rumah mendiang Brigadir Jenderal Koesmayadi (Wakil Asisten Logistik TNI-AD) beberapa waktu yang lalu adalah masalah sangat serius yang perlu diusut secara tuntas dan juga secara transparan. Peristiwa ini menunjukkan bahwa banyak sekali praktek-praktek yang dilakukan oleh kalangan militer (khususnya TNI-AD) perlu diawasi atau diselidiki, dan berbagai pelanggaran atau penyelewengan harus ditindak dengan tegas.

Langkah yang sudah dimulai oleh Panglima TNI Marsekal Djoko Suyanto dengan membuka kepada publik kasus penimbunan senjata perlu didorong terus atau dituntut terus oleh masyarakat luas untuk ditindaklanjuti dengan langkah-langkah yang lebih tegas dan benar-benar tidak pandang bulu. Sebab, sudah lebih dari 62 orang (di antaranya 14 orang sipil) telah dimintai keterangan tentang penimbunan senjata dan perlengkapan militer ini.

Berdasarkan hasil keterangan dari 62 orang tersebut, diketahui terjadi 29 kali pemasukan senjata dan amunisi. Enam kali di antaranya dilakukan dari Singapura selama periode Mei 2003 sampai Maret 2006 berupa senjata non-standar militer berdasarkan surat permintaan yang ditandatangani Brigjen Koesmayadi selaku Waaslog Kasad sebanyak 60 pucuk.

Pemasukan lainnya adalah 23 kali senjata standar militer dari berbagai negara di luar Singapura selama periode Maret 2001 sampai Oktober 2004. Pemasukan itu didasarkan pada surat pemberitahuan impor barang dari Mabes TNI melalui Bandara Internasional Soekarno-Hatta, Pelabuhan Tanjung Priok Jakarta dan Tanjung Perak Surabaya



Mengandung banyak pertanyaan

Tindakan-tindakan tegas perlu sekali dilakukan untuk membongkar penimbunan senjata oleh alm brigjen Kusmayadi ini karena kasus ini mengandung banyak sekali pertanyaan dan penuh dengan hal-hal yang “aneh” serta meragukan. Apakah benar bahwa penimbunan senjata yang sebanyak itu bermotif hanya sebagai koleksi atau hobby saja? Juga, apakah benar bahwa senjata-senjata itu untuk “sport” saja? Apakah ada motif-motif atau tujuan “bisnis”? Mengapa ada peluru sampai hampir 30 000 butir dan 9 geranat ? Penimbunan senjata itu untuk siapa atau dengan tujuan apa ? Ia berbuat begitu itu atas perintah siapa?

Adalah hal yang aneh bahwa praktek pemasukan senjata atau perlengkapan militer yang sudah dilakukan oleh brigjen Kusmayadi sejak tahun 2001 itu dibiarkan terus tanpa kontrol oleh atasannya. Ataukah tindakannya itu justru memang sepengetahuan atau persetujuan fihak atasannya? Atau, betulkah bahwa peristiwa itu adalah akibat perbuatannya seorang diri? Apakah penimbunan senjata ini ada hubungannya dengan beredarnya senjata gelap di berbagai daerah ( antara lain : Poso, Maluku, Aceh) dan di kalangan kelompok-kelompok Islam tertentu?

Dari banyaknya tempat di mana ditemukan senjata-senjata itu besar sekali adanya kemungkinan bahwa brigjen Kusmayadi tidak bekerja sendirian.



Nama buruk TNI-AD

Kasus penimbunan senjata oleh brigjen Kusmayadi perlu sekali diselesaikan secara tegas dan tuntas, mengingat bahwa selama ini sudah terlalu banyak kasus-kasus yang membikin TNI-AD dicurigai atau diduga sebagai pelakunya, tetapi kebanyakan tidak bisa terbongkar secara jelas. Boleh dikatakan bahwa karena banyaknya pelanggaran, penyelewengan, atau kejahatan yang dilakukan oleh oknum-oknum TNI-AD, maka nama TNI-AD sudah busuk sejak lama.

Di antara kasus-kasus yang membikin jatuhnya citra TNI-AD jatuh di mata banyak orang adalah, antara lain, pembantaian besar-besaran orang-orang tidak bersalah dalam tahun 65, pembunuhan banyak orang di Timor Timur, tragedi Tanjung Priok tahun 1984, pembantaian orang Papua dan Aceh, tragedi Talangsari dan Haur Koneng, pembunuhan di Sampang, tragedi 27 Juli 1996, penculikan mahasiswa 1996-1998, penembakan mahasiswa Trisakti 12 Mei 1998, penembakan mahasiswa di Semanggi November 1998, pembunuhan Marsinah, peracunan pejuang HAM Munir dalam pesawat terbang.

Karenanya, kalau kasus penimbunan senjata kali ini pun juga akan tidak bisa terbongkar secara tuntas dan transparan, maka kecurigaan banyak orang terhadap praktek-praktek tidak sehat di kalangan TNI-AD akan makin besar dan menumpuk.

Adalah salah sama sekali kalau dengan maksud menjaga nama baik TNI-AD maka kejahatan atau penyelewengan sekitar penimbunan senjata ini ditutup-tutupi atau direkayasa, oleh fihak yang mana pun. Dan juga adalah keliru sama sekali kalau ada kalangan di TNI-AD yang dengan dalih “menjaga rahasia negara” maka berbagai borok-borok yang selama ini menghinggapi tubuh aparat negara ini tidak boleh disentuh.

---

Tempo Interaktif, 26 September 2006

TNI Usut Pembocor Dokumen Rahasia
TEMPO Interaktif, Jakarta:Kepala Staf TNI Angkatan Darat Jenderal Djoko Santoso membuktikan ancamannya mengusut orang yang diduga membocorkan gepokan dokumen milik TNI. Kini pembocor dokumen rahasia milik TNI itu sedang diusut dan diperiksa Pusat Polisi Militer TNI Angkatan Darat.

"Ya, ya, sudah diperiksa," ujar Komandan Pusat Polisi Militer TNI Angkatan Darat Mayor Jenderal Hendardji Soepandji di sela-sela rapat kerja dengan Komisi Pertahanan DPR kemarin.

Informasi yang dihimpun Tempo menyebutkan pembocor dokumen itu seorang anggota TNI. Anggota TNI ini membocorkan dokumen, salah satunya dokumen itu berupa fotokopi perjanjian kerja sama jual-beli senjata antara asisten logistik Angkatan Darat dan CV Adian Nalombok senilai US$ 209.200. Dokumen ini beberapa waktu lalu sempat bocor di kalangan anggota Dewan.

Dokumen lain yang sempat bocor di antaranya dugaan penyimpangan di Akademi Militer, bisnis mobil mewah, dan ruilslag tanah di Surabaya.

Bocornya dokumen rahasia ini mengemuka saat rapat kerja antara Menteri Pertahanan, Panglima TNI, dan para kepala staf pada 10 Juli lalu. Saat itu anggota Komisi Pertahanan DPR, Ade Daud Nasution, menanyakan soal itu kepada Kepala Staf Angkatan Darat Jenderal Djoko Santoso.

Ditanyai seperti itu, Djoko mengaku tidak mengetahui surat itu. Mestinya, kata Djoko, surat seperti itu tidak boleh bocor ke luar lingkungan TNI Angkatan Darat. "Itu dokumen rahasia negara. Pasti ada anak buah saya yang membocorkannya. Akan saya usut," ujarnya dengan nada tinggi.

Sayang, saat didesak siapa anggota dan pangkatnya, Hendardji enggan menyebutkannya. Saat ditanyai apakah orang itu berpangkat perwira bintang satu, Hendardji hanya menjawab, "Bukan saya yang sebutkan itu."
Hendardji hanya tertawa kecil saat ditanyai apakah orang itu masih aktif. "Yang jelas, ketika perbuatan itu dilakukan, (ia) masih aktif," katanya.

Menurut Hendardji, persoalan utama yang tertulis dalam dokumen itu juga sudah diusut, tidak hanya masalah pembocoran dokumen tersebut. Orang tersebut, kata Hendardji, baru beberapa waktu lalu ditetapkan untuk diperiksa. "Baru saja," katanya.

Anggota TNI itu, kata Hendardji telah melanggar pasal perbuatan pidana. Namun, dia enggan menyebutkan pasal pidana tersebut.
Anggota Komisi Pertahanan, Yuddy Chrisnandi, menghargai pengusutan itu. Menurut dia, pengusutan itu merupakan disiplin yang harus ditegakkan TNI. dian yuliastuti

* * *

SUARA PEMBARUAN, 25 September 2006

DPR Pesimistis Kasus Koesmayadi Dituntaskan


[JAKARTA] Sejumlah kalangan di Komisi I DPR pesimistis penyelidikan atas penemuan 185 pucuk senjata di rumah almarhum mantan Wakil Asisten Logistik Kepala Staf Angkatan Darat (Waaslog Kasad) Brigjen Koesmayadi bisa diungkap tuntas.
"Bila kasus Koesmayadi ini diungkap akan memalukan TNI sendiri," kata anggota Komisi I DPR Permadi dari Fraksi PDI Perjuangan di sela-sela rapat kerja komisinya dengan Menteri Pertahanan Juwono Sudarsono dan Panglima TNI Marsekal Djoko Suyanto, di Gedung MPR/DPR, Jakarta, Senin (25/9) pagi.


Permadi mengakui, dia tidak yakin kasus Koesmayadi bisa terungkap hingga tuntas.
Begitu pula AS Hikam, anggota Komisi I dari Fraksi Partai Kebangkitan Bangsa mengemukakan pendapat serupa. "Saya kira akan selesai begitu saja," tukasnya.
Wacana pembentukan panitia kerja (panja) sendiri, tampaknya tidak akan berlanjut. "DPR tidak akan ada keberanian untuk serius," kata Hikam.


Belum Ada Perkembangan


Sementara itu, penjelasan Menhan dalam rapat kerja dengan Komisi I DPR, juga belum ada hal baru sebagai perkembangan hasil penyelidikan.
"Masih seperti yang dipaparkan pada DPR sebulan lalu," tutur Permadi.
Permadi menilai, jawaban Juwono mengenai kasus penemuan senjata di rumah Koesmayadi tidak jelas. "Kita tidak tahu, misalnya, apakah Kasad yang lama sudah diperiksa," ujarnya.


Seperti penjelasan kepada sejumlah anggota Komisi I DPR, di Mabes TNI, 10 Agustus lalu, Juwono menyebutkan, hingga kini ada tujuh tersangka, termasuk almarhum Koesmayadi, untuk masalah pengadaan senjata. Untuk penyimpanan senja- ta, ada empat tersangka termasuk almarhum, kemudian pemindahan senjata ada lima tersangka.


Juwono melalui jawaban tertulis dalam rapat kerja tersebut menyebutkan, dari total 655 senjata yang di- impor oleh Koesmayadi, hanya 43 pucuk senjata api nonstandar militer yang diimpor atas inisiatif Koesmayadi, dan tidak dilengkapi oleh dokumen. Sedangkan impor senjata lainnya diketahui oleh pimpinan TNI, serta dilengkapi dengan dokumen SP2.


Sebelumnya, Komisi I DPR ingin mengetahui apakah Koesmayadi menyimpan ratusan senjata api dan ribuan amunisi atas kehendak sendiri atau perintah atasan. Ini menimbulkan tanda tanya karena begitu Koesmayadi meninggal dunia, rumahnya langsung digerebek.


"Karena bukan hanya senjatanya yang diambil, tetapi sampai diary-nya segala. Apakah itu diarahkan ke satu kelompok tertentu yang dianggap mengganggu negara atau pemerintah. Itu harus ada klarifikasi dari Panglima atau Angkatan Darat. Begitu juga soal pemberi order pengusutan itu. Kalau Sudi Silalahi yang memberi order, itu juga harus ditelusuri kebenarannya," kata anggota Komisi I DPR Effendi Muara Sakti Simbolon dari Fraksi PDI Perjuangan, baru-baru ini .
Karena itu, lanjutnya, Komisi I DPR perlu memanggil Panglima TNI Marsekal Djoko Suyanto guna menjelaskan kasus ini. Karena kalau fakta bahwa Koesmayadi menyimpan senjata, itu sudah pasti diketahui Markas Besar TNI AD sejak lama, bahkan sejak dia berpangkat kapten.
"Almarhum berperan sebagai semacam 'agen' petinggi atau TNI sendiri yang berfungsi dalam rangka memasukkan senjata. Dan peran ini memang lumrah di seluruh negara bahwa dalam pemasukan senjata ada jalur formal dan jalur tidak formal," ungkapnya.


Soal Panser


Hingga berita ini diturunkan, Juwono masih membacakan jawaban tertulisnya atas pertanyaan anggota Komisi I, tentang pengiriman 850 prajurit TNI sebagai pasukan perdamaian di Lebanon. Beberapa anggota Komisi I juga menyebut masih memiliki banyak pertanyaan, terutama seputar rencana pembelian panser.


"Tidak ada alasan untuk tidak tender," ujar Hikam. Terutama dengan diundurnya jadwal pemberangkatan pasukan, menurut dia, masih ada waktu bagi TNI untuk mencari opsi lain pembelian panser, baik terkait dengan harga maupun kualitas yang lebih baik.
Untuk pembelian panser, menurut dia, tanpa adanya program pemberangkatan pasukan perdamaian ke Lebanon, pengadaan kendaraan tempur itu memang sudah diprogramkan. Namun ada yang harus diklarifikasi, di antaranya mengenai kualitas panser tersebut.
"Saya butuh jaminan dari Pemerintah Prancis mengenai kualifikasi dan harga dari panser tersebut karena itu buatan tahun 2001, kenapa tidak tahun 2006," katanya. [B-14]
* * *

Tempo Interaktif, 12 Agustus 2006

Kasus Koesmayadi Perlu Ditangani Peradilan Sipil

TEMPO Interaktif, Jakarta:Kasus penimbunan ratusan senjata dan amunisi di kediaman Brigadir Jenderal Koesmayadi perlu ditangani oleh peradilan sipil. Karena, menurut Frans Hendra Winarta dari Komisi Hukum Nasional, melalui peradilan sipil yang bersifat terbuka, proses hukum kasus ini bisa lebih transparan.

Selama ini ada kesan seolah anggota militer tidak dapat tersentuh oleh hukum sipil. Oleh sebab itu, sudah waktunya jika anggota militer melakukan pelanggaran pidana di luar kondisi peperangan, yang berlaku adalah hukum sipil.

“Tidak ada lagi anggota militer yang diadili oleh pengadilan militer dalam keadaan damai,” kata Frans usai menjadi pembicara dalam diskusi interaktif “Obrolan Sabtu” di Cafe Marios Place, Sabtu (12/8).

Jika negara dalam kondisi perang, dia melanjutkan, sesuai Konvensi Jenewa harus hukum militer yang memprosesnya jika seorang tentara melanggar. “Kalau militer melakukan penyelundupan, adili dengan pasal penyelundupan oleh hakim sipil, jangan hakim militer,” ujarnya.

Frans khawatir dengan hasil penyelidikan Puspom TNI terhadap kasus Koesmayadi masih ada semangat melindungi korps sehingga ada hal yang ditutup-tutupi.

Senada dengan Frans, Johnson Panjaitan dari Perhimpunan Bantuan Hukum Indonesia mengatakan penyidikan kasus Koesmayadi ini harus terbuka bagi publik. Hasil penyelidikan kasus yang telah diumumkan, menurutnya tidak cukup terbuka. Oleh sebab itu Panglima TNI harus mengumumkan bagaimana sebenarnya posisi kasus penemuan senjata tersebut. “Hanya kasus administrasi atau ada jaringan penjualan senjata,” katanya. Dimas Adityo
* * *

Tempo Interaktif, 17 Juli 2006

Puspom TNI Periksa 95 Orang

Terkait Senjata Koesmayadi



TEMPO Interaktif, Jakarta:Kepala Staf TNI Angkatan Darat, Jenderal TNI Djoko Santoso, mengatakan Pusat Polisi Militer TNI hingga saat ini telah memeriksa 95 orang terkait kasus penimbunan ratusan senjata di rumah (almarhum) Brigjen Koesmayadi, Wakil Asisten Logistik KSAD.

"Masalah senjata ini sedang diadakan pemeriksaan lebih lanjut oleh Puspom TNI," kata Djoko, usai mengikuti rapat di kantor Menteri Koordinasi Bidang Politik Hukum dan Keamanan, hari ini. Namun, Djoko tidak menyebutkan nama di antara yang telah diperiksa tersebut.

Ditanya kapan mantan KSAD Jenderal (Purn.) Ryamizard Ryacudu mendapat giliran diperiksa, Djoko menolak menjawab. "Saya tidak mau menyebut spesifik nama, tunggu kelengkapan pemeriksaannya, kan kita dikasih waktu sampai tanggal 10 (Agustus)," kata dia.

Senada dengan Djoko Santoso, Panglima TNI Marsekal TNI Djoko Suyanto, menolak ketika ditanya apakah mantan KSAD Ryamizard Ryacudu akan diperiksa terkait kasus penimbunan senjata tersebut. "Saya tidak akan menyebut nama, tapi siapa yang diperlukan Puspom untuk dimintai keterangan akan dipanggil," kata Panglima.

Ditanya apakah terhadap Ryamizard sudah dilayangkan panggilan untuk dimintai keterangan, "Puspom yang tahu, saya kan tidak satu per satu tahu siapa orangnya (yang diperiksa)," kata Panglima.

* * *


Editorial Media Indonesia, 14 Juli 2006

Komnas HAM, TNI dan Orang Hilang


KOMISI Nasional Hak Asasi Manusia (Komnas HAM) telah melayangkan surat kepada Ketua Pengadilan Negeri Jakarta Pusat Cicut Sutiarso pada Senin (10/7). Surat yang diantar Wakil Ketua Komnas HAM Zoemrotin itu berisi permohonan pemanggilan paksa atas sejumlah anggota TNI yang diduga mengetahui kasus penghilangan orang secara paksa pada 1997-1998.Sedikitnya 14 orang yang hilang bak ditelan bumi pada periode itu. Mereka aktivis muda yang menentang kekuasaan represif ketika itu.Ada enam orang anggota dan purnawirawan TNI yang dimintakan pemanggilan paksa, termasuk Sekjen Dephan Letjen Sjafrie Sjamsuddin yang ketika kejadian menjabat Pangdam Jaya.

Mereka sudah dua kali dipanggil dalam kasus pelanggaran berat HAM itu. Sebanyak 23 anggota dan purnawirawan TNI lainnya juga tidak mau memenuhi panggilan pertama Komnas HAM. Hanya seorang yang bersedia memenuhi panggilan tersebut.Penolakan itu resmi disampaikan Badan Pembinaan Hukum TNI. Alasannya, tim penyelidik Komnas HAM dinilai tidak memiliki kewenangan untuk memanggil para anggota TNI karena kejadian yang dituduhkan tersebut berlangsung sebelum UU 39/1999 tentang HAM diundangkan.Sjafrie juga memastikan tidak akan memenuhi panggilan paksa itu.

Sebab, kata dia, dirinya hanyalah pejabat yang melaksanakan tugas operasional dari institusi. Kita percaya, tugas operasional institusi itu suci. Kesalahan, jika ada, sepenuhnya berada di pundak pelaksana.Memenuhi panggilan Komnas HAM jelas merupakan peluang emas bagi TNI untuk 'membersihkan' diri. Terlalu sayang kesempatan itu dilewatkan begitu saja. Apalagi, Komnas HAM sudah menyimpulkan pelaku penghilangan paksa berasal dari kelompok terorganisasi. Anggota dan purnawirawan TNI yang dipanggil mestinya berkewajiban menjelaskan kepada Komnas HAM bahwa kelompok terorganisasi itu bukan TNI. Tunjukkan bahwa tugas mulia institusi telah dilaksanakan tanpa noda.Kita justru memberikan penghargaan yang tinggi kepada Panglima TNI Marsekal Djoko Suyanto yang membuka kepada publik kasus penimbunan senjata di kediaman Wakil Asisten Logistik KSAD mendiang Brigjen Koesmayadi.

Pengungkapan kasus itu, kata Panglima, untuk memetakan TNI dalam aturan dan tatanan yang benar. Panglima ingin menunjukkan kepada publik bahwa TNI memanglimakan hukum. Itulah roh reformasi yang dicanangkan TNI.Pemeriksaan oleh Komnas HAM terkait kasus penghilangan orang secara paksa sudah seharusnya juga diletakkan pada perspektif untuk memetakan TNI dalam aturan dan tatanan yang benar. Sekaranglah saatnya menjelaskan sejelas-jelasnya kepada Komnas HAM tentang kasus orang-orang hilang agar tidak ada lagi prasangka di kemudian hari. Bila tidak dijelaskan dan diselesaikan sekarang, Komnas HAM akan selalu menempatkan kasus orang hilang sebagai pending matters yang setiap kali diungkapkan, mau tidak mau akan mempertanyakan kesungguhan TNI menjelaskan masalah ini.
* * *


Republika, 10 Juli 2006

Panglima TNI: 62 Orang Dimintai Keterangan Kasus Senjata Koesmayadi

Panglima TNI Marsekal TNI Djoko Suyanto mengatakan, hingga akhir pekan lalu, sudah 62 orang dimintai keterangan terkait dengan kasus dugaan kepemilikan senjata ilegal Wakil Asisten Logistik Kasad, Brigjen TNI Koesmayadi yang meninggal 25 Juni lalu.

"Dari 62 orang tersebut, 14 di antaranya adalah warga sipil yang diduga
terkait dan mengetahui kepemilikan senjata ilegal Koesmayadi," katanya dalam rapat dengar pendapat dengan Komisi I DPR-RI di Jakarta, Senin.

Berdasarkan hasil keterangan dari 62 orang tersebut, diketahui terjadi 29
kali pemasukan senjata dan amunisi. Enam kali di antaranya dilakukan dari
Singapura selama periode Mei 2003 sampai Maret 2006 berupa senjata
non-standar militer berdasarkan surat permintaan yang ditandatangani Brigjen Koesmayadi selaku Waaslog Kasad sebanyak 60 pucuk.

Pemasukan lainnya adalah 23 kali senjata standar militer dari berbagai
negara di luar Singapura selama periode Maret 2001 sampai Oktober 2004.
Pemasukan itu didasarkan pada surat pemberitahuan impor barang dari Mabes
TNI melalui Bandara Internasional Soekarno-Hatta, Pelabuhan Tanjung Priok
Jakarta dan Tanjung Perak Surabaya, katanya.

Pengadaan tersebut antara lain diperuntukkan bagi Pleton Intai Tempur
(Tontaipur) Kostrad sebanyak 661 pucuk. Dari jumlah itu, terdapat senjata
laras panjang 623 pucuk, dan senjata laras pendek 38 pucuk, 16 granat asap,
amunisi 9.030 butir, disamping tiga boks lainnya namun jumlahnya tidak
diketahui, katanya.

Djoko mengatakan, sampai saat ini, secara keseluruhan, rekapitulasi senjata
api dan amunisi serta perlengkapan militer lain yang disita Puspom AD adalah senjata api 185 pucuk yang terdiri atas senjata api laras panjang Licin delapan pucuk, senjata laras panjang beralur 122 pucuk (dua di antaranya senjata mainan), dan senjata laras pendek 55 pucuk (satu di antaranya mainan).

Seterusnya, amunisi 28.985 butir, terdiri atas 28.976 butir peluru dan
sembilan butir granat. Selain itu disita pula perlengkapan militer lainnya,
seperti teropong berbagai jenis, magazin pendek, dan magazin panjang,
katanya.

Senjata-senjata, amunisi, serta perlengkapan militer lainnya itu ditemukan
tim investigasi TNI AD di berbagai lokasi, seperti rumah almarhum di Jalan
Pangandaran V No.15, Ancol, Jakarta Utara (140 pucuk, tiga di antaranya
senjata mainan), dan Kompleks Perwira Tinggi Gatot Subroto, Blok E No.16,
Jakarta Selatan (empat pucuk).

Selain itu, berbagai senjata itu pun ditemukan pula di Kompleks Perumahan
Raflesia Blok B-I No.15, Cilengsi Bogor (satu pucuk), di ruang kerja Waaslog Kasad di Mabes TNI AD Jalan Merdeka Utara, Jakarta (tiga pucuk), penyerahan dari Kolonel Infantri Tedy Lasmana (32 pucuk merupakan senjata titipan almarhum), serta penyerahan dari Kopassus sebanyak lima pucuk yang merupakan senjata yang digunakan pada saat operasi mendukung darurat militer di Aceh.

Terkait dengan motivasi dan latar belakang, ia mengatakan, sampai saat ini,
tim memiliki perkiraan sementara bahwa senjata-senjata tersebut antara lain
merupakan koleksi karena ada di antara temuan itu merupakan senjata-senjata
model lama semasa Perang Dunia II.

Kepentingan lainnya masih didalami oleh tim investigasi namun anggota tim
memperkirakan bahwa senjata-senjata itu adalah senjata-senjata "sport".

Tim juga memprediksi bahwa senjata-senjata tersebut digunakan dalam rangka
pembangunan satuan TNI-AD yang pada waktu itu memang diperkukan untuk
mendukung darurat sipil di Aceh terkait dengan embargo militer AS di mana
saat itu tidak seluruh senjata langsung diserahkan ke TNI-AD, katanya.

Mengenai pelanggaran hukum dan penyimpangan yang terdapat dalam kasus
tersebut, ia mengatakan, untuk sementara, tim mengkategorikannya ke dalam
dua aspek, yaitu aspek manajemen di mana tidak dipatuhinya aturan tentang
ketentuan-ketentuan penerimaan pengadaan pertanggungjawaban pengurusan
administrasi senjata api dan amunisi serta peralatan militer.

Aspek lainnya adalah aspek hukum di mana saat ini tim Puspom AD baru masuk
pada tahap penyelidikan, belum pada tahap penyidikan dan evaluasi, katanya.

Perihal kepemilikan berbagai jenis senjata, amunisi dan perlengkapan militer lainnya itu terkuat setelah Koesmayadi meninggal dunia pada Minggu (25/6) akibat serangan jantung.

Sesuai prosedur baku internal TNI, maka dilakukan penarikan kembali atas
barang inventaris bagi anggota TNI yang telah pensiun atau meninggal
dunia.*****[gospol]

* * *



Panglima TNI telah melontarkan dugaan mendiang Koesmayadi tak mungkin
sendirian dalam kegiatan gelap di bidang pengadaan perangkat militer ini.
Maka, anggota lain komplotan ini tentu harus ditangkap dan dihukum berat
sebagai bagian dari terapi kejut untuk memberantas peredaran senjata api
gelap di Indonesia.

++++++++++++++++++++++++++

TEMPO, Edisi 3 - 9 Juli 2006

Warisan Maut Jenderal Koes

Sebuah berita mengejutkan muncul pekan lalu. 103 pucuk senapan, 42 pistol, 6
granat, dan hampir 30 ribu peluru ditemukan di rumah mendiang Brigadir
Jenderal Koesmayadi. Timbunan perangkat perang yang cukup untuk melengkapi
persenjataan dua kompi pasukan itu segera diamankan, dan pimpinan TNI pun
meme-rintahkan satuan polisi militer melakukan penyelidikan. Tak kurang dari
Kepala Staf Angkatan Darat, Jenderal Djoko Santoso, langsung memberikan
konferensi pers me-ngenai temuan di rumah Wakil Asisten Logistik TNI-AD yang
baru saja meninggal itu. Presiden Susilo Bambang Yudhoyono pun merasa perlu
memanggil Panglima TNI untuk membahas soal ini.

Maklum, ini memang temuan serius. Soalnya, berba-gai konflik bersenjata,
seperti yang belum lama terjadi di Aceh dan Maluku maupun yang masih
berlangsung seperti di Poso dan Papua, sering memunculkan pertanyaan tak
terjawab: dari mana senjata standar militer para pelaku kriminal itu
diperoleh? Termasuk juga ketika, dalam pekan yang sama, seorang penjual
senjata otomatis Uzi di Jakarta Utara ditangkap polisi. Dan sebelumnya,
ketika polisi melaporkan telah menyita senapan serupa dari kelompok Jamaah
Islamiyah.

Temuan di rumah Brigjen Koesmayadi mudah-mudahan akan menjadi titik terang
dalam upaya melacak asal-usul peredaran senjata militer gelap yang telah
menyebabkan berbagai aksi kekerasan di republik ini, bahkan juga ke luar
negeri. Jangan lupa, Mari Alkatiri terpaksa mundur dari jabatan Perdana
Menteri Timor Leste gara-gara dituding mempersenjatai milisi sipil partainya
secara rahasia dan mengimpor ribuan senjata, termasuk senapan serbu dan
pistol buatan PT Pindad, tanpa melalui transaksi resmi. Pemimpin negeri
tetangga itu bahkan kini berstatus tersangka pelaku tindak pidana.

Apa boleh buat, menindak keras para pengedar senjata gelap, siapa pun dia,
memang wajib dilakukan untuk memberi rasa aman kepada masyarakat. Ini bukan
hanya berlaku di Indonesia. Buktinya, persoalan ini bahkan sedang
dibicarakan di markas Perserikatan Bangsa-Bangsa di New York dalam
konferensi yang melibatkan sekitar 2.000 pakar dan pejabat dari
negara-negara anggota PBB.

Dunia internasional memang sedang merasa prihatin karena senjata api ringan
diperkirakan menjadi penyebab tewasnya sekitar seribu penduduk dunia setiap
hari. Perdagangan gelap alat membunuh ini dituding sebagai biang keladi 90
persen korban mati di kawasan konflik. "Senjata api ringan menyuburkan
konflik dan mendukung kegiatan kelompok-kelompok yang terlibat dalam
kejahatan terorganisasi serta perdagangan narkotik dan manusia," kata Prasad
Kariya-wasam, Duta Besar Sri Lanka untuk PBB yang menjadi ketua konferensi
PBB itu.

Tingginya keuntungan yang diraih dari bisnis gelap penjualan senjata api
ringan ini menyebabkan banyak petugas penegak hukum tergiur dan terlibat.
Transaksi ilegal yang diperkirakan bernilai sekitar Rp 10 triliun setiap
tahun ini telah menggurita ke seluruh penjuru dunia dan memicu berbagai
konflik berdarah yang sulit dihentikan. Itu sebabnya, sejak tahun 2001,
lebih dari 50 negara telah memperberat ancaman hukuman terhadap para
pengedar senjata api gelap di wilayah masing-masing.

Di Indonesia, ancaman hukuman terhadap pemilik atau pengedar senjata api
sebenarnya sudah cukup berat: penjara 20 tahun. Setidaknya itu yang tertera
pada Pasal 1 Undang-Undang No. 12 Tahun 1951. Hanya, sayangnya, selama ini
penegakannya di dunia nyata tidak efektif, terutama bila menyangkut kalangan
yang berkuasa.

Lemahnya penerapan hukum ini telah menyebabkan maraknya peredaran senjata
api gelap di Indonesia. Terbukti data yang tercatat di Markas Besar Polisi
RI menyebutkan sekitar 12 ribu perangkat bunuh haram ini, baik yang buatan
pabrik maupun rakitan, telah disita dari puluhan tersangka. Bila diasumsikan
bahwa yang beredar biasanya lebih banyak ketimbang yang disita polisi, tentu
ini sebuah keadaan yang mencemaskan dan harus secepatnya ditangani.

Panglima TNI telah melontarkan dugaan mendiang Koesmayadi tak mungkin
sendirian dalam kegiatan gelap di bidang pengadaan perangkat militer ini.
Maka, anggota lain komplotan ini tentu harus ditangkap dan dihukum berat
sebagai bagian dari terapi kejut untuk memberantas peredaran senjata api
gelap di Indonesia. Banyak cara dapat dilakukan untuk menelusuri siapa saja
yang terlibat. Salah satunya adalah dengan mencari tahu bagaimana seorang
perwira tinggi bintang satu dapat memperoleh sebuah rumah di kawasan mahal
dengan nilai melebihi seluruh gajinya sejak ia masuk dinas militer.

Kita berharap komplotan di belakang kasus Brigjen Koesmayadi-jika ada-akan
terbongkar tuntas, diadili, dan dihukum. Dengan demikian, sebuah titik balik
dalam upaya pemberantasan peredaran senjata api gelap dapat diraih dan
berbagai konflik berdarah di berbagai pen-juru negeri ini pun menyurut.
Seperti kata almarhum Munir, "kita telah lelah dengan kekerasan," dan
berharap damai segera hadir di Tanah Air. ***



* * *



Riau Pos, 8 Juli 2006

Jika Bukan Kudeta, Lalu Apa?


AKAN ada kudeta? Kita terperanjat mendengar salah satu spekulasi yang beredar setelah penemuan 145 pucuk senjata api di rumah almarhum Brigjen Koesmayadi pada Ahad (25/6/2006) silam. Tapi, jika melihat struktur dan konstruksi politik dewasa ini nampaknya cuma pikiran yang paranoid. Di era reformasi dan supremasi sipil ini, apalagi dengan UU Pertahanan, TNI telah “kembali ke barak” dan jauh dari panggung politik. TNI juga berada di bawah Departemen Pertahanan yang selalu dikontrol DPR. Jika pun ada segelintir kecil personel militer, baik yang masih aktif dan sudah purnawirawan yang tak puas dengan keadaan, toh iklim demokratisasi memungkinkan mereka untuk mengungkapkan aspirasi, atau bahkan melancarkan kritik. Namun, secara politik relatif mereka tak lagi “bergigi.” Bahkan, tidak ada sosok militer yang berpotensi untuk memimpin sebuah “kudeta” seperti yang pernah dilakukan Idi Amin dan Moamar Kadafi di Afrika.

Setelah era AH Nasution, M Yusuf dan Benny Moerdany, rasanya tidak ada jenderal yang mempunyai visi besar dan berkharisma di kalangan prajurit. Mungkin, inilah iklim politik di masa rezim Orde Baru ysng tak memungkinnya tampilnya “dua matahari” di bawah langit yang sama. Mungkin pula karena jarak usia Soeharto dengan banyak jenderal setelah era Benny, relatif sangat jauh. Bahkan banyak jenderal kita yang justru berkarir bagus karena berada di lingkungan Presiden Soerharto. Beda dengan para perwira seperti Hasan Saleh, Maludin Simbolon yang pernah angkat senjata bersama PRRI dan DI-TII, serta beberapa perwira lain dalam kasus Permesta dan RMS.

Mungkin ada analisis dengan kemungkinan kecil, bahwa militer bisa saja memainkan “peran politik”, entah berupa “kudeta” atau mungkin sejenis pemulihan keadaan dalam bahasa yang lebih sopan. Hal itu terjadi bila pemerintahan sipil gagal dalam menjawab aspirasi dan kehendak rakyat. Logikanya, kira-kira, “peran poilitik militer” itu adalah atas permintaan rakyat. Tapi analisis ini pun tak realistis. Karena kudeta, seperti halnya revolusi, selalu membutuhkan syarat-syarat subjektif dan objektif. Mungkin, bayang-bayang syarat objektif, seperti beberapa kebijakan pemerintah yang mengecewakan, katakanlah langkah untuk mengatasi akibat kenaikan harga BBM. Bisa ditambah lagi dengan masalah kemiskinan, lapangan kerja yang sempit, dan pengangguran yang semakin merajalela. Namun, syarat subjektif, yakni faktor internal, katakanlah siapa figur yang mempunyai kapasitas mengambil alih keadaan di kalangan militer, nampaknya tidak ada, dan semoga tidak pernah ada.

Misalkan ada yang mencoba-coba, tapi harus diingat bahwa sebuah junta militer haruslah punya basis dukungan yang luas, baik di kalangan tentara maupun rakyat. Harus juga didukung oleh kaum kapitalis yang punya duit. Memang militer bisa bangkit bila ada kekuatan ekstrim yang hendak berbuat makar, misalnya atas nama agama tentu atau ide komunisme. Tapi faktor ini sangat-sangat tipis, bahkan tidak ada bila mengingat gerakan sejenis di masa lalu selalu gagal, dan terbukti komunis misalnya malah tak lagi laku di Cina dan Rusia. Jangan lupa, Presiden Susilo Bambang Yudhoyono pun berasal dari kalangan militer dan logis punya dukungan dan basis yang kuat. Lagi pula umumnya orang lebih suka ikut bersama “kapal yang berlayar” daripada hanya menonton di dermaga. Wapres Jusuf Kalla juga seorang mantan pebisnis, bersama Aburizal Bakrie yang duduk di pemerintahan, sehingga membuat banyak pebinis berpikir pragmatis.

Idolakan Militer
Nampaknya yang perlu dikontrol bukan militer. Melainkan sipil. TNI toh sudah back to basic dan hanya menghadapi musuh dari luar, bahkan termasuk memadamkan siapapun yang berniat melakukan kudeta. Tokoh sipil haruslah dihambat, atau jangan lagi mau “main mata” dengan militer untuk tujuan politik. Tegasnya, tak lagi menjadikan militer sebagai instrumen politik seperti pernah terjadi di masa silam. Memang, dalam riil politik, suka terdengar wacana bahwa siapa yang ingin eksis di panggung politik, hendaklah berbaik-baik dengan tentara. Nah, inilah yang di masa lalu membuat militer bagaikan pengantin, yang menunggu dilamar secara politik. Kira-kira, buat apa kudeta, yang nanti akan dibenci rakyat dan dunia internasional. Lebih baiklah menunggu “pinangan” sehingga kelak diikat dalam akad politik.

Masyarakat kita pun tak disangkal masih menyukai sosok tentara, atau mantan tentara di panggung politik. Kemenangan Presiden Susilo pada Pemilu 2004 lalu menjadi bukti. Termasuk kalahnya politikus senior Akbar Tandjung dalam konvensi Calon Presiden Golkar oleh Jenderal Purnawirawan Wiranto. Mungkin inilah sisa kultur politik Orde Baru. Apalagi setelah era reformasi 1998, ternyata pemerintahan sipil tak mampu membuat perubahan yang menggembirakan rakyat. Padahal, dengan kondisi Indonesia yang rusak, siapa yang bisa menyelesaikannya dalam tempo lima-enam tahun. Terbukti ketika kini pun Susilo yang mantan militer itu menjadi Presiden, toh keadaan belum berubah drastis.

Strategi lama tersebut mengingatkan kita kepada manuver politik Soerjadi dan John Naro yang mendekati militer dan kekuasaan sehingga bisa eksis, meski untuk sementara, karena kemudian bisa berubah jika kepentingan politik bergeser. Jika politik seperti itu diteruskan, maka social change tidak akan pernah terjadi, dan akan selalu merupakan situasi yang kondusif bagi militer untuk kembali ke peran politik. Toh militer sudah berpengalaman selama 32 tahun Orde Baru dijadikan sebagai instrumen politik. Kini benih itu belum hilang 100 persen dan masih ada residunya. Bisa saja tampilannya lebih halus, tapi barangkali naluri politik itu belum sepenuhnya padam, khususnya, mungkin bagi yang sudah purnawirawan. Karena itu militer harus mereformasi diri secara tuntas dan meninggalkan manuver atan gaya “pengantin” dimaksud. Namun kaum sipil, khususnya politisi haruslah percaya diri, dan tak lagi perlu doping politik dari kalangan tentara, atau mantan tentara.

Jika kembali ke pokok soal, mungkin spekulasi bisnis rada mungkin. Maklum, TNI sendiri sudah terlatih berbisnis di masa Orde Baru. Bahkan secara khusus selama ini pembelian keperluan militer yang formal sekalipun tidak selalu melalui Departemen Pertahanan. Contohnya adalah pembelian empat unit helikopter MI-17 dari Rusia pada 19 Desember 2002 lalu. Meskipun uang muka sebesar 15 persen setara 3,2 juta dólar AS sudah diberikan, namun hingga sekarang, tahun 2006, heli dimaksud tak kunjung datang. Bahkan sekarang sudah menjadi urusan Kejaksaan Agung karena dinilai mengandung korupsi. Konon ada mark up sampai 4 juta olar AS, karena proses pembeliannya melalui mata rantai yang panjang, bahkan melibatkan sebuah perusahaan nasional, dan dua perusahan yang berbasis di Singapura dan Malaysia. Memang, urusan bisnis selalu mengejar rente.

Toh, ini hanya sebuah analisis. Marilah kita tunggu hasil penyelidikan dan investigasi yang dilakukan Pus Pom TNI. Panglima TNI pun telah berjanji akan mengusut kasus ini sampai tuntas, bahkan sekalipun jika melibatkan jenderal, baik yang aktif maupun purnawirawan.***


Bersihar Lubis, wartawan senior, tinggal di Jakarta.



* * *

Suara Merdeka, 4 Juli 2006

DPR Usut Temuan Senjata

AM Fatwa: Tak Lepas dari Motif Bisnis



JAKARTA - Ketua DPR RI Agung Laksono meminta agar Komisi I DPR (bidang
pertahanan) membentuk Panitia Kerja (Panja) untuk mengusut kasus
ditemukannya ratusan senjata api, ribuan amunisi, dan granat di rumah Wakil
Asisten Logistik KSAD (alm) Brigjen Koesmayadi.

Dia berharap, dengan dibentuknya Panja tersebut maka tidak akan ada upaya
untuk menghilangkan jejak kasus yang menimbulkan spekulasi. Dengan
dibentuknya Panja, maka secara politik kasus ini tidak berhenti begitu saja.

''Nantinya, yang menangani Panja tersebut adalah Komisi I. Saya mendukung
dibentuknya Panja tersebut. Langkah ini agar kasus itu dapat diusut secara
cepat, apakah terkait dengan peristiwa yang ada di dalam negeri atau
tidak,'' kata Agung Laksono di Gedung DPR RI Senayan Jakarta, Senin (3/7)
kemarin.

Wakil Ketua Umum DPP Partai Golkar itu juga mendesak agar pemerintah dan DPR tidak menghentikan pengusutan kasus tersebut. Selain itu, kasus itu harus dijelaskan kepada masyarakat luas, agar tidak menimbulkan berbagai
penafsiran politik ataupun keamanan yang bisa merugikan bangsa dan negara.

''Pengusutan itu harus dilakukan secara tuntas dan hasilnya harus pula
disampaikan kepada masyarakat luas, apakah keberadaan senjata itu hanya
sekadar hobi atau ada misi lain,'' ujarnya.

Untuk Makar

Secara terpisah, Wakil Ketua MPR AM Fatwa menilai, keberadaan senjata yang
begitu banyak itu tidak lepas dari motif bisnis. Namun dia tidak
mengesampingkan kemungkinan senjata itu akan digunakan untuk makar. Makar
yang dimaksud oleh Fatwa adalah dengan mengganggu stabilitas keamanan dalam
negeri.

''Senjata sebanyak itu bisa digunakan untuk membentuk satu kompi. Selain itu semua senjata laras panjang itu untuk menyerang. Dengan demikian pengusutan itu harus betul-betul tuntas dan dibuka ke publik,'' tandasnya.

Fatwa berharap, KSAD dan Panglima TNI mampu membuka dan mengusut kasus ini
secara terbuka dan transparan. Jangan sampai TNI melindungi anggotanya yang
bersalah. ''Saya sangat menyayangkan jika ada petinggi TNI yang melakukan
pembelaan seperti itu. Saya juga berharap DPR menggelar rapat untuk membahas senjata,'' tuturnya.

Kasus Mi-17

Di tempat terpisah, Ketua Tim Koneksitas Hendarman Supandji mengemukakan,
almarhum Brigjen Koesmayadi sudah direncanakan akan diperiksa oleh tim
koneksitas terkait dengan kasus dugaan korupsi proyek pengadaan helikopter
tempur Mi-17.

''Tim penyidik koneksitas sudah berencana untuk memeriksanya berkaitan
dengan pengadaan heli Mi-17. Karena baru rencana, jadwalnya belum kita
tetapkan. Namun rencana itu sudah ada,'' ungkap Hendarman di Kejagung,
kemarin.

JAM Pidsus Kejagung itu menjelaskan, dalam kasus Mi-17 tim koneksitas sudah
memeriksa 25 saksi, dari 39 saksi yang direncanakan akan diperiksa. Tidak
tertutup kemungkinan akan ada saksi-saksi lain yang akan dipanggil
berdasarkan perkembangan dari pemeriksaan saksi-saksi yang ada, seperti
halnya (alm) Brigjen Koesmayadi. ''Setelah pemeriksaan saksi selesai, baru
kita umumkan tersangkanya,'' tambahnya.

Laporan Berkala

Sementara itu, Presiden Susilo Bambang Yudhoyono (SBY) terus memantau proses investigasi kasus penemuan senjata di kediaman (alm) Brigjen TNI Koesmayadi di kompleks Puri Marina Jalan Pangandaran 15, Jakarta, 25 Juni lalu.

''Presiden telah memerintahkan Penglima TNI untuk melakukan investigasi
kasus tersebut sampai tuntas. Presiden meminta laporan secara berkala,''
ucap juru bicara kepresidenan Andi Malarangeng di Desa Sengon, Kecamatan
Prambanan, Klaten, kemarin.

Menurutnya, penemuan 145 pucuk senjata laras panjang dan pendek beserta
28.985 amunisi dan sembilan granat itu sudah di luar kewajaran. Saat ini,
masalah penemuan senjata itu juga menjadi perhatian publik. Publik
bertanya-tanya untuk apa senjata sebanyak itu.

''Presiden sangat memperhatikan masalah tersebut. Dia sangat ingin
mengetahui dari mana asal senjata-senjata tersebut dan akan digunakan untuk
apa. Tapi tunggu saja, saat ini kasus tersebut masih ditangani Puspom TNI,''papar Andi.

Namun, Andi belum menjelaskan tentang perkembangan laporan terakhir yang
diterima presiden. Selama dua hari presiden berada di Magelang dan sehari
kemudian berkunjung ke Klaten, sehingga belum memantau perkembangan kasus
tersebut.

Sebelumnya, kasus itu terkuak tak lama setelah Koesmayadi meninggal di
kediamannya di Cibubur, 25 Juni lalu. Penemuan senjata dalam jumlah besar
mengundang berbagai komentar. Telah muncul pula desakan dari masyarakat agarkasus itu diselesaikan sampai tuntas. Pengamat militer dan anggota DPR RI juga ingin kasus itu diselesaikan secepatnya secara transparan.(H28, F4,F5,hsn-49,41v)
