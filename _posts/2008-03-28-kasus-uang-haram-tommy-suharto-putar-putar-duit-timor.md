---
layout: post
title: 'Kasus uang haram Tommy Suharto : putar-putar duit Timor'
date: 2008-03-28
---

Tempo Interaktif, 31 Mar 2008


Tommy Soeharto Gagal Cairkan Uangnya
Pengadilan banding negara bagian Guernsey, Inggris, memberi kesempatan kepada pemerintah RI untuk memberi sanggahan atas informasi baru yang diberikan kuasa hukum Hutomo Mandala Putra alias Tommy Soeharto.

“Persidangan banding diundur sampai Juli nanti,” kata Arif Havas Oegroseno, Direktur Perjanjian Politik dan Keamanan Wilayah di Departemen Luar Negeri, Senin malam (31/3).

Havas bersama pengacara negara dari Kejaksaan Agung, Yoseph Suardi Sabda, sempat mengikuti dua kali persidangan singkat di Guernsey tersebut. “Sidang singkat pukul 09.30 (waktu setempat) menerima secara formal informasi tambahan dari penggugat,” kata Havas.

Pemerintah langsung minta waktu untuk menyanggah informasi baru berupa vonis tidak bersalah terhadap Soeharto dalam gugatan perdata pemerintah atas Yayasan Supersemar dan perjanjian damai dalam kasus Goro melawan PT Bulog.

Dalam sidang kedua yang berlangsung pukul 2 sore, hakim memutuskan memberi waktu kepada pemerintah RI hingga Juli nanti untuk menyiapkan sanggahannya. “Tim penggugat jumlahnya tujuh orang dan mereka langsung kelihatan lesu,” kata Havas.

Maklum, putusan ini berarti menunda harapan mereka untuk segera mencairkan uang senilai Rp 421 miliar milik Tommy di Banque Nationale de Paris and Paribas yang hingga kini masih dibekukan.

Sebelum berangkat ke Guernsey, Yoseph sempat mengatakan akan mengajukan status tersangka Tommy Soeharto pada kasus BPPC sebagai bukti baru dalam sidang banding. “Itu saja cukup untuk tetap membekukan dana.”

Adapun O.C. Kaligis, kuasa hukum Tommy, mengatakan putusan kasus Goro dan Yayasan Supersemar akan dijadikan bukti dalam sidang banding di Guernsey. Dalam kasus Goro, Pengadilan Negeri Jakarta Selatan menolak gugatan yang diajukan Bulog. Sedangkan dalam kasus Yayasan Supersemar, mantan presiden Soeharto dinyatakan tidak melakukan perbuatan melawan hukum.

Kaligis mengatakan Pengadilan Guernsey sudah memberi waktu enam bulan lebih kepada pemerintah Indonesia untuk membuktikan duit Tommy itu hasil korupsi. "Ternyata tidak terbukti," ujarnya.

Mengenai kasus BPPC, Kaligis mengatakan, "Jangan mentang-mentang berkuasa. Dalam waktu enam bulan kan sudah terbukti tidak ada kesalahan." Bhm | Titis

* * *

Jawapos, 29 Maret 2008,

Negara Tak Perlu Bayar Rp 5 Miliar

Soal Pencabutan Gugatan atas Tommy Soeharto


JAKARTA - Langkah Perum Bulog mencabut gugatan kepada Hutomo Mandala
Putra alias Tommy Soeharto dalam kasus dengan PT Goro Batara Sakti
dianggap pemerintah sebagai solusi terbaik. Tapi, Wapres Jusuf Kalla
membantah pencabutan gugatan itu sebagai upaya perdamaian antara
pemerintah dan keluarga Soeharto.

"Sebenarnya, itu bukan perdamaian. Tapi, hanya menghentikan
permasalahan sekaligus menyelesaikan persoalan," ujar Kalla di
Kantor Wakil Presiden, Medan Merdeka Selatan, Jakarta, kemarin.

Wapres menegaskan, pencabutan gugatan tersebut tidak akan merugikan
keuangan negara. Bulog justru terhindar dari kewajiban membayar
ganti rugi Rp 5 miliar, sebagaimana putusan Pengadilan Negeri (PN)
Jakarta Selatan. "Tidak perlu (bayar). Kan semua (gugatan) dicabut,"
tuturnya.

Meski demikian, Kalla tidak bersedia berkomentar soal dampak
pencabutan gugatan tersebut terhadap gugatan negara atas uang Rp 420
miliar milik Tommy yang dibekukan Banque Nationale de Paris and
Paribas (BNP Paribas). Dana yang tersimpan atas nama perusahaan
milik Tommy, Garnet Investment Limited, itu dibekukan karena diduga
sebagai hasil korupsi. Garnet Investment kini tengah menggugat BNP
Paribas karena menolak mencairkan dana itu.

Saat dikonfirmasi kemarin, Dirut Perum Bulog Mustafa Abubakar
menolak berkomentar mengenai perdamaian antara Bulog dan
Tommy. "Saya no comment. Silakan tanya pengacara Bulog. Sejak awal
sudah ada kesepakatan, saya tidak mengurusi perkara hukum. Fokus
saya hanya stabilisasi harga pangan," katanya. (noe/dwi)

* * *

Anak-anak Soeharto lolos

Berikut di bawah ini dikutip dua berita dari Jawapos dan majalah Gatra mengenai persidangan pengadilan tentang Yayasan Supersemar, yang mengindikasikan bahwa upaya hukum untuk mengembalikan uang negara dari keluarga Suharto mengalami kegagalan. Karena berita penting ini pasti akan menimbulkan kemarahan banyak orang, dan memancing berbagai reaksi keras dari banyak kalangan, maka « Kumpulan berita soal uang haram Tommy Suharto » akan terus-menerus berusaha menyajikan beraneka-ragam berita dan tulisan mengenai persoalan ini. Berbagai kesulitan yang menghalangi pembongkaran kasus korupsi keluarga Suharto dan dikembalikannya harta haramya untuk negara merupakan cermin yang jelas bagaimana busuknya sistem hukum dan peradilan di negeri kita dan juga rusaknya moral banyak tokoh-tokoh di berbagai kalangan. Silakan baca, walaupun dengan hati yang geram ….



Paris, 28 Maret 2008

A. Umar Said
* * *

Jawapos,, 28 Maret 2008,

Anak-Anak Soeharto Lolos

Kasus Korupsi, Pengadilan Hanya Hukum Pengurus Yayasan Supersemar


JAKARTA - Upaya hukum untuk mengembalikan uang negara dari keluarga mantan Presiden Soeharto (alm) berakhir sia-sia. Dalam sidang kemarin (27/3), PN Jakarta Selatan membebaskan lima ahli waris Soeharto dari segala kerugian negara dalam kasus korupsi dana Yayasan Supersemar.

Lima ahli waris itu adalah Siti Hardiyanti Rukmana (Tutut), Sigit Harjojudanto, Bambang Trihatmodjo, Siti Hediati Hariyadi (Titiek), dan Siti Hutami Endang Adiningsih (Mamiek). Sedangkan Tommy Soeharto menolak menjadi ahli waris dalam kasus Supersemar.

Majelis hakim yang diketuai Wahjono membeberkan, Soeharto selaku ketua yayasan tidak dapat dimintai pertanggungjawaban pribadi karena telah melaporkan pertanggungjawaban kepada yayasan dan di hadapan Sidang MPR 1998. "Sebagai pengurus yayasan, dia (Soeharto) telah membuat laporan pertanggungjawaban," ujar Wahjono yang didampingi hakim Aswan Nurcahyo dan Eddy Risdianto.

Hakim berpendapat, pengurus yayasan justru yang terbukti melakukan perbuatan melawan hukum. Mereka dapat dimintai pertanggungjawaban membayar kerugian materiil sebesar 25 persen dari total nilai gugatan, yakni USD 105,727 juta dan Rp 46,479 miliar.

Emerson Yuntho, ketua bidang Hukum dan Monitoring Peradilan ICW, menilai bahwa putusan tersebut menguatkan bahwa upaya perdata kasus Supersemar merupakan persidangan pura-pura. "Soeharto dan kroni-kroninya menjadi tak tersentuh lagi," kata Emerson saat dihubungi koran ini kemarin (27/3). Putusan tersebut, lanjut Emerson, telah diperkirakan sejak kejaksaan menghentikan penuntutan pidana melalui penerbitan SKPP (surat ketetapan penghentian penuntutan).

Menurut Emerson, ketidakseriusan penanganan kasus Supersemar terlihat ketika jaksa pengacara negara (JPN) tidak menyita seluruh aset Soeharto dan yayasan. Padahal, upaya perdata selalu diikuti sita jaminan alias conservatoir beslag (CB).

Di tempat terpisah, Ketua Ombudsman Nasional (KON) Antonius Sudjata menilai, kejaksaan telah gagal atas upaya hukum kasus Soeharto. Kejaksaan seharusnya tidak menempuh upaya perdata karena pewarisan gugatan tidak dikenal. "Gugatan itu sejak awal sudah diprediksi lemah," jelas Antonius, yang mantan Jaksa Agung Muda Pidana Khusus (JAM Pidsus).

Selain itu, lanjut Antonius, putusan tersebut dapat dibenarkan karena mengacu pada UU Yayasan. "Dalam undang-undang tersebut dijelaskan, apabila ketua tidak dapat dipertanggungjawabkan, dapat dialihkan kepada pengurus lain," jelasnya.

Meski demikian, kejaksaan harus menempuh upaya banding apabila putusan tersebut dianggap cacat. "Saya kira, satu-satunya jalan, kejaksaan harus banding," ujar Antonius.

Gayus Lumbuun, anggota Komisi II DPR (membidangi hukum), menolak mengomentari isi putusan kasus Soeharto. "Kalau putusannya demikian, ya begitulah," jelas Gayus. Anggota Fraksi PDIP itu juga menolak menyimpulkan bahwa putusan tersebut merupakan cermin kegagalan kejaksaan mengembalikan kerugian negara dalam kasus Supersemar.


Melanggar Kepmenkeu

Kasus Yayasan Supersemar memang rumit. Yayasan itu didirikan untuk memberi beasiswa kepada para mahasiswa yang tak mampu. Soeharto lantas menerbitkan PP 15 Tahun 1976 dan Kepmenkeu No 333/KMK.011/1978, yakni meminta BUMN menyerahkan keuntungan 2,5 persen dari laba bersih. Nah, inilah salah satu sumber dana beasiswa itu.

Dalam perjalanannya, sebagian dana disalahgunakan. Dana yang seharusnya untuk pendidikan itu dipinjamkan atau dimasukkan pernyataan modal ke sejumlah perusahaan milik kroni Cendana. Di antaranya, PT Kiani Sakti, PT Sempati Air, kelompok usaha Kosgoro, PT Bank Duta, PT Kalhod Utama, Essam Timber, dan PT Tanjung Redep Hutan Tanaman Industri (lihat grafis).

Sebagian besar dana yang diberikan ke perusahaan itu tak kembali. Negara lantas menggugat kerugian USD 420 juta dan Rp 185 miliar. Soeharto sebagai ketua yayasan menjadi tergugat utama.

Walaupun jaksa menganggap dana Yayasan Supersemar dari negara yakni potongan laba BUMN, majelis hakim berpendapat sebagian dana yang disalurkan bukan dari pemerintah.

Hakim juga menganggap yayasan telah menyalurkan sebagian uang yayasan sesuai peruntukan selama 1975 hingga 2007. Di antaranya, beasiswa Rp 206,76 miliar untuk 413.530 mahasiswa, beasiswa Rp 156,77 miliar untuk siswa SMK, beasiswa Rp 6,98 miliar untuk 835 ribu atlet, dan bantuan beasiswa Rp 42,8 miliar untuk anak asuh SD.

Menurut Wahjono, dengan bercampurnya uang untuk beasiswa dan keperluan sosial, sepantasnya uang pemerintah yang masuk dalam yayasan hanya 25 persen dari total nilai gugatan USD 420 juta dan Rp 185 miliar. Karena itu, majelis hanya mewajibkan pengurus yayasan mengganti kerugian 25 persen dari total nilai gugatan. Anak-anak Soeharto sendiri terbebas.

Bukan hanya itu. Keluarga Cendana juga bebas dari tuntutan membayar kerugian immaterial. "Majelis menolak nilai kerugian immaterial Rp 10 triliun, karena tidak ada hitungan jelas berapa mahasiswa (calon penerima beasiswa) yang dirugikan akibat penyalahgunaan tersebut," jelas majelis hakim.

Seusai sidang, jaksa pengacara negara (JPN) Dachmer Munthe menilai aneh putusan tersebut. Majelis tidak dapat menjadikan alasan pertanggungjawaban di depan MPR sebagai penghapus perbuatan melawan hukum. "Itu kan bersifat politis yang berbeda dengan pertanggungjawaban hukum," ujar Dachmer. Karena itu, kejaksaan memastikan mengajukan banding atas putusan tersebut.

Meski demikian, Dachmer menghormati putusan tersebut. Apalagi, lanjut Dachmer, itu dianggap mengabulkan dalil perbuatan melawan hukum sesuai pasal 1365 KUH Perdata. "Gugatan saya sudah terbukti. Masalah nilai uangnya, itu (urusan) nanti. Kami benarin itu. Kalau kita, inginnya semua dong, USD 420 juta," jelasnya.

Terpisah, pengacara Soeharto, Juan Felix Tampubolon, mengatakan, tim pengacara tidak sependapat dengan putusan tersebut. "Kami juga ajukan banding," ujar Juan Felix.

Dia menganggap putusan hakim berbeda dengan sejumlah materi eksepsi yang diajukan tim pengacara. Salah satunya, penggunaan uang negara, meski hanya 25 persen dari total USD 420 juta dan Rp 185 miliar. "Dari fakta persidangan kan sudah jelas bahwa klien kami dan yayasan tidak pernah menggunakan uang negara, tetapi putusan hakim lain. Jadi, kalau kami harus mengembalikan uang negara, kami melihat bahwa negara tidak punya kepentingan apa-apa. Negara kan bukan sebagai pihak," beber Juan Felix.

Selain itu, lanjut Juan Felix, penggunaan uang yayasan untuk swasta dibenarkan sesuai anggaran dasar (AD) yayasan. Dia juga menolak pendapat hakim yang menyebut pengurus yayasan melanggar PP No 15 Tahun 1976. "PP itu kan di bawah undang-undang. Sedangkan yayasan selama ini sudah bergerak sesuai undang-undang," jelas Juan Felix yang didampingi M. Assegaf, Denny Kailimang, dan O.C. Kaligis.


Pengurus Yayasan Mengaku Heran

Di tempat terpisah, Humas Yayasan Supersemar Herno Sasongko mengaku heran atas putusan pengadilan yang menghukum pengurus yayasan mengembalikan kerugian negara senilai 25 persen dari total nilai gugatan USD 420 juta dan Rp 185 miliar. "Saya juga hadir dalam sidang, tetapi hakimnya bingung, menghitungnya dari mana bisa dapat hanya 25 persen uang negara yang masuk ke yayasan," jelas Herno.

Menurut dia, total aset yayasan Rp 600 miliar yang dikelola pengurus. Uang tersebut berasal dari sisa pengucuran pemerintah dan sejumlah perusahaan swasta. "Sejak 1998, kami sudah tidak dapat kucuran lagi bantuan dari pemerintah," ujarnya.

Sejak berdiri 1975 silam, lanjutnya, total uang yang disalurkan untuk beasiswa mencapai Rp 400 miliar.

Herno menambahkan, pengurus yayasan justru bersyukur atas putusan tersebut. Sebab, Soeharto tidak dibebani kerugian negara. "Kalau pengurus sendiri masih menunggu. Sebab, putusannya kan belum final, kami akan ajukan banding," tegas Herno.

Usai pembacaan putusan hakim, katanya, seluruh pengurus yayasan tidak menggelar pertemuan untuk menyikapi isi putusan. "Kami biasa-biasa saja." Yayasan Supersemar kini hanya diurus oleh Sekretaris Abdurrahman dan Bendahara Subagyo. (agm/tof)

· * *

Majalah GATRA, 27 Maret 2008

Yayasan Beasiswa Supersemar Didenda


Majelis hakim Pengadilan Negeri Jakarta Selatan, Kamis (27/3), memutuskan agar Yayasan Beasiswa Supersemar membayar ganti rugi sebesar 105,7 juta dolar AS dan Rp46,47 miliar kepada negara.

Majelis hakim yang diketuai Wahjono memutuskan perkara tersebut dalam sidang perkara gugatan perdata yang dilayangkan negara kepada mantan presiden Soeharto dan Yayasan Beasiswa Supersemar atas dugaan penyelewengan dana beasiswa.

Negara melalui Tim Jaksa Pengacara Negara (JPN) menyatakan Soeharto dan Yayasan Beasiswa Supersemar melakukan perbuatan melawan hukum karena menyalurkan dana yayasan ke sejumlah perusahaan, padahal tujuan pembentukan yayasan adalah memberikan beasiswa.

Hal itu dinilai melanggar Peraturan Pemerintah (PP) Nomor 15 Tahun 1976 Tentang Penetapan Penggunaan Sisa Laba Bersih Bank-Bank Milik Pemerintah, yang kemudian diatur dengan Keputusan Menteri Keuangan Nomor 373/KMK.011/1978, serta Pasal 3 Anggaran Dasar Yayasan Beasiswa Supersemar.

Untuk itu, JPN menuntut pengembalian dana yang telah disalahgunakan senilai 420 juta dolar AS dan Rp185,92 miliar, ditambah ganti rugi imateriil Rp10 triliun.

Anggota majelis hakim, Edy Risdiyanto, menyatakan, perbuatan pengurus yayasan untuk meminjamkan uang kepada sejumlah perusahaan sebenarnya dibenarkan oleh Anggaran Dasar dan Anggaran Rumah Tangga Yayasan Beasiswa Supersemar.

Kendati demikian, kata Edy, hal itu tidak dapat dibenarkan apabila mengacu pada PP 15 Tahun 1976 tentang Penetapan Penggunaan Sisa Laba Bersih Bank-Bank Milik Pemerintah, yang kemudian diatur dengan Keputusan Menteri Keuangan Nomor 373/KMK.011/1978.

Pasal 2 F PP 15 Tahun 1976 menyatakan lima persen dari laba bersih bank pemerintah harus digunakan untuk kepentingan sosial. Yayasan Beasiswa Supersemar menerima 50 persen dari lima persen laba bersih bank-bank milik pemerintah. "Jadi uang itu tidak boleh dibisniskan untuk mendapatkan bunga," kata Edy.

Edy menegaskan, PP dan Keputusan Menteri merupakan aturan yang lebih tinggi dari Anggaran Dasar dan Anggaran Rumah Tangga Yayasan Beasiswa Supersemar, sehingga penyaluran dana yayasan kepada perusahaan merupakan perbuatan melawan hukum.

Majelis hakim hanya menghukum Yayasan Beasiswa Supersemar sebesar 105,7 juta dolar AS dan Rp46,47 miliar atau 25 persen dari nilai gugatan negara. Hal itu disebabkan karena sulit untuk menjamin bahwa dana yang disalurkan yayasan kepada sejumlah perusahaan murni dari bank-bank pemerintah.

Selain itu, Majelis Hakim juga menilai, angka 25 persen dari total nilai gugatan merupakan angka yang adil, dengan harapan yayasan tidak mengalami kekurangan kas, sehingga tetap bisa menyalurkan beasiswa.

Anggota majelis hakim Aswan Nurcahyo menyatakan, sumber keuangan Yayasan Beasiswa Supersemar tidak hanya berasal dari bank pemerintah, melainkan juga dari sejumlah donatur, lembaga, dan perusahaan.

Nilai sumbangan di luar bank pemerintah mencapai Rp217,6 miliar. "Tidak semua uang berasal dari pemerintah," kata Aswan.

Selain hanya mengabulkan sebagian nilai gugatan, majelis menolak permohonan ganti rugi imateriil sebesar Rp10 triliun. Menurut majelis, alasan JPN bahwa ganti rugi imateriil didasarkan pada hilangnya kesempatan pelajar untuk mendapatkan beasiswa merupakan alasan yang tidak dapat diterima.

Seharusnya, menurut majelis, JPN merinci jumlah siswa yang kehilangan kesempatan mendapatkan beasiswa. Selain itu, Presiden sebagai pemberi kuasa kepada JPN tidak dibenarkan oleh hukum perdata dan hukum tata negara untuk menjadi kuasa golongan masyarakat tertentu.

Menanggapi putusan itu, tim kuasa hukum Yayasan Beasiswa Supersemar yang diwakili Juan Felix Tampubolon menyatakan banding. Dia menilai seharusnya yayasan tidak dibebani apapun karena uang yayasan bukan uang negara.

Soeharto Bebas
Gugatan perdata tersebut juga dilayangkan terhadap Soeharto yang diwakili oleh sejumlah anaknya sebagai tergugat I.

Majelis hakim menilai penguasa Orde Baru itu tidak bisa digugat sebagai pribadi karena aliran dana ke sejumlah perusahaan terjadi saat Soeharto menjadi Ketua Yayasan.

Sebagai Ketua Yayasan dan Presiden, menurut majelis, Soeharto juga tidak digugat karena telah mempertanggungjawabkan pekerjaan kepada pengurus yayasan dan Majelis Permusyawaratan Rakyat (MPR). "Maka tergugat I tidak melakukan perbuatan melawan hukum," kata Aswan.

"Maka para ahli warisnya juga tidak melakukan perbuatan melawan hukum," lanjut Aswan.

Terhadap putusan itu, Ketua JPN Dachamer Munthe merasa keberatan. Menurutnya, pertanggungjawaban seorang tergugat haruslah diukur dengan pertanggungjawaban hukum, bukan pertanggungjawaban politis di depan MPR. "Ini kan agak aneh, pertanggungjawaban politis dan pertanggungjawaban hukum kan beda," kata Dachamer setelah sidang.

* * *

"Bangkitnya kembali" Tommy Suharto
Mohon sudilah kiranya para pembaca ikut merenungkan isi berita yang dimuat Jawapos 26 Februari 2008 mengenai “bangkit-kembalinya” Tommy Suharto, yang menunjukkan bahwa ia masih mempunyai uang yang banyak sekali, sehingga bisa terus mempengaruhi banyak orang (termasuk pejabat-pejabat penting negara) dengan berbagai cara, dan meneruskan “bisnis”nya. Sesudah membaca berita Jawapos ini, orang bisa geleng kepala karena keheranan (atau karena geram), sambil bertanya-tanya “Mengapa Tommy, yang jelas-jelas adalah sampah masyarakat dan penjahat besar itu masih bisa bertindak seperti itu?”.

Pertanyaan semacam itu adalah wajar sekali. Sebab, seperti yang dapat dibaca kembali dalam “Kumpulan berita tentang uang haram Tommy Suharto” persoalan yang menyangkut Tommy Suharto merupakan cermin betapa sudah rusaknya sistem hukum negara kita dan bejatnya moral anak Suharto ini, yang dengan uangnya yang berlimpah-limpah itu bisa saja dengan mudah “membeli” para pejabat, hakim, jaksa, polisi, militer, pengacara, pejabat pajak, pimpinan berbagai bank, pembantu-pembantu utamanya, konco-konconya, dan para “pengawalnya”.

Berkat itu semualah, maka persoalannya dengan masalah BPPC (urusan cengkeh, kerugian negara Rp 1,7 triliun), masalah projek mobil Timor (utang sebesar Rp 4,5 triliun), persoalan dengan PT Vista, simpanan uang sebesar 36 juta dollar AS di BNP Parisbas di Guernsay, persoalan dengan Bulog (gugatan Rp 3 triliun), tetap merupakan urusan yang ruwet, atau memang sengaja dibikin “ruwet” oleh berbagai fihak berkat banyaknya uang suapan yang bisa dimainkan dengan berbagai cara. Kalau dibaca kembali “Kumpulan berita tentang uang haram Tommy Suharto” maka dari bahan-bahan ini saja sudah kelihatan – walaupun baru sebagian – wajah sebenarnya atau jati diri Tommy Suharto ini.

Dan sosok yang seperti inilah yang ketika datang di Lampung baru-baru ini, disambut dengan “kehormatan” yang tinggi oleh banyak orang, termasuk oleh Gubernur Lampung dan para bupati di Lampung dan Sumatera Selatan.

Padahal, sejak Tommy memulai “bisnis”-nya (dengan berbagai fasilitas yang berupa penyalahgunaan kekuasaan oleh bapaknya) sudah tercium bau busuknya. Apakah sosok semacam ini masih dipercayai untuk membikin hidup para petani Indonesia lebih baik? Dan apakah tujuan “bisnis”-nya yang sekarang sudah berbeda dengan tujuan “bisnis”-nya yang lalu ? Singkatnya, apakah dari seorang yang sudah menimbulkan kerugian begitu besar kepada negara dan rakyat masih bisa diharapkan adanya kebaikan bagi kepentingan orang banyak?

Untuk lebih jelasnya, harap baca berita Jawapos itu.

A. Umar Said
= = =

Berita dalam Jawapos :

Tommy Soeharto Sosialisasikan Bisnis
lewat Tradisi Temu Wicara Warisan Ayah
Sering Pakai Kata Daripada dan Semangkin

“Sebulan setelah Soeharto wafat, Hutomo Mandala Putra alias Tommy Soeharto mulai rajin ke daerah. Bersama para pejabat, sang putra bungsu itu juga menghidupkan budaya "temu wicara" dengan para petani dengan bahasa dan gaya Pak Harto dulu.

TAHUN ini boleh jadi hari-hari yang sibuk bagi Tommy Soeharto. Lewat bendera baru perusahaannya, PT Mandala Energi Terapanindo, perusahaan yang memproduksi mesin pengubah batubara menjadi gas sintetis, putra kelima Pak Harto itu mencoba bangkit lagi.

Pesanan terhadap mesin Baragas (merek yang diusung PT Mandala) memang terus mengalir. Kamis (21/2) lalu "mengantarkan" pesanan kepada para anggota kelompok tani di Panjang, Bandar Lampung. Dan, Maret nanti melayani order perusahaan tekstil milik warga India di Rancaekek, Bandung. Total tahun ini ditargetkan mampu menjual 150 unit.

Pabrik Baragas dibangun di bekas lahan pabrik mobil nasional PT Timor Putera Nasional di Cikampek, Jawa Barat.

Selain itu, order datang dari beberapa pemesan di Kalimantan dan Sulawesi. Untuk pasar ekspor, Tommy mengincar Kamboja, negeri yang menghormati mendiang ayahnya karena dianggap memfasilitasi "kemerdekaan" negara tersebut.

Saat hadir di Lampung, provinsi penghasil kopi dan lada terbesar di Indonesia, Tommy masih seperti anak "kepala negara". Tiba di bandara dengan pesawat carter Beechcraft 1900, Tommy langsung menuju tempat acara dengan pengawalan voorrijders.

Di lokasi PT Sinar Alam Kencana, tempat acara, dia disambut Gubernur Lampung H Sjahroedin Z.P., Duta Besar Kerajaan Kamboja untuk Indonesia Mr Khem Bunnaeng, serta para bupati di Provinsi Lampung dan Sumatera Selatan.

Mengenakan batik cokelat, celana dan sepatu hitam, Tommy menjadi pusat perhatian. Para undangan, terutama ibu-ibu, berusaha mengabadikan wajahnya dengan kamera handphone. Bahkan, di antara mereka ada yang meminta foto bareng.

Puluhan fotografer maupun kamerawan pun berebut mengambil gambar presiden komisaris PT Humpuss tersebut. Tommy memang bak selebriti. Bahkan, gubernur Lampung pun seperti kalah pamor.

Didampingi direksi PT Humpuss, perusahaan yang menangani distribusi Baragas, pria 46 tahun itu memberikan bingkisan kepada 40 yatim piatu. Setelah itu, Tommy menandatangani prasasti hibah mesin Baragas kepada Kelompok Tani Sertani yang berasal dari tiga kabupaten di Lampung.

Tommy yang beberapa tahun dipenjara di Lapas Batu, Nusakambangan, karena kasus pembunuhan hakim agung, lalu menggelar temu wicara dengan para petani. Lima kursi berjajar rapi berhadapan dengan para petani. Tommy duduk di tengah, diapit Gubernur H Sjahroedin Z.P. dan para direksi PT Humpuss.

"Pak Tommy, kami para petani tolong dibantu. Kenaikan BBM ini sangat memukul kami," curhat seorang petani.

Mendengar keluhan itu, Tommy pun menjelaskan bahwa kehadirannya di Lampung salah satunya ingin membantu petani dengan meluncurkan produk Baragas. Langkah yang ditempuh bukan semata-mata bisnis. Dia ingin membantu petani, pengusaha, serta mencari energi alternatif yang ramah lingkungan dan murah.

Kepada petani, Tommy mengatakan juga sedang memproduksi pupuk organik. Dengan produk itu, produktivitas petani akan naik 50 persen. Selain biaya pemupukan lebih hemat (20-30 persen), produktivitas hasil pertanian akan naik sekitar 50 persen. "Ini kalau dijalankan secara nasional, maka subsidi pupuk semangkin tidak diperlukan lagi," katanya.

Begitu pula dengan Baragas. Menurut Tommy, kalau dijalankan secara nasional, kelangkaan BBM yang dialami banyak pembangkit listrik tidak terjadi lagi. Tapi, karena produknya baru tahap awal, Tommy minta waktu untuk pengembangan. "Semoga tidak butuh waktu terlalu lama untuk bisa diwujudken. Gensetnya bisa pakai Baragas lagi. Tidak pakai daripada solar lagi," jelas dia disambut tepuk tangan meriah para petani.

Ada juga petani yang kritis. "Pak Tommy, jangan-jangan kalau mesin Baragas sudah diterima petani dan petani sudah sangat tergantung, tidak ada pembinaan terhadap mereka," katanya.

Oleh kelompok tani setempat, kata dia, unit Baragas itu dipakai untuk pengeringan jagung. "Sejauh mana perhatian PT Humpuss sendiri terhadap petani. Jangan sampai kasih alat, tapi petani tidak diperhatikan," sambungnya.

Mendengar pertanyaan itu, Tommy, seperti gaya Pak Harto dulu, hanya tersenyum. Kata dia, mesin Baragas -harganya sekitar Rp 350 juta untuk tipe yang kecil hingga Rp 3 miliar- bukan hanya dipakai di Lampung, tapi sudah dipakai di berbagai daerah, bahkan negara lain. Karena itu, kata Tommy, Humpuss tak akan meninggalkan begitu saja customer-nya, sehingga memberi kesan jelek kepada karya asli anak bangsa ini.

Menurut Tommy, kelompok petani adalah pasar potensial Baragas. Kalau puas dengan produknya, kata Tommy, mereka akan menyampaikan kepada teman-teman mereka. "Untuk pembiayaan, Humpuss menyerahken ke petani sendiri, apakah perseorangan atau kelompok," katanya.

Selama temu wicara, tampak sekali Tommy seperti mewarisi gaya sang ayah. Senyumnya tampak sengaja ditata sedemikian rupa. Termasuk penggunaan kosa kata Soeharto yang sangat khas, seperti "semangkin", "daripada", serta akhiran "ken". Bedanya, kalau ada pertanyaan lucu, Soeharto membalas dengan tawa lepas dan renyah, Tommy hanya tersenyum-senyum. (el)
* * *


Kompas , 27 Februari 2008

Tommy Soeharto Tolak Surat Kuasa


Hutomo Mandala Putra atau Tommy Soeharto menolak menandatangani surat kuasa penunjukan kuasa hukum dalam perkara gugatan perdata pemerintah terhadap mantan Presiden Soeharto dan Yayasan Beasiswa Supersemar. Tommy masih mempertanyakan eksistensi ahli waris dalam perkara tersebut setelah ayahnya itu meninggal dunia.


Hal itu disampaikan Indriyanto Seno Adji, salah seorang kuasa hukum putra-putri almarhum Soeharto, saat dihubungi Kompas, Selasa (26/2) di Jakarta. ”Karena itu, diajukan satu ahli lagi untuk memberikan keterangan tentang kedudukan tergugat dalam perkara ini, apakah dapat digantikan oleh ahli waris,” katanya.
Lima putra-putri almarhum Soeharto, yakni Siti Hardiyanti Hastuti Indra Rukmana, Sigit Harjojudanto, Bambang Trihatmodjo, Siti Hediyati Haryadi, dan Siti Hutami Endang Adiningsih, menandatangani surat penunjukan kuasa hukum kepada OC Kaligis, Juan Felix Tampubolon, Denny Kailimang, Mohamad Assegaf, Wimboyono Seno Adji, dan Indriyanto Seno Adji.


Pada sidang di Pengadilan Negeri Jakarta Selatan, Selasa, majelis hakim yang diketuai Wahjono menyatakan, Tommy Soeharto dianggap melepaskan haknya untuk mengajukan pembelaan dalam perkara ini. Pasalnya, Tommy yang tidak menunjuk kuasa dianggap tak hadir meskipun telah dipanggil oleh pengadilan.
Majelis hakim juga mengabulkan permintaan Sigit Harjojudanto, yang mengajukan Prof Dr Busthanul Arifin, untuk memberikan keterangan di persidangan sebagai ahli. Namun, keterangan ahli hanya sebatas pada kedudukan ahli waris almarhum Soeharto sebagai tergugat I, untuk menggantikan kedudukan tergugat I yang meninggal dunia.


Ahli hukum Islam
Soal ahli yang akan dihadirkan pada persidangan tanggal 4 Maret 2008, Indriyanto membenarkan, Busthanul Arifin adalah mantan Ketua Muda Urusan Peradilan Agama Mahkamah Agung. Keterangan Busthanul nanti untuk menguatkan pendapat, perkara gugatan perbuatan melawan hukum yang diajukan Pemerintah Republik Indonesia itu bukanlah perkara warisan.
”Ini adalah gugatan perbuatan melawan hukum. Jadi, tidak bisa diwariskan kepada ahli waris tergugat,” kata Indriyanto.


Jaksa pengacara negara Yoseph Suardi Sabda berpendapat, kewajiban ahli waris tergugat tidak ditentukan oleh hadir atau tidaknya ahli waris itu. ”Kalau pengadilan sudah memanggil ahli waris itu, lalu tidak datang, ya sudah. Perkara tetap bisa berjalan seperti biasa. Kalau putusan mengabulkan gugatan, ahli waris yang tidak datang tetap dikenai kewajiban seperti yang lain,” ujarnya lagi.
Soal kehadiran ahli dari pihak tergugat pada sidang pekan depan, Yoseph berpendapat, hal itu hanya memperlambat pembacaan putusan. Seharusnya, pekan depan pihak penggugat dan tergugat dapat membacakan kesimpulan, tetapi ditunda sepekan untuk memberi waktu bagi ahli.


Yoseph juga menyatakan jaksa sudah menyiapkan hal-hal untuk menanggapi keterangan ahli nanti. ”Busthanul Arifin ini ahli hukum Islam. Kami akan menegaskan, di pengadilan umum yang berlaku adalah HIR (Het Herziene Indonesisch Reglement) atau Kitab Undang-undang Hukum Acara Perdata,” ujarnya.
Pemerintah RI menggugat Soeharto dan Yayasan Beasiswa Supersemar atas perbuatan melawan hukum dalam menyalurkan dana beasiswa Supersemar yang diduga tidak sesuai dengan peruntukan. Pemerintah mengajukan ganti rugi materiil 420 juta dollar AS dan Rp 185 miliar serta ganti rugi imateriil Rp 10 triliun. (idr)

* * *

Tempo Interaktif, , 27 Februari 2008

Nurdin Halid Diperiksa Soal BPPC

Ketua Umum Persatuan Sepakbola Seluruh Indonesia (PSSI), Nurdin Halid diperiksa penyidik Kejaksaan Agung dalam kasus dugaan korupsi di tubuh Badan Penyangga dan Pemasaran Cengkeh (BPPC).Menurut Direktur Penyidikan Kejaksaan Agung, Muhammad Salim, Nurdin yang kini berstatus terpidana kasus dugaan korupsi distribusi minyak goreng diperiksa di Lembaga Pemasyarakatan Salemba, Jakarta Pusat.

"Pemeriksaan seputar kasus BPPC." katanya singkat.Salim menjelaskan, Nurdin diperiksa oleh penyidik dalam kapasitasnya sebagai mantan pengurus Induk Koperasi Unit Desa (Inkud). Inkud merupakan salah satu unsur pimpinan BPPC selain pemerintah dan swasta.Kasus dugaan korupsi di BPPC bermula dari penyalahgunaan dana kredit likuiditas Bank Indonesia.

Menurut penyidikan kejaksaan, dana yang diduga bermasalah nilainya mencapai Rp 175 Miliar. Saat ini kejaksaan sudah menetapkan Hutomo Mandala Putra alias Tommy Soeharto sebagai tersangka. Kasus ini ditangani oleh bagian pidana khusus Kejaksaan Agung.

Kuasa hukum Nurdin Halid, Ali Abbas, membenarkan soal pemeriksaan penyidik terhadap kliennya. Saat ini, kata dia, Nurdin berstatus sebagai saksi dalam kasus dugaan korupsi yang membelit Tommy Soeharto. "Saya tidak mendampingi Nurdin saat pemeriksaan," katanya. Sandy Indra Pratama

* * *

Bali Post, 14 Februari 2008

Kasus BPPC, Kejakgung

Kesulitan Seret Tommy

Upaya Kejaksaan Agung (Kejakgung) untuk menyeret Hutomo Mandala Putra alias Tommy Soeharto ke pengadilan bakal gagal. Pasalnya, penyidikan terhadap tersangka kasus dugaan korupsi dana Kredit Likuiditas Bank Indonesia (KLBI) yang diterima Badan Penyangga dan Pemasaran Cengkeh (BPPC) itu mentok, karena sulitnya mendapatkan alat-alat bukti. Kesulitan tersebut disebabkan kantor cabang BPPC yang tersebar di sejumlah daerah sudah tutup. Bahkan, dokumen-dokumen yang akan dijadikan barang bukti telah banyak yang raib.

Ini kendala utama yang dirasakan tim penyidik. ''Tetapi kami akan terus berusaha mencari alat-alat bukti itu,'' kata Direktur Penyidikan Pidsus Kejaksaan Agung Muhammad Salim, Rabu (13/2) kemarin. Meski kesulitan mendapatkannya, Salim memastikan penyidikan kasus itu takkan dihentikan. Kejaksaan tetap melanjutkannya hingga tuntas. Namun, tidak bisa dipastikan kapan waktunya.

Langkah ini merupakan bagian dari upaya kejaksaan untuk meyakinkan Pengadilan Guersney yang telah membekukan aset milik Tommy yang tersimpan di Banque Nationale Paris de Paribas (BNP Paribas) senilai dana 39 juta euro. Selain kesulitan mendapatkan dokumen, lanjut Salim, pihaknya juga masih menunggu laporan BPKP yang masih belum tuntas menghitung nilai kerugian negara yang ditimbulkan dari kasus ini. Tetapi, pihaknya tetap yakin bahwa simpanan perusahaan Tommy Soeharto, Garnet Investment Limited (GIL), yang dibekukan BNP Paribas itu merupakan milik Indonesia.Kasus BPPC ini sendiri bermula dari pengucuran dana KLBI sebesar Rp 175 milyar yang diterima BPPC. Dana yang seharusnya digunakan untuk membeli cengkeh petani itu, dalam pelaksanaannya hanya 30 persen yang dipakai sesuai peruntukannya. Selebihnya untuk kepentingan pribadi tersangka dan sejumlah pihak lainnya.

Dalam kasus ini, hanya Tommy yang baru ditetapkan sebagai tersangka.Terkait gugatan intervensi pemerintah RI melalui Kejaksaan Agung itu, sudah diproses di Royal Court of Guernsey terhadap gugatan GIL atas asetnya yang diblokir BNP Paribas. Gugatan intervensi kejaksaan ini dikabulkan pengadilan setempat. Hal itu ditandai dengan dibekukannya aset perusahaan Tommy. Pemerintah RI yang diwakili Kejaksaan Agung dalam perkara ini menyewa pengacara setempat yakni Lloyd Strappini. BNP Paribas diwakili pengacaranya, Paren La Cruz. Sedangkan GIL menyewa tujuh penasihat hukum yang dikoordinatori Christoper Edwards. Sementara pihak Tommy Soeharto mengutus OC Kaligis sebagai pengacara pendamping bagi GIL. (kmb3)


* * *

Jawa Pos , 9 Februari 2008,

Kejaksaan Agung Atur Strategi Gugat Tommy

JAKARTA - Setelah masa berkabung wafatnya Presiden Soeharto berakhir, Tommy harus bersiap-siap kembali menghadapi persoalan hukum. Pangeran Cendana itu bakal menghadapi gugatan Kejaksaan Agung terhadap dugaan buyback (pembelian kembali) aset PT Timor yang berada dalam penguasaan negara.

Kejagung sebagai jaksa negara telah menyiapkan serangkaian strategi untuk menggugat pembelian bekas aset Tommy oleh PT Vista yang disinyalir didanai Grup Humpuss.

Menurut Jaksa Agung Muda Perdata dan Tata Usaha Negara (JAM Datun) Untung Udji Santoso, kejaksaan menyusun strategi khusus untuk mengajukan gugatan pembatalan transaksi tersebut. "Apa strategi tersebut, kami perlu membicarakan lagi dengan Menteri Keuangan selaku pemberi kuasa," ujar Untung saat ditemui di Gedung Kejagung, kemarin (8/2).

Menurut Untung, dalam strategi yang disusunnya, kejaksaan belum tentu mengajukan somasi, pembatalan sepihak, atau pembatalan lewat pengadilan atas transaksi pembelian aset PT Timor tersebut. "Tolong beri waktu dua minggu. Kami perlu mematangkan lagi," jelas mantan kepala Kejati (Kajati) DKI ini.

Untung mengakui, tim jaksa telah menerima surat kuasa khusus (SKK) dari Menkeu untuk mengajukan gugatan kasus PT Vista. "SKK tersebut turun dari jaksa agung sekitar lima hari lalu," jelas Untung.

Dari informasi koran ini, dalam strategi gugatan tersebut, para pihak yang digugat kejaksaan adalah PT Vista dan Grup Humpuss serta investor asing yang memfasilitasi pembelian aset PT Timor dari BPPN (Badan Penyehatan Perbankan Nasional). Mereka diasumsikan merekayasa pembelian aset PT Timor dengan harga lebih murah dari utang PT Timor yang ditanggung pemerintah.

Sebelumnya, piutang PT Timor yang sebelumnya dimiliki Tommy senilai Rp 4,576 triliun. Piutang tersebut lantas dibeli PT Vista melalui BPPN "hanya" seharga Rp 512 miliar. Baik KPK maupun Depkeu mencurigai transaksi tersebut sarat dengan benturan kepentingan. Maklum, belakangan dicurigai PT Vista mendapat aliran duit dari Grup Humpuss untuk membeli aset PT Timor.

Di tempat terpisah, Dirut PT Vista, Taufik Suryadarma, mengaku siap menghadapi gugatan tersebut. "Kalau ditanya siap, ya harus siap grak!!!" ujar Taufik saat dihubungi koran ini tadi malam (8/2). PT Vista bakal menindaklanjuti dengan menyiapkan tim pengacara.

Menurut Taufik, PT Vista sebenarnya tidak tahu apa-apa dalam transaksi PT Timor. PT Vista tak lebih sekadar perantara untuk menjembatani investor asal Singapura yang ingin menguasai aset perusahaan otomotif tersebut.

Ditanya apakah PT Vista mendapat instruksi khusus dari Grup Humpuss, Tommy, atau dari pengusaha tertentu, pria asal Malang ini menolak menjawab. "Kalau soal itu, saya nggak bisa komentar," ujar Taufik. Dia minta, agar semua pihak menunggu perkembangan lebih lanjut kasus yang dihadapinya tersebut. (agm)


* * *

Koran Tempo, 2 Januari 2008

Tommy Suharto tetap minta
ganti rugi Rp 10 triliun dari Bulog


JAKARTA -- Tergugat perkara perdata gugatan Bulog atas ruilslag PT Goro Batara Sakti, Tommy Soeharto, tetap meminta agar gedung Bulog dijadikan sita jaminan dalam gugatan balik pada perkara gugatan perdata Bulog di Pengadilan Negeri Jakarta Selatan.
"Kami tetap meminta sita jaminan itu," kata kuasa hukum Tommy, Kapitra Ampera, saat dihubungi Tempo dua hari lalu.


Padahal, menurut Pasal 50 huruf d Undang-Undang Nomor 1 Tahun 2004 tentang Perbendaharaan Negara, aset negara atau daerah tidak boleh disita. Namun, Kapitra justru mempertanyakan, kalau negara bisa mengklaim aset seseorang, mengapa warga negara juga tidak bisa melakukan hal serupa. "Kita coba, lagi pula semua tergantung putusan hakim," katanya.


Dalam gugatan perdata Bulog dengan PT Goro Batara Sakti, Tommy Soeharto, Ricardo Gelael, dan Beddu Amang, Tommy menggugat balik Bulog karena dinilai menjatuhkan reputasi, kredibilitas, dan nama baik Tommy sehingga mengganggu kepercayaan mitra bisnis dalam dan luar negeri.


Dalam gugatan balik itu, Tommy menuntut ganti rugi materiil Rp 9,259 triliun, imateriil Rp 1 triliun, dan uang paksa Rp 1 miliar serta meminta sita jaminan berupa kantor Bulog, yang terletak di Jalan Gatot Subroto, Jakarta Selatan; tanah Bulog di Kelapa Gading; dan tanah di Marunda, Jakarta Utara.


Sebelumnya, pengacara Bulog, Asfifudin, mengatakan kantor Bulog tidak bisa dimasukkan dalam aset yang dijadikan jaminan dalam gugatan balik tersebut. Sebab, kata dia, hingga kini kantor itu berstatus aktif sebagai kantor Bulog, yang merupakan aset negara. "Itu sesuai dengan peraturan," katanya.


Adapun Direktur Perdata Kejaksaan Agung Yoseph Suardi Sabda menyatakan pencantuman sita aset jaminan berupa aset pemerintah merupakan kesalahan. Selain itu, dalam gugatan balik Tommy tidak mencantumkan pasal yang dilanggar Bulog.
"Kesalahan tersebut membuat gugatan balik Tommy mentah," ujarnya saat dihubungi Tempo kemarin. RINI KUSTIANI | SANDY INDRA PRATAMA
* * *

Jawapos, 22 Des 2007,

Menkeu Kirim Kuasa Gugat Tommy

Utang Tommy kepada pemerintah Rp 4000.000.000.000 !!!

JAKARTA - Menteri Keuangan Sri Mulyani dan Jaksa Agung Hendarman Supandji ternyata sudah bertemu dan menyepakati rencana menggugat Tommy Soeharto atas dugaan pembelian aset PT Timor Putra Nasional (TPN) oleh PT Vista Bella Pratama (PT Vista). Hal itu diungkapkan Hendarman di Gedung Bundar, Kejagung, kemarin (21/12).Sebagai tindak lanjut, Menkeu telah mengirimkan surat kuasa khusus (SKK) ke jaksa agung (jakgung). "SKK-nya sudah diteken untuk arbitrase, tapi sampai sekarang belum diserahkan," ujar Hendarman.Selain di pengadilan, untuk tahap pertama, kasus PT Vista akan diselesaikan melalui jalur arbitrase alias perundingan di Badan Arbitrase Nasional Indonesia (BANI).

Lewat arbitrase, kata Hendarman, Kejagung ditugasi membatalkan perjanjian pembelian piutang PT Timor oleh PT Vista.Menurut Hendarman, selain SKK untuk beracara di arbitrase, pihaknya juga berharap Menkeu dapat mengirimkan lagi SKK untuk beracara di pengadilan. "Dengan dua SKK, kami dapat menempuh proses hukum di arbitrase maupun pengadilan," ujar mantan ketua Timtastipikor itu.Dia menyatakan, kejaksaan mencadangkan upaya hukum di pengadilan sebagai antisipasi kemungkinan gagalnya proses penyelesaian di arbitrase. "Kami menyiapkan dua langkah," tuturnya.Sebelumnya, Menkeu menyatakan penjualan aset Timor merugikan negara hingga Rp 4 triliun.

Pada Juni 2003, melalui Badan Penyehatan Perbankan Nasional (BPPN), Timor -termasuk Grup Humpuss milik Tommy Soeharto- menjual asetnya kepada PT Vista Rp 512 miliar. Padahal, aset Timor bernilai Rp 4,576 triliun sesuai dengan utangnya ke pemerintah.Belakangan diketahui bahwa Vista Bella memiliki kaitan dengan Grup Humpuss. Pasal 19 Perjanjian Jual Beli Piutang (PJBP) menyebutkan bahwa jika ada sengketa mengenai perjanjian itu, BPPN dan PT Vista sepakat untuk menyelesaikan lewat arbitrase. Dalam UU No 30 Tahun 1999 tentang Arbitrase, disebutkan bahwa bila dalam perjanjian terdapat klausul arbitrase, pengadilan tidak boleh mengadili. (agm/dwi)


* * *

Kasus uang haram Tommy Suharto :
Putar-putar Duit Timor

Mengingat pentingnya persoalan uang haram Tommy Suharto, yang meliputi jumlah triliunan Rupiah dan kelihatan makin berbelit-belit sekali urusannya, maka berikut ini disajikan laporan majalah Tempo edisi 23 Desember 2007. Dengan membaca tulisan ini, walaupun sepintas lalu dan hanya sepotong-sepotong saja, para pembaca dapat memperoleh gambaran betapa “hebatnya” dan luasnya jaring-jaringan “bisnis”-nya. “Bisnis” Tommy Suharto adalah -- pada intinya -- praktek-praktek yang meliputi : korupsi, kolusi, main suap, main gertak, main desak, main tipu, yang dilakukan dengan berbagai cara dan bentuk, dengan dibantu oleh para kakitangannya yang terdapat dimana-mana). Praktek-praktek “bisnis” kotor inilah yang memungkinkan Tommy Suharto menumpuk kekayaan yang begitu besar dalam waktu singkat.. Tentang berbagai macam kasus Tommy Suharto harap simak : “Kumpulan berita soal harta haram Tommy Suharto”.

A. Umar Said

=== ===

Tulisan dalam majalah Tempo 23 Desember 2007 tersebut adalah sebagai berikut :

“Bukti-bukti Tommy Soeharto di balik perusahaan pembeli aset Timor
semakin kuat. Transaksi Rp 4,5 triliun itu bisa dibatalkan.

DOKUMEN setebal 10 sentimeter itu dibentangkan di atas meja. Isinya
pelbagai berkas yang berhubungan dengan perjanjian jual-beli hak
tagih piutang pemerintah atas PT Timor Putra Nasional milik Hutomo
Mandala Putra. Di antaranya bukti transfer duit dari Timor ke PT
Vista Bella Pratama, pembeli hak tagih senilai Rp 4,5 triliun itu.

"Ini menunjukkan adanya hubungan antara Vista Bella dan Timor serta
PT Humpuss," kata Yoseph Suardi Sabda, Direktur Perdata Kejaksaan
Agung, Jumat pekan lalu. PT Humpuss adalah kelompok usaha milik
Hutomo alias Tommy Soeharto.

Bukti-bukti keterkaitan antara Tommy dan Vista Bella kini gencar
diburu penyidik Kejaksaan Agung dan Komisi Pemberantasan Korupsi.
Soalnya, hak tagih piutang Rp 4,5 triliun itu dijual dengan harga Rp
512 miliar saja. Pemerintah berharap bisa membatalkan perjanjian
jual-beli piutang antara Badan Penyehatan Perbankan Nasional dan
Vista Bella yang diteken pada 15 April 2003 ini.

Transfer duit dari Timor ke Vista itu dilakukan pada November 2003.
Buktinya adalah dua surat perintah bayar tertanggal 3 November dari
Timor kepada Bank Mandiri untuk dikirim ke Vista Bella. Ada pula
transfer dana dari Humpuss kepada Vista Bella yang dilakukan melalui
perusahaan lain, yaitu PT Mandala Buana Bhakti. Mandala Buana, juga
milik Tommy, berkantor di Gedung Granadi, Kuningan, Jakarta Selatan,
tempat sejumlah yayasan mantan presiden Soeharto berkantor.

Menurut Yoseph, pada 10 April 2003 Humpuss mengirim surat ke Bank
Negara Indonesia Cabang Menteng untuk mentransfer dana ke Mandala
Buana. Besarnya US$ 8 juta atau sekitar Rp 72 miliar. Surat itu
ditandatangani Direktur Utama Humpuss Rulyani Basyir.

Dua hari sebelum transfer itu, Mandala ternyata telah memindahkan
duit lain. Perusahaan itu memerintahkan Bank Niaga membayarkan Rp 53
miliar ke BPPN. Nah, yang jadi soal, pembayaran ini dilakukan untuk
kepentingan Vista Bella. "Jadi dibayar dulu oleh Mandala, baru
diganti," Yoseph menjelaskan.

Kepada Tempo, Taufik Surya Darma, Direktur Vista Bella, tak menampik
adanya aliran duit dari Timor ke perusahaannya. Namun, menurut dia,
duit itu tidak ada kaitannya dengan transaksi pembelian hak tagih
yang sudah selesai tujuh bulan sebelumnya. "Lagi pula duit itu
masuk ke rekening Vista Bella untuk diteruskan ke pihak lain,"
katanya (lihat "Nggak Mungkin Saya yang Main").

Menurut akta pembentukannya, Vista Bella dibentuk pada April 2002
oleh Mohammad Hartono Fauzan dan Nyonya Chatarina Widayanti. Di situ
tertulis, Vista dibuat sebagai usaha di bidang perdagangan,
pemborongan dan kontraktor umum, usaha real estate, industri mebel,
makanan dan minuman, serta peternakan dan pertanian.

Taufik membeli perusahaan itu pada 12 Maret 2003. Sebulan kemudian,
Vista Bella membeli hak tagih atas Timor. Transaksi ini meliputi
seluruh hak tagih, manfaat, serta kepentingan lainnya berdasarkan
perjanjian kredit dan dokumen jaminan atas nama Timor. Jumlah utang
tertunggak Timor, menurut akta, antara lain 4,75 juta franc Swiss,
25,43 juta mark Jerman, Rp 910 miliar, dan US$ 331 juta. Semua
setara dengan Rp 4,5 triliun.

Taufik mengakui perusahaannya hanya dipakai pemodal asing, yaitu
Amazonas Finance dan Wedingley Capital. Alasannya, persyaratan
investor asing untuk membeli aset di BPPN jauh lebih
rumit. "Menurut perhitungan Amazonas, waktunya nggak cukup jika
tanpa bantuan kami," ujarnya.

Tak aneh, Vista Bella kembali mengalihkan hak tagih itu kepada
Amazonas, dua bulan setelah transaksi. Yang agak aneh, belakangan
Amazonas menjual lagi hak tagih itu. Pembelinya Global Auto
Technology yang berkongsi dengan Humphrey International Limited
membentuk PT Auto Car Industri Komponen. Perusahaan inilah yang kini
mengoperasikan pabrik Timor di kawasan Cikampek, Jawa Barat, itu.

Auto Car sendiri berkantor di Hanurata Graha, gedung perkantoran
delapan lantai di Jalan Kebon Sirih, Jakarta Pusat. Di gedung yang
sama, ternyata berkantor sejumlah perusahaan dan organisasi milik
Tommy Soeharto dan Keluarga Cendana lainnya.

Menurut situs Internet perusahaan itu, Auto Car dipimpin Achmad Budi
Pramono sebagai direktur dan Nindito Sutarjadi sebagai komisaris.
Sumber Tempo menyebutkan Nindito dulu sering terlihat bersama-sama
dengan Oscar Gonzales, salah satu pemilik Amazonas Finance. Adapun
Oscar, yang berkewarganegaraan Venezuela, menurut sumber Tempo yang
mengetahui penyelidikan kasus ini, masuk ke Indonesia atas sponsor
PT Mandala Marmer, perusahaan milik Tommy.

Tempo belum berhasil memperoleh konfirmasi dari Nindito tentang
kedekatannya dengan Oscar. Meski terdengar nada tunggu, telepon
selulernya tak kunjung diangkat. Pesan pendek yang dikirim pun tak
dibalas. Tempo yang mengunjungi alamat kantor Auto Car di Hanurata
Graha juga tak berhasil menemui Nindito.

Komisi Pemberantasan Korupsi dan kejaksaan kini harus menyusun
mozaik jejak-jejak Tommy Soeharto pada transaksi pembelian hak tagih
atas PT Timor ini. Jika itu berhasil, transaksi senilai Rp 4,5
triliun tersebut otomatis dibatalkan. Menurut sumber, bukti yang
dikumpulkan Komisi Pemberantasan Korupsi sangat kuat. "Paling hanya
perlu melegalisasi sejumlah dokumen, yang kini masih berbentuk
fotokopi," tuturnya.

Komisi Pemberantasan Korupsi telah memeriksa Taufik Surya Darma dan
Oscar Gonzales. Oscar pun telah menunjuk pengacara dari Kantor Hotma
Sitompoel and Associates. "Dia ditangani oleh pengacara Andi
Simangunsong," kata Hotma. Kejaksaan kabarnya juga telah meminta
keterangan Alfian Sanjaya, yang dalam akta perubahan Vista Bella
disebut sebagai komisaris.

Hubungan antara Vista Bella dan Tommy memang diharamkan. Setidaknya
ada empat klausul dalam perjanjian itu yang melarang adanya
keterkaitan penjual dan pembeli. Misalnya, pada pasal 12.4a
disebutkan pembeli tidak memiliki kepentingan ekonomi secara
langsung, hubungan asosiasi, atau hubungan lainnya (termasuk
hubungan pribadi atau keluarga) dengan peminjam (Timor), karyawan,
direksi, komisaris, atau pemegang sahamnya.

Yoseph menjelaskan, jika transaksi dibatalkan, Timor diwajibkan
membayar utang-utangnya kepada sejumlah bank dulu. Pemerintah akan
terus menagih utang itu sesuai dengan surat sanggup Tommy Soeharto
kepada Bank Bumi Daya yang diteken pada 21 September 1998. Tommy
sebagai Komisaris Utama Timor dan Mujiono sebagai direktur utama
menyatakan sanggup membayar US$ 260 juta atau Rp 2 triliun lebih
sebagai jaminan pribadi.

Kendati begitu, pertarungan antara pemerintah dan Tommy tampaknya
tak akan sebentar. O.C. Kaligis, pengacara putra bungsu mantan
presiden Soeharto itu, menyatakan kliennya siap menghadapi tudingan
pemerintah. Kata Kaligis garang, "Pengalihan Timor ke BPPN lalu ke
Vista Bella itu tidak dilakukan di bawah tangan. Kenapa pemerintah
baru mengungkitnya sekarang?"

Budi Setyarso, Widi Nugroho, Rini Kustiani

* * *

Tempo Interaktif, 17 Desember 2007

Keberatan Ditolak Pengadilan,

Tommy Akan Gugat Balik Bulog

TEMPO Interaktif, Jakarta:Pengadilan Negeri Jakarta Selatan
Menyatakan berwenang memeriksa perkara gugatan perdata Bulog terhadap PT Goro
Batara Sakti, Tommy Soeharto, Ricardo Gelael dan Beddu Amang.

"Menolak eksepsi tergugat satu (PT Goro) dan menyatakan Pengadilan
Negeri Jakarta Selatan berwenang memeriksa perkara ini," kata Ketua
Majelis Hakim Haswandi saat membaca putusan sela di PN Jaksel, Senin
(17/12).

Dalam perkara ini, Bulog menggugat PT Goro Batara Sakti, Hutomo
Mandala Putra atau Tommy Soeharto (Komisaris PT Goro), Ricardo Gelael
(Direktur Goro) dan Beddu Amang (mantan Kepala Bulog). Gugatan ini
diajukan karena Bulog merasa dirugikan dalam kasus tukar guling
(ruislag) gudang Bulog seluas 150 hektar di Marunda, Jakarta Utara
dengan PT Goro.

Haswandi mengatakan, eksepsi atau keberatan PT Goro yang menyatakan
PN Jaksel tidak berwenang karena gugatan ini masuk dalam hukum kepalitan
dan harus masuk ke Pengadilan Niaga ditolak majelis.

"Karena gugatan penggugat didasarkan pada perbuatan melawan hukum
yang diduga dilakukan tergugat," kata Haswandi. Perbuatan melawan hukum,
lanjut dia, tidak masuk dalam ruang lingkup hukum kepailitan yang
didasarkan pada Undang-Undang Nomor 37 tahun 2004 tentang Kepailitan,
melainkan berdasarkan Kitab Undang-Undang Hukum Perdata.

Mengenai keadaan PT Goro yang telah dinyatakan pailit oleh Pengadilan
Niaga Jakarta Pusat pada 26 Juli 2006, majelis menilai status Goro
tersebut masih berkaitan dengan hukum kepailitan dan oleh karenanya
harus ditolak. "(status pailit) tidak beralasan menurut hukum dan
ditolak," katanya.

Setelah membacakan putusan, kuasa hukum Goro, Nuryanto mengatakan
Akan menyatakan banding karena segala perkara yang berkaitan dengan Goro
telah diperiksa oleh pengadilan niaga yang akhirnya menyatakan Goro
pailit. "Alasannya sama seperti eksepsi kemarin," kata Nuryanto
setelah sidang.

Sementara dalam persidangan, kuasa hukum Tommy, Kapitra Ampera
mengatakan akan mengajukan eksepsi dan gugatan balik kepada Bulog.
"Karena kami sangat dirugikan dengan adanya gugatan ini," kata
Kapitra usai sidang.

Menurut Kapitra, gugatan ini telah merusak kredibilitas kliennya.
"Kredibilitas dia terganggu," ujarnya. Perbuatan melawan hukum itu,
lanjut dia, sebenarnya dilakukan oleh Bulog kepada Tommy. "Gugatan
ini menyerang dan mematikan secara personal," katanya.

Oleh karenanya, pada sidang berikutnya yang akan berlangsung pada 27
Desember mendatang, pihaknya akan mengajukan keberatan (eksepsi) dan
gugatan balik terhadap Bulog dengan nilai gugatan moril dan materiil
sebesar Rp 1 triliun.

Saat ditanya, jika eksepsi Goro diterima majelis, maka putusan sela
ini akan digunakan untuk membatalkan pembekuan dana Tommy di BNP
Paribas Guernsay, Kapitra mengatakan, "jangan berandai-andai. yang
jelas ditolak kan (eksepsinya)."

Terpisah, jaksa pengacara negara Yoseph Suardi Sabda mengatakan,
putusan sela yang dikeluarkan majelis hakim sesuai dengan harapan
kejaksaan.

Mengenai gugatan balik yang akan diajukan kuasa hukum Tommy, Yoseph
mengatakan, "silahkan saja." Alasannya, mengajukan gugatan di
pengadilan buka merupakan perbuatan melawan hukum.

Yoseph menambahkan, dengan dilanjutkannya pemeriksaan perkara ini,
maka pembekuan dana Tommy di Guernsay kemungkinan akan diperpanjang.
"Karena jelas dia ada perkara di dalam negeri," ujar Yoseph saat
dihubungi Tempo.

Jika putusan sela Goro menyatakan pemeriksaan perkara dilanjutkan,
maka dana Tommy sebesar 36 juta euro atau sekitar Rp 421 miliar di
BNP Paribas tetap dibekukan. Namun, jika putusan sela perkara Goro
menyatakan pemeriksaan tidak bisa dilanjutkan, artinya dana Tommy
yang dibekukan oleh pengadilan Guernsay pada Mei lalu bisa dicairkan.

* * *

Jawapos, 18 Des 2007,

Keberatan Ditolak, Tommy Gugat Balik

Gugatan Ruilslag Bulog – Goro

JAKARTA - Tommy Soeharto semakin sulit lepas dari belitan hukum.
Selain bakal menghadapi gugatan dugaan buyback PT Timor, posisi
pangeran Cendana itu terpojok dalam gugatan kasus ruilslag (tukar
guling) Bulog dengan PT Goro Batara Sakti (GBS).

Dalam persidangan ruilslag Bulog itu kemarin (17/12), PN Jakarta
Selatan menolak eksepsi (keberatan) yang diajukan Tommy atas gugatan
Perum Bulog melalui Kejaksaan Agung.

Majelis hakim yang diketuai Haswandi memutus PN Jakarta Selatan
berwenang melanjutkan pemeriksaan perkara. "Kami menolak seluruh
keberatan yang diajukan tergugat PT Goro," kata Haswandi dalam
putusan sela pada persidangan kemarin.

Dalam kasus ruilslag Bulog tersebut, kejaksaan menjadikan PT Goro
sebagai tergugat I, Tommy (komisaris utama PT Goro) selaku tergugat
II, Ricardo Gelael (Dirut PT Goro) sebagai tergugat III, dan mantan
Kabulog Beddu Amang selaku tergugat IV.

Haswandi menjelaskan, materi gugatan terkait dengan perbuatan
melawan hukum yang diatur dalam pasal 1365 KUH Perdata. Terutama
perbuatan semasa terjadinya ruilslag antara PT Goro dengan Bulog
pada 1995.

Karena itu, kata dia, majelis menolak anggapan para tergugat bahwa
gugatan terkait dengan perkara kepailitan menjadi wewenang
pengadilan niaga. "Kasus itu tetap menjadi wewenang kami, pengadilan
negeri," tegas Haswandi.

Sebelumnya, PT Goro melalui pengacaranya, Nuryanto, menyatakan bahwa
kliennya telah berstatus pailit, sehingga urusan hukumnya harus
diselesaikan melalui jalur pengadilan niaga. Hal itu sesuai pasal 2
ayat 1 UU No 37/2004 tentang Kepailitan dan Penundaan Kewajiban
Pembayaran Utang (PKPU).

Dalam putusannya, majelis menolak keberatan PT Goro tentang tuntutan
ganti rugi yang juga harus diselesaikan di pengadilan niaga. "Ini
sudah memasuki pokok perkara, sehingga keberatan harus ditolak,"
ujar Haswandi.

Selanjutnya, dengan penolakan eksepsi tergugat I, sidang berlanjut
dengan mendengarkan jawaban tergugat II-IV pada 27 Desember 2007.

Di tempat yang sama, Nuryanto menegaskan bahwa kliennya akan
mengajukan banding atas putusan tersebut. "Kami tetap bersikukuh
pada pendapat kami," tegasnya.

Pengacara Tommy, Kapitra Ampera, mengungkapkan bahwa kliennya akan
merealisasikan gugatan balik (rekompensi) terhadap Perum Bulog.
Gugatan didaftarkan bersamaan dengan persidangan pembacaan eksepsi
pada 27 Desember 2007. "Atas kasus ini, posisi klien saya terganggu,
baik dari bisnis maupun harga diri. Karena itu, nilai gugatannya
mencapai Rp 1 triliun," ungkap Kapitra yang mewakili pengacara Elza
Syarief.(agm/tof)

* * *

Selasa, 18 Desember 2007

Tommy Merasa Diserang Pribadi

Gugat Bulog Rp 1 Triliun


JAKARTA - Merasa bisnisnya terganggu dan menyerang secara
pribadi, Hutomo Mandala Putra alias Tommy Soeharto akan menggugat
balik Badan Urusan Logistik (Bulog) sebesar Rp 1 triliun.

Pasalnya, gugatan perdata Bulog melalui Jaksa Pengacara Negara
(JPN) kepada Tommy terkait tukar guling antara aset Bulog dan PT
Goro Batara Sakti (GBS) dinilai merugikan putra bungsu mantan
Presiden Soeharto itu.

''Kita akan ajukan gugatan balik Bulog. Nilai nominalnya
minimal Rp 1 triliun. Sebab kita dirugikan dalam gugatan perdata
ini,'' ujar kuasa hukum Tommy, Kapitra Ampera usai sidang di
Pengadilan Negeri Jakarta Selatan (PN Jaksel), Senin (17/12).

Kapitra mengatakan gugatan balik itu dilayangkan karena
gugatan Bulog tidak hanya mengganggu bisnis, namun menyerang
kliennya secara pribadi.

"Selain itu, dalam perkara ruilslag itu merekalah yang
sebenarnya melakukan perbuatan melawan hukum," ujarnya.

Dalam kasus ini, Bulog menggugat Tommy Soeharto (tergugat I)
senilai Rp 550 miliar dalam perkara ruislag GBS dengan Bulog tahun
1995. Tergugat lainnya GBS (tergugat I), Ricardo Gelael (tergugat
II), dan mantan Kabulog Beddu Amang (tergugat IV).

Tolak Eksepsi

Dalam persidangan itu, majelis hakim menolak eksepsi PT Goro
yang mempertanyakan kewenangan pengadilan negeri dalam menangani
gugatan Bulog. Sebab GBS telah dinyatakan pailit oleh Pengadilan
Niaga.

"Menolak eksepsi tergugat I. Menyatakan Pengadilan Negeri
Jakarta Selatan berwenang memeriksa perkara ini yakni perkara
bernomor 1228/PDTG/PN/2007/ PN Jaksel tanggal 22 Agustus tahun 2007.
Memerin- tahkan para pihak untuk melanjutkan proses perkara ini,"
ujar Ketua Majelis Hakim Haswandi.

Majelis hakim memutuskan persidangan berikutnya dilaksanakan
Kamis (27/12), dengan agenda keberatan tergugat atas gugatan
penggugat.

''Karena eksepsi ditolak, kami meminta waktu untuk
mempersiapkan jawaban dan gugatan balik kepada penggugat,'' ujar
Kapitra.

Sebelumnya, dalam eksepsi pengacara kurator Goro Nuryanto
mempertanyakan kompetensi absolut PN Jaksel dalam mena-ngani gugatan
Bulog. PN dinilai tidak berwenang karena tuntutan hukum Bulog dalam
ruang lingkup kepailitan, padahal PT Goro telah dinyatakan pailit
oleh Pengadilan Niaga.

Menurut Haswandi, Bulog mendasarkan gugatannya pada perbuatan
melawan hukum tergugat dalam ruislag tahun 1995 lalu. Sesuai UU No
37/2004 tentang Kepailitan dan Penundaan Kewajiban Pembayaran Hutan
(PKPU) gugatan itu bisa ditujukan ke pengadilan negeri.

"Memang kerugian PT Goro Batara Sakti telah diperiksa oleh
Pengadilan Niaga, sehingga pengadilan negeri tidak berwenang. Tapi
majelis hakim berpendapat itu telah memasuki pokok perkara,
karenanya alasan eksepsi harus ditolak sepenuhnya," ujar Haswandi.
(J21,dtc-48)
* * *

Tempo Interaktif,, 18 Desember 2007

Soal Tommy: Gugatan Vista Bela diselesaikan Melalui Arbitrase


TEMPO Interaktif, Jakarta:Gugatan Menteri Keuangan terhadap PT Vista
Bela Pratama atas jual beli aset PT Timor Putra Nasional
diselesaikan melalui arbitrase.

"Dalam perjanjian jual beli piutang ada klausul untuk diselesaikan
melalui arbitrase," kata Direktur Perdata pada Jaksa Agung Muda
Perdata dan Tata Usaha Negara Yoseph Suardi Sabda di Pengadilan
Negeri Jakarta Selatan, Selasa (18/12).

Dia menjelaskan, pasal 19 Perjanjian Jual Beli Piutang (PJBP)
menyebutkan jika ada sengketa mengenai perjanjian tersebut, Badan
Penyehatan Perbankan Naasional (BPPN) dan Vista Bela sepakat untuk
diselesaikan di arbitrase. "Kita harus ikuti itu," kata Yoseph.

Apalagi dalam Undang-Undang Nomor 30 tahun 1999 tentang Arbitrase
menyebutkan apabila dalam perjanjian terdapat klausul arbitrase maka
perngadilan tidak boleh mengadili.

Saat ditanya, apakah artinya kejaksaan tidak bisa mengajukan gugatan
perdata, Yoseph mengatakan, kejaksaan tetap bisa menggugat
perdata. "Karena gugatan perdata boleh diajukan di luar pengadilan,
contohnya arbitrase ini," jelasnya.

Meskipun kejaksaan belum memperoleh Surat Kuasa Khusus (SKK) dari
menteri keuangan, namun menurut Yoseph, rencananya kejaksaan akan
menggugat Vista Bela karena telah melanggar pasal 129 perjanjian
jual beli piutang. Dalam pasal itu, kata Yoseph, melarang semua
hubungan, baik hubungan ekonomi maupun keuangan baik langsung maupun
tidak langsung antara perusahaan pembeli piutang dengan perusahaan
yang memiliki utang.

"Itu sudah mereka (Vista Bela) langgar, berarti (perjanjian) batal,"
katanya. Jika perjanjian jual beli piutang itu dibatalkan maka, kata
Yoseph, semua yang sudah dibayarkan tidak boleh ditarik kembali.

Saat ditanya apakah kejaksaan akan mengajukan gugatan kepada
Humpuss, Yoseph mengatakan, kejaksaan hanya akan mengajukan gugatan
kepada Vista Bela terlebih dahulu karena dia adalah pihak yang
bertanggung jawab pada transaksi itu. "Kita akan gugat Vista Bela
dulu, tidak sampai ke Humpuss," jelasnya.

Mengenai keberadaan Vista Bela saat ini, Yoseph memperkirakan
perusahaan itu sudah tidak beroperasi lagi. "Saya kira tidak
(beroperasi lagi)," katanya. Karena, dia menduga, satu-satunya
kegiatan yang dilakukan Vista Bela adalah membeli piutang.

Dalam akta pemilikan Vista Bela yang dimiliki kejaksaan menyebutkan
Vista Bela adalah perusahaan yang bergerak di bidang perdagangan
umum. Saat perjanjian jual beli dilakukan, yang menjadi pemilik
Vista Bela adalah Taufik Surya Darma dan Alfian Sanjaya.

Selain itu, kejaksaan juga memiliki bukti lain yang menerangkan ada
hubungan antara Vista Bela dengan Timor dan Humpuss. Bukti itu
berupa salinan transfer dana kepada Vista Bela baik dari Timor
maupun Humpuss.

Seperti diberitakan, Menteri Keuangan Sri Mulyani menyatakan
penjualan aset PT Timor ternyata merugikan negara hingga Rp 4
triliun. Pada Juni 2003, melalui Badan Penyehatan Perbankan Nasional
(BPPN), PT. Timor yang termasuk dalam grup Humpuss milik Tommy
Soeharto menjual asetnya kepada Vista Bella Pratama sebesar Rp 512
miliar, padahal aset Timor sebenarnya bernilai Rp 4,576 triliun.
Belakangan diketahui bahwa Vista Bella masih memiliki kaitan dengan
grup Humpuss. Rini Kustiani
* * *

Kompas, 15 Desember 2007

Transfer Dana Tommy Langsung

dan Tak Langsung

Kejaksaan Agung semakin yakin hubungan jual-beli aset utang PT Timor
Putra Nasional antara Badan Penyehatan Perbankan Nasional dan PT
Vista Bella Pratama harus batal. Salah satu larangan dalam pembelian
aset suatu perusahaan apabila pembeli memiliki afiliasi dengan
pemilik aset yang dibeli.

Direktur Perdata pada Bagian Perdata dan Tata Usaha Negara Kejaksaan
Agung Yoseph Suardi Sabda menyampaikan, banyak bukti yang menyatakan
PT Vista Bella Pratama, sebagai pembeli aset PT Timor Putra Nasional
(TPN), memiliki hubungan dengan PT TPN maupun Grup Humpuss. "Banyak
bukti, kebanyakan transfer uang. Ada yang dari TPN, ada dari
Humpuss," kata Yoseph, Jumat (14/12), sambil menunjukkan tumpukan
dokumen setebal 10 sentimeter.

Ada 33 dokumen, di antaranya berupa fotokopian, yang diterima
Kejaksaan Agung dari Menteri Keuangan. Salah satunya berupa surat
tanggal 10 April 2003 di atas kertas berkop surat Humpuss yang
ditandatangani Direktur Humpuss Bennyman Saus. Surat yang ditujukan
kepada Gaguk Hartadi, Kepala PT BNI Kantor Cabang Menteng, Jakarta,
itu perihal mendebet dana dari rekening giro Humpuss sebesar 8,327
juta dollar AS ke rekening PT Manggala Buana Bhakti di Bank Niaga
Cabang Gajah Mada Jakarta.

Dokumen lain berupa surat dari PT Manggala Buana Bhakti kepada Bank
Niaga untuk mentransfer dana Rp 53 miliar ke Badan Penyehatan
Perbankan Nasional. Dalam surat 8 April 2003 itu disebutkan, transfer
dana guna kepentingan PT Vista Bella Pratama.

"Bukti aliran uang, baik langsung maupun tidak langsung ini lebih
dari
Rp 100 miliar. Hampir Rp 200 miliar. Ada Rp 1 miliar pun, berarti ada
hubungan PT Vista Bella dengan Humpuss dan Timor, toh?" ungkap
Yoseph.

Yoseph menyebutkan, ada juga jaminan pribadi tertanggal 21 September
1998 yang ditandatangani Hutomo Mandala Putra selaku komisaris utama
dan Mujiono selaku Direktur Utama PT TPN. Surat tersebut menyebutkan
jaminan kesediaan membayar 260,112 juta dollar AS ke Bank Bumi Daya
apabila PT TPN tak bisa membayar utangnya ke bank itu. Padahal, utang
PT TPN sebesar Rp 4,576 triliun.

Dengan demikian, ujar Yoseph, langkah pertama adalah menuntut
pembatalan perjanjian antara PT Vista Bella Pratama dan Badan
Penyehatan Perbankan Nasional. Setelah perjanjian batal, maka utang
kembali menjadi kewajiban PT TPN. (IDR)
