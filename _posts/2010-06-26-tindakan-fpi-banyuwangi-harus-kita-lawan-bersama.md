---
layout: post
title: Tindakan FPI Banyuwangi harus kita lawan bersama
date: 2010-06-26
---

Berikut di bawah ini disajikan pernyataan (statement) Dr Ribka Tjiptaning dan berita Antara tentang tindakan FPI (Front Pembela Islam) Banyuwangi yang dengan menggunakan kekerasan telah membubarkan pertemuan antara  anggota-anggota Komisi IX DPR (bidang Kesehatan dan Tenaga Kerja) dengan masyarakat, dengan alasan bahwa pertemuan itu adalah suatu kegiatan berselubung  untuk menumbuhkan semangat komunisme lagi karena banyak peserta dari luar Kabupaten Banyuwangi yang datang, Menurut Ketua FPI Banyuwangi, pertemuan itu merupakan acara temu kangen bekas anggota PKI dan keturunannya, sehingga pertemuan tersebut harus dibubarkan.

Untuk itu, lanjut dia, FPI bersama organisasi masyarakat Islam di Banyuwangi membubarkan acara tersebut untuk menjaga kondusivitas keamanan di kabupaten paling timur Pulau Jawa itu.
"Kami mengantisipasi tumbuhnya bibit PKI baru karena gerakan PKI pada tahun 1965 berawal dari Kabupaten Banyuwangi," katanya menambahkan.

Mengingat seriusnya tindakan destruktif FPI Banyuwangi dalam peristiwa ini bagi persatuan bangsa, yang bertentangan dengan Pancasila dan  Bhinneka Tunggal Ika seperti yang selalu dianjurkan oleh Bung Karno dan Gus Dur, maka  akan disajikan sebuah tulisan tersendiri mengenai berbagai hal yang berkaitan dengan peristiwa ini.

Sebab, peristiwa FPI Banyuwangi ini sebenarnya mempunyai dimensi yang tidak kecil, dan yang ada hubungannya juga dengan politik dan praktek-praktek Orde Baru, yang anti-Bung Karno dan anti-komunis.Pernyataan Ketua FPI Banyuwangi bahwa pertemuan itu merupakan acara temu kangen bekas anggota PKI dan keturunannya, sehingga pertemuan tersebut harus dibubarkan, sepenuhnya mencerminkan satunya atau persamaan antara pandangan FPI dengan  rejim militer Suharto mengenai  masalah ini.

Tindakan FPI Banyuwangi yang destruktif bagi persatuan bangsa dan karenanya juga mencemarkan nama Islam ini perlu kita lawan bersama dengan berbagai cara dan jalan, demi kebaikan bangsa beserta anak cucu kita di kemudian hari. Kita tidak boleh membiarkan FPI terus-menerus menyebarkan racun dan merusak sendi-sendi demokrasi, dan menghancurkan dasar-dasar Republik Indonesia, yang telah dibangun dengan susah-payah oleh para perintis kemerdekaan.

Paris, 26 Juni 2010

Umar Said


= = ==   = = = ==





 Statement Dr. Ribka Tjiotaning :



TOLAK POLITIK ANTI DEMOKRASI,
TOLAK POLITIK DISKRIMINATIF

 Pada tanggal 21 Juni sampai dengan 23 Juni 2010, Komisi IX yang membidangi
Kesehatan dan Tenaga Kerja melakukan Kuker (Kunjungan Kerja) ke Propinsi
Jawa Timur. Rombongan Komisi IX DPR RI tersebut dipimpin langsung oleh Ketua Komisi,

dr Ribka Tjiptaning.

 Kuker tersebut bertujuan memantau langsung pelayanan kesehatan dan
kebijakan ketenagakerjaan berbagai kota di Jawa Timur. Sekaligus ingin
menghimpun secara langsung aspirasi dan masukan masyarakat.

 Pada tanggal 24 Juni seharusnya jadwal Kuker sudah selesai, tetapi banyak
elemen masyarakat berbagai kota di Jatim ingin bertemu dengan Ketua Komisi
IX DPR RI. Selama ini Ketua Komisi IX menerapkan kebijakan yang tidak
birokratis kepada elemen masyarakat yang berkeinginan menyampaikan
aspirasinya. Walau sudah selesai jadwal resmi, Ketua Komisi dr Ribka
Tjiptaning beserta Rieke Dyah Pitaloka dan Nursuhud (semuanya anggota Fraksi

PDIP) mau menerima undangan tersebut.

 Mereka bertiga tanggal 23 berkunjung ke Pondok Pesantren Al Qodiri 1, yang
diasuh KH Ach Muzakki Syah. Kunjungan rombongan ini diterima dan disambut
meriah oleh ribuan santri. Pada tanggal 24 Juni kami bertiga berencana
bertemu dengan PPNI (Persatuan Perawat Indonesia) dan IBI (Ikatan Bidan
Indonesia) pada pukul 13.00 WIB di Banyuwangi. Pada pukul 10.00 kami
bertemu  terlebih dahulu dengan masyarakat di satu rumah makan di Kelurahan Pakis,

Kabupaten Banyuwangi, Jawa Timur. Intiya Ketua Komisi IX memberi materi tentang

hak masyarakat untuk memperoleh kesehatan secara gratis, dan juga sosialisasi

RUU BPJS yang sedang dibahas di DPR.

 Acara tersebut dibubarkan secara paksa oleh Ormas Islam  : Front Pembela
Islam Banyuwangi, Jawa Timur bersama Forum Umat Beragama, dan LSM Gerak.
Polisi yang berada di sana justru turut membubarkan seperti permintaan dan
tututan ormas tersebut. Mereka menuduh acara tersebut adalah pertemuan kader komunis.

 Atas peristiwa ini, kami menyatakan sikap :
1. Bahwa yang dilakukan Front Pembela Islam Banyuwangi, Jawa Timur bersama
Forum Umat Beragama, dan LSM Gerak merupakan tindakan anti demokrasi dan
melanggar HAM.

 2. Hapuskan sikap politik diskriminatif. Meniadakan satu kelompok dalam
realita kehidupan berbangsa dan bernegara, seperti kebijakan yang
diskriminatif terhadap korban ’65 merupakan tindakan yang tidak menghargai
pluralisme, tidak toleran, dan tidak berbudaya.

 3. Aparat yang turut membubarkan acara tersebut cermin kegagalan
pemerintahan SBY dalam mereformasi tubuh Polri.

 Dengan begitu, saya akan melaporkan tindakan pelanggaran HAM tersebut ke
Komnasham pada Senin tanggal 28 Juni 2010.

 Jakarta, 25 Juni 2010

 Dr Ribka Tjiptaning

* * *



FPI Bubarkan Sosialisasi Kesehatan Komisi IX DPR

Kamis, 24 Juni 2010

Banyuwangi (ANTARA News) - Front Pembela Islam (FPI) bersama Forum Banyuwangi Cinta Damai dan LSM Gerak membubarkan acara sosialisasi kesehatan gratis yang digelar Komisi IX DPR di salah satu rumah makan di Kelurahan Pakis, Kabupaten Banyuwangi, Jawa Timur, Kamis.

"Ini ada komunitas anggota PKI (Partai Komunis Indonesia). Kenapa ada di sini?" kata Ketua FPI Banyuwangi, Aman Faturahman, kepada sejumlah peserta pertemuan yang terkejut melihat kehadiran anggota FPI itu.

Acara sosialisasi kesehatan gratis itu dihadiri Ketua Komisi IX DPR, dr. Ribka Tjiptaning Proletariati dan anggota Komisi IX, Rieke Dyah Ayu Pitaloka.

Melihat suasana yang semakin memanas, panitia segera mengevakuasi Ribka dan Rieke ke kantor DPC Partai Demokrasi Indonesia Perjuangan (PDIP) di Jalan Jaksa Agung Suprapto, Banyuwangi.

Menurut Ketua FPI Banyuwangi, pertemuan itu merupakan acara temu kangen bekas anggota PKI dan keturunannya, sehingga pertemuan tersebut harus dibubarkan.

"Sosialisasi kesehatan gratis dari Komisi IX hanya sebagai kedok. Saya curiga acara itu merupakan kegiatan terselubung untuk menumbuhkan semangat komunisme lagi karena banyak peserta dari luar Kabupaten Banyuwangi yang datang," kata Aman.

Untuk itu, lanjut dia, FPI bersama organisasi masyarakat Islam di Banyuwangi membubarkan acara tersebut untuk menjaga kondusivitas keamanan di kabupaten paling timur Pulau Jawa itu.

"Kami mengantisipasi tumbuhnya bibit PKI baru karena gerakan PKI pada tahun 1965 berawal dari Kabupaten Banyuwangi," katanya menambahkan.

Sementara itu, Ribka Tjiptaning mengaku kecewa dengan sikap FPI yang membubarkan secara paksa acara sosialisasi kesehatan gratis Komisi IX DPR. Padahal, menurut dia, sosialisasi tersebut sangat diperlukan oleh masyarakat di daerah.

"Kami tidak melakukan temu kangen bekas anggota atau keturunan PKI di Banyuwangi. Acara kami ini murni tugas Komisi IX DPR tentang sosialisasi pentingnya penyediaan fasilitas kesehatan gratis di daerah," katanya.

Penulis buku berjudul "Aku Bangga Jadi Anak PKI" itu pada 2002 mengaku sudah terbiasa mengalami intimidasi seperti itu.

"Ini menunjukkan bahwa negara kita belum demokratis sehingga orang lain masih berpikir awam tentang latar belakang saya," katanya.

Sementara itu, Rieke menambahkan, kegiatan sosialisasi kesehatan gratis tersebut merupakan kegiatan umum dan bisa dihadiri siapa saja, termasuk bekas anggota atau keturunan PKI.

"Saya menyayangkan sikap yang dilakukan FPI karena bekas anggota atau keturunan PKI juga warga negara Indonesia," katanya.

Wakil Ketua DPC PDIP Kabupaten Banyuwangi, Muhammad Abas, mengatakan undangan yang hadir dalam kegiatan sosialisasi kesehatan gratis tersebut berasal dari berbagai elemen, namun beberapa peserta yang hadir merupakan keturunan keluarga bekas anggota PKI.

"Memang benar, ada beberapa peserta yang keturunan keluarga bekas anggota PKI," kata Abas yang juga menjadi panitia dalam kegiatan tersebut.(*)
