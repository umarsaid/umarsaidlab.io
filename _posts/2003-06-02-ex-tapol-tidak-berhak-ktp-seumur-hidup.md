---
layout: post
title: Ex-tapol tidak berhak KTP Seumur Hidup
date: 2003-06-02
---

Mohon perhatian para pembaca sekalian terhadap berita tentang “Bekas Tahanan Politik Tidak Berhak KTP Seumur Hidup” yang disiarkan oleh Harian Sinar Indonesia Baru (30 Mei 2003). Sebab berita yang singkat ini menunjukkan bahwa sisa-sisa buruk dari sistem politik rezim militer Orde Baru masih diteruskan sampai sekarang. Walaupun Suharto sudah “dilengserkan” lima tahun yang lalu oleh gerakan pemuda/mahasiswa dan tekanan inernasional, tetapi masih banyak praktek-praktek rezimnya yang terus mendatangkan - sampai sekarang ! - , penderitaan bagi para korban Orde Baru beserta keluarga mereka.
Berita tersebut lengkapnya berbunyi sebagai berikut :

“Nona Nani Nurani, warga Kelurahan Tugu Utara, Kec Koja Jakarta Utara, yang mem-PTUN-kan camatnya ternyata seorang Eks Tapol Golongan C, yang tidak berhak memperoleh KTP seumur hidup.

"SK Mendagri No.24 tahun 1991, antara lain berisi bahwa mereka yang berstatus eks Tapol tidak berhak memperoleh KTP seumur hidup," kata Camat Koja, Usman, Rabu (28/5).

Karena itulah Sudin Kependudukan dan Catatan Sipil Jakut tidak memberikan KTP seumur hidup kepada yang bersangkutan. Tetapi KTP biasa yang harus diperpanjang lima tahun tetap diberikan. Soal di-PTUN-kannya camat, Usman mengatakan itu hak yang bersangkutan. Tentunya semua langkah yang dilakukan telah melalui prosedur yang sebenarnya.

"Pihak kecamatan sudah melakukan rapat koordinasi dengan bagian hukum Walikotamadya Jakut dan tidak ada masalah. Kami akan mengikuti semua aturan main yang ada," tambah Usman.

Sebelumnya Minggu (25/5) memberitakan, Camat Koja digugat oleh warganya Nona Nani Nurani, 62 karena tidak memberikan KTP seumur hidup. Alasannya sesuai Perda DKI No 1 tahun 1996, warga yang telah berusia 60 tahun keatas berhak mendapatkan KTP seumur hidup. (kutipan selesai)

## Kejahatan Yang Dilakukan Negara

Kesimpulan apa yang bisa ditarik dari berita ini? Banyak. Dan serius pula. Berita ini merupakan salah satu di antara banyak bukti bahwa perjuangan untuk melawan sisa-sisa sistem politik yang tidak demokratis Orde Baru harus kita teruskan, bahkan lebih kita galakkan. Sebab, sekarang makin jelas bagi banyak orang, bahwa orang-orang yang bermental Orde Baru masih banyak menguasai bidang eksekutif, leglislatif dan judikatif di negeri kita.

Nani Nurani, 62 tahun, adalah bekas eks-tapol golongan C. Apa artinya ini ? Bahwa ia adalah, seperti halnya kebanyakan eks-tapol golongan C lainnya - yang jumlahnya ratusan ribu – pernah ditahan oleh rezim militer Orde Baru. Ia meringkuk dalam tahanan, entah berapa lama. Tetapi, seperti juga eks-tapol golongan C lainnya, akhirnya ia terpaksa dibebaskan, karena tidak ada bukti-bukti atau alasan untuk bisa diadili di depan pengadilan. Artinya, kebebasannya sebagai warganegara Republik Indonesia pernah dirampas oleh rezim militer secara sewenang-wenang. Ini merupakan kejahatan besar-besaran yang dilakukan negara atau aparat negara. Kejahatan yang dilakukan atas nama negara ini sekarang patut dikutuk dan digugat.

Sekarang, walaupun rezim militer Suharto dkk sudah jatuh, tetapi peraturan tentang KTP untuk para eks-tapol masih belum dirobah. Apa yang dialami Nani Nurani adalah hanya setetes air dalam lautan penderitaan yang dialami puluhan juta korban Orde Baru beserta keluarga mereka selama lebih dari 32 tahun. Mereka itu tidak bersalah apa-apa, tetapi dipenjarakan berpuluh-puluh tahun. “Kesalahan” mereka hanyalah karena bersimpati kepada PKI atau karena menyokong politik Bung Karno, atau menjadi anggota atau simpatisan SOBSI, BTI, Pemuda Rakyat, Gerwani, HSI, CGMI dan berbagai ormas lainnya. Padahal, semua organisasi tersebut adalah organisasi yang pada waktu itu didirikan secara sah menurut hukum dan berfungsi secara legal.

## SK Mendagri Yang Keterlaluan Buruknya

Nani Nurani, mem-PTUN-kan camatnya (kecamatan Koja, Jakarta Utara) karena fihak kecamatan tidak mau memberikan KTP Seumur Hidup . Alasannya, menurut SK Mendagri No.24 tahun 1991, mereka yang berstatus eks Tapol tidak berhak memperoleh KTP Seumur Hidup. Dan keputusan camat menolak pemberian KTP Seumur Hiudp kepada Nanai Nurani itu sudah “melalui prosedur, dan sesuai dengan hasil rapat dengan bagian hukum Walikotamadya Jakarta Utara.’

Sungguh, keterlaluan! Masalah Nani Nurani ini perlu kita angkat sebagai contoh tipikal, untuk menunjukkan kepada Presiden, dan DPR, dan Mahkamah Agung, dan Menteri Kehakiman dan Komnas Ham, atau kepada pers dan media massa lainnya, bahwa dewasa ini masih banyak peraturan-peraturan yang tidak demokratis atau praktek-praktek buruk zaman rezim militer Orde Baru yang masih terus dipakai. Termasuk peraturan dan ketentuan-ketentuan yang dipraktekkan terhadap para eks-tapol.

Masalah Nani Nurani bukan sekedar persoalan KTP Seumur Hidup. Melainkan berkaitan dengan masalah yang lebh besar. Yaitu masalah perasaan keadilan, masalah perikemanusiaan, dan penegakan hukum bagi seluruh negeri, baik di Jawa, Sumatra, Kalimantan, Sulawesi, Maluku, Nusa Tenggara dll. Nani Nurani pernah ditahan sebagai eks-tapol golongan C. Penahanan eks-tapol golongan C merupakan perampasan kebebasan terhadap sejumlah besar warganegara RI yang tidak bersalah apa-apa. Ini saja sudah merupakan kejahatan atau pelanggaran atau kesalahan, dalam skala besar-besaran, yang harus dipertanggungjawabkan oleh rezim militer Suharto dkk.

## Siksaan Yang Berkepanjangan

Sesudah para tapol terpaksa dibebaskan oleh karena desakan internasional, mereka dalam jangka lama tetap terus mendapat bermacam-macam perlakuan yang tidak berperikemanusiaan dari pemerintah (dan masyarakat). Tanda ET (eks-tapol) yang dicantumkan pada setiap KTP yang dimiliki olek para eks-tapol adalah salah satu di antara berbagai bentuk siksaan yang dipraktekkan oeh rezim militer Orde Baru. Sekarang ini, ketika rezim Orde Baru sudah terguling sejak lima tahun yang lalu, para eks-tapol yang sudah tua usianya, dan pada umumnya sudah tidak bisa berbuat banyak kecuali menunggu ajal mereka, masih harus terus mendapat perlakuan yang tidak sepatutnya.

Para eks-tapol ini, walaupun sudah berumur lebih dari 60 tahun, tidak berhak mendapat KTP Seumur Hidup seperti halnya warganegara RI lainnya. Demikian bunyi SK Mendagri No.24 tahun 1991. Di sini kelihatan kejanggalan (atau kegilaan ?) SK Mendagri yang dipakai sejak 1991 itu. Bahwa rezim militer Suharto mau menyiksa terus para eks-tapol dengan berbagai cara, itu adalah dapat dimengerti . Tetapi, sekarang ini, ketika reformasi sudah didengung-dengungkan, dan pemerintahan Gotongroyong sudah dibentuk, SK Mendagri ini perlu dicabut. Pencabutan SK Mendagri mengenai KTP Seumur Hidup ini merupakan bukti bahwa pemerintahan Gotongroyong yang dipimpin oleh Presiden Megawati tidak mau meneruskan peraturan yang buruk ini.

## Bisa Dijadikan Contoh

Kasus Nani Nurani yang mem-PTUN-kan camat Koja (Jakarta Utara) dalam urusan KTP Seumur Hidup adalah contoh keberanian dan kesedaran seorang warganegara Republik yang patut mendapat penghargaan dari kita semua. Perbuatannya dalam hal ini dapat dijadikan contoh bagi banyak orang (baik perseorangan maupun organisasi), untuk mempersoalkan segala perlakuan-perlakuan yang “aneh-aneh” dari pemerintah dan pejabat-pejabatnya terhadap para korban Orde Baru (beserta keluarga mereka).

Selama puluhan tahun, sudah terlalu banyak penderitaan berkepanjangan yang mereka pikul, yang disebabkan oleh berbagai politik anti-Sukarno, dan anti-kiri atau anti-PKI yang dianut reziim militer Suharto (beserta pendukung-pendukungnya). Penderitaan ini tidak bisa dan tidak boleh diteruskan. Sebab, diteruskannya penderitaan ini berarti meneruskan ketidakadilan dan melestarikan pelanggaran terhadap perikemanusiaan. Bangsa Indonesia tidak akan bisa menjadi bangsa yang terhormat di dunia, kalau terus-menerus terjadi ketidakadilan di antara berbagai suku, agama, ras, kepercayaan politik, atau aliran kepercayaan.

Oleh karena itu, adalah kewajiban yang mulia dan juga hak yang sah dari setiap warganegara Republik ini, termasuk para eks-tapol (seperti yang dilakukan oleh Nani Nurani) untuk menggugat negara beserta aparat-aparatnya atas segala perlakuan atau tindakan yang bertentangan dengan prinsip-prinsip keadilan. Seperti sudah sama-sama kita saksikan, tidak selamanya tindakan pemerintah adalah berdasarkan keadilan atau kebenaran. Dan, patut disayangkan bahwa kontrol terhadap segala kebijakan atau tindakan pemerintah ini tidak bisa dipercayakan sepenuhnya kepada DPR atau DPRD. Kualitas dan integritas politik dan moral para anggota badan-badan perwakilan rakyat kita dewasa ini tidak meyakinkan kita bahwa persoalan ’65 dan para korban Orde Baru akan mendapat penanganan secukupnya dan sebaik-baiknya, termasuk masalah KTP Seumur Hidup. Gebrakan, kutukan, tuntutan, protes, perlu terus-menerus digelar oleh kita semua yang menginginkan adanya pembaruan dalam kehidupan bangsa.

## Rekonsiliasi Nasional Atas Dasar Keadilan

Persoalan Kartu Seumur Hidup yang diangkat oleh Nani Nurani adalah hanya secuwil kecil dari berbagai perlakuan tidak adil yang dialami para eks-tapol beserta keluarga mereka. Selama lebih dari 32 tahun (ini jangka waktu yang lama sekali dalam kehidupan seorang manusia!) para korban Orde Baru harus mengalami berbagai perlakuan yang tidak adil atau tidak berperikemanusiaan dari rezim militer Suharto dkk. Keluarga dan para sanak-saudara jutaan korban pembantaian tahun ’65 dapat menjadi saksi atau nara sumber yang otentik tentang kejahatan besar-besaran yang dilakukan oleh Sujarto dkk itu.

Dalam jangka puluhan tahun para eks-tapol dilarang menjadi guru, dosen, penulis, wartawan, pengarang; dalang, seniman, pengacara, penasehat hukum, atau menjadi ketua RT/RW, anggota pemerintahan desa, dan tidak boleh dicalonkan dalam pemilihan umum. Dalam undang-undang pemilu 2003 ada disebutkan bahwa yang dapat dipilih menjadi anggota legislatif dan eksekutif adalah mereka yang "Bukan bekas anggota organisasi terlarang Partai Komunis Indonesia, termasuk organisasi massanya, atau orang yang terlibat langsung atau tidak langsung dalam G30S/ PKI atau organisasi terlarang lainnya."

Itu semua adalah bukti bahwa walaupun rezim militer Orde Baru sudah tumbang, tetapi banyak peraturan atau undang-undang tidak demokratis (yang ditrapkan pada masa-masa yang lalu) masih terus dipakai, sampai sekarang. Dihapuskannya segala peraturan dan undang-undang yang mendiskriminasi para eks-tapol dan para korban Orde Baru adalah syarat mutlak untuk merajut rekonsiliasi nasional. Rekonsiliasi nasional tidak dapat dibangun atas dasar-dasar ketidak-adilan dan rasa permusuhan yang selama ini dipupuk oleh rezim militer Orde Baru.
