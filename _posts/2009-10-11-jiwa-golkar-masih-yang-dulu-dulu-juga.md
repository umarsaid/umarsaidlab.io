---
layout: post
title: Jiwa GOLKAR masih yang dulu-dulu juga !
date: 2009-10-11
---

Dengan selesainya Munas Partai Golkar yang diadakan di Pakanbaru tanggal 4-8 Oktober 2009, maka banyak hal yang menarik untuk sama-sama kita perhatikan atau kita amati. Tulisan kali ini berusaha untuk mencoba menyajikan sebagian dari hal-hal penting yang berkaitan dengan munas dari partai yang pernah selama lebih dari 32 tahun telah menjadi pendukung setia dan alat utama kekuasaan rejim militer Suharto.

Sebab, walaupun Golkar sekarang ini sudah dalam keadaan terpuruk (menurut ucapan Aburizal Bakri, ketua umum Golkar yang baru), namun masih tetap merupakan salah satu di antara partai-partai terbesar di Indonesia, di samping Partai Demokrat yang dipimpin oleh SBY dan PDI-Perjuangan.  Dan oleh karena Golkar pernah menguasai atau mengangkangi secara ketat kehidupan negara (bersama golongan militer) selama lebih dari 32 tahun, maka kini masih mempunyai jaring-jaringannya di banyak bidang kehidupan bangsa.

Lalu, sesudah Munas Golkar di Pakanbaru itu, perkembangan apa saja yang mungkin terjadi bagi kehidupan negara dan bangsa kita ? Apakah hasil Munas Golkar bisa membawa pengaruh besar kepada keadaan politik, sosial dan ekonomi, baik dalam jangka dekat maupun jangka jauh? Mengenai soal ini tentulah akan banyak analisa, atau pendapat, atau ramalan, dari berbagai kalangan. Yang berikut ini adalah salah satu di antaranya, sekadar sebagai bahan tambahan untuk melihat persoalan ini dari berbagai segi atau sudut pandang.

Pimpinan Partai Golkar hasil Munas di  Pakanbaru

Munas di Pakanbaru itu telah memilih Aburizal Bakri sebagai ketua umum Golkar yang baru, mengalahkan Surya Paloh, Tommy Suharto, dan  Yuddi Chrisnandi. Dalam pemungutan suara yang dilakukan oleh  536 utusan dari seluruh Indonesia itu, Aburizal Bakri mendapat 296 suara, Surya Paloh 240 suara, sedangkan Tommy Suharto dan  Yuddi Chrisnandi tidak memperoleh satu pun  suara.

Dalam kepengurusan baru di bawah pimpinan Aburizal Bakri,  Agung Leksono menjabat sebagai Wakil Ketua Umum bersama Theo L. Sambuaga, dan Idrus Marham sebagai sekretaris jenderal. Yang menarik untuk diperhatikan adalah bahwa di antara sebelas wakil sekretaris jenderal Partai Golkar yang sekarang terdapat Titiek Suharto.  

Kalau melihat nama-nama pengurus yang baru hasil Munas di Pakanbaru, maka orang bisa saja menyimpulkan bahwa apa pun hasil Munas Golkar, atau siapa pun menjadi Ketua Umum, atau bagaimana pun komposisi dewan pimpinannya, maka  -- pada dasarnya atau pada hakekatnya -- Golkar  yang sekarang adalah tetap Golkar yang dulu-dulu juga. Muka muka baru boleh saja muncul, atau isi pidato-pidato para tokohnya pun bisa saja terdengar muluk-muluk tentang perlunya adanya perubahan demi kepentingan rakyat banyak. Namun, sekali lagi namun, Golkar pada dasarnya adalah tetap Golkar yang sudah dikenal oleh banyak kalangan bangsa kita sejak  45 tahun yang lalu.

Kiranya, perlulah  terlebih dulu sama-sama kita ingat kembali bahwa Golkar dilahirkan dalam tahun 1964 oleh segolongan militer, yang mendalangi SOKSI dan Sekber Golkar, yang ditujukan untuk melawan kekuatan PKI yang pada waktu itu makin menunjukkan dirinya sebagai pendukung utama berbagai politik Bung Karno. Golkar ini jugalah yang merupakan kekuatan politik, yang selama rejim Suharto berkuasa, dibesarkan oleh golongan militer untuk dipakai alat ampuh sekali dalam menunjang dan sekaligus melindungi rejim Orde Baru.

Peran golongan militer dalam Golkar

Golkar, yang selama 32 tahun dikendalikan oleh Suharto sebagai pemimpin tertingginya (selaku ketua dewan pembina) pernah dalam jangka lama sekali menjadi satu dan senyawa dengan ABRI (waktu itu). Boleh dikatakan bahwa -- pada hakekatnya  --  golongan militerlah yang menguasai Golkar, walaupun dalam masa panjang itu terdapat orang-orang sipil yang menduduki berbagai jabatan di banyak jenjang Golkar di seluruh negeri. Dalam banyak hal, adanya tokoh-tokoh sipil dalam Golkar adalah untuk memupuri wajah yang sebenarnya, untuk menunjukkan (ke dalam negeri dan luar negeri) bahwa Orde Baru adalah bukan rejim militer melainkan pemerintahan demokratis, seperti halnya di kebanyakan negara lainnya di dunia.

Untuk menipu opini umum di dalam negeri (dan juga luar negeri) maka rejim militer Orde Baru menyelenggarakan apa yang dinamakan pemilu, yang digembar-gemborkan dengan slogan palsu “luber jurdil” (Langsung Umum BEbas Rahasia JUjur dan aDIL). Tetapi, seperti yang sama-sama kita ingat, dalam prakteknya, pemilu yang berkali-kali diadakan selama Orde Baru sama sakali tidaklah bebas dan rahasia, dan tidak pula jujur dan adil. Itulah sebabnya maka Golkar selalu bisa tampil sebagai pemenang terbesar atau mutlak, bahkan pernah meraih suara lebih dari 70 % !



Rejim militer Orde Baru telah menggunakan « demokrasi parlementer »  yang berdasarkan hasil pemilu (yang palsu)  ini juga untuk melawan opini dari kalangan pendukung politik Bung Karno dan simpatisan kekuatan kiri lainnya yang dipelopori PKI, dengan kaok-kaok bahwa  kekuasaan Suharto (bersama para jenderalnya) adalah berdasarkan suara rakyat atau berlandaskan dukungan  rakyat lewat pemilu.

Golkar diam seribu bahasa tentang kejahatan Orde Baru

Dengan cara demikian, Suharto dan konco-konconya (termasuk tokoh-tokoh utama Golkar) berusaha  memompakan opini umum bahwa tindakan mereka menggulingkan kekuasaan Bung Karno dan menumpas kekuatan kiri (terutama PKI) mendapat persetujuan dari sebagian terbesar rakyat Indonesia. Artinya, secara tidak langsung , Suharto dan konco-konconya berusaha juga menyatakan bahwa pembunuhan jutaan orang tidak berdosa apa-apa dan pemenjaraan jutaan orang lainnya adalah sesuai dengan kehendak rakyat banyak.  

Bahwa Golkar adalah satu dan senyawa dengan rejim militer Orde Baru terbukti dari dukungan sepenuhnya partai ini kepada boleh dikatakan semua tindakan Suharto, walaupun sudah jelas bahwa banyak sekali kejahatan-kejahatan atau pelanggaran di bidang HAM telah dilakukan selama puluhan tahun. Kita semua menyaksikan sendiri selama ini bahwa Golkar tidak pernah menyatakan, -- secara tegas dan terang-terangan - ketidak-persetujuannya atau kemarahannya atas terjadinya pembantaian jutaan orang-orang kiri yang tidak berdosa apa-apa sama sekali, atau pemenjaraan begitu banyak orang, tanpa pengadilan.



Demikian pula, Golkar tidak pernah terang-terangan dan dengan tegas mengutuk pembunuhan terhadap Bung Karno, dengan menterlantarkan ketika ia sakit keras dalam status tahanan. Golkar juga diam seribu bahasa saja, terhadap kasus sepuluh ribu tapol yang dibikin sengsara di pulau Buru selama belasan tahun. Golkar juga menutup mata dan telinga serta mulut selama puluhan tahun, walau jutaan keluarga para korban peristiwa 65 (yang terdiri dari para ibu-ibu beserta anak-anak mereka) mengalami perlakuan yang tidak manusiawi selama jangka waktu yang lama sekali..

Golkar jugalah yang membiarkan dan melindungi terjadinya korupsi yang merajalela selama masa Orde Baru yang puluhan tahun itu di kalangan pimpinan militer dan tokoh-tokoh pemerintahan dan masyarakat, termasuk di kalangan Golkar sendiri. Oleh karenanya, maka tidak banyak kasus korupsi besar-besaran yang bisa dibongkar selama pemerintahan Orde Baru. Sebab, dalam banyak kasus korupsi, juga tersangkut terutama sekali orang-orang Golkar sendiri, di samping para petinggi militer. Itu jugalah salah satu faktor mengapa Suharto beserta keluarganya (ingat kasus Tommy) dapat leluasa mencuri harta rakyat dan negara, dan menumpuk harta haram dengan cara-cara yang bathil.

Tanpa Suharto Golkar kehilangan « kejayaan »nya

Dengan turunnya Suharto dari pimpinan negara sejak 1998, maka Golkar kehilangan juga « kebesaran » atau « kejayaan »-nya yang disandangnya selama puluhan tahun. Sebenarnya, kebesaran dan kejayaan Golkar adalah melulu hanya karena Suharto dan para petinggi militernya. Tanpa Suharto dan tanpa ikut sertanya pimpinan militer seperti semasa Orde Baru, maka Golkar makin merosot sejak dimulainya era reformasi.

Kalau dalam tujuh kali pemilu selama masa Orde Baru Golkar selalu memperoleh suara diatas 60%, maka dalam pemilu 2004 hanya 21 % dan dalam 2009 lebih merosot lagi menjadi 14,5%.  Makin jelaslah kiranya bahwa tanpa Suharto dan tanpa dukungan golongan militer, Golkar tidak mungkin mampu mempertahankan « kejayaan » dan « kebesaran »-nya seperti selama Orde Baru.

Tommy Suharto menyiapkan Rp 51 triliun untuk « kejayaan » Golkar

 Munas Golkar di Pakanbaru berikut hasil-hasilnya memberikan berbagai pertanda  bahwa Golkar akan makin merosot dan, karenanya,  tidak akan mungkin sama sekali mengembalikan « kejayaan » masa lalunya. Kenyataan bahwa Tommy Suharto tidak memperoleh satu suara pun dari 536 suara dalam pemilihan ketua umum, adalah salah satu di antara banyak petunjuk ke arah itu. Kegagalan total Tommy untuk mewujudkan impiannya untuk menjadi ketua umum Golkar (sebagai tangga untuk menjadi presiden di kemudian hari) adalah salah satu bukti bahwa citra keluarga Jalan Cendana sudah semakin pudar.  

Walaupun  (menurut harian Rakyat Merdeka 30 September 2009) Tommy  sudah menyiapkan dana Rp 51 triliun untuk melaksanakan programnya membangun kembali Partai Golkar jika ia terpilih menjadi Ketua Umum dalam Munas di Pakanbaru, ternyata ia dicuweki saja oleh para peserta Munas. (Harap ingat bahwa uang Rp 51 triliun adalah angka Rp 51 dengan 12 nol di belakangnya, atau jelasnya : Rp 15 000 000   000 000,  atau Rp 15 juta dikalikan satu juta. Masya Allah ! Dari mana saja uang sebanyak itu yang akan digaruk oleh Tommy ?)

Sekarang ini, yang menjadi Ketua Umum Golkar adalah Aburizal Bakrin dan bukannya Surya Paloh yang menjanjikan adanya perubahan-perubahan di Golkar. Mengingat sepak terjang Golkar selama masa Orde Baru 32 tahun ditambah lebih dari 10 tahun sesudah turunnya Suharto, tidak  ada dasar atau alasan untuk percaya atau berharap bahwa Golkar akan meninggalkan jati dirinya yang asli, yaitu sebagai kekuatan reaksioner atau kontra revolusioner yang anti ajaran-ajaran revolusioner Bung Karno dan anti-kiri, baik yang dibawakan oleh PKI maupun oleh golongan-golongan kiri lainnya.



Golkar adalah kekuatan yang menentang perubahan fundamental

Golkar (dan juga golongan militer pendukung Suharto) adalah pengejawantahan (manifestasi) kekuatan yang menentang adanya perubahan-perubahan besar dan fundamental di Indonesia. Sebab, pada intinya atau pada hakekatnya perubahan-perubahan besar dan fundamental berarti perombakan besar-besaran atau pembuangan jauh-jauh segala sesuatu yang berbau Orde Baru, yang sudah terbukti dengan jelas sekali merupakan sistem politik, ekonomi, sosial dan moral yang busuk dan telah merusak tubuh bangsa dan negara Indonesia.

Walaupun Golkar sekarang  sudah tidak lagi memainkan peran sepenting dan sebesar seperti masa lalu namun masih menguasai dana yang melimpah-limpah dan sumber daya manusia yang besar, berkat adanya jaring-jaringan di berbagai bidang pemerintahan dan ekonomi yang sudah dipupuk atau dibangun selama puluhan tahun,  Golkar yang sekarang adalah juga Golkar  yang masih tetap bangga atas « kejayaan » masa lalu, yang penuh dosa dan aib bangsa. Berbagai ucapan tokoh-tokohnya di Munas mencerminkan watak asli Golkar sebagai kekuatan politik reaksioner. Dan banyaknya jas kuning yang dipakai dengan bangga oleh banyak peserta Munas juga merupakan sebagian dari pertanda bahwa Golkar yang sekarang adalah  -- pada dasarnya  -- masih Golkar yang dulu-dulu juga.

Golkar tidak bisa melahirkan gagasan-gagasan besar

Selama lebih dari setengah abad bangsa dan negara kita sudah menyaksikan apa-apa saja yang telah dilakukan oleh Golkar untuk kepentingan rakyat. Jujjur jujur saja, tidak banyak !  Kalau kita amati dengan seksama tidaklah pernah ada gagasan-gagasan besar dan gemilang mengenai kehidupan dan perjuangan rakyat, yang bisa dilahirkan oleh kalangan pimpinan Golkar, termasuk oleh Suharto sendiri. Yang sudah sering terdengar atau dibaca selama puluhan tahun itu (termasuk di Munas di Pakanbaru) hanyalah ungkapan-ungkapan fikiran yang umumnya klise-klise yang itu-itu juga dan  yang bermutu mediocre (tidak bermutu atau tidak memadai),  

Kita juga bisa sama-sama menyaksikan sendiri bahwa dalam perjuangan bangsa Indonesia untuk merebut kemerdekaan, hanyalah tokoh agung Bung Karno yang telah bisa melahirkan gagasan-gagasan besar yang kemudian  dijadikan pedoman jiwa bangsa, seperti Indonesia Menggugat, lahirnya Pancasila, Konferensi Bandung, Berdikari, Panca Azimat Revolusi, Go to Hell with your aid, To build the World Anew,  Trisakti, Nasakom dll dll.

Mengingat hal-hal itu semua, kiranya kita semua masih perlu mengamati Golkar, walaupun sudah semakin terpuruk. Sebab, sisa-sisa Orde Baru yang bercokol terus di Golkar masih cukup banyak.

Namun, sisa-sisa Orde Baru juga terdapat di partai-partai lainnya, terutama  di kalangan Partai Demokratnya SBY, Hanuranya Wiranto, Gerindranya Prabowo, dan di berbagai partai Islam. Dan sisa-sisa Orde Baru yang tersebar di berbagai partai dan kalangan Islam ini pada umumnya bersikap anti ajaran-ajaran revolusioner Bung Karno dan anti golongan kiri lainnya.

Perkembangan situasi politik sesudah pemilihan Presiden yang lalu, ditambah dengan dipilihnya Aburizal Bakri sebagai ketua umum Golkar meramalkan indikasi yang kurang  baik bagi kehidupan demokratis di negara kita. Karena melalui pilpres SBY telah dipilih sebagai presiden dengan suara lebih dari 60 % maka ia memiliki otoritas jang cukup tinggi di puncak pimpinan negara. Presiden SBY  sudah dan sedang terus menghimpun dukungan dari berbagai fihak sambil sekaligus melumpuhkan atau menggerogoti kekuatan lawan atau saingannya.

Bersiap-siaplah, SBY bisa menjadi makin otoriter

Dengan mendukung Taufik Kimas menjadi Ketua MPR, maka SBY (artinya Partai Demokrat) sudah mengikat atau menyerimpung PDI-P dan menyeret ke kubunya. PDI-P sudah tidak  bisa terlalu kritis terhadap pemerintahan SBY-Boediono, dan tidak  pula merupakan kekuatan oposisi yang biasa. Demikian juga halnya dengan partai Golkar yang diketuai oleh Aburizal Bakri, yang selama ini kelihatan dekat dengan SBY, dan sudah pula terang-terangan menyatakan bahwa Golkar bunkanlah partai oposisi (Kompas 8 Oktober 2009).



Dengan didominasinya DPR oleh tiga partai politik  (Partai Demokrat, PDI-P dan partai Golkar), maka SBY-Boediono bisa melumpuhkan suara-suara yang kritis terhadap pemerintahannya. Artinya, berbagai politik pemerintahan SBY-JK yang pro- neoliberal dan tidak pro-rakyat akan diteruskan, seperti halnya pada masa-masa lalu.. Oleh karenanya, perubahan-perubahan besar dan fundamental tidak akan terjadi, dengan adanya kekuasaan DPR yang didominasi oleh sisa-sisa kekuatan Orde Baru yang terdapat di 9 partai, termasuk partai-partai Islam.  Dengan peta politik yang demikian, maka kemungkinannya pemerintahan SBY-Boediono menjadi otoriter sangat besar sekali, walaupun mungkin tidak sampai totaliter (mudah-mudahan… !)



Mengingat itu semua, seluruh kekuatan demokratis yang terdapat di berbagai kalangan bangsa, perlulah kiranya sejak sekarang bersiap-siap menghadapi situasi yang mungkin makin menyulitkan kehidupan demokrasi dan menyengsarakan rakyat.
