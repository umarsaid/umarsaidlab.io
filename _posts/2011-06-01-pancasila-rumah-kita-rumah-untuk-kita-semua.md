---
layout: post
title: Pancasila rumah kita, rumah untuk kita semua
date: 2011-06-01
---

Luar biasa !!! Sosok agung Bung Karno sebagai bapak bangsa muncul kembali di tanah air kita, dengan diperingatinya Hari Lahir Pancasila 1 Juni, baik di Jakarta maupun di berbagai kota di seluruh Indonesia.  Peringatan kali ini merupakan peristiwa yang amat penting dan menarik sekali  bagi negara dan bangsa kita sejak Suharto bersama-sama konco-konconya (dalam negeri dan luar negeri) menggulingkan Bung Karno.

Sebab, nama besar Bung Karno sebagai penggali (atau penggagas atau pencipta) Pancasila disebut-sebut dengan kebanggaan dan rasa hormat oleh banyak kalangan, lebih banyak dan lebih luas dari pada yang sudah-sudah. Ini kelihatan dari banyaknya tulisan-tulisan dalam media cetak dan tayangan di televisi mengenai hal-hal yang berkaitan dengan Hari Lahirnya Pancasila dan Bung Karno.

Di antara peristiwa-peristiwa itu adalah acara besar yang diselenggarakan oleh MPR di Gedung MPR, yang dihadiri oleh banyak sekali pejabat-pejabat tinggi negara dan tokoh-tokoh terkemuka masyarakat, antara lain oleh presiden SBY, mantan presiden BJ Habibi, presiden Megawati.

Dalam upacara peringatan ini telah berpidato Ketua MPR Taufik Kiemas, mantan presiden BJ Habibi dan Megawati dan diakhiri oleh pidato presiden SBY. Baik dari pidato mantan Presiden Megawati maupun pidato presiden SBY dapat diperoleh kesan bahwa sekarang ini kebesaran Bung Karno sebagai tokoh pemersatu dan pemimpin bangsa sudah makin diakui oleh sebagian besar rakyat kita. Bukan hanya mantan presiden Megawati saja yang berkali-kali mengutip pidato-pidato Bung Karno, melainkan juga presiden SBY, lebih dari pada yang sudah-sudah.

Ini semua memberikan indikasi bahwa situasi bangsa dan negara kita dewasa ini memang betul-betul membutuhkan Pancasila yang direvitalisasi (dihidup-segarkan kembali) dan diamalkan dalam pratek kehidupan  sehari-hari.

Dan, seperti yang pernah ditulis berkali-kali, Pancasila hanya  bisa dilaksanakan atau dipraktekkan secara  baik dengan menghayati  (sungguh-sungguh !)  dan menjiwai sepenuhnya ajaran-ajaran revolusioner Bung Karno mengenai Pancasila. Bukan dengan teori  yang muluk-muluk lainnya, atau interprestasi-interpretasi yang bertentangan, atau berbeda,  atau bahkan anti ajaran-ajaran revolusioner Bung Karno

Paris, 1 Juni 2011

A. Umar Said

* * *

Untuk membantu para pembaca bisa mengetahui sekedarnya berbagai hal yang berkaitan dengan peringatan Hari Lahir Pancasila di Jakarta ini maka di bawah ini disajikan kumpulan berbagai berita.



Megawati Serukan Rakyat Indonesia Lestarikan Pancasila

Okezone, 1 Juni 2011

JAKARTA - Presiden Republik Indonesia ke-5 Megawati Soekarnoputri menegaskan peringatan hari kelahiran Pancasila pada 1 Juni hari ini bisa dijadikan sebagai landasan bagi bangsa Indonesia sekaligus mempertegas bahwa Pancasila adalah kepunyaan dan murni ideologi Indonesia.

"Bagi saya peringatan kali ini mestinya merupakan jalan baru, jalan ideologis, untuk mempertegas bahwa tidak ada bangsa besar jika tidak bertumpu pada ideologi yang mengakar pada nurani rakyatnya," kata Megawati, dalam pidato kebangsaan memperingati Harlah Pancasila, di gedung Nusantara IV MPR/DPR, Senayan, Rabu (1/6/2011).

Mega menjelaskan, hal ini penting bagi Indonesia terutama di mata dunia internasional bahwa Indonesia memang negara besar yang berideologi kuat.

"Kita bisa memberi contoh negara Jepang, Jerman, Amerika, Inggris dan Republik Rakyat Tiongkok, menemukan kekokohannya pada fondasi dan ideologi yang mengakar kuat dalam budaya masyarakatnya. Sebab ideologi itu menjadi alasan, sekaligus penuntun arah sebuah bangsa dalam meraih kebesarannya," terang Mega.

Putri sulung Bung Karno itu menambahkan, ideologi Pancasila sudah seharusnya dipelihara oleh setiap anak bangsa demi menjaga kebesaran dan martabat NKRI.

"Memudarnya Pancasila di mata dan hati sanubari rakyat sendiri telah berakibat jelas, yakni negeri ini kehilangan orientasi, jati diri dan harapan. Tanpa harapan negeri ini akan sulit menjadi bangsa yang besar karena harapannya adalah salah satu kekuatan yang mampu memilihara daya juang sebuah bangsa," pungkasnya. (put)

* * *

 Megawati: Pancasila Bukan Urut-urutan Sila

Vivanews, 1 Juni 2011

JAKARTA - Mantan Presiden RI ke-5 Megawati Soekarnoputri kembali berapi-api dalam pidato peringatan Hari Lahir Pancasila 1 Juni. Dalam pidatonya Megawati mengkritik orang yang beranggapan Pancasila dari urutan sila-sila.

"Pancasila bukan terletak pada urut-urutan sila. Tapi, kita harus memaknai jauh dari hal itu," kata Megawati yang disambut tepuk tangan hadirin di Gedung DPR, Jakarta, Rabu (1/6/2011).

Menurut Mega, jauh dari hal itu, Pancasila harus dimaknai sesuai yang terdapat dalam alinea ke empat pembukaan UUD 1945. Pengakuan terhadap Pancasila justru terletak dalam asas dan pengertiannya yang tetap sebagai dasar filsafat NKRI.

"Bukan pada bentuk formilnya akan tetapi sifat materik yang dimaksudkannya," terang Mega.

Dalam pidato tersebut Mega juga menyinggung soal peran besar sang ayah, Soekarno, dalam merumuskan dan mengkaji falsafah negara yang kemudian tertuang dalam Pancasila. "Bukan karena beliau ayah saya, tetapi peran Bung Karno sangat besar dalam menciptakan dasar negara ini," ucap Mega bangga.

Sejumlah mantan presiden dan pejabat negara hadir dalam peringatan Hari Lahir Pancasila 1 Juni di Gedung DPR, Jakarta. Dalam acara ini juga menjadi pertemuan Megawati dan Presiden SBY dalam forum formil.


* *  *

Kutip Lagu Franky, Mega Teteskan Air Mata

Okezone, 1 Juni 2011

JAKARTA - Mantan Presiden ke-5 Megawati Soekarnoputri sempat menitikkan air mata pada saat membawakan pidato kebangsaan memperingati Hari Kelahiran Pancasila  di Gedung MPR/DPR Senayan hari ini. Di dalam pidatonya tersebut, Mega menyelipkan syair lagu karya musisi mendiang Franky Sahilatua berjudul 'Pancasila Rumah Kita'

"Sebelum mengakhiri pidato ini saya ingin menyampaikan sedikit cuplikan lagu yang begitu indah yang disampaikan sahabat saya Franky Sahilatua dalam syair 'Pancasila Rumah Kita'," kata Mega seraya membuka kacamata dan menghapus air matanya saat berpidato di Gedung Nusantara IV MPR/DPR, Senayan, Jakarta, Rabu (1/6/2011).

"Pancasila rumah kita, rumah untuk kita semua, nilai dasar Indonesia, rumah kita selamanya, untuk semua keluarga menyatu, untuk semua saling membagi pada semua insan, sama dapat sama rasa oh Indonesiaku," kata Mega dengan suara lirih.

Penyampaian syair tersebut mendapat sambutan tepuk tangan dari para hadirin yang memenuhi ruangan Gedung Nusantara IV MPR/DPR.

Acara dilangsungkan di Gedung Nusantara IV, DPR, Senayan, Jakarta. Peringatan pidato Bung Karno terkait Pancasila ini yang di peruntukkan membudidayakan empat pilar kehidupan berbangsa dan bernegara yaitu Pancasila, UUD 1945, NKRI, dan Bhineka Tunggal Ika yang sedang gencar dilakukan pimpinan MPR secara massif. Dalam acara ini turut hadir pula mantan Presiden RI ketiga, BJ Habibie, Istri Almarhum Gus Dur, Shinta Nuriah Abdurrahman Wahid, mantan Wakil Presiden RI, Jusuf Kalla, dan Try Sutrisno dan sejumlah menteri Kabinet Indonesia Bersatu Jilid II. (put)

 * *



Pancasila Dinilai Tinggal Menjadi Kenangan

Rabu, 01 Juni 2011

REPUBLIKA.CO.ID,JAKARTA--Pengamat dari Asosiasi Ekonomi Politik Indonesia Ichsanuddin Noorsy menilai Pancasila kini telah menjadi kenangan tanpa makna karena negara sudah tak lagi menghiraukan apalagi mengimplementasikannya. "Penjajahan di Indonesia sudah berjalan secara sistemik, sehingga sejak orde baru hingga orde reformasi Pancasila tinggal sebagai kenangan tidak bermakna," katanya di Jakarta, Rabu.



Ia menilai, setelah Orde Baru berkuasa dan terlebih sejak Orde Reformasi bergulir, bangsa yang kaya dengan pemikiran harkat martabat kemanusiaan ini kehilangan keyakinan karena sistem ekonomi, politik dan hukumnya yang berlandaskan Pancasila telah berganti menjunjung tinggi nilai-nilai liberal.

Hal ini, menurut dia, telah menjadikan Indonesia kembali terjajah terutama secara ekonomi. Pancasila yang harusnya menjadi pandangan hidup, telah berganti menjadi Ekonomi liberal. Ia menambahkan, penghempasan nilai-nilai Pancasila justru didukung oleh para pengambil kebijakan.



"Saya mengatakan DPR bersama Pemerintah justru yang melahirkan UU sehingga ekonomi Indonesia secara struktural terjajah lebih mendalam. Kondisi ini membuktikan bahwa Indonesia bukan saja telah menyia-nyiakan Pancasila, juga telah menjungkirbalikkannya," katanya.



Pengamat kritis itu menambahkan, dahulu Pancasila telah menjadi inspirasi bagi dunia. Pidato Bung Karno pada 1 Juni 1945 berlanjut dengan tawarannya kepada PBB untuk menggunakan Pancasila sebagai piagam kehidupan pergaulan antarbangsa yang damai dan sejahtera.

Pancasila, menurut dia, mampu membangun keyakinan pada mereka yang mau membuka mata hati dan pikiran sebagai jalan yang benar dan lurus menuju masa depan yang adil dan penuh dengan nilai-nilai kemanusiaan. Nilai-nilai ini tidak dimiliki oleh paham bangsa manapun di dunia.



"Pikiran Bung Karno yang jernih dalam menggali nilai-nilai bangsa Indonesia yang secara struktural terjajah dalam segala bentuk dan maknanya, telah memberi inspirasi kepada bangsa-bangsa Asia Afrika. Inspirasi ini membangun keyakinan bahwa sikap mendominasi atau menghegemoni suatu bangsa terhadap bangsa-bangsa melalui strategi dan taktik apapun tidak dibenarkan," katanya.

Menurut dia, Pancasila waktu itu membuat Indonesia tidak hanya disegani tapi juga diakui sebagai bangsa yang kaya pemikiran dan kaya sumberdaya alamnya.



* *



SBY: Tidak Ada Tempat Selain Ideologi Pancasila



Media Indonesia 01 Juni 2011

JAKARTA--MICOM: Presiden SBY menyampaikan pidatonya dalam rangka memperingati hari lahirnya Pancasila di gedung DPR Senayan hari ini Rabu (1/6).

Dalam pidatonya, SBY menyinggung keberadaan NII (Negara Islam Indonesia) yang belakangan berkembang kembali di Indonesia.



"Dalam era ini ada yang tergoda untuk memeluk ideologi lain selain Pancasila, ada juga yang cemas jangan-jangan ada yang ingin mendirikan negara berdasarkan agama. Sebagai kepala negara, saya sampaikan gerakan seperti itu tidak ada tempat di Indonesia," kata Presiden SBY.

Menurut orang nomor 1 di Indonesia tersebut, gerakan seperti NII bertentangan dengan Pancasila yang menjadi landasan negara. Sehingga, di era sekarang ini menurut SBY sangat penting dilakukan revitalisasi dan aktualisasi secara efektif.



"Gerakan itu bertentangan dengan semangat kita mendirikan negara berdasarkan Pancasila," ujarnya.

SBY juga mengatakan bila gerakan tersebut sudah melanggar hukum pastinya tidak boleh dibiarkan. Masalah tersebut bisa diselesaikan dengan cara-cara yang sesuai hukum.

"Caranya harus diselesaikan dengan cara-cara demokrasi »



* *

Survei: Pancasila Masih Dibutuhkan



Rabu, 1 Juni 2011,
VIVAnews - Badan Pusat Statistik menemukan dalam surveinya, publik masih membutuhkan Pancasila. Dari 12.000 responden yang ditanya, 79,26 persen menyatakan Pancasila penting dipertahankan.

"Sementara 89 persen masyarakat berpendapat permasalahan bangsa, disebabkan kurangnya pemahaman nilai-nilai Pancasila," kata Presiden Susilo Bambang Yudhoyono melansir hasil survei itu dalam pidato kebangsaan di Gedung MPR, Rabu 1 Juni 2011.

Berdasarkan survei itu, kata SBY, ada beberapa solusi yang muncul yakni sosialisasi melalui pendidikan (30 persen), contoh dari pejabat negara (19 persen), dan ceramah agama (10 persen).

Siapa yang mendidik soal Pancasila? "Sebanyak 43 persen menjawab dosen dan guru bisa dipercaya untuk memberikan pendidikan Pancasila, 20 persen oleh badan khusus yang dibentuk pemerintah," kata SBY.
Sementara elite politik mendapat porsi paling kecil dipercaya oleh publik yakni sebanyak 3 persen.

Dari survei ini, SBY juga mendapat informasi bagaimana pandangan publik ketika ditanya soal negara dengan dasar agama. "Sekitar 75 persen mereka mengatakan keinginan mendirikan dan gerakan politik negara berdasar agama itu tidak dapat dibenarkan," ujarnya.

Di Lorong Sunyi

Hasil survei ini menjawab keresahan mantan Presiden BJ Habibie. Dia menilai Pancasila sebagai dasar negara makin jarang dibahas dan dikutip dalam berbagai forum kehidupan.

Pasca-reformasi, kata dia, Pancasila seperti tersandar di sebuah lorong sunyi di tengah kehidupan bangsa yang semakin hiruk-pikuk oleh politik. Ada beberapa penjelasan, mengapa Pancasila seolah lenyap.
Pertama, situasi dan kehidupan bangsa yang telah berubah baik domestik, regional, maupun global. "Situasi kehidupan bangsa sejak 1945 mengalami perubahan nyata saat ini dan akan terus berubah pada masa yang akan datang," kata dia dalam pidato kebangsaan memperingati Hari Pancasila itu.

Kedua, pemanfaatan teknologi informasi yang amat berpengaruh pada kehidupan saat ini. "Tetapi juga rentan dengan manipulasi informasi dengan segala dampak."

Ketiga, Pancasila juga luntur akibat pengaitan dengan masa lalu, terutama masa Orde Baru di mana Pancasila diposisikan sebagai alat melanggengkan kekuasaan. Ketika terjadi pergantian kekuasaan munculnya dekonsepsi Pancasila yang dianggap sebagai aktor. "Pancasila ikut dipersalahkan. Sehingga membekas sebagai trauma sejarah," ujarnya.

Menurut Habibie, pengkaitan Pancasila dengan rezim tertentu merupakan kesalahan mendasar. Dia mengakui ada mistifikasi di masa Orde Baru yang kemudian menjadi senjata sistematis melanggengkan kekuasaan. "Akibatnya, ketika terjadi pergantian rezim di era reformasi, terjadi demistifikasi dan dekonstruksi Pancasila," ujarnya.

Berbagai hal tersebut, kata Habibie, membuat Pancasila semakin jauh dari kehidupan berbangsa. Secara formal Pancasila diakui sebagai dasar negara tapi tidak dijadikan pilar dalam membangun bangsa. Sebab itu, dia menilai perlu reaktualisasi, restorasi atau rekonstruksi nilai Pancasila. "Problema kebangsaan semakin kompleks baik nasional regional dan global. Butuh panduan menuju indonesia lebih baik," ujarnya.

Setengah hati
Mantan Presiden Megawati Soekarnoputri pun meminta pelajaran Pancasila kembali ditanamkan di sekolah-sekolah. Dia mengingatkan, Pancasila adalah produk yang terbentuk dengan pemikiran panjang, bukan produk sekali jadi.

“Sosialisasi dan institusionalisasi Pancasila sebagai salah satu pilar kehidupan berbangsa dan bernegara menjadi tanggung jawab lembaga-lembaga negara, baik di tingkat pusat maupun daerah,” ujar Megawati.

"Sebaiknya Pancasila diajarkan kepada generasi muda sejak SD, SMP dan SMA. Bila dihilangkan, maka akan menghilangkan pondasi bernegara. Di negara manapun ideologi bangsa selalu diajarkan," kata Pramono Anung, Wakil Ketua DPR.

Ketua MPR, Taufiq Kiemas, menyatakan Pendidikan Pancasila akan dimasukkan lagi ke kurikulum pelajaran sekolah. Dulu Pancasila diajarkan di sekolah-sekolah dengan nama mata pelajaran Pendidikan Moral Pancasila (PMP) namun kemudian dihapus dan diganti dengan Pendidikan Kewarganegaraan, tanpa embel-embel Pancasila.

“Pemerintah 1.000 persen setuju untuk mengembalikan Pendidikan Pancasila ke kurikulum sekolah. Untuk itu MPR akan dengar pendapat dengan ahli pendidikan dan ahli tata negara selama tiga hari,” kata Taufiq beberapa hari lalu.

Namun institusionalisasi melalui pelajaran di sekolah ini dirasa sejarawan Asvi Marwan Adam belum cukup. Menurut Asvi, pemerintah tak boleh setengah hati menyosialisasikan Pancasila. "Kenyataannya setengah hati, lembaga tinggi negara baru membentuk komisi."

Mestinya, tambah dia, Pancasila ditangani oleh lembaga setingkat Kementerian. Tak perlu sampai membentuk yang baru yang membutuhkan dana besar, tapi cukup "Melalui kementerian yang ada yakni Kementerian Agama dan Pancasila. Kelihatannya agama dan Pancasila dua hal yang berbeda, tapi saling melengkapi," ujar dia.

Langkah ke dua, Presiden harus menetapkan tanggal 1 Juni sebagai Hari Lahir Pancasila. "Harusnya ada Keputusan Presiden yang memutuskan 1 Juni sebagai Hari Lahir Pancasila."

Asvi menilai Pemerintah aneh hanya menetapkan hari lahir Undang-undang Dasar 1945, tapi Pancasila yang menjiwainya tidak ditetapkan hari lahirnya. "Masih ada diskriminasi terhadap Pancasila, belum diakui, pemerintah masih setengah hati."
• VIVAnews

* * *

Presiden Tegaskan Negara Ketuhanan Bukan Agama

Okezone 1 Juni 2011

JAKARTA - Dalam peringatan Hari Lahir Pancasila pada 1 Juni 2011 ini Presiden Susilo Bambang Yudhoyono kembali menyinggung soal bentuk dasar negara yang dianut RI. Bagi Presiden, sudah menjadi konsensus fundamental bahwa RI berdasarkan negara ketuhanan dan nasional bukan agama.

"Sejak awal pendiri republik secara arif dan luas memandang ke depan sudah menetapkan konsensus fundamentar bahwa negara berdasarkan ketuhanan dan nasionalisme bukan negara agama," kata Presiden SBY dalam pidato Hari Lahir Pancasila di Gedung DPR, Rabu (1/6/2011).

Meski demikian, Presiden mengatakan agama tetap dijunjung tinggi dalam kehidupan individu dan masyarakat. "Jadi bukan sekuler yang tidak mengakui adanya tuhan," tegas Presiden.

Presiden juga menegaskan bahwa ideologi pancasila menjadi dasar negara dan bukan ideologi yang lain seperti fasisme, komunisme dan liberalisme. Menurut Presiden, arus globalisasi bisa menarik orang untuk menganut ideologi selain pancasila seperti mendirikan negara berdasarkan agama.

"Saya tegaskan bahwa niat gerakan politik untuk mendirikan negara berdasarkan pancasila bertentangan dengan semangat bernegara," terang Presiden.
