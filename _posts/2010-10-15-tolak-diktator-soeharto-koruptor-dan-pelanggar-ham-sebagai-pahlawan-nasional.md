---
layout: post
title: Tolak Diktator Soeharto, Koruptor dan Pelanggar HAM, Sebagai Pahlawan Nasional
date: 2010-10-15
---

Masalah gagasan atau usul untuk mencalonkan Suharto mendapat gelar  « Pahlawan nasional » yang diajukan oleh berbagai kalangan, rupanya masih terus mendapat reaksi keras yang menentang. Ini wajar, sebab Tommy Suharto sendiri baru-baru ini juga menyatakan bahwa soal pemberian gelar yang begitu tinggi kepada mendiang bapaknya itu « tinggal selangkah » lagi, artinya tidak akan lama lagi.

Mengingat pentingnya kontroversi mengenai masalah gelar pahlawan nasional untuk Suharto ini bagi bangsa Indonesia beserta generasi-generasi yang akan datang, maka disajikan berikut ini surat terbuka  M. Fadjroel Rachman, Ketua Lembaga Pengkajian Demokrasi dan Negara Kesejahteraan  kepada Presiden SBY.

Surat terbuka ini merupakan dokumen menarik, penting, dan serius yang dengan tajam dan jelas sekali membeberkan berbagai ragam dosa-dosa Suharto, sehingga sama sekali tidaklah pantas atau berhak mendapat gelar pahlawan nasional.

Surat terbuka ini diajukan kepada SBY sebagai seorang petinggi militer di bawah Suharto, yang tentu tahu betul apa saja tindakan-tindakannya sebagai pimpinan rejim militer Orde Baru selama 32 tahun.  Bahkan, boleh dikatakan bahwa SBY adalah  tokoh yang pernah menjadi bagian yang cukup penting dalam rejim militer itu.

Karena itu, kalau SBY akan ikut-ikut – secara langsung atau tidak langsung, secara tertutup atau melalui berbagai ragam rekayasa – dalam pemberian gelar pahlawan nasional kepada Suharto, maka  SBY akan termasuk dalam kelompok yang akan digugat dan dihujat oleh banyak orang, termasuk generasi-generasi yang akan datang.

SBY, sebagai mantan perwira tinggi Angkatan Darat,  bisa saja atau  boleh saja mempunyai rasa hormat kepada Suharto oleh karena berbagai sebab dan pertimbangan pribadi, namun (sekali lagi, namun !) kalau ia ikut-ikut menyetujui diberikannya gelar pahlawan nasional kepada Suharto, maka SBY akan tercatat dalam sejarah bangsa Indonesia sebagai seorang yang ikut mencoreng muka bangsa dengan kotoran manusia.

Bukan itu saja ! Kalau SBY ikut « merestui » pemberian gelar pahlawan nasional kepada Suharto, maka akan membikin lebih buruk lagi nama Angkatan Darat, yang selama puluhan tahun  sudah tidak harum sama sekali. Gelar « pahlawan nasional « kepada Suharto bisa saja menjadi kebanggaan bagi sejumlah kecil pimpinan militer, namun akan menjadi sumber kebencian sebagian besar rakyat terhadap pimpinan militer pada umumnya, dan pimpinan Angkatan Darat pada khususnya.

Sekarang ini  makin jelaslah  bahwa Suharto  (beserta konco-konconya) adalah perusak jiwa TNI Angkatan Darat  secara besar-besaran dan menyeluruh selama puluhan tahun, sehingga menjadi asing bagi rakyat banyak. Sebenarnya, Suharto adalah kotoran dalam kalangan militer.

Mengingat itu semuanya, dan dengan bahan-bahan yang diajukan dalam surat terbuka Fadjroel Rachman kepada SBY, maka jelas bahwa Suharto tidaklah bisa  (dan juga tidak boleh !!!)  disebut sebagai pahlawan nasional. Suharto adalah kotoran bangsa Indonesia.



Paris, 15 Oktober 2010

A. Umar Said

= = = =



Surat untuk Tuan Jenderal (purn) SBY :

Oleh:M. Fadjroel Rachman

Tuan Jenderal (purn) SBY ingin menobatkan Jenderal Besar (purn) Soeharto sebagai pahlawan untuk SBY sendiri, dipersilakan, tak ada yang akan menolaknya. Lalu membuat patung raksasa Jenderal Besar (purn) Soeharto di depan rumahnya di Puri Cikeas, juga dipersilakan, tak ada yang akan menolaknya. Tetapi bila SBY ingin menobatkan Soeharto sebagai pahlawan nasional, maka seluruh rakyat Indonesia, juga bumi dan langit akan menolaknya. Tak masuk akal bila seorang diktator, koruptor dan pelanggar HAM menjadi pahlawan nasional Indonesia. Ini penghinaan terhadap akal sehat dan kemanusiaan.

Apakah Tuan Jenderal (purn) SBY masih ingat siapakah sosok sebenarnya Jenderal Besar (purn.) Soeharto? Tentu sangat ingat, karena anda menjadi seorang Letnan Jenderal di  bawah rezim totaliter Soeharto bukan? Ketika 32 tahun lebih berkuasa, jutaan lawan politik Soeharto harus masuk penjara, mati, ataupun hilang tak tentu rimbanya sampai sekarang. Kenalkan anda dengan Widji Thukul, isteri dan anaknya masih menunggu kepulangannya hingga hari ini. Nasib Soehato tentu lebih baik daripada para korbannya: tidak pernah diadili, dan tidak pernah dipenjara. Soeharto adalah diktator yang berbahagia. Ingin mendengar dalih pintarnya? Dalam Pikiran, Ucapan dan Tindakan Saya (1989),”…kita harus mengadakan treatment, tindakan tegas. Tindakan tegas bagaimana? Yah, harus dengan kekerasan.”
Dalih acuh tak acuh inilah yang membenarkan terbunuhnya jutaan orang dan dipenjarakan, mahasiswa, petani, rakyat jelata ditembak mati hingga Peristiwa Trisakti 12 Mei 1998: Elang Mulia Lesmana, Heri Hartanto, Hendriawan Sie, Hafidin Royan; Peristiwa Tanjung Priok, Peristiwa 27 Juli, Tragedi Talang Sari, Tragedi 1965, Daerah Operasi Militer Aceh, Papua, dan lain-lain. Tanpa ada pertanggungjawaban apa pun. Mereka tetap menjadi korban.

Koruptor Nomor Satu di Dunia

Selain itu Tuan Jenderal (purn) SBY seharusnya ingat, bahwa praktik pidana korupsi Soeharto atas Yayasan Supersemar, Yayasan Amal Bhakti Muslim Pancasila dan lainnya, belum selesai di pengadilan hingga sekarang (Djokomoelyo, Proses Peradilan Soeharto, 2001). Tanda tanya korupsi Soeharto dan keluarga juga menggantung, harta korupsi Soeharto dan keluarga yang bernilai 15-35 miliar dolar AS ditetapkan Perserikatan Bangsa-Bangsa melalui Stolen Asset Recovery (StAR) Initiative  sebagai koruptor nomor satu di dunia, melalui korupsi politik, Meski, Soeharto pernah mengemukakan tidak memiliki uang sesen pun.

Apa itu korupsi politik? Korupsi politik didefinisikan sebagai, “penyalahgunaan mandat rakyat untuk keuntungan pribadi.” Ban Ki-moon, Sekretaris Jenderal Perserikatan Bangsa-Bangsa, patut diacungi jempol karena ia meluncurkan program global Stolen Asset Recovery Initiative itu di Markas Besar PBB, New York (17/9/2005). Sepuluh besar pemimpin politik (presiden atau perdana menteri) terkorup di dunia di dasarkan pada jumlah uang rakyat yang dijarah, (1) Soeharto (Indonesia), 15-35 miliar dolar AS; (2) Ferdinand E. Marcos (Filipina), 5-10 miliar dolar AS; (3) Mobutu Sese Seko (Kongo), 5 miliar dolar AS; (4) Sani Abacha (Nigeria); (5) Slobodan Milosevic (Serbia); (6) Jean-Claude Duvalier (Haiti); (7) Alberto Fujimori ((Peru); (8) Pavlo Lazarenko (Ukraina); (9) Arnoldo Aleman (Nikaragua) dan; (10) Joseph Estrada (Filipina).

Demikian juga Transparency International (25 Maret 2004) menempatkannya diurutan pertama dari sepuluh pemimpin politik terkorup di dunia. Menurut Newsweek (Januari 1998) nilainya 40 miliar dolar AS, sedang majalah Forbes menobatkan Soeharto orang terkaya keempat di dunia (Juli 28, 1997). Harta KKN-nya berkisar 60 miliar dolar AS, angka tengah dari perkiraan 40-80 miliar dolar AS, karena ke 7 anak dan cucunya memiliki 312-350 perusahaan di dalam dan luar negeri. Siapakah yang memegang harta Soeharto Inc.? Harta yang dikuasai Siti Hardiyanti Rukmana (74 Perseroan Terbatas/PT), Sigit Harjo Judanto (44 PT), Bambang Triatmodjo (60 PT), Siti Hediati  Hariadji (22 PT), Hutomo Mandala Putra (49 PT), Siti Hutami Endang Adiningsih (2 PT), dan Ari Harjo Wibowo (29 PT) dan 32 perusahaan di luarnegeri (Todung Mulya Lubis,dkk, Soeharto vs Time: Pencarian dan Penemuan Kebenaran, Penerbit Buku Kompas, 2001).

Ironisnya, Ketetapan MPR RI No.XI/MPR/1998 tanggal 13 November 1998 tentang Penyelenggaraan Negara yang Bersih dan Bebas KKN, masih berlaku, dan pasal 4 berbunyi: “Upaya pemberantasan korupsi, kolusi dan nepotisme harus dilakukan secara tegas terhadap siapa pun juga, baik pejabat negara, mantan pejabat negara, keluarga dan kroninya maupun pihak swasta/konglomerat termasuk mantan presiden Soeharto.”

Rezim Fasis Militerisme Orba

Tuan Jenderal (purn) SBY, anda juga haruslah ingat bahwa Orde Baru itu bukanlah rezim otoriter biasa tetapi adalah rezim fasis militerisme Orde Baru. Fasisme didefinisikan sebagai sistem yang menolak demokrasi, rasionalisme, dan parlementarisme. Menjunjung tinggi kekuasaan negara tak terbatas. Ciri fasisme Orba yang totaliter tidak jauh dari ciri yang dikemukakan Carl Friedrich dalam Encyclopedia of Social Sciences, Vol.5 dan 6 (1957).

 Pertama, sebuah ideologi dominan, menyeluruh, dan tertutup. Tidak disangsikan lagi Pancasila versi Soeharto, pada 1980-an dijadikan azas tunggal bagi seluruh organisasi politik dan kemasyarakatan. Kedua, satu partai yang menganut ideologi totaliter tersebut. Golongan Karya (sekarang Partai Golongan Karya) sebagai the ruling party dan kedua partai marginal PDI dan PPP adalah penganut ideologi Pancasila versi Soeharto yang tertutup dan totaliter tersebut. Ketiga, sistem intelijen militer maupun sipil yang mengawasi dan meneror kehidupan masyarakat. Rezim Orba memiliki lembaga ekstra konstitusional dan ekstra yudisial seperti Kopkamtib (Komando Pemulihan Keamanan dan Ketertiban), lalu berganti nama menjadi Bakorstanas (Badan Koordinasi Stabilitas Nasional). Di perguruan tinggi, melalui resimen mahasiswa (Menwa) memiliki seksi intelijennya sendiri. Keempat, kontrol tunggal semua aktifitas masyarakat sipil, seperti media massa dengan lembaga SIUPP, dan semua organisasi sosial politik dibawah UU Partai Politik dan Golkar, Ormas, dan Referendum. Kelima, watak teknokratis dalai menjlankan pembangunan dan sistem kapitalisme monopoli yang menghantam kebebasan buruh untuk berserikat, melakukan depolitisasi massa (floating mass) dan bekerjasama dengan kapitalisme internasional (modal, portofolio, dan TNC/MNC). Keenam, bersifat korporatis, rezim memecah masyarakat menjadi golongan fungsional, sehingga tidak perlu berhubungan dengan masyarakat secara langsung, hanya bersedia bersedia berhubungan dengan perwakilan dari golongan/kelompok korporatis itu. Adapun perwakilannya harus direstui atau sesuai keinginan rezim Orba.

Nah, Tuan Jenderal (purn) SBY, semua ciri di atas menjelmakan rezim Orba sebagai rezim antidemokrasi. Fasis militerisme Orba yang berkarakter korporatis inilah yang mematikan semua aktifitas  dan organisasi otonom masyarakat sipil yang merupakan ciri demokrasi. Watak korporatis dan kelompok korporatis ini menjadikan kekuasaan rezim Orba sebagai sebuah totalitas, dimana pelaku, dan lembaga seperti Golkar, adalah unit yang dalai kesatuan utuh sebuah mesin penggilas demokrasi. Selain itu, watak kelompok korporatis ini bercirikan, unit konstituennya terbatas, wadah tunggal karena memonopoli kepentingan tertentu,  mewajibkan keanggotaan, diatur secara hirarkis dan direstui, bahakn diciptakan sendiri untuk dapat dikendalikan oleh rezm Orba. Singkatnya, cara korporatis ini adalah kontrol yang dipaksakan, atau kooptasi untuk mempertahankan kepentingan rezim Orba. Kelompok korporatis seolah-olah mewakili suara masyarakat, padahal perpanjangan tangan rezim fasis Orba untuk memaksakan kepentingannya kepada masyarakat.


Penutup

Semoga Tuan Jenderal (purn) SBY mempertimbangkan surat terbuka saya yang menolak Jenderal Besar (purn) Soeharto menjadi pahlawan nasional. Sekali lagi tak masuk akal bila seorang diktator, koruptor dan pelanggar HAM menjadi pahlawan nasional Indonesia, seorang pencipta rezim fasis militerisme yang antidemokrasi selama 32 tahun. Ini penghinaan terhadap akal sehat dan kemanusiaan di Indonesia, dan di seluruh dunia, karena korupsi dan pelanggaran HAM  adalah saudara kembar kejahatan terhadap kemanusiaan.

Memang ingatan manusia terlalu pendek, tetapi rakyat juga dibombardir informasi palsu dan manipulatif sepanjang 32 tahun Orba dan 12 tahun sesudah reformasi, akibatnya Milan Kundera jadi benar dalam kasus dikatator, koruptor, dan pelanggar HAM seperti Jenderal Besar (purn) Soeharto, bahwa, ”the struggle of man against power is the struggle of memory against forgetting, “ (“The Book of Laughter and Forgetting”, 1996).

Tuan Jenderal (purn) SBY, saya akhiri surat penolakan terhadap pengangkatan Jenderal Besar (purn) Soeharto sebagai pahlawan nasional Indonesia, dengan mengutip Bertrand Russel (Pergolakan Pemikiran, YOI,1988), “Dunia penuh ketidakadilan, dan mereka yang memperoleh keuntungan dari ketidakadilan itu juga berwenang memberikan hadiah serta hukuman. Hadiah didapatkan oleh mereka yang bisa menemukan dalih-dalih yang pintar untuk mendukung ketidakadilan, dan hukuman didapatkan oleh mereka yang mencoba menghilangkan ketidakadilan tersebut.” Semoga anda Tuan Jenderal (purn) SBY bukan salah seorang dicemaskan Bertrand Russel, karena Salus Populi Suprema Lex : keselamatan rakyat adalah hukum tertinggi, bukan keselamatan seorang diktator, koruptor dan pelanggar HAM seperti Jenderal Besar (purn) Soeharto.

Nah, Tuan Jenderal (purn) SBY, penanda tegas sejarah gemilang demokrasi Indonesia dan keberpihakan moral kita adalah penolakan terhadap Soeharto sebagai pahlawan nasional.

Jakarta, 15 Oktober 2010

M. Fadjroel Rachman, Ketua Lembaga Pengkajian Demokrasi dan Negara Kesejahteraan (Pedoman Indonesia)

* * *







SBY-Budiono supaya melepaskan mandat
sebagai Presiden dan Wapres.





Berikut ini adalah bahan tentang perkembangan dan kesimpulan mengenai situasi ekonomi,
politik, dan kebudayaan di dalam negeri dan situasi internasional, yang dibuat  oleh KPP PRD dalam bulan Oktober 2010. Mengingat pentingnya bahan yang disiarkan oleh salah satu di antara berbagai partai atau organisasi kiri di Indonesia ini, maka di bawah ini disajikan teksnya secara selengkapnya.



Pandangan Politik KPP PRD

Terhadap Perkembangan Situasi Nasional dan Internasional



I. SITUASI  NASIONAL

A. Situasi Ekonomi:

1.   Derajat pelaksanaan ekonomi neoliberal saat ini menunjukkan, bahwa
di beberapa sektor ekonomi yang penting, modal asing telah mengambil
porsi yang lebih besar dibanding modal dalam negeri, misalnya jenis
tambang dan migas (± 80%), bank (± 50%), industri, jasa, dan 70%
saham di pasar modal. Perusahaan asing juga menguasai sektor perkebunan,
ritel, telekomunikasi, air minum, dan sektor strategis lainnya.

2.   Sebetulnya ini juga tercermin dalam PDB kita, yang sebagian besar
adalah hasil produksi orang dan perusahaan asing di Indonesia, namun
dibangga-banggakan pemerintah sebagai prestasi ekonomi. Jadi,
peningkatan PDB sebagian besar karena kontribusi asing. Sebagai
perbandingan mengenai hal ini, dalam Produk Domestik Bruto (PDB)
Indonesia, ekonomi nasional atau rakyat (UMKM) yang berjumlah 40,1 juta
atau 99,8% dari total pelaku ekonomi hanya menikmati 39,8% dari PDB,
sementara korporasi besar asing menikmati hingga 60,2%.

3.   Pertumbuhan industri dalam negeri terus merosot, hanya tercatat
3,5%, terendah dalam 10 tahun terakhir ini. Fenomena de-industrialisasi
dipicu oleh dua hal pokok; (1) kenaikan semua komponen biaya produksi,
seperti kenaikan tarif TDL dan kebutuhan energi lainnya. (2) produk
industry dalam negeri kehilangan daya kompetitifnya saat pasar dalam
negeri dibanjiri produk industry dari luar.

4.   Ekonomi rakyat, yang sekarang ini bertumpu pada pertanian dan
usaha-usaha kecil (UKM, industri rumah tangga, dan usaha informal),
semakin hancur akibat invasi dari retail-retail bermodal besar, seperti
Circle K, Alfamart, Seven-Eleven, dsb, yang sudah bisa masuk ke
kampung-kampung.

5.   Dalam hal pasar ini, sebuah data menunjukkan, bahwa ekonomi
nasional atau ekonomi rakyat hanya menempati 20% pangsa pasar nasional,
sementara korporasi besar asing dan domestik menguasai 80%.

6.   Sektor perbankan, yang seharusnya menjadi penyedia dana untuk
industri dan membiayai pembangunan, sekarang ini mulai dikuasai modal
asing. Karena didominasi oleh pihak asing, maka Bank Indonesia kesulitan
mengontrol suku bunga. Itu pula yang menyebabkan banyak industri di
dalam negeri yang kekeringan kredit.

7.   Sementara itu, selama lima tahun mengurus keuangan negara, Sri
Mulyani berhasil menambah total utang Indonesia sebesar Rp 275 triliun,
total utang kita saat ini sudah mencapai Rp1.588 triliun, lebih besar
daripada total APBN kita tiap tahunnya (Sebetulnya, dalam hitungan
kertas, Indonesia sudah bangkrut).

8.   Selain dengan retorika investasi dan membiayai pembangunan,
sebagian besar penarikan utang baru ini dilakukan untuk membiayai
"program belas kasihan" SBY-Budiono, seperti BLT, KUR, PNP
Mandiri, dan termasuk membiayai program anti-kemiskinannya Budiono
sebesar Rp. 55 Triliun.

9.   Pada kenyataannya, program belas kasihan itu tidak dimaksudkan
untuk mengangkat ekonomi rakyat (kaum miskin), melainkan untuk mendorong
konsumsi dan sekaligus sebagai penawar terhadap potensi ledakan sosial
akibat kemiskinan.

10.  Belum lagi korupsi yang merusak ekonomi nasional seperti rayap yang
memakan tiang-tiang bangunan. Tidak terhitung berapa jumlah anggaran
negara yang dimakan koruptor tiap tahunnya, dan itu terjadi merata di
seluruh departemen negara, di seluruh tingkatan (pusat hingga daerah),
di seluruh pelosok nusantara.



B. Situasi Politik

1.      Kenyataan di lapangan politik menggariskan beberapa hal;
Pertama, mulai terbongkarnya persekongkolan rejim neoliberal di dalam
negeri dan pihak asing dalam penyusunan puluhan Undang-Undang yang
berbau neoliberal. Kedua, ketidakmampuan atau ketidakcakapan SBY dalam
memimpin bangsa mulai dikritik secara luas, termasuk dari kalangan
internal kekuasaan itu sendiri. Ketiga, munculnya gesekan-gesekan yang
bersifat etnik, kedaerahan, antar-agama yang berpotensi memicu konflik
horizontal dan mendisorientasikan perjuangan nasional melawan
imperialisme.

2.      Ada petunjuk yang semakin jelas dan terang terkait keterlibatan
pihak asing dalam proses penyusunan UU berbau neoliberal di Indonesia.
Menurut BIN (badan intelijen negara), organisasi yang mengungkap laporan
ini, menyatakan bahwa ada 79 UU di DPR yang dikonsep oleh konsultan
asing. UU pro-neoliberal ini bukan saja begitu terbuka terhadap modal
asing dan liberalisasi ekonomi, tetapi juga mengacaukan sistim politik
kita ( UU Pemilu, Pilkada dsb).

3.      Rejim SBY-Budiono masih berusaha menggunakan politik pencitraan,
dengan dukungan media dan sejumlah intelektual kanan, untuk menjaga
kekuasaan politiknya dan mengaburkan berbagai persoalan-persoalan besar.
Hanya saja, politik pencitraan ini mengalami keterbatasan dalam
menghadapi sejumlah isu krusial dan membutuhkan ketegasan, seperti
sengketa perbatasan dengan Malaysia, kasus penyerangan HKBP, dsb.

4.      Lahirnya sejumlah orang-orang yang berani menabrak "tabu
politik", seperti kasus kolonel Adjie Suradji dan lain-lain, dan
berani menyampaikan kritiknya secara langsung mengenai lemahnya
kepemimpinan SBY. Ini sekaligus menandai "krisis kepemimpinan"
nasional dewasa ini yang penyebabnya, mengutip Pramoedya Ananta Toer,
adalah ketiadaan pemimpin yang berkarakter.

5.      Reshuffle kabinet hanya akan menjadi tambal sulam politik,
sekaligus untuk menata kembali koalisi pendukung pemerintah yang sempat
"kacau-balau" karena persoalan Century. Dipastikan, SBY akan
menjadikan "reshuffle" sebabagi alat untuk bargain politik
dengan rekan-rekan koalisinya, termasuk merangkul kekuatan dari luar
untuk bergabung.

6.      Isu "anti terorisme" terus dikembangkan sedemikian rupa
untuk tujuan-tujuan politik tertentu, misalnya pengalihan issue,
memelihara front dengan imperialism AS dan Australia, dan mengalihkan
atau mengaburkan musuh pokok perjuangan rakyat di dalam negeri.

7.      Pemunculan nama Sri Mulyani (SMI), kendati masih wacana
segelintir kaum liberal dan sosdem kanan, cukup meresahkan di tengah
kekosongan kandidat lain yang lebih progressif, demokratis, bersih, dan
pro-rakyat pada pemilu 2014. Diperkirakan, jika kandidat-kandidat
pilpres 2014 adalah Aburizal Bakrie, Prabowo Subianto, dan sebagainya
(semuanya bermasalah), maka SMI bisa menjadi "kuda hitam" yang
disokong penuh capital internasional.



II. Persoalan Budaya

1.      Perkembangan kebudayaan nasional seperti sudah terhenti,
tergantikan oleh proses penghancuran karakter dan jiwa sebagai bangsa
oleh imperialisme. Syarat-syarat untuk kemajuan kebudayaan, seperti
pendidikan, literature, kesenian, kesusastraan, dsb hendak dimatikan dan
ditundukkan oleh kapitalisme neoliberal, misalnya melalui konsumtifisme,
liberalism, komersialisasi, dan lain-lain.

2.      Contoh terbaru kebijakan terbaru di bidang pendidikan adalah
pemberlakuan RSI/RSBI, yang sebetulnya hanya "ganti papan nama"
tetapi kualitas dan mutu pendidikannya tetapi sama, namun harganya telah
dinaikkan berkali-kali lipat.

3.      Pendidikan kita masih berwatak kolonial, yaitu, bahwa pendidikan
berkualitas hanya bisa dinikmati kaum kaya, sementara rakyat miskin
hanya diberi kesempatan menikmati sekolah rendahan, yang nantinya akan
dilempar menjadi tenaga kerja murah.

4.      Film, lagu-lagu, majalah, tarian-tarian masih didominasi oleh
kebudayaan imperialis, yang bersifat cabul, membodohi, dan merusak moral
rakyat. Meskipun begitu, tidak bisa ditutupi adanya sedikit kemajuan di
perfilm nasional, yaitu dengan mulai dimunculkannya film-film inspiratif
seperti "Sang Pencerah", "Darah Garuda", "Sang
Pemimpi", "Indonesia tanah air beta", dsb.



III. Situasi Internasional

1.      Perkembangan politik internasional semakin menegaskan pergeseran
dari uni-polar menjadi multi-polar.

2.      Di Asia Timur, posisi AS memang masih dominan, khususnya melalui
Jepang, Korsel, dan Taiwan, sedangkan China sepertinya belum sukses
untuk menembus dominasi ini. Hegemoni AS semakin merosot di Irak dan
Afghanistan, dua medan perang yang gagal dikuasai sepenuhnya oleh pasukan
pendudukan AS dan aliansinya. Selain itu, AS juga kurang berhasil
menjaga halaman belakangnya sendiri, yaitu Amerika Latin, yang saat ini
sedang giat-giatnya membangun integrasi regional tanpa kehadiran Kanada
dan AS.

3.      Di Eropa, tempat dimana krisis baru saja dimulai, sejumlah
pemerintahannya mulai mengeluarkan kebijakan yang memangkas
kesejahteraan rakyatnya, seperti pemangkasan subsidi, memperpanjang usia
pensiun, dan lain sebagainya. Di Eropa juga ditandai dengan meningginya
politik rasialisme, seperti ditunjukkan oleh Perancis yang mengusir kaum
Gypsi, dan munculnya partai politik ultra-kanan penentang imigran.

4.      Untuk ASEAN, diketahui adanya peningkatan pembelian senjata,
terutama setelah isu sengketa perbatasan Indonesia-malaysia menguat.
Indonesia terus-menerus membeli pesawat tempur Sukhoi, sedangkan
Malaysia dan Philipina juga melakukan hal serupa.

5.      Secara global, ada kesimpulan mengenai meningkatnya
kecencerungan masing-masing negara bangsa untuk mengutamakan kepentingan
nasionalnya. Di eropa, kebangkitan isu-isu nasional ini telah memicu
lahirnya xenophobia, anti-imigran, danislamicphobia. Sementara di
negeri-negeri dunia ketiga, yang sebagian besar adalah bekas jajahan,
sentiment nasionalismenya cenderung progressif.

6.      Politik luar negeri Indonesia sangat perlu dikoreksi, sebab
tidak lagi mencerminkan politik luar negeri yang bebas –aktif,
melainkan politik luar negeri yang memihak kepada imperialism global,
khususnya kepada AS. Politik luar negeri Indonesia tidak mempunyai
karakter, tidak bermartabat, dan tidak mandiri terhadap imperialisme.



IV. Posisi Partai Rakyat Demokratik

Posisi Terhadap Persoalan Ekonomi:

1.      Tetap mengkampanyekan tiga program Tripanji persatuan nasional;
(1). Nasionalisasi perusahaan strategis asing, (2) penghapusan utang luar
negeri, (3) dan bangun pabrik (industrialisasi) nasional untuk
kesejahteraan rakyat.

2.      Mendorong kontrol negara dalam pengelolaan sumber daya alam,
terutama hasil tambang, pertanian, kehutanan, kelautan, dsb.

3.      Proteksi terhadap pasar di dalam negeri, khususnya untuk
menampung produk industri menengah dan kecil di dalam negeri.

Posisi Terhadap Persoalan Politik;

1.      Mendesak pencabutan seluruh UU pro-neoliberal;

2.      Pemberantasan korupsi tanpa pandang bulu.

Posisi Terhadap Persoalan Luar Negeri:

1.      Mengecam politik luar negeri SBY yang tidak bermartabat, tidak
berdaulat, dan pro-kepada imperialisme.

2.      Mengecam intervensi Amerika Serikat terhadap kedaulatan negara
lain, seperti Korea Utara, iran, Irak, Afghanistan, dan lain sebagainya.

3.      Menyerukan kepada bangsa-bangsa tertindas di dunia untuk kembali
kepada semangat Bandung 1955, hasil-hasil konferensi Asia-Afrika.

4.      Mendorong kerjasama antar bangsa untuk memajukan umat manusia.

Kesimpulan Umum Partai Rakyat Demokratik:

Sistim neoliberalisme telah menjadi penyebab paling pokok atas
keterpurukan ekonomi, politik, dan kebudayaan di dalam negeri.
Neoliberalisme juga menyebabkan politik luar negeri Indonesia tidak
bermartabat. Dan, dalam keadaan itu, SBY-Budiono telah menjadi bagian
dari masalah neoliberal ini.

Untuk itu, bagi Partai Rakyat Demokratik (PRD), pilihan satu-satunya dan
paling mungkin adalah gerakan banting stir atau memutar haluan, yakni
meninggalkan segala hal yang berbau neoliberalisme dan imperialism dan
kembali mewujudkan kedaulatan politik, ekonomi, dan budaya.



Taktik Perjuangan:

PRD sangat menyadari, bahwa untuk menghancurkan rejim neoliberal yang
sangat kuat ini, maka sandaran utamanya adalah gerakan massa yang sadar
(bewuste massa-actie). Untuk itu, dalam rangka mendorong kebangkitan
politik massa ini, PRD menganjurkan penggalangan konsultasi-konsultasi
umum dengan massa rakyat, misalnya menggalang tanda-tangan rakyat
Indonesia untuk mosi tidak percaya terhadap rejim neoliberal sekarang
ini. Ini akan dilakukan dengan terjun langsung ke kampung-kampung,
pemukiman, pabrik-pabrik, desa-desa, dengan mengetuk satu per satu pintu
rumah rakyat dan menawarkan petisi untuk mosi tidak percaya.

Ini dapat menghasilkan dukungan yang besar, sebuah potensi mobilisasi di
masa depan, sekaligus sebagai "legitimasi" paling sah untuk
mendesak SBY-Budiono melepaskan mandat sebagai Presiden dan Wapres.

Demikian pandangan politik kami.

Hentikan Neoliberalisme! Rebut (kembali) Kedaulatan Nasional!

Jakarta, 5 Oktober 2010



Komite Pimpinan Pusat Partai Rakyat Demokratik

(KPP PRD)



Agus Priyono , Ketua Umum

Gede Sandra, Sekretaris Jenderal

Tebet Dalam II G No. 1 Tebet. Jakarta Selatan – 12810

Telp/Fak.021-8354513, e-mail : kppprd@yahoo.com
mailto:kpp_prd@progressif.net>
