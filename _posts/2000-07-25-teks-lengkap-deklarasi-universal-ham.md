---
layout: post
title: Teks lengkap Deklarasi Universal HAM
date: 2000-07-25
---

Berhubung adanya berbagai reaksi terhadap tulisan tentang Deklarasi Universal Hak Asasi Manusia dan permintaan pemuatan kembali teksnya secara keseluruhan, maka di bawah ini disajikan dokumen penting tersebut selengkapnya. Semoga pemuatannya kembali memudahkan bagi mereka yang ingin menyimpannya sebagai bahan dokumentasi dan referensi dalam perjuangan untuk menegakkan Hak Asasi Manusia di bumi Indonesia, yang setelah mengalami masa gelap selama 32 tahun, sekarang ini masih sedang terus bersama-sama melawan bahaya laten Orde Baru.

(Catatan : Diterjemahkan dari teks asli bahasa Inggris dan dibandingkan dengan teks bahasa Prancis)

## Mukadimah (preamble)
Mengingat, bahwa penghargaan terhadap martabat (dignity) dan hak-hak yang setara dan tak terpisahkan (equal and inalienable rights) bagi semua anggota keluarga umat manusia (human family) adalah dasar bagi kemerdekaan, keadilan dan perdamaian di dunia,

Mengingat, bahwa pengingkaran dan pelecehan (disregard and contempt) terhadap hak manusia telah menyebabkan terjadinya tindakan-tindakan biadab yang telah menimbulkan kemarahan kesedaran umat manusia, dan bahwa munculnya dunia di mana ummat manusia dapat menikmati kebebasan untuk berbicara dan menganut kepercayaan (freedom of speech and belief) dan kebebasan dari ketakutan dan kekurangan (kemiskinan) telah diproklamasikan sebagai aspirasi bagi semua orang,

Mengingat, bahwa hak-hak manusia perlu sekali dilindungi oleh tegaknya hukum (protected by the rule of law), supaya orang tidak dipaksa, sebagai jalan terakhir, untuk membrontak terhadap tirani dan penindasan,

Mengingat, bahwa adalah sangat perlu untuk mendorong penggalangan hubungan bersahabat antara bangsa-bangsa,

Mengingat, bahwa rakyat-rakyat yang tergabung dalam PBB telah menegaskan dalam piagam ini kepercayaan mereka terhadap hak asasi manusia, terhadap martabat dan nilai-nilai perseorangan manusia (dignity and worth of the human person) dan hak-hak yang sama antara laki-laki dan perempuan, dan juga bertekad untuk mendorong kemajuan sosial dan tingkat hidup yang lebih baik dalam kebebasan yang lebih besar (to promote social progress and better standards of life in larger freedom),

Mengingat, bahwa negara-negara anggota PBB berjanji untuk mengusahakan dihormatinya dan ditrapkannya secara universal dan nyata hak-hak manusia dan kebebasan fondamental,

Mengingat, bahwa kesamaan pengertian (common understanding) mengenai hak-hak dan kebebasan-kebebasan ini (rights and freedoms) adalah sangat penting bagi pelaksaan piagam ini secara sepenuhnya,

Maka, Sidang Umum (PBB) memproklamasikan Deklarasi Universal Hak Asasi Manusia sebagai cita-cita bersama bagi semua rakyat dan semua bangsa (all peoples and all nations) supaya setiap individu (orang seorang) dan semua badan dalam masyarakat (every organ of society), dengan selalu memegang Deklarasi Universal ini dalam ingatan, berusaha lewat pengajaran dan pendidikan, untuk mendorong dihormatinya hak-hak dan kebebasan-kebebasan ini, dan juga lewat peraturan yang secara berangsur-angsur, baik secara nasional mau pun internasional, untuk mendapat pengakuannya dan pentrapannya secara universal dan nyata, baik antara rakyat-rakyat negara-anggota (PBB) sendiri, mau pun antara rakyat dalam wilayah juridiksinya (teks Mukadimah habis).

### Artikel 1
Semua mahluk manusia dilahirkan secara bebas dan memiliki martabat dan hak yang sama. Mereka mempunyai kenalaran (reason) dan kesedaran (conscience) dan kewajiban untuk bertindak antara yang satu dan lainnya dalam semangat persaudaraan (in a spirit of brotherhood).

### Artikel 2
Semua orang berhak untuk memiliki hak dan kebebasan seperti yang dicantumkan dalam Deklarasi ini, tanpa perbedaan apa pun dalam hal ras, warna kulit, kelamin, bahasa, agama, opini politik atau pun opini lainnya, asal kebangsaan atau asal sosial, perbedaan kekayaan, kelahiran atau status lainnya.

Lebih lagi, tidak diperbolehkan adanya pembedaan (no distinction shall be made) yang didasarkan atas status politik, juridis atau international negara atau teritori (wilayah) di mana ia menjadi warganegaranya, tanpa mempedulikan apakah negara itu merdeka, di bawah pengawasan, tidak otonom atau berada dalam kedaulatan yang terbatas.

### Artikel 3
Semua individu berhak untuk hidup, untuk menikmati kebebasan dan keamanan bagi pribadinya

### Artikel 4
Tidak seorangpun boleh diperlakukan dalam perbudakan (slavery) atau dalam perhambaan (servitude) dan perdagangan budak (slave trade) dilarang dalam segala bentuknya.

### Artikel 5
Tidak seorang pun boleh disiksa (torture) atau mendapat hukuman dan perlakuan yang kejam, tidak berperikemanusiaan dan merendahkan martabat manusia (cruel, inhuman or degrading treatment).

### Artikel 6
Dimana pun, semua orang berhak untuk mendapat pengakuan sebagai seseorang di depan hukum (recognition everywhere as a person before the law).

### Artikel 7
Semua orang mempunyai kedudukan yang sama di depan hukum, dan tanpa kecuali berhak untuk mendapat perlindungan hukum yang sama. Semua orang berhak untuk mendapat perlindungan terhadap diskriminasi apa pun yang melanggar Deklarasi ini dan juga terhadap semua hasutan yang menganjurkan diskriminasi itu (against any incitement to such discrimination).

### Artikel 8
Setiap orang berhak untuk mengadukan kepada pengadilan nasional yang kompeten semua pelanggaran terhadap hak asasinya yang dijamin oleh Konstitusi atau undang-undang.

### Artikel 9
Seorang pun tidak boleh secara sewenang-wenang ditangkap, ditahan atau di-exilkan (arbitrary arrest, detention or exile).

### Artikel 10
Semua orang berhak, dalam kedudukan yang sama, untuk menuntut agar urusannya bisa diperiksa secara adil dan secara terbuka oleh pengadilan yang bebas dan imparsial (tidak memihak) untuk menentukan hak dan kewajibannya, atau memeriksa semua dakwaan pelanggaran kriminal (any criminal charge) yang ditujukan kepadanya.

### Artikel 11
1. Setiap orang yang dituduh melakukan tindakan pidana haruslah dianggap tidak bersalah sampai kesalahannya itu bisa dibuktikan secara hukum oleh pengadilan terbuka di mana ia mempunyai semua jaminan yang dibutuhkan bagi pembelaannya (all the guarantees necessary for his defence).
2. Tidak seorang pun boleh dihukum akibat suatu tindakan atau ketidaksengajaan (kealpaan, omission) yang pada waktu kejadian itu, menurut hukum nasional atau internasional, tidaklah merupakan tindakan pidana. Demikian juga, hukuman yang lebih berat tidak boleh dijatuhkan ketimbang hukuman yang berlaku pada waktu pelanggaran itu diperbuat.

### Artikel 12
Tidak seorangpun boleh secara sewenang-wenang diganggu (arbitrary interference with his privacy) kehidupan pribadinya, keluarganya, rumah tinggalnya atau surat-menyuratnya, dan dilanggar kehormatannya atau nama-baiknya (reputation). Semua orang mempunyai hak atas perlindungan hukum terhadap gangguan atau pelanggaran semacam itu.

### Artikel 13
1. Di dalam wilayah suatu negeri, semua orang berhak untuk bersikulasi secara bebas (freedom of movement) atau menentukan tempat tinggal menurut pilihannya.
2. Semua orang berhak untuk meninggalkan setiap negeri, termasuk negerinya sendiri dan untuk kembali kenegerinya sendiri (to leave any country, including his own, and to return to his country).

### Artikel 14
1. Menghadapi suatu persekusi, semua orang berhak untuk mencari tempat perlindungan (asylum) dan mendapatkan asylum dari negeri lain.
2. Hak ini tidak boleh dituntut dalam hal pengusutan terhadap kejahatan yang benar-benar berdasar kriminal (non-political crimes) atau terhadap tindakan- tindakan yang bertentangan dengan tujuan dan prinsip PBB.

### Artikel 15
1. Semua orang mempunyai hak atas kewarganegaraan (nationality)
2. Tidak seorangpun bisa dicabut kewarganegaraannya secara sewenang-wenang, atau dilarang haknya untuk merobah kewarnegaraannya.

### Artikel 16
1. Semua orang, laki-laki maupun perempuan dewasa, tanpa pembatasan ras, kebangsaan atau agama, berhak untuk kawin dan membentuk rumahtangga. Mereka mempunyai hak yang sama mengenai masalah perkawinan, selama perkawinan dan ketika perceraian.
2. Perkawinan hanyalah dapat dilaksanakan dengan persetujuan bebas dan penuh (free and full consent) antara calon suami-istri.
3. Keluarga adalah kelompok (group unit) masyarakat yang alamiah dan fondamental dan berhak atas perlindungan oleh masyarakat dan negara.

### Artikel 17
1. Semua orang berhak memiliki harta-benda, baik sendiri-sendiri maupun secara bersama-sama (in association with others).
2. Tidak seorangpun boleh dirampas harta-bendanya secara sewenang-wenang.

### Artikel 18
Semua orang berhak untuk mempunyai kebebasan fikiran, keyakinan dan agama (freedom of thought, conscience and religion). Hak ini mencakup kebebasan untuk mengganti agama atau kepercayaannya, dan kebebasan untuk secara sendirian atau bersama-sama dengan orang lain, baik di depan umum maupun di tempat tersendiri (private) memanifestasikan agamanya atau kepercayaannya lewat pendidikan, praktek, sembahyang dan upacara (worship and observance).

### Artikel 19
Semua orang mempunyai hak atas kebebasan berfikir dan menyatakan pendapat (the right to freedom of opinion and expression); hak ini mencakup kebebasan untuk mempunyai pendapat tanpa mendapat gangguan (to hold opinions without interference) dan kebebasan untuk mencari, memperoleh dan menyebarkan informasi dan gagasan (to seek, receive and impart information and ideas), lewat media yang manapun dan tanpa memandang perbatasan negara.

### Artikel 20
1. Semua orang mempunyai hak untuk menyelenggarakan rapat atau perkumpulan yang bertujuan damai ((peaceful assembly and association).
2. Tidak seorang pun boleh dipaksa untuk menjadi anggota sesuatu perkumpulan.

### Artikel 21
1. Semua orang berhak untuk ambil bagian dalam pemerintahan negerinya, secara langsung atau lewat perwakilannya yang dipilih secara bebas.
2. Setiap orang berhak untuk mendapat akses yang sama pada jabatan pemerintahan negerinya (equal acces to public service in his country).
3. Kehendak rakyat haruslah menjadi landasan bagi otoritas pemerintah: kehendak rakyat ini haruslah dinyatakan oleh pemilihan umum secara periodik dan jujur, lewat pemungutan suara secara universal dan setara (by universal and equal suffrage) dan diselenggarakan dengan suara rahasia (by secret vote) atau dengan prosedur pemungutan suara secara bebas lainnya

### Artikel 22
Setiap orang, sebagai anggota masyarakat, mempunyai hak jaminan sosial (his right to social security), dan mendapat bagian dari realisasi, lewat usaha nasional dan kerjasama internasional dan sesuai dengan pengaturan dan kemampuan setiap negaranya, atas hak ekonomi, sosial dan kebudayaan yang sangat dibutuhkan bagi martabatnya dan pengembangan kepribadiannya secara bebas (indispensable for his dignity and the free development of his personality).

### Artikel 23
1. Setiap orang mempunyai hak untuk bekerja, untuk menentukan pilihan pekerjaannya secara bebas, untuk bekerja dengan syarat-syarat yang adil dan mendapat perlindungan dari bahaya pengangguran.
2. Setiap orang, tanpa diskriminasi apa pun, berhak untuk menerima upah yang sama untuk pekerjaan yang sama.
3. Setiap orang yang bekerja mempunyai hak untuk menerima upah yang adil dan menguntungkan untuk memberikan jaminan baginya sendiri dan keluarganya atas kehidupan yang sesuai dengan martabat manusia, dan ditambah, kalau perlu, dengan cara-cara proteksi sosial lainnya.
4. Setiap orang mempunyai hak untuk membentuk serikat-buruh atau bergabung di dalamnya (to form and to join trade unions) demi melindungi kepentingannya.

### Artikel 24
Setiap orang mempunyai hak untuk mendapat istirahat dan hiburan, termasuk dibatasinya jam kerja dan mendapat hari libur yang dibayar, menurut batas-batas yang masuk akal.

### Artikel 25
1. Setiap orang mempunyai hak atas standar hidup yang memadai bagi kesehatan dirinya dan keluarganya, termasuk makan, pakaian, perumahan, pengobatan, dan pelayanan sosial, dan atas jaminan dalam menghadapi pengangguran, sakit, cacad, kematian suami atau istri (widowhood), hari-tua, atau menghadapi situasi kehidupan sulit yang di luar kemauannya.
2. Masa keibuan (motherhood) dan masa kekanakan (childhood) berhak untuk mendapatkan pertolongan dan bantuan khusus. Semua anak yang lahir, baik yang lahir dalam perkawinan atau di luarnya, harus menerima proteksi sosial yang sama (same social protection).

### Artikel 26
1. Setiap orang mempunyai hak atas pendidikan. Pendidikan haruslah bebas beaya, setidak- tidaknya bagi pendidikan tahap elementer (elementary stage) dan dasar. Pendidikan dasar haruslah wajib. Pendidikan teknik dan kejuruan (professional) haruslah tersedia untuk umum dan pendidikan tinggi harus terbuka bagi semua dengan hak yang sama berdasarkan merit masing-masing.
2. Pendidikan harus diarahkan untuk pengembangan sepenuhnya kepribadian seseorang sebagai manusia (full development of the human personality) dan untuk memperkokoh dihargainya hak-hak manusia dan kebebasan-kebebasan dasar (fundamental freedoms). Pendidikan ini harus mempromosikan saling pengertian, toleransi dan persahabatan antara semua bangsa, grup sosial atau agama, dan memperkuat aktivitas PBB untuk mempertahankan perdamaian.
3. Orang tua anak mempunyai hak yang utama (prior right) untuk memilih jenis pendidikan yang harus diberikan kepada anak mereka.

### Artikel 27
1. Setiap orang berhak untuk berpartisipasi secara bebas dalam kehidupan kebudayaan masyarakatnya, menikmati kesenian dan memperoleh bagian dari kemajuan ilmu beserta hasil-hasilnya.
2. Setiap orang mempunyai hak untuk mendapat perlindungan atas kepentingan moral atau material yang dilahirkan oleh produk ilmiah, literer atau artistik yang diciptakannya.

### Artikel 28
Setiap orang mempunyai hak atas adanya orde sosial dan internasional, di mana hak-hak dan kebebasan-kebebasan yang dicantumkan dalam Deklarasi ini dapat direalisasi secara sepenuhnya.

### Artikel 29
1. Setiap orang mempunyai kewajiban terhadap masyarakatnya di mana dimungkinkan pengembangan kepribadiannya secara bebas dan sepenuhnya.
2. Dalam mempertahankan hak-hak dan kebebasan-kebebasannya, setiap orang harus dikenakan pembatasan oleh undang-undang yang tujuannya adalah semata-mata untuk mengakui dan menghormati secara selayaknya hak dan kebebasan orang lain dan untuk memenuhi tuntutan moral, ketertiban umum dan kesejahteraan bersama dalam suatu masyarakat demokratis.
3. Hak-hak dan kebebasan-kebebasan ini tidak dapat, bagaimana pun juga, dijalankan secara berlawanan dengan tujuan dan prinsip-prinsip PBB.

### Artikel 30
Tidak ada satu pun bagian Deklarasi ini bisa diartikan oleh suatu negara, grup atau perseorangan, sebagai hak untuk melakukan kegiatan apa pun atau melancarkan tindakan apa pun yang bertujuan untuk menghancurkan semua hak-hak dan kebebasan yang dicantumkan di dalamnya.

(Teks dokumen habis di sini)
