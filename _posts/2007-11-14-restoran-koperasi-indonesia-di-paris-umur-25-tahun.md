---
layout: post
title: Restoran koperasi INDONESIA di Paris umur 25 tahun !
date: 2007-11-14
---

Restoran koperasi INDONESIA di Paris akan merayakan ulang tahunnya yang ke-25, yang kali ini akan diperingati secara agak lain dari pada biasanya, yaitu dengan mengadakan peringatan selama tiga malam berturut-turut, dari tanggal 23 sampai 25 November 2007.

Selama peringatan yang tiga malam itu akan diundang para tokoh berbagai kalangan dan organisasi di Perancis serta para sahabat, baik Indonesia maupun yang lain, yang dalam masa-masa yang lalu sudah memberi sumbangan dengan macam-macam bentuk dan cara, sehingga restoran koperasi ini bisa terus berjalan dengan baik sampai sekarang.

Restoran INDONESIA adalah usaha kolektif yang dilahirkan oleh SCOP Fraternité (“Persaudaraan”), yang dalam tahun 1982 telah didirikan oleh 4 orang Perancis dan 4 orang Indonesia ( A. Umar Said, Budiman Sudharsono, Sobron Aidit, dan JJ Kusni) yang waktu itu datang bermukim di Perancis sebagai orang-orang yang mendapat asil politik karena akibat peristiwa 65 di Indonesia.

Ciri-ciri yang “unik” restoran INDONESIA

Usaha kolektif ini mempunyai sejarah dan ciri-ciri yang “unik” atau, boleh dikatakan, agak “luar biasa” dibandingkan dengan restoran-restoran biasa lainnya.Tentang “keunikan” restoran koperasi INDONESIA di Paris ini sudah banyak ditulis, baik yang dalam bentuk artikel-artikel maupun dalam bentuk buku-buku. Dan buku yang terakhir adalah yang ditulis oleh JJ Kusni, yang berjudul “Membela martabat diri dan Indonesia” (penerbitan Ombak, Jogya, 2005)

Di antara ciri-ciri yang menonjol dari restoran INDONESIA ini adalah bahwa ia didirikan dalam bentuk SCOP , singkatan dari “Société Coopérative Ouvrière de Production” (perusahaan koperasi pekerja untuk produksi). SCOP adalah suatu badan hukum yang di Perancis dikenal sebagai usaha di bidang koperasi yang mengutamakan persamaan hak dan kewajiban, dan mementingkan segi persaudaraan, mengedepankan kesetiakawanan dan mentrapkan prinsip-prinsip kolektif dalam pengelolaan perusahaan. Pada garis besarnya, dan secara umum, restoran koperasi INDONESIA dijalankan dengan tujuan-tujuan yang disebut dalam bahasa Perancisnya “l’économie sociale” atau “l’économie solidaire”, atau juga istilah lainnya “l’économie alternative”.

Bahwa restoran INDONESIA -- dalam tahun 1982 -- telah dilahirkan sebagai koperasi (atau SCOP) untuk menjalankan usaha-usahanya, adalah sudah merupakan salah satu aspek yang unik dari kegiatan kolektif ini Sebab, dengan koperasi (atau SCOP) sebagai dasar usaha bersama ini, maka inilah yang membedakan restoran INDONESIA agak lain dari pada restoran-restoran lainnya di Paris khususnya atau di Perancis pada umumnya.

Di daerah Paris dan sekitarnya ada ribuan restoran yang menghidangkan masakan dari berbagai negeri di dunia, antara lain :Perancis, Spanjol, Portugal; Italia, Rusia, Polandia, Yugoslavia, Maroko, Aljazair, Tunisia, Libanon, Mesir, Yahudi, India, Pakistan, Korea, Jepang, Cina, Amerika Latin, Cuba, dan Indonesia. Patutlah kiranya disebutkan di sini bahwa di Paris hanya ada 2 restoran yang menghidangkan masakan-masakan Indonesia. Yaitu restoran koperasi INDONESIA dan restoran JAKARTA-BALI.

Salah satu segi yang menarik perhatian bagi banyak orang di Paris (dan Perancis umumnya) dari restoran INDONESIA adalah bahwa restoran ini dikelola secara koperasi. Sebab, meskipun di daerah Paris dan sekitarnya ada banyak sekali restoran, selama ini hanyalah ada 3 restoran yang dikelola secara koperasi dan berbentuk SCOP. Dan, di antara 3 restoran koperasi yang ada di Paris ini terdapat dua restoran Perancis yang menyajikan masakan Barat dan hanya satu yang menghidangkan masakan Indonesia.

Arti dan tujuan SCOP Fraternité

Hal lain yang sangat menarik dari sejarah restoran koperasi INDONESIA yalah bahwa pendiri-pendirinya terdiri dari orang-orang Perancis dan Indonesia, yang bersama-sama membentuk badan koperasi yang diberi nama Fraternité (“Persaudaraan”). Di belakang nama Fraternité ini terkandung salah satu tujuan besar yang menjadi selogan yang terkenal di Perancis, yaitu “Liberté, Egalité, Fraternité” (Kebebasan, Persamaan dan Persaudaraan). Selogan ini sudah terkenal semenjak Revolusi di Perancis dalam tahun-tahun 1780-an. Badan hukum yang bernama Fraternité inilah yang melahirkan restoran koperasi INDONESIA.

Dipilihnya nama Fraternité (“Persaudaraan”) untuk badan hukum yang mendirikan restoran koperasi INDONESIA ini ada hubungannya yang erat sekali dengan masalah-masalah yang dihadapi pada waktu itu, yaitu sekitar permulaan 1980-an. Pada waktu itu mulai banyak orang Indonesia, yang tadinya terpaksa bermukim di berbagai negeri (antara lain : di Tiongkok, Korea Utara, Eropa Timur), yang berdatangan minta suaka politik (asil politik) di Perancis. .

Sesudah mendapat asil politik di Perancis mereka menghadapi kesulitan yang lain, yaitu masalah penghidupan. Untuk bisa hidup normal sebagai orang biasa lainnya, mereka yang sudah mendapat asil politik ini ingin bekerja. Bekerja apa saja, atau sebagai apa saja, asal bisa mandiri. Tetapi, justru mencari pekerjaan inilah yang pada permulaan tahun 1980 itu sudah makin sulit, karena pengangguran mulai membengkak dimana-mana. Bagi banyak orang Perancis sendiri sudah mulai sulit untuk mencari pekerjaan, apalagi bagi orang asing (yang berasal dari Indonesia).
Untuk bisa mendapatkan pekerjaan apa saja ini banyak sekali halangannya bagi pendatang-pendatang baru ini.

Menciptakan kerja sendiri

Mengingat hal-hal itu, ditambah dengan berbagai masalah lainnya, maka satu-satunya jalan adalah menciptakan kerja sendiri bagi mereka yang baru datang dengan mendapatkan asil politik ini Menciptakan kerja sendiri adalah dalam rangka mentrapkan prinsip berdikari dan mengentaskan diri keluar dari keadaan ketergantungan atau hidup dari bantuan (bahasa Perancisnya : assistanat). Di samping itu, menciptakan kerja sendiri dan hidup berdikari ini ada hubungannya juga dengan usaha untuk mempertahankan harga diri, di tengah-tengah situasi ketika Perancis sedang dibanjiri oleh political refugees dari berbagai negeri, yang kebanyakan terpaksa harus hidup dari bantuan, dan untuk jangka lama pula.

Gagasan untuk mendirikan alat kolektif untuk hidup mandiri dan keluar dari keadaan ketergantungan (assistanat) dan dalam semangat persaudaraan ini ternyata mendapat sambutan yang besar dan hangat dari berbagai fihak di Perancis dan negeri-negeri lainnya. Hal ini tidak saja tercermin dengan adanya 4 orang Perancis yang menaruh simpati besar terhadap rencana untuk mendirikan koperasi bagi political refugees (terutama yang dari Indonesia) dan bahkan bersemangat ikut menjadi anggota badan pendirinya. Dari segi ini pulalah kelihatan sangat menonjolnya– dan dengan nyata sekali ! -- manifestasi isi dan makna “Fraternité” (“Persaudaraan”)

Tergabungnya 4 orang Perancis dalam usaha kolektif yang berbentuk koperasi ini bukan hanya mengandung aspek humaniter (perikemanusiaan) saja, melainkan juga sarat dengan aspek politik. Sebab, orang-orang Perancis yang menjadi anggota badan pendiri koperasi ini tahu betul bahwa orang-orang yang akan diajak dalam usaha kolektif ini adalah orang-orang Indonesia yang dimusuhi oleh rejim militer Suharto.

Restoran yang “beda” dengan lainnya

Mereka tahu bagaimana politik rejim Orde Baru sejak peristiwa 65 dan sejak kudeta Suharto terhadap presiden Sukarno. Karena itulah mereka menaruh simpati atau bersikap hangat terhadap orang-orang Indonesia yang minta suaka politik di Perancis, dan berusaha membantu sebisa mungkin dan dengan segala cara. Semua latarbelakang yang demikian itulah yang menjadikan badan usaha Fraternité (yang melahirkan restoran INDONESIA) mempunyai juga arti politik, dan yang, karenanya, berlainan dengan kebanyakan restoran lainnya.

Bolehlah kiranya dikatakan bahwa kalau dilihat dari berbagai segi , restoran koperasi INDONESIA di Paris memang tidak ada persamaannya dengan kebanyakan restoran-restoran Indonesia, baik di Indonesia maupun di negeri-negeri lainnya. Artinya, dalam sejarah pe-restoranan Indonesia, baru inilah yang mempunyai latarbelakang sejarah serta ciri-ciri yang demikian itu. Oleh karena itulah, ditambah dengan segala pengalaman “berjuang” selama seperempat abad, maka ada orang-orang Indonesia yang mengatakan bahwa restoran INDONESIA betul-betul “legendaris” , atau, seperti dalam dongeng saja.

Ciri-ciri yang “unik” dari restoran koperasi INDONESIA, itu bukanlah hanya yang sudah disebutkan di atas itu semuanya, melainkan juga masih ada lagi lainnya, yang juga menarik dan patut diketahui oleh banyak orang, dalam rangka peringatan ulangtahunnya yang ke 25 ini.

Umpamanya, adanya buku tamu (dalam bahasa Perancisnya dikenal dengan sebutan “livre d’or” atau “buku emas”), yang jumlahnya sekarang sudah 24 jilid. Karena jarang sekali, atau bahkan tidak ada sama sekali restoran lainnya yang mempunyai buku-tamu yang sebanyak itu, dan yang isi dan fungsinya seperti yang dipunyai restoran INDONESIA.

Tokoh-tokoh yang menjadi sahabat

Banyaknya tokoh-tokoh penting dari Perancis atau berbagai negeri lainnya yang pernah menjadi langganan atau tamu restoran juga merupakan ciri lainnya dari usaha kolektif ini. Di antara para tamu penting-penting itu ada yang kemudian menjadi sahabat restoran, antara lain Madame Danielle MITTERRAND (istri mendiang presiden Perancis, François MITTERRAND dan juga Louis JOINET yang pernah menjabat sebagai penasehat hukum dari 5 Perdana Menteri Perancis berturut-turut dan hakim agung di Perancis)

Salah satu hal yang juga patut dicatat dalam sejarah restoran INDONESIA ialah sikap Pascal LUTZ, seorang Perancis yang selama 20 tahun menjadi manager (atau direktur) sukarela restoran koperasi ini. Ia mau, dengan senang hati, dan selama itu, menjadi pimpinan usaha kolektif ini, tanpa mendapat gaji atau bayaran sedikitpun. Berkali-kali diusulkan kepadanya, untuk menerima imbalan, sebagai uang saku atau ganti uang bensin, tetapi ia selalu menolaknya. Ia mengatakan kepada semua orang bahwa ia bersedia menjadi manager sukarela (bahasa Perancisnya : gérant bénévole) karena ia menaruh simpati kepada tujuan usaha kolektif yang “unik” ini.

Restoran koperasi INDONESIA yang sekarang sudah berumur 25 tahun ini menjadi kebanggaan bagi orang-orang yang pernah bekerja di dalamnya dengan berbagai macam suka-duka atau jerih payah, sehingga memungkinkan usaha kolektif ini bisa bertahan sampai sekarang, bahkan mendapat sukses-sukses yang tidak sedikit. Kebanggaan ini juga telah dinyatakan oleh banyak kalangan di Perancis dan di berbagai negeri, termasuk di Indonesia.

Prestasi yang patut dicatat

Dari itu semua, nyatalah bahwa kumandang nama restoran INDONESIA terdengar di banyak tempat dan kalangan. Dan nyata jugalah bahwa banyak orang menganggap bahwa restoran ini mempunyai ciri-ciri dan sejarah yang agak berbeda dengan kebanyakan restoran lainnya. Restoran INDONESIA juga merupakan suatu prestasi yang patut dicatat yang telah dicapai oleh orang-orang yang terpaksa berjuang di luar tanah-air mereka, akibat situasi politik. Dalam hal ini, peran Suyoso sebagai manager restoran sejak lama adalah besar sekali dan amat penting, dengan dibantu Didien sebagai wakilnya.

Untuk memperingati hal-hal itu semualah maka diadakan peringatan ulangtahun ke-25 selama tiga malam sebagai kenang-kenangan bagi mereka yang pernah ikut membesarkan dan merawatnya bersama-sama, dan sebagai penghargaan dan pernyataan terimakasih kepada semua orang dan kalangan (baik yang Indonesia maupun yang bukan) yang pernah memberikan bantuan dan simpati dalam segala bentuk dan cara.

Peringatan bersama selama tiga malam ini bisa juga diartikan sebagai manifestasi dari harapan para sahabat dan para simpatisan agar usaha kolektif yang sudah dibangun dengan cita-cita kesetiakawanan, persahabatan, kebebasan, persamaan, persaudaraan (liberté, égalité, fraternité) ini selanjutnya bisa berlangsung terus dengan sukses-sukses yang lebih besar.

PS. Bagi mereka yang ingin menyampaikan pesan-pesan atau harapan-harapan (atau pendapat) berkaitan dengan tulisan tentang ulangtahun ke-25 restoran koperasi INDONESIA ini dapat mengirimkannya (lewat E-mail) sebelum tanggal 23-25 November ataupun sesudahnya.
