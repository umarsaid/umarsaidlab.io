---
layout: post
title: Satu tahun pemerintahan SBY didemo di 40 kota
date: 2010-10-22
---

Peringatan 1 Tahun Kabinet Indonesia Bersatu jilid II, yang diadakan pada tanggal 20 Oktober 2010 membuktikan kepada rakyat Indonesia bahwa pemerintahan SBY-Budiono memang betul-betul sudah makin tidak populer, sehingga berbagai ragam pernyataan kritis dan keras disuarakan secara bertubi-tubi oleh banyak kalangan.

Dalam rangka peringatan ini berbagai pakar atau tokoh penting, dan banyak organisasi massa telah mengeluarkan pendapat mereka tentang kegagalan pemerintahan SBY-Budiono. Di antara organisasi-organisasi itu banyak yang menuntut supaya SBY-Budiono mundur atau meletakkan jabatannya.

Mereka terdiri dari golongan yang berbeda-beda, seperti antara lain : Front Oposisi Rakyat Indonesia, Gerakan Indonesia Baru, Petisi 28, Front Perjuangan Rakyat, Bendera, PRP, KASBI, PRD, KPRM-PRD,

Tetapi fenomena yang paling menonjol dalam peringatan satu tahun kabinet SBY-Budiono ini  adalah besarnya dan luasnya demonstrasi-demonstrasi yang dilakukan oleh mahasiswa di banyak kota di Indonesia.

Bukan hanya di kota-kota besar saja – seluruhnya lebih dari 40 kota !  --  berbagai kalangan mahasiswa sudah melancarkan aksi-aksi untuk melampiaskan kemarahan mereka kepada Presiden SBY, bahkan sampai ke kota-kota kabupaten, antara lain di Gorontalo, Lamongan, Bogor, Solo, Kupang, Makassar, Denpasar, Pamekasan, Malang, Surabaya, Jember, Semarang, Purwokerto, Tasikmalaya, , Samarinda, Balikpapan, Palu, , Medan, Jambi, Lampung, Palembang, dll.

Fenomena yang penting  lainnya dalam aksi-aksi yang dilancarkan oleh mahasiswa ini adalah ikut sertanya berbagai macam kalangan mahasiswa dari golongan Islam, nasionalis, dan kiri (atau berhaluan sosialis), yang tergabung dalam HMI, Ikatan Mahasiswa Muhammadiyah, Gerakan Mahasiswa Nasional Indonesia (GMNI), LMND (Liga Mahasiswa Nasional untuk Demokrasi), PMKRI,

Generasi muda makin bersikap independen

Demonstrasi-demonstrasi yang dilancarkan oleh kalangan mahasiswa di kota-kota yang sampai sebanyak itu menunjukkan bahwa sebagian besar generasi muda bangsa kita sudah mengambil sikap yang makin independen dan juga makin kritis terhadap para tokoh partai yang duduk dalam kabinet SBY dan DPR atau lembaga-lembaga negara lainnya.

Menjauhnya (atau ketidak-tergantungannya) sebagian besar generasi muda (dalam hal ini kalangan mahasiswa) dari kalangan partai-partai politik yang duduk dalam koalisi pemerintahan SBY dan DPR (atau DPRD) adalah perkembangan penting. Sebab, partai-partai yang menyokong pemerintahan SBY sudah menunjukkan kualitasnya dan integritasnya, yang umumnya mengecewakan rakyat banyak.

Jadi, kritik tajam terhadap banyak kebijakan presiden SBY, yang pada umumnya memang megecewakan banyak orang, adalah hal yang wajar. Tetapi, dengan  mengkritik atau menghujat presiden SBY, sebenarnya mereka juga menyatakan kemarahan mereka kepada partai-partai pendukung koalisi presiden SBY Ini adalah fenomena yang penting dan menarik sekali, sebab sikap politik dari sebagian besar generasi muda bangsa kita yang demikian itu menimbulkan harapan bahwa hari kemudian bangsa kita tidak akan terus-menerus dikangkangi oleh golongan-golongan yang mentalnya tidak pro-rakyat atau moralnya bejat dan korup.

Dan lagi, bahwa kalangan mahasiswa yang tergabung dalam IMM (Ikatan Mahasiswa Muhammadiyah) dan HMI (Himpunan Mahasiswa Islam) ikut bicara keras atau menyuarakan secara santer terhadap neo-liberalisme dan keterpurukan ekonomi bangsa, dan juga menolak pemberian gelar pahlawan kepada Suharto, adalah perkembangan yang amat penting dalam golongan Islam di Indonesia.

Sikap yang anti-neoliberalisme dan anti-pemberian gelar pahlawan kepada Suharto yang ditunjukkan oleh generasi muda Islam adalah petunjuk penting yang menandakan bahwa situasi di Indonesia sedang secara sedikit demi sedikit memasuki masa-masa perubahan. Sikap generasi muda Islam yang demikian ini, kalau dikembangkan terus akan merupakan sumbangan yang besar sekali dalam perjuangan bangsa untuk mengadakan perubahan-perubahan besar dan fundamental menuju Indonesia Baru,

Kepercayaan terhadap kepemimpinan SBY makin merosot

Aksi-aksi yang dilakukan oleh berbagai kalangan, terutama oleh kalangan mahasiswa di banyak kota itu, mengindikasikan bahwa kepercayaan terhadap kepemimpinan presiden SBY sudah makin merosot. Kenyataan bahwa kalangan mahasiswa di Makasar mengadakan aksi-aksi besar-besaran  untuk menolak kedatangan presiden SBY di kota itu adalah pukulan besar bagi seorang kepala negara. Juga pembakaran atau dirobek-robeknya foto presiden SBY di berbagai aksi-aksi adalah  inidikasi yang jelas tentang rendahnya martabat dan kewibawaan presiden SBY di mata para demonstran.

Tayangan berbagai siaran televisi tentang aksi-aksi di banyak tempat itu menyajikan gambar-gambar betapa besarnya dan kerasnya bentrokan-bentrokan antara aparat keamanan dan demonstran. Juga kelihatan dalam tayangan-tayangan itu betapa gigihnya sikap para demonstran,  yang dalam aksi-aksi mereka itu membawa berbagai spanduk dengan macam-macam slogan anti SBY. Ada juga tayangan televisi yang menyajikan demonstran-demonstran menyanyikan lagu « Revolusi sampai mati ».

Kiranya bolehlah dikatakan bahwa aksi-aksi dalam rangka peringatan setahun kabinet Indonesia Bersatu tanggal 20 Oktober itu termasuk gerakan yang paling besar atau paling luas selama beberapa tahun terakhir ini. Dapat diperkirakan bahwa aksi-aksi tersebut, sedikit banyaknya,  akan bisa membawa dampak tertentu bagi perkembangan situasi di kemudian hari.
