---
layout: post
title: TERIMAKASIH tentang Hari Ulang Tahun ke 81
date: 2009-11-01
---

Berkaitan dengan hari ulang tahun saya ke-81 yang jatuh pada tanggal 26 Oktober 2009, telah saya terima ucapan selamat dari berbagai teman, sahabat, serta kenalan-kenalan Internet atau pembaca website (http://umarsaid.free.fr) . Ucapan-ucapan selamat tersebut, yang semuanya berisi harapan-harapan terbaik telah dituangkan dalam berbagai bentuk dan cara, serta dalam bermacam-macam nada dan aneka-ragam nuansa, yang mencerminkan kehangatan serta persahabatan.


Berbagai macam ungkapan yang demikian ini telah diterima dari Perancis, Holland, Jerman,  Czech, Aljazair, Indonesia, Tiongkok, Hongkong/Macao, Australia, New Zealand.


Sebagai penghargaan terhadap isi atau semangat yang tercermin dalam berbagai E-mail itu, maka di bawah berikut ini disajikan sebagian di antaranya, tanpa menyebut pengirimnya.


Sebab, saya menganggap bahwa pesan-pesan atau harapan-harapan itu semuanya tidaklah ditujukan hanya kepada saya sendiri saja , melainkan juga (secara tidak langsung atau secara implisit) kepada semua sesama yang memperjuangkan demokrasi, HAM, Pancasila, dan ajaran-ajaran Bung Karno.

Paris, 1 November 2009


A. Umar Said


= = = = = = = =



Nama UMAR SAID adalah satu paduan dan tak terpisahkan dengan kata kata :

+++ lawan fasis orba Suharto, bela Bung Karno dan pemikirannya, tegar  dan ulet ,kreatif, fair,tak takut kritik, berani mengakui kesalahan, bela Timor Timur , disegani kawan dan lawan, galang persatuan, maju terus, tak kenal lelah,vivere pericoloso, Restoran Indonesia Paris, demokrasi untuk Indonesia .........+++.
Bung Umar Said , mawar merah yang tumbuh dipadang pengasingan !

Selamat berulang tahun yang ke 81 Bung Umar Said  !

Semoga sehat sehat selalu dan panjaaaaang umur , tugas mengabdi rakyat miskin Indonesia masih selalu menunggu  Bung !

* * *
Pak umar said,

Selamat Ulang Tahun yang ke 81 untuk Pak Umar Said.

Sukses, Sehat dan Sejahtera selalu yah.

Aku suka membaca tulisan dan kumpulan tulisan dari Bapak.
Tak dinyana usia Bapak sudah sepuh, sedang tulisan dan kumpulan tulisan
seperti pekerjaan seusia 60 tahun an.

Bravo pak Umar Said, jaga kesehatan yah

* * *

Selamat ulang tahun pak Umar Said, semoga saya masih bisa mengucapkan setidaknya 20x lagi selamat ulang tahun bagi Anda.

Anda benar-benar orang luar biasa,dan saksi sejarah yang teramat penting, saya beberapa kali membaca website Anda.

Semoga kita semua, generasi muda, khususnya seangkatan saya yang lahir jauh setelah peristiwa GESTOK (saya memilih pake istilah GESTOK-nya Soekarno), dan angkatan yang lebih muda dari angkatan saya, tidak pernah dan jangan menjadi JAS MERAH, jangan sekali-kali meninggalkan sejarah.

selamat dan sukses buat Anda pak Umar Said.

* * *

Yik,

kami ucapkan

SELAMAT BERULANG TAHUN

SELAMA HAYAT DIKANDUNG BADAN,

SELAMA ITU UMAR SAID

TETAP UMAR SAID  !!!!

SEMOGA KESEHATANNYA STABIL
BAHAGIA BERSAMA ZUS NINON

MAJU TERUS YIK

* * *

Selamat ulang tahun semoga tambah semangat menulis, tambah bermanfaat bagi generasi muda seperti saya.
Your great fan

* * *

Agak telat Pak Umar, namun tetap menyambut dgn

SELAMAT, DAN TETAPLAH SEHAT SEJAHTERA
TAMBAH PANJANG USIA, TAMBAH GIAT PERJUANGAN!

* * *

Salam perkenalan Pak,
Saya norman 67+thn purnawirawan bi , dengan ini mengucapkan :

SELAMAT MERAYAKAN HARI KELAHIRAN YANG KE 81
Semoga Bapak selalu dalam Lindungan dan Naungan Allah SWT  ~ sehat dan sukses selalu.

* * *

Wah:   81  !

Selamat banget!
Terus untuk socialism ala Indonesia!

Viva being free!

Selamat HUT bung Umar



* * *

+++ lawan fasis orba Suharto, bela Bung Karno dan pemikirannya, tegar
 dan ulet ,kreatif, fair,tak takut kritik, berani mengakui kesalahan,
bela Timor Timur , disegani kawan dan lawan, galang persatuan, maju
terus, tak kenal lelah,vivere pericoloso, Restoran Indonesia Paris,
demokrasi untuk Indonesia .........+++.

Ingin menambahi sedikit, tapi saya anggap cukup mengesankan
bagi mereka yang kenal dekat dengan bung Umar Said,  yaitu
sifatnya yang selalu ramah, sederhana dan hangat dengan sahabat.

Selamat Ulang Tahun untuk bung Umar Said Ytc. ,
semoga panjang umur dan baik-baik semuanya.

* * *

Bung Umar Said yang kami kagumi,
Meskipun agak terlambat, kami mengucapkan SELAMAT ULANG TAHUN.
Kami yakin dengan perjuangan bung Umar, dan berteladan kepada bung.
Tak banyak yang dapat kami utarakan. Kami bangga dengan bung.
Selamat dan salam hangat

* * *

Selamat Ulang Tahun Pak Umar,Selamat Panjang Umur,
Delapan Puluh Satu Tahun Sepenuh Hati Mengabdi Rakyat
Memayu Ayuning Buwono!
Bravo!!!!

* * *

Bung Umar Said yb.,

Bersama ini saya sekeluarga mengucapkan "Selamat berulang tahun".
Semoga Bung Umar baik-baik dan sehat-sehat saja, sehingga bisa berjuang terus untuk meluruskan sejarah NKRI!

* * *

Kami atas nama Lembaga Bantuan Hukum Pers - Jakarta.

Mengucapkan :

"Selamat ulang tahun teruntuk bung Umar Said, semoga panjang umur, tetap sehat dan terus bersemangat dalam karya dan cipta untuk negeri tercinta Indonesia"

* * *

Oom  Umar Said dan Tante Yth
SAYA MENGUCAPKAN SELAMET HARI ULANG TAHUN  SEMOGA SELALU  SEHAT WAL'AFIAT
DAN UNTUK Oom Umar  saya Salut  dari semangat perjuangan PENA yg selama ini Oom jalankan dan lakukan demi  kebenaran sejarah dan keadilan Demokratie Indonesia.

* * *

Bung Umar Said Ytc,

Saya juga mengucapkan:

diiringi harapan dengan selalu didampingi Uni Ninon senantiasa sehat wal 'alfiat, bahagia dan sejahtera, penuh sukses dalam bekerja dan berkarya.

Salam hangat dengan penuh rindu,
        SELAMAT ULANG TAHUN

* * *

Selamat Ulang Tahun Bapak ya...Spirit dan semangat Kebangsaan, catatan perjalanan , tulisan selalu menjadi inpsirasi yang tak habis digali oleh generasi muda.

Semoga panjang umur dan selalu diberi Tuhan Kesehatan.



"Merdeka...!!! Marhaen...Menang"

* * *

Dear Pak Umar,

Saya pun hendak mengucapkan "SELAMAT BERULANG TAHUN. SEMOGA TETAP SEHAT WALAFIAT DAN TERUS BERSEMANGAT BERBAGI CERITA."

Rasa kagum saya atas spirit Pak Umar yang terus menyala, dalam berbagi informasi berguna untuk kami-kami yang katanya lebih muda, tetapi kalau soal kedisiplinan jauh dari kata setara. Kedisiplinan dalam menuliskan bukan sekadar kata, tetapi fakta. Maupun kerajinan dalam mengumpulkan kliping berguna dari berbagai media Indonesia, untuk kemudian disebar menjadi santapan siap kami - para pembaca.

Satu kata mutiara hiasan yang tampak bersinar menyilaukan mata, belum tentu lebih mulia dan berharga daripada satu kata kebenaran sejarah masa, yang walaupun kata itu pahit adanya, namun tetap fakta sejarah yang sebenar-benarnya.

Teriring do'a dan salam hangat dari Bonn di bulan Oktober, di saat-saat musim gugur yang begitu mempesona!

* * *

Saya juga turut mengucapkan selamat ulang tahun kepada Pak Umar Said. Semoga selalu sehat dan terus berjuang melawan setiap ketidakadilan, melawan Orba dan sisa-sisanya yang masih tersebar luas di Indonesia. Walaupun kita tercampak di luarnegeri, karena paspor kita dirampas oleh Orba Soeharto dan merupakan pelanggaran HAM yang belum pernah dipulihkan, namun kita akan terus berjuang melawan pelanggaran tsb. Sejak jaman Orba Soeharto hingga kini di jaman apa yang dikatakan "reformasi", tak satu pun pemerintahan yang berusaha memulihkan hak kita sebagai warga Indonesia.
Sekali lagi selamat berulang tahun dan selalu sehat.


* * *


Kepada Bapak Umar Said Yth,

Selamat ber-Ultah kepada Bapak Umar Said yang jatuh pada hari ini
tgl. 26.Oktober.

Kegigihan beliau memperjoangkan filosofi-nya berkenaan dengan semua kebaikan, kemajuan untuk rakyat Indonesia, membangkitkan semangat bagi para :pendukung ,pembaca "suara"nya ,di pelbagai media dan milis.

Teruskan perjoangannya Pak, dari jauh aku harapkan Bapak diparengi
kesehatan dan kebahagian dan terus berkiprah untuk Indonesia.

salam persahabatan,

* * *

kami sekeluarga  di surabaya mengucapkan selamat ulang tahun yang ke 81 , semoga diberi kesehatan yang terbaik dan diberi rejeki yang berlimpah , dengan begitu bisa berkarya dengan dengan sempurna dan seterusnya kita bisa menikmati dan  membaca tulisan-tulisan uda yang  up to date

* * *

Bung AYIK nan jaueh di mato;

Dari jauh kami mengucapkan SELAMAT ULANG TAHUN. Besertakan harapan: semoga senantiasa sehat wal afiat, umur panjang, kian aktif dan kreatif dalam berkarya.

Maju terus, pantang mundur, sampai tanah air dan bangsa bebas dari kekuasaan otoriter rezim orba.

* * *

Pak Umar Said,
Saya juga mengucapkan selamat ulang tahun, semoga Pak Umar tetap melanjutkan perjuangan melalui tulisan yang terus mengalir. Semoga Pak Umar sekeluarga tetap diberkati kesehatan. Ternyata ultah bapak sama dengan Pak Soemarsono yang pada 26 Oktober yl berusia 88 tahun.

* * *

Ikut gerbong jama'ah masbuk alias terlambat, saya mengucapkan: SELAMAT
ULANG TAHUN BUAT PAK UMAR SAID! Semoga selalu sehat dan terus
bersemangat bergiat dalam dunia pergerakan. Sungguh, lentera tak kenal
padammu telah terangi jalan setapak kami, generasi penerusmu.

Salam hangat dari jogja,

* * *

HARI INI, SENIN, tanggal 26 Oktober 2009.
Dengan penuh rasa gembira saya sampaikan SELAMAT HARI UANG TAHUN kepada seorang bapak, sahabat dan kawan yang terhormat dan tercinta: A. UMAR SAID, yang pada hari ini memperingati Hari Lahir yang ke-81.  Semoga pak Umar Said senantiasa sehat walafiat dalam lindungan Allah YME. Begitupun keluarga yang mendampingi, selalu mendapat berkat Allah hendaknya.
Nama A. UMAR SAID, pertama kali saya dengar adalah sekitar tahun 1957 ketika beliau menjadi pemimpin redaksi suratkabar Harian Penerangan di Padang, Sumatra Barat. Ketika itu saya masih menjadi pelajar SMA di Pekanbaru dan belajar menjadi penulis berita di suratkabar mingguan di Pekanbaru dan juga di Harian Penerangan yang dipimpin oleh pak Umar.


Terus terang pak Umar, kendatipun kita belum pernah berjumpa, belum pernah bertemu muka dan bahkan belum pernah bersalaman, semenjak saya dengar nama pak Umar di Sumbar tahun 1957 sampai sekarang,  dengan ini dengan penuh ketulusan saya sampaikan SELAMAT ULANG TAHUN ke 81. Semoga pak Umar tetap sehat walafiat dan senantiasa mendapat berkat dan rahmat dari Allah YME untuk bisa terus berkarya menegakkan keadilan, kebenaran dan kemanusiaan buat semua.  Salam kasih dan rindu serta hormat buat pak Umar dan keluarga. Sekaligus, juga "selamat ulang tahun" buat website http://umarsaid.free.fr "

(ma’af disingkat)
