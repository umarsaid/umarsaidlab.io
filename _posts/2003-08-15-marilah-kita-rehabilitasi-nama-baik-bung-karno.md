---
layout: post
title: Marilah kita rehabilitasi nama baik Bung Karno
date: 2003-08-15
---

Salah satu di antara hasil-hasil penting sidang tahunan MPR baru-baru ini yalah adanya saran supaya Presiden Megawati mengadakan langkah-langkah merehabilitasi nama baik Bung Karno. Saran penting dan bersejarah MPR tersebut selengkapnya berbunyi : "Untuk menciptakan situasi kondusif bagi rekonsiliasi nasional, Majelis menyarankan kepada Presiden untuk merehabilitasi nama baik para pahlawan dan tokoh-tokoh nasional yang telah berjasa pada bangsa dan negara. Mengingat jasa-jasa Bung Karno, khususnya sebagai salah seorang proklamator dan Presiden pertama Republik Indonesia, Majelis menyarankan kepada Presiden untuk mengambil langkah-langkah merehabilitasi nama baik Bung Karno." (Suara Merdeka, 9 Agustus 2003).

Saran MPR kepada Presiden Megawati tersebut di atas ini patut mendapat perhatian, dan dukungan yang seluas-luasnya dari semua fihak. Sebab, saran yang dirumuskan dengan bahasa yang tegas dan jelas mengenai rehabilitasi Bung Karno ini bisa merupakan permulaan dari berkembangnya kehidupan politik yang baru di negeri kita. Masing-masing fraksi dalam MPR bisa saja mempunyai agenda tersendiri atau perhitungan sendiri-sendiri, tetapi kalau semuanya juga mempunyai maksud dan tujuan yang tulus dalam merehabilitasi nama baik Bung Karno, ini akan memudahkan terciptanya situasi yang kondusif bagi rekonsiliasi nasional.

## Apa Arti Rehabilitasi Bung Karno

Rehabilitasi nama baik Bung Karno yang disarankan oleh MPR adalah langkah penting dalam kehidupan negara dan bangsa kita. Sebab, ini berarti koreksi besar-besaran terhadap politik Orde Baru, yang sudah dijalankan dalam tempo yang amat panjang, yaitu lebih dari 32 tahun. Dengan kalimat lain, rehabilitasi nama baik Bung Karno adalah pernyataan bahwa apa yang dilakukan oleh Orde Baru terhadap Bung Karno adalah salah. Dan, sejarah digulingkannya Bung Karno melalui “kudeta merangkak”-nya Suharto dkk membuktikan dengan gamblang berbagai kesalahan atau pengkhianatan Orde Baru ini.

Sekarang sudah makin jelas bahwa sejak meletusnya G30S, Suharto sebagai perwira militer sudah terus-menerus melakukan serentetan panjang insubordinasi (pembangkangan) dan pengkhianatan terhadap Panglima Tertinggi Angkatan Perang Republik Indonesia, yaitu Presiden Sukarno. Di antara pengkhianatan Suharto dkk yang menyolok adalah manipulasi dan penyalahgunaan Surat Perintah Sebelas Maret, yang dalam jangka lama sekali digembar-gemborkan sebagai “Super Semar” yang ampuh Pengkhianatan dan pembangkangan lainnya adalah pembubaran PKI dan penangkapan menteri-menteri yang diangkat Bung Karno, dan penangkapan tokoh-tokoh penting dalam DPR/MPRS; DPRD, dan pemerintahan. Semua tindakan ini telah dilakukan oleh Suharto dkk tanpa persetujuan Bung Karno

Puncak pengkhianatanSuharto (dan pendukung-pendukungnya) adalah rekayasa dan konspirasi besar-besaran untuk menggulingkan dan melucuti kekuasaan Bung Karno lewat ketetapan MPRS no 33/1967. Sebab, sesudah pencopotan kekuasaan Bung Karno sebagai presiden RI ini maka beliau telah diperlakukan secara sewenang-wenang, mendapat siksaan lahir dan batin, dan akhirnya wafat sebagai tahanan Orde Baru.

Jadi rehabilitasi Bung Karno adalah konsekwensi yang logis dari penolakan rakyat terhadap Orde Baru. Artinya, karena sudah jelas rakyat menajiskan rezim militer Orde Baru maka dosa atau pengkhianatannya terhadap Bung Karno harus dipertanggung-jawabkannnya pula.

## Rehabilitasi Bung Karno Adalah Masalah Urgen

Rehabilitasi nama baik Bung Karno adalah masalah politik yang urgen, dan punya relevansi dengan situasi dan kebutuhan kehidupan bangsa dewasa ini. Jadi, masalahnya bukan mengutik-utik masalah lampau yang tidak ada hubungannya dengan masa kini. Rehabilitasi Bung Karno ada hubungannya yang erat dengan perjuangan bangsa untuk mengikis habis sisa-sisa Orde Baru. Rehabilitasi Bung Karno adalah bagian dari reformasi, bahkan merupakan bagian yang penting. Omong kosong besar saja bicara soal reformasi tanpa bicara soal rehabilitasi Bung Karno. Dan juga omong kosong besar saja bicara soal rekonsiliasi nasional tanpa membicarakan rehabilitasi Bung Karno.

Dengan adanya saran MPR kepada Presiden Megawati untuk merehabilitasi Bung Karno, maka mereka yang masih kepala batu anti-Sukarno lama-kelamaan akan terkucil atau terisolasi. Para dedengkot anti-rehabilitasi Bung Karno ini adalah mereka-mereka yang itu-itu juga, yang selama ini mendukung dengan setia rezim militer Orde Baru. Mereka dengan segala cara – dan segala dalih - akan terus mengadakan gerpol (gerilya politik). Siapa-siapa saja dan dari golongan yang mana saja mereka itu, kita akan sama-sama menyaksikan sendiri di kemudian hari.

Memang, rehabilitasi nama baik Bung Karno saja tidak akan segera memecahkan berbagai persoalan parah yang sudah bertumpuk-tumpuk menggunung sebagai warisan Orde Baru yang diteruskan oleh pemerintahan Habibi, Gus Dur, dan Megawati-Hamzah sekarang. Rehabilitasi Bung Karno bukan satu-satunya obat mujarab untuk mengatasi segala kesulitan, tetapi menghilangkan satu sumber penyakit perpecahan bangsa yang sudah membikin penderitaan selama berpuluh-puluh tahun.

## Presiden Megawati, Jangan Lewatkan Kesempatan Ini!

Saran yang diajukan MPR kepada Presiden Megawati bukan saja merupakan masukan yang penting bagi Kepala Negara untuk merehabilitasi Bung Karno; melainkan sudah merupakan dorongan politik yang penting. Sebagai kepala negara yang mempunyai hak prerogatif; Presiden Megawati bisa menggunakan kesempatan yang baik yang sudah diberikan oleh MPR ini untuk mengeluarkan Keputusan Presiden (Kepres), atau apa saja namanya. Kesempatan ini tidak boleh dilewatkan begitu saja olehnya. Bukan hanya karena Bung Karno itu adalah ayahnya, melainkan karena rehabilitasi Bung Karno mempunyai dimensi sejarah, dimensi politik dan dimensi moral yang lebih luas bagi bangsa.

Mengingat itu semua, maka gerakan untuk mendukung tindakan Presiden Megawati merehabilitasi Bung Karno perlu dilancarkan oleh semua fihak. Gerakan ini harus bersifat lintas-partai, lintas- kelompok, lintas-golongan; lintas-agama, lintas-suku. Karenanya, gerakan ini tidak bisa dianggap hanya menguntungkan PDI-P saja, atau hanya menguntungkan keluarga Megawati saja, melainkan menguntungkan seluruh kekuatan yang benar-benar anti-Orde Baru. Dukungan yang luas dan dorongan yang kuat kepada Presiden Megawati akan lebih memberanikannya untuk bertindak.

Sebab, alangkah janggalnya, atau alangkah mengecewakan kita semuanya, kalau setelah ada saran yang begitu jelas dan tegas dari MPR, seandainya Presiden Megawati toh tidak mengambil tindakan apa pun untuk merehabilitasi Bung Karno. Seandainya Presiden Megawati bersikap tidak mengacuhkan saran MPR yang demikian: maka banyak orang tidak akan mengerti, dan tidak akan mema’afkannya. Jadi, sekarang tergantung kepada Presiden Megawati, yang mempunyai hak prerogatif, untuk memenuhi harapan atau seruan MPR mengenai rehabilitasi Bung Karno. Adalah kewajiban kita semua untuk mendorongnya, supaya ia mengambil langkah-langkah ke arah itu.

## TAP MPRS 33/1967 Dan TAP MPRS 25/1966

Dalam Sidang Tahunan MPR yang diadakan permulaan bulan Agustus memang diperdebatkan usul (dari Fraksi PDI-P) dicabutnya TAP MPRS 33/1967 tentang pemberhentian kekuasaan Presiden Sukarno sebagai Presiden RI dan TAP MPRS 25/1966 tentang pembubaran PKI dan pelarangan Marxisme. Tetapi usul ini ditolak, dengan macam-macam dalih atau alasan ; yang menggambarkan bahwa fikiran banyak anggota MPR masih terlalu dipengaruhi oleh sisa-sisa fikiran model Orde Baru.

Akhirnya, sesudah melalui tawar-menawar dalam perundingan yang cukup alot, maka dicapai jalan mundur yang merupakan kompromi, yaitu saran kepada Presiden Megawati untuk merehabilitasi Bung Karno dan perlakuan yang lebih humanis terhadap anak-cucu para korban persitiwa ’65. Dari sidang tahunan MPR ini orang mendapat kesan bahwa mengenai berbagai pelanggaran kemanusiaan di masa yang lalu, para simpatisan Orde Baru betul-betul terpojok dan terpaksa mengambil posisi defensif. Karena itu pulalah mereka terpaksa menyetujui usul mengenai rehabilitasi Bung Karno dan perbaikan perlakuan terhadap para korban peristiwa ’65. Tidak bisa lain, sebab kejahatan rezim militer Suharto dkk adalah terlalu banyak dan pelanggaran kemanusiaan sudah begitu parahnya, sehingga siapa pun sulit untuk bisa membelanya.

Saran MPR kepada Presiden Megawati mengenai rehabilitasi Bung Karno, dalam konteks situasi di negeri kita dewasa inin mempunyai signifikasi tersendiri. Pendapat yang diajukan oleh lembaga legislatif yang tertinggi ini sejalan dengan pendapat lembaga judikatif yang tertinggi negeri kita, yaitu Mahkamah Agung yang juga sudah menulis surat kepada Presiden Megawati tentang rehabililitasi para korban ’65.

Gejala-gejala ini semuanya menunjukkan arah bahwa - lambat-laun - opini publik (termasuk kalangan elite) mengakui bahwa sistem politik dan kekuasaan Orde Baru, yang memusuhi politik Bung Karno, adalah salah. Perkembangan ini penting dan menggembirakan, kalau diingat bahwa Suharto baru ditumbangkan 5 tahun yang lalu, sesudah berkuasa 32 tahun dengan “tangan besi”.

Tidak bisa lain! Orde Baru sudah begitu terlalu banyak membuat dosa kepada bangsa dan kesalahan kepada rakyat, sehingga siapa pun yang berani terang-terangan membelanya akan dianggap sebagai orang “aneh”, atau kurang waras nalarnya atau buta hati nuraninya. Orang-orang yang dengan diam-diam atau sembunyi-sembunyi menaruh simpati kepada rezim militer Suharto dkk masih cukup banyak, walaupun mereka itu jumlahnya minoriter sekali. Mereka inilah yang umumnya akan menentang dengan segala cara rehabilitasi Bung Karno.

## Mari Kita Rehabilitasi Bung Karno

Kita belum tahu apakah Presiden Megawati segera bertindak sesuai dengan apa yang diusulkan MPR atau masih mau menunggu saat yang dianggapnya cocok untuk itu. Tetapi seluruh kekuatan demokratis dan anti-Orde Baru (partai, Ornop, LSM, universitas, lembaga-lembaga ilmiah dll dll) perlu sejak sekarang mendorong – dengan berbagai cara dan bentuk - Presiden Megawati untuk bertindak sesuai dengan harapan MPR itu. Tidak semua keputusan MPR itu perlu mendapat dukungan rakyat, dan tidak semua sikap MPR patut disokong oleh opini umum. Tetapi seruan MPR kepada Presiden Megawati tentang rehabilitasi Bung Karno ini perlu mendapat dukungan yang luas.

Dukungan yang luas dari opini publik terhadap seruan MPR ini akan merupakan dorongan yang kuat kepada Presiden Megawati untuk menggunakan hak prerogatifnya demi kepentingan seluruh bangsa. Bukan demi kepentingan PDI-P, dan bukan pula demi kepentingan keluarganya saja, melainkan demi semua. Dengan begitu, bisa dikatakan bahwa rehabilitasi Bung Karno adalah manifestasi aspirasi kolektif banyak orang,

Rehabilitasi Bung Karno, kalau sudah bisa direalisasikan sepenuhnya, akan merupakan kemenangan semua fihak yang mendambakan keadilan, kebenaran, dan persatuan bangsa.
