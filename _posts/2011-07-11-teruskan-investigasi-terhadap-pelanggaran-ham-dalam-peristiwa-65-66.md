---
layout: post
title: Teruskan investigasi terhadap pelanggaran HAM dalam peristiwa 65/66 !
date: 2011-07-11
---

Tulisan ini dimaksudkan sebagai dukungan kepada  Komnas Ham yang sedang terus mengadakan penyelidikan Pro Justicia tentang peristiwa 65/66. Usaha Komnas HAM ini  sangat penting untuk didukung seluas mungkin dan sebesar mungkin, dan juga  terus-menerus dengan segala macam cara.  


Sebab masalah Peristiwa HAM  1965-66 adalah masalah yang sangat besar dan penting bangsa, baik untuk masa kini maupun untuk seterusnya di masa-masa yang akan datang, untuk generasi-generasi yang akan datang, yang merupakan  anak cucu kita semua.


Pembantaian dan pemenjaraan dan penyiksaan jutaan manusia  --  yang tidak bersalah apa-apa !!! --  oleh pimpinan Angkatan Darat (waktu itu) beserta golongan-golongan reaksioner lainnya adalah pelanggaran HAM yang termasuk terbesar di dunia sejak berakhirnya Perang Dunia ke-2.


Oleh karena itu, dapatlah kiranya kita katakan bahwa siapa yang senang dengan pelanggaran HAM tahun 65-66 adalah orang-orang yang tidak bisa dianggap bernalar sehat. Mereka yang menyetujui pembantaian sewenang-wenang terhadap jutaan orang sesama bangsa adalah orang-orang yang pantas disebut sebagai orang yang berjiwa aneh (untuk tidak mengatakan gila).


Dan orang-orang yang tidak peduli atau cuwek saja terhadap penderitaan  puluhan juta keluarga para korban pembunuhan massal tahun 65-66 (dan pemenjaraan berjangka lama terhadap ratusan ribu orang lainnya) patut dipertanyakan kebersihan hati nurani mereka atau diragukan tentang adanya rasa kemanusiaan mereka.


Coba marilah sama-sama kita renungkan : Hanya karena satu Ruwiati saja, (TKI yang dipancung di Saudi Arabia) . maka sudah menggeloralah  heboh besar di seluruh negeri dalam jangka lama. Dan suara kemarahan luar biasa juga sudah meggema  karena adanya berita bahwa ada puluhan TKI lainnya yang diancam akan menyusul dipancung.


Dibandingkan dengan kasus Ruwiati (yang sudah sepatutnya mendapat simpati dari begitu banyak orang dari berbagai kalangan) kasus pembunuhan massal dalam peristiwa 65-66 adalah jauh lebih besar, jauh lebih biadab, jauh lebih tidak bermanusiawi, jauh lebih gila-gilaan.


Yang dibunuh dalam genocida 65/66 tidak hanya 10 orang yang tidak bersalah, bahkan tidak hanya 100 atau  1 000  orang saja, melainkan lebih dari sejuta nyawa ( dengan angka : antara  1 000 000 sampai  3 000 000 jiwa manusia) !!!!!


Pembunuhan massal yang begitu biadab itu merupakan kejahatan besar yang telah membikin kerusakan-kerusakan besar terhadao kehidupan bangsa dan negara kita. Sebab dengan dibunuhnya begitu banyak pendukung Bung Karno dan simpatisan PKI, maka negara dan bangsa kita telah menjadi rusak seperti yang kita saksikan dengan jelas dewasa ini.


Pembunuhan dan pemenjaraan jutaan orang itu telah melumpuhkan kekuatan revolusioner yang menjadi tulang punggung Bung Karno dalam melaksanakan revolusi rakyat untuk menegakkan Pancasila dan Bhinneka Tunggal Ika, dalam mempersatukan bangsa guna melawan segala kekuatan reaksioner dalam negeri maupun luar negeri, terutama imperialisme AS.


Keadaan yang serba semrawut, pembusukan moral yang begitu parah di semua bidang yang sama-sama kita saksikan dewasa ini , yang  menyuburkan korupsi, dan yang membiarkan pelecehan atau pelanggaran hukum,  adalah akibat dari dilumpuhkannya kekuatan revolusiober dengan adanya pembunuhan massal di masa-masa yang lalu.


Dengan dibunuhnya jutaan orang tidak bersalah, yang sebagian besar adalah para pendukung  politik Bung Karno dan simpatisan (dekat atau jauh) PKI , maka hilang pulalah (untuk sementara !) kekuatan dalam masyarakat untuk melanjutkan revolusi dalam usaha untuk menciptakan masyarakat adil dan makmr. Dalam konteks sekarang, ini semua terasa sekali dan kelihatan amat jelas.


Karenanya, mempersoalkan pelanggaran HAM dalam peristiwa 65/66 adalah  urusan yang justru sangat berkaitan erat dengan situasi dewasa ini. Kalau seandainya Bung Karno tidak dikhianati oleh pimpinan Angkatan Darat (watu itu) dan kekuatan PKI tidak dilumpuhkan, maka pastilah situasi negara dan bangsa kita tidak begitu terpuruk dan serba busuk dan rusak seperti sekarang ini.


Menuntut dibongkarnya segala keburukan pembunuhan besar-besaran dan berbagai pelanggaran HAM lainnya dalam peristiwa 65/66 adalah tidak hanya untuk kepentingan para pendukung politik Bung Karno dan membela simpatisan-simpatisan PKI saja, melainkan juga untuk kepentingan seluruh  bangsa serta berbagai generasi kita di kemudian hari. Termasuk untuk kepentingan orang-orang yang  tadinya anti Bung Karno dan tidak suka kepada perjuangan PKI,  beserta anak-cucu mereka.


Sebab, pembunuhan massal dan banyak macam pelanggaran HAM lainnya dalam tahun-tahun 65/66 adalah betul-betul aib bangsa dan dosa besar yang sudah  merusak jwa banyak orang. Oleh karena itu, orang-orang atau kalangan yang betul-betul menghayati Pancasila, Bhinneka Tunggal Ika dan UUD 45 patut sekali mendukung sekuat-kuatnya dan sebesar-besarnya usaha Komnas HAM untuk mengadakan penyelidikan atau investigasi pro justicia tentang kasus peristiwa 65/66.


Berikut di bawah ini disajikan pernyataan dari Komunitas Korban Peristiwa 65/66 dan organisasi KontraS tentang berbagai soal yang berkaitan dengan pelanggaran HAM yang terbesar dalam sejarah bangsa Indonesia ini.

Paris 11 Juli  2011
A. Umar Said
= = =



Desakan  Komunitas Korban 65 kepada Komnas HAM



« Pada hari Rabu 06 Juli 2011 pukul 11.00 Komunitas Korban 65  bersama  dengan  YPKP 65 dan  KontraS  mendatangi Komisi Nasional Hak Asasi Manusia (Komnas HAM) di Jl. Latuharhary, Menteng, Jakarta.  Komnas HAM pada jam yang sama sedang mengadakan Rapat Paripurna untuk membahas Laporan Akhir Hasil Investigasi Tim Penyelidik pro justicia  Komnas HAM  tentang Kasus Peristiwa 1965-1966.  Delegasi Komunitas Korban 65 diterima oleh Komisioner  Kabul Supriyadi dan Ridha Saleh. Sedianya Ketua Tim Penyelidik kasus 65 Nur Kholis akan menemuinya, namun karena sedang memberikan presentasi di depan rapat paripurna, beliau batal hadir.

Dari Korban 65 yang datang ke Komnas antara lain: Bedjo Untung, Mujayin, Ir. Djoko Sri Muljono, Giri Jati, Anwar Umar, Gustaf Dupe, Tumiso, Palupi, Bu Tahsrin, Haroto, Rasmadi, Mulyono SH., Hutomo S, Sutriyanto, dan seorang utusan dari Purwokerto Jawa Tengah. Sedangkan dari KontraS ialah  Putri Kanesia dan Daud Beureuh.

Maksud kedatangan Komunitas Korban 65 ialah  untuk memberi dukungan moral kepada Tim Penyelidik agar Laporan Akhir yang sedang dibahas di Rapat Paripurna benar-benar berpihak kepada korban, yaitu memastikan bahwa  Tragedi kemanusiaan 1965/1966 adalah rekayasa sistematik oleh militer yang berpuncak pada pembunuhan massal  dan akhirnya menggulingkan Presiden Sukarno.
Komisioner Kabul Supriyadi dalam keterangannya mengatakan «  kami sedang berjuang  di  Rapat Paripurna. Apa yang korban tuntut dan usulkan akan saya bawa ke paripurna ».

Pada kesempatan pertemuan itu, Bedjo Untung (ketua YPKP) atas nama Komunitas Korban 65 menyampaikan pernyataan pers sebagai berikut:



                      Bentuk Pengadilan HAM ad hoc dan Pulihkan Hak-Hak Korban 65

Tragedi Kemanusiaan 1965/1966, mencakup beberapa  dimensi pelanggaran Hak Asasi  Manusia  yang berat, berskala meluas, sistematis  yang melibatkan  institusi Negara  yaitu aparat militer, polisi dan aparat pemerintah. Pelanggaran HAM  tersebut  berupa pembunuhan massal, penghilangan manusia secara paksa, pembunuhan  tanpa proses hukum, penculikan, pemerkosaan/pelecehan seksual, penyiksaan, penahanan dalam jangka waktu tidak terbatas (12-14 tahun), pemaksaan kerja  tanpa diupah, diskriminasi  hak-hak dasar warga negara (politik, ekonomi, social, budaya serta hukum), perampokan harta benda milik korban, pencabutan paspor tanpa proses pengadilan, pengalihan hak kepemilikan  tanah secara tidak sah, pencabutan hak pensiun, pencabutan hak sebagai pahlawan masional, pengucilan dan pembuangan, dsb.nya.

Diperkiraan 500.000 sampai 3.000.000  jiwa terbunuh pada tragedy kemanusiaan 1965/66 dan  20.000.000  orang korban bersama keluarganya yang masih hidup  menderita stigma  serta  diskriminasi oleh penguasa.

Kejadian ini berlangsung  selama 46 tahun  sejak 1965 hingga hari ini, Negara/Pemerintah  belum ada niat untuk mengungkap dan menuntaskan persoalan yang menimbulkan  kesengsaraan jutaan orang yang tidak bersalah tersebut. Malahan ada indikasi Negara/Pemerintah ingin melupakan  dan mengabaikan tindak kekerasan  mau pun pelanggaran HAM  tragedy kemanusiaan 1965/66. Tindakan pengabaian tersebut  bisa  dikategorikan sebagai tindakan kejahatan kemanusiaan.

Komisi Nasional Hak Asasi Manusia (Komnas HAM )  telah membentuk Tim Penyelidik pro justicia Peristiwa 1965-66 sejak tahun 2008, namun belum mengumumkan  hasil investigasinya. Ada dugaan kuat Komnas HAM mendapatkan tekanan oleh pihak pelaku kejahatan 65, karena tindak kekerasam/kejahatan 1965  melibatkan institusi militer/penguasa  yang merekayasa, dan memproduksi kebohongan  serta menggelapkan dokumen-dokumen penting  serta masih terus menutup-nutupi  kejadian sesungguhnya atas  tragedy kelam itu.

Atas dasar itu , kami para Korban/Keluarga Korban  Tragedi Kemanusiaan 1965/66 dengan ini mendesak:

Segera umumkan hasil investigasi Tim Penyelidik pro justicia Komnas HAM  tentang Peristiwa 1965-66,
Pastikan adanya pelanggaran HAM  berat yang dilakukan oleh Negara secara massive, sistematis, pembunuhan massal (genocida)  serta kekerasan politik dan diskriminasi  social   pada kasus  tragedy 1965/66.
Kejaksaan Agung harus segera menindak lanjuti hasil temuan Tim Investigasi Komnas HAM dan segera  membentuk Pengadilan  HAM ad hoc untuk mengadili para pelaku penjahat HAM serta agar ada  kepastian  hukum  dan keadilan bagi  Korban.
Presiden Republik Indonesia segera menerbitkan surat  Keputusan Presiden (Keppres) untuk memberikan rehabilitasi, reparasi dan kompensasi  kepada Korban 65 seperti yang diamanatkan  Undang-Undang Nomer 39 Tahun 1999  tentang Hak Asasi Manusia, UU  No.26 Tahun 2000  tentang Pengadilan HAM  serta Surat Rekomendasi Ketua Komnas HAM,  Mahkamah Agung, Ketua DPR-RI.
Negara/Pemerintah agar menjamin tidak akan mengulangi lagi tindak  kejahatan/pelanggaran HAM berat  seperti yang terjadi pada kasus Tragedi Kemanusiaan 1965/66 ».Demikian pernyataan pers Komunitas Korban 1965/66.


 Desakan yang keras oleh KontraS kepada Komnas HAM



Sementara itu, Haris Azhar SH MA,  Koordinator Badan Pekerja organisasi KontraS, yang selama ini terkenal dalam membela HAM dan melawan segala macam ketidakadilan, pada tanggal 6 Juli telah mengirim surat kepada Ketua Komnas HAM, Ifdal Kasim SH , yang mengangkat juga masalah pelanggaran HAM 1965/1966. Surat tersebut berbunyi sebagai berikut :

« Penyelidikan Pro justisia untuk Pelanggaran HAM Berat Peristiwa 1965/1966 berjalan sangat lambat. Tiga tahun (Mei 2008 – Juli 2011) paska pembentukan Tim Ad Hoc Pro Justisia untuk kasus ini (Mei 2008 – Juli 2011) belum juga ada hasil penyelidikan yang dapat dipertanggungjawabkan. Kondisi ini menimbulkan kekecewaan yang mendalam, khususnya bagi para korban yang telah diperiksa sebagai saksi korban.
Berlarut-larutnya proses penyelidikan telah mengarah pada keadaan “ketidakpastian hukum”, terpeliharanya impunitas dan juga melanggar hak-hak korban peristiwa 1965/1966, khususnya hak atas pengungkapan kebenaran dan keadilan, hak untuk memperoleh rehabilitasi dan reparasi, serta hak untuk memperoleh jaminan terhindar dari pengulangan peristiwa serupa di masa depan.
Berkenaan dengan hal diatas dan agenda Sidang Paripurna Komnas HAM yang digelar pada 5 – 6 Juli 2011. Komisi untuk Orang Hilang dan Korban Tindak Kekerasan (KontraS) bersama dengan korban peristiwa 1965/1966 berharap Sidang Paripurna Komnas HAM kali ini mengambil keputusan yang dapat mempercepat penyelesaian penyelidikan pro justisia untuk kasus 1965/1966.
Untuk menjawab persoalan – persaoalan diatas, kami merekomendasikan kepada Komnas HAM untuk:

1. Segera menyelesaikan laporan penyelidikan dan memastikan adanya pelanggaran HAM berat yang sistematis/meluas untuk peristiwa 1965;
2. Merekomendasikan kepada Jaksa Agung untuk segera melakukan penyidikan untuk peristiwa 1965 berdasarkan hasil penyelidikan Komnas HAM;
3. Merekomendasikan kepada Presiden RI untuk memberikan rehabilitasi, pemulihan dan pengungkapan kebenaran dalam peristiwa 1965
4. Menyampaikan perkembangan proses penyelidikan dan mengambil langkah – langkah yang dapat menyegerakan upaya – upaya pemulihan Hak Korban peristiwa 1965/1966. »

Demikian isi surat KontraS kepada Komnas HAM.
