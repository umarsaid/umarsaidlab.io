---
layout: post
title: Indonesia membutuhkan gerakan extra-parlementer yang kuat
date: 2009-07-12
---

Setelah melalui masa yang penuh dengan hiruk-pikuk, umbaran janji-janji kosong, saling serang-menyerang dengan macam-macam kebohongan, baku hantam dengan segala cara,   maka pertarungan antara tiga kubu capres-cawapres telah  - pada pokoknya – selesai dengan diselenggarakanya pemilu presiden tanggal 8 Juli yang lalu.

Meskipun hasil terakhir pemilihan presiden masih belum diumumkan secara resmi, dan juga masih mengandung banyak hal yang bisa dipersoalkan, namun sudah dapat diketahui bahwa pasangan SBY-Budiono mendapat suara yang terbanyak dibandingkan dengan dua pasangan lainnya, yaitu Mega-Prabowo dan Jusuf Kalla-Wiranto.


Menurut hasil quick count KPU,  pasangan SBY-Boediono unggul dengan perolehan mencapai 61 persen suara. Sementara Mega-Prabowo mendulang suara sebesar 28 persen, sedangkan  pasangan JK-Wiranto mendapatkan 12 persen suara. (Tempo Interaktif 12 Juli). Tentu saja, angka-angka hasil quick count ini masih bisa berobah di kemudian hari , walaupun tidak besar. Dan walaupun hasil pemilu presiden yang lalu itu masih terus menjadi soal yang dipertentangkan oleh berbagai fihak atau kalangan.


Jadi, kiranya sudah dapat disimpulkan bahwa negara dan pemerintahan kita selama lima tahun mendatang akan dipimpin oleh presiden SBY dan wakilnya Budiono, sebagai hasil kemenangan pasangannya dalam pemilihan presiden. Apakah kemenangan SBY-Budiono merupakan bukti bahwa sebagian terbesar rakyat kita betul-betul menganggap pasangan itu terbaik dibandingkan dengan yang lain, ataukah karena sebab-sebab yang lain, hal ini bisa menjadi bahan analisa yang bermacam-macam oleh berbagai kalangan atau golongan.
Dengan terpilihnya pasangan SBY-Budiono maka slogan « Lanjutkan », yang dipakai selama kampanye pemilu 2009, akan diusahakan untuk dilaksanakan selama 5 tahun mendatang. Ini berarti bahwa banyak hal-hal negatif, atau masalah-masalah besar dan parah yang terjadi selama pimpinannya dari 2004-2009 akan berlangsung terus, disamping hal-hal positif yang tercapai selama itu.



Masalah-masalah besar yang harus dihadapi


Berikut di bawah ini adalah sebagian dari hal-hal negatif atau masalah-masalah besar dan parah selama pimpinan SBY antara 2004-2009, yang masih akan terus dihadapi pemerintahan di bawahnya yang akan datang. Deretan sejumlah sebagian masalah-masalah besar dan parah  ini mengingatkan kita semua, baik yang mendukung pasangan SBY-Budiono maupun yang menentangnya, bahwa negara dan bangsa kita memang betul-betul membutuhkan adanya perubahan besar dan fundamental, dan tidak bisa diatasi dengan tindakan « tambal sulam », apalagi dengan sikap politik yang pro-neoliberal dan tidak menguntungkan rakyat miskin yang jumlahnya besar sekali.


Pemerintahan SBY-Budiono harus tetap menghadapi masalah besar yang sudah puluhan tahun tidak bisa diatasi oleh berbagai pemerintahan sebelumnya, yaitu pengangguran yang menurut BPS jumlahnya berkisar 10 juta orang. (Ini jumlah « penganggur terbuka » atau yang sama sekali tidak punya pekerjaan, sedangkan kalau dihitung dengan mereka yang setengah menganggur, maka bisa mencapai puluhan juta orang). Jumlah penganggur yang begitu banyak tidaklah mungkin diatasi dalam tempo 5 tahun yang mendatang.
Menurut BPS juga jumlah penduduk yang berada di bawah Garis Kemiskinan di Indonesia pada bulan Maret 2009 sebesar 32,53 juta (14,15 persen). Masalah rakyat miskin yang begitu besar itu bukanlah hal yang mudah dipecahkan, apalagi tanpa politik yang fundamental pro rakyat miskin. Pemerintahan SBY yang lalu sudah mencoba menguranginya dengan mentrapkan BLT dan tindakan-tindakan tambal sulam lainnya.  Di bawah pemerintahan yang dipimpin  SBY-Budiono nantinya berapakah jumlah rakyat miskin yang tetap menderita ? Sulit diperkirakan.


Yang juga menyedihkan ialah adanya pengangguran lulusan perguruan tinggi di Indonesia yang mencapai 2,6 juta orang dari pengangguran kurang lebih 40 juta orang. Dari jumlah itu terbagi atas pengangguran terbuka yang mencapai kurang lebih 1,2 juta orang dan setengah pengangguran yang mencapai 1,4 juta orang (Antara 19 Juni 09). Jelaslah kiranya bahwa soal ini juga tidak mudah diatasi oleh pemerintahan SBY-Budiono dalam lima tahun saja.
Karena adanya krisis keuangan dan ekonomi dunia akhir-akhir ini, maka banyak perusahaan kecil dan menengah Indonesia mengalami kesulitan atau bangkrut, yang menyebabkan dipecatnya banyak buruh dan pegawai. Berbagai macam kesulitan besar dan kecil  juga terjadi di bidang pertanian, kehutanan, industri perkayuan, industri tekstil dan kerajinan tangan, pendidikan, dan kesehatan.  


9 partai politik akan “menguasai” DPR dan pemerintahan


Itu semua barulah sebagian kecil dari masalah-masalah besar dan parah yang sudah lama dan sedang dihadapi bangsa dewasa ini,  dan juga dalam 5 tahun yang mendatang. Dan berdasarkan pengamatan terhadap pengalaman 5 tahun pemerintahan SBY-JK (antara 2004-2009) maka wajarlah kalau kita memiliki kesangsian bahwa pemerintahan SBY-Budiono akan bisa memecahkan masalah-masalah besar dan parah yang banyak itu dalam 5 tahun.


Mengingat itu semuanya, kita semua perlu mempersiapkan diri untuk menghadapi situasi yang tidak pasti, atau yang penuh dengan kesulitan yang bermacam-macam itu.  Kita semua perlu mengamati atau mengawasi terus-menerus segala politik atau tindakan pemerintahan SBY-Budiono. Bersandarkan kepada pengalaman selama pemerintahan yang sudah-sudah, kita tidak boleh lagi membiarkan adanya sekelompok penguasa atau « golongan elite »  terus-menerus melakukan hal-hal yang merugikan rakyat banyak atau kepentingan negara.
Dengan hasil pemilu legislatif yang lalu, DPR kita akan « dikuasai » oleh 9 partai politik, besar dan kecil, yang sebagian terbesar sudah  kita kenal praktek-prakteknya pada  masa-masa yang lalu dalam « mewakili » rakyat. Selama periode « masa bakti » yang lalu, kita sudah menyaksikan kualitas  professional dan kualitas moral sebagian besar anggota-anggota DPR, yang sangat mengecewakan dan , karenanya,  tidak patut mendapat penghargaan  dari rakyat.



Jangan punya ilusi kepada mereka


Seperti  yang sama-sama kita ingat, DPR kita nantinya akan dipenuhi oleh wakil-wakil partai politik : Partai Demokrat, Partai Golkar, PDI-P, PKS, PAN, PPP, PKB, Gerindra dan Hanura.  Dengan komposisi yang semacam ini, dan dengan mengingat pengalaman selama ini, maka kita tidak bisa, atau tidak boleh, atau juga tidak patut sama sekali mempunyai ilusi bahwa negara dan pemerintahan kita akan diurus oleh mereka dengan baik.


Kebejatan  moral dan kerusakan patriotisme, dan kebobrokan semangat kerakyatan, yang sudah dengan jelas dipertontonkan selama pemilu lesgislatif dan pemilu presiden 2009 menjadi peringatan bagi kita semua bahwa wakil-wakil partai yang duduk dalam badan legislatif (DPR, DPRD, DPD) dan juga dalam pemerintahan perlu sama-sama kita awasi dengan waspada sekali.
Terlalu besar berbagai  kebohongan yang sudah dijajakan dan terlalu banyak janji-janji palsu  atau kosong yang mereka sodorkan, demi memperoleh kursi di parlemen atau jabatan presiden dan wakil presiden, sehingga kita menjadi ragu terhadap kejujuran atau ketulusan mereka dalam mengurus negara dan kepentingan rakyat banyak.



Penjahat, maling, penipu dan pengkhianat


Patutlah kiranya sama-sama kita akui secara jujur  bahwa sejak berbagai pemerintahan selama Orde Baru yang disusul oleh pemerintahan-pemerintahan sesudahnya kita saksikan  adanya terlalu banyak penguasa (militer dan sipil) dan tokoh-tokoh masyarakat (termasuk tokoh agama) yang sebenarnya bisa kita masukkan dalam kategori sebagai penjahat, atau maling, atau penipu, atau pengkhianat kepentingan rakyat banyak.


Nyata sekali bahwa selama pemilu legislatif dan pemilu presiden 2009 faktor kekuatan dana memainkan peran yang amat besar sekali, bahkan ikut menentukan. Tidak peduli apakah dana yang dipakai dalam kampanye itu haram atau halal, atau juga tidak peduli asal-usul dari mana datangnya dana. Kalau diusut atau diteliti benar-benar, maka nyatalah bahwa sebagian besar dana yang dipakai berbagai fihak selama pemilu adalah hasil tindakan yang kotor atau najis dan haram..



Pentingnya membangun kekuatan extra-parlementer


Mengingat itu semunya, maka seluruh kekuatan demokratis di Indonesia, tidak peduli dari golongan atau aliran politik yang mana pun (termasuk 39 partai pôlitik yang kecil-kecil yang tidak bisa masuk parlemen atau partai-partai kecil lainnya,  dan ornop atau LSM, serta  segala macam gerakan atau perkumpulan dalam masyarakat, termasuk serikat-serikat buruh ) untuk membangun kekuatan extra-parlementer yang kuat dan meluas.
Gabungan luas dan besar segala macam kekuatan extra-parlementer ini diperlukan sekali oleh rakyat kita yang berjumlah 240 juta ini untuk menghadapi, mengawasi, bahkan menandingi DPR yang terdiri dari wakil-wakil 9 partai itu, dan juga menghadapi pemerintah yang akan mengurus negara selama 5 tahun yang mendatang.


Kekuatan extra-parlementer yang besar dan luas , yang terdiri dari macam-macam aliran politik dan golongan dalam masyarakat itu adalah alat atau senjata di tangan rakyat, dalam memperjuangkan kepentingannya. Sebab, selama sejak pemerintahan Orde Baru yang 32 tahun ditambah dengan 10 tahun berbagai pemerintahan .yang menyusulnya, sudah terbukti bahwa rakyat Indonesia tidak bisa lagi – dan tidak boleh terus-menerus  - meyerahkan mentah-mentah segala urusan negara dan bangsa hanya di tangan DPR dan pemerintah saja.
Perlu kita ingat secara jelas atau kita sadari sedalam-dalamnya bahwa  rakyat berhak, bahkan wajib, dan  secara sah pula ( !)  untuk mengontrol atau mengawasi pekerjaan DPR atau berbagai tindakan pemerintah. Ini adalah tugas mulia dan luhur dari rakyat. Jadi, jangan takut dituduh mau mengrecoki DPR atau mempersulit pekerjaan pemerintah Sebab, dalam demokrasi yang betul-betul ditrapkan secara baik, peran gerakan extra-parlementer sangat dihargai sebagai pelengkap persenjataan di tangan rakyat.



Contoh dari praktek di Prancis


Di negara-negara maju (antara lain : di Eropa, Australia, Amerika Serikat, Amerika Latin  kekuatan extra-parlementer memainkan peran yang tidak kecil dalam kehidupan rakyat masing-masing negara. Contohnya umpamanya, apa yang terjadi di Prancis selama ini.
Prancis sejak lama sudah terkenal sebagai negara yang tradisi kehidupan demokratiknya  menjadi contoh berbagai negeri.. Setiap minggu Parlemen dan Senat Prancis mengadakan sidang  satu kali untuk memberi kesempatan kepada anggota-anggotanya mengajukan pertanyaan, atau melancarkan kritik, atau menyampaikan pendapat yang ditujukan kepada Perdana Menteri atau para menteri mengenai berbagai kebijakan atau tindakan pemerintah. Sidang khusus oleh dua badan tertinggi Prancis (yaitu Parlemen dan Senat) ini dinamakan  « Question au gouvernement » (Pertanyaan kepada pemerintah).


Walaupun tiap minggu pemerintah Prancis »diperiksa » atau diawasi oleh dua badan legislatif tertinggi, namun sejak lama pula kehidupan extra-parlementer tetap diperlukan oleh masyarakat. Gerakan extra-parlementer ini terdiri dari partai-partai politik yang kecil-kecil, organisasi-organisasi sosial-politik yang macam-macam, banyak sekali serikat buruh yang amat kuat, dan berbagai lembaga masyarakat.
Oleh karena kuatnya kehidupan extra-parlementer inilah maka boleh dikatakan bahwa di Prancis setiap harinya ada saja demonstrasi (besar dan kecil)  oleh serikat buruh atau berbagai macam organisasi. Ini menunjukkan bahwa walaupun sudah ada dua badan legislatif tertinggi yang mengawasi berbagai kebijakan pemerintah Prancis setiap minggu melalui sidang khusus, namun toh masyarakat masih membutuhkan adanya berbagai macam wadah atau alat lainnya untuk menyalurkan perasaan atau pendapat mereka, dan sekaligus juga untuk mengontrol pemerintah serta parlemen .


Gerakan besar extra-parlemnter untuk mendorong revolusi


Seyogyanya, demikian jugalah kiranya di Indonesia !!! Mengingat banyaknya persoalan besar dan parah yang sedang dihadapi rakyat Indonesia, dan juga mengingat buruknya sikap politik para penguasa selama ini, ditambah dengan rendahnya sikap moral golongan elite kita pada umumnya, maka kebutuhan rakyat akan adanya kekuatan extra-parlementer yang kuat, yang luas, yang betul-betul  memihak rakyat kecil atau rakyat miskin, adalah besar sekali dan  mendesak atau urgen sekali.
Berbagai kegiatan atau usaha untuk membangun kekuatan extra-parlementer, dari mana pun datangnya atau oleh siapa pun pelakunya, adalah penting untuk mencegah supaya urusan negara dan bangsa hanya dikuasai  atau dimonopoli oleh wakil-wakil 9 partai di DPR dan di pemerintahan.


Gerakan extra-parlementer yang besar dan perkasa, akan bisa memberikan sumbangan penting untuk dibangunnya secara bersama-sama kekuatan revolusioner, yang merupakan syarat mutlak untuk terjadinya perubahan besar dan fundamental  (atau revolusi, menurut bahasa yang sering digunakan Bung Karno) bagi kepentingan rakyat banyak. Karena rakyat sudah tidak bisa lagi menaruh kepercayaan kepada wakil partai-partai politik yang duduk di DPR dan pemerintahan, maka gerakan extra-parlementer yang luas dan kuat merupakan  tameng atau senjata dan sekaligus pengayom kepentingan rakyat banyak, dalam perjuangan bersama meneruskan revolusi yang belum selesai, sesuai dengan ajaran-ajaran revosioner Bung Karno.
