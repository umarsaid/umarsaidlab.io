---
layout: post
title: PDI-P pilih Golkar untuk koalisi ?
date: 2003-12-02
---

Kalau berita yang disiarkan Jawa Pos (29 November 2003) itu mengandung kebenaran, maka kita semua harus siap-siap untuk menghadapi situasi politik, ekonomi dan sosial yang tidak menentu, atau menghadapi masalah-masalah besar sejak tahun 2004. Menurut berita itu, Taufik Kiemas (suami Presiden Megawati ) sudah menyatakan terang-terangan di Bali bahwa dalam usaha untuk memenangkan Megawati sebagai presiden kembali, PDI-P akan mengajak koalisi parpol yang mempunyai platform yang sama, yakni Pancasila. Ditegaskannya bahwa « peluang terbesar kita berkoalisi dengan Golkar ».

« Lantas, bagaimana bisa berkoalisi, sementara PDIP di mana-mana punya masalah dengan Partai Golkar? » , tanya Jawa Pos. Menurut Kiemas, hal itu tak perlu dirisaukan. Meski sakit dirasakan, itulah politik. "Bila menginginkan Ibu Megawati jadi presiden, kita harus berkoalisi. Karena itu, Golkar satu-satunya pilihan kita," ucapnya.

Menurut berita tersebut, Taufik Kiemas berbicara di depan para kader PDI-P di Tabanan, dan selama bertatap muka dengan mereka ia berusaha membakar semangat massa banteng untuk memenangi Pemilu 2004. “Apakah Anda siap mengantarkan Ibu Megawati menjadi presiden kembali?" pancing Kiemas. Massa langsung menjawab, "Siaaap." ((kutipan dari Jawa Pos selesai).

## Diperlukan Presiden Dan Pemerintahan Baru

Berita tersebut di atas mungkin mengejutkan banyak orang, meskipun sebagian lagi banyak juga yang menganggapnya sebagai sesuatu yang wajar-wajar saja, atau sebagian lagi acuh tak acuh. Namun, bagaimana pun juga, berita ini patut menjadi perhatian yang serius dari kita semua. Sebab, ini berkaitan erat dengan berbagai segi penting kehidupan Republik dan rakyat kita, yang berjumlah sekitar 210 juta orang. Jadi, bukanlah hanya urusan PDI-P saja, atau pun urusan Golkar saja.

Karena, ini menyangkut terpilihnya Presiden negara dan terbentuknya pemerintahan kita yang akan datang. Situasi negeri kita akan banyak ditentukan oleh kedua faktor yang penting ini. Sesudah tergulingnya Orde Barunya Suharto dkk, negara dan rakyat kita memerlukan adanya Presiden yang berwibawa, yang bijaksana, yang bermoral tinggi, yang disegani dan dihormati, yang sungguh-sungguh mengabdi kepada kepentingan rakyat, yang bersih dari KKN, yang betul-betul bisa jadi pemimpin bangsa.

Dan Republik kita, sesudah mengalami perusakan besar-besaran dan pembusukan parah oleh rezim militernya Suharto dkk (baca: sebagian pimpinan TNI-AD dan Golkar) selama lebih dari 32 tahun, memerlukan adanya reformasi total dan pembaruan moral besar-besaran di segala bidang. Untuk pekerjaan raksasa yang begini besar diperlukan adanya pemerintahan baru, yang dijiwai oleh semangat pengabdian yang baru pula, yang berlainan dengan berbagai pemerintahan sesudah peristiwa 65.

## Apa Artinya Koalisi PDI-P Dan Golkar

Kalau yang diberitakan oleh Jawa Pos tanggal 29 November 2003 mencerminkan kebenaran, maka ini akan merupakan suatu sinyalemen kepada seluruh gerakan pro-demokrasi di Indonesia (dan di luarnegeri) bahwa mereka harus menghadapi situasi yang lebih rumit (dan mungkin juga lebih sulit) dari pada selama pemerintahan Habibi, Gus Dur, dan Megawati-Hamzah sekarang ini. Oleh karena itu, seluruh berbagai gerakan pro-reformasi, yang memperjuangkan pembaruan serta perombakan atas sisa-sisa pemerintahan Orde Baru, perlu sejak sekarang merencanakan, mengatur, dan mengorganisasi gerakan atau perjuangan mereka sesuai dengan perkembangan yang baru ini.

Karena, pernyataan Taufik Kiemas kali ini cukup jelas dan tegas (sekali lagi, menurut Jawa Pos) ketika ia mengatakan bahwa “Bila menginginkan Ibu Megawati jadi presiden, kita harus berkoalisi. Karena itu, Golkar satu-satunya pilihan kita," ucapnya. Walaupun ini bukan pernyataan resmi DPP PDI-P (atau belum!) tapi karena diucapkan oleh Taufik Kiemas, maka mempunyai bobot dan juga arti tertentu.

Pernyataan Taufik Kiemas, kalau kemudian disusul dengan berbagai tindakan atau praktek kongkrit - yang dilakukan oleh baik PDI-P maupun Golkar - akan merupakan pertanda awal akan terjadinya perobahan-perobahan besar dalam peta politik di Indonesia serta goncangan-goncangan yang tidak kecil di kalangan berbagai gerakan yang memperjuangkan demokrasi, reformasi, penegakan hukum; keadilan sosial dan ekonomi, pemberantasan korupsi.

Bisa diramalkan bahwa koalisi PDI-P bersama Golkar (kalau akhirnya betul-betul terlaksana juga) akan menyebabkan makin tersingkirkannya PDI-P dari hati rakyat. Bahkan, karena menyatukan diri dengan Golkar, PDI-P akhirnya akan juga makin menjadi sasaran perjuangan berbagai gerakan rakyat. Perkembangan ke arah yang demikian ini adalah wajar, sebab Golkar sudah membuktikan diri - secara jelas dan selama lebih dari 32 tahun - sebagai penyangga utama Orde Baru. Dan Orde Baru telah dinajiskan oleh rakyat Indonesia.

## PDI-P Akan Jadi Tawanan Politik Golkar

Adalah amat penting (dan berguna) kalau para sarjana atau para pakar dari berbagai disiplin ilmu membikin studi tentang baik buruknya koalisi PDI-P dengan Golkar bagi negara dan bangsa, baik sekarang maupun bagi generasi yang akan datang. Sebab, kalau PDI-P betul berkoalisi dengan Golkar, maka akan terpaksa diadakan semacam perjanjian atau persetujuan atau konsesi-konsesi (tertulis atau tidak tertulis, dan rahasia atau tidak rahasia) mengenai hal-hal penting dan besar di berbagai bidang. Berikut adalah sejumlah bahan-bahan untuk sekadar direnungkan bersama :

Dengan mengadakan koalisi dengan Golkar, PDI-P akan terikat kaki dan tangannya, sehingga kehilangan kebebasan untuk bertindak (umpamanya dalam rangka : penegakan hukum, pemberantasan korupsi, pengadilan pelanggaran HAM, reformasi terhadap sisa-sisa Orde Baru). Selama ini saja, presiden Megawati sudah kelihatan “loyo” mengenai banyak hal, karena koalisinya dengan berbagai partai dalam kabinet “Gotong-royong”. Koalisi terang-terangan dengan Golkar akan lebih mempersulit lagi hal-hal yang selama ini tidak terselesaikan.

Dengan adanya koalisi dengan PDI-P, partai Golkar akan dapat “tameng” dari serangan-serangan lawannya, dan juga akan dapat berbagai kemudahan untuk menghindari tuntutan umum terhadap banyak kesalahan parah dan berbagai dosa besar yang pernah dibuatnya selama Orde Baru. PDI-P akan dihadapkan kepada dilemma (kalau Golkar diserang) : apakah harus membela Golkar, atau bersikap “netral” saja, atau pura-pura acuh-tak acuh. Sebab, logisnya, PDI-P terpaksa bersikap loyal terhadap partner koalisinya. Lagi pula, secara etika, dalam banyak hal PDI-P harus menunjukkan solidaritasnya, atau setidak-tidaknya tidak kelihatan merugikan kepentingan Golkar.

## Apa Lagi Yang Diperjuangkan PDI-“Perjuangan” ?

Kalau koalisi jadi terbentuk antara PDI-P dan Golkar, maka mungkin akan muncul berbagai suara di berbagai kalangan masyarakat, yang mempertanyakan “missi” PDI-P, umpamanya (dan antara lain) : PDI-P didirikan untuk tujuan apa? Karena PDI-P memilih Golkar sebagai partner, bagaimana penilaian PDI-P mengenai peran Golkar selama Orde Baru? Apakah karena sebagai partner koalisi, dosa dan kesalahan Golkar selama lebih dari 32 tahun bisa dianggap sepi saja? Apakah PDI-P tidak mengutuk Golkar karena sudah “men-tapol-kan” Bung Karno dan menghacurkan kekuatan pendukung beliau? Bagimanakah sikap PDI-P terhadap Golkar yang “membuta-tuli dan tutup mulut” selama lebih dari 32 tahun terhadap pembantaian terhadap jutaan orang tidak bersalah dan pemenjaraan jangka panjang ratusan ribu orang tapol?

Sesudah koalisi dengan Golkar nanti, orang akan lebih tidak mengerti lagi apakah yang sesungguhnya ”diperjuangkan” PDI-Perjuangan yang dipimpin oleh Megawati. Sebab, dengan berkoalisi dengan Golkar, masyarakat adil dan makmur yang jadi cita-cita bangsa Indonesia sejak proklamasi, akan kelihatan menjadi makin menjauh, dan keadilan dan penegakan hukum akan menjadi makin lebih remang-remang.

PDI-P makin tidak bisa diharapkan lagi bisa mengantar - dan menuntaskan - perjuangan reformasi. Sebab, reformasi berarti merombak atau merobah atau merobohkan sisa-sisa negatif Orde Baru. Dan kita tahu, bahwa Golkar yang sekarang adalah, pada hakekatnya, sisa Orde Baru yang paling menonjol secara kongkrit.

## Apa Itu Sebenarnya Golkar

Karena adanya “gagasan” yang menganggap baik (atau perlu, bahkan keharusan) terbentuknya koalisi antara PDI-P dan Golkar, maka seyogyanya masalah ini dijadikan studi umum (atau perdebatan) di berbagai kalangan (universitas, ornop, berbagai gerakan rakyat, pesantren, organisasi pemuda dan mahasiswa). Sebab, masalah Golkar adalah masalah penting untuk dipelajari oleh sebanyak mungkin kalangan, mengingat sejarah dan perannya yang amat negatif dan merusak bagi negara dan bangsa, seperti yang sudah dibuktikan dalam berbagai banyak praktek dalam jangka waktu yang begitu lama.

Seperti kita ketahui, Golkar didirikan oleh sebagian pimpinan TNI-AD dalam tahun 64 (artinya sebelum 65) untuk menghadapi politik Bung Karno yang dianggap condong ke kiri atau ke PKI, Artinya, Golkar, seperti halnya sebagian pimpinan TNI-AD (terutama waktu itu), adalah musuh Bung Karno dan musuh golongan kiri (termasuk PKI), Ini sudah terbukti dengan amat jelas dari politik Orde Baru selama puluhan tahun.

Dengan kalimat lain, atau dengan ungkapan lain, Golkar adalah pengkhianat Bung Karno. Dan dengan mengkhianati Bung Karno Golkar telah mengkhianati revolusi rakyat Indonesia. Tidak saja Golkar perlu dengan tegas, dan jelas, mengakui dosanya yang besar karena sudah ikut aktif “membunuh” Bung Karno secara politik, dan tidak saja Golkar perlu mengakui pula kesalahan-kesalahannya yang monumental karena sudah menjadi bagian yang tak terpisahkan dari Orde Baru selama kurun waktu yang begitu panjang. Melainkan Golkar (yang sekarang) harus secara aktif pula (dan secara kongkrit) ikut gerakan untuk merehabilitasi Bung Karno dan para korban peristiwa 65, dan ikut secara aktif dan kongkrit dalam perjuangan merealisasikan reformasi

Tentang apakah Golkar punya platform yang sama dengan PDI-P mengenai Pancasila, perlu sama-sama kita ingat bahwa Golkar pernah ikut-ikut pimpinan rezim militer Suharto dkk dalam mengebiri, memalsu, mensalahtafsirkan, dan menghina Pancasila. Karena, selama Orde Baru, Pancasila telah dibikin begitu memuakkan dan menjijikkan (ingat, antara lain, kursus-kursus atau “penataran” P4) sambil melaksanakan berbagai politik yang tidak berperikemanusiaan ( sekali lagi, ingat pembantaian besar-besaran tahun 65 dan pemenjaraan ratusan ribu tapol tidak bersalah selama puluhan tahun)

Begitu besar kesalahan atau dosa Golkar terhadap Pancasila, sehingga tokoh-tokohnya sama sekali tidak pantas (dan tidak berhak!) berbicara muluk-muluk tentang Pancasila karena mereka telah mengkhianati penggalinya, yaitu Bung Karno, dan bahkan men-‘tapol’-kan beliau dengan cara-cara sadis.

Dengan makin dekatnya Pemilu dan pemilihan presiden maka makin urgen pula bagi kita semua untuk menelaah dalam-dalam apakah suatu partai politik atau suatu kekuatan sosial semacam Golkar itu dapat diajak dalam koalisi untuk memimpin negara dan bangsa? Apakah pengalaman 32 tahun masa suram Orde Baru tidak merupakan bukti yang cukup untuk menilai apa itu sebenarnya Golkar? Apakah sesudah hasil negatif selama 32 tahun Golkar masih bisa diharapkan bisa memainkan peran yang positif bagi bangsa dan negara?
