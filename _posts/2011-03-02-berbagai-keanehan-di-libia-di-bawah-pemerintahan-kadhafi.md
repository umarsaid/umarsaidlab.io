---
layout: post
title: Berbagai Keanehan di Libia Di Bawah Pemerintahan Kadhafi
date: 2011-03-02
---

Pergolakan-pergolakan  besar yang menunjukkan kebangkitan rakyat di negara-negara Arab untuk memperjuangkan kebebasan dan demokrasi masih terus berlangsung dan bahkan makin berkembang. Perkembangan demikian ini kelihatan  -- dalam skala yang berbeda-beda - di Maroko, Aljazair, Tunisia, Libia, Jordania, Siria, Bahrein, Yaman, bahkan juga di Irak dan Mauritania akhir-akhir ini ! Dan juga, yang terbaru, di Oman, negara kecil  yang sudah diperintah oleh seorang Sultan selama 40 tahun !!! Sederetan pergolakan-pergolakan besar dan kecil, yang sambung-menyambung di begitu banyak negara Arab sekaligus ini mengindikasikan bahwa di negara-negara itu dibutuhkan sekali adanya perubahan besar-besaran.

Bahwa di Mauritania massa rakyat sudah mulai ikut-ikut bergerak menuntut digantinya pemerintahan dan dijalankannya demokrasi menunjukkan bahwa perjuangan rakyat yang dimulai di negara-negara Arab di Afrika Utara sekarang sudah menjalar ke negara-negara sebelah Selatan Sahara, yang mungkin nantinya akan meluas ke negara-negara Afrika hitam yang berpenduduk Islam lainnya.
Perkembangan yang amat penting juga adalah mulai adanya pergolakan-pergolakan di Iran yang non-Arab, walaupun masih belum besar.

Dari aksi-aksi yang dilakukan oleh massa rakyat di banyak negara-negara Arab ‘(dan non-Arab)  yang bisa dilihat dalam berbagai siaran televisi dunia, kelihatan sekali bahwa  massa rakyat (yang dimana-mana dipelopori oleh generasi muda !), menuntut digantinya sistem pemerintahan, sosial, ekonomi dan kebudayaan, yang kebanyakan mengalami kemandegan, keterbelakangan, bahkan kemunduran.

Kecuali sejumlah kecil negara-negara seperti Kuwait, Libanon,, dan negara-negara yang tergabung dalam Emirat Arab (Dubai, Abu Dhabi,Qatar) maka boleh dikatakan bahwa kebanyakan negara-negara Arab pada umumnya ada dalam “keterbelakangan”. Bahkan, segi-segi keterbelakangan ini juga nampak di negara Saudi Arabia, yang sangat kaya-raya berkat adanya sumber-sumber minyak yang besar sekali.

Libia-nya Kadhafi adalah contoh paling jelek

Dari sekitar dua puluhan negara Arab di dunia, Libia di bawah kolonel Muammar Kadhafi selama 42 tahun adalah contoh yang paling tipikal jeleknya, dalam banyak hal, dan itu pun sejak lama. Dan dibandingkan dengan kasus presiden Tunisia, Ben Ali, maupun kasus presiden Mesir Husni Mubarak, yang dua-duanya sudah ditendang oleh rakyatnya masing-masing, maka kasus Kadhafi adalah jauh lebih “luar biasa” buruknya bagi rakyat Libia.

Kalau presiden Ben Ali sekarang diketahui sebagai maling besar sekali (harap ingat : tumpukan-tumpukan besar uang kertas Euro dan dollar  dalam brankas di kantornya yang disiarkan oleh banyak televisi di dunia dan berton-ton mas yang dibawa lari ke Jedah) dan kalau sekarang terbongkar bahwa presiden Mubarak juga memiliki kekayaan besar di Inggris, Spanyol, Prancis, Dubai dll, maka sekarang mulailah terbongkar segala kejahatan yang dilakukan Kadhafi terhadap rakyat Libia.

Banyak politik atau tindakan buruk yang dilakukan oleh Kadhafi menunjukkan bahwa ia merupakan tokoh yang paling membanggakan diri  atau mengangkat dirinya sebagai “pemimpin” tertinggi Islam, namun   -- dalam prakteknya -- banyak tindakannya malah merugikan citra kepemimpinan Islam, dan bahkan merusak nama Islam.

Apa yang terjadi akhir-akhir ini di Libia membuktikan lebih jelas lagi  bahwa Kadhafi adalah memang seorang yang tidak pantas – dan tidak boleh !!! – meneruskan  kedudukannya sebagai kepala negara dan pemimpin ummat Islam. Makin cepat ia terdepak dari kursi kepresidenannya, maka akan makin baik bagi rakyat Libia sendiri, maupun bagi berbagai rakyat lainnnya. Dan, syukurlah bahwa banyak indikasi di Libia sendiri maupun di bidang internasional menunjukkan ke arah itu, yaitu berakhirnya kekuasaan Kadhafi.

Sanksi Dewan Keamanan PBB terhadap Kadhafi

Pada permulaan Maret ini kedudukan Kadhafi (beserta keluarganya, termasuk anak-anaknya yang menduduki jabatan yang penting-penting di Libia) makin terjepit di dalam negeri maupun di luar negeri. Dewan Keamanan PBB sudah menjatuhkan sanksi kepada  Kadhafi beserta keluarganya, dengan keputusan untuk melakukan embargo pengiriman semua macam senjata ke Libia, melarang perjalanan untuknya dan semua keluarganya, pembekuan harta kekayaannya dan harta sekitar 20 orang terdekatnya. Libia sudah dinyatakan tidak berhak lagi duduk dalam Dewan HAM PBB, karena keanggotaannya dibekukan akibat pelanggaran-pelanggaran HAM yang terjadi akhir-akhir ini.

Amerika Serikat, Kanada, Inggris dan berbagai negara lainnya sudah membekukan rekening Kadhafi beserta keluarganya. Presiden Obama sudah terang-terangan menyatakan bahwa Kadhafi harus meninggalkan jabatannya sebagai presiden, mulai sekarang juga. Ia mengatakan bahwa seorang presiden yang ingin berkuasa terus tetapi dengan melakukan kekerasan terhadap rakyatnya sendiri
berarti sudah kehilangan legitimasi untuk memerintah.

Sekarang sedang diusahakan oleh berbagai kalangan internasional untuk mengajukan Kadhafi ke depan Mahkamah Pidana Internasional, untuk diperiksa dalam pelanggarannya terhadap HAM dan pembunuhan besar-besaran terhadap rakyat Libia.

Dengan perlakuan dunia internasional yang demikian terhadapnya,  maka Kadhafi,  yang selama 42 tahun pernah menampilkan diri sebagai pemimpin Muslim terkemuka (termasuk di dunia !), sekarang menjadi sasaran hujatan di dunia internasional..

Perlawanan hebat dari dalam negeri Libia sendiri

Walaupun kehancuran kekuasaan Kadhafi adalah karena faktor-faktor dalam negeri dan luar negeri yang saling berhubungan, namun faktor dalam negerilah yang merupakan sebab-sebab yang terutama. Meskipun dalam masa selama 42 tahun pemerintahan Kadhafi sudah merealisasikan banyak projek pembangunan dan memajukan infrastruktur (antara lain pembuatan sungai di bawah tanah Sahara, yang merupakan projek besar di dunia) dan mengundang berbagai maskapai besar internasional untuk pemboran minyak dan gas, namun di berbagai bidang lainnya (antara lain : pemerintahan, politik, ekonomi,sosial, kebudayaan) merupakan hambatan untuk kemajuan rakyat dan menjadi alat Kadhafi untuk penindasan.

Begitu buruknya sistem Kadhafi ini di banyak bidang, terutama sejak terjadinya pergolakan rakyat sebagai dampak peristiwa Tunisia dan Mesir, sehingga  duta besar Libia untuk PBB ketika mengumumkan pengunduran dirinya sebagai wakil Libia di PBB ia mengatakan bahwa Kadhafi adalah seorang fasis, dan karenanya ia menyerukan kepada tentara Libia dan rakyat bangkit melawannya. Menteri dalam negeri Libia dan menteri kehakiman juga mengatakan tidak mendukung pemerintahan Kadhafi lagi, seperti banyak sekali dutabesar-dutabesar Libia di banyak negara, dan menyatakan bergabung dengan gerakan rakyat. Dutabesar Libia di Belgia yang juga meletakkan jabatannya mengatakan bahwa Kadhafi adalah seorang tiran dan fasis.

Perkembangan perjuangan rakyat Libia sudah demikian luasnya dan besarnya, sehingga sebagian besar daerah sudah dibebaskan dari kekuasaan Kadhafi dan mendirikan administrasi sementara dan bahkan mengibarkan kembali bendera Libia yang dipakai sebelum Kadhafi memerintah. Dari segi ini kelihatan bahwa perjuangan rakyat Libia melawan Kadhafi berlangsung lebih menjangkau jauh, lebih besar dan lebih luas dibandingkan dengan apa yang terjadi di Tunisia dan di Mesir.

Perjuangan rakyat Libia lebih berat dari pada yang lain-lain

Perjuangan rakyat Libia melawan Kadhafi juga lebih berat dan mungkin juga lebih rumit, dari pada perjuangan di Tunisia dan Mesir, disebabkan banyaknya dan luasnya hal-hal yang buruk, usang, dan reaksioner, yang sudah lebih dari 40 tahun dijalankan oleh Kadhafi dengan tangan besi. Karena itu, terdapat berbagai hal-hal yang “aneh-aneh” (bizarreries, dalam bahasa Prancisnya) di Libia, yang menjadikan Libia sebagai negara yang berbeda  dari pada negara-negara Arab lainnya. Sebagian kecil di antara hal-hal  yang “aneh-aneh” itu adalah yang sebagai berikut :

--  Selama 42 tahun sejak didirikannya “Jamhairiyah Arab Libia Rakyat dan Sosialis” negara ini tidak mempunyai konstitusi atau undang-undang dasar.

--  Negara Libia tidak mempunyai bendera seperti yang dipunyai negara-negara lain di dunia, melainkan hanya sehelai kain yang berwarna hijau (sejak tahun 1977), yang dianggap sebagai lambang Islam

--  Kadhafi sering menyatakan bahwa dirinya bukanlah presiden negara, melainkan pemimpin revolusi, dan karena itu ia tidak bisa dituntut turun sebagai presiden

--  Libia diperintah oleh seorang pemimpin yang bertindak sebagai diktator dan merupakan  negara yang tidak mempunyai parlemen, dan juga tidak mengadakan pemilu, dan tidak menjalankan demokrasi seperti yang dijalankan di banyak  negara-negara lainnya.

--  Kadhafi menjadikan “buku hijau” (diterbitkan tahun 1979)  yang berisi kutipan-kutipan dari tulisan, pidato atau fikiran-fikirannya sebagai petunjuk bagi kehidupan rakyat Libia. Buku yang berkulit hijau (sebagai warna Islam)  ini dicetak jutaan eksemplar dan merupakan bacaan wajib

--  Di Libia, dewan perwakilan rakyat yang di negara-negara di dunia biasanya disebut parlemen nasional atau lokal,  diganti dengan congres atau comite revolusioner rakyat di berbagai tingkat, yang juga diberi nama “demokrasi langsung”

Ucapan-ucapan Kadhafi yang keterlaluan .....

Selama memerintah Libia selama 42 tahun Kadhai terkenal dengan ucapan-ucapannya yang galak mengenai imperialisme, yang serba megalomania mengenai “revolusi hijau”-nya, sehingga dalam jangka cukup lama banyak orang mempunyai pandangan yang salah (artinya terlalu baik) tentang dia.
Karena itu, ia pernah diangkat sebagai pimpinan persatuan negara-negara Afrika (Union Africaine) dalam sidangnya di Adis Abeba tahun 2009.

Megalomanianya yang keterlaluan juga kelihatan dari ucapannya ketika selesai menghadiri sidang puncak negara-negara Arab di Saudi Arabia 31 Maret  2009. Dengan tidak tanggung-tanggung ia mengatakan :” Saya adalah pemimpin internasional, sesepuh (doyen, bhs Prancis) dunia Arab, raja dari segala raja Afrika, dan imam dari seluruh orang Muslim, dan status internasional saya tidak membolehkan saya lebih rendah dari itu” (dikutib dari Wikipedia)

Untuk mengukur “kehebatan” pikirannya patut kiranya disajikan berikut ini  ucapannya tentang presiden Tunisia Ben Ali,  yang sebagai berikut : “ Rakyat Tunisia, kalian sedang mengalami kehilangan yang besar. Tidak ada orang lainnya yang lebih baik kecuali Ben Ali untuk memerintah Tunisia”

Kadhafi orang yang “istimewa” dalam banyak hal

Dari bahan-bahan yang disajikan di atas nyatalah bahwa Kadhafi memang bisa dimasukkan dalam kategori orang-orang yang “istimewa” dalam banyak hal. Sebagai seorang opsir yang berumur 27 tahun ia sudah melakukan kudeta terhadap raja Idris, dan mendirikan negara “Islam dan revolusioner” dengan program dan tata negara dan cara kerja yang cukup aneh bagi kebanyakan orang.

Berbagai negara-negara Arab juga menjalankan sebagian dari apa yang terjadi di Libia, namun pada umumnya tidak se-ekstrim di Libia. Seperti yang bisa sama-sama ketahui, negara-negara Arab yang sedang dilanda oleh gerakan massa rakyat untuk kebebasan dan demokrasi pada umumnya diperintah oleh para penguasanya (para presiden, para raja,  para Sultan, atau para Emir) yang kolot, reaksioner, korup, otokratis dan menindas kebebasan rakyat.

Di berbagai negara-negara ini,  para penguasanya menjalankan sistem politik, ekonomi, sosial, kebudayaan (dan adat-istiadat dan agama)  yang bertentangan dengan demokrasi, dengan HAM, yang menghambat kemajuan peradaban ummat. Dalam banyak hal, para penguasa di berbagai negara ini menggunakan (tepatnya :menyalahgunakan) adat-istiadat, kepercayaan, mentalitas kesukuan, untuk kepentingan mereka dan golongan mereka, di atas penderitaan orang banyak.

Akan lahir pemikiran-pemikiran baru  melawan yang usang

Situasi yang sudah lama memborgol kebebasan rakyat atau membungkam suara untuk demokrasi di begitu banyak negara itulah yang dewasa ini sedang didobrak oleh gerakan massa ( harap catat baik-baik : terutama oleh generasi muda !).  Begitu hebatnya, dan begitu besarnya, atau begitu  luasnya gerakan massa untuk kebebasan dan demokrasi yang melanda begitu banyak negara-negara dewasa ini, sehingga tiap hari televisi dan media massa di dunia  tidak henti-hentinya menyajikan tiap hari siaran mengenai perkembangan yang begitu penting bagi dunia dewasa ini.

Melalui dan dengan gerakan massa – sekali lagi : terutama generasi mudanya – di berbagai negara-negara Arab (dan non-Arab yang berpenduduk Muslim) akan lahir era pemikiran-pemikiran baru, perubahan atau pembaruan, dalam usaha bersama untuk meninggalkan segala macam adat dan faham tua, yang selama ribuan tahun sudah menjadi belenggu ummat manusia.

Dari sudut pandang ini, nyatalah bahwa kita semua sedang menyaksikan berlangsungnya kejadian-kejadian yang amat bersejarah bagi dunia !!!
