---
layout: post
title: Masalah korupsi Suharto dan PBB-Bank Dunia
date: 2007-09-20
---

Mohon perhatian para pembaca terhadap berita Suara Pembaruan (3 Desember 2007) yang berjudul «Kembalikan Harta Negara yang Dicuri Soeharto » yang disajikan selengkapnya seperti di bawah ini. Karena, dari berita ini saja kita bisa mendapat gambaran bahwa usaha PBB dan Bank Dunia membantu berbagai negara untuk memperoleh kembali harta yang dicuri oleh para koruptor akan menemui berbagai kesulitan atau halangan di Indonesia.


Artinya, usaha untuk membongkar kejahatan Suharto (beserta keluarganya) yang berupa KKN (korupsi, kolusi dan nepotisme) akan tetap sulit dilaksanakan , karena masih adanya atau banyaknya pejabat di berbagai aparat atau lembaga negara yang masih bersimpati kepada rejim Orde Baru. Padahal, banyak orang yakin bahwa harta Suharto (beserta keluarganya, terutama Tommy Suharto) yang bertumpuk-tumpuk sampai menggunung itu bukanlah hasil jerih payah usaha secara normal dan jujur, melainkan karena menyalahgunakan kekuasaan, alias kejahatan.


Sebab, menurut nalar yang sehat, jelaslah bahwa kekayaan Suharto (dan anak-anaknya, dan terutama Tommy Suharto) tidak mungkin mencapai sampai begitu besar (harap baca kembali laporan majalah Time dan hasil riset George Aditjondro), kalau tidak melalui jalan yang tidak sah alias jalan haram.
Karena itu, ketika kita membaca bahwa menurut Jaksa Agung Hendarman Supanji temuan PBB dan Bank Dunia mengenai Suharto itu “ masih diragukan kebenarannya” patutlah kita mempertanyakan kejujurannya dan juga mempersoalkan integritas pernyataannya tersebut. Pernyataan Jaksa Agung yang demikian ini menggambarkan -secara implisit -- tidak adanya “political will” untuk mengusut kejahatan Suharto. Dan kita bisa menduga bahwa sikap Jaksa Agung yang demikian ini tidaklah berdiri sendirian, artinya ada kekuatan yang mendukung. Dan bahwa yang mendukung sikap Jaksa Agung ini adalah kalangan sisa-sisa Orde Baru yang masih berpengaruh.

Jadi, kita semua perlu menyadari bahwa berbagai kejahatan Suharto itu tidak akan bisa terbongkar dan diadili, selama sisa-sisa kekuatan Orde Baru masih bercokol terus di berbagai aparat atau lembaga negara. Hanya kekuasaan politik yang betul-betul anti Orde Baru-lah yang bisa dan mau mengadili berbagai kejahatan Suharto. Dan hanya kekuasaan politik yang anti-Orde Baru-lah yang bisa dan mau betul-betul menjalankan reformasi secara tuntas. Sekarang makin jelas bagi kita bahwa untuk memperbaiki segala kebobrokan moral yang sudah merajalela di kalangan « orang-orang tua », diharapkan munculnya peran angkatan muda (kaum muda) untuk berusaha mengambil-alih kekuasaan politik. Sebab, sesudah 32 tahun kekuasaan di bawah Suharto dan diteruskan dengan 10 tahun berbagai kekuasaan di bawah Habibi, Gus Dur, Megawati dan SBY-Kalla, maka keadaan negara kita tetap menghadapi berbagai kerusakan dan kemerosotan.

Pemilu tahun 2009 juga tidak akan (tidak mungkin !!!) bisa mendatangkan perubahan-perubahan besar, karena hasilnya akan tetap dikuasai partai-partai politik yang selama ini ikut bertanggung-jawab terhadap segala kebobrokan, termasuk kebobrokan moral yang tercermin dalam penyakit korupsi yang sudah merajalela.

Perubahan besar, termasuk dibongkarnya kejahatan-kejahatan Suharto, hanya bisa dilaksanakan kalau ada perubahan yang substansial dalam kekuasaan politik !!!

A. Umar Said

***


Berita di Suara Pembaruan (tanggal 3 Desember 2007) itu adalah sebagai berikut :


Medio Oktober 2007, Stolen Asset Recovery (StAR) Initiative: Challenges, Opportunities and Action Plan mempublikasikan harta jarahan sejumlah pemimpin dan mantan pemimpin negara. Mantan Presiden Soeharto ditempatkan sebagai orang yang paling banyak mencuri harga negara dengan jumlah US$ 15 juta - US$ 35 miliar.


Pengumuman yang resmi dilakukan PBB dan Bank Dunia itu memunculkan sedikitnya dua pendapat di dalam negeri. Pertama, temuan dan pengumuman itu disambut positif. Temuan PBB dan Bank Dunia itu harus dijadikan pintu masuk mengusut harta Soeharto di luar negeri dan mengembalikan harta-harta itu ke negara.
Advokat senior, Todung Mulya Lubis mengatakan, PBB dan Bank Dunia adalah lembaga yang kredibel, sehingga tidak mungkin temuan itu mengada-ada dan tidak berdasar pada fakta dan metodologi yang benar. Oleh karena itu, kata Todung, pemerintah harus menindaklanjuti secara serius temuan tersebut.
Sosiolog yang juga penulis buku "Korupsi Kepresidenan, Reproduksi Oligarki Berkaki Tiga: Istana, Tangsi, dan Partai Penguasa," George Junus Aditjondro mengatakan, temuan PBB dan Bank Dunia tak berlebihan. Pemerintah, dalam hal ini Kejaksaan Agung, segera mengambil langkah mengembalikan aset negara yang dicuri Soeharto yang kini berada di sejumlah negara.


Kedua, temuan PBB dan Bank Dunia itu tidak perlu digubris. Pasalnya, temuan itu didasarkan sumber-sumber yang diragukan. "Laporan PBB dan Bank Dunia itu hanya berdasarkan pada kliping koran. Jadi diragukan kebenarannya," kata salah satu kuasa hukum Soeharto, Muhammad Assegaf.
Jaksa Agung Hendarman Supandji ketika menjadi pembicara kunci dalam seminar Pengkajian Hukum Nasional 2007 dengan tema "Pengembalian Aset Melalui Instrumen Stolen Asset Recovery (StAR) Initiative, dan Perundang-undangan Indonesia" di Jakarta, Rabu (28/11), mengatakan, temuan PBB dan Bank Dunia itu masih diragukan kebenarannya.


Kemauan Politik
Ketua Program Studi Magister Hukum Fakultas Hukum Universitas Airlangga, Surabaya, Peter Mahmud Marzuki dalam makalahnya sebagai salah satu pembicara dalam seminar yang diselenggarakan Komisi Hukum Nasional (KHN) di atas mengatakan, pemerintah harus tetap berusaha mengembalikan harta negara yang dicuri Soeharto.


Untuk melakukan itu, kata dia, pemerintah harus memiliki kemauan politik yang sungguh-sungguh dan membentuk lembaga khusus. Lembaga ini hanya menginventarisasi kontrak-kontrak yang dibuat oleh BUMN-BUMN pada rezim Soeharto, akte-akte pendirian perusahaan yang dibangun pada masa pemerintahan Soeharto sampai lengsernya Soeharto, serta kontrak-kontrak yang dibuat oleh perusahaan-perusahaan itu.
Akan tetapi, untuk mengetahui akte-akte terutama akte notaris dan dokumen-dokumen itu diperlukan izin pengadilan. Dalam hal inilah pengadilan dapat membantu apakah perusahaan-perusahaan yang didirikan itu memang layak didirikan.


Dalam studi yang dilakukan oleh United Nations Office on Drugs and Crime (UNODC) dan World Bank Group (WBG), kata Peter, inisiatif mengembalikan aset yang telah dicuri oleh suatu rezim yang memerintah secara otoriter dan korup mulai dalam negeri negara yang asetnya telah dicuri itu. [SP/Siprianus Edi Hardum]

* * *

Catatan A. Umar Said

Masalah korupsi Suharto dan PBB-Bank Dunia

Seperti yang sudah diduga semula, berita tentang dinyatakannya Suharto sebagai pencuri nomor satu di dunia oleh PBB dan Bank Dunia, telah menarik perhatian besar sekali dari berbagai kalangan di Indonesia dan di luarnegeri. Berita penting yang mengagetkan banyak orang itu juga membangkitkan beragam reaksi, termasuk dari berbagai penjuru dunia.

Perhatian yang besar dari berbagai kalangan di Indonesia terhadap berita bahwa Suharto diumumkan sebagai pencuri uang negara dan rakyat ini tercermin dari disiarkannya segala macam informasi dan reaksi dalam pers dan lewat Internet. Dalam rangka untuk memudahkan bagi setiap orang yang berminat untuk mengikuti atau mengamati perkembangan sekitar masalah besar ini, telah disediakan rubrik khusus dalam website http://kontak.club.fr/index.htm, yang berjudul “Kumpulan berita masalah Suharto dan PBB-Bank Dunia”.

Rubrik khusus ini diadakan mengingat pentingnya masalah korupsi Suharto, yang sekarang ini sudah menjadi masalah besar di skala internasional, dengan dilansirkannya inisiatif yang diambil PBB dan World Bank dalam rangka program StAR (Stolen Asset Recovery) Initiative, atau “Prakarsa Pengembalian Kekayaan Negara yang Dicuri”.

Perspektif cerah gugatan terhadap Suharto
Dengan menyimak “Kumpulan berita masalah Suharto dan PBB-Bank Dunia” kita akan mengetahui bahwa dengan adanya prakarsa PBB dan Bank Dunia untuk memburu para pemimpin dan pejabat korupsi yang mencuri uang rakyat di negara-negara berkembang, maka perjuangan menggugat korupsi Suharto, yang sudah lama dilakukan berbagai kalangan dan golongan di Indonesia, akan mempunyai perspektif yang lebih cerah daripada yang sudah-sudah.

Dalam daftar pemimpin-pemimpin korup di dunia, yang dibuat oleh dokumen PBB dan Bank Dunia, yang disiarkan tanggal 17 September 2007 di Markas Besar PBB, dengan dihadiri banyak pembesar PBB dan pejabat penting berbagai negara (termasuk dari Indonesia) nama Suharto tercantum paling atas. Bisa dikatakan, karenanya, bahwa isi dokumen PBB dan Bank Dunia tersebut merupakan bukti bahwa laporan majalah TIME tanggal 24 Mei 1999 ( yang digugat oleh Suharto dan dimenangkan oleh Mahkamah Agung) mengandung berbagai hal yang benar.

Dengan dikeluarkannya pernyataan PBB dan Bank Dunia mengenai tempat teratas dalam daftar para koruptor besar dunia, maka putusan sidang kasasi Mahkamah Agung, yang dipimpin Mayjen TNI (Pur) kelihatan makin janggal dan menimbulkan kecurigaan akan latar-belakangnya, atau mengundang pertanyaan akan keabsahannya. Apalagi, karena dimenangkannya gugatan Suharto terhadap majalah TIME adalah atas dasar untuk membela “kehormatan atau nama baik Suharto” sebagai mantan jenderal TNI dan presiden RI.

Suharto anggap berita PBB itu sebagai sampah


Sebab, dengan sudah dinyatakannya Suharto sebagai pencuri nomor satu di dunia – yang menurut PBB dan Bank Dunia dan majalah TIME diduga mencuri uang rakyat sebesar antara 15 miliar sampai 35 miliar dollar AS -- apakah Suharto masih pantas berkaok-kaok tentang “kehormatan atau nama baik”, walaupun ia pernah menjabat sebagai jenderal TNI dan mantan presiden ?

Dengan mempertimbangkan kejahatan-kejahatannya yang sudah bertumpuk-tumpuk selama puluhan tahun di bidang KKN dan pelanggaran HAM, sudah benarlah kalau ada orang yang menyebutkannya sebagai “sampah”. Dan, orang yang sudah begitu terkenal kebusukannya inilah yang malahan menganggap sepi saja atau meremehkan sikap PBB dan Bank Dunia mengenai dirinya. Bahkan, Suharto menganggap berita tentang dirinya itu “sebagai sampah saja”.

Menurut harian Jawapos (19 Sept 07) ,”saat ditanya tentang peluncuran daftar tersebut, mantan Presiden Suharto mengabaikan rilis yang dikeluarkan PBB itu. Kriteria rilis tersebut dianggap tidak jelas dan tidak berasal dari sumber akurat yang dapat dipertanggungjawabkan secara yuridis. « Klien saya menganggap berita tersebut sebagai sampah," kata salah seorang pengacara Suharto, Juan Felix Tampubolon, saat dihubungi Jawapos.

Apakah Suharto berani menggugat PBB?

Menurut Juan Felix, di balik rilis tersebut, tersimpan kepentingan pihak tertentu yang terus-menerus ingin menyudutkan Suharto. Dia lantas menyebut salah satu LSM internasional yang getol menyuarakan kasus korupsi kliennya. "LSM itu punya kepentingan. Dan, semua orang pun tahu," jelasnya. (Jawapos, 19 September 2007)

Ditanya apakah Suharto bakal mengajukan somasi atau bahkan gugatan kepada para pihak yang merilis informasi tersebut, Juan Felix mengatakan akan mempertimbangkan. "Saya kira perlu dipelajari dulu," ujarnya. (kutipan dari Jawapos selesai).

Kiranya, adalah suatu peristiwa yang akan tambah sangat menarik kalau Suharto bisa dihasut, atau dipengaruhi, oleh orang-orang terdekatnya untuk mengajukan gugatan terhadap PBB dan Bank Dunia, karena dua badan internasional ini telah menyatakannya sebagai koruptor yang paling besar di dunia.

Sebab, untuk menggugat dua badan internasional ini posisi Suharto lemah sekali. Kelemahannya terletak pada banyaknya kejahatan yang telah dilakukannya lewat KKN yang sebagian kecilnya bisa ditelusuri dengan mengamati kekayaan Tutut, Bambang, Sigit, Titiek, Tommy, Probosutedjo, di samping kekayaan atas nama Suharto sendiri. Gugatan Suharto terhadap PBB dan Bank Dunia akan merupakan langkah yang makin menjebloskan namanya - yang sudah buruk itu -- dalam jurang yang lebih dalam lagi.

Sikap Bank Dunia positif sekali

Perkembangan yang menarik juga ialah tindakan Bank Dunia, yang mungkin di kemudian hari akan memberikan sumbangan yang besar artinya dalam membongkar jaring-jaringan harta haram Suharto beserta keluarganya. Menurut Indopos/Jawapos (20/9/07) ” Bank Dunia bergerak cepat dalam memburu mantan Presiden Suharto yang ditempatkan sebagai mantan penguasa terkorup. Lembaga kredibel itu kemarin menyerahkan segepok dokumen kepada Kejaksaan Agung (Kejagung). Data-data itu berisi tentang uang milik mantan penguasa Orde Baru tersebut yang tersimpan di sejumlah bank di luar negeri (LN).

Dokumen itu diterima langsung oleh Jaksa Agung Hendarman Supandji dalam pertemuan yang berlangsung di Gedung Bundar. Bank Dunia diwakili Kepala Kantor Perwakilan Bank Dunia di Jakarta Joachim von Amsberg. (baca berita selengkapnya dalam « Kumpulan berita masalah Suharto dan PBB-Bank Dunia) Di samping itu, diberitakan bahwa dalam kunjungannya presiden SBY ke AS (22-26 September yad) untuk menghadiri Sidang Umum PBB, direncanakan ada pertemuan antara presiden SBY dengan presiden Bank Dunia dan pejabat-pejabat penting lainnya, untuk membicarakan langkah-langkah apa yang bisa dilakukan untuk merealisasikan tujuan “Prakarsa Pengembalian Kekayaan Negara yang Dicuri”.

Selain itu, kejaksaan dan Bank Dunia bersepakat mengintensifkan komunikasi dengan membentuk petugas penghubung alias liaison officer (LO) untuk membahas rilis StAR Initiative. Di kejaksaan, petugas penghubung tersebut akan diproses oleh bagian JAM Pembinaan.

Waspadalah terhadap sisa-sisa Orba
Perlulah kiranya dicatat di sini, bahwa dengan adanya sikap positif sekali dari PBB dan Bank Dunia untuk mengusahakan dikembalikannya kekayaan negara yang dicuri Suharto, kita harapkan adanya sikap yang tidak memalukan bangsa dari para pejabat tinggi pemerintahan yang ikut menanganinya. Sebab, kalangan PBB maupun Bank Dunia tentu mempunyai informasi-informasi dan data-data mengenai masih banyaknya sisa-sisa Orde Baru atau simpatisan-simpatisan Suharto di berbagai lembaga pemerintahan.

Semua fihak perlu waspada terhadap oknum-oknum (di Kejaksaan Agung atau badan-badan lainnya) yang menghalangi atau menyabot -- dengan berbagai cara dan bentuk -- pelaksanaan program StAR Initiative dari PBB dan Bank Dunia mengenai kasus Suharto.

Sebab, untuk kali ini, dalam hal kasus harta haram Suharto, sikap atau inisiatif PBB dan Bank Dunia, perlu disambut gembira oleh kita semua.
