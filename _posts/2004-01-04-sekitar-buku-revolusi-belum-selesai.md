---
layout: post
title: 'Sekitar buku "Revolusi belum selesai"'
date: 2004-01-04
---

Dalam rangka mengusahakan diteruskannya perjuangan untuk reformasi, untuk rekonsiliasi nasional, dan penulisan kembali sejarah Indonesia, ada inisiatif penting yang perlu mendapat perhatian besar dari kita semua. Inisiatif ini berupa dicetaknya buku “ Revolusi belum selesai”. Dua jilid buku “Revolusi belum selesai” yang diterbitkan oleh Masyarakat Indonesia Sadar Sejarah (Semarang) bulan Juli 2003 merupakan dokumen sejarah yang amat penting bagi bangsa Indonesia. Dalam kedua jilid buku ini terkumpul 103 pidato-pidato Bung Karno, yang diucapkannya dalam berbagai kesempatan, sesudah peristiwa G30S di tahun 1965 sampai disampaikannya (oleh Bung Karno kepada MPRS) Pelengkap Nawaksara tanggal 10 Januari 1967. Kata pengantar buku ini dibuat oleh sejarawan Sdr Aswi Warman Adam.

Sebagian terbesar dari pidato-pidato Bung Karno yang terdapat dalam dua jilid buku ini (jilid pertama terdiri dari 443 halaman dan jilid kedua 456 halaman) merupakan bahan-bahan yang selama hampir 38 tahun ini tidak pernah - atau sedikit sekali - diketahui oleh umum. Sebab, rezim militer Orde Baru telah melakukan berbagai usaha, dan dengan macam-macam cara, supaya pidato-pidato Bung Karno ini tidak bisa dibaca (atau diketahui) oleh masyarakat luas.

Kalaupun ada koran atau majalah Indonesia pada waktu itu pernah memberitakan sejumlah kecil pidato Bung Karno itu, maka hanyalah sepotong-sepotong atau singkat-singkat saja, atau diplintir-plintir dan umumnya dengan nada negatif pula. Bolehlah dikatakan bahwa rezim militer Orde Baru selama 32 tahun telah memblack-out atau “menyembunyikan” bahan-bahan penting dan amat bersejarah ini.

Buku “Revolusi belum selesai” ini adalah hasil jerih payah kalangan muda yang tergabung dalam Mesias (Masyarakat Indonesia Sadar Sejarah), terutama Sdr Budi Setiyono dan Sdr Bonnie Triyana. Mereka telah « menemukan » dalam tumpukan arsip-arsip yang tersimpan dalam Arsip Nasional Republik Indonesia, kumpulan pidato-pidato Bung Karno pasca-G0S.. Karena mereka berpendapat bahwa bahan-bahan sejarah yang amat penting ini perlu diketahui oleh orang banyak, maka mereka mencari segala jalan untuk bisa menerbitkannya.

Usaha mereka dengan menerbitkan buku ini merupakan sumbangan yang besar sekali untuk menyingkap sebagian dari berbagai aspek yang masih gelap atau remang-remang mengenai peristiwa 1965. Dari isi pidato-pidato Bung Karno yang terdapat dalam kedua jilid buku ini didapat gambaran betapa besar ketokohannya sebagai pemimpin bangsa Indonesia, yang sejak umur 16 tahun sudah menceburkan diri dalam gerakan menentang kolonialisme Belanda. Pidato-pidatonya yang kebanyakan diucapkannya secara spontan ( tanpa teks) dengan lancar, indah, dan sarat dengan kedalaman, membuktikan bahwa ia memang orator yang ulung.

Dan bukan itu saja. Sikap Bung Karno sesudah terjadi G3OS juga membuktikan bahwa ia tetap konsisten atau setia kepada cita-cita dan keyakinan politik yang dianutnya sejak muda, walaupun menghadapi berbagai kesulitan besar dan rumit sekali. Ini tergambar pada kesetiaannya kepada berbagai gagasan-gagasan besarnya, umpamanya (dan antara lain) tentang : sifat dan tujuan revolusi rakyat Indonesia, perjuangan menentang imperialisme, Nasakom, tentang Pancasila dan Ampera, persatuan bangsa, dan soal-soal besar lainnya yang berkaitan dengan ajaran-ajarannya.

## NAsakom, Marxisme Dan PKI

Ketika membaca pidato-pidatonya yang terkumpul dalam « Revolsui belum selesai » mengenai berbagai persoalan yang berkaitan dengan peristiwa G30S maka kelihatan bahwa Bung Karno banyak melihatnya sebagai hasil usaha neo-kolonialism atau imperialisme untuk menghancurkan revolusi rakyat Indonesia dan menyingkirkan Bung Karno. Bahkan, Bung Karno berkali-kali menuding CIA terang-terangan sebagai kekuatan di belakang layar dalam gerakan kontra-revolusioner, yang bekerja-sama dengan kekuatan-kekuatan reaksioner dalamegeri, untuk menjatuhkannya. (Kita sekarang mengetahui bahwa kekuatan reaksioner dalamnegeri pada waktu itu antara lain terdiri dari sebagian dari TNI-AD dan sejumlah kalangan dalam masyarakat yang anti-Bung Karno dan anti-PKI, termasuk kalangan Islam)

Dengan cara yang jelas ia berkali-kali menerangkan apa arti revolusi Indonesia di bawah pimpinannya pada waktu itu, bagi kepentingan perjuangan internasional menentang imperialisme secara global. Dan, walaupun sudah terjadi peristiwa 1965 (ia tidak mau menamakannya Gestapu melainkan Gestok, Gerakan Satu Oktober) ia masih terus-menerus dengan gigihnya mempropagandakan Nasakom. Berkali-kali ia menerangkan apa arti marxisme bagi perjuangan rakyat Indonesia. Bahkan ia memuji PKI sebagai partai atau gerakan yang paling besar sumbangannya kepada bangsa dalam perjuangan melawan kolonialisme Belanda, dibandingkan dengan partai-partai lainnya. Dari berbagai pidatonya ini kelihatan sekali bahwa Bung Karno bukanlah tokoh yang anti-komunis, tetapi sebaliknya, adalah justru penganjur persatuan seluruh bangsa, termasuk golongan komunis.

Dari segi ini pulalah menonjol sekali peran penting - atau sumbangan besar - buku ini bagi pengenalan kita lebih jauh tentang diri pribadi Bung Karno, sebagai pemimpin besar bangsa Indonesia, yang jarang ada tandingannya. Buku ini merupakan pelengkap yang amat penting « Di bawah Bendera Revolusi », kumpulan lainnya dari karya-karya Bung Karno yang bersejarah itu.

## Ampera Dan Pancasila Yang Dipalsu

Dalam berbagai pidatonya itu, Bung Karno telah memblejeti berbagai pemalsuan atau penyelewengan arti Ampera dan Pancasila yang banyak digunakan (atau, lebih tepatnya, disalahgunakan) oleh para pendukung Suharto dkk dalam tahun-tahun 1965, 1966 dan selanjutnya, untuk menghantam politik Bung Karno. Padahal, Ampera dan Pancasila adalah rumus atau gagasan yang telah diciptakan oleh Bung Karno sendiri.

Di antara penjelasan-penjelasan Bung Karno yang penting-penting dalam berbagai pidatonya itu, adalah penegasannya bahwa revolusi rakyat Indonesia adalah kiri, bahwa Pancasila adalah kiri, dan bahwa ia sendiri sejak muda belia sudah melihat pentingnya penggalangan Nasakom. Semua itu dijelaskannya sesudah terjadinya peristiwa G30S. Itu berarti bahwa Bung Karno berusaha melawan usaha-usaha golongan pendukung Suharto (dalam TNI-AD dan kalangan-kalangan tertentu dalam masyarakat) yang sejak terjadinya G30S telah melakukan berbagai kegiatan untuk melemahlan kewibawaan Bung Karno dan menjatuhkannya dari kemimpinan nasional.

Kecuali itu, sesudah terjadinya G30S Bung Karno dengan bahasa yang jelas dan tegas (dan berkali-kali) mengutuk campur-tangan imperialisme AS dalam urusan-urusan dalamnegeri Indonesia. Ia mengungkapkan bahwa kekuatan imperialis memang sudah lama memutuskan untuk menghilangkan Bung Karno dari kepemimpinan revolusi rakyat Indonesia, termasuk dengan cara membunuhnya.

Bolehlah dikatakan bahwa dalam sebagian terbesar dari 103 pidatonya ini Bung Karno telah berbicara tentang revolusi, dan perjuangan anti-imperialisme. Dan patutlah kiranya dicatat bahwa dalam mengemukakan berbagai masalah ini ia memperlihatkan pengetahuannya yang luas tentang sejarah berbagai negeri, pengenalannya tentang falsafah dan politik, dan penguasaannya bahasa asing (Bahasa Indonesia Bung Karno indah sekali. Ia juga menguasai secara baik bahasa Belanda, Inggris, Jerman dan sedikit Perancis). Tidak salahlah kalau kita katakan bahwa Bung Karno adalah orang besar dalam sejarah bangsa Indonesia, bahkan mungkin yang terbesar sampai dewasa ini.

## Arti Penting Buku “Revolusi Belum Selesai”

Buku « Revolusi belum selesai » mengungkap banyak hal mengenai pandangan Bung Karno tentang berbagai soal yang berkaitan dengan situasi pasca-G30S. Oleh karena berbagai masalah G30S masih terus diselidiki dan terus diteliti, atau terus dianalisa, maka penerbitan buku ini mungkin bisa merupakan sumbangan untuk mendalami lebih lanjut berbagai soal yang berkaitan dengan peristiwa itu.

Memang, selama ini, sudah banyak tulisan yang dibuat oleh orang-orang Indonesia sendiri atau oleh orang-orang asing mengenai berbagai soal yang berkaitan dengan G30S. Tetapi, baru kali inilah ada kumpulan teks yang utuh dari pidato-pidato Bung Karno yang diucapkannya sesudah peristiwa yang amat penting itu. Karena itu, buku ini mempunyai nilai penting tersendiri, baik sebagai dokumen sejarah atau sebagai bahan penelitian. Oleh karena itu, patutlah dipikirkan bersama-sama oleh seluruh golongan pendukung politik Bung Karno (dan oleh berbagai kekuatan pro-demokrasi) untuk ikut menyebarluaskan isi berbagai pidatonya itu.


Penyebarluasan isi pidato Bung Karno pasca-G30S pada dewasa ini adalah perlu sekali. Dalam konteks situasi politik menjelang pemilu, dan ketika kekuatan sisa-sisa Orde Baru mulai makin “berani” (dan tanpa malu-malu) menampilkan diri lagi di berbagai panggung politik, maka pidato-pidato Bung Karno merupakan alat ampuh untuk memblejeti kereaksioneran dan pengkhianatan Suharto beserta pendukung-pendukungnya terhadap revolusi rakyat Indonesia.

Penjelasan Bung Karno mengenai Pancasila, Ampera, revolusi rakyat Indonesia, dan perjuangan anti-imperialisme (terutama imperialisme AS) membuka tabir Suharto dan pendukung-pendukungnya yang dengan bermacam-macam dalih telah menggunakan peristiwa G30S untuk merebut kekuasaan dari tangan Bung Karno dan membelokkan revolusi ke arah kanan.

Dalam rangka penulisan kembali sejarah bangsa, perlu sekali (bahkan mutlak) ada penilaian yang tepat atau objektif tentang sifat dan sistem politik Suharto dan pendukung-pendukungnya. Dan dalam pidato-pidato pasca-G30S Bung Karno itulah tergambar betapa kotornya atau jahatnya berbagai pengkhianatan Suharto dan pendukung-pendukungnya dalam mensabot dan menghancurkan kepemimpinan Bung Karno sebagai Presiden, Panglima Tertinggi, Pemimpin Besar Revolusi, dan Mandataris MPRS.

Sekarang ini sudah terbukti, secara gamblang pula, bahwa Suharto beserta para pendukung Orde Barunya bukanlah penyelamat Republik Indonesia, dan bukan pula penyelamat revolusi rakyat Indonesia. Suharto dan para pendukungnya adalah pengkhianat terhadap Bung Karno, dan melalui pengkhianatan terhadap Bung Karno mereka juga melakukan pengkhianatan terhadap revolusi. Suharto dan para pendukungnya adalah perusak sendi-sendi moral bangsa, adalah penghancur jiwa kerakyatan bangsa, adalah pembunuh jiwa asli Pancasila dan Ampera. Pendek kata, Suharto dan para pendukungnya adalah kontra-revolusi dalam sejarah bangsa Indonesia. Hal ini perlu diketahui oleh generasi muda kita dewasa ini, dan semua generasi yang akan datang.

Mengingat itu semua, maka selanjutnya perlu sekali kita semua ikut serta dalam usaha untuk menyebarluaskan isi buku “Revolusi belum selesai “ ini, dengan berbagai cara dan bentuk. Dalam rangka inilah akan diusahakan oleh website http://perso.club-internet.fr/kontak/ ) adanya cuplikan-cuplikan yang penting-penting dari berbagai pidato Bung Karno, yang dimuat dalam buku tersebut. Cuplikan-cuplikan ini dimaksudkan sebagai ajakan kepada para pendukung gagasan besar Bung Karno untuk ikut menyebarluaskan lebih lanjut isi buku yang penting dan bersejarah ini.

Dengan menyebarkan isi pidato Bung Karno, maka kita semua bisa ikut serta dalam memberi sumbangan terhadap perjuangan menghancurkan sisa-sisa Orde Baru, demi tuntasnya reformasi dan tegaknya demokrasi, dan demi terciptanya masyarakat adil dan makmur.

---

Beberapa catatan tentang buku “Revolusi belum selesai” :
Diterbitkan oleh Masyarakat Indonesia Sadar Sejarah.
Sekretariat : d.a. Widya Mitra (samping BNI Undip)
Jalan Imam Bardjo no 3 Semarang
Telp 024-8314030 Fax 024-8446981
E-mail : mesiass 2100@yahoo.com
