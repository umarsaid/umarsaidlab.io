---
layout: post
title: Kualitas DPR sangat menyedihkan
date: 2008-12-11
---

Di bawah ini adalah kutipan dari dua editorial (disingkatkan) dari harian Media Indonesia, yang mengangkat berbagai hal yang “gila” mengenai DPR, yang mestinya (!!!) merupakan lembaga yang paling tinggi dan juga yang paling “terhormat” di Republik kita. Tetapi, apa lacur, nyatanya DPR tidaklah demikian halnya. Akibat berbagai kebrengsekan dalam cara kerjanya yang bersumber pada sikap moral yang rendah serta berbagai sikap politik yang keliru, DPR menjadi sasaran kritik, celaan, cemooh, sindiran, cibiran, ejekan, makian dan umpatan dari banyak kalangan di masyarakat. Apa yang ditulis dalam dua editorial tersebut hanyalah sekelumit kecil saja dari banyaknya kebobrokan atau kebrengsekan yang selama ini menyelubungi DPR. Silakan baca editorial yang berjudul “Mengekang nafsu pemborosan DPR” (29 November 2008) dan “ Popularitas Murahan di DPR (19 November 2008) “, yang disajikan berikut ini :

“RASIONALITAS publik lagi-lagi terganggu oleh ulah DPR. Di tengah masih rendahnya kinerja dewan, lembaga itu justru meningkatkan anggarannya dari Rp1,6 triliun pada 2008 menjadi Rp1,9 triliun pada 2009. Argumen bahwa peningkatan anggaran untuk lebih memacu kinerja anggota dewan sungguh jauh dari nalar normal. Tengok saja Sidang Paripurna Pembukaan Masa Sidang II DPR Tahun Sidang 2008-2009, Senin (24/11) lalu, yang cuma dihadiri 135 anggota dari 550 anggota dewan.
Yang lebih mengganggu publik, dari total anggaran DPR sebesar Rp1,9 triliun pada tahun depan, lebih dari Rp118 miliar dialokasikan untuk kegiatan ke luar negeri. Angka itu dua kali lipat lebih dari jumlah anggaran pada 2007 yang mencapai Rp53 miliar.

“Padahal publik tahu perjalanan ke luar negeri dengan bungkus studi banding itu tidak lebih dari pelesiran belaka. Publik tidak pernah tahu substansi dan manfaat dari studi banding ke luar negeri itu.
Pemborosan anggaran DPR tak cuma sebatas menyangkut studi banding. Renovasi gedung DPR yang menelan biaya Rp33,4 miliar bisa menjadi contoh lain alokasi anggaran yang serampangan.
Ingatan publik juga belum lupa ketika DPR membuat mata anggaran ecek-ecek, seperti tunjangan komunikasi, dana legislasi, dan pengadaan laptop, yang mengundang banyak kecaman.
Itu semua bisa terjadi karena hak yang melekat dalam diri dewan terus bertambah. DPR tak hanya punya kekuasaan di lingkup pengawasan, anggaran, dan perundang-undangan. Tetapi, DPR juga diberi hak mengelola keuangan sendiri.

“Ironisnya, segudang kekuasaan yang dimiliki itu tidak sebanding dengan apa yang diberikan kepada negara dan rakyat. Minimnya kuantitas dan kualitas UU yang dihasilkan DPR serta tiadanya target waktu yang jelas tentang kapan sebuah RUU selesai, bisa menjadi indikator ukuran kinerja dewan.
Karena itu, sikap suka-suka DPR dalam menentukan jumlah dan mata anggaran harus segera dihentikan. Apalagi banyak lubang pemborosan. Namun, pola belanja lembaga negara yang suka-suka, apalagi sekadar pemborosan, haruslah segera dihentikan.

“Politik anggaran lembaga negara bagaimanapun tidak boleh jauh dari ekspektasi publik. Apalagi, di tengah kesulitan negara menghadapi krisis global, anggaran DPR layak dikekang. “ (kutipan pertama selesai)



Sedangkan dalam editorial yang tanggal 19 November 2008 ditulis antara lain sebagai berikut :


“DPR terus mempertontonkan diri sebagai lembaga yang doyan hura-hura. Gedung DPR yang sudah megah itu kembali direnovasi dengan dana miliaran rupiah.

“Renovasi itu sendiri sudah dimulai pekan lalu. Proyek itu mencakup pembenahan ruang kerja, penggantian lantai toilet, pembangunan 10 ruang kerja baru, dan pengadaan furnitur yang menghabiskan dana Rp33,4 miliar. Gagasan renovasi itu sebelumnya bahkan lebih gila. Semula akan dibangun dua menara megah di sisi utara dan selatan dengan jalan penghubung seperti menara Petronas di Malaysia. Dananya diperkirakan sekitar Rp1 triliun. Namun, gagasan pembangunan menara itu urung dilaksanakan.

“Politik bunglon anggota dewan itu sudah berlangsung sejak lama. Setiap muncul persoalan yang menjadi sorotan publik, acap kali pula anggota dewan tampil sebagai pembela rakyat. Bahkan dengan embel-embel ancaman pembuatan hak angket atau pansus, yang kemudian hilang tidak berbekas.
DPR hasil Pemilu 2004 memang menyedihkan. Dan jangan harap dengan kesadaran sendiri mereka akan berhenti hipokrit, berhenti korupsi, berhenti menjadi pahlawan kesiangan. Yang harus menghentikannya adalah rakyat. Yaitu jangan pilih mereka pada Pemilu 2009. (Kutipan selesai. Cetak tebal dari penulis. A. US).



Mutu DPR yang menyedihkan

Ketika membaca kutipan dua editorial tersebut di atas, agaknya kita bisa menarik kesimpulan bahwa negara dan bangsa kita memang sedang menghadapi berbagai penyakit parah, yang sangat akut atau gawat sekali. Apa yang tercermin dalam tulisan itu menunjukkan betapa jeleknya atau betapa rendahnya mutu professional dan mutu moral para anggota DPR kita pada umumnya. Padahal, seperti yang kita ketahui, parlemen adalah badan perwakilan rakyat yang tertinggi di Republik kita. Oleh karena itu, DPR juga seharusnya terdiri dari wakil-wakil rakyat yang betul-betul mau dan bisa sepenuhnya dan setulus-tulusnya mengabdi kepada kepentingan rakyat terbanyak. Namanya saja gagah atau mentereng , “Dewan Perwakilan Rakyat”.

Tetapi, kita sudah melihat bahwa sejak negara dan bangsa kita dikangkangi oleh rejim militer Orde Baru-nya Suharto dkk, maka sebenarnya DPR sudah tidak pantas sama sekali menyandang nama “dewan perwakilan rakyat”. Selama 32 tahun Orde Baru, DPR pada hakekatnya hanyalah merupakan hiasan (dekorasi) untuk memupuri muka buruk rejim militer Suharto supaya kelihatan demokratis, atau supaya kelihatan punya legitimasi seperti kebanyakan pemerintahan lainnya di dunia. DPR waktu itu juga hanyalah sebagai stempel dari golongan jenderal-jenderal dan kolonel-kolonel yang mengangkangi -- degan bermacam-macam cara -- banyak kedudukan kunci di berbagai lembaga negara, termasuk DPR dan partai-partai politik (terutama Golkar).

Kita semua masih ingat bahwa DPR selama Orde Baru (jadi, dalam jangka waktu yang panjang sekali !) bukanlah parlemen seperti yang biasanya bisa kita lihat di berbagai negara di dunia. DPR selama Orde Baru telah dikuasai, atau dikontrol, atau “diborgol” oleh golongan militer lewat kendaraan politiknya yang bernama Golkar. Tidak banyak atau jarang sekali di dunia ada parlemen yang bisa didominasi oleh golongan militer (dalam hal ini : Golkar) dengan kursi antara 60 –70% dan selama lebih dari 30 tahun !!!. DPR “dekorasi” semacam itu adalah hasil dari diselenggarakannya berkali-kali pemilu sandiwara (yang dengan gagah diberi nama “pesta demokrasi”) dengan kemenangan Golkar sekitar 60-70 % suara.

DPR yang sebenarnya tidak mewakili rakyat semacam inilah yang selama puluhan tahun “tutup mulut, tutup kuping dan tutup mata” saja terhadap peristiwa-peristiwa besar seperti pembunuhan massal sampai jutaan orang tidak bersalah apa-apa, pemenjaraan dan penganiayaan ratusan ribu orang kiri dan pendukung Bung Karno, dan berbagai pelanggaran HAM lainnya. Dan DPR semacam ini pulalah yang telah menutup-nutupi atau “diam seribu bahasa” saja terhadap segala kesalahan besar dan dosa-dosa berat Suharto (antara lain : korupsi besar-besaran , kolusi dan nepotisme). Dan selama puluhan tahun pula !



DPR sesudah pemilu 2009 yad

Setelah Suharto dipaksa turun dari jabatannya sebagai presiden, maka banyak orang mengira (dan mengharapkan !!!) bahwa akan ada perubahan besar atau perbaikan di bidang perwakilan rakyat, yang tercermin dalam pemilu dan komposisi di DPR atau DPRD. Memang, ada perubahan di sana-sini, dan bahkan ada yang cukup penting, yang menandakan perbedaan dengan pemilu yang diadakan selama Orde Baru.

Tetapi, alangkah besarnya kekecewaan banyak orang setelah melihat hasil pemilu 1999 (diikuti oleh 48 parpol), atau pemilu 2004 (diikuti oleh 24 parpol). DPR yang sekarang ini, yang menjadi sorotan editorial Media Indonesia tersebut di atas, adalah hasil dari pemilu 2004. Editorial tersebut dengan tegas mengatakan bahwa DPR hasil pemilu 2004 sangat menyedihkan.

Mengingat itu semua, mungkin sekali banyak orang bertanya-tanya bagaimanakah kiranya DPR hasil pemilu 2009 nanti. Dari apa yang sudah terjadi selama ini agaknya sudah dapat diramalkan bahwa pemilu tahun 2009 juga tidak akan mungkin menghasilkan perubahan-perubahan besar dan mendasar di negeri kita. Sebab, pada pokoknya, para peserta utama dalam pemilu tahun 2009 adalah mereka-mereka yang ikut juga dalam pemilu tahun 2004 (sebagian juga ada yang ikut dalam pemilu 1999). Jadi, kecuali adanya perubahan-perubahan kecil dalam program partai-partai yang akan ikut dalam pemilu 2009, kita tidak bisa (dan tidak boleh) punya ilusi atau impian kosong bahwa akan ada perbaikan besar-besaran dalam kehidupan politik, sosial dan ekonomi negeri kita yang serba semrawut seperti sekarang ini.

Yang tidak memungkinkan perubahan drastis atau perbaikan besar-besaran di Republik kita dewasa ini adalah karena kerusakan atau pembusukan yang terlalu parah yang telah dibikin oleh rejim militer Suharto selama 32 tahun, dan yang telah diteruskan sampai sekarang oleh berbagai pemerintahan pasca-Suharto. Kerusakan besar atau pembusukan parah yang melanda negeri kita dewasa ini adalah terutama di bidang moral, sebagai akibat atau produk sistem politik atau ketatanegaraan Orde Baru.

Para pengamat politik dan sosial di Indonesia (termasuk tokoh-tokoh terkemuka berbagai golongan masyarakat) yang bersikap jujur dan objektif, bisa melihat dengan jelas bahwa kerusakan moral ini sudah melanda dengan keganasan yang luar biasa di kalangan “elite” bangsa (baik sipil maupun militer, termasuk kalangan terkemuka dalam partai-partai dan ormas). Kerusakan moral ini sudah menyentuh pimpinan negara, MPR, DPR, Mahkamah Agung, Kejaksaan Agung, TNI, Kepolisian, atau birokrasi negara pada umumnya. Kerusakan moral ini merupakan salah satu sumber segala kejahatan dan pelanggaran, yang antara lain berupa penyalahgunaan kekuasaan, penyelewengan tugas, korupsi, kolusi, dan banyak penyakit lainnya.

Apa yang ditulis dalam editorial Media Indonesia hanyalah sebagian kecil (dan juga terbatas) dari apa yang selama ini sudah dirasakan atau difikirkan oleh banyak kalangan dan golongan dalam masyarakat luas. Dari berbagai bahan yang disajikan media massa (pers dan TV) dapat dilihat bahwa rakyat luas sudah makin kritis, bahkan tidak percaya, kepada DPR yang menunjukkan begitu banyak sikap dan tindakan yang mengkhianati kepentingan rakyat banyak (korupsi sekitar dana dari BI, korupsi sekitar hutan lindung, korupsi pembuatan kapal, korupsi sekitar BLBI dll dll). Ketidakpercayaan masyarakat kepada DPR adalah manifestasi dari ketidakpercayaan terhadap partai-partai politik yang duduk dalam DPR (tidak semuanya, tentu saja).



Merosotnya kepercayaan terhadap DPR

Merosotnya citra DPR, yang disebabkan oleh kerusakan moral (di samping banyaknya kesalahan-kesalahan politik) sangatlah menyedihkan atau memprihatinkan sekali. Sebab DPR merupakan satu bagian yang penting dari wajah muka bangsa dan negara kita Republik Indonesia. DPR juga salah satu lembaga yang ikut menentukan dan mengkontrol baik buruknya penyelenggaraan negara, di samping memikul tugas yang amat tinggi dan mulia, yaitu mewakili dan membela kepentingan rakyat Indonesia yang berjumlah 230 juta orang ini (nomor empat di dunia). DPR adalah salah satu cermin yang menunjukkan jati diri bangsa kita. Jadi, kualitas DPR juga bisa menjadi ukuran kualitas negara dan bangsa. Dan mengenai kualitas DPR ini, tulisan ini ikut menyetujui pendapat yang menyimpulkan : sangat menyedihkan !!!

Selama lebih dari 40 tahun (32 tahun selama Orde Baru ditambah 10 tahun pasca-Suharto, sampai sekarang) DPR sudah memperlihatkan kualitasnya (atau mutunya) yang sebenarnya, Dan kita bisa memperkirakan bahwa DPR yang akan datang (sebagai hasil pemilu 2009) tidak mungkin berbeda jauh dari yang sekarang atau yang masa-masa lalu. Selama Golkar dan golongan-golongan yang pro Suharto dan anti-Bung Karno masih menduduki pos-pos kunci dalam berbagai lembaga negara, tidaklah mungkin sama sekali adanya perubahan besar dalam DPR. Ini berarti bahwa negara dan bangsa akan tetap terus dikangkangi oleh Golkar beserta sekutu-sekutunya. Dan alangkah menyedihkannya bahwa Golkar (beserta sebagian gologan militer dan pendukung-pendukung Orde Baru lainnya) yang merupakan perusak Pancasila (yang asli dan murni) akan tetap bisa meneruskan pengkhianatannya terhadap perjuangan bangsa menuju masyarakat adil dan makmur seperti yang dicita-citakan oleh Bung Karno bersama rakyat banyak.

Mengingat itu semuanya, perlulah kiranya bagi seluruh kekuatan demokratis di Indonesia, untuk menggiatkan atau mengobarkan berbagai aksi dan kegiatan – melalui macam-macam cara, saluran, dan bentuk) untuk tetap menelanjangi kebusukan dan kebobrokan di DPR. Sebab perjuangan extra-parlementer melawan ketidakberesan dalam DPR berarti sekaligus juga berjuang terhadap berbagai macam praktek kotor dan tidak bermoral di kalangan partai-partai politik (antara lain : komersialisasi jabatan, pengumpulan dana haram untuk caleg). Berjuang terhadap sikap tidak bermoral di kalangan anggota DPR berarti juga membersihkan lembaga tertinggi rakyat ini dari segala borok dan penyakit yang sudah menjangkiti bangsa sejak lama dan pada taraf yang akut sekali pula.



DPR perlu dikontrol ketat oleh rakyat

DPR kita sekarang mempunyai kekuasaan yang besar, luas dan kuat. Karena itu, kecenderungan untuk menyalahgunakan kekuasaan juga besar sekali Di samping tugas utamanya membuat undang-undang DPR juga mempunyai fungsi untuk mengontrol jalannya pemerintahan. Dalam melaksanakan tugas dan fungsinya ini bisa saja terjadi banyak kesalahan, penyelewengan dan pelanggaran, baik secara kolektif atau perorangan sebagai anggota.

Itulah sebabnya, DPR yang begitu besar kekuasaannya itu perlu juga dikontrol. Karena namanya saja “dewan perwakilan rakyat”, maka sudah semestinya atau sudah seharusnya bahwa rakyat berhak juga mengkontrol DPR. Kontrol rakyat terhadap DPR adalah mutlak perlu, dan merupakan kewajiban yang sah dan luhur bagi masyarakat.

Kontrol terhadap DPR dapat dilakukan oleh berbagai gerakan extra-parlementer, ornop dan LSM, organisasi buruh, tani, pegawai negeri, pemuda, mahasiswa, perempuan, dengan mengadakan aksi-aksi bersama atau sendiri-sendiri. Berbagai macam aksi dan kegiatan terus-menerus untuk mengkontrol DPR bisa menjadi pendidikan politik dan bisa diharapkan menjadi jalan untuk meninggikan kesadaran politik berbagai golongan masyarakat. Meningkatnya kesadaran politik masyarakat ini akhirnya bisa memungkinkan terpilihnya partai politik yang pro-rakyat. Dan besarnya dukungan terhadap partai-partai politik yang pro-rakyat adalah syarat penting untuk kemungkinan terselenggaranya pemilu yang menghasilkan DPR yang pro-rakyat. Yaitu DPR yang benar-benar pantas dan juga berhak disebut sebagai “dewan perwakilan rakyat”, dan bukannya lagi sebagai “dewan penipu rakyat” seperti yang sering terdengar selama ini.
