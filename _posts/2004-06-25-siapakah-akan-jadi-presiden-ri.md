---
layout: post
title: Siapakah akan jadi presiden RI
date: 2004-06-25
---

Meskipun hiruk-pikuk kampanye pemilihan presiden masih terus berlangsung, kita mendapat kesan bahwa banyak orang mulai jenuh, bahkan muak, dengan kebanyakan isi dan cara kampanye yang diselenggarakan oleh para pendukung Mega-Hasyim, Wiranto-Salahudin Wahid, SBY-Jusuf Kalla, Amien Rais-Siswono, dan Hamzah Haz-Agum. Karena, seperti yang bisa kita amati bersama, dalam banyak kampanye yang diadakan di berbagai daerah di Indonesia itu para capres dan wacapres telah mengumbar banyak janji yang muluk-muluk atau tidak realis, dan penuh kebohongan, serta memberikan gambaran palsu tentang masa depan negara dan bangsa, kalau mereka nanti terpilih jadi presiden.

Padahal, kalau direnungkan dalam-dalam, problem-problem gawat dan penyakit parah yang dihadapi negara dan bangsa kita dewasa ini adalah sedemikian banyaknya dan sedemikian rumitnya, sehingga siapapun yang akan terpilih menjadi presiden dan wakil presiden, akan menghadapi banyak sekali kesulitan. Kerusakan negara dan kebobrokan bangsa sudah begitu parahnya, sehingga kita sama sekali tidak boleh atau tidak bisa mengharapkan akan adanya perbaikan besar-besaran dalam waktu 5 tahun yang akan datang. Karenanya, banyak orang memandang segala ucapan para capres dan wacapres selama kampanye itu dengan kritis sekali, atau dengan kecurigaan atas kejujuran mereka, atau bahkan dengan kejengkelan dan kemarahan.

## Kekosongan Kepemimpinan Nasional

Karena, boleh dikatakan bahwa pembusukan yang diakibatkan pemerintahan Orde Baru selama 32 tahun sudah merajalela secara parah dan ganas di semua bidang kehidupan bangsa, sehingga banyak problem-problem rumit sulit diatasi dewasa ini. Ketika kita menghadapi banyak kesulitan besar dan masalah-masalah pelik ini terasa sekali bahwa bangsa kita tidak memiliki kepemimpinan nasional yang cukup berwibawa, yang dihormati oleh sebagian terbesar rakyat, yang jujur dan bersih, yang bisa merupakan pemersatu bangsa, yang mempunyai dedikasi yang jelas kepada kepentingan rakyat, yang hidupnya dibaktikan kepada nusa dan bangsa. Kita pernah mempunyai pemimpin bangsa yang memiliki sifat-sifat demikian, yaitu Bung Karno. Tetapi Bung Karno telah dikhianati dan dihancurkan oleh Suharto beserta para pendukungnya, yang bersekongkol dengan kekuatan kubu AS dalam rangka perang dingin waktu itu. Sejak naiknya Suharto di panggung kekuasaan, segala cita-cita baik yang diperjuangkan para perintis kemerdekaan dicampakkan atau dirusak oleh Orde Baru.

Dewasa ini terasa sekali bahwa persatuan dan kesatuan negara Republik Indonesia mengalami gangguan, akibat salah urus rezim militer. Ini bisa kita saksikan di Aceh, Poso (Sulawesi), Maluku, Irian Barat (Papua). Masalah-masalah pelik dan rumit juga timbul di sebagian kalangan Islam, dengan munculnya aliran fondamentalis dan grup-grup yang mendukung terorisme. Otonomi daerah telah digunakan oleh orang-orang yang bermental busuk untuk melakukan korupsi besar-besaran dan menyebarkan racun kesukuan atau perasaan kedaerahan yang negatif.

Kacau-balaunya banyak soal-soal penting dalam urusan negara disebabkan kerusakan mental atau kebejatan akhlak kalangan elite, yang menduduki jabatan-jabatan penting di bidang eksekutif, legislatif dan judikatif. Karena itulah rule of law tidak berjalan seperti semestinya, dan dalam banyak kasus sudah berbalik menjadi “law of the rulers”. Hukum bisa dilumpuhkkan oleh uang haram. Hakim dan jaksa banyak yang bisa “dibeli”, dan sebagian aparat kepolisian, imigrasi dan bea-cukai mudah disuap.

## Kebejatan Akhlak Dan Utang Yang Menumpuk

Kebejatan akhlak yang sudah melanda secara parah di kalangan para elite ini tidak akan mudah diperbaiki oleh presiden yang akan terpilih nantinya. Sebab, kebejatan akhlak inilah yang sudah melahirkan kebudayaan korupsi di kalangan pejabat dan membikin busuk banyak tokoh politik, tokoh agama atau berbagai tokoh masyarakat lainnya. Pembusukan di begitu banyak bidang ini membikin banyak orang menjadi pesimis dan prihatin akan hari depan bangsa dan negara. Banyak orang sudah putus asa, atau menyerah saja kepada takdir. Ada pula sebagian orang yang sudah tidak bisa melihat adanya kemungkinan untuk memperbaiki keadaan parah ini kecuali dengan mengadakan revolusi !

Sebab, problem-problem besar yang sedang dihadapi bangsa dan negara kita adalah betul-betul parah dan gawat. Contohnya : Utang luar negeri RI sampai bulan Maret 2004 tercatat 136 milyar dollar AS (Jakarta Post, 7 Juni 2004). Jelaslah bagi semua orang, bahwa utang yang sebesar itu adalah beban yang tidak ringan bagi rakyat, termasuk bagi anak cucu kita. Utang sebesar 136 miliar dollar AS (artinya 136 000 juta atau 136 000 000 000 dollar) itu kalau dihitung dalam Rupiah merupakan angka-angka yang sungguh mengerikan. (Kalau satu dollar AS = Rp 9.000, maka utang luarnegeri kita adalah Rp 9000 x 136 000 000 000= Rp 1224 000 000 000 000. Satu jumlah yang besarnya sulit bisa kita bayangkan !). Yang perlu diperhatikan lagi dalam hal ini adalah kenyataan bahwa sebagian besar dari utang itu timbul karena berbagai salah-urus dan penyelewengan para tokoh (pemerintah dan swasta) yang bermental busuk.

Yang sangat menyedihkan lagi yalah walaupun utang kita sudah begitu besar, banyak tokoh-tokoh kita yang tidak segan-segan terus mencuri kekayaan negara dan rakyat. Menurut Kejaksaan Agung, antara Januari 2002 dan April 2004 telah diperiksa 1 194 kasus korupsi yang menyebabkan negara dirugikan sebesar Rp 22 trilyun (atau 2, 35 miliar dollar AS) selama dua tahun itu. (Jakarta Post 18 Juni 2004). Kalau kita amati bahwa korupsi ini dilakukan oleh orang-orang yang umumnya sudah bisa hidup enak atau mewah, maka akan kelihatan betapa sudah parahnya kebejatan akhlak banyak elite kita ini. Mereka korupsi bukan karena kebutuhan dasar yang mendesak, melainkan untuk menumpuk kekayaan atau hidup secara mewah dengan jalan haram.

## Beginikah Mutu “Wakil Rakyat” Kita

Bisa kita duga bahwa apa yang telah bisa diperiksa atau ditangani oleh Kejaksaan Agung itu baru sebagian kecil dari ribuan (bahkan puluhan ribu) kasus korupsi lainnya yang terjadi di seluruh Indonesia, baik di tingkat propinsi, kabupaten, kota, maupun kecamatan. Di antara korupsi yang mencerminkan kebobrokan akhlak ini adalah yang dilakukan oleh apa yang dinamakan “wakil rakyat’ yang tergabung dalam DPR atau berbagai DPRD propinsi, kabupaten dan kota. Kejaksaan Jawa Tengah di Semarang telah menyelidiki penyelewengan anggaran oleh sejumlah anggota DPRD propinsi. Demikian juga di kota Bandung, sejumlah angota DPRD digugat oleh sebuah Ornop (Bandung Institute of Governance Studies) atas terjadinya ketidakberesan dalam anggaran tahun 2001 dan 2002. Hal yang sama juga terjadi di Kalimantan Timur di mana uang sebesar Rp 5,4 miliar dalam anggaran pemerintahan propinsi menjadi persoalan.

Menurut Jakarta Post 18 Juni 2004, Kejaksaan Agung telah menindak kasus korupsi yang dilakukan oleh para “wakil rakyat” dari kalangan DPRD

1. Kabupaten Padang
2. Propinsi Sumatra Barat
3. Kabupaten Payakumbuh
4. Kota Cirebon Cirebon
5. Kabupaten Garut
6. Kabupaten Ciamis
7. Kota Bandarlampung
8. Propinsi Sumatera Selatan (Palembang)
9. Kabupaten Singkawang
10. Kabupaten Pontianak
11. Kabupaten Banda Aceh
12. Propinsi Jawa Tengah
13. Propinsi Kaltim
14. Kabupaten Kutai Timur

## Praktek-Praktek Buruk Akan Berlangsung Terus

Kebejatan moral atau kerusakan akhlak di kalangan politisi yang tergabung dalam berbagai partai politik dan menduduki DPR (dan banyak DPRD) ini juga tercermin dari cara kerja dan cara hidup mereka. Sudah menjadi rahasia umum bahwa banyak sekali dari wakil rakyat ini yang sebelum menjadi anggota badan legislatif (di Jakata atau di daerah) hidupnya pas-pasan saja, tetapi menjadi kaya raya dalam tempo sebentar setelah diangkat menjadi “wakil rakyat”. Pembusukan yang terjadi di kalangan legislatif adalah ukuran betapa buruknya mutu kehidupan politik dan moral bangsa kita. (Ingat, antara lain : hiruk pikuk soal kapal tanker Pertamina, yang menyangkut kebersihan puluhan anggota parlemen).

Pemilu 5 tahun yang lalu telah melahirkan badan legislatif pusat dan daerah yang kwalitasnya sudah bisa sama-sama kita saksikan selama ini. Dan amat menyedihkan bahwa dari pemilu lesgislatif yang baru saja diadakan tahun ini juga tidak banyak harapan akan terjadinya perbaikan atau perobahan yang mendasar, terutama yang berkaitan dengan moral atau akhlak. Sudah bisa diperkirakan bahwa DPR (dan DPRD berbagai tingkat) yang akan datang akan terdiri dari banyak wakil-wakil partai politik yang akan meneruskan praktek-praktek buruk di masa lalu. Dengan komposisi yang mencerminkan hasil pemungutan suara dalam pemilu legislatif tahun 2004 itu, DPR (dan DPRD) akan tetap menjadi tempat untuk dagang sapi, dagang projek, dagang anggaran, pembagian kekuasaan, pembagian uang haram.

## Problem-Problem Besar Yang Kita Hadapi

Padahal, terlalu banyak pekerjaan-pekerjaan besar dan soal-soal serius yang harus diselesaikan bersama-sama. Contohnya, menurut suratkabar Straits Times 8 Juni 2004, Indonesia sudah mernjadi eksportir terbesar palacur muda (termasuk anak-anak) di dunia. Sekitar 70.000 anak perempuan telah diperdagangkan ke luarnegeri sebagai pelacur di Malaysia, Thailand, Jepang, Hongkong, Singapura dan Australia. Dari pelacur yang jumlahnya ditaksir 400.000 di seluruh Indonesia, setengahnya adalah anak-anak perempuan di bawah umur 18 tahun. Di samping itu ditaksir ada 3 juta sampai 6 juta anak yang terlantar dan tidak mendapat perawatan orang tua.

Contoh lainnya, ialah soal pengangguran yang sangat tinggi di Indonesia. Dewasa ini ditaksir ada 42 juta orang yang menganggur, artinya tidak punya pekerjaan dan penghasilan yang tetap. Pengangguran ini akan terus-menerus menjadi sumber frustrasi bagi banyak orang, karena tiap tahun ada tambahan penganggur sebanyak 2,5 juta orang. Pengangguran terbuka (artinya orang-orang yang sama sekali tidak punya pekerjaan) sudah mencapai lebih dari 10 juta orang, sedangkan lebih dari 31 juta orang dalam keadaan setengah menganggur.

Menurut angka-angka Bank Dunia (World Bank) separoh dari penduduk Indonesia hidup di bawah 2 dollar AS seharinya. Penduduk Indonesia termasuk yang termiskin di kawasan Asia Tenggara. Angka kematian bayi di Indonesia dua kali lipat dari pada di Filipina dan 5 kali lipat dari pada Vietnam (Radio Australia 2 Juni 2004).

Dari sebagian kecil persoalan-persoalan yang dicantumkan di atas itu saja sudah tergambar betapa besar masalah-masalah serius yang dihadapi bangsa kita. Belum lagi soal-soal lainnya, umpamanya bidang pendidikan yang makin sulit bagi generasi muda kita, bidang kesehatan untuk rakyat kecil. Ditambah lagi perlunya pemberesan di kalangan militer dan kepolisian, yang betul-betul sangat membutuhkan perombakan mental. Di bidang perekonomian terasa perlunya ada dobrakan-dobrakan baru sehingga para pengusaha bisa memainkan peran mereka yang positif. Kesejahteraan buruh dan tani juga perlu ditingkatkan.

## Mana Yang Emas Dan Mana Yang Loyang ?

Apakah dari capres dan cawapres yang akan “bertarung” dalam pemilihan itu nantinya ada yang bisa diharapkan bisa menangani persoalan-persoalan besar dan rumit itu semuanya ? Kita semua bisa mempunyai penilaian yang berbeda-beda, tentang kemampuan dan mutu mereka masing-masing. Tetapi, dari sepak-terjang atau sejarah hidup mereka selama ini, sedikit banyaknya kita sudah melihat mana saja yang emas dan mana yang loyang. Dari sikap mereka selama ini sudah bisa ukur apakah mereka betul-betul membela HAM, dan apakah mereka sungguh-sungguh memperjuangkan reformasi dan membela demokrasi.

Siapa pun yang akan menjadi presiden (dan wakil presiden) nantinya, satu hal sudah jelas, yaitu bahwa seluruh kekuatan demokratis di Indonesia akan tetap mempunyai tugas besar (bahkan makin penting !) dalam menggalang dan mengkonsolidasi barisannya. Sebab, siapa pun jadi presiden berbagai persoalan dan kesulitan parah akan tetap timbul, termasuk penyelewengan dan pembusukan. Kerusakan dan pembusukan yang sudah ditimbulkan oleh rezim militer Orde Baru (dan diteruskan oleh pemerintahan-pemerintahan berikutnya) sudah sedemikian parahnya dan sedemikian luasnya, sehingga presiden yang mana pun tidak akan bisa memperbaikinya dalam waktu yang singkat. Apalagi, kalau presidennya adalah orang yang moralnya tidak luhur!

Dan dalam keadaan di mana bidang eksekutif, legislatif dan judikatif sudah tidak berdaya lagi untuk melawan pembusukan, maka tugas mulia ini jatuh di pundak seluruh kekuatan demokratis rakyat dalam masyarakat. Urusan negara dan bangsa tidak bisa (dan tidak boleh !!!) diserahkan begitu saja mentah-mentah kepada para pejabat dan “wakil rakyat”, apalagi kalau sudah terbukti bahwa mereka tidak jujur dalam pengabdian mereka kepada kepentingan rakyat. Rakyat berhak dan bahkan berkewajiban mengontrol mereka, dengan berbagai cara dan berbagai jalan.
