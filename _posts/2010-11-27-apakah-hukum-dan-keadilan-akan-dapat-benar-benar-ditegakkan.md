---
layout: post
title: Apakah hukum dan keadilan akan dapat benar-benar ditegakkan ?
date: 2010-11-27
---

Sesudah berbulan-bulan mengikuti hiruk pikuk kasus besar Gayus Tambunan yang menghebohkan seluruh negeri, dan kemudian baru-baru ini mendengar tentang dipilihnya Busyro Muqoddas oleh Komisi 3 DPR sebagai Ketua KPK dan ditunjuknya Basrief Arief oleh presiden SBY sebagai Jaksa Agung baru, maka wajarlah bahwa di antara banyak kalangan timbul harapan bahwa akan ada kemajuan-kemajuan atau perbaikan dalam penegakan hukum dan keadilan di negeri kita.

Walaupun itulah yang menjadi impian banyak orang, namun apakah   -- dalam prakteknya  --  keadilan dan hukum akan masih bisa benar-benar ditegakkan di Republik kita,  apalagi dalam masa dekat yang akan datang ? Melalui tulisan kali ini dicoba untuk mengajak para pembaca merenungkannya dari berbagai sudut pandang dan segi, yang sebagian kecilnya adalah antara lain sebagai berikut :

Seperti yang bisa sama-sama kita saksikan setiap hari di sekitar kita masing-masing, ketidak-adilan adalah wajah sebenarnya dari Republik Indonesia, dan terutama sekali ( !!!)  sejak dimunculkannya Orde Baru oleh rejim militer Suharto. Ketidak-adilan adalah ciri utama dari negara kita. Ketidak-adilan adalah penyakit yang amat parah yang sudah dirundung rakyat kita sejak lama. Namun, ketidak-adilan ini lebih-lebih makin menonjol akhir-akhir ini di bawah pemerintahan SBY.

« Indonesia akan karam » akibat merajalelanya ketidak-adilan

Kalau kita telaah dalam-dalam, maka nyatalah bahwa merajalelanya berbagai macam ketidak-adilan di negara kita, ada hubungannya dengan membusuknya moral secara luas dan besar-besaran yang mengakibatkan lemahnya penegakan hukum, atau  diinjak-injaknya hukum oleh berbagai kalangan dan golongan, atau dilanggarnya serta dilecehkannya hukum oleh  berbagai ragam penjahat (pejabat) dari yang paling atas sampai ke yang paling bawah, serta bejatnya  akhlak golongan  elite di kalangan pengusaha besar.

Kebejatan akhlak besar-besaran ini tidak hanya kelihatan dalam kasus Gayus Tambunan, melainkan juga dalam kasus-kasus Bank Century, kasus Krakatau Steel, kasus BLBI, dan kasus-kasus  korupsi sejumlah gubernur dan DPRD di banyak daerah di Indonesia. Semuanya itu makin menambah banyaknya ketidak-adilan yang memang sudah merajalela sebelumnya.

Begitu parahnya ketidak-adilan di negara kita sehingga Ketua Mahkamah Konstitusi, Mahfud MD, di depan ribuan jamaah shalat Idul Adha di Makasar, mengingatkan bahwa ancaman dan bencana terbesar yang dihadapi bangsa Indonesia adalah ketidak-adilan.

“Indonesia akan karam, bukan karena bencana. Indonesia akan karam, karena bencana yang lebih dahsyat. Bencana yang lebih dahsyat, bukan bencana alam. Tetapi bencana ketidak-adilan. Bencana ketidak-adilan itulah yang akan mengakibatkan Indonesia karam", ujar Mahfud. (Media Indonesia, 18/11/2010)

“Maraknya jual beli hukum adalah bencana ketidak-adilan. Negara yang tidak dapat menegakkan hukum akan hancur di manapun dan di masa apapun”, ujar Mahfud. Selanjutnya, Mahfud mengingatkan bahwa Indonesia sedang dalam masalah besar dan terancam karam, bukan karena perbedaan antar umat beragama, melainkan karena hukum dan keadilan yang tidak ditegakkan ».

Prof Sahetapy : « Seperti perahu yang bocor »

Peringatan Mahfud MD yang begitu keras dan tajam adalah indikasi yang jelas bahwa kebobrokan dunia hukum di Indonesia sudah kelewatan parahnya, sehingga menurutnya negara terancam hancur atau karam.

Peringatan yang seirama atau senada juga diberikan oleh tokoh terkemuka lainnya, Prof DR J.E. Sahetapy, yang menekankan  bahwa situasi yang semrawaut di Indonesia sekarang ini adalah tidak ditegakkannya hukum, dan karenanya Indonesia ibarat sebuah perahu yang bocor.

Mungkin, ada saja orang yang menganggap bahwa ucapan Ketua Mahkamah Konstitusi itu agak berlebih-lebihan, namun kiranya kita semua patut mengakui bahwa apa yang ditunjukkan dewasa ini oleh kasus Gayus Tambunan, dan oleh berbagai kejahatan atau ketidakjujuran di kalangan tinggi kepolisian, kejaksaan, dan peradilan, memang membenarkan inti atau jiwa peringatan yang begitu tajam itu.

Karena itu, setelah ada Ketua KPK yang baru, Jaksa Agung  yang baru dan juga kepala Polri yang baru, kita semua sedang menunggu-nunggu apakah trio aparat penegakan hukum kita itu akan bisa  mencegah karamnya atau hancurnya Republik Indonesia karena merajalelanya ketidak-adilan dan berbagai ragam pelanggaran hukum, atau rusaknya penegakan hukum.

Aparat-aparat negara yang kehilangan kepercayaan rakyat

Kita bisa saja, atau boleh-boleh saja, untuk mempunyai harapan bahwa Jaksa Agung yang baru, Basrief Arief, akan bisa mengadakan pembersihan besar-besaran dan tuntas di lembaga Kejaksaan yang selama ini sudah terlalu banyak dirusak secara parah oleh para jaksa yang berakhlak buruk atau bermoral rendah sekali. Namun, kita dapat mengerti bahwa kerusakan moral di kalangan kejaksaan ini sudah demikian besarnya dan juga begitu hebatnya, sehingga tidak lama lagi akan ketahuan apakah Basrief Arief akan bisa mengadakan perbaikan-perbaikan yang sudah ditunggu-tunggu oleh banyak orang.

Demikian juga di  bidang kepolisian. Tugas Kapolri Timur Pradopo untuk juga mengadakan perbaikan dan pembersihan dari unsur-unsur yang kotor, busuk, atau rusak juga amat besar dan berat. Sebab, dari kira-kira 240 jenderal polisi yang ada sekarang ini, berapakah kiranya yang masih bisa dikategorikan betul-betul bersih ? Perlu sama-sama kita ingat, bahwa kasus Kompol Iwan Siswanto ( kepala Rutan Brimob),   bersama 8 polisi lainnya  telah terima uang suapan Rp 368 juta dari Gayus untuk bisa keluar masuk rutan sebanyak 68 kali, adalah hanya satu bagian kecil sekali dari borok-borok yang selama ini membikin sakit tubuh kepolisian RI.

Berbagai pengamatan dan survey yang pernah diadakan menunjukkan bahwa rakyat sudah tidak mempercayai lagi aparat kepolisian, kejaksaan dan  pengadilan. Karena busuknya tiga aparat-aparat negara itu, yang masing-masing mendapat penilaian yang negatif atau minus dari masyarakat luas, maka harapan masyarakat luas tinggallah pada KPK. KPK masih mendapat kepercayaan dari banyak orang, sebagai lembaga yang independen (dan relatif bersih)  untuk menangani pemberantasan korupsi, yang sudah dirusak oleh berbagai kalangan di kepolisian, kejaksaan dan pengadilan.

Halangan yang tidak sedikit dan « godaan » yang besar

Sekarang, dengan dipilihnya oleh DPR Busyro Muqoddah sebagai Ketua KPK yang baru (untuk menggantikan Antasari Ashar), maka KPK mendapat darah baru untuk melanjutkan tugas-tugas untuk menyelesaikan kasus-kasus yang besar, yang masih banyak dan belum terselesaikan oleh aparat-aparat hukum. Di antara kasus-kasus besar itu terdapat kasus Bank Century, kasus Gayus, kasus Krakatau Steel, dan berbagai urusan lainnya dengan perusahaan besar asing.

Mengingat besarnya berbagai kepentingan (antara lain : politik dan kekuasaan, dana-dana yang amat besar yang berkaitan dengan kasus-kasus ini, dan usaha-usaha berbagai fihak untuk menumpuk harta dll), maka baik KPK maupun aparat-aparat kepolisian dan kejaksaan dan pengadilan akan menghadapi berbagai ragam halangan (dan « godaan » !!!) yang tidak sedikit dan juga tidak kecil.

Sebagai contohnya adalah kasus Gayus. Pegawai kategori rendahan (tingkat 3A) di lembaga perpajakan ini, sudah bisa mengantongi dana haram sebanyak Rp 100 miliar, karena « menangani » (secara gelap) urusan pajak sekitar 150 perusahaan besar. Di antara perusahaan sebanyak itu terdapat sejumlah besar perusahaan asing, dan juga 3 perusahaan dari Grup Aburizal Bakrie.

Jadi, di belakang kasus Gayus ini tersembunyi urusan perusahaan-perusahaan  besar yang mempunyai hubungan kepentingan dengan para penguasa (atau pembesar)  tingkat tinggi atau penting negeri kita (di bidang eksekutif,  legislatif, dan judikatif) dan juga kepentingan asing.

« Kultur pelanggaran hukum » yang membudaya

Karena itu, walaupun kasus Gayus sudah mulai ditindak, namun kita bisa sama-sama melihat bahwa hanya perkara-perkara kecil saja yang sudah diperiksa, sedangkan persoalan-persoalan besarnya masih selalu diusahakan untuk dihalang-halangi dibongkar. Umpamanya, dari mana datangnya uang sebanyak Rp 100 milyar yang ada dalam rekening Gayus ? Dan siapa yang mengatur kepergiannya ke Bali dan bertemu dengan siapa saja ? Dan, sebenarnya, siapa-siapa saja yang selama ini bekerja-sama dengannya ?

Kita bisa memperkirakan bahwa pemeriksaan terhadap kasus Gayus, kalau saja dilakukan oleh kejaksaan,  kepolisian, pengadilan dan KPK betul-betul dengan semangat menegakkan hukum, kebenaran dan keadilan, maka akan bisa membongkar sebanyak-banyaknya dan sejelas-jelasnya segala kebusukan dan kebobrokan di negara kita.

Sudah terlalu banyak  -- dan juga sudah terlalu lama !!! -- kejahatan atau pelanggaran hukum telah dilakukan oleh berbagai kalangan negeri kita, yang menimbulkan ketidak-adilan bagi masyarakat luas, yang, karenanya, juga menyebabkan kesengsaraan dan penderitaan banyak orang. Dan
kultur pelanggaran hukum » ini sudah menjadi « kebudayaan » banyak orang, sehingga tidak mudah  diberantas.

Karena itu, walaupun sudah dipilih Ketua KPK yang baru, dan ditunjuk jaksa agung dan Kapolri yang baru, itu semua bukanlah jaminan bahwa penegakan hukum akan  terlaksana dengan cepat dan mulus jalannya. Kerusakan moral sudah terlalu parah, godaan juga terlalu besar, kemunafikan juga merajalela, sehingga banyak orang (terutama di kalangan elite) sudah tidak malu-malu lagi atau tidak segan-segan untuk korupsi, atau menyalahgunakan kekuasaan dan pengaruh untuk memperoleh harta haram.

Penegakan hukum adalah urusan kita semua

Jadi, patutlah kiranya sama-sama kita sadari, terutama oleh  para pembesar dan tokoh-tokoh di berbagai kalangan (termasuk kalangan pengusaha besar) bahwa keserakahan untuk memburu harta haram melalui segala macam korupsi dan beragam kejahatan lainnya adalah jalan atau cara-cara yang sesat dan khianat, yang bertentangan dengan segala ajaran agama  Adalah sia-sia saja, atau percuma saja, kalau ada koruptor (atau penjahat-penjahat lainnya) yang mengira bahwa dosa mereka terhadap rakyat sudah terhapus dengan sembahyang atau menghafal ayat-ayat kitab suci.

Mengingat bahwa kerusakan iman ini sudah menjalar di banyak kalangan penting negara kita, maka adalah tugas yang mulia bagi seluruh kekuatan demokratis, segala kalangan patriotik, semua golongan yang mau sungguh-sungguh membela kepentingan rakyat Indonesia, untuk mengawasi jalannya usaha penegakan hukum demi  perbaikan pemerintahan dan perbaikan kehidupan bernegara bangsa kita.

Penegakan hukum dan perjuangan melawan segala macam ketidak-adilan adalah urusan  kita semua, urusan rakyat banyak, dan bukannya hanya urusan presiden beserta pembantu-pembantunya, dan bukan pula hanya urusan DPR atau partai-partai politik. Dan, karena perjuangan melawan mafia hukum, mafia pengadilan, dan mafia korupsi ini akan makan waktu yang lama, dan juga berbelit-belit, maka diperlukan adanya kekuatan yang besar sekali untuk bisa melaksanakannya. Dan kekuatan besar ini ada di tangan rakyat banyak.

Karena itu, rakyat banyak perlu ikut serta dalam perjuangan  besar ini, melalui macam-macam kegiatan serta dengan beraneka-ragam jalan dan cara serta bentuk. Perjuangan untuk mendorong, mengawasi, mengawali, dan memperkuat usaha penegakan hukum guna melawan ketidak-adilan ini bisa diteruskan dan dikembangkan lewat segala macam organisasi masyarakat dan LSM, seperti (antara lain) Indonesian Corruption Watch, Indonesian Police Watch, Parliament Watch, Koalisi Anti-korupsi, gabungan Ornop, dan LBH-LBH  di seluruh Indonesia.
Gerakan extra-parlementer untuk meneruskan revolusi rakyat

Ketika pemerintah sudah memperlihatkan berbagai kelemahannya atau ketika DPR sudah memamerkan kemandulannya, serta ketika partai-partai politik juga sudah menunjukkan pengkhianatannya terhadap kepentingan rakyat banyak, maka berbagai bentuk perjuangan rakyat lewat gerakan extra-parlementer yang kuat dan luas adalah jalan yang perlu ditempuh. Hanya dengan pengawasan dan pengawalan gerakan extra-parlementer yang besarlah penegakan hukum dapat dilaksanakan dan ketidak-adilan dapat dihapuskan atau dikurangi.

Hanyalah gerakan extra-parlementer yang sangat besar, yang meluas dan yang kuat bisa mencegah « karamnya » Republik Indonesia ( untuk meminjam peringatan pimpinan Mahkamah Konstitusi, Mahfud MD) . Juga hanyalah  dengan  gerakan extra-parlementer yang bisa dibangun bersama secara besar-besaran  oleh berbagai macam kekuatan dalam masyarakat maka perubahan besar dan fundamental bisa dilaksanakan, untuk mencegah timbulnya Gayus-Gayus baru, atau terulangnya kembali kasus-kasus semacam Bank Century.

Sejarah akan membuktikan bahwa gerakan extra-parlementer yang luas adalah juga fondamen yang kokoh atau dasar yang kuat untuk meneruskan revolusi rakyat, yang sudah sering ditunjukkan Bung Karno, untuk membangun  masyarakat adil dan makmur (atau masyarakat sosialis a la Indonesia) dengan berpedoman Pancasila dan di bawah pangayoman Bhinneka Tunggal Ika.
