---
layout: post
title: Jasa-jasa besar Bung Karno tak terlupakan
date: 2010-06-14
---

Ajaran-ajaran revolusionernya  patut dihayati oleh kita semua



Memperingati wafatnya Bung Karno  40 tahun yang lalu, yaitu pada tanggal 21 Juni 1970,  adalah hal yang penting  bagi kalangan atau golongan yang ingin meneruskan perjuangan besarnya demi revolusi rakyat Indonesia dan demi cita-cita bersama untuk menciptakan masyarakat adil dan makmur.

Wafatnya Bung Karno merupakan kehilangan yang besar sekali bagi bangsa Indonesia, terutama bagi yang mencintainya, menghormatinya, mengaguminya, sebagai bapak bangsa, dan sebagai pemersatu bangsa yang paling agung sepanjang sejarah Indonesia..

Agaknya bagi sebagian terbesar dari rakyat Indonesia tidak adalah  pemimpin yang bisa menyumbangkan  jasa-jasa sebesar jasa Bung Karno, atau yang bisa mencetuskan  gagasan-gagasan serevolusioner dan sebanyak dia.

Namun, ada orang atau kalangan yang berpendapat bahwa memperingati wafatnya Bung Karno, yang sudah terjadi 40 tahun yang lalu adalah sesuatu yang sudah kedaluwarsa,  atau sesuatu yang tidak perlu lagi « dikunyah-kunyah terus-menerus ». Ada juga yang mengatakan « biarkanlah masa yang lalu, jangan diutik-utik lagi, yang penting adalah masalah depan kita ».

Orang-orang atau golongan yang mengatakan seperti tersebut di atas,  sebaiknya diajak untuk secara serius merenungkan berbagai hal yang yang berkaitan erat dengan  sakitnya atau wafatnya Bung Karno. Sebab, masalah ini sama sekali bukanlah hal yang sudah kedaluwarsa, dan bukannya pula sesuatu yang tidak perlu dikunyah-kunyah terus-menerus atau tidak pelu diutik-utik lagi.

Berbagai akibat wafatnya Bung Karno

Wafatnya Bung Karno merupakan satu rentetan rantai yang tidak bisa dipisahkan dari penggulingannya  secara khianat oleh pimpinan Angkat Darat (waktu itu) di  bawah Suharto. Dan akibat yang menyedihkan dari penyerobotan kekuasaan Bung Karno itu adalah lumpuhnya  revolusi rakyat Indonesia dan rusaknya negara dan bangsa seperti yang kita saksikan dewasa ini. Jadi membicarakan sebab-sebab dan akibat-akibat wafatnya Bung Karno, justru ada hubungannya  yang erat sekali dengan berbagai hal masa kini.

 Banyak soal di masa kini yang merupakan akibat  -- secara langsung atau tidak langsung  --  dari berbagai persoalan yang menyebabkan wafatnya Bung Karno dalam tahun 1970 itu . Jelaslah bahwa masalah-masalah yang menyebabkan dan akibat wafatnya Bung Karno sama sekali bukanlah hal yang sudah kedalu warsa untuk diingat kembali atau ditelaah lagi untuk kepentingan masa kini dan untuk masa depan negara dan bangsa kita.

Wafatnya Bung Karno bukanlah seperti wafatnya pemimpin atau tokoh-tokoh Indonesia lainnya.
Peristiwa besar ini bukan saja menimbulkan dukacita yang dalam dan luas  bagi banyak orang, melainkan juga membangkitkan kemarahan, rasa brontak atau protes terhadap segala perlakuan  buruk sebelumnya, yang sunguh-sungguh biadab dan tidak manusiawi yang menyebabkan wafatnya

Mempunyai rasa duka terhadap wafatnya Bung Karno, meskipun sudah 40 tahun berlalu, adalah wajar dan bisa dimengerti, mengingat besarnya arti Bung Karno bagi bangsa Indonesia. Selain itu, bersikap marah besar atau gusar sekali terhadap perlakuan yang begitu ganas dan keji atas dirinya adalah sah, serta benar dan bahkan sudah seharusnya.

Mengutuk perlakuan biadab terhadap Bung Karno

Sebaliknya, tidak marah atau tidak mengutuk perlakuan yang keterlaluan biadabnya dari pimpinan Angkatan Darat (waktu itu) terhadap Bung Karno adalah sikap yang salah, yang berdasarkan moral yang rendah atau akhlak yang rusak. Sedang mengutuk atau menghujat segala hal yang tidak manusiawi yang diperlakukan terhadap Bung Karno adalah benar, sah, adil, dan juga luhur.

Siapapun yang beradab, yang berhati-nurani, yang bernalar sehat, yang bermoral, akan tidak menyetujui siksaan fisik dan bathin yang sudah dikenakan terhadap Bung Karno. Dan siapapun yang  merasa gembira, atau yang menyetujui, atau yang membenarkan perlakuan tidak manusiawi pimpinan Angkatan Darat (waktu itu) terhadap pemimpin besar bangsa kita adalah orang-orang yang patut diragukan kesehatan jiwanya atau kewarasan nalarnya.

(Untuk mendapat sedikit gambaran tentang betapa biadabnya perlakuan terhadap Bung Karno sebelum wafat, harap disimak kumpulan berbagai bahan yang sudah disajikan tersendiri melalui berbagai milis dan juga website)

Pendidikan politik dan moral yang penting

Peringatan tentang hari wafatnya Bung Karno tidak saja patut menjadi sumber pelajaran yang sangat berharga bagi para pendukungnya atau pencintanya yang jumlahnya besar sekali, melainkan juga bagi mereka yang pernah anti-Bung Karno, atau bagi mereka yang tidak begitu mengenal sejarahnya dan perjuangannya.

Peringatan sekitar peristiwa  ini  tidak saja bisa menjadi sumber pendidikan politik yang penting sekali bagi banyak orang, melainkan juga sebagai sumber pendidikan moral yang ideal sekali bagi rakyat luas, dan sekaligus juga menjadi sumber inspirasi perjuangan revolusioner bagi berbagai golongan, terutama bagi generasi muda bangsa.

Dengan  mendengar atau membaca kembali macam-macam bahan tentang wafatnya Bung Karno dalam keadaan yang tidak normal, maka orang banyak bisa menilai sendiri betapa besar kejahatan  pimpinan Angkatan Darat (waktu itu) yang berupa cara-cara biadab yang tidak bisa dima’afkan oleh nalar yang sehat, atau tidak bisa diterima oleh hati-nurani yang bersih, atau juga tidak bisa dibenarkan  oleh iman yang benar.

Bulan Juni dijadikan « Bulan Bung Karno »

Mengingat itu semuanya, maka dicetuskannya  Pancasila oleh Bung Karno pada tanggal 1 Juni 1945, dan Hari Kelahirannya pada tanggal 6 Juni 1901 serta  Hari Wafatnya pada tanggal 21 Juni 1970 adalah tiga hari sangat bersejarah yang untuk selanjutnya di kemudian hari patut diperingati oleh bangsa kita secara selayaknya.

Tiga hari bersejarah ini membuat tiap bulan Juni sebagai « Bulan Bung Karno », yang dapat digunakan oleh berbagai golongan rakyat untuk mengenang kembali keagungan satu-satunya pemimpin besar bangsa yang telah berjuang dengan konsekwen selama seluruh hidupnya demi kepentingan rakyat.

Mengenang kembali Bung Karno berarti juga mengingat kembali berbagai ajaran-ajaran revolusionernya, yang sekarang ini terasa sekali dibutuhkan oleh banyak golongan sebagai pedoman atau sumber inspirasi untuk mengadakan perubahan-perubahan besar dari keadaan serba bejat akibat sistem pemerintahan Orde Baru dan  politik pro-neoliberalisme yang dijalankan oleh pemerintahan-pemerintahan pasca-Suharto sampai sekarang.

Sudah lama banyak orang melihat  -- serta merasakan sendiri  -- bahwa bangsa dan negara kita sedang menghadapi kekosongan pedoman besar dan pimpinan nasional yang kuat dan dicintai rakyat  dan berwibawa seperti Bung Karno, sejak Suharto memerintah dengan Orde Barunya.

Keagungan ajaran-ajaran revolusioner Bung Karno

Kita semua ingat bahwa kalau Bung Karno telah berjasa dengan banyak sumbangan-sumbangan besarnya untuk negara dan bangsa yang berupa berbagai  ajaran-ajaran revolusionernya, maka dari Suharto beserta para jenderalnya  --  atau tokoh-tokoh sipil pendukungnya  -- sama sekali  tidak ada  (atau sedikit sekali, itu kalau pun ada !) pedoman atau ajaran yang berharga yang bisa jadi panutan bangsa.

Kalau kita perhatikan bersama, maka nyatalah bahwa selama Suharto bersama Orde Barunya berkuasa (bahkan juga sesudahnya) tidak ada dokuman atau karya yang mengandung pemikiran-pemikiran besar serta cemerlang yang sudah disajikan kepada bangsa, yang setingkat dengan kebesaran ajaran-ajaran Bung Karno, seperti, antara lain : Indonesia Menggugat, Lahirnya Pancasila, Manifesto Politik, Trisakti, Berdikari, pidato di Konferensi Bandung, Panca Azimat Revolusi, pidato di PBB « To build the world Anew » dll dll.

Dari pengamatan sesudah Bung Karno digulingkan secara khianat oleh Suharto beserta para jenderalnya, maka di Indonesia  hanya terdapat sosok-sosok yang kerdil, atau tokoh-tokoh politik yang « bonsai », yang jauh sekali perbedaannya dengan kebesaran sosok atau keagungan ketokohan revolusioner Bung Karno. Sampai sekarang !

Sosok-sosok yang kerdil atau « bonsai »

Padahal, seperti yang kita saksikan bersama dewasa ini, negara dan bangsa kita sedang menghadapi banyak persoalan-persoalan besar, yang berupa kerusakan moral yang sudah parah sekali, dan kebejatan akhlak atau pembusukan mental yang disebabkan oleh korupsi, dan situasi ekonomi dan sosial yang buruk akibat sistem politik yang busuk oleh kalangan-kalangan « atas » yang bersikap dekaden, dan berkolaborasi dengan kekuatan neoliberalisme.

Sebagian kecil dari kerusakan-kerusakan parah itu tercermin dalam kegaduhan sekitar peristiwa Bank Century, persoalan Bibid Chandra , kasus Gayus Tambunan, kasus pajak perusahaan-perusahaan Aburizal Bakri, hiruk-pikuk usul « dana aspirasi » Rp 15 miliar untuk tiap anggota DPR setahun, dan tersangkutnya para pembesar Polri, Kejaksaan, dan pengadilan dalam soal korupsi dan berbagai kejahatan dll dll dll )

Di tengah-tengah kerusakan-kerusakan berat dan parah di bidang moral dan politik itu semualah  sebagian dari masyarakat kita memperingati Hari Wafatnya Bung Karno tanggal 21 Juni. Dan kita semua tahu bahwa segala yang rusak parah  yang sedang terjjadi  dewasa ini, adalah hasil atau kelanjutan dari produk yang dibikin oleh sistem politik dan praktek-praktek rejim Orde Barunya Suharto beserta para jenderal pendukungnya.

Rejim Suharto adalah pengubur revolusi rakyat

Sekarang sudah terbukti, dengan jelas pula, bahwa rejim militer Orde Baru pada dasarnya telah merusak cita-cita proklamasi 17 Agustus 1945. Makin terang juga bagi  banyak orang, bahwa pada hakekatnya Suharto (beserta para jenderal pendukungnya) adalah pengkhianat Pancasila. Sudah tidak bisa dibantah oleh siapa pun yang berhati jujur bahwa Suharto bukanlah penyelamat Republik Indonesia yang diproklamasikan tahun 1945 melainkan, sebaliknya, malahan  merusaknya. Jelasnya, Suharto bersama para jenderal pendukungnya adalah pengubur revolusi rakyat Indonesia di bawah pimpinan Bung Karno.

Dengan mengingat hal-hal itu semualah kita bisa menjadikan Hari Wafatnya Bung Karno sekarang ini sebagai kesempatan yang baik sekali untuk mengangkat kembali tinggi-tinggi sejarah perjuangannya serta ajaran-ajaran revolusionernya,

Hari wafatnya Bung Karno bisa kita jadikan bagian dari « Bulan Bung Karno » selama bulan Juni tiap tahun yang mencakup juga tanggal lahirnya Pancasila (            1 Juni) dan  hari lahirnya Bung Karno (6 Juni).
Dengan cara begini kita semua dapat bersama-sama mengisi setiap bulan Juni dengan berbagai kegiatan untuk memperingati tiga hari bersejarah yang berkaitan dengan Bung Karno.

Oleh karena dalam sejarah sudah dibuktikan dengan gamblang sekali bahwa perjuangan Bung Karno adalah untuk kepentingan semua golongan bangsa Indonesia, maka seyogianya « Bulan Bung Karno » ini juga menjadi urusan semua golongan yang mendukung berbagai gagasannya yang revolusioner untuk menyatukan bangsa dan meneruskan revolusi yang belum selesai.

Dengan mengisi « Bulan Bung Karno » dengan berbagai kegiatan  -- dan melalui berbagai macam cara dan bentuk --  untuk mengangkat kembali ajaran-ajaran revolusioner dan gagasan-gagasan agung  Bung Karno, maka kita semua bisa menjadikan « Bulan Bung Karno » sebagai bulan pendidikan politik, dan  pendidikan moral,  atau pemupukan semangat pengabdian kepada rakyat.

Ajaran-ajaran Bung Karno perlu disebarluaskan

Karena sudah lebih dari 45 tahun ajaran-ajaran revolusioner Bung Karno telah dilarang, atau disembunyikan, atau dibuang dengan berbagai cara oleh rejim Suharto (dan pemerintahan-pemerintahan penerusnya) maka segala macam kegiatan untuk menyebarkannya  kembali adalah penting sekali bagi kehidupan bangsa , termasuk bagi generasi muda dewasa ini dan anak cucu kita di kemudian hari.

Sejarah bangsa sudah membuktikan bahwa ajaran-ajaran revolusioner Bung Karno merupakan gagasan-gagasan  politik yang paling bisa mempersatukan bangsa, dan merupakan pedoman moral revolusioner, serta sumber inspirasi perjuangan bagi rakyat yang mau berjuang, teutama  bagi kaum buruh, tani, perempuan, kalangan muda, dan rakyat miskin pada umumnya.  

Bangsa Indonesia patut merasa bangga mempunyai ajaran-ajaran revolusioner dan gagasan-gagasan  agung yang telah disumbangkan oleh Bung Karno. Oleh karena itu  ajaran-ajaran  atau gagasan-gagasan besarnya itu perlu disebarluaskan seluas-luasnya untuk dipelajari dan dihayati oleh sebanyak mungkin orang dari berbagai golongan yang mau berjuang.

Ajaran-ajaran revolusioner Bung Karno adalah senjata yang ampuh sekali bagi semua golongan yang mau berjuang melawan ketidakadilan , penindasan, dan penghisapan dari semua kalangan reaksioner di Indonesia, dan juga untuk melawan neo-liberalisme. Sari pati atau  inti jiwa revolusioner ajaran-ajaran revolusionernya itu dapat digali oleh siapa saja dalam berbagai bukunya, terutama dalam « Dibawah Bendera Revolusi » dan « Revolusi Belum Selesai ».

Dengan semangat untuk menjunjung tinggi-tinggi ajaran-ajaran revolusionernya  dan jasa-jasanya yang besar kepada bangsa Indonesia inilah kita peringati Hari Wafatnya Bung Karno pada tanggal 21 Juni ini.
