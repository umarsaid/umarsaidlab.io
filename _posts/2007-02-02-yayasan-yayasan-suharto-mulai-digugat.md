---
layout: post
title: Yayasan-yayasan Suharto mulai digugat
date: 2007-02-02
---

Menurut radio Elshinta Jakarta (1 Februari 2007) Presiden Susilo Bambang Yudhoyono telah menandatangani surat kuasa khusus (SKK) yang akan diberikan kepada Kejaksaan Agung untuk menggugat perdata sejumlah yayasan milik mantan Presiden Soeharto. Demikian diungkapkan Jaksa Agung Abdulrahman Saleh kepada wartawan di kantornya, Kamis (1/2)

Arman - demikian dia biasa disapa, red., mengatakan dirinya sudah berbicara dengan Presiden. Selanjutnya Presiden, kata Arman, menyatakan bahwa SKK sudah ditandatanganinya. Oleh karena itu, pihaknya kata Arman, pada Jumat (2/2) akan mengambil SKK tersebut. Sedangkan mengenai kesiapan gugatan perdata terhadap mantan Presiden Soeharto dalam kasus dana di Yayasan Supersemar, Arman menyatakan bahwa pihaknya tinggal menyelesaikan draft secara teknis untuk nantinya segera dilimpahkan ke Pengadilan Negeri Jakarta Pusat (berita Elshinta habis)
Di tengah-tengah berita yang menyedihkan tentang banjir yang melanda Jakarta, yang menyebabkan penderitaan yang sangat parah bagi banyak orang, mencuatnya berita tentang kasus penyimpanan uang haram sebesar 36 juta Euro (atau sekitar Rp 421 miliar) oleh Tommy Suharto di bank asing Banque Nationale de Paris (BNP) makin membikin marahnya banyak orang. Kemarahan banyak orang terhadap KKN yang dilakukan oleh keluarga Suharto ini makin menjadi-jadi dengan tersiarnya berita tentang akan digugatnya oleh Kejaksaan Agung mengenai yayayan-yayasan yang dikuasai “keluarga Cendana”.

Sebab, sedikit banyaknya, sejak lama sudah diketahui oleh banyak orang bahwa keluarga Cendana telah menumpuk harta haram secara besar-besaran, ketika rejim militer Orde Baru berkuasa dengan tangan besi selama 32 tahun. Tetapi, berhubung masih dikuasainya bidang-bidang eksekutif, legislatif, dan judikatif (dan terutama aparat-aparat kehakiman dan kejaksaan) oleh unsur-unsur pendukung Orde Baru, maka masalah kejahatan-kejahatan Suharto beserta keluarganya ini tidak pernah bisa dibongkar (kecuali sebagian dari kasus Tommy Suharto).


Berapa banyak yayasan-yayasan Suharto ?

Memang, masih banyak soal yang belum jelas tentang tindakan Kejaksaan Agung untuk menggugat (secara perdata) yayasan-yayasan Suharto ini. Dan kita semua bisa bertanya-tanya, apakah penggugatan perdata ini akan bisa merupakan langkah permulaan yang akhirnya bisa membeberkan dengan jelas banyak kejahatan KKN keluarga Cendana.

Kita belum tahu sekarang ini apakah Kejaksaan Agung akan memusatkan penggugatannya terhadap masalah satu yayasan Suharto saja (yaitu Yayasan Suoersemar), atau akan juga mempersoalkan 7 yayasan lainnya. Perlu diketahui oleh banyak orang bahwa yayasan-yayasan yang didirikan oleh keluarga Suharto adalah banyak sekali. Menurut data-data yang diumumkan oleh situs Soeharto Center (tanggal 7 Februari 2007) ada sebanyak 17 yayasan yang telah didirikan oleh keluarga Suharto.

Menurut situs Soeharto Center yayasan-yayasan keluarga Suharto itu adalah :
Yayasan Dharmais, Yayasan Supersemar, Yayasan Trikora, Yayasan AMP, Yayasan DGRK, Yayasan Harapan Kita, Yayasan Dakab, Yayasan Damandiri, Yayasan Seroja, Yayasan Mangadeg, Yayasan Ibu Tien, RS Harapan Kita, RS Dharmais, TMII –MPBP, Tapos, Mekar Sari, Pustakanas.


Karya George Adicondro dan majalah Time

Dan yayasan-yayasan adalah hanya sebagian kecil dari seluruh kekayaan haram yang telah dijarah keluarga Suharto lewat KKN selama puluhan tahun rejim militer Orde Baru. Mengenai jaring-jaringan kekayaan keluarga Suharto, dan keterangan-keterangan tentang yayasan dan perusahaan yang dimiliki olehnya, harap baca tulisan Geoge Junus Aditjondro, yang telah melakukan riset bertahun-tahun tentang masalah KKN yang dilakukan oleh keluarga Cendana. Tulisan panjang (dan amat pentingi ini!) ini disiarkan oleh Tempo Interaktif tanggal 14 Mei 2004, dan disajikan juga oleh website http://perso.club-internet.fr/kontak


Di samping itu harap disimak kembali juga laporan panjang yang pernah dibuat oleh majalah mingguan TIME Asia yang disiarkan tanggal 24 Mei 1999 mengenai “kerajaan keluarga”. Website http://perso.club-internet.fr/kontak.menyajikan kembali terjemahannya dalam bahasa Indonesia, dibawah judul “Suharto Incorporated -Kerajaan keluarga Suharto” .


Hampir 10 tahun terbengkalai

Sayang sekali bahwa baru sekarang ini (dalam tahun 2007) usaha untuk mempersoalkan yayasan-yayasan keluarga Suharto ini mulai ada kemajuan, dengan ditandatanganinya SKK (surat kuasa khusus) oleh presiden SBY baru-baru ini. Padahal, masalah yayasan-yayasan Suharto sudah ramai dibicarakan oleh berbagai kalangan sejak jatuhnya Suharto dari kekuasaannya sebagai presiden dalam tahun 1998.

Sudah dapat diduga bahwa sejak itu keluarga Suharto telah melakukan berbagai tindakan untuk “menyelamatkan” uang haram mereka, berhubung adanya suara-suara di masyarakat yang mempersoalkan yayasan-yayasan yang mereka dirikan itu. Adalah wajar bahwa karena itu yayasan-yayasan yang didirikan oleh keluarga Suharto (beserta orang-orang terdekatnya atau orang-orang kepercayaannya) sudah mengalami perobahan-perobahan penting, termasuk pemindahan besar-besaran aset-asetnya.

Walaupun sebagian dari yayasan-yayasan keluarga Suharto ini sudah dipreteli aset-asetnya, atau sudah tidak berfungsi semegah dulu-dulunya ketika Orde Baru masih berkuasa, atau bahkan sebagian dari yayasan-yayasan itu sudah tidak ada lagi, namun gugatan yang dilakukan sekarang ini tetap mempunyai arti yang penting. Yayasan-yayasan keluarga Suharto adalah sebagian dari wajah buruk Orde Baru. Di balik tirai kata-kata yang serba muluk dan serba “mulia” itu terbentang tujuan-tujuan yang nista dan jahat, yaitu pemupukan kekayaan dan penggalangan kekuasaan dan pengaruh, untuk memupuri berbagai kebobrokan praktek-praktek yang dilakukan oleh rejim militer Orde Baru yang dipimpin Suharto.

A. Umar Said


· * *
·
SUARA MERDEKA 2 Februari 2007

Kejakgung Dapat Kuasa untuk

Gugat Soeharto

JAKARTA - Presiden Susilo Bambang Yudhoyono telah memberikan
Surat Kuasa Khusus (SKK) kepada Kejaksaan Agung untuk mengajukan gugatan
perdata terhadap mantan Presiden Soeharto terkait Yayasan Supersemar.

"Tadi saya bicara dengan Presiden, Presiden bilang ternyata
sudah. Besok akan saya ambil," kata Jaksa Agung RI Abdul Rahman Saleh
kepada wartawan yang ditemui usai acara Purna Bakti mantan Wakil Jaksa
Agung Basrief Arief di Kejaksaan Agung, Kamis sore.

Seperti diberitakan sebelumnya, Kejaksaan Agung melalui Bidang
Perdata dan Tata Usaha Negara (Datun) sedang menyusun draft gugatan perdata
terhadap Soeharto terkait tujuh yayasannya termasuk Yayasan Supersemar.

Hati-hati

Kejaksaan menilai Yayasan Supersemar telah menyalahgunakan
dana donasi dari pemerintah. Gugatan yang dilakukan oleh Kejaksaan, menurut
Jaksa Agung, merupakan upaya penyelamatan uang negara yang diduga
mengalir ke sejumlah yayasan Soeharto tersebut.

Kejaksaan bersikap hati-hati dan tidak terburu-buru dalam
penyusunan dan pendaftaran gugatan terhadap Yayasan Supersemar itu karena
khawatir adanya berkas atau data penting yang tercecer atau tertinggal.

Disinggung mengenai kesiapan berkas gugatan untuk dilimpahkan
ke pengadilan, Jaksa Agung mengatakan, masih butuh waktu untuk
melengkapi berkas tersebut.

"Dari SKK ya, tapi dari segi kerapihan ada tenggang waktu
lagi. Masalah SKK sudah selesai," kata Jaksa Agung Abdul Rahman Saleh.


· * *


Jawapos, 24 Januari 20077

Gugatan atas Pak Harto Terganjal SKK SBY


JAKARTA - Kejaksaan Agung (Kejagung) menunggu surat kuasa khusus
(SKK) dari Presiden Susilo Bambang Yudhoyono (SBY) sebelum
melimpahkan gugatan terhadap mantan Presiden Soeharto atas kerugian
negara dalam kasus korupsi tujuh yayasan senilai Rp 1,7 triliun.

Permohonan SKK tersebut dikirim Jaksa Agung Abdul Rahman Saleh
kepada SBY melalui Mensesneg Yusril Ihza Mahendra dua pekan lalu.
Namun, hingga kemarin SBY belum menjawab surat tersebut. "Saat ini
(SKK) masih di tangan Mensesneg," kata Arman -sapaan Abdul Rahman
Saleh- di gedung Kejagung kemarin.

Menurut Arman, pelimpahan berkas gugatan terhadap Soeharto tinggal
menunggu SKK dari SBY. Jika SKK tersebut telah dikeluarkan,
kejaksaan menargetkan, pelimpahan berkas diharapkan dilaksanakan
pekan ini. "Kami masih menunggu, presiden kan kerjaannya banyak,"
katanya.

Arman mengatakan, tidak ada kendala serius untuk melimpahkan gugatan
tersebut, termasuk soal SKK. "Tidak ada persoalan. Tadi saya sudah
bicara dengan Mensesneg. Ya, dalam waktu dekat (dilimpahkan),"
jelasnya.

Sebelumnya, JAM Perdata dan Tata Usaha Negara (Datun) Alex Sato Bya
mengatakan, SKK dari presiden dibutuhkan untuk memenuhi ketentuan
hukum acara perdata. Sedangkan Direktur Perdata Yoseph Suardi Sabda
menambahkan, SKK diperlukan untuk memperkuat gugatan. Ini mengingat
SKK dari kementerian terkait rawan dipatahkan dalam proses
pembuktian.

Secara terpisah, pengacara Soeharto, M. Assegaf, mempertanyakan
sikap kejaksaan yang ngotot menggugat perdata kliennya. Kejaksaan
dinilai tak punya dasar untuk menyimpulkan bahwa kliennya telah
merugikan negara. "Kejaksaan perlu mendefinisikan dulu apa arti
kerugian negara," jelasnya.

Assegaf tidak setuju dengan kesimpulan kejaksaan bahwa Soeharto
merugikan negara saat mengalihkan uang yayasan untuk kepentingan
bisnis. Sebaliknya, langkah Soeharto menanamkan modal ke perusahaan
adalah untuk kepentingan pendanaan yayasan. "Ini konsekuensi ketika
yayasan dilarang melakukan aktivitas bisnis," kata Assegaf.

Sebelumnya, kejaksaan berencana mendaftarkan gugatan terhadap
Soeharto pada akhir Januari 2007 ini. Kejaksaan tinggal mengubah
surat dakwaan menjadi surat gugatan atas perkara Soeharto.

Merujuk pada surat dakwaan kasus korupsi tujuh yayasan, nilai
kerugian yang didakwakan kepada Soeharto Rp 1,3 triliun dan USD 419
juta (Rp 373,1 miliar).

Soeharto didakwa menggunakan dana Yayasan Supersemar untuk menutup
rugi valas Bank Duta USD 419 juta. Selain itu, mengalirkan dana Rp
1,026 triliun untuk bisnis anak dan kroninya, Tommy Soeharto, Sigit
Soeharto, dan Bob Hasan.

Dana yayasan juga digunakan membeli tanah di Citeureup, membeli
saham Indocement, hibah Bank Duta, dan membeli saham Citra Marga
Nusaphala (milik Tutut).(agm)

· * *
·


Jawapos, 15 Januari 2007,

Menggugat Soeharto: Rugikan Negara

Kejagung optimistis bisa memenangi gugatannya terhadap mantan
Presiden Soeharto atas kerugian negara Rp 1,3 triliun dan USD 410
juta (Rp 373,1 miliar). Berikut wawancara koran ini dengan Direktur
Perdata Kejagung Yoseph Suardi Sabda:

Apa dasar kejaksaan menggugat perdata Soeharto?

Kejaksaan awalnya mengajukan dakwaan pidana atas kasus korupsi tujuh
yayasan. Total nilai kerugiannya di atas Rp 1,5 triliun. Kami
berupaya mengajukan pidana ke PN Jakarta Selatan, tetapi selalu gagal
menghadirkan terdakwa. Nah, dari fakta tersebut, kejaksaan menggugat
perdata. Dalam (sidang) perdata, tergugat atau subjek perkara dapat
tidak hadir dan diwakilkan kepada pengacaranya. Jaksa tinggal
mengubah surat dakwaan menjadi surat gugatan.

Apa Soeharto terindikasi melakukan perbuatan yang merugikan negara?

Ya, itu didasarkan kajian kejaksaan selama ini. Kami tidak asal
menggugat. Dari surat dakwaan terungkap bahwa Soeharto selaku ketua
harian menyalahgunakan uang yayasan untuk berbagai kepentingan di
luar ketentuan perundang-undangan. Di Yayasan Supersemar, misalnya,
ada aset negara Rp 1,5 triliun. Saya sebut aset negara karena
penghimpunan dananya didasarkan pada PP No 15 Tahun 1976 dan Keppres
No 90 Tahun 1995. PP No 15 mengatur tentang kewajiban bank BUMN
menyetor keuntungan lima persen untuk yayasan, sedangkan Keppres No
90 tentang keharusan konglomerat menyumbang dua persen dari laba di
atas Rp 100 juta. Nah, dua produk hukum dilanggar saat Soeharto
memerintahkan yayasan untuk kepentingan di luar sosial, seperti
menutup kerugian Bank Duta dan membayar utang SempatiAir. Itu belum
lagi pada yayasan lain. Selain itu, dari kajian kami, langkah
tersebut menyalahi anggaran dasar yayasan.

Selain dua produk perundang-undangan itu, kejaksaan punya barang
bukti lain?

Ya, kami sedang mengumpulkan aliran dana dari yayasan kepada sejumlah
perusahaan milik kroni (Soeharto). Barang buktinya cukup banyak.
Seingat saya, jumlahnya mencapai sembilan filling cabinet.

Apa tujuan Kejaksaan dari gugatan perdata ini?

Sebagai wakil pemerintah, tentunya kami menginginkan uang negara
(yang disalahgunakan) itu dikembalikan. Gugatannya tidak saja
diarahkan kepada Soeharto, tetapi juga para pengurus yayasan.
Logikanya begini.
Saya kasih Anda uang sekian rupiah untuk membeli rokok. Tetapi, Anda
tidak membelikannya. Tetapi, justru digunakan untuk membeli permen.
Nah, saya selaku pemilik uang tentunya menginginkan uang itu
dikembalikan. Ini sangat sederhana.

Saat ini rencana gugatan itu sampai tahap apa?

Draf gugatannya sudah beres. Saat ini jaksa agung tinggal menunggu
surat kuasa khusus (SKK) dari presiden. Sesuai perundang-undangan,
memang SKK dapat diberikan melalui menteri terkait. Tetapi, kami
menginginkan dasar gugatan lebih tinggi. Kalau SKK dari Menkeu atau
menteri BUMN tak cukup, kami tak ingin gugatan kami kandas. Sebab,
mereka (pengacara Soeharto) dapat berdalih, pengucuran uang negara
itu didasarkan pada menteri ini atau menteri itu. Nah, dengan SKK
dari presiden, tentunya, perdebatan itu akan selesai.(agm

· * *


Koran Tempo, 17 Januari 2007

"Semua Dana Yayasan Diketahui

oleh Soeharto"

JAKARTA -- Direktur Perdata Kejaksaan Agung Yoseph Suardi Sabda
menegaskan mantan presiden Soeharto terkait dengan seluruh aliran
dana yang masuk atau keluar dari tujuh yayasan yang akan digugat
secara perdata oleh kejaksaan. "Semua dana yayasan diketahui oleh
Soeharto," ujarnya kepada Tempo, Senin lalu.

Karena itu, dia melanjutkan, Soeharto dinilai pantas untuk didudukkan
sebagai pihak tergugat I dalam rencana gugatan perdata yang akan
dilayangkan kejaksaan.

Kejaksaan Agung saat ini sudah merampungkan draf gugatan perdata
terhadap Soeharto terkait dengan tujuh yayasan, yakni Yayasan
Supersemar, Dana Sejahtera Mandiri, Trikora, Dharmais, Dana Abadi
Karya Bakti, Amal Bhakti Muslim Pancasila, dan Yayasan Dana Gotong
Royong Kemanusiaan.

Yayasan Supersemar akan menjadi yayasan pertama yang akan digugat
secara perdata. Kejaksaan menilai yayasan ini telah menyalahgunakan
dana sumbangan dari pemerintah yang nilainya mencapai Rp 1,5 triliun.

Menurut Yoseph, kedudukan Soeharto di yayasan tersebut memang sudah
tidak lagi menjadi pemimpin atau ketua. Bahkan, dia melanjutkan,
kepemimpinan yayasan pun kerap berganti-ganti orang. "Namun,
kapasitas dan otoritas Soeharto di tujuh yayasan itu sangat besar,"
ujarnya.

Sementara itu, sebelumnya mantan pengacara Soeharto pada perkara
pidana, Mohammad Assegaf, mengatakan gugatan perdata terhadap tujuh
yayasan ini dinilai bermasalah secara hukum. Apalagi, kata dia,
kejaksaan mencoba mendudukkan Soeharto sebagai tergugat.

Sebab, kata Assegaf, posisi Soeharto sudah tidak lagi menjabat
sebagai ketua di tujuh yayasan tersebut. "Kejaksaan hanya ingin
menjadikan Soeharto sebagai target saja," katanya.

Menanggapi pernyataan itu, Yoseph menyatakan gugatan perdata yang
akan dilakukan oleh kejaksaan bertujuan untuk menyelamatkan aset-aset
negara. Selama ini, menurut dia, sebagian aset negara tersebut diduga
tertimbun di tujuh yayasan Soeharto. SANDY INDRA PRATAMA
