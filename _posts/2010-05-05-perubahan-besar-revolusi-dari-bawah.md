---
layout: post
title: Perubahan besar (revolusi) dari bawah   
date: 2010-05-05
---

Sebagai pengantar tulisan :


Puisi Negeri Para Bedebah
Karya:Adhie Massardi (mohon ma’af, disingkat)

Tahukah kamu ciri-ciri negeri para bedebah?
Itulah negeri yang para pemimpinnya hidup mewah
Tapi rakyatnya makan dari mengais sampah
Atau jadi kuli di negeri orang yang upahnya serapah dan bogem mentah

Di negeri para bedebah
Orang baik dan bersih dianggap salah

Maka bila negerimu dikuasai para bedebah
Jangan tergesa-gesa mengadu kepada Allah
Karena Tuhan tak akan mengubah suatu kaum
Kecuali kaum itu sendiri mengubahnya

Maka bila negerimu dikuasai para bedebah
Usirlah mereka dengan revolusi
Bila tak mampu dengan revolusi,
Dengan demonstrasi
Bila tak mampu dengan demonstrasi, dengan diskusi


                  *  * *     


         Perubahan besar (revolusi ) dari bawah   




Harap sebelum membaca tulisan ini sampai akhir, para pembaca sudah siap dalam fikiran bahwa akan menjumpai berbagai ungkapan-ungkapan yang kasar, yang tidak « senonoh », yang bisa dianggap tidak sopan, yang mungkin menjengkelkan perasaan , atau , bahkan ( !) menyakitkan hati sebagian orang.


Sebab, tulisan ini memang merupakan curahan uneg-uneg dari hati yang gundah, ledakan kemarahan yang meluap-luap, dan letupan kedongkolan yang sudah berpuluh-puluh tahun terpendam dalam dada dan kepala.


Seperti yang mungkin terjadi juga pada diri banyak orang lainnya, kemarahan dan kejengkelan ini makin menggunung akhir-akhir ini melihat situasi negara dan rakyat kita, yang kelihatan dibikin semakin bobrok dan semakin rusak oleh sistem pemerintahan SBY-Budiono, sebagai penerus pemerintahan-pemerintahan sebelumnya (terutama sistem pemerintahan di bawah kekuasaan Suharto).


Kerusakan, kebobrokan, kebejatan, kebrengsekan, kebusukan di negeri kita sudah kelewat parah dan merata atau menyeluruh. Tidak hanya di tingkat pusat (Jakarta) saja, melainkan juga juga di daerah-daerah, di propinsi, kabupaten, dan kecamatan, bahkan juga di tingkat pedesaan. Tidak salahlah kiranya kalau ada orang-orang yang mengatakan bahwa negeri kita sekarang sudah menjadi negeri bedebah, negara para maling, atau negara para penjahat.



Kerusakan sesudah digulingkannya Bung Karno


Kerusakan moral atau pembusukan mental atau kebejatan akhlak di kalangan elite ini ( antara lain : pejabat tinggi pemerintahan, pimpinan partai politik, anggota DPR, pimpinan aparat negara) sebenanya sudah nyata-nyata kelihatan sejak digulingkannya pemerintahan Bung Karno oleh para jenderal di bawah pimpinan Suharto.


Kebejatan akhlak dan kerusakan moral itu tidak hanya tercermin dalam pengkhianatan besar-besaran terhadap Bung Karno berikut seluruh ajaran-ajaran revolusionernya dan penghinaan dan penyiksaan --- yang keterlaluan besarnya -- terhadapnya (ingat : perlakuan terhadap Bung Karno selama bertahun-tahun dalam tahanan rumah ketika ia sedang sakit parah).


Perlulah agaknya ditegaskan sekali lagi bahwa kerusakan moral dan kebejatan akhlak secara besar-besaran (dan sangat serius !!!) ini termanifestasi sejak pemerintahan Orde Baru -- yang ditulangpunggungi oleh militer dan Golkar dan didukung oleh kekuatan-kekuatan reaksioner dalamnegeri dan luarnegeri - mengangkangi negara selama 32 tahun.

Nation building dan character building dibusukkan Orde Baru


Kiranya, banyak pengamat sejarah Republik Indonesia dan berbagai ahli ilmu politik dan sosial bisa melihat, dengan jelas pula, bahwa pemerintahan Orde Baru di bawah Suharto adalah pemerintahan yang telah menimbulkan kerusakan-kerusakan besar sekali di bidang oembangunan jiwa bangsa.


Nation building dan character building yang dengan susah payah dan gigih sekali sudah diperjuangkan Bung Karno selama lebih dari 40 tahun (sejak ia masih muda dalam tahun 20-an) telah dirusak, dikotori atau dibusukkan oleh berbagai politik dan praktek-praktek Orde Baru, yang diteruskan oleh berbagai pemerintahan, sampai sekarang.
Salah satu di antara berbagai macam perusakan jiwa bangsa dan pembusukan akhlak manusia Indonesia (tidak semuanya, sebagian saja) adalah pembunuhan, pemenjaraan, penahanan, persekusi terhadap puluhan juta orang kiri anggota dan simpatisan PKI yang tidak bersalah serta para pendukung setia politik Bung Karno.
Untuk jangka yang lama sekali, akibat propaganda keji dan jahat rejim militer yang terus-menerus selama puluhan tahun, sebagian besar masyarakat Indonesia bisa dipengaruhi (secara salah) untuk anti-Bung Karno, anti-PKI atau anti-kiri
Dengan politik anti-Bung Karno dan anti-PKI ini, maka seluruh ajaran revolusioner Bung Karno (antara lain : Pancasila, Marhaenisme,Trisaksi, Berdikari, Nasakom, Manipol, Dekon, Ampera) yang pernah menjadi pedoman perjuangan rakyat Indonesia, telah dibuang jauh-jauh atau dilarang selama puluhan tahun.
Dengan dilarangnya ajaran-ajaran revolusioner Bung Karno, dan dilumpuhkannya kekuatan kiri pada umumnya, maka terjadi kerusakan jiwa dan pembusukan moral secara besar-besaran dan parah yang berjangka lama. Bolehlah dikatakan, bahwa ajaran-ajaran revolusioner Bung Karno, yang secara garis besar sejiwa atau searah dengan ajaran-ajaran golongan kiri, merupakan tameng rakyat atau benteng bangsa terhadap kerusakan jiwa atau dekadensi moral.

Kerusakan jiwa dan pembusukan moral


Oleh karena itulah mengapa selama pemerintahan ada di bawah pimpinan Bung Karno orang tidak banyak bicara tentang kemerosotan moral yang meluas, penyalahgunaan kekuasaan, korupsi, kolusi, nepotisme, seperti yang terjadi selama Orde Baru dan diteruskan sampai sekarang. Moral publik selama jaman Bung Karno telah ditandai dengan jiwa revolussioner perjuangan, jiwa pengabdian kepada revolusi, jiwa pengabdian kepada rakyat banyak, jiwa gotong royong, jiwa anti-imperialisme


Orde Baru (dan berbagai pemerintahan yang menggantikannya kemudian) telah merusak sama sekali jiwa bangsa yang pernah dikagumi oleh banyak rakyat Asia-Afrika dan di berbagai rakyat lainnya di dunia, berkat dipancarkannya keagungan gagasan gemilang dan ajaran-ajaran revolusioner Bung Karno lewat Konferensi Bandung, Dibawah Bendera Revolusi, Setiakawan rakyat-rakyat Asia Afrika, Ganefo, Conefo, « Go to hell with your aid », dan berbagai politiknya yang anti-imperialis.
Kerusakan jiwa yang parah dan pembusukan moral yang terjadi selama Orde Baru ini begitu besarnya dan begitu luasnya dan berlangsung begitu lamanya, sehingga akibatnya sangat buruk sekali bagi jiwa dan moral berbagai angkatan bangsa sejak 1966. Perusakan jiwa dan moral bangsa inilah merupakan dosa yang terbesar dari rejim militer Suharto, yang akibat buruknya sama-sama banyak kita lihat dewasa ini.
Sekarang ini, kita saksikan di Indonesia keadaan sosial yang serba semrawut, situasi politik yang kotor dan kacau, ekonomi yang menyulitkan orang banyak, dan kebudayaan yang tidak sehat, Negara kita sudah menjadi negara maling, para pejabatnya menjadi penjahat, para tokohnya banyak yang menjadi penipu rakyat. Rakyat banyak hidup sengsara, terutama rakyat miskin, sedangkan sebagian kecil bergelimang dengan kemewahan hasil korupsi atau perbuatan haram lainnya.



Bukan negara macam ini yang dicita-citakan para pejuang
Negara kita yang sebrengek seperti sekarang ini bukanlah negara yang dicita-citakan para pahlawan revolusi 17 Agustus 45, dan bukan pula yang diidam-idamkan oleh para pejuang perintis kemerdekaan, dan sama sekali bukanlah yang dikehendaki Bung Karno. Mereka itu semuanya pastilah sedang menangis sambil mengutuk dalam makam mereka masing-masing melihat negara dan bangsa dibikin serba bejat sekarang ini.
Bejatnya pemerintahan sekarang, dan rusaknya moral di banyak kalangan - terutama sekali ditingkat atas - sudah kelewatan sehingga sulit diperbaiki dalam jangka dekat, bahkan juga tidak mungkin dalam puluhan tahun. Dengan SDM (sumber daya manusia) yang seperti sekarang ini, yang sebagian terbesar adalah produk sistem Orde Baru, adalah tidak mungkin mengadakan perubahan besar-besaran dan perbaikan fundamental.
Sebab, urat nadi pemerintahan atau pusat syaraf kekuasaan di tingkat pusat sekarang ini sudah keterlaluan membusuknya sehingga seluruh badan negara ikut sakit parah. Bayangkan, wakil presidennya dan Menkeu-nya harus diperiksa KPK, banyak jenderal Polri diperiksa, hakim-hakim tinggi makan suap, Kejaksaan Agung jadi sarang jaksa yang korup, pusat perpajakan dipenuhi maling-maling tingkat kakap, banyak anggota DPR yang dicurigai tidak bersih, dan badan-badan negara lainnya juga jadi sarang penyamun. Komplit sudah !
Sekarang ini, banyak orang sudah tidak lagi punya ilusi bahwa perbaikan besar-besaran atau perubahan fundamental bisa diadakan mulai dari atas. Jelaslah bahwa itu hanyalah impian kosong saja !!!. Berbagai pengamat sejarah dan pakar-pakar di bidang ilmu sosial dan politik akan mengatakan bahwa banyak penyakit parah bangsa -- yang disebabkan kerusakan moral sekarang ini -- akan memerlukan banyak generasi untuk menyembuhkannya.

Perubahan besar-besaran lewat revolusi


Nyatalah bagi kita semua sekarang, bahwa untuk memperbaiki kerusakan besar dan menyembuhkan penyakit parah yang melanda bangsa kita dewasa ini diperlukan adanya shock therapi atau perubahan besar-besaran dengan cara-cara yang luar biasa, dan tidak bisa hanya dengan reformasi kecil-kecilan atau langkah-langkah tambal sulam, yang seperti selama ini sudah dilakukan di Indonesia. Perubahan besar-besaran dan fundamental ini bisa dinamakan revolusi. Dan revolusi ini bisa berlangsung dengan bermacam-macam cara, bentuk, dan isi ; yang pada umumnya berarti perombakan kekuasaan, atau perubahan kekuasaan , atau penggeseran kekuasaan, atau perebutan kekuasaan.


Perubahan besar-besaran itu ( atau revolusi itu) seyogianyalah datang dari bawah atau dari rakyat yang dilakukan dengan dan oleh kekuatan revolusioner massa rakyat yang luas. Sebab, hanyalah rakyat yang dipersenjatai semangat atau jiwa revolusioner untuk menciptakan perubahan besar yang bisa menyelamatkan negara dan bangsa. Dan sama sekali bukannya lapisan atas (antara lain, para jenderal reaksioner dan tokoh-tokoh korup di berbagai bidang dan golongan) yang dekaden moralnya, yang selama ini merupakan lintah-lintah bangsa, yang menjadi bedebah-bedebah dan sampah bangsa.


Sayangnya, kekuatan massa rakyat yang berhaluan progresif revolusioner ini (yang terutama terdiri dari pendukung berbagai politik Bung Karno dan kaum kiri atau anggota dan simpatisan PKI) telah selama puluhan tahun ditindas atau dipersekusi oleh Orde Barunya Suharto, sesudah jutaan di antaranya dibantai secara ganas. Untunglah bahwa sebagian dari kekuatan progresif ini setapak-setapak mulai bangkit dan bergerak lagi, dengan macam-macam cara dan bentuk.

Munculnya kembali kekuatan kiri akan baik buat bangsa
Munculnya kembali -- dari bawah dan secara luas -- kekuatan progresif atau kekuatan kiri di Indonesia (walaupun perlahan-lahan dan melalui berbagai kesulitan) hanya akan mendatangkan kebaikan bagi negara dan bangsa, dan bukan sebaliknya. Sudah terbukti dari pengalaman bangsa kita selama ini bahwa kehancuran atau melemahnya kekuatan kiri hanyalah menyebabkan kemunduran atau kerusakan atau dekadensi bangsa. Ini sudah dibuktikan oleh jaman Suharto.


Sejarah bangsa Indonesia akan membuktikan di kemudian hari bahwa pada akhirnya, kekuatan reaksioner di Indonesia - yang sejenis Orde Baru, atau jenis-jenis lainnya -- akan bisa dikalahkan oleh kekuatan massa rakyat. Jatuhnya rejim militer Suharto yang sangat luar biasa « digdaya »-nya -- tetapi sangat busuk dan korup -- dalam tahun 1998 adalah salah satu contohnya.


Generasi-generasi kita di kemudian hari juga akan menyaksikan bahwa hanya pemerintahan rakyat, yang sungguh-sungguh.berjuang untuk masyarakat adil dan makmur (atau masyarakat sosialis Indonesia, menurut Bung Karno) yang akan menyelamatkan tujuan proklamasi 17 Agutus 45. Bukan pemerintahan atau kekuasaan politik type lainnya.
