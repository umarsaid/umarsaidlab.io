---
layout: post
title: Generasi muda adalah pendorong reformasi
date: 2000-10-31
---

[ Artikel dimuat dalam Website Masyarakat Transparansi Indonesia -MTI- Jakarta ]

Ketika udara politik di Indonesia sedang pengap oleh tingkah laku para "elite" yang mencak-mencak di berbagai panggung sambil mengumbar berbagai ucapan, yang membikin bingung banyak orang tentang arah perjalanan bangsa, maka suara yang dikumandangkan dalam beraneka-ragam kegiatan dalam rangka peringatan Hari Sumpah Pemuda baru-baru ini, adalah bagaikan angin segar rasanya. Kegiatan-kegiatan yang mengambil bermacam-macam bentuk ini (demonstrasi, pertemuan besar dan kecil, ceramah dan seminar dll) telah digelar di berbagai kota dan daerah oleh kalangan muda dengan partisipasi berbagai kalangan masyarakat.

Kegiatan-kegiatan itu memberikan sumbangan penting bagi banyak golongan untuk menyadari akan bahaya perpecahan dan permusuhan yang sedang mengancam persatuan bangsa dewasa ini, dan sekaligus juga untuk mengenang kembali jasa-jasa para perintis kemerdekaan nasional dari kolonialisme Belanda dan mengenal sejarah para pendiri Republik Indonesia. Mengenang kembali perjuangan mereka itu bisa merupakan sumber inspirasi bagi kita semua, (termasuk generasi muda kita), untuk meneruskan perjuangan dan cita-cita luhur mereka.

Yang menarik, dan juga menggembirakan, adalah bahwa dalam banyak kegiatan itu terasa adanya kebutuhan bersama, untuk mengkaitkan Hari Sumpah Pemuda itu dengan situasi aktual bangsa dewasa ini, yang sedang menghadapi berbagai persoalan. Umpamanya, antara lain (menurut siaran Koridor.com 29 Oktober), adalah sarasehan aktifis mahasiswa pro-demokrasi angkatan 80-an yang berlangsung selama tiga hari di Puncak (Jawa Barat) dari tanggal 27 sampai 29 Oktober yang lalu. Para aktifis mahasiswa yang datang dari berbagai kota dan daerah di Jawa, Sumatera, Sulawesi, Nusa Tenggara dan Irian Jaya itu umumnya telah melibatkan diri dalam berbagai komite aksi mahasiswa, kelompok studi dan pers mahasiswa.

Di akhir sarasehan itu telah dilahirkan resolusi dan himbauan moral yang, antara lain, mendesak para pemimpin politik yang anti-rakyat dan anti-demokrasi untuk segera mengundurkan diri, mendesak pemerintah untuk konsisten menegakkan hukum bagi pelaku-pelaku KKN dan para pelanggar HAM. Para aktifis ini juga mendesak agar sistem ekonomi yang berkeadilan dan berpihak kepada rakyat menjadi agenda dan prioritas pemerintah. Pemerintah juga didesak untuk segera menyelenggarakan pendidikan gratis kepada rakyat (kutipan selesai).

Di antara berbagai kegiatan lainnya yang patut juga diketahui yalah upacara sederhana di Museum Sumpah Pemuda (Jalan Kramat Raya 106, Jakarta Pusat) yang diselenggarakan tanggal 28 Oktober, ketika puluhan siswa SLTA mengucapkan ikrar Sumpah Pemuda dan mengajukan tujuh seruan pelajar Indonesia. Seruan itu berbunyi : "Menyerukan kepada seluruh elemen bangsa untuk memperteguh keimanan dan ketakwaan kepada Tuhan YME dalam kehidupan berbangsa dan bernegara, meningkatkan kualitas ilmu pengetahuan dan teknologi dengan cara menyesuaikan anggaran pendidikan nasional, memperbaiki sistem pendidikan nasional, terutama dari segi kurikulum, agar sesuai dengan perkembangan zaman; memperkukuh persatuan dan kesatuan bangsa Indonesia dengan memperbaiki supremasi hukum, menentang keras tindakan anarkis dan peredaran serta pemakaian narkoba dan menumbuhkan semangat nasionalisme yang berbasiskan keadilan sosial demi keutuhan bangsa dan negara".

Dalam beraneka-ragam kegiatan-kegiatan lainnya yang diselenggarakan di berbagai kota dan daerah (juga di Palangka Raya, yang dihadiri oleh Gus Dur) jiwa yang serupa atau semangat yang senada juga telah dikumandangkan, termasuk tuntutan supaya Suharto beserta kroni-kroninya segera diadili.

## Generasi Muda Adalah Asset Bangsa

Walaupun Hari Sumpah Pemuda sudah diperingati selama puluhan tahun, terutama sejak lahirnya Republik Indonesia tahun 1945, tetapi arti pentingnya lebih-lebih terasa lagilah pada dewasa ini, ketika bangsa kita sedang melewati masa-masa gawat dan sulit. Dan ini pulalah, yang, rupanya, menjadi pemikiran atau keprihatinan banyak orang, termasuk di kalangan angkatan muda kita. Apa yang kita baca di bagian atas tulisan ini hanyalah sebagian kecil sekali dari apa yang menjadi suara hati nurani angkatan muda kita dewasa ini, termasuk yang dari "generasi 80-an".

Generasi 80-an adalah angkatan muda (terutama mahasiswa) yang mempunyai saham terbesar dalam menggulingkan rezim Suharto dan mengumandangkan pentignya reformasi. Mereka inilah yang secara besar-besaran - tanpa preseden dalam sejarah! - melancarkan gerakan dalam skala nasional untuk melawan sistem politik Orde Baru, mengutuk peran Dwifungsi ABRI, dan menuntut dihargainya demokrasi dan hak asasi manusia.

Di antara mereka ini ada yang, dewasa ini sesudah jatuhnya Suharto dan Habibi, masih terus dengan gigih meneruskan perjuangan untuk dituntaskannya reformasi, dan untuk menentang bahaya restorasi Orde Baru. Ketika sisa-sisa kekuatan Orde Baru masih kuat di bidang eksekutif, legislatif dan judikatif seperti sekarang ini, dan ketika kedudukan Gus Dur mengalami serangan dari berbagai fihak, maka peran angkatan muda untuk membela demokrasi dari bahaya laten Orde Baru (dan juga dari para reformis gadungan) adalah penting sekali.

Adalah kewajiban utama bagi seluruh kekuatan pro-demokrasi dan pro-reformasi, untuk tetap memupuk kerjasama dan persatuan dengan gerakan angkatan muda. Organisasi-organisasi buruh, tani, nelayan, pedagang dan pengusaha, perempuan, ex-tapol beserta keluarga mereka, kum miskin kota dll, perlu mengajak angkatan muda sebagai mitra perjuangan.

## Kader Bangsa Di Masa Depan

Keterlibatan gerakan angkatan muda dalam perjuangan membela kepentingan rakyat adalah pendidikan yang penting dalam menanamkan kepedulian terhadap rakyat, menyuburkan kecintaan kepada prinsip-prinsip "res publica" (demi kepentingan umum), memupuk rasa pengabdian kepada bangsa. Prestasi besar yang sudah dilakukan oleh gerakan mahasiswa dalam menjatuhkan Suharto dkk adalah pengalaman dan pelajaran yang berharga, yang perlu diteruskan dewasa ini dalam melawan sisa-sisa kekuatan Orde Baru.

Kalau kita menengok ke belakang beberapa tahun yang lalu, alangkah mempesonakannya bagi kita semua waktu itu, ketika melihat mahasiswa (dan pelajar sekolah lanjutan atas) hampir di seluruh perguruan tinggi di tanah-air telah menyatukan diri dalam satu perjuangan untuk menghadapi musuh yang sama : yaitu, sistem politik Orde Baru. Gelombang besar perjuangan ini menghimpun beraneka-ragam suku, agama, asal sosial, asal keturunan ras, aliran politik dll. Gelombang besar ini merupakan pendidikan politik secara besar-besaran, dalam tempo yang singkat dan dengan intensitas yang tinggi pula.

Angkatan muda generasi 80-an sekarang ini sebagian masih ada yang meneruskan studi atau ada yang baru saja menyelesaikannya. Mereka inilah yang, dalam masa dekat ini, akan menduduki berbagai tempat dalam masyarakat di bidang-bidang yang beraneka-ragam. Mereka inilah yang bersiap-siap untuk menggantikan generasi atau angkatan yang terdahulu, baik angkatan 70-an, angkatan 66, atau angkatan 45.

Mengingat bahwa banyak di antara angkatan 70-an (apalagi angkatan 66 dan angkatan 45) yang sudah termakan racun pola berfikir Orde Baru (mohon catat: tidak semua!), atau sudah "karatan" oleh berbagai praktek buruk sistem politik yang lama, atau sudah dibusukkan oleh KKN dan cara hidup yang tidak halal, maka patutlah kiranya kalau kita semua mengharapkan bahwa generasi muda kita akan menampilkan wajah yang baru dan memainkan peran yang baru pula.

## Janganlah Dirusak Mereka Itu!

Seperti yang sudah sama-sama kita saksikan selama puluhan tahun ini, kerusakan besar dan parah yang dibuat oleh sistem politik Orde Baru adalah justru di bidang moral atau mental. Kerusakan moral atau mental inilah yang sekarang kita saksikan dewasa ini, di mana-mana, di berbagai bidang, dan dalam berbagai bentuk. Tidak hanya di kalangan pemerintahan atau lembaga-lembaga, tetapi juga juga dalam masyarakat, bahkan juga di kalangan keluarga. Korupsi, penipuan, ketidakjujuran dalam urusan-urusan publik sudah bukan tabu lagi. Pelanggaran hukum dan penyalahgunaan kekuasaan, yang menjadi "tradisi" selama Orde Baru, masih terus terjadi juga sekarang ini. Banyak orang sudah tidak peduli lagi apakah harta atau kekayaan yang diperolehnya itu adalah lewat jalan halal atau haram. Dan orang juga tidak mau tahu lagi apakah perbuatannya itu merugikan kepentingan umum atau tidak. Membohongi rakyat juga sudah dianggap sebagai sesuatu yang sah dan wajar-wajar saja.

Dari segi ini pulalah dapat kita coba membaca keruwetan peta politik, sosial dan ekonomi yang sedang terhampar di hadapan bangsa kita dewasa ini. Kebanyakan para "elite" di berbagai kalangan (pemerintahan, DPR, lembaga, agama, masyarakat luas) adalah - langsung atau tidak langsung - produk Orde Baru. Kalaupun bukan, maka, setidak-tidaknya, mereka itu kebanyakan sudah ter-kontaminasi (ketularan) oleh penyakit ganas yang disebarkan selama puluhan tahun. Mereka inilah yang sekarang ini ikut melestarikan, atau meneruskan, praktek dan kebiasaan Orde Baru.

Oleh karena itu, adalah tugas strategis bagi seluruh kekuatan pro-demokrasi, pro-rakyat, dan pro-reformasi, unt itu kebanyakan sudah ter-kontaminasi (ketularan) oleh penyakit ganas yang disebarkan selama puluhan tahun. Mereka inilah yang sekarang ini ikut melestarikan, atau meneruskan, praktek dan kebiasaan Orde Baru.

Oleh karena itu, adalah tugas strategis bagi seluruh kekuatan pro-demokrasi, pro-rakyat, dan pro-reformasi, untuk sama-sama berusaha supaya angkatan muda kita dewasa ini jangan sampai dirusak lagi oleh fihak yang mana pun juga. Angkatan muda kita dewasa ini adalah asset berharga bagi kehidupan bangsa dan negara kita di masa-masa yang akan datang. Kalau mereka sejak sekarang terus menyatukan diri dengan perjuangan berbagai kalangan dalam membela demokrasi dan reformai, maka mungkin sekali negara dan bangsa kita akan bisa dikelola lebih baik dari pada yang sudah-sudah. Dengan begitu, maka bisalah kiranya digantungkan harapan bahwa hak-hak asasi manusia juga akan lebih dihargai di bumi Indonesia.

## Jiwa Dan Darah Segar Bangsa

Tidaklah berlebih-lebihan, kalau ada orang-orang yang mengatakan bahwa Orde Baru telah merusak generasi muda selama puluhan tahun. Melalui indoktrinasi yang menyesatkan, dan dengan menggunakan berbagai peraturan yang membatasi kebebasan berfikir, dalam kurun waktu yang panjang sekali generasi muda telah di-depolitisasi dan "digiring" secara paksa ke satu arah yang keliru. Akibatnya, generasi muda di zaman Orde Baru sudah kehilangan patriotisme kerakyatannya, dilucuti sifat-sifat kerevolusionerannya, atau ditumpulkan daya kritisnya. Dalam jangka lama KNPI (Komite Nasional Pemuda Indonesia), yang resminya adalah "wadah" angkatan muda, ternyata hanyalah dijadikan "kerangkeng politik".

Karena itu, wajarlah bahwa generasi muda di zaman yang gelap itu, pada umumnya (artinya tidak semuanya) tidak mengenal secara baik sejarah perjuangan para perintis kemerdekaan yang melahirkan Sumpah Pemuda di tahun 1928. Sebagai contoh, banyak di antara generasi itu yang mempunyai pengertian yang dangkal - bahkan keliru - tentang arti penting perjuangan Bung Karno dkk dalam usahanya untuk mempersatukan bangsa. Di sekolah-sekolah, atau pesantren, mereka telah terpaksa belajar sejarah bangsa dari buku-buku yang berisi pemalsuan atau ketidakjujuran.

Karena situasi sudah berobah, dan kebebasan demokratis mulai terbuka sedikit demi sedikit, maka perlulah kiranya seluruh bangsa kita memberikan tempat yang semestinya kepada angkatan muda kita dewasa ini. Generasi muda kita adalah komponen bangsa yang penting di kemudian hari, dan adalah juga darah segar rakyat kita.

Dalam rangka pemikiran yang demikian itulah kita semua patut menyambut, dengan gembira, lahirnya beraneka-ragam organisasi di kampus-kampus universitas atau di luar kampus. Demikian juga, kita patut merasa bangga dengan adanya berbagai kegiatan atau aksi-aksi dalam masyarakat yang dilancarkan oleh generasi muda, yang menunjukkan kepedulian atau komitmen mereka terhadap persoalan-persoalan yang sedang dihadapi oleh rakyat dan negara kita. Makin banyak generasi muda yang ikut menerjunkan diri dalam perjuangan bersama untuk menegakkan demokrasi dan meneruskan reformasi, makin besarlah investasi politik dan moral bagi kebaikan masa depan bangsa dan negara kita.

Dalam perjuangan bersama-sama dengan berbagai komponen bangsa inilah generasi muda kita akan bisa menarik pelajaran tentang apa yang baik bagi rakyat dan bangsa dan apa yang tidak. Dari praktek langsung, mereka akan bisa terus-menerus menghimpun pengalaman individual maupun kolektif, tentang perlunya melawan segala bentuk ketidakadilan sosial, ketidakadilan politik, dan ketidakadilan dalam bidang ekonomi.

Mengingat begitu banyaknya persoalan rumit dan gawat yang sedang melanda negeri kita, yang sebagian terbesar adalah tumpukan warisan zaman Orde Baru selama 32 tahun, maka bidang perjuangan kekuatan pro-demokrasi dan pro-reformasi sangatlah luas dan berat. Apalagi, sekarang sudah timbul banyak gejala yang makin lama makin jelas, yang menunjukkan bahwa sisa-sisa kekuatan Orde Baru di banyak bidang masih belum tersentuh sama sekali oleh reformasi. Bahkan, di sektor-sektor tertentu, dominasi masih dikuasai sepenuhnya oleh orang-orang lama pendukung sistem politik Suharto.

Karena itulah, generasi muda kita yang telah mengambil peran penting dalam menjatuhkan Suharto dalam tahun 1998, perlu meneruskan perjuangan, untuk mencegah restorasi Orde Baru. Bahaya laten Orde Baru ini tidak boleh diremehkan. Sebab, mereka ini sekarang sedang - dan pasti akan terus - menghalalkan segala cara, untuk bisa merebut kembali kedudukan politiknya. Mereka inilah yang sedang melakukan sabotase dalam berbagai bentuk, untuk menggoyahkan pemerintahan Gus Dur, dan berusaha menggulingkannya.

Reformasi harus berjalan terus, untuk mencegah terulangnya masa gelap Orde Baru. Dalam medan perjuangan inilah generasi muda Indonesia memikul tugasnya sebagai tenaga pendorong reformasi.

[ Artikel-Artikel: Upaya Penanggulangan Korupsi di Indonesia ]

---

© Copyright 1999 Masyarakat Transparansi Indonesia
The Indonesian Society for Transparency
http://www.transparansi.or.id
E-mail: mti@centrin.net.id
Jl. Ciasem I No. 1 Kebayoran Baru Jakarta Selatan 12180
Telp: (62-21) 724-8848, 724-8849 Fax: (62-21) 724-8849
