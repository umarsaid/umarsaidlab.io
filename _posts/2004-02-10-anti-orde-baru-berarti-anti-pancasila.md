---
layout: post
title: Anti-Orde Baru berarti anti-Pancasila ?
date: 2004-02-10
---

Selama lebih dari 30 tahun, para pendiri rezim militer Orde Baru (terutama dari kalangan TNI-AD) telah menyalahgunakan dan melacurkan Pancasila, dalam rangka untuk melaksanakan de-Sukarnoisasi, dan menghancurkan kekuatan utama pendukung politik Bung Karno, yaitu PKI dan golongan kiri lainnya. Dengan Pancasila yang sudah dipalsu - atau dibunuh jiwa aslinya- para tokoh militer telah memaksakan legitimasi Orde Baru. Dewasa ini, sisa-sisa kekuatan rezim militer Suharto dkk masih terus berusaha menggunakan (secara licik!) Pancasila untuk mempamerkan barang dagangan mereka yang busuk dan sudah dinajiskan oleh banyak orang, yaitu Orde Baru.

Salah satu contohnya ialah apa yang diberitakan Kompas pada tanggal 13 Januari 2004 yang berbunyi, antara lain : “Partai Karya Peduli Bangsa yang tegas-tegas menyatakan sebagai partai Orde Baru menyerang tokoh maupun partai yang anti-Orde Baru sebagai anti-Pancasila dan UUD 1945. "Orde Baru itu adalah sikap mental yang menjaga kemurnian Pancasila dan UUD 1945. Anti-Orde Baru berarti anti- UUD 1945, anti-Ketuhanan Yang Maha Esa, antikemakmuran, dan mendukung penjajahan," kata Ketua Umum Partai Karya Peduli Bangsa (PKPB) Jenderal (Purn) R Hartono kepada sekitar seribu kader PKPB dalam acara Pembukaan Rapat Kerja Nasional PKPB di Hotel Borobudur,

“Dalam acara tersebut hadir putri pertama mantan Presiden Soeharto, Siti Hardiyanti Rukmana, yang disebut-sebut sebagai calon presiden dari PKPB. Selain itu, hadir juga adik kandung mantan Presiden Soeharto, Probosutedjo, dan sejumlah mantan pejabat militer dan polisi, seperti mantan Kepala Polri Jenderal (Purn) Dibyo Widodo.

“Siti Hardiyanti dalam sambutannya mengajak semua kader PKPB untuk mengharumkan kembali citra bangsa Indonesia di mata internasional. "Dulu, Indonesia itu dihormati di luar negeri. Sekarang ini seperti dihina. Tugas kita mengangkat nama bangsa," ucapnya. (kutipan selesai).

## Pancasila Telah Dilecehkan Dan Dikhianati

Bahwa Jenderal (Purn) R. Hartono membela Orde Baru adalah wajar, dan juga haknya yang setimpal dengan apa yang telah dinikmatinya atau diperolehnya sebagai mantan KSAD rezim militer Suharto. Tetapi, kalau ia menyerang mereka yang anti-Orde Baru sebagai anti-Pancasila dan UUD 1945, adalah sikap yang betul-betul “tidak tahu diri” atau membuktikan bahwa sebenarnya ia tidak mengerti sama sekali arti Pancasila. Seperti kita ketahui dari pengalaman, Orde Baru selama puluhan tahun selalu menampilkan diri sebagai pembela Pancasila yang gigih dan setia, tetapi sebenarnya dalam praktek telah nyata-nyata menginjak-injak, melecehkan, mengotori, mengkhianati, bahkan membunuh Pancasila. Bahkan, rezim militer Suharto dkk juga telah membunuh secara kejam penciptanya Pancasila, yaitu Bung Karno.

Kita semua masih ingat bahwa dalam pidato-pidato Suharto atau para pembesar rezim militernya (dan juga tokoh-tokoh Golkar) sering sekali disebut-sebut kata Pancasila. Di seluruh negeri juga telah dipaksakan adanya kursus “penghayatan Pancasila” (P4) bagi pegawai-pegawai negeri, dengan isi yang menyesatkan dan dengan thema yang steril dan cara membosankan (bahkan memuakkan!). Rezim militer Orde Baru telah menggantikan Hari Kelahiran Pancasila 1 Juni 1945 menjadi 18 Agustus 1945, dalam rangka mengingkari peran Bung Karno sebagai penggali, atau perumus , atau penggagas Pancasila yang sebenarnya.

Selama puluhan tahun, setiap tanggal 1 Oktober dirayakan sebagai Hari Kesaktian Pancasila, untuk memperingati terbunuhnya 6 jenderal TNI-AD (dan seorang perwira) dalam peristiwa G30S, dan sekaligus sebagai legitimasi lahirnya Orde Baru. Hari Kesaktian Pancasila selalu dirayakan oleh rezim militer Orde Baru dengan penuh nuansa anti-Bung Karno dan anti-PKI.

Begitu besar sikap permusuhan para penguasa rezim militer Orde Baru (terutama tokoh-tokoh utama TNI-AD) terhadap Bung Karno, sehingga Kopkamtib melarang peringatan Hari Lahir Pancasila sejak 1 Juni 1970. Tidak lama kemudian, beberapa hari sesudah diumumkannya larangan itu, Bung Karno, pencipta Pancasila, wafat dalam keadaan sebagai tapol yang menderita sakit.

## Orde Baru Menjaga Kemurnian Pancasila ?

"Orde Baru itu adalah sikap mental yang menjaga kemurnian Pancasila dan UUD 1945” kata Hartono. Kita bisa bertanya-tanya tentang kesehatan jiwa dan kewarasan nalar atau kejernihan fikiran mantan jenderal yang satu ini, karena berani mengatakan bahwa “Orde Baru adalah sikap mental yang menjaga kemurnian Pancasila.”. Sebab, apakah ia tidak tahu bahwa justru karena tingkah-laku para pembangun Orde Barulah maka jutaan orang warghanegara RI yang tidak bersalah apa-apa telah dibunuhi dengan b iadab dalam tahun 1965-1966? Sikap mental yang “menjaga kemurnian Pancasilakah”, ketika para pembesar Orde Baru menjebloskan ratusan ribu orang tidak bersalah dalam penjara di seluruh Indonesia dalam jangka waktu yang lama sekali? Pancasila apa yang dipakai para penguasa Orde Baru ketika membiarkan jutaan (bahkan puluhan juta) anak, istri atau suami, dan anggota keluarga tapol lainnya tersiksa lebih dari 32 tahun (sampai sekarang !!!) meskipun mereka tak berdosa sama sekali? Apakah tindakan untuk “mengasingkan” –secara sewenang-wenang- puluhan ribu tapol di pulau Buru, Nusakambangan dan penjara-penjara lainnya berdasarkan Pancasila?

Sejak permulaan dibangunnya Orde Baru para pembesar militer (terutama TNI-AD° telah menyalahgunakan (dan menggunakan!) Pancasila untuk menggulingkan kekuasaan Bung Karno serta menghancurkan kewibawaannya. Orde Baru telah menyerobot Pancasila dari tangan penciptanya, Bung Karno, dan memakainya untuk hal-hal yang justru bertentangan sama sekali dengan jiwa asli Pancasila.

## Bung Karno : Pancasila Adalah Kiri Dan Pemersatu

Dalam dua jilid buku “Revolusi belum selesai” dapat dibaca kumpulan pidato-pidato Bung Karno sesudah peristiwa G30S, yang banyak menyinggung masalah Pancasila. Berikut adalah satu bagian kecil sekali dari pidato beliau dalam sidang paripurna Kabinet Dwikora di Bogor pada tanggal 6 November 1965 (yaitu kita-kira sebulan lebih setelah terjadinya G30S, ketika para pembesar militer pendukung Suharto mulai menggunakan Pancasila untuk menyerang Bung Karno) :

“Jangan kira, Saudara-saudara, kiri is alleen maar (keterangan : bahasa Belanda, yang artinya : hanyalah ) anti-imperialisme. Jangan kira kiri hanya anti-imperalisme, tetapi kiri juga anti-uitbuiting (penghisapan). Kiri adalah juga menghendaki satu masyarakat yang adil dan makmur, di dalam arti tiada kapitalisme, tiada exploitation de l’homme par l’homme, tetapi kiri. Oleh karena itu saya berkata tempo hari, Pancasila adalah kiri. Oleh karena apa ? Terutama sekali oleh karena di dalam Pancasila adalah unsur keadilan sosial. Pancasila adalah anti-kapitalisme. Pancasila adalah anti-exploitation de l’homme par l’homme. Pancasila adalah anti-exploitation de nation par nation. Karena itulah Pancasila kiri” (Revolusi belum selesai, halaman 77).

Dalam sidang pimpinan MPRS ke 10 di Istana Negara, 6 Desember 1965 (jadi dua bulan sesudah G30S) Bung Karno mengatakan :” Apakah tidak benar kalau saya berkata bahwa di waktu yang akhir-akhir ini, Pancasila dipergunakan sebagai satu barang an sich.Aku Pancasila! Maksudnya apa orang yang berkata demikian ini ? Aku antikomunis. Perkataan dipakai untuk sebetulnya men-demonstreer anti kepada Kom. Padahal Pancasila sebetulnya tidak anti-Kom. Kom dalam arti ideologi sosial untuk mendatangkan di sini suatu masyarakat yang sosialistis. Kalau dikatakan, ya aku Pancasila, tetapi dalam hatinya anti-Nasakom. Pancasila dipakai untuk mengatakan aku Pancasila, tetapi aku anti-Nas. Aku Pancasila tetapi aku anti-A.

Pancasila adalah pemersatu, adalah satu ideologi yang mencakup segala. Dan aku sendiri berkata, aku ini apa? Aku Pancasila. Aku apa ? Aku perasan daripada Nasakom. Aku adalah Nasionalis, akui adalah A, aku adalah sosialis, kataku. Tetapi banyak orang memakai Pancasila ini sebagai hal yang anti.” (Revolusi belum selesai, halaman 217).

## Mereka Tidak Berhak Bicara Tentang Pancasila

Kita bisa menduga-duga bahwa Jenderal (Purn) Hartono bukanlah satu-satunya orang yang rupa-rupanya bisa bicara tentang Pancasila tanpa mengerti apa sebenarnya isi dan jiwa Pancasila, yang digagas dan dilahirkan oleh Bung Karno. Tidaklah berlebih-lebihan, kalau kita katakan bahwa kebanyakan pejabat rezim militer Suharto dan para “tokoh” Orde Baru tidak pernah membaca tulisan-tulisan Bung Karno tentang Pancasila, apalagi “menghayati” keagungan arti Pancasila . Bagaimana mungkin? Bung Karno, bapak kemerdekaan bangsa dan pejuang anti-imperialis yang gigih telah dianggap musuh bebuyutan oleh kalangan militer pendukung Suharto, yang telah kerjasama dengan fihak imperialis.

Orang-orang seperti Hartono (dan sejenisnya!) tidak mungkin melaksanakan Pancasila yang sesungguhnya. Sebab, untuk bisa melaksanakan dengan sungguh-sungguh arti dan jiwa Pancasila, yang merupakan salah satu ajaran penting Bung Karno, diperlukan sikap yang correct terhadap penggagasnya atau penciptanya, yaitu Bung Karno sendiri.

Oleh karena itu, orang-orang sejenis Hartono, tidak berhak dan tidak pantas bicara tentang Pancasila. Sebab, sejarah Orde Baru sudah membuktikan bahwa Pancasila telah digunakan oleh “mereka” secara munafik selama puluhan tahun. Pada hakekatnya, Orde Baru telah membunuh Pancasila, berikut penciptanya, yaitu Bung Karno.
