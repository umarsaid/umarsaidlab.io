---
layout: post
title: Buntut kasus BLBI yang panjang
date: 2008-03-05
---

Berhubung dengan makin gencarnya berita-berita yang “seru” tentang kasus BLBI (Bantuan Likuiditas Bank Indonesia), yang sekarang kelihatan makin mengobok-obok berbagai lembaga tinggi negara (eksekutif, legislatif, judikatif), dan dengan tujuan untuk memberi kesempatan dan kemudahan kepada para pembaca untuk bisa mengikuti masalah besar ini maka website http://kontak.club.fr/index.htm menyediakan ruangan yang luas dengan memasang rubrik khusus yang berisi kumpulan berita, atau tulisan, atau berbagai pendapat yang berkaitan dengan persoalan ini.

Sekarang makin kelihatan bahwa di antara banyak masalah besar tentang penyelewengan atau korupsi yang terjadi di negara kita selama ini, kasus BLBI adalah salah satu sumber keruwetan atau sumber penyakit yang terparah. Bukan saja, karena menyangkut dana yang luar biasa besarnya (147,7 triliun Rupiah, atau 147 000 000 000 000 Rupiah, atau 147 juta Rupiah dikalikan sejuta. Tentu saja, sulit dibayangkan berapa besarnya uang yang begitu banyak itu !!!), tetapi juga karena banyaknya berbagai permainan kotor (dari banyak fihak) yang tersangkut di dalamnya.

BLBI secara singkat

Berikut adalah sekadar bahan untuk menyegarkan kembali ingatan kita bersama tentang BLBI : Bantuan Likuiditas Bank Indonesia (BLBI) adalah pinjaman yang diberikan Bank Indonesia kepada bank-bank yang mengalami masalah likuiditas pada saat terjadinya krisis moneter 1998 di Indonesia. Pinjaman ini dilakukan berdasarkan perjanjian Indonesia dengan IMF dalam mengatasi masalah krisis. Pada bulan Desember 1998, BI telah menyalurkan BLBI sebesar Rp 147,7 triliun kepada 48 bank.

Menurut Wikipedia Indonesia, audit BPK terhadap penggunaan dana BLBI oleh ke-48 bank tersebut menyimpulkan telah terjadi indikasi penyimpangan sebesar Rp 138 triliun. Dana BLBI banyak yang diselewengkan oleh penerimanya. Proses penyalurannya pun banyak yang melalui penyimpangan-penyimpangan. Beberapa mantan direktur BI telah menjadi terpidana kasus penyelewengan dana BLBI, antara lain Paul Sutopo Tjokronegoro, Hendro Budiyanto, dan Heru Supratomo

Selama bertahun-tahun, masalah BLBI merupakan soal yang tidak bisa diselesaikan secara tuntas oleh berbagai pemerintahan sesudah Suharto “turun tachta” (pemerintahan Habibi, Abdurrahman Wahid, Megawati dan sekarang SBY-JK), berhubung dengan banyaknya persoalan yang dihadapi oleh para pejabat negara yang bertugas untuk mengurusnya serta para konglomerat yang bersangkutan (pimpinan bank-bank yang menerima pinjaman). Di samping itu, ada “permainan” (dengan macam-macam cara) antara para konglomerat yang berusaha “mengemplang” dan para pejabat dan tokoh penting negara, yang menggunakan (atau mensalahgunakan ???) alasan hukum atau macam-macam dalih lainnya.

Banyak fihak yang “tersangkut” dengan BLBI

Entah sudah berapa saja uang haram yang sudah digunakan (sebagai suapan ke berbagai fihak ) dalam tahun-tahun yang lalu oleh para konglomerat hitam untuk usaha mereka menghindari kewajiban membayar utangnya (sebagai obligor) yang umumnya berjumlah sampai puluhan bahkan ratusan miliar Rupiah. Dapat diperkirakan bahwa banyak sekali pejabat negara dan tokoh masyarakat (termasuk di pemerintahan pusat, DPR, Kejaksaan Agung, Mahkamah Agung, hakim dan jaksa pengadilan) yang telah menjadikan para konglomerat yang tersangkut BLBI sebagai sapi perahan.

Kasus jaksa Urip Tri Gunawan yang tertangkap basah sedang menerima uang suapan sebesar 600.000 US$ (atau lebih dari 6 miliar Rupiah) dari Artalyta Suryani (orang dekat Syamsul Nursalim, tokoh penting bank BDNI) adalah salah satu dari banyak contoh yang bisa diangkat mengenai persoalan besar BLBI ini.

Dari apa yang terjadi dengan kasus BLBI, yang melibatkan uang negara dan rakyat ratusan triliun Rupiah dan menyangkut banyak pejabat-pejabat negara dan konglomerat hitam, jelas sekali kelihatan bahwa kasus BLBI adalah salah satu dari begitu banyak penyakit parah yang telah diidap bangsa kita sebagai akibat pemerintahan Orde Baru yang puluhan tahun. Penyakit parah bangsa kita dewasa ini bukan hanya karena kasus BLBI saja, dan bukan pula hanya karena kejahatan-kejahatan keluarga Cendana saja, tetapi juga karena kerusakan moral atau dekadensi mental yang menyerang secara besar-besaran kalangan elite bangsa kita.

Kasus jaksa Urip Tri Gunawan
Dari sudut ini pulalah kita bisa mencoba menelaah kasus jaksa Urup Tri Gunawan. Bahwa kasus ini merupakan cermin kebejatan moral yang amat parah adalah berikut ini :

Jaksa Urip Tri Gunawan tadinya dianggap oleh banyak orang sebagai jaksa (Kajari di Klungkung, Bali) yang berkepribadian baik, dan karenanya telah dipilih oleh Jaksa Agung sebagai Ketua atau Koordinator Tim Penyelidikan Kasus BLBI yang beranggotakan 35 jaksa yang diseleksi dari berbagai daerah di Indonesia. Tugas dari Tim yang beranggotakan 35 jaksa “terpilih” ini adalah mengusut kasus BLBI, terutama yang menyangkut bank BCA (pimpinan Antony Salim) dan bank BDNI (pimpinan Syamsul Nursalim). Setelah Tim ini bekerja selama 7 bulan untuk memeriksa kasus ini, maka kemudian diumumkan oleh Kejaksaan Agung bahwa “tidak ditemukan bukti-bukti adanya tindakan melawan hukum” oleh kedua pimpinan bank ini, dan karenanya Tim lalu dibubarkan.

Karuan saja, bahwa dinyatakannya Antony Salim dan Syamsul Nursalim bebas dari pengusutan Kejaksaan Agung ini menimbulkan kecurigaan sejumlah kalangan (termasuk kalangan DPR), apalagi tindakan untuk membubarkan Tim Penyelidikan Kasus BLBI. Persoalan yang kontroversial atau menimbulkan kecurigaan berbagai kalangan ini sampai menjadi gugatan yang ditujukan kepada presiden SBY. Sudah dapat diperkirakan bahwa buntut perkara ini masih panjang di kemudian hari, walaupun juga sudah dapat diramalkan sejak sekarang bahwa banyak persoalan menyangkut BLBI ini akhirnya akan tetap tidak bisa diselesaikan secara tuntas.

Banyak pertanyaan dan kecurigaan

Seperti yang dapat diduga, penangkapan jaksa Urip Tri Gunawan ini mengandung banyak pertanyaan atau berbagai persoalan, yang mungkin di kemudian hari baru bisa menjadi jelas, umpamanya dan antara lain :

-- apakah jaksa Urip Tri Gunawan menerima uang suapan sebesar itu atas suruhan atasannya (atau orang lain) atau apakah atas inisiatifnya sendiri ?

-- apakah uang suapan itu ada hubungannya dengan “pembebasan” Syamsul Nursalim (dan Antony Salim) dari kewajiban melunasi sisa utangnya dalam rangka BLBI?

-- apakah 35 jaksa anggota Tim Penyelidikan Kasus BLBI yang dipimpinnya ikut mengetahui bahwa ada uang sebanyak 6 miliar Rupiah dari “orang dekat”-nya Syamsul Nursalim ?

-- apakah ikut tertangkapnya Artalyta Suryani karena menyerahkan uang suapan sebesar 6 miliar Rupiah kepada jaksa Urip itu akan menyeret Syamsul Nursalim dan sejumlah pejabat-pejabat penting Kejaksaan Agung?

Bagaimanapun juga, penangkapan terhadap jaksa Urip Tri Gunawan sebagai orang penting dalam pengusutan kasus BLBI adalah peristiwa besar yang menggoncangkan Kejaksaan Agung sebagai instansi tinggi yang diharapkan atau dipercayai (tadinya !) oleh banyak orang untuk memerangi korupsi. Ada orang yang mengatakan bahwa tertangkapnya jaksa Urip menyerupai ledakan bom besar, yang merusak citra Kejaksaan Agung. Apalagi, kalau di kemudian hari ternyata bahwa 35 jaksa “pilihan” anggota Tim Penyelidikan Kasus BLBI juga tersangkut urusan kotor Urip ini, maka makin hilanglah kepercayaan publik terhadap aparat hukum dan peradilan di negeri kita, yang selama ini memang sudah dianjlokkan oleh moral bejat para simpatisan Orde Baru.

Kasus BLBI yang berbuntut panjang (dan ruwet!) sampai sekarang ini adalah produk dari “kebijaksanaan” yang diambil oleh sistem politik dan ekonomi Orde Baru. Tragisnya adalah bahwa rakyat Indonesia (bersama anak cucunya) harus menanggung banyak derita dan sengsara yang panjang karenanya, sedangkan sekelompok pejabat atau tokoh negara kita, bersama-sama para konglomerat hitam, dapat berfoya-foya dengan kemewahan yang berlebih-lebihan, yang didapat dari uang haram yang dicuri dengan berbagai cara dan jalan.

Itulah sebabnya, maka patut kita dukung atau kita sambut dengan sikap positif segala gerakan, atau aksi-aksi yang dilakukan oleh berbagai kalangan dalam masyarakat, yang menuntut diselesaikannya kasus BLBI secara tuntas. Menyelesaikan kasus BLBI secara tuntas berarti juga membersihkan dunia hukum negara kita, dengan menegakkan benar-benar keadilan dan kebenaran, dan bertindak terhadap oknum-oknum tidak bermoral di kalangan aparat negara, dan terhadap para koruptor atau konglomerat hitam. Aksi-aksi atau gerakan menuntut diselesaikannya kasus BLBI hendaknya juga merupakan bagian dari gerakan umum atau perjuangan untuk terpilihnya kekuasaan politik yang benar-benar pro-rakyat, atau terbentuknya kekuasaan politik di tangan rakyat. Hanya kekuasaan politik yang betul-betul pro-rakyatlah yang bisa menyelesaikan kasus BLBI secara baik, tanpa merugikan kepentingan rakyat banyak dan negara.

Untuk mengikuti sebagian dari berbagai persoalan BLBI itu, harap disimak kumpulan berita/artikel yang terdapat dalam rubrik khusus “Buntut kasus BLBI yang panjang” di website http://kontak.club.fr/index.htm
