---
layout: post
title: 'Dukungan kepada "komite Waspada Orde Baru"'
date: 2004-01-14
---

Situasi dalamnegeri menjelang Pemilu yad membikin banyak orang makin pesimis, atau prihatin, atau kuatir, atau bingung, atau putus asa. Berita-berita tentang korupsi atau penyelewengan kekuasaan di banyak bidang masih terus kita baca setiap hari di berbagai media di Indonesia. Kalau kita teliti dalam-dalam, maka nyatalah dengan jelas bahwa kebanyakan dari segala kesulitan-kesulitan besar yang dihadapi bangsa dan negara kita adalah disebabkan kerusakan moral atau kebusukan akhlak para “tokoh” kita. Namun, dalam menghadapi situasi yang demikian ini, ada banyak orang dari berbagai kalangan yang tidak mau berpangku-tangan saja atau “pasrah” saja. Contohnya, antara lain, diumumkannya baru-baru ini Manifesto Politik Mahasiswa Indonesia oleh 8 organisasi mahasiswa.

Perkembangan baru lainnya yang menggembirakan yalah berita yang disiarkan oleh Sriwijaya Pos (tanggal 10 Januari 2004), yang berbunyi sebagai berikut :

“Setelah sejumlah LSM ramai-ramai kampanye menolak politisi busuk, kini rakyat juga ingatkan agar menolak politisi anti-demokrasi. Komite Waspada Orde Baru (Tewas-Orba) mensinyalir banyak politisi anti-demokrasi kembali masuk kancah politik pada pemilu mendatang. Ketua Presidium (Tewas Orba) Judil Herry Justam kepada wartawan dalam Jumpa persnya di sekretariat Tewas Orba, Jl. Raya Pasar Minggu, Jakarta, Jumat (9/11), menunjuk Akbar, Beddu Amang dan R Hartono sebagai contoh politisi yang jelas-jelas anti-demokrasi.

Menurut Judil, para politisi anti-demokrasi ini akan mengganggu proses demokrasi yang sedang berjalan di Indonesia. Untuk itu menjadi kewajiban bersama untuk berhati-hati dan waspada terhadap masuknya kembali politisi anti-demokrasi tersebut.

"Kami akan mendata dan mempublikasikan politisi-politisi anti-demokrasi. Ini kami anggap penting, sebab bukan hanya politisi busuk saja yang menggangu proses demokrasi. Politisi anti-demokrasi juga berbahaya. Ini sekaligus menguatkan gerakan anti-politisi busuk," tegas Judil.

Untuk itu Tewas-Orba mengharapkan partisipasi masyarakat dalam bentuk masukan. Siapa saja yang mempunyai data dan informasi yang dapat dipertanggungjawabkan tentang sikap, ucapan dan tindakan dari politisi yang menunjukkan anti-demokrasi, dipersilakan menghubungi Tewas Orba. "Pengumpulan informasi dan data yang melibatkan masyarakat luas ini merupakan bagian dari proses pemberdayaan civil society dalam demokrasi," ujar Judil. Tewas-Orba menetapkan beberapa kriteria untuk menentukan seseorang sebagai politisi anti-demokrasi. "Antara lain pernah mendukung Dwi Fungsi TNI serta mendukung kebijakan anti-demokratisasi kampus, seperti NKK (Normalisasi Kehidupan Kampus)," kata Judil.

Judil menunjuk contoh Akbar Tanjung. Saat menjabat sebagai Ketua Fraksi Golkar, Akbar Tandjung menyetujui pembubaran dewan mahasiswa. Sementara R Hartono, kata Judil, sosok perwira TNI yang sangat mendukung dwi fungsi ABRI/TNI. "Saat menjadi KSAD R Hartono mengatakan, ABRI/TNI adalah kader Golkar. Sedangkan Beddu Amang, saat menjabat Ketua Kahmi, pada saat itu mendukung Dwi Fungsi ABRI/TNI," ungkap Judil.

Kriteria lainnya yang juga dijadikan dasar penetapan politisi anti-demokrasi adalah mendukung pembredelan media massa, terlibat manipulasi dan segala bentuk rekayasa untuk memenangkan konsep mayoritas Golkar pada masa Orba, serta mendukung Soeharto. "Kami masih menjaring nama-nama lain, caranya antara lain dengan melihat kliping-kliping berita. Politisi yang dulu terlibat anti-demokrasi akan kami keluarkan," tutur Jurdil.

Ketika ditanya kesiapan Tewas-Orba menghadapi gugatan dari politisi yang dimasukkan dalam daftar anti-demokrasi nanti, Judil dengan tegas telah mempersiapkan segalanya. "Kami siap menghadapi gugatan atau tuntutan dari pihak manapun," tegas Judil. (kutipan dari Sriwijaya Pos selesai).

## Gerakan Politik Dan Moral Yang Penting

Dilancarkannya oleh Komite Waspada Orde Baru (TEWAS ORBA) gerakan untuk mencegah masuknya kembali politisi busuk di gelanggang politik merupakan gerakan politik yang amat penting dan juuga gerakan moral yang tepat dalam situasi menghadapi pemilu yang akan datang. Sebab, sekarang sudah banyak tanda-tanda bahwa banyak politisi busuk (politisi hitam) sudah mulai berusaha, dengan segala macam cara dan jalan – termasuk yang kotor, haram dan bathil – untuk tampil dalam gelanggang politik.

Politisi busuk ini pada umumnya adalah orang-orang yang anti-demokrasi, Banyak di antara mereka itu pernah menjadi pendukung setia rezim militer Orde Baru, atau memainkan peran penting untuk berjayanya Golkar selama puluhan tahun. Mereka itu adalah pendukung berat dan pembela setia Suharto, yang telah melakukan berbagai kejahatan terhadap bangsa dan banyak pelanggaran kemanusiaan terhadap rakyat. Politisi busuk (politisi hitam) ini umumnya adalah orang-orang yang menyetujui politik rezim militer Suharto yang anti-Bung Karno beserta seluruh pendukung beliau, temasuk PKI dan golongan kiri lainnya. (Ingat, antara lain : pembantaian besar-besaran tahun 65, penahanan ratusan ribu tapol, pembuangan puluhan ribu orang tidak bersalah di pulau Buru dll dll).

Juga, politisi busuk ini banyak yang menjadi koruptor kelas kakap (atau menengah), yang sebagian sudah atau sedang ditindak Kalau kita teliti riwayat hidup para koruptor kelas kakap dan para “tokoh” yang sekarang sedang menghadapi pemeriksaan di berbagai kantor kepolisian, kejaksaan (atau pengadilan) di seluruh negeri maka akan nyatalah bahwa banyak di antara mereka itu yang pada masa jayanya Orde Baru telah menjadi pendukung aktif berbagai politik rezim militer Suharto. Boleh dikatakan bahwa para koruptor yang sudah (atau sedang) ditindak menurut hukum adalah orang-orang (memang, tidak semuanya !) yang pernah menduduki tempat penting di pemerintahan Orde Baru, atau mempunyai hubungan erat dengan orang-orang penting Orde Baru.

## Meneruskan Penghancuran Sisa-Sisa Orba

Aksi-aksi yang dilancarkan oleh TEWAS ORBA dengan dukungan berbagai kalangan ini, akan memperkuat gerakan politik dan gerakan moral yang dilancarkan oleh seluruh kekuatan pro-demokrasi dan pro-reformasi untuk mencegah sisa-sisa kekuatan Orde Baru bercokol kembali dengan leluasa dan secara besar-besaran. Perjuangan TEWAS ORBA adalah sejalan atau searah dengan aspirasi kalangan-kalangan lainnya yang anti-Orde Baru, umpamanya kalangan mahasiswa, seperti yang tercantum dalam Manifesto Politik Mahasiswa Indonesia. Atau sejiwa dengan aksi-aksi yang dilakukan oleh Indonesian Corruption Watch (Teten Masduki) yang akan mengeluarkan buku berisi daftar “politisi hitam”.

Mencegah bercokolnya politisi busuk (atau politisi hitam) berarti juga menuntaskan reformasi, yang sekaligus berarti meneruskan perjuangan menghancurkan sisa-sisa Orde Baru. Dalam konteks perkembangan situasi Indonesia menjelang pemilu, pemblejetan politisi busuk ini tidak hanya terbatas pada golongannya Akbar Tanjung, atau kalangannya mantan KSAD Hartono, atau konco-konco koruptor besar Bedu Amang saja. Para politisi busuk (politisi hitam) ini juga terdapat di banyak partai politik lainnya, atau di berbagai lembaga lainnya, baik di tingkat nasional maupun di tingkat daerah.

Ikut-sertanya banyak golongan (kalangan Ornop) dalam aksi-aksi untuk membikin dan mempublikasi daftar politisi busuk (politisi hitam) yang anti-demokrasi dan anti-reformasi akan merupakan sumbangan besar bagi pendidikan politik masyarakat luas. Dalam banyak hal, kita sudah tidak bisa lagi mengandalkan kepada kebanyakan partai-partai politik untuk mencerdaskan rakyat dalam soal-soal politik. Banyak partai politik di Indonesia yang malahan melakukan pembodohan atau penipuan politik terhadap rakyat. Tingkah-laku para anggota DPR dan DPRD pun banyak yang menunjukkan bahwa mereka tidak pantas (dan tidak berhak!) disebut mewakili rakyat.

Mengingat situasi yang makin simpang-siur dan penuh dengan gejala-gejala yang “tidak sehat” menjelang pemilu yang dilakukan oleh para politisi busuk, maka gerakan-gerakan atau aksi-aksi oleh berbagai kalangan masyarakat (seperti yang dilakukan oleh TEWAS ORBA, Indonesian Corruption Watch, organisasi-organisasi penandatangan Manifesto Politik Mahasiswa Indonesia dll) merupakan usaha penting untuk mencegah negara kita dikuasai oleh orang-orang yang rusak moralnya; yang anti-demokrasi, yang anti-reformasi, yang mendukung politik Orba.

Selama ini, kerusakan-kerusakan di berbagai bidang sudah terlalu besar yang dibuat oleh para politisi busuk ini. Keadaan yang demikian ini tidak bisa kjita biarkan terus!
