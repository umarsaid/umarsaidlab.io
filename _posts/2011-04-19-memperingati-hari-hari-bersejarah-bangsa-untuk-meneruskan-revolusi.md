---
layout: post
title: Memperingati Hari hari Bersejarah Bangsa Untuk Meneruskan Revolusi
date: 2011-04-19
---

Tidak lama lagi, dalam bujlan Mei dan Juni, bangsa Indonesia akan memperingati hari-hari bersejarah, yaitu : tanggal 1 Mei hari buruh internasional ; tanggal 20 Mei Hari Kebangkitan Nasional ; tanggal 23 Mei hari lahir Partai Komunis Indonesia (PKI) ; tanggal 1 Juni hari Lahirnya Pancasila ; tanggal 6 Juni hari lahirnya pemimpin besar revolusi Bung Karno ; tanggal 21 Juni wafatnya Bung Karno.

Website A. Umar Said akan berusaha ikut menyemarakkan peringatan-peringatan hari bersejarah bangsa tersebut di atas dengan menyajikan berbagai informasi dan tulisan-tulisan yang mengingatkan kita bersama kepada masalah-masalah besar bangsa dan negara di tengah-tengah penderitaan rakyat kecil dewasa ini, kebejatan  moral yang melanda seluruh negeri, korupsi yang mengganas dan merajalela dimana-mana, penegakan hukum yang macet, persatuan dan kerukunan bangsa yang morat-marit, Pancasila dan Bhinneka Tunggal Ika yang terancam oleh berbagai golongan.

Penyajian berbagai tulisan ini, termasuk yang akan diambil dari berbagai sumber,  adalah penting sekali,  mengingat bahwa selama ini isi yang dikandung atau pesan yang dibawa oleh hari-hari besar bangsa itu sudah dilupakan atau dimasabodohkan oleh sebagian besar bangsa kita, terutama oleh sebagian tokoh-tokoh dari « lapisan atas ».

Pesan Hari Buruh Internasional  1 Mei

Pesan (message) yang terkandung dalam Hari Buruh Internasional telah menjadi sumber inspirasi perjuangan kaum buruh di banyak sekali negeri di dunia, yaitu (antara lain) persatuan di antara kaum buruh dalam berjuang untuk kehidupan yang lebih baik, dengan melawan penindasan dan pemerasan kaum kapitalis reaksioner.

Di Indonesia, selama rejim militer Suharto, Hari Buruh Internasional 1 Mei, dilarang dirayakan karena dianggap (secara salah sama sekali !!!), bahwa 1 Mei adalah ciptaan golongan komunis atau salah satu siasat PKI untuk menggalang kekuatan. Bukan itu saja ! Bahkan, untuk jangka lama, bendera merah pun dilarang dikibarkan. Bahkan, lebih-lebih lagi, yang keterlaluan ( !), kata-kata « buruh » harus diganti dengan kata « pekerja » atau « karyawan ». Kata-kata « buruh » pernah menjadi momok di masa-masa Orde Baru.

Mengingat pentingnya arti Hari Buruh 1 Mei bagi kaum buruh di Indonesia yang dewasa ini sedang berjuang dengan gigih menghadapi berbagai persoalan berat, maka akan diusahakan adanya tulisan tersendiri, untuk menyambut Hari Satu Mei (May Day) ini.

Hari Kebangkitan Nasional tanggal 20 Mei.

Sejak digulingkannya Bung Karno secara khianat oleh Suharto  bersama-sama para jenderalnya, maka peringatan Hari Kebangkitan Nasional (20 Mei) seolah-olah kehilangan apinya, yang menyala-nyala dan bisa mengobarkan jiwa nasionalisme banyak orang. Dan kalaupun Hari Kebangkitan Nasional diperingati selama Orde Baru, namun selalu hanya dilakukan sebagai ritual, yang dangkal, yang kosong,  yang tidak ada isinya sama sekali dalam rangka nation and character building bangsa Indonesia. Tentu saja !!!

Memang, tidak bisalah kiranya diharapkan dari seorang yang pernah menjadi serdadu kolonial Belanda (KNIL) seperti Suharto mempunyai pengertian yang mendalam tentang sejarah kebangkitan nasional yang didorong oleh lahirnya Boedi Oetomo dalam tahun 1908. Padahal, lahirnya berbagai organisasi pemuda Jong Java, Jong Sumatra, Jong Ambon dll,  dapat dilihat sebagai rentetan lahirnya Boedi Oetomo.

Juga lahirnya Serikat Dagang Islam, dan kemudian Serikat Islam dan Serikat Rakyat, ditambah dengan lahirnya PKI dalam tahun 1920 (dan lahirnya PNI dalam tahun 1928) merupakan kelanjutan (dan perluasan) gerakan-gerakan kebangkitan nasionalisme, yang dicetuskan oleh Boedi Oetomo.

Hari lahir PKI tanggal 23 Mei

Sampai sekarang, sejak Suharto menjadi pemimpin tertinggi TNI, siapapun dilarang untuk merayakan atau memperingati hari lahir PKI pada tanggal 23 Mei. Ketetapan MPRS no 25/1966 yang melarang semua kegiatan PKI, dan larangan penyebaran Marxisme/Leninisme, masih berlaku meskipun sudah dipaksakan selama lebih dari 40 tahun.

Namun, kenyataannya, selama ini Marxisme dan Leninisme beredar dengan berbagai cara dan jalan (antara lain dengan Internet, Face-book dll ) Dan, selain itu,  juga dapat diduga bahwa kegiatan-kegiatan sisa-sisa PKI juga dilakukan oleh berbagai kalangan, dengan macam-macam cara dan bentuk,

Juga, untuk  Hari Lahir PKI (23 Mei),yang akan datang,  para simpatisan dan penganut PKI pastilah mencari jalan untuk memperingatinya, termasuk dengan merayakannya dalam hati saja, atau bersama-sama kawan sefaham mereka, walaupun terpaksa dengan diam-diam dan sembunyi-sembunyi.

Mengingat pentingnya masalah PKI bagi sejarah bangsa, maka perlu ada tulisan-tulisan tersendiri tentang soal besar ini.

Tanggal 1 Juni lahirnya Pancasila

Meskipun Pancasila sudah dinyatakan sebagai dasar-dasar negara dan pedoman besar bangsa, namun masih saja ada golongan (termasuk sebagian kecil kalangan Islam) yang ingin mempersoalkan atau mengotak-atiknya kembali. Suharto dan para jenderalnya juga pernah selama puluhan tahun menyalahgunakan « Pancasila » (palsu !!!) untuk melakukan kejahatan kemanusiaan yang luar biasa besarnya terhadap rakyat, terutama terhadap golongan kiri pendukung politik Bung Karno.

Dewasa ini, terdapat banyak pembesar, atau tokoh-tokoh, yang di mulut saja kaok-kaok Pancasila, Pancasila, Pancasila, namun perbuatan mereka dalam praktek sama sekali mengkhianati Pancasila berikut penggagasnya, yaitu Bung Karno. Tidak mungkin, atau mustahillah kalau ada orang yang mengatakan dirinya Pancasilais tetapi bersamaan dengan itu ia anti Bung Karno. Anti politik Bung Karno adalah pada intinya juga anti Pancasila.

Pada peringatan Hari Lahirnya Pancasila nanti, perlu diulangi berkali-kali bahwa untuk sungguh-sungguh menjiwai Pancasila perlu juga menjiwai ajaran-ajaran Bung Karno. Mengagung-agungkan Pancasila, tetapi  menolak ajaran-ajaran revolusioner Bung Karno, adalah omong kosong besar atau kemunafikan. Tidak bisa lain.

Hari lahir Bung Karno tanggal 6 Juni

Hari Lahir Bung Karno tanggal 6 Juni perlu diperingati oleh para pencintanya dan pendukung berbagai politiknya yang revolusioner dan pro-rakyat kecil, setiap tahun terus-menerus, sampai generasi-generasi yang akan datang.

Bung Karno, lahir sebagai putera bangsa Indonesia yang paling besar ketokohannya dalam berbagai bidang, dan paling agung jiwanya sebagai pemersatu bangsa. Tidak ada pemimpin Indonesia lainnya yang bisa menyamai kebesaran Bung Karno dalam hal perjuangan untuk bangsa dan pengabdian kepada rakyat, seperti yang terkandung dalam Bhinneka Tunggal Ika, dalam konsepsinya gotong-royong, dalam gagasannya yang besar yang dirumuskannya dengan Nasakom, dalam cita-citanya masyarakat adil dan makmur atau sosialisme à la Indonesia.

Memperingati Hari Lahir Bung Karno dan mengenang apa yang diperjuangkannya sampai hari wafatnya, adalah sama dengan menimba inspirasi perjuangan untuk menempuh jalan revolusi yang terus-menerus dan tanpa henti menuju masyarakat adil dan makmur.

Memperingati Hari Lahir Bung Karno dan berusaha menjiwai perjalanan perjuangannya sejak muda adalah pendidikan politik yang amat penting bagi seluruh kekuatan di negeri kita yang mau berjuang demi kepentingan rakyat banyak dan demi Indonesia Baru.



Hari wafatnya Bung Karno 21 Juni

Dalam memperingati wafatnya Bung Karno pada tanggal 21 Juni 1970 perlu diperingati juga tindakan yang tidak manusiawi (artinya, ma’af saja : biadab !) dari Suharto beserta konco-konconya, yang membiarkan Bung Karno menderita sakit parah selama tahanan, tanpa perawatan dan pengobatan yang seperlunya. Ini adalah satu rentetan yang mempunyai hubungan erat satu sama lainnya.

Perlakuan pimpinan Angkatan Darat (waktu itu) terhadap Bung Karno sesudah ia digulingkan dari kedudukannya sebagai Presiden RI adalah sedemikian buruknya, sehingga makin jelaslah pengkhianatan mereka terhadap bapak bangsa yang jasa-jasa perjuangannya tidak ada taranya itu.

Berbagai cerita mengenai wafatnya Bung Karno perlu sesering mungkin disajikan kembali, sebagai bukti dan juga sebagai pelajaran politik  bagi seluruh bangsa (beserta generasi-generasi yang akan datang). Untuk menunjukkan bahwa kejahatan pimpinan militer di bawah Suharto tidak saja sudah membunuhi dan memenjarakan jutaan orang tidak bersalah apa-apa, melainkan juga « membunuh », dengan cara-cara sadis pula, Panglima Tertinggi mereka yang sudah sakit parah dalam tahanan.

Bulan Mei dan Juni yang menggugah  semangat



Dari uraian yang serba singkat dan berisi pokok-pokok masalah seperti tersebut di atas, maka dapat dikatakan bahwa banyak peristiwa-peristiwa bersejarah bangsa kita akan diperingati dalam bulan Mei dan Juni yang akan datang.

Dalam situasi negara dan bangsa yang morat-marit di banyak bidang kehehidupan, terutama sekali karena kebejatan moral dan kerusakan iman atau pembusukan rochani sebagian besar kalangan-kalangan  (terutama « kalangan atas ») maka memperingati hari-hari bersejarah bangsa adalah penting sekali.

Dalam bulan Mei dan Juni yang akan datang akan kita kenang kembali pentingnya PKI dan Bung Karno dalam sejarah bangsa, yang dua-duanya dicoba untuk dihapus atau dihitamkan oleh rejim militer Suharto, dengan berbagai cara, termasuk cara-cara yang paling keji, kotor, jahat, dan tidak manusiawi. Dosa-dosa Suharto beserta konco-konconya itu tidak akan, dan tidak bisa, atau tidak boleh dilupakan oleh siapa pun yang menghormati keadilan

Bung Karno dan PKI adalah satu jiwa, dalam hal pengabdian kepada persatuan bangsa, kepada kepada keluhuran martabat bangsa. Bung Karno dan PKI adalah kawan seperjuangan  dalam melawan imperialisme dan kolonialisme dan kapitalisme reaksioner. Bung Karno adalah salah satu dari pemimpin-pemimpin Indonesia yang paling mengerti tentang besarnya sumbangan PKI bagi perjuangan bangsa.

Semua ini dapat ditelusuri atau dijiwai dengan menyimak tulisannya ketika ia masih umur 26 tahun « Nasionalisme, Islamisme dan Marxisme », atau pidatonya tentang lahirnya Pancasila, atau seluruh gagasannya tentang Nasakom. Dengan membaca dan mendalami dan menjiwai ( !)  isi buku « Dibawah Bendera Revolusi » dan « Revolusi Belum selesai », maka nyatalah bagi banyak orang Indonesia, bahwa Bung Karno adalah satu-satunya tokoh yang paling besar dan betul-betul berhak menjadi panutan bangsa.

Dalam pengertian yang luas, Bung Karno adalah pengejowantahan Pancasila. Seluruh perjuangannya adalah manifestasi yang gamblang dari Bhinneka Tunggal Ika. Seperti dikatakan oleh Subadio Sastrosatomo, mantan dedengkot PSI, « Sukarno adalah Indonesia, dan Indonesia adalah Sukarno ».

Ketika dewasa ini  terasa ada kekosongan kepemimpinan nasional yang mempunyai integritas moral dan politik yang tinggi, dan ketika di Indonesia sekarang ini patriotisme kerakyatan sudah luntur atau mulai hilang, dan  ketika cita-cita masyarakat adil dan makmur sudah dikentuti saja oleh para pembesar dan tokoh partai dan « wakil-wakil rakyat » maka memperingati hari-hari bersejarah bangsa (seperti yang tersebut di atas) adalah seperti mendengar seruan Bung Karno untuk melanjutkan revolusi yang belum selesai.
