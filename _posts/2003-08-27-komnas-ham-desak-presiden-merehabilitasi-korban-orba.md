---
layout: post
title: Komnas HAM desak Presiden merehabilitasi korban Orba
date: 2003-08-27
---

Surat ketua Mahkamah Agung, Bagir Manan, kepada Presiden Megawati yang berisi rekomendasi untuk merehabilitasi para korban peristiwa ’65 merupakan terobosan penting dan bersejarah di bidang hukum. Terobosan ini diperkuat oleh langkah penting lainnya, yaitu surat yang dikirimkan Ketua Komisi Nasional Hak Asasi Manusia, Abdul Hakim Garuda Nusantara, kepada Presiden Megawati mengenai soal yang sama. Mengenai soal ini harian Kompas tanggal 26 Agustus 2003 memberitakan sebagai berikut :

« Komisi Nasional Hak Asasi Manusia (Komnas HAM) mendesak Presiden Megawati Soekarnoputri segera merehabilitir korban Gerakan 30 September Partai Komunis Indonesia (G30S PKI). "Mereka tidak pernah diputus bersalah oleh pengadilan dan terlalu lama menanggung beban penderitaan sebagai akibat perlakuan yang diskriminatif oleh rezim Orde Baru. Selain itu, anak cucu mereka juga harus menanggung beban dosa politik turun-temurun. Padahal, mereka tidak tahu sama sekali peristiwa tersebut." Demikian surat Komnas HAM untuk Presiden, 25 Agustus 2003, yang ditandatangani Ketua Komnas HAM Abdul Hakim Garuda Nusantara.

Komnas HAM telah menerima pengaduan dari Paguyuban Korban Orba. Mereka meminta Komnas HAM membantu memperjuangkan rehabilitasi bagi korban G30S PKI. "Berdasar UUD 1945 hasil amandemen disebutkan, presiden memberi grasi dan rehabilitasi dengan pertimbangan Mahkamah Agung. Dalam soal ini, Mahkamah Agung telah mempertimbangkan memberi rehabilitasi terhadap korban G30S PKI lewat surat 12 Juni 2003," tulis Hakim.

Menurut Hakim, perlakuan diskriminatif serta pembebanan dosa kolektif terhadap keturunan mereka merupakan tindakan tidak adil dan melanggar HAM. Untuk itu, pemerintah harus dengan segera memberi rehabilitasi dan kompensasi agar mereka dapat menjalani serta menikmati kehidupan mereka dengan tenang dan damai. Hal ini sejalan dengan Pasal 3 Undang-Undang Nomor 39 Tahun 1999 tentang HAM yang berbunyi, Setiap orang berhak atas pengakuan, jaminan, perlindungan, dan perlakuan hukum yang adil serta mendapat kepastian hukum dan perlakuan yang sama di depan hukum » (kutipan selesai)

## Presiden Megawati Perlu Didorong Terus

Surat Ketua Komnas HAM dan ketua Mahkamah Agung kepada Presiden Megawati soal rehabilitasi korban peristiwa 65 adalah perkembangan yang penting dan menggembirakan bagi para korban Orde Baru pada umumnya dan para korban peristiwa ’65 pada khususnya. Setidak-tidaknya, perkembangan ini memberikan harapan bahwa banyak penderitaan yang dihadapi oleh para korban peristiwa 65 dapat sedikit demi sedikit atau setapak demi setapak dapat dikurangi. Harapan ini didasarkan pada kenyataan bahwa Mahkamah Agung adalah lembaga hukum dan peradilan yang tertinggi, sedangkan Komnas HAM adalah lembaga tingkat nasional yang khusus banyak mengurusi masalah-masalah HAM.

Sekarang kita semua sedang menunggu keputusan Presiden Megawati dalam menggunakan hak prerogatifnya untuk mengadakan langkah-langkah menuju rehabilitasi para korban 65 itu. Sebenarnya, Presiden Megawati sudah tidak perlu ragu-ragu lagi, atau kuatir dan takut-takut untuk menggunakan hak prerogatifnya merehabilitasi para korban peristiwa 65. Surat rekomendasi dari Mahkamah Agung dan Komnas HAM, ditambah dengan saran kepada Presiden yang diajukan oleh MPR tentang rehablilitasi Bung Karno merupakan dukungan politik, moral dan hukum yang cukup kuat kepada langkah-langkah yang akan diambil Presiden.

Untuk mendorong Presiden Megawati menggunakan hak prerogatifnya dan memenuhi anjuran Mahkamah Agung, Komnas HAM, MPR dan mengabulkan tuntutan berbagau Ornop/LSM, maka gerakan besar-besaran perlu dilancarkan oleh kitta semua. Presiden Megawati perlu diingatkan, melalui berbagai cara dan beraneka saluran, bahwa rehabilitasi para korban 465 adalah tidak hanya untuk kepentingan mereka para korban saja, melainkan juga untuk kepentingan seluruh bangsa. Dan bukan hanya untuk menyelesaikan persoalan-persoalan masa kini saja – yang ada sangkut-pautnya yang erat dengan masa lampau – melainkan juga untuk menyongsong hari depan bangsa.

## Jangan Kecewakan Harapan Banyak Orang

Presiden Megawati perlu terus-menerus diingatkan bahwa banyak orang mengharapkan sekali adanya langkah kongkrit mengenai rehabilitasi para korban Orde Baru ini. Sekarang sudah tercipta syarat-syarat atau suasana yang lebih baik dari pada yang sudah-sudah untuk merealisasikannya. Kesempatan emas ini tidak boleh dilewatkan begitu saja atau disia-siakan. Harapan yang begitu besar banyak orang tidak boleh dikecewakan. Sebab, dampak kekecewaan yang kali ini akan bisa berakibat jauh dan panjang. Antara lain, kekecewaan yang kali ini bisa mempengaruhi hasil pemilu yang akan datang, baik pemilihan presiden maupun legislatif.

Selama ini sudah banyak kekecewaan atau ketidakpuasan yang dinyatakan berbagai kalangan (termasuk mereka yang biasanya dikategorikan sebagai pendukungnya) terhadap berbagai kebijaksanaan Ibu Megawati, baik dalam kwalitasnya sebagai presiden maupun sebagai pimpinan PDI-P. Seperti yang sama-sama kita saksikan sendiri, sebagian dari kekecewaan ini ada dasarnya atau alasannya, sedangkan yang sebagian lagi tidaklah berdasar sama sekali.

Kalau Presiden Megawati kali ini tidak mau - atau tidak berani - menggunakan hak prerogatifnya untuk merehabibiltasi para korban Orde Baru, banyak orang bukan saja akan kecewa tetapi juga akan tidak mengerti. Sebab, masalah korban peristiwa 65 adalah masalah pelanggaran HAM besar-besaran dan parah yang sudah berlangsung lama sekali, yaitu 38 tahun. Entah berapa puluh juta orang yang selama ini sudah menderita (dan ikut menderita,) karena perlakuan tidak berperi-kemanusiaan rezim militer Orde Barunya Suharto dkk ini. Ditambah lagi, banyak di antara para korban peristiwa 65 itu adalah pendukung politik Bung Karno.

Dalam rangka membongkar pengkhianatan Orde Baru terhadap Bung Karno ini pulalah banyak orang mengharapkan bahwa Presiden Megawati memenuhi saran atau anjuran MPR untuk merehabilitasi nama baik Bung Karno.

## Perlu Dukungan Luas

Meskipun Presiden Megawati memiliki kemungkinan - yang sah - untuk menggunakan hak prerogatifnya guna merehabilitasi para korban peristiwa 65 tetapi dukungan yang luas dari opini publik tetap merupakan elemen positif yang diperlukan. Tidak saja semua organisasi yang selama ini memperjuangkan rehabilitasi para korban Orde Baru perlu « bersuara », melainkan juga seluruh kekuatan demokrasi. Berbagai aksi atau kegiatan yang dilakukan terus-menerus oleh beraneka-ragam kalangan bisa merupakan dukungan sekaligus peringatan bagi Presiden Megawati.
Berbagai aksi atau kegiatan juga bisa dilakukan di bidang internasional, dengan memobilisasi opini publik luarnegeri lewat organisasi-organisasi di berbagai negeri. Dukungan opini publik internasional terhadap rehabilitasi para korban peristiwa 65 juga merupakan dorongan untuk Presiden Megawati. Kadang-kadang, tekanan internasional merupakan faktor yang penting. (Umpamanya, antara lain : Komisi HAM PBB di Jenewa, parlemen Eropa, International Human Rights Watch dll..

Kalau (sekali lagi : kalau !) seandainya Presiden Megawati toh tidak mau (atau tidak berani) menggunakan hak prerogatifnya, sedangkan syarat-syaratnya sudah tersedia, maka ini merupakan bukti bahwa masalah pelanggaran HAM besar-besaran yang sudah dilakukan oleh Orde Baru selama puluhan tahun ini tidak dihiraukannya. Kalau (sekali lagi : kalau ) sudah demikian maka makin jelaslah sikap apa yang harus diambil oleh banyak orang terhadap Presiden Megawati

## Untuk Hari Depan Bangsa

Dengan adanya kedua surat ketua Mahkamah Agung dan ketua Komnas HAM kepada Presiden Megawati , ditambah dengan anjuran MPR untuk merehabilitasi nama baik Bung Karno, maka perjuangan untuk merehabilitasi para korban peristiwa 65 memasuki tahap baru. Bolehlah dikatakan bahwa fikiran yang membenarkan pelanggaran HAM besar-besaran itu sekarang sedang mengalami kemunduran, atau posisinya menjadi defensif. Makin jarang (atau makin sedikit) orang yang berani terang-terangan membela politik rezim militer Orde Baru terhadap para korban peristiwa 65 ini.

Perkembangan ini adalah wajar. Sebab, makin jelaslah bagi banyak orang bahwa berbagai politik rezim militer Orde Baru adalah politik yang salah, termasuk politik terhadap para eks-tapol, yang meruapakan kesalahan yang monumental. Bangsa kita tidak boleh meneruskan berlangsungnya kesalahan besar ini. Sedikipun tidak ada keuntungan bagi bangsa kita untuk melanggengkan ketidakadilan ini.

Perlakuan terhadap para korban 65 yang sudah berlangsung 38 tahun ini harus diakhiri, dan makin cepat makin baik. Sebab, mereka sudah terlalu lama diperlakukan tidak adil dan sewenang-wenang .
Rehabilitasi para korban peristiwa 65 (besertra keluarga mereka) akan membawa kebaikan untuk kehidupan baru bangsa, yang selama puluhan tahun telah dikoyak-koyak oleh rasa permusuhan yang terpendam, atau rasa dendam yang tersembunyi. Rehabilitasi ini juga akan bisa memudahkan untuk tergalangnya rekonsiliasi nasional, sebagai syarat penting untuk persatuan dan kesatuan bangsa, yang sangat dibutuhkan dewasa ini untuk menghadapi berbagai kesulitan besar yang sedang kita hadapi bersama.

Dengan semangat yang demikianlah tulisan ini disajikan, untuk mendukung surat Ketua Mahkamah Agung dan Ketua Komnas HAM kepada Presiden Megawati.
