---
layout: post
title: Saksi bisu dari pulau Buru
date: 2004-08-10
---

“Bung Umar yth.

Sesuai dengan anjuran salah satu tulisan yang dimuat dalam Web yang Bung asuh, saya sudah menyelesaikan tulisan pengalaman saya selama dalam tahanan mulai dari Jawa, Nusakambangan sampai Pulau Buru. Masalahnya saya tidak tahu penerbit mana yang bisa menerbitkan naskah buku tersebut. Judulnya "Saksi Bisu dari Pulau Buru" setebal kurang lebih 200 halaman.

Barangkali Bung bersama kawan-kawan bisa memberikan rekomendasi kepada penerbit mana yang bisa saya hubungi?

Sekian dan terimakasih.

Suparman

e-mail: hamirsyah@yahoo.com
Phone Office 0265-328190 - 0265-332492
Home: 0265-342296”

---

E-mail tersebut di atas adalah yang saya terima dari seorang teman lama, Bung Suparman, yang pernah menjadi pimpinan suratkabar Warta Bandung. Sebagai akibat terjadinya G30S, penerbitannya telah ditutup oleh penguasa militer di bawah pimpinan Suharto, dan kemundian ia ditahan di penjara Bandung selama 5 tahun, dan di pulau Buru selama 8 tahun, dengan transit di Nusakambangan selama 3 bulan.

Sekembalinya dari pulau Buru, ia merintis didirikannya sebuah lembaga pendidikan dan pada tahun 1984 berdirilah sebuah lembaga pendidikan non formal yang diberi nama Institut Teknologi Komputer Indonesia. Institut ini pada tahun 1987 bergabung dengan Data Computer Institut Bandung dan sekarang sudah menjadi sebuah perguruan tinggi komputer dan informatika yang disebut STMIK-DCI.

Surat Bung Suparman saya sajikan dalam tulisan kali ini, dalam rangka mengingatkan kembali kepada para korban Orde Baru tentang betapa pentingnya untuk berusaha menulis atau menceritakan tentang berbagai pengalaman dan penderitaan mereka, baik yang di masa lalu maupun yang sekarang. Juga untuk menyerukan kepada semua kalangan atau golongan dalam masyarakat yang anti rezim militer Suharto untuk menaruh perhatian dan membantu berbagai kegiatan para eks-tapol dan para keluarga korban Orde Baru dalam memperjuangkan rehabilitasi dan menuntut keadilan.

## Korban Orde Baru Adalah Masalah Besar Bangsa

Masalah korban Orde Baru (terutama akibat pembunuhan besar-besaran 65 dan penahanan begitu banyak tapol) adalah aib besar dan dosa berat dalam sejarah bangsa Indonesia. Begitu besarnya aib dan begitu beratnya dosa ini, sehingga tidak patutlah kiranya kalau kita tidak peduli sama sekali terhadapnya. Apalagi, kalau ada golongan yang malahan senang dengan adanya aib besar dan dosa berat yang memalukan bangsa ini. Selama persoalan korban pembunuhan besar-besaran dan pemenjaraan begitu banyak orang tidak bersalah ini tidak dikutuk oleh bangsa, maka aib besar dan dosa berat ini akan tetap terus meracuni kehidupan generasi sekarang dan juga generasi-generasi yang akan datang. Selama itu, bangsa kita tidak bisa (dan tidak berhak atau tidak pantas!) menepuk dada sambil berteriak lantang menyatakan diri sebagai bangsa yang beradab, dan yang menjunjung tinggi-tinggi Pancasila pula.

Berbagai macam penderitaan berkepanjangan yang dialami keluarga korban pembantaian 65 dan keluarga eks-tapol adalah persoalan bangsa yang serius sekali, karena berkaitan dengan puluhan juta jiwa warganegara Republik Indonesia. Selama hampir 40 tahun berpuluh-puluh juta orang ini telah mengalami perlakuan secara tidak manusiawi atau tidak adil, baik selama rezim militer Suharto berkuasa maupun selama berbagai pemerintahan yang berikutnya (Habibi, Abdurrahman Wahid, Megawati), bahkan sampai sekarang.

## Kejahatan Orde Baru Perlu Terus Diblejedi

Bangsa kita perlu mengetahui dengan lebih jelas lagi bahwa rezim militer Suharto telah membuat banyak sekali kejahatan besar terhadap banyak orang selama 32 tahun. Berbagai kesalahan besar atau dosa berat Orde Baru (yang tulang-punggungnya adalah sebagian pimpinan ABRI dan Golkar) tidak boleh terjadi lagi, dalam bentuknya yang bagaimanapun juga (!!!). Untuk itu berbagai kegiatan untuk terus-menerus membongkar segala aspek negatif rezim militer Suharto tetap perlu dikerjakan oleh sebanyak mungkin kalangan dan golongan. Ini penting sebagai pendidikan politik dan pendidikan moral bangsa. Memblejedi kesalahan berat dan kejahatan besar rezim militer Suharto adalah amat penting untuk usaha kita bersama dalam membangun bangsa yang beradab.

Dalam rangka ini pulalah terasa sekali betapa sangat perlunya bagi para korban Orde Baru (keluarga korban pembantaian 65 dan eks-tapol) untuk terus “bersuara” dan melakukan berbagai kegiatan untuk selalu mengangkat berbagai penderitaan yang telah mereka alami pada masa-masa yang lalu, dan yang masih terus mereka alami masa kini. “Suara” mereka adalah sangat perlu untuk mengingatkan seluruh bangsa bahwa – sekali lagi, sampai sekarang ini ! - ada puluhan juta orang yang masih tetap terus menanggung segala macam penderitaan (termasuk penderitaan bathin) akibat berbagai kesalahan dan kejahatan rezim militer Orde Baru.

Artinya, suara para korban Orde Baru adalah perlu sekali dikumandangkan demi kebaikan seluruh bangsa, bukan hanya demi kepentingan para korban beserta para keluarga mereka saja. Suara mereka merupakan sumbangan yang berharga sekali dalam mengkoreksi kesalahan serius pada masa lalu, guna membersihkan kehidupan bangsa dan negara dewasa ini, dan di kemudian hari. Pembangunan kehidupan bangsa yang baik tidaklah mungkin dilaksanakan tanpa mengkoreksi kesalahan serius pada masa lalu, atau tanpa membongkar segala kejahatan besar yang dilakukan oleh rezim militer Suharto. Dalam kalimat lain, membongkar berbagai kejahatan Orde Baru adalah syarat mutlak untuk menyongsong hari depan yang lebih baik bagi kita semua sebagai bangsa.

## Sumbangan Untuk Generasi Yang Akan Datang

Oleh karena itu, kita semua perlu menyambut dengan gembira adanya berbagai kegiatan dari para korban Orde Baru, yang tergabung dalam macam-macam organisasi non-pemerintah, paguyuban, yayasan, atau lembaga (antara lain : Pakorba, LPR-KROB, LPKP, YPKP dll). Kegiatan mereka itu amatlah penting untuk terus memupuk kesadaran bahwa rezim militer Suharto adalah rezim - yang secara parah dan secara luas, ditambah dalam jangka waktu yang lama sekali pula ! - telah merusak atau membusukkan sendi-sendi penting kehidupan bangsa, Di antara berbagai kegiatan itu termasuk penulisan pengalaman dan penderitaan mereka di masa lalu sebagai korban, seperti yang sedang dikerjakan oleh Bung Suparman.

Tetapi, meskipun sudah mulai banyak tulisan atau buku-buku yang dibuat oleh para korban Orde Baru, namun jumlahnya masih terlalu sedikit sekali ( sekali lagi : terlalu sedikit sekali!) kalau dibandingkan dengan besarnya persoalan dan seriusnya penderitaan yang mereka alami selama hampir 40 tahun. Untuk kebaikan seluruh bangsa perlulah kiranya para korban Orde Baru membeberkan sebanyak mungkin dan seluas mungkin pengalaman mereka. Pengalaman jutaan para korban Orde Baru beserta keluarga mereka adalah gudang dokumen sejarah atau dokumen sosial yang sangat berharga sekali bangsa kita. Generasi kita yang akan datang akan bisa menarik pelajaran penting dari tulisan-tulisan itu, dan, karenanya, akan berterimakasih kepada para korban Orde Baru atas sumbangan mereka yang berharga ini.

Bagi para korban Orde Baru sendiri (dan keluarga mereka) menuliskan pengalaman mereka juga amat perlu. Bukan saja untuk sekadar melampiaskan uneg-uneg yang sudah terpendam selama puluhan tahun, melainkan juga sebagai sumbangan kepada seluruh bangsa dalam usaha mulia menegakkan rasa keadilan dan peri-kemanusiaan. Dan, bukan saja untuk sekadar berkeluh-kesah tentang masa lalu yang penuh dengan berbagai penderitaan mereka yang berkepanjangan, melainkan untuk ikut serta dalam perjuangan melawan sisa-sisa Orde Baru dalam segala bentuknya. Jelaslah kiranya bagi kita semua bahwa perjuangan para korban Orde Baru adalah benar, adil, sah dan mulia.

## Mengutuk Ketidak-Adilan Adalah Wajib

Sebagai golongan yang telah diperlakukan secara sewenang-wenang – dan dalam jangka puluhan tahun ! – mereka berhak dan bahkan wajib “angkat suara” untuk mengutuk ketidak-adilan yang telah dilakukan secara besar-besaran. Tidak mengutuk berbagai kejahatan rezim militer Suharto adalah sikap yang salah, baik secara politik maupun secara moral. Suara para korban Orde Baru yang menuntut adanya rehabilitasi adalah suara yang mencerminkan kemauan atau keinginan untuk terciptanya rekonsiliasi. Rekonsiliasi yang berdasarkan keadilan dan kebenaran.

Setelah dibungkam selama lebih dari 32 tahun, sejak jatuhnya kekuasaan Suharto 6 tahun yang lalu, para korban Orde Baru akhirnya mulai bisa “angkat suara” sedikit demi sedikit. Tetapi, karena indoktrinasi yang dicekokkan secara intensif - dan secara besar-besaran pula - selama puluhan tahun, dampaknya masih terasa besar sekali dalam masyarakat. Sebagian golongan dalam masyarakat masih terus menganggap para korban Orde Baru sebagai “orang-orang bersalah” dan karenanya “pantas mendapat perlakuan yang setimpal”. Di samping itu, banyak tokoh-tokoh di kalangan birokrasi, di kalangan lembaga-lembaga penting negara (Mahkamah Agung, Kejaksaan Agung, DPR, dll), di kalangan intelektual, dan di kalangan agama, yang masih bermental Orde Baru. Karena itulah perjuangan para korban Orde Baru untuk memperoleh rehablitasi menjadi tidak mudah dan menghadapi banyak rintangan.

Namun, walaupun menghadapi banyak kesulitan, perjuangan para korban Orde Baru perlu diteruskan, demi tercapainya keadilan dan pulihnya hak-hak mereka yang sah dan penuh sebagai manusia dan sebagai warnegara RI. Perjuangan para korban Orde Baru untuk tegaknya hukum dan keadilan adalah juga untuk kepentingan seluruh bangsa, termasuk untuk generasi yang akan datang. Dan salah satu di antara banyak cara dalam perjuangan besar ini adalah “bersuara” melalui berbagai tulisan megenai pengalaman mereka.

## Bantuan Dan Dorongan Bagi Para EKS-Tapol

Dalam rangka ikut mendukung inisiatif para korban Orde Baru (atau eks-tapol) ini patutlah kiranya kita semua menaruh perhatian yang selayaknya kepada masalah yang sedang dihadapi oleh Bung Suparman. Ia ingin minta bantuan untuk bisa dihubungkan dengan para penerbit yang bersedia menerbitkan naskahnya.

Kepada para pembaca tulisan ini diharapkan bantuannya untuk menyumbangkan fikiran dan usul-usul, yang bisa disampaikan secara langsung kepadanya melalui E-mail atau telpon yang tercantum pada permulaan tulisan ini.

Bantuan yang diberikan kepadanya adalah salah satu manifestasi kepedulian dari kita semua terhadap perjuangan para korban Orde Baru dalam menegakkan hukum dan keadilan, demi kebaikan kita bersama sebagai bangsa.
