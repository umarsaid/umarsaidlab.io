---
layout: post
title: Diperlukan Lahirnya Kekuatan Politik Alternatif yang Revolusioner
date: 2011-08-06
---

Seperti yang sama-sama kita saksikan, negara  dan bangsa Indonesia sekarang ini sedang menghadapi masa-masa yang penuh gejolak.  Banyak sekali orang tidak bisa membayangkan atau mengira-ira apa saja yang mungkin terjadi dalam waktu dekat ini. Buntut kasus Nazaruddin dan Anas Urbaningrum masih panjang dan sudah membikin gonjang-ganjing besar politik yang luar biasa dahsyatnya.

Kehormatan dan kewibawaan Presiden SBY sudah anjlog dalam sekali. Dalam sejarah Republik Indonesia hanya  SBY dan Suharto-lah yang merupakan presiden kepala negara yang menjadi bulan-bulanan kutukan yang begitu hebat. Pemerintah sudah kelihatan loyo, tidak berdaya, dan kacau menghadapi merajalelanya korupsi serta  pelecehan hukum oleh segala macam mafia di banyak bidang.

DPR yang terdiri dari wakil partai-partai, sudah tidak dipercayai lagi oleh publik, karena banyaknya persoalan-persoalan, yang mencerminkan betapa rusaknya moral di kalangan anggota-anggotanya. Contohnya adanya 44 (bahkan lebih)  anggota DPR yang sedang menghadapi pemeriksaan hukum karena berbagai pelanggaran.

Partai-partai sudah menjadi pelindung atau benteng para koruptor yang menggunakan BUMN sebagai sapi perah untuk memperkaya diri dan memakai APBN dan ABPD sebagai sumber dana haram partai.  Kareanya, pemilu-pemilu selalu menjadi kesempatan untuk mencuri berjemaah dan mengumbar chotbah-chotbah palsu yang penuh dengan janji-janji kosong.

Kerusakan akhlak juga banyak sekali terjadi di kalangan tinggi kepolisian, kejaksaan dan peradilan. Dari yang sudah ketahuan saja, ada lebih dari 30 bupati di antara  33 propinsi ditindak karena korupsi, dan sejumlah gubernurpun sedang diperiksa. Bahkan, banyak suara-suara negatif juga dilontarkan kepada petinggi-petinggi KPK (antara lain Chandra Hamzah dan Ade Rahardja) .



Partai Demokrat yang busuk dan bobrok

Kebobrokan atau kebusukan Partai Demokrat kelihatan telanjang bulat-bulat karena munculnya berbagai kasus yang berkaitan dengan perkara Nazarrudin, Anas Urbaningrum, Andi Nurpati, dan kongres Partai Demokrat di Bandung, projek  Wisma Atlet di Palembang dan kompleks olahraga Hambalang. Kebobrokan Partai Demokrat ini sudah  menjadi pembicaraan ramai di kalangan  masyarakat.

Dari yang sudah diberitakan oleh pers dan televisi, maka banyak bukti atau indikasi bahwa Partai Demokrat, yang merupakan partai terbesar, dan memegang kekuasaan,  ternyata adalah partai yang tokoh-tokoh utamanya  paling korup, paling tidak jujur, paling munafik.  Tidak hanya Anas Urbaningrum atau Nazaruddin saja yang  demikian itu, melainkan banyak juga lainnya. (Kita semua akan mengetahuinya lebih jelas dan lebih banyak di kemudian hari)

Apa yang terjadi di kalangan Partai Demokrat adalah cermin bahwa negara kita memang sudah  memasuki pancaroba yang besar sekali. Sebab, segala macam kebobrokan dan kebusukan itu tidak hanya terdapat di Partai Demokrat saja, melainlan juga di partai-partai lainnya, walaupun dalam skala yang berbeda-beda dan jenis yang macam-macam.

Keadaan yang demikian buruk di kalangan partai-partai politik koalisi yang mendukung pemerintahan SBY menimbulkan pesimisme bahwa seandainya tsunami yang terjadi di Partai Demokrat mengakibatkan tergulingnya pemerintahan SBY dan digantikan oleh pemerintahan lainnya, maka tidak akan terjadi perubahan-perubahan besar.
Sebab, besar kemungkinan bahwa pemerintahan yang begitu itu juga akan terdiri dari partai-partai yang sekarang ini ikut membikin pemerintahan yang korup, yang menjalankan berbagai politik yang tidak pro-rakyat, dan yang pro neo-liberalisme.

Keadaan yang serba rusak akan berlangsung lama, kalau ….

Keadaan yang demikian itu memberikan gambaran bahwa situasi ambrul-adul, bobrok, dan serba rusak seperti yang kita saksikan dewasa ini, masih akan terus berlansung lama sekali, selama masih belum bisa didirikannya pemerintahan yang sama sekali baru, dan yang menjalankan politik yang berbeda sama sekali dari pemerintahan-pemerintahan sejak Orde Baru sampai sekarang.

Jadinya, kita bisa bayangkan betapa banyaknya rakyat kita yang harus tetap menderita berkepanjangan, akibat  terus merajalelanya  korupsi dan pelecehan hukum dan berbagai kejahatan kalangan elite. Penderitaan ratusan  juta orang akibat kebejatan akhlak para tokoh-tokoh ini adalah kejahatan besar sekali, yang tidak bisa dima’afkan sedikitpun oleh orang-orang  yang berperikemanusiaan.

Kasus para tokoh Partai Demokrat yang terkait dengan korupsi besar-besaran sampai ratusan miliar rupiah (bahkan mungkin sampai  triliunan) , seperti kasus Nazaruddin dan Anas Urbaningrum adalah sebagian  saja dari borok-borok parah dan busuk yang sedang membikin sakit negara dan bangsa kita sekarang ini.

Masalah Anas Urbaningrum merupakan contoh yang paling tipikal untuk dipakai sebagai tolok ukur tentang betapa rusaknya ketokohan pimpinan partai-partai politik kita dewasa ini. Ia adalah Ketua Umum Partai Demokrat, yang dalam pemilu yang lalu mendapat suara lebih dari 20%   (150 kursi dalam parlemen), jauh lebih banyak dari pada partai-partai besar lainnya (Golkar 14 % dan PDIP juga 14%).

Menurut Nazaruddin, untuk bisa menjadikan Anas Urbaningrum sebagai  Ketua Umum Partai  Demokrat dalam kongresnya di Bandung dalam bulan Mei 2010 telah dihabiskan uang haram sebanyak 20 juta US dollar atau 170 miliar Rupiah. Sebagian dari uang ini adalah hasil « permainan » dalam  projek pusat olahraga Hambalang , sebesar 50 miliar.

Sebagai Bendahara Umum Partai Demokrat, Nazarrudin yang masih muda ini (33 tahun) telah mengumpulkan dana (termasuk yang haram ) dengan  macam-macam cara. Dari segala cerita tentang Nazaruddin yang paling menarik dan sekaligus paling mengherankan adalah hubungannya dan kedekatannya (tadinya) dengan Ketua Umumnya , Anas Urbaningrum.

Kedua tokoh Partai Demokrat ini sudah berteman erat sekali sejak bertahun-tahun  sebelum Anas Urbaningrum menjadi Ketua Umum. Mereka berdua sama-sama menjadi pimpinan perusahaan (antara lain : PT Anugerah Nusantara), sering saling bertemu dan makan bersama, bikin baju sama-sama, bahkan pergi mandi di sauna sama-sama. Nazaruddin adalah kawan terdekat dan terpercaya Anas Urbaningrum. Karena itu banyak sekali uang « gelap » dalam jumlah  besar-besar sudah mereka tangani berdua secara bersekongkol.

Hubungan kedua tokoh ini rusak, bahkan kemudian menjadi permusuhan yang sengit sekali setelah muncul kasus korupsi dalam projek Wisma Atlet untuk SEA Games di Palembang. Setelah Nazaruddin mengetahui bahwa akan dicekal oleh KPK, maka ia kabur dengan keluarganya ke Singaura pada tanggal 23 Mei.  Dan sejak itu ia telah membongkar segala kebobrokan Partai Demokrat melalui Black  Berry Message, yang menghebohkan  khalayak ramai.

Pembongkaran kebobrokan Partai Demokrat ini kemudian lebih jelas lagi dengan adanya telewicara melalui Skype antara Nazaruddin dengan Iwan Piliang.  Lebih-lebih lagi, dengan, pengakuan atau kesaksian 2 sopir Nazaruddin  dan 2 pengawal  yang mengangkut (dengan 4 mobil) ratusan milyar Rupîah untuk kongres Partai Demokrat di Bandung  maka kejahatan besar yang dilakukan oleh Nazaruddin dan Anas Urbaningrum makin sulit untuk ditutup-tuttupi lagi.

Kerusakan moral menghebat selama di bawah kepemimpinan SBY

Meskipun banyak hal yang masih harus diselidiki lebih lanjut, namun apa yang sudah dibongkar selama ini menunjukkan  dengan jelas bahwa Partai Demokrat ternyata sama sekali bukanlah partai yang bisa diharapkan untuk mendatangkan kebaikan bagi bangsa dan negara. Di bawah kepemimpinan SBY sebagai  Ketua Dewan Pembina, partai ini justru telah mendatangkan kemunduran dan kerusakan moral secara besar-besaran dan luas sekali di berbagai bidang.

Kerusakan dan pembusukan ini sudah sedemikian parahnya dan luasnya, sehingga adalah hanya ilusi atau mimpi kosong saja kalau ada orang atau kalangan yang mengharapkan adanya perbaikan atau perubahan. SBY adalah sosok yang telah gagal sebagai presiden dan pemimpin negara dan partai politik, walaupun ia telah mendapat suara lebih dari 60% dalam pemilihan presiden yang lalu.

Itulah sebabnya maka makin banyak suara yang dilontarkan oleh banyak sekali kalangan, yang menuntut mundurnya SBY dari jabatannya sebagai presiden. Bermacam-macam demonstrasi sudah dilancarkan oleh ormas pemuda dan mahasiswa di banyak kota, dan berbagai ragam aksi telah digelar oleh kaum buruh, tani, seniman dan intelektual.

Banyaknya macam-macam gerakan dan aksi  (dan suara-suara lewat pers dan televisi) oleh berbagai golongan masyarakat ini menunjukkan bahwa kesedaran politik rakyat  sudah makin meningkat tinggi. Mereka sudah tidak mempercayakan masalah-masalah penting bangsa dan negara hanya kepada DPR atau DPRD saja , yang terdiri dari tokoh-tokoh partai yang kebanyakannya adalah justru orang-orang yang sama sekali tidak pantas disebut mewakili rakyat.

Pentingnya kelahiran kekuatan politik alternatif

Makin berkembangnya berbagai gerakan atau aksi-aksi masyarakat melalui ormas-ormas, perkumpulan atau LSM adalah pertanda yang baik dan  menggembirakan bagi kehidupan bangsa, yang sekarang sedang ditempa oleh berbagai macam penyakit parah. Kalau gerakan atau aksi-aksi ini makin membesar dan meluas, maka akan bisa menjadi kekuatan sosial dan politik alternatif.

Oleh  karena itu, berbagai gerakan (antara lain : Gerakan Pasal 33 , aksi BEM se-Indonesia, Gerakan Indonesia Bersih, Koalisi Anti-Korupsi) perlu disokong dengan aktif dan besar-besaran oleh semua golongan masyrakat.

Kekuatan alternatif ini,  yang juga merupakan gerakan ekstra-parlementer pro-rakyat dan terdiri dari macam-macam kekuatan demokratis dalam masyarakat  (antara lain : buruh, tani, pemuda, mahasiswa, perempuan, intelektual) bisa kemudian menjadi dasar atau sumber lahirnya partai-partai politik progresif atau kiri, yang betul-betul mewakili kepentingan rakyat.  

Lahirnya partai-partai politik yang sungguh-sungguh memperjuangkan kesejahteraan rakyat, untuk menggantikan partai-partai yang sekarang hanya menjadi alat penipu rakyat, adalah syarat mutlak bisa tercapainya masyarakat adil dan makmur sesuai dengan tujuan revolusi 17 Agustus 45. Hanya dengan partai-partai yang berjuang untuk terjadinya perubahan-perubahan besar, drastis, radikal dan fundamental maka hari depan bangsa dan negara bisa diselamatkan dari keterpurukan seperti yang sedang kita saksikan dewasa ini.

Sekarang makin jelas bagi kita semua bahwa perubahan-perubahan besar tidak akan mungkin bisa terjadi dengan terus berkuasanya SBY bersama Partai Demokratnya, yang mendapat dukungan dari koalisinya. Perubahan besar menuju perbaikan negara dan bangsa juga tidak mungkin dengan  partai-partai yang tergabung dalam koalisinya, seperti Golkar atau PAN atau PKS. Dalam kenyataannya,  partai-partai semacam itu atau sejenis itu adalah sama saja buruknya atau sama busuknya, hanya berbeda dalam kadar dan skalanya saja..

Perubahan besar-besaran hanya mungkin dengan revolusi

Perubahan besar-besaran dan fundamental dan drastis untuk perbaikan bangsa dan negara hanya mungkin dengan menempuh jalan revolusi oleh kekuatan alternatif dan partai-partai revolusioner, seperti yang sudah ditunjukkan oleh berbagai ajaran revolusioner Bung Karno.

Ajaran-ajaran revolusioner Bung Karno, yang pada intinya adalah pro-rakyat, pro-« wong cilik », anti-imperialisme dan anti-kapitalisme, untuk menciptakan masyarakat adil dan makmur, adalah senjata di tangan kekuatan politik alternatif dan  gerakan ekstra-parlementer, untuk menggantikan dominasi pemerintahan-pemerintahan reaksioner, sejenis pemerintahan  SBY beserta sekutu-sekutunya, yang bersekongkol dengan kekuatan asing (neo-liberalisme).
Ajaran-ajaran revolusioner Bung Karno adalah satu-satunya jalan bagi bangsa  Indonesia  untuk benar-benar merealisasikan isi UUD 45, terutama pasal 33, mewujudkan tujuan Pancasila dalam praktek, dan menjadikan Bhinneka Tunggal Ika menjadi kenyataan.

Jalan lain menuju masyarakat adil dan makmur tidak ada !  Ini sudah ditunjukkan oleh Orde Baru dan semua pemerintahan sejak itu, sampai sekarang. Segala macam sistem sudah dicoba , beraneka ragam konsep sudah ditrapkan, dan berbagai teori politik, sosial, ekonomi sudah digunakan, namun hasilnya adalah keterpurukan seperti yang sama-sama kita saksikan dewasa ini.

Semua itu perlu menjadi renungan kita bersama ketika bangsa kita sedang menghadapi perayaan 17 Agustus yang akan datang.
