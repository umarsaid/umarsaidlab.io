---
layout: post
title: Angin segar di cakrawala Indonesia (1)
date: 2000-12-31
---

CATATAN DARI DEN HAAG

Pertemuan dan dialog yang terjadi di KBRI di Den Haag (negeri Belanda) tanggal 17 Januari 2000, antara lebih dari seratus orang Indonesia dan Menkumdang (Menteri Hukum dan Perundang-undangan) Yusril Ihza Mahendra mengandung arti penting dalam berbagai segi dan bidang. Banyaklah hal yang menarik dan patut dicatat tentang pertemuan yang berlangsung lebih dari jam itu. Baik yang berkaitan dengan suasana pertemuan itu sendiri secara keseluruhan, maupun tentang isi dan arah dialog, dan juga berbagai masalah yang muncul dalam dialog itu.

Jelaslah kiranya bahwa tidak mudah untuk menulis atau membuat "laporan" yang relatif akurat atau agak lengkap tentang pertemuan yang sarat dengan berbagai emosi, dan penuh dengan uraian tentang begitu banyaknya masalah yang dialami banyak orang Indonesia yang selama lebih dari 30 tahun telah mengalami penderitaan di luarnegeri akibat politik Orde Baru. Di samping itu, pertemuan besar ini mengemban pesan politik dan pesan moral yang besar artinya bagi kehidupan bangsa kita, dalam usaha bersama untuk menegakkan demokrasi, menjunjung tinggi kaidah-kaidah kemanusiaan, membela HAM dan memperkokoh persatuan bangsa.

Oleh karena itu, alangkah baiknya seandainya ada berbagai tulisan mengenai peristiwa penting ini. Angin segar atau udara baru yang ditiupkan oleh pertemuan ini patut disebar-luaskan. Gemanya perlu dikumandangkan lagi ke berbagai penjuru, terutama di tanah-air. Sebab, jiwa pertemuan di KBRI Den Haag adalah luhur, dan orientasinya benar. Sumbangannya akan penting untuk mencari kebenaran guna menegakkan keadilan bagi banyak korban politik Orde Baru, baik yang terpaksa bermukim lama di luarnegeri, maupun --bahkan terutama sekali-- bagi puluhan juta keluarga yang sudah mengalami berbagai penderitaan yang bertubi-tubi dalam jangka puluhan tahun di tanah-air.

Instruksi Presiden nomor 1 Tahun 2000

Pertemuan besar tanggal 17 Januari di KBRI Den Haag adalah salah satu dari realisasi politik pemerintahan Gus Dur, yang dituangkan dalam Instruksi Presiden RI nomor 1 Tahun 2000. Dalam instruksi presiden itu (yang copynya juga dibagi-bagikan kepada peserta pertemuan) antara lain disebutkan "bahwa permasalahan orang-orang Indonesia yang berada di luar negeri dan terhalang pulang ke Tanah Air sejak terjadinya peristiwa G30S/PKI, perlu segera dicarikan jalan penyelesaian yang terbaik. Dan bahwa sehubungan dengan hal tersebut di atas, dipandang perlu untuk menugaskan Menteri Hukum dan Perundang-undangan untuk berangkat ke Negeri Belanda, guna melakukan pertemuan dan dialog di Negeri Belanda dengan orang-orang Indonesia yag terhalang pulang ke Tanah Air".

Instruksi Presiden itu sendiri sudah menunjukkan dengan jelas adanya kemauan politik pemerintahan Gus Dur untuk mencari penyelesaian yang terbaik bagi orang-orang Indonesia yang berada di luar negeri dan terhalang pulang ke Tanah Air. Manifestasi kemauan politik (political will) ini diperjelas dengan dikirimnya Menkumdang Yusril Mahendra ke Belanda untuk melakukan "pertemuan dan dialog". Walaupun instruksi presiden itu dirumuskan secara sederhana, tetapi isi penjelasan dan jiwa orientasi politik Gus Dur yang dipaparkan oleh Menkumdang dalam pertemuan menunjukkan bahwa dengan pimpinan Gus Dur-Megawati, negeri kita sedang membuka halaman baru dalam sejarahnya. Halaman baru inilah yang sedang bersama-sama kita isi.

Terselenggaranya pertemuan dan dialog di KBRI Den Haag, serta keluarnya Instruksi Presiden nomor 1 Tahun 2000, tidak terlepas dari prakarsa atau usaha Menkumdang yang telah menyarankan kepada Presiden Gus Dur untuk segera menuntaskan masalah nasib orang-orang yang terhalang pulang ke Tanah Air. Dikemukakannya kepada Gus Dur bahwa kita harus menempuh rekonsiliasi nasional, dan mulai lembaran baru sebagai bangsa. Dan karena Presiden Gus Dur sangat peka terhadap masalah-masalah semacam ini, maka saran Menkumdang Yusril disetujui dan didukung oleh Gus Dur.

Mereka Merasa "Pulang Ke Rumah Sendiri"

Pertemuan dan dialog tanggal 17 Januari yang berlangsung lebih dari 4 jam dalam suasana keterbukaan, dan dilaksanakan bersama-sama dengan semangat keakraban dan rekonsiliasi, adalah satu langkah penting untuk mengisi sebagian dari halaman baru bangsa kita. Pekerjaan besar ini indah sekali!

Banyaklah kiranya hal-hal menarik yang bisa dicatat mengenai pertemuan yang bersejarah bagi banyak orang ini. Baik yang berkaitan dengan suasananya, cara penyelenggaraannya, acaranya, para pesertanya, maupun "message"-nya yang sangat penting untuk mendorong lebih lanjut realisasi politik pemerintahan Gus Dur dalam membangun Indonesia Baru di atas puing-puing reruntuhan sistem politik Orde Baru.

Catatan bisa dimulai dengan pemandangan yang mengharukan, ketika mulai jam 16.30 secara bergelombang dan kelompok demi kelompok, sejumlah besar orang-orang Indonesia mendekati gedung KBRI di Den Haag. Sebagian di antara mereka terdiri atas lelaki dan perempuan yang sudah berusia lebih dari 60 tahun. Bahkan banyak yang kelihatannya sudah terpaksa berjalan agak lambat, karena kaki atau tubuh mereka sudah harus menanggung "hukum alam". Seorang di antara mereka terdapat Sidik Kertapati (80 tahun. Orang Indonesia pertama yang berhasil merebut stengun dari tangan sekutu pada tahun 1945. Catatan Redaksi SiaR) yang memasuki pintu gerbang KBRI di atas kursi roda yang didorong oleh temannya.

Entah apa saja yang terbayang dalam pikiran atau hati mereka, ketika orang-orang lansia (lanjut usia) ini memasuki pintu gerbang KBRI dan melihat dari dekat Bendera Merah Putih yang berkibar-kibar dengan megahnya di udara musim dingin negeri Belanda itu. Sebab, gedung KBRI semacam ini, yang juga terdapat di banyak negeri di dunia, sudah lama tidak bisa (atau tidak boleh) diinjak oleh kaki orang-orang "semacam" mereka ini. Wilayah gedung ini, secara hukum internasional, adalah wilayah Republik Indonesia.

Tetapi orang-orang ini, yang semestinya bisa menganggap tempat ini sebagai "rumah sendiri", selama puluhan tahun sudah diharamkan untuk memasukinya, karena dianggap sebagai musuh oleh pemerintahan Orde Baru. Kali ini, mereka disambut dengan ramah dan diperlakuan secara terhormat. Perkembangan situasi yang menggembirakan! Indah! Mengharukan!

Sambutan hangat dan senyuman yang menyejukkan hati ini makin lebih terasa lagi bagi banyak anak hilang itu, ketika disusuli oleh makanan dan minuman dan tegur-sapa yang ramah dari para pejabat KBRI, termasuk Dubes Abdul Irsan dan Menkumdang Yusril Mahendra. Sekitar jam 17.00 ruangan Nusantara makin dipenuhi orang. Di antara mereka terdapat orang-orang yang menjelang akhir 1965 menjabat sebagai anggota parlemen atau MPRS, insinyur, dokter, jurist, wartawan, seniman, pengarang, pegawai negeri dengan jabatan yang cukup penting, pimpinan berbagai macam organisasi. Sebagian besar adalah mahasiswa ikatan dinas yang dikirim oleh pemerintah waktu itu.Mereka ini ada yang pernah lama tinggal di Moskow, Peking, Praha (atau tempat-tempat lainnya) sebelum menetap di negeri Belanda, Jerman, Prancis dll.

Mereka kelihatan antusias untuk memenuhi undangan KBRI (undangan yang< disediakan berjumlah seluruhnya 150 lembar), karena sebelumnya sudah tersiar berita atau artikel dalam pers Indonesia maupun lewat Internet, tentang missi Menkumdang Yusril yang bertujuan untuk mengadakan dialog dengan orang-orang Indonesia yang tercekal untuk pulang ke Tanah Air. Mereka juga sudah membaca berita-berita tentang pernyataan Gus Dur, yang memberikan isyarat bahwa mereka ini bisa mendapatkan kemudahan-kemudahan untuk pulang ke Indonesia. Perkembangan situasi yang begitu baik semacam ini sudah mereka tunggu-tunggu selama puluhan tahun.

Sayang Mereka Sudah Tiada

Adalah sangat sayang, bahwa ada sejumlah orang-orang yang sudah tidak sempat lagi menyaksikan peristiwa penting ini. Berpuluh-puluh orang telah meninggal dalam "pembuangan" di berbagai negeri. Di antara mereka terdapat tokoh-tokoh di berbagai bidang, seperti : Kyai Haji Dasuki Siradj (tokoh Islam dari Solo), Mr. Abdulmadjid Djajadiningrat (anggota DPRGR, walikota Semarang), Djawoto (Dutabesar RI, dan pimpinan Kantor Berita Antara), Sukrisno (Dutabesar RI), Sunan Hamzah SH (Gurubesar Sum. Utara), Basuki Resobowo (seniman, pelukis, anggota DPRGR), Suselo Triharso (anggota BPH Dewan Kotapraja Jakarta), Ang Hong To (wartawan), Suparna Sastradiredja (anggota MPRS, pimpinan SOBSI).

Seandainya mereka masih hidup, pastilah mereka dengan bangga dan gembira akan ikut juga bersama-sama menyanyikan lagu kebangsaan "Indonesia Raya" dan "Satu Nusa Satu Bangsa", dan juga akan ikut terharu dengan terlaksananya dialog yang terjadi tanggal 17 Januari ini. Mereka tidak sempat lagi untuk merasakan hembusan segar Angin Baru, yang dipancarkan dari ruangan "Nusantara" di KBRI Den Haag ini.

(BERSAMBUNG)
