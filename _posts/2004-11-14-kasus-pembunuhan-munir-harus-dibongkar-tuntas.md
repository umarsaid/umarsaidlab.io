---
layout: post
title: Kasus pembunuhan Munir harus dibongkar tuntas
date: 2004-11-14
---

Kelanjutan cerita tentang wafatnya tokoh perjuangan HAM dan Direktur Eksekutif Imparsial , Munir, pada tanggal 7 September 2004 di dalam kabin pesawat Garuda Indonesia dalam perjalanan ke Belanda, makin menarik. Saat berangkat dari Jakarta, ia berada dalam kondisi sehat. Saat pesawat singgah di Changi, Singapura, Munir mulai muntah-muntah dan dua jam sebelum pesawat mendarat di Schiphol, Amsterdam, ia menghembuskan nafas terakhir (Suara Pembaruan 12 November 2004)

Menurut suratkabar Belanda NRC Handelsblad (11 November 2004), bedah jenazah Munir dilakukan oleh Institut Forensik Belanda (NFI/Nederlands Forensisch Instituut). Sumber di Departemen Luar Negeri Indonesia menyebutkan ada zat beracun arsenicum dari hasil otopsi itu.

"Dalam bahasa Jawa, zat itu disebut warangan. Laporan hasil bedah jenazah itu sekarang berada pada Departemen Luar Negeri Indonesia di Pejambon Jakarta," tulis NRC Handelsblad.

Ditambahkan, kadar arsenicum yang terdapat di dalam tubuh Munir itu cukup mematikan. Namun, juru bicara Deplu Belanda di Den Haag tidak bersedia mengungkapkan isi laporan hasil otopsi, karena sifatnya very confidential.

## Siapa Yang Membunuh Munir ?

Dengan hasil penemuan Lembaga Forensik Belanda, yang menyatakan bahwa dari otopsi yang dilakukan pada badan Munir almarhum terbukti adanya zat racun arsenicum yang menyebabkan kematiannya, maka jelas sudah bahwa Munir telah menjadi korban tindakan kriminal. Tetapi, siapakah atau dari kalangan manakah yang telah menghabisi nyawa tokoh perjuangan HAM yang muda ini, adalah suatu persoalan yang penting sekali untuk dibongkar.

Banyak orang tahu bahwa Munir adalah tokoh yang sejak lama dengan sangat berani mempersoalkan orang-orang yang hilang atau dihilangkan oleh aparat-aparat Orde Baru. Ia juga banyak mengangkat kasus-kasus pelanggaran HAM lainnya di Indonesia (antara lain di Aceh dan Papua) dan di Timor Timur. Sikapnya yang sangat kritis terhadap berbagai tindakan aparat militer (terutama oleh sejumlah satuan-satuan TNI AD) telah diperlihatkannya selama menjadi anggota LBH, dan juga sebagai pimpinan KONTRAS serta LSM Imparsial.

Karena itu, banyak orang menduga bahwa Munir tidak disukai oleh berbagai tokoh di kalangan militer. Menurut kantor berita Belanda ANP (11 November 2004) : “Sejak minggu terakhir di Indonesia banyak yang berpendapat bahwa Munir, karena latar belakangnya, telah menjadi korban kejahatan. Bahkan banyak yang menduga bahwa Tentara Indonesia yang melatar belakangi atas kematiannya. Munir pada sebelumnya telah menjadi target sasaran dari berbagai serangan »

## Pemerintahan SBY Harus Tegas

Pemerintahan Presiden SBY harus menjadikan wafatnya Munir sebagai masalah serius yang memerlukan penanganan secara adil, jujur, tegas dan transparan. Dengan hasil otopsi yang dilakukan dinas forensik Belanda, sudah jelas bahwa kematian Munir menuntut diadakannya penyelidikan lebih lanjut yang mengarah kepada penindakan secara hukum terhadap para pelaku kejahatan tersebut, dari golongan mana pun asalnya.

Banyak orang akan mengawasi dengan cermat (dan secara kritis) sikap pemerintahan SBY mengenai kasus wafatnya Munir ini. Apakah pemerintahan SBY akan berani mengambil sikap tegas untuk benar-benar membongkar rahasia pembunuhan Munir, akan merupakan ukuran sampai dimana penegakan hukum betul-betul dapat dilaksanakan. Kepercayaan rakyat terhadap pemerintahan di bawah Presiden SBY akan merosot atau anjlog kalau ternyata tidak mau bertindak tegas, berani dan jujur terhadap kasus kriminal yang besar ini.

Kalau pemerintahan di bawah Presiden SBY berusaha untuk mem-« peti-es »-kan, atau berusaha menyembunyikan berbagai aspek kasus ini, atau menghalang-halangi - dengan berbagai cara ! – terbongkarnya kejahatan ini, maka ini berarti bahwa praktek-praktek busuk yang telah lama dilakukan aparat-aparat Orde Baru (dan pemerintahan berikutnya) masih juga diteruskan sampai sekarang. Sebaliknya, kesungguhan yang diperlihatkan pemerintahan di bawah Presiden SBY untuk mengusut secara tuntas pembunuhan Munir akan menimbulkan kesan kepada banyak orang bahwa pemerintahannya betul-betul mau dan bisa menegakkan hukum terhadap semua pelanggaran atau kejahatan (termasuk yang dilakukan oleh kalangan militer).

## Pembunuhan Politik Harus Kita Lawan

Kasus pembunuhan terhadap Munir harus kita jadikan masalah nasional yang besar sebagai usaha bersama untuk mencegah diteruskannya atau bekembangnya kejahatan yang biadab ini. Sebab, kalau tidak, maka praktek jahat dan biadab semacam ini bisa terus-menerus membahayakan kehidupan politik di negeri kita.

Menurut pengacara Todung Mulia Lubis, kematian Munir memiliki pola sama dengan meninggalnya Baharudin Lopa di luar negeri, dan merupakan pembunuhan politik yang bisa terjadi pada siapa saja yang vokal terhadap pemerintah. Ini, kata dia, tanda bahaya karena orang tidak boleh lagi berbeda pendapat dan mengemukakan aspirasinya. Todung meminta, penyelidikan kasus ini tidak hanya ditangani oleh kepolisian. "Komisi Nasional HAM dan kekuatan sipil harus terlibat di dalamnya," kata dia. (Tempo Interaktif, 13 November 2004)

Kematian Munir harus bisa kita jadikan peringatan bahwa walaupun Orde Baru sudah jatuh, tetapi masih banyak orang (di kalangan aparat-aparat negara dan jugadi kalangan masyarakat luas) yang terus dihinggapi cara berfikir (dan praktek ) seperti yang lazim dipakai selama rezim militer Suharto. Memang, penculikan, penahanan sewenang-wenang, atau pembunuhan gelap yang sering terjadi zaman Orde Baru (ingat, antara lain : hilangnya anak-anak muda PRD, dibunuhnya secara gelap penyair terkenal Wiji Tukul, juga dibunuhnya dan ditahannya jutaan orang di tahun 65) sudah tidak sering terjadi lagi. Tetapi, pembunuhan terhadap Munir baru-baru ini, menunjukkan bahwa pembunuhan politik masih bisa terjadi lagi.

Dari sudut inilah kita lihat betapa pentingnya bagi kita semua untuk menjadikan masalah pembunuhan terhadap Munir sebagai aksi besar-besaran untuk meneruskan perjuangannya dalam membongkar kejahatan-kejahatan terhadap HAM. Di samping itu, lewat pengangkatan masalah pembunuhan Munir kita juga bisa mendorong, mengontrol, dan mengkritik berbagai politik dan kebijakan pemerintahan Presiden SBY dalam penegakan hukum, pembelaan HAM, dan pemberantasan sisa-sisa praktek buruk Orde Baru.

## Partisipasi Masyarakat Adalah Perlu

Untuk mendorong pemerintahan di bawah Presiden SBY supaya bisa mengusut kasus pembunuhan Munir secara tuntas, jujur, dan adil, perlu sekali seluruh kekuatan pro-demokrasi dan pro-reformasi mengadakan berbagai aksi, baik menurut bidangnya masing-masing maupun secara bersama-sama. Partisipasi masyarakat luas dalam mengawasi dengan waspada dalam penanganan pemerintah terhadap kasus ini adalah sangat perlu. Sebab, sudah ada gejala-gejala yang aneh-aneh yang terjadi di sekitar masalah ini. Di antaranya adalah : mengapa keluarga/istri Munir tidak segera menerima hasil otopsi? Dan mengapa ada kesan bahwa aparat-aparat pemerintah Indonesia “kurang greget” untuk menangani kasus wafatnya Munir?

Jelaslah bahwa pembunuhan Munir adalah masalah besar (yang serius pula), karena ada sangkut-pautnya yang erat dengan masalah-masalah politik, atau dengan masalah-masalah HAM, Dan seperti yang kita alami sendiri selama ini, masalah politik dan HAM adalah dua bidang yang telah banyak dirusak atau dibusukkan oleh rezim militer Orde Baru beserta para pendukungnya. Karena itu, membongkar pembunuhan Munir adalah merupakan juga suatu tindakan yang membongkar sisa-sisa kekuatan busuk Orde Baru.

## Kasus Munir Jangan Direkayasa !

Mengingat perjuangan Munir dalam melawan - secara berani dan gigih sekali ! – berbagai kejahatan dan pelanggaran Orde Baru di bidang HAM (yang kebanyakan dilakukan oleh kalangan militer) adalah wajar saja bahwa Munir dianggap sebagai musuh oleh kalangan tertentu. Itulah sebabnya kantor LSM-nya sudah berkali-kali diterror dan rumahnya juga pernah kena ledakan bom.

Walaupun penyelidikan pihak polisi Indonesia terhadap kasus pembunuhan ini baru mencapai tingkat permulaan, tetapi banyak orang sudah menduga (atau menuduh) bahwa ada oknum militer di belakangnya. Dan kalau dugaan atau tuduhan orang-orang ini mengandung kebenaran, maka adalah kewajiban pemerintahan SBY untuk bertindak tegas dalam menegakkan hukum dan mengusahakan supaya kasus ini dapat diselesaikan secara jujur, adil, dan transparan.

Pemerintahan Presiden SBY akan mendapat penilaian positif sekali dari rakyat luas kalau kasus pembunuhan Munir dapat dibongkar secara tuntas dan para pelakunya – atau para “otak” di belakangnya - dihukum, tidak peduli apakah mereka dari kalangan militer atau bukan. Selama ini masih banyak juga orang yang tidak percaya bahwa Presiden SBY tidak mau (atau tidak bisa) bertindak terhadap petinggi-petinggi militer yang melakukan pelanggaran atau kejahatan berat, karena Presiden SBY sendiri adalah mantan petinggi militer.

Presiden SBY (dan para pendukungnya) akan melakukan kesalahan besar sekali, seandainya karena ingin menjaga nama baik TNI maka ia akan mengusakan supaya kasus pembunuhan Munir ini jangan sampai terbongkar secara tuntas. Sebab, dengan berbagai cara dan rekayasa, kasus Munir ini bisa saja dimanipulasi. Sebaliknya, justru nama baik TNI akan “tertolong” dengan adanya penanganan kasus pembunuhan Munir ini secara jujur dan transparan. Sebab, berbagai bagian dari TNI (terutama TNI-AD) selama puluhan tahun sudah banyak sekali melakukan penahanan, penculikan dan pembunuhan (secara gelap) terhadap orang-orang tidak bersalah, tetapi sedikit sekali dari pelakunya yang dihukum.

Kasus pembunuhan Munir adalah salah satu dari banyak batu ujian bagi pemerintahan SBY apakah hukum bisa betul-betul ditegakkan, seperti yang sudah banyak dijanjikan selama kampanye pemilihan umum legislatif dan pemilihan presiden yang lalu. Mari sama-sama kita amati dengan waspada.
