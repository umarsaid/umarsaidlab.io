---
layout: post
title: Memilih SBY atau Megawati ?
date: 2004-07-22
---

Sekarang sudah dapat dipastikan bahwa pertarungan dalam putaran kedua pemilihan presiden bulan September akan dimainkan antara pasangan Susilo Bambang Yudoyono-Jusuf Kalla dan pasangan Megawati-Hasyim Muzadi. Ini berarti bahwa rakyat diundang untuk memilih presiden dan wakil presiden antara dua pasangan itu.

Dari hasil pilpres putaran pertama kelihatan bahwa pasangan SBY-Jusuf Kalla mengungguli pasangan Mega-Hasyim. Apa keunggulan SBY-Jusuf Kalla dapat dipertahankan pada putaran kedua di bulan September, masih merupakan teka-teki besar bagi banyak orang. Sebab, banyak faktor yang ikut menentukan hasil pertarungan kedua pasangan itu. Dan masih banyak kejadian atau persoalan-persoalan yang masih muncul sampai berlangsungnya putaran kedua, yang bisa ikut mempengaruhi atau ikut menentukan kalah-menangnya kedua pasangan itu.

Selama kampanye pemilihan presiden kita sudah dibanjiri oleh segala macam informasi, analisa, komentar, ramalan, mengenai segala macam capres dan cawapres. Dari apa yang sudah kita baca atau kita dengar itu kita bisa belajar banyak tentang tingkah-laku para politisi kita, tentang permainan yang dimainkan partai-partai, atau tentang kalutnya dunia perpolitikan negeri kita. Di tengah-tengah keadaan yang demikian itu timbul pertanyaan apakah presiden kita nanti SBY ataukah Mega?

## Ah, Situasi Akan Sama Saja!

Banyak analisa, komentar, atau pendapat yang telah diajukan berbagai fihak mengapa sebaiknya nanti memilih pasangan SBY-Jusuf Kalla atau memilih pasangan Mega-Hasyim. Di antara berbagai analisa atau pendapat itu banyak yang menarik, tetapi ada juga yang tidak perlu kita anggap serius.

Yang penting kita perhatikan ialah adanya golongan-golongan orang yang sekarang ini sudah tidak peduli lagi tentang siapa yang akan menjadi presiden kita, karena siapapun akan jadi presiden situasi toh akan sama saja nantinya. Mereka beranggapan bahwa siapapun jadi presiden, toh tidak akan ada perobahan, apalagi perobahan radikal. Sebabnya, kesulitan atau masalah-masalah sudah sedemikian besarnya dan sedemikian banyaknya, sehingga tidaklah mungkin bagi pasangan Sby-Kalla atau pun pasangan Mega-Hasyim untuk mengatasinya, apalagi hanya dalam waktu 5 tahun saja.

Apalagi kalau mengingat bahwa siapapun jadi presiden, terpaksa membentuk aliansi atau koalisi, baik di dalam DPR-DPRD atau di luarnya. Artinya, presiden yang mana pun akan terpaksa terikat atau terkerangkeng karena keharusan aliansi atau koalisi ini. Dan dalam aliansi atau koalisi ini Golkar akan tetap memainkan peran yang tidak kecil. Dan inilah yang merupakan halangan atau hambatan akan terjadinya perobahan-perobahan besar atau radikal. Jadi, karena bagaimanapun Golkar masih akan bisa memainkan perannya seperti yang sudah-sudah, maka wajarlah kalau orang-orang mengatakan :”Ah, situasi akan sama saja!”

## PDI-P Terpaksa Merangkul Golkar ?

Sudah santer dibicarakan di berbagai kalangan tentang kemungkinan PDI-P berkoalisi dengan Golkar. Kalau terjadi koalisi antara PDI-P dan Golkar dan karenanya Megawati terpilih lagi sebagai presiden, maka bisa diramalkan bahwa situasi politik tidak akan mengalami goncangan-goncangan yang besar. Presiden Megawati akan bisa memimpin negara dengan dukungan suara Golkar yang cukup besar di parlemen.

Tetapi justru karena dukungan Golkar itulah nantinya presiden Megawati tidak akan bisa banyak mengadakan perubahan-perubahan yang radikal atau mendasar. Artinya, reformasi tetap tidak akan jalan, korupsi akan tetap merajalela di banyak bidang, hukum tidak bisa ditegakkan sepenuhnya, dan kebusukan akhlak di kalangan elite akan tetap meracuni kehidupan bangsa.

Megawati-Hasyim terpaksa merangkul Golkar untuk menang dalam putaran ke-II pemilihan presiden bulan September. Justru inilah yang merupakan dilemma. Sebab Megawati terpaksa bersatu dengan suatu kekuatan politik, yang selama lebih dari 32 tahun sudah terbukti merusak negara dan bangsa, yang akibatnya masih sama-sama kita saksikan atau rasakan sampai dewasa ini.

## SBY-Kalla Yang Meragukan

Selama ini banyak pendapat atau komentar yang terdengar mengenai capres SBY, yang mengatakan bahwa SBY simpatik, gagah atau tampan, kalem dan sopan, dan kalau ia bicara enak didengar. Bukti bahwa ia berhasil menarik simpati banyak orang ialah bahwa ia bisa meraih suara yang paling besar di antara semua capres (SBY 33% sedangkan Megawati 26%, Wiranto 22 %, Amin Rais 14%, dan Hamzah Haz 3%).

Tetapi, apakah dalam putaran ke II nantinya ia akan mengalahkan Megawati masih belum pasti. Sebab, masih banyak faktor yang tak terduga dan kejadian yang bisa muncul antara sekarang dan September. Memang, khalayak ramai mengharapkan adanya perobahan-perobahan radikal, di bidang politik, ekonomi dan sosial. Tetapi, apakah pasangan SBY-Jusuf Kalla akan bisa mengadakan perobahan-perobahan besar dalam waktu 5 tahun mendatang, masih perlu diragukan.

SBY adalah mantan petinggi militer, yang masih mendapat simpati atau dukungan aktif dari banyak kalangan militer. Inilah yang merupakan kekuatan dan sekaligus juga kelemahan SBY. Di samping itu Jusuf Kalla adalah tokoh Golkar yang cukup menonjol. Dari pasangan ini tidak bisa pula diharapkan terlalu besar adanya pelaksanaan reformasi secara sungguh-sungguh. Banyak hal-hal yang masih gelap mengenai sepak-terjang pasangan ini, baik sekarang maupun di kemudian hari.

Sekarang pun sudah mulai terdengar isyu-isyu yang berkaitan dengan sejarah SBY (antara lain perannya sebagai petinggi militer ketika terjadi peristiwa 27 Juli dan Aceh).

## Buanglah Jauh-Jauh Itu Ilusi !

Mengingat itu semua pantaslah kalau sebagian orang sudah tidak punya harapan lagi akan terjadinya perobahan-perobahan besar menuju perbaikan, siapa pun akan jadi presiden, apakah Megawati atau SBY. Banyaknya masalah-masalah berat yang harus ditanggulangi dan luasnya kerusakan-kerusakan yang diwariskan Orde Baru - dan pemerintahan-pemerintahan sesudahnya – memerlukan adanya pimpinan nasional yang bersih, berwibawa, cakap, berbobot, dihormati oleh sebagian terbesar rakyat kita, dan bisa menjadi simbul persatuan bangsa. Apakah pimpinan semacam itu bisa kita temukan pada sosok Megawati atau SBY adalah pertanyaan besar.

Kalau begitu, apa sebaiknya hanya menjadi Golput saja nantinya di bulan September? Menjadi Golput atau memilih salah satu diantara dua capres adalah hak pribadi masing-masing. Tetapi, penting untuk diingat oleh semua orang bahwa kita tidak bisa dan tidak boleh mempunyai ilusi bahwa dengan terpilihnya presiden baru nanti banyak perubahan akan terjadi, dan bahwa reformasi akan bisa dilancarkan dengan sungguh-sungguh. Perubahan radikal menuju perbaikan tidaklah mungkin dilaksanakan oleh golongan-golongan yang akhlaknya sudah membusuk secara parah. Mereka ini terdapat di kubu Megawati dan juga di kubu SBY. Tetapi, pembusukan mental yang paling parah adalah di dalam partai Golkar. Jadi, buanglah jauh-jauh itu ilusi bahwa perubahan radikal menuju perbaikan akan bisa dilaksanakan oleh mereka.

Dapat diramalkan bahwa dalam 5 tahun yang akan datang, situasi politik, ekonomi dan sosial negeri kita akan tetap menghadapi masalah-masalah besar yang rumit, kalau tidak dikatakan makin besar dan makin rumit. Dalam situasi yang demikian, sebagian besar rakyat akan tetap menderita, atau bahkan makin menderita, sedangkan kaum elite-nya, yang jumlahnya tidak banyak, akan terus menjadi parasit bangsa. Kaum elite yang sudah membusuk akhlaknya ini terdiri dari pembesar-pembesar sipil dan militer, tokoh-tokoh partai politik, tokoh-tokoh kalangan agama dan ulama, dan para koruptor di banyak kalangan, baik di kalangan swasta maupun pemerintah. Mereka juga terdiri dari anggota-anggota DPR dan DPRD.

## Kekuatan Extra-Parementer Yang Besar

Dalam menghadapi situasi yang demikian, tidak peduli apakah di bawah kekuasaan presiden Megawati atau pun SBY, seluruh kekuatan pro-reformasi dan pro-demokrasi dalam masyarakat perlu terus membangun kekuatannya sendiri, dengan membentuk berbagai ornop independen. Lahirnya berbagai kekuatan extra-parlementer yang besar, yang berbentuk macam-macam, akan merupakan senjata di tangan rakyat, untuk menghadapi segala macam politik yang dimainkan pemerintah, partai, dan berbagai kalangan lainnya (termasuk kalangan agama), yang merugikan kepentingan rakyat banyak. Organisasi pemuda, mahasiswa, buruh, tani, pegawai negeri, wanita, pedagang dan pengusaha, yang selama ini sudah mengadakan berbagai kegiatan, perlu meningkatkan terus perjuangan mereka dalam membela kepentingan mereka sendiri dan kepentingan rakyat umumnya.

Pengalaman selama berbagai pemerintahan sesudah jatuhnya Suharto 6 tahun yang lalu, menunjukkan bahwa kontrol terhadap pemerintah tidaklah cukup dilakukan hanya oleh DPR atau DPRD saja. Bahkan sudah banyak bukti bahwa DPR dan DPRD justru harus dikontrol oleh rakyat, karena banyak anggotanya (betul, tidak semua!) melakukan korupsi dalam berbagai bentuk dan mempraktekkan penyelewengan melalui berbagai cara. Berita tentang adanya “korupsi kolektif” yang dilakukan para anggota DPRD di berbagai kota dan daerah sudah sering kita baca.

## Perbanyak Dan Kembangkan Organisasi Rakyat

Mengingat bahwa kita tidak boleh dan tidak bisa terlalu mengharapkan bahwa perubahan-perubahan radikal akan terjadi di bawah Megawati atau SBY, maka seluruh kekuatan pro-reformasi dan pro-demokrasi harus sejak sekarang mengadakan langkah-langkah untuk menghadapi segala kemungkinan yang bisa muncul di kemudian hari. Sebab, negara kita akan tetap menghadapi banyak masalah-masalah besar dan sulit, yang disebabkan ulah kaum elite yang sudah rusak akhlak mereka.

Karena rakyat sudah tidak bisa sepenuhnya mempercayai DPR/DPRD atau lembaga-lembaga lainnya, maka tugas mulia membela kepentingan rakyat banyak ini jatuh di pundak organisasi-organisasi rakyat yang bisa dibangun dalam berbagai bentuk.

Dengan berkembangnya secara luas segala macam organisasi rakyat, maka akan lahir kekuatan yang merupakan “pressure group” (golongan penekan), yang berfungsi sebagai senjata publik untuk membela kepentingan orang banyak atau menyalurkan aspirasi rakyat. Dengan begini demokrasi bukan hanya urusan yang dimonopoli kaum elite yang duduk dalam DPR/DPRD atau lembaga-lembaga lainnya. Oleh karena itu, seluruh kekuatan pro-reformasi dan pro-demokrasi perlu memberikan bantuan sebesar-besarnya dan dukungan seluas-luasnya, untuk berkembangnya berbagai macam organisasi rakyat ini.

Siapapun yang akan jadi presiden, masalah diperbanyaknya dan diperkuatnya organisasi extra-parlementer ini, akan tetap menjadi tugas utama seluruh kekuatan pro-reformasi dan pro-demokrasi. Organisasi non-pemerintah yang melakukan kegiatan-kegiatan extra-parlementer adalah benteng terakhir yang dipunyai rakyat.
