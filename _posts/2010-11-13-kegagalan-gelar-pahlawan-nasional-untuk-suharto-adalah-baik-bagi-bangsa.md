---
layout: post
title: Kegagalan gelar pahlawan nasional untuk Suharto adalah baik bagi bangsa
date: 2010-11-13
---

Gagalnya usaha berbagai kalangan untuk menjadikan Suharto sebagai pahlawan nasional mengandung arti penting yang patut kita coba telaah bersama-sama dari berbagai segi. Masalah apakah Suharto itu pantas atau tjdak (atau berhak atau tidak) mendapat gelar pahlawan nasional adalah masalah besar bangsa. Jadi, bukan hanya masalah penting bagi sebagian pimpinan Angkatan Darat (yang masih aktif maupun yang sudah pensiun), atau bagi sebagian pimpinan GOLKAR dan pimpinan semua partai politik yang dengan diam-diam dan secara terselubung masih mendukung Orde Baru, atau anti politik Bung Karno dan anti-kiri.



Masalah apakah Suharto itu adalah  pahlawan nasional atau tidak juga merupakan soal yang besar sekali terutama bagi kalangan golongan kiri di Indonesia, yang diwakili oleh PKI beserta semua organisasi-organisasi yang berafiliasi kepadanya, serta juga bagi macam-macam golongan di seluruh Indonesia yang pernah dirugikan oleh Orde Baru. Jadi,  orang-orang yang pernah menjadi korban rejim militer Suharto adalah besar dan luas sekali, dan bukan hanya golongan kiri saja.

Perlawanan yang santer dan luas sekali

Golongan kiri (terutama dari kalangan PKI) dan golongan non-PKI yang menjadi korban rejim militer Suharto inilah yang baru-baru ini menyuarakan secara lantang, bertubi-tubi, dan meluas sekali untuk menentang usul atau gagasan diberikannya gelar pahlawan nasional kepada Suharto. Jadi sama sekali tidak benarlah apa yang dikatakan oleh Ruhut Sitompul  (tokoh Partai Demokrat) yang menyatakan bahwa « hanya anak-anak PKI sajalah  yang tidak  setuju dengan pencalonan Suharto sebagai pahlawan »

Menjelang diperingatinya Hari Pahlawan 10 November  berbagai tokoh korban peristiwa 65, baik yang di Indonesia maupun di luar negeri, telah menyatakan menolak diberikannya gelar pahlawan  kepada Suharto ini. Juga banyak sekali kegiatan-kegiatan yang dilancarkan dengan berbagai bentuk dan cara di Indonesia.. Di antaranya, yang mempunyai arti penting sekali, adalah pernyataan dari Ketua Umum HMI (Himpunan Mahasiswa Islam) yang juga menolak dengan tegas usul gelar pahlawan untuk Suharto.

Begitu santernya suara dan begitu kuatnya desakan (pressure) opini publik di seluruh negeri yang menentang gelar pahlawan bagi Suharto, maka akhirnya memaksa Dewan Gelar, Tanda Kehormatan dan Tanda Jasa (dan presiden SBY) untuk tidak meloloskan pencalonan Suharto sebagai pahlawan.  Perlawanan terhadap pencalonan Suharto ini akhirnya mengalahkan suara-suara yang tadinya terdengar arogan dan terlalu percaya diri (antara lain Tommy Suharto menyatakan berkali-kali bahwa gelar pahlawan nasional untuk Suharto « sudah tinggal selangkah lagi » atau ucapan-ucapan Probo Sutedjo yang menggambarkan rasa optimisnya).

Nama Suharto sudah makin ambles atau anjlog

Sebenarnya, kalau sama-sama kita amati dengan tajam, maka nyatalah bahwa akhir-akhir ini sosok Suharto semakin pudar, bahkan makin ambles (tenggelam) atau anjlog. Ketika di seluruh negeri rakyat Indonesia memperingati Hari Sumpah Pemuda atau Hari Pahlawan, maka sedikit sekali (kalau tidak dikatakan tidak ada ) orang yang mengucapkan nama Suharto yang dikaitkan dengan kedua hari besar bersejarah itu. Sebaliknya, nama Bung Karno sering disebut-sebut dalam berbagai kesempatan yang bersejarah itu oleh banyak kalangan.

Ini wajar. Sebab, nama Bung Karno adalah nama yang betul-betul erat hubungannya  dengan Hari Sumpah Pemuda dan Hari Pahlawan,, dan bukan nama Suharto. Kalau nama Bung Karno adalah jelas-jelas nama yang sepenuhnya  berhak diberi gelar pahlawan nasional, maka sebaliknya Suharto adalah nama yang betul-betul najis atau haram untuk mendapat kehormatan yang begitu tinggi.

Karena itu, bahwa untuk tahun 2010 Suharto tidak diloloskan menjadi pahlawan nasional adalah suatu hal yang penting sekali bagi  bangsa dan anak cucu kita di kemudian hari. Sebab, kalau seandainya diberi gelar pahlawan nasional kepadanya, lalu apa yang akan bisa dijelaskan kepada anak-cucu kita di kemudian hari ? Mengapa orang yang sudah melakukan pengkhianatan begitu besar terhadap Bung Karno, dan terhadap Republik Indonesia, diberi gelar pahlawan nasional ? Mengapa seorang pembunuh (atau menyebabkan terbunuhnya) 3 juta manusia tidak bersalah apa-apa sama sekali sampai dihormati begitu tinggi ? Mengapa koruptor, yang kejahatan besarnya dalam bidang KKN sudah begitu jelasnya, masih dipilih juga oleh sebagian golongan kecil elite pendukungnya ?

Daftar nama pahlawan nasional tidak bisa dikotori dengan najis

Para sejarawan dan para tokoh dalam bidang pendidikan dan  kebudayaan  (dan kita semua) akan bisa menyaksikan bahwa   tidak diloloskannya pencalonan  Suharto sebagai pahlawan nasional juga berarti tidak dicemarinya daftar nama pahlawan nasional dengan kotoran atau najis, yang sudah terbukti menimbulkan berbagai kerusakan  bagi kehidupan bangsa,  dan membusukkan moral banyak orang serta  meyesatkan iman banyak kalangan ummat Islam.

Dalam kaitan ini, kiranya kita semua bisa merenungkan  -- sedalam-dalamnya  --  berbagai hal yang sebagai berikut :

Diberikannya gelar pahlawan nasional kepada Suharto tidaklah akan  mendatangkan kebaikan atau keuntungan apa-apa sama sekali bagi seluruh bangsa, kecuali hanya  menyenangkan segolongan kecil sekali pengagumnya. Bahkan, hanya akan melestarikan berbagai ragam  kebencian karena sakit hati atau memperpanjang rasa dendam di kalangan korban Orde Baru yang besar ( !)  sekali atau luas ( !!)  sekali jumlahnya di seluruh Indonesia. Ini akan merupakan kerugian besar sekali bagi persatuan dan kesatuan bangsa, baik sekarang maupun di kemudian hari.

Diberikannya gelar pahlawan nasional kepada Suharto akan merupakan bukti atau petunjuk yang jelas bahwa golongan  atau kalangan yang mengusulkannya adalah orang-orang yang buta matanya, atau mati hati nuraninya, atau rusak akhlaknya, sehingga tidak mau tahu-menahu terhadap kebusukan dan kejahatan Suharto yang banyak dan berbagai ragam, yang sudah dilakukan dalam jangka waktu begitu lama itu. Mereka ini perlu dicatat sebagai bahan penting dalam sejarah bangsa kita, demi kebaikan anak-cucu kita di kemudian hari.

Apa arti gelar pahlawan untuk Suharto

Mendukung Suharto sebagai pahlawan nasional berarti ikut menyetujui segala politik atau tindakan Suharto selama 32 tahun, termasuk segala macam pelanggaran HAM yang sangat serius dan beragam kejahatannya di bidang KKN. Gelar pahlawan nasional bisa berarti bahwa segala kesalahan atau kejahatan Suharto menjadi terhapus begitu saja dan dima’afkan oleh bangsa. Kalau ini terjadi, maka akan bisa diartikan sebagai kesalahan besar  rakyat atau kebodohan  bangsa, yang tidak bisa dima’afkan oleh generasi kita sekarang maupun yang akan datang, termasuk oleh berbagai kalangan di dunia.

Semua itu perlu kita angkat kembali ke permukaan, karena walaupun untuk kali ini dalam tahun 2010 pencalonan Suharto sebagai pahlawan nasional sudah gagal, namun bisa saja para pendukungnya, baik di Golkar maupun di sebagian pimpinan Angkatan Darat (dan di berbagai partai politik) masih akan terus berusaha mengusulkannya kembali di tahun-tahun yang akan datang.

Oleh karena itu, pemblejetan segala kebusukan Suharto atau pembongkaran segala macam kejahatan Suharto adalah tugas bersama yang perlu terus-menerus dilakukan oleh  semua golongan yang mendambakan adanya perubahan radikal dan fundamental atau perubahan besar-besaran di negeri kita,  Sebab, perubahan besar-besaran di Indonesia tidaklah mungkin diadakan tanpa menghilangkan segala keburukan dari sistem politik Orde Baru beserta sisa-sisanya.

Menghujat terus kejahatan-kejahatan Suharto adalah penting

Jadi, ini berarti bahwa membongkar berbagai kejahatan Suhato dan menentang dicalonkannya sebagai pahlawan nasional adalah soal yang erat hubungannya dengan soal-soal besar di masa kini dan masa depan negara kita. Membongkar terus kejahatan dan kebusukan Suharto sama sekali bukanlah hanya merupakan perbuatan iseng atau balas dendam terhadao persoalan atau peristiwa yang sudah kedaluwarsa. Menghujat segala kejahatan  Suharto adalah bagian penting dari usaha bersama kita untuk mengadakan perubahan-perubahan menuju lahirnya Indonesia Baru.

Selama berlangsungnya hiruk pikuk tentang pencalonan gelar pahlawan nasional Suharto beberapa waktu yang lalu, kelihatan nyata  sekali bahwa banyak orang membanding-bandingkan ketokohan Bung Karno dengan Suharto. Banyak orang melihat sosok Bung Karno sebagai satu-satunya tokoh besar  - bahkan terbesar ! – dalam sejarah perjuangan rakyat Indonesia, sedangkan Suharto sebaliknya, ia hanya merupakan noda hitam yang mengotori halaman sejarah bangsa.

Oleh karena itu, para pengamat masalah internasional dan para pakar di berbagai bidang, akan bisa menyaksikan di kemudian hari bahwa gagalnya pencalonan Suharto sebagai pahlawan nasional tidaklah mengurangi sedikit pun  martabat Republik Indonesia di mata internasional. Bahkan sebaliknya ( !!!) , penolakan gelar pahlawan untuknya justru menaikkan pandangan berbagai kalangan internasional terhadap bangsa dan negara Republik Indonesia.

Jadi, singkatnya, perlawanan banyak kalangan di Indonesia terhadap pemberian gelar pahlawan kepada Suharto adalah sejalan atau searah  dengan perjuangan berbagai kalangan di dunia yang membela hak-hak azasi manusia dan melawan segala macam kejahatan yang merugikan kepentingan orang banyak.

Di sini pulalah letak kebesaran atau keluhuran  - serta kebenaran -  perlawanan terhadap gelar pahlawan nasional untuk Suharto.
