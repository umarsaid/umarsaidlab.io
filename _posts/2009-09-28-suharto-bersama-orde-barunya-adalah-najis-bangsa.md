---
layout: post
title: Suharto bersama Orde Barunya adalah najis bangsa
date: 2009-09-28
---

(Mohon dimaklumi terlebih dahulu, bahwa dalam tulisan kali ini terdapat kalimat-kalimat yang bernada agak keras, yang disengaja untuk sekadar menekankan persoalan-persoalan atau memperkuat ungkapan-ungkapan tertentu)



Kedatangan tanggal 30 September sekarang ini membuat berbagai kalangan di  Indonesia teringat kembali kepada pembunuhan besar-besaran terhadap jutaan orang yang tidak bersalah (atau yang tidak berdosa apa-apa sama sekali !) di Aceh, Sumatera Utara, Sumatera Barat, Riau, Sumatera Selatan, Jawa Tengah, Jawa Timur, Bali, Kalimantan, Sulawesi dan tempat-tempat lainnya.

Hal yang demikian adalah baik sekali, karena pembunuhan besar-besaran yang dilakukan di bawah pimpinan, atau pengarahan, atau hasutan militer dalam tahun-tahun 1965-1966 merupakan kejahatan yang luar biasa besarnya sepanjang sejarah bangsa Indonesia sampai sekarang. Kiranya, kejahatan terhadap peri kemanusiaan yang merupakan aib besar dan dosa maha-berat ini  patut dikenang terus dan dikutuk oleh seluruh bangsa, termasuk oleh anak cucu kita di kemudian hari.

Sebab, mereka yang dibunuh ini kebanyakan adalah anggota (atau simpatisan) PKI dan  berbagai organisasi massa buruh, tani, nelayan, prajurit, pegawai negeri, wanita, pemuda, mahasiswa, pengusaha dan berbagai kalangan masyarakat lainnya. Di samping itu juga telah ditahan atau dipenjarakan 1.900 000 orang (yang juga sudah terbukti dengan jelas sekali tidak bersalah apa-apa sama sekali !) dalam jangka waktu yang berbeda-beda. Ada yang ditahan sampai puluhan atau belasan tahun, di antaranya di pulau Buru.  

Disebabkan oleh  pembunuhan massal dan pemenjaraan jutaan orang-orang yang umumnya adalah pendukung Bung Karno itu, maka puluhan juta istri para korban tindakan militer ini (beserta anak-anak mereka) terpaksa hidup dalam kesengsaraan atau penderitaan yang berkepanjangan. Banyak di antara mereka sampai sekarang (tahun 2009) masih tetap menderita akibat tindakan militer di bawah Suharto itu.



Memperingati peristiwa 65  adalah perlu sekali

Oleh karena itu, agaknya adalah baik sekali kalau setiap menghadapi 30 September berbagai kalangan masyarakat di Indonesia bisa mengadakan berbagai kegiatan, dalam macam-macam bentuk dan cara, untuk memperingati peristiwa besar berdarah ini. Sebab, dengan mengadakan berbagai ragam kegiatan ini, bisa diangkat kembali berbagai aspek yang berkaitan dengan peristiwa 30 September, atau tindakan militer di bawah Suharto yang serba sewenang-wenang, atau pengkhianatan terhadap Bung Karno, dan dibangunnya diktatur militer Orde Baru.

Dengan selalu mengangkat kembali berbagai hal yang berkaitan dengan segala kejahatan  militer terhadap para pendukung Bung Karno (terutama golongan kiri yang dipelopori oleh PKI), maka makin nyatalah  -- dan dengan jelas sekali pula ! -- bahwa para korban pembunuhan dan para tapol itu adalah orang-orang yang tidak bersalah atau tidak berdosa apa-apa sama sekali !!!!! (harap perhatikan, tanda seru lima kali). Yang bersalah besar atau yang berdosa berat sekali  adalah justru pimpinan militer di bawah Suharto (beserta segala jenis pendukungnya, antara lain Golkar). Hal ini adalah sulit sekali dibantah.

Sekarang, sebelas  tahun sesudah turunnya Suharto dari jabatannya sebagai presiden dan tumbangnya diktatur militer Orde Baru, keluarga para korban pembunuhan massal dan pemenjaraan besar-besaran, yang selama 32 tahun dibungkam suara mereka, dikucilkan, dan disiksa dengan berbagai macam tindakan dan segala macam  peraturan-peraturan yang aneh-aneh, mulai buka suara untuk menggugat segala perlakuan yang tidak manusiawi dan berkepanjangan itu.



Menggugat dan mengutuk Orde Baru adalah benar dan luhur

Bahwa mereka menggugat, atau mengutuk segala tindakan kejam dan tidak bermanusiawi itu adalah hal yang wajar, benar, luhur dan juga 100% sah-sah saja. Adalah hak mereka yang terhormat dan juga mulia bahwa mereka menggugat atau mengutuk berbagai kekejaman diktatur militer Suharto terhadap begitu banyak orang itu ! Sebaliknya, tidak menggugat atau tidak mengutuknya adalah sikap moral yang salah dan juga iman yang tidak sehat. Semua orang yang berhati nurani bersih dan bernalar waras tidak akan menyetujui tindakan atau politik rejim militer Suharto yang sudah menyengsarakan puluhan juta bapak-ibu beserta anak-anak itu (sekali lagi : yang tidak bersalah apa-apa itu !), dan dalam jangka waktu yang begitu panjang pula.

Selama puluhan tahun citra bangsa Indonesia telah dipermalukan atau dikotori oleh aib besar yang dibikin oleh Suharto dkk. Nama Indonesia (baca : Suharto) pernah menjadi olok-olok atau cemooh di banyak  kalangan di dunia yang mencintai demokrasi, yang menghargai HAM, atau yang beradab. Berlainan dengan Bung Karno yang berhasil menaikkan tinggi-tinggi derajat bangsa di mata dunia, Suharto telah membikin jatuh nama bangsa Indonesia  di lumpur busuk yang dinajiskan banyak orang.

Karenanya, hanyalah orang-orang yang perlu diragukan kesehatan nalar mereka atau, bahkan,  harus dipertanyakan kebersihan hati nurani merekalah yang sekarang ini masih terus menganggap bahwa Suharto sudah amat berjasa bagi negara dan bangsa Indonesia. Padahal, sebaliknya !

Sudah terbukti dengan jelas sekali bahwa Suharto (beserta konco-konconya baik di kalangan militer maupun sipil, termasuk Golkar) sudah melakukan kerusakan-kerusakan yang hebat sekali terhadap negara dan bangsa, dengan berbagai kejahatan di bidang politik, sosial dan ekonomi, dan dalam jangka waktu yang sangat lama.  Kita bisa melihat akibatnya itu semua sampai sekarang, dengan banyaknya pelanggaraan HAM, menghebatnya korupsi, parahnya kebejatan moral, meluasnya kebudayaan maling harta negara dan rakyat, makin menghebatnya perlombaan mengejar uang haram (ingat, antara lain kasus-kasus : bank Century, BLBI, Pertamina dll  dll dll ).



Aib besar bagi bangsa yang dibikin Suharto

Kalau kita tinjau dengan cermat, maka nyatalah bahwa separo  dari umur Republik Indonesia (yaitu 32 tahun dari 64 tahun) dikangkangi oleh diktatur militer Suharto, yang dibangun atas mayat jutaan yang dibantai secara biadab dan dipenjarakan secara sewenang-wenang tanpa proses pengadilan. Dan, yang lebih-lebih lagi menggeramkan hati banyak orang adalah bahwa Suharto (bersama konco-konconya  dan  pendukungnya di kalangan militer dan pimpinan Golkar) telah menumpuk harta haram besar-besaran di atas mayat, dan aliran darah serta lautan air mata puluhan juta orang tidak bersalah apa-apa.

Mengingat itu semua,  agaknya sudah sewajarnyalah kalau banyak orang sekarang mengatakan bahwa Suharto sekali-kali bukanlah pemimpin rakyat, melainkan sebaliknya, yaitu pengkhianat besar kepentingan rakyat banyak. Di samping itu Suharto adalah jelas-jelas pengkhianat pemimpin besar rakyat Indonesia, Bung Karno. Ditambah pula Suharto adalah pengkhianat revolusi rakyat Indonesia dalam melawan sisa-sisa feodalisme, neo-kapitalisme dan imperialisme (terutama AS).

Karenanya, rakyat kita (termasuk generasi-generasi yang akan datang) akan mencatat bahwa dalam sejarah bangsa Indonesia tidak adalah « tokoh » yang melakukan berbagai politik anti-kerakyatan sebengis Suharto selama 32 tahun, sambil sekaligus mencuri harta bangsa sebanyak yang ia lakukan.  Banyak sejarawan, politisi, dan sarjana-sarjana  -- yang jujur dan berhati nurani yang bersih – yang akan menyimpulkan bahwa dalam  hal yang beginian, jelaslah bahwa Suharto tidak ada bandingannya di Indonesia, bahkan mungkin juga untuk selama beratus-ratus tahun yang akan datang.

Sebab, menurut agama apapun di dunia dan juga menurut nalar yang beradab, membunuh hanya satu orang yang tidak bersalah saja sudah merupakan kejahatan yang mengandung dosa berat dan juga harus dihukum (contohnya : pembunuhan hakim agung Syafiudin Kartasasmita oleh Tommy Suharto atau tuduhan pembunuhan Nasrudin oleh Antasari) maka mengapa justru pembunuhan jutaan orang tidak bersalah didiamkan saja? Siapa sajakah yang membunuh atau menyuruh bunuh begitu banyak orang itu ? Dan siapa sajakah yang harus bertanggung-jawab terhadap pemenjaraan begitu banyak orang tidak bersalah begitu lama itu? Semua ini juga merupakan aib besar sekali  bagi bangsa,  yang dibikin Suharto beserta  para pendukungnya.



Melupakan kejahatan Orde Baru adalah sulit sekali

Aib besar dan dosa berat ini akan tetap menghantui bangsa kita (termasuk anak-cucu kita di kemudian hari) selama para pendukung setia Suharto  (baik di kalangan militer maupun Golkar dan sejenisnya) masih belum terang-terangan dan tegas-tegas mengutuk segala kejahatan besar yang dibikin Suharto bersama Orde Barunya selama puluhan tahun itu. Mengutuk dengan tulus dan tegas segala kejahatan Suharto bersama Orde Barunya adalah salah satu cara  untuk membuang aib besar atau najis yang mengotori sejarah bangsa ini..

Sebab, segala kejahatan  yang dilakukan oleh golongan militer di bawah Suharto adalah sedemikian  bengis dan ganasnya, serta sedemikian luas dan banyaknya, sehingga tidaklah  mudah untuk dilupakan begitu saja oleh berbagai kalangan masyarakat yang mendambakan keadilan demi kebenaran. Dan sikap yang demikian itu adalah tepat, juga  benar dan adil.  Melupakan sama sekali berbagai kejahatan yang begitu besar oleh Suharto (dan konco-konconya, termasuk pimpinan Golkar) adalah salah besar ! Apalagi, atau lebih-lebih lagi (!) , bagi keluarga para korban pembunuhan massal dan keluarga para tapol,  mungkin  melupakan  itu semua  adalah sulit sekali. Sikap yang begitu itu adalah wajar, atau masuk akal dan manusiawi, dan juga bisa diterima oleh nalar yang sehat.

Dari itu semua dapat dilihat bahwa pemerintahan Suharto (yang disokong para jenderal yang pada umumnya juga berlepotan dengan berbagai macam dosa, dan pimpinan Golkar) merupakan najis yang mengotori sejarah bangsa, yang pernah ditulis dengan indah dan penuh dengan keluhuran oleh para perintis kemerdekaan sejak 1908 (Boedi Oetomo), diteruskan oleh gerakan politik Islam,  nasionalis, komunis  (ingat : Sarekat Islam, Sarekat Rakyat, pembrontakan PKI tahun 1926 terhadap Belanda, pidato Bung Karno “Indonesia Menggugat”, lahirnya Sumpah Pemuda, Proklamasi 1945). Mengingat itu semuanya, tidak salahlah kalau ada orang atau kalangan yang mengatakan bahwa dalam sejarah bangsa Indonesia Suharto bersama Orde Barunya adalah sampah yang selayaknya (bahkan, seharusnya !) dinajiskan oleh rakyat. .



30 September dan Munas Golkar di Pakanbaru

Dalam menghadapi 30 September (dan kebetulan juga akan diselenggarakannya Munas Golkar di Pakanbaru tidak lama  lagi) semua itu adalah hal-hal yang bagus sekali kita jadikan renungan bersama, baik bagi yang menentang Suharto bersama Orde Barunya,  maupun bagi mereka yang masih mempunyai ilusi untuk mendukung sisa-sisanya.

Selama ini,  sejarah Indonesia sudah membuktikan, dan akan menunjukkan bukti-bukti yang lebih jelas -- dan lebih banyak lagi -- di kemudian hari bahwa selama kekuasaan negara dan politik di dominasi oleh golongan yang berorientasi kepada politik Orde Barunya Suharto maka tidak akan mendatangkan kebaikan bagi sebagian terbesar rakyat. Sebab, berlainan sama sekali dengan politik atau ajaran-ajaran revolusioner Bung Karno, bisalah dikatakan dengan jelas dan tegas-tegas bahwa segala politik Orde Baru (yang diteruskan oleh berbagai pemerintahan) adalah pada umumnya tidak menguntungkan rakyat, bahkan anti-rakyat.

Kita semua (kalau tidak kita, maka mungkin anak cucu kita) akan sama-sama menyaksikan bahwa siapa pun menjadi pimpinan negara, atau golongan atau partai apa pun yang memegang pemerintahan di Indonesia , maka tidak akan bisa mendatangkan kebaikan bagi kepentingan sebagian terbesar rakyat kita, kalau masih tetap bepegang kepada segala hal yang berjiwa atau berbau Orde Baru, dan menentang ajaran-ajaran revolusioner Bung Karno.

Inti jiwa atau jati diri Suharto beserta Orde Barunya adalah anti ajaran-ajaran revolusioner Bung Karno, yang merupakan ajaran-ajaran pro-rakyat untuk melawan segala kekuatan reaksioner di dalam negeri maupun luar negeri. Ajaran-ajaran revolusioner pro-rakyat ini telah dituangkan dengan bagus sekali oleh Bung Karno sejak mudanya (ingat, antara lain  : idee Nasakomnya sejak tahun 1926, Indonesia Menggugat, Dibawah Bendera Revolusi jilid I dan II, Revolusi Belum Selesai jilid I dan II).  Dengan menentang dan melarang ajaran-ajaran revolusioner Bung Karno yang demikian luhur dan gemilang ini, maka jelas sekalilah bahwa Suharto bersama konco-konconya atau pendukung setianya (antara lain pimpinan Golkar)  adalah betul-betul sampah sejarah,  yang patut dinajiskan oleh bangsa, dan juga oleh anak-cucu kita.
