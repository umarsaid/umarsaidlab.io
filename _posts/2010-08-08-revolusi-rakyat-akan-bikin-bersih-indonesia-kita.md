---
layout: post
title: Revolusi Rakyat akan bikin bersih Indonesia kita
date: 2010-08-08
---

Jiwa revolusioner perjuangan 17 Agustus perlu dikobarkan

dan digelorakan lagi terus-menerus



Judul tullisan ini sepintas lalu kelihatan terlalu “galak”, atau berlebih-lebihan, atau terlalu bombastis, dan bisa menimbulkan macam-macam kesan dan pendapat atau juga pertanyaan. Sebab, kata revolusi masih bisa mengundang  macam-macam pengertian, termasuk yang serba negatif dan menakutkan. Ada yang selalu menghubungkan kata revolusi dengan situasi  yang penuh dengan keonaran, anarchi, pengrusakan, pembunuhan,atau gontok-gontokan yang berdarah-darah.

Juga, kata-kata “bikin bersih” dalam judul di atas  bisa saja diartikan secara salah sebagai  pembabatan golongan reaksioner di Indonesia dengan pemenggalan kepala musuh-musuh rakyat dalam  pembantaian secara sewenang-wenang. “Bikin bersih” di atas juga bisa dijabarkan dengan tindakan-tindakan yang serba “main hantam kromo” saja.

Padahal, revolusi atau revolusi rakyat, yang dimaksudkan dalam judul dan isi tulisan ini adalah sama sekali bukan hal-hal biadab dan tidak manusiawi seperti yang disebutkan di atas. Jauh dari itu, bahkan, bertentangan sama sekali dengan hal-hal negatif itu semua !!! Revolusi Rakyat yang disebut-sebut di sini adalah Revolusi Rakyat menurut ajaran-ajaran revolusioner Bung Karno, yang justru telah diangkat oleh MPRS menjadi Pemimpin Besar Revolusi (PBR).

Seperti yang  sudah ditunjukkan dengan gamblang sekali oleh sejarah bangsa kita, Revolusi yang dianjurkan Bung Karno adalah untuk memperkuat Negara Republik Indonesia, dan bukannya untuk memperlemah dengan adanya permusuhan antar suku, antar agama, antar warganegara, antar keyakinan politik, untuk melawan segala macam bahaya yang mengancam Pancasila dan Bhinneka Tunggal Ika.

Pengalaman selama pemerintahan di bawah Bung Karno juga membuktikan bahwa Revolusi yang digerakkan dengan ajaran-ajaran revolusionernya  telah melawan segala macam anarchi, segala kekerasan yang bersifat kriminal, segala pertumpahan darah yang tidak perlu, dan pelanggaran-pelanggaran HAM yang sewenang-wenang.

Revolusi Rakyat yang berkobar-kobar pada waktu itu  justru melindungi kepentingan rakyat, dan bertujuan jelas-jelas untuk kesejahteraan rakyat dan membela keadilan bagi semua. Artinya, selama pemerintahan di bawah Bung Karno, Revolusi Rakyat, telah dengan nyata-nyata sekali  menghormati dan menjaga dan melindungi serta menjunjung tinggi-tinggi Pancasila, Bhinneka Tunggal  Ika, dan UUD 45.



Apolitisasi  menyebabkan  lunturnya patriotisme kerakyatan

Itu semua sangat penting untuk kita renungkan bersama-sama sekarang ini, ketika kita akan memperingati Hari Kemerdekaan 17 Agustus dalam tahun 2010 ini. Banyak hal yang bisa sama-sama kita persoalkan, dan banyak pertanyaan yang bisa kita ajukan, dan banyak pula pendapat yang bisa kita utarakan.

Umpamanya, di antaranya adalah :  Apakah jiwa proklamasi 17 Agutus 45 sekarang ini masih dihayati oleh sebagian besar rakyat kita ? Apakah cita-cita dan tujuan proklamasi 17 Agustus 45 sudah benar-benar diperjuangkan oleh bangsa Indonesia? Apakah situasi negara seperti yang sekarang ini yang diinginkan oleh para pejuang kemerdekaan kita? Negara dan bangsa kita sekarang ini apa berjalan di atas relnya revolusi 45? Apa saja penyelewengan-penyelewengan yang terjadi atas tujuan revolusi 17 Agustus 45?

Kalau kita betul-betul mau jujur terhadap diri kita masing-masing, maka kita sudah selayaknya mengatakan sayang sekali bahwa sebagian  (yang cukup besar) bangsa kita sudah tidak peduli lagi sama sekali terhadap proklamasi 17 Agutus 45. Sebagian lainnya ikut merayakan dengan berbagai macam cara, hanya karena ikut-ikutan saja.

Banyak di antara bangsa kita yang apatis saja terhadap politik atau situasi negara dan bangsa, dan karenanya terjadi apolitisasi secara besar-besaran dalam masyarakat atau sebagian terbesar bangsa. Apolitisasi ini menyebabkan merosotnya patrtotisme kerakyatan, dan lunturnya atau lumpuhnya jiwa revolusioner yang pernah menjadi ciri utama dalam nation and character building di Indonesia.

Seperti kita ketahui dari pengalaman kita masing-masing, lunturnya patriotisme kerakyatan, dan lumpuhnya jiwa revolusioner bangsa Indonesia akibat berbagai politik reaksioner dan mengandung ciri-ciri fasisme yang dijalankan pemerintahan Suharto bersama jenderal-jenderalnya (dan GOLKAR) dalam jangka puluhan tahun, maka situasi negara dan bangsa menjadi serba menyedihkan bagi sebagian terbesar rakyat kita seperti sekarang ini.



Tujuan dan jiwa proklamasi telah diselewengkan dan dikhianati

Ketika kita memperingati 17 Agustus  45 dalam tahun 2010, maka kita bisa mengatakan dengan tegas, bahwa situasi bangsa dan negara yang  kita saksikan bersama sekarang ini sama sekali bukanlah yang diinginkan oleh para perintis dan pejuang kemerdekaan, yang sudah berjuang dengan pengorbanan besar. Mereka akan kecewa sekali dan mengutuk kepada mereka yang menyebabkan timbulnya situasi pembusukan besar-besaran dan sangat parah seperti sekarang ini.

Melihat keadaan negara dan bangsa dalam tahun 2010 ini bisalah kiranya kita katakan bahwa cita-cita 17 Agustus 45 telah diselewengkan, bahkan telah dikhianati, sejak pemerintahan Suharto dan diteruskan oleh berbagai pemerintahan yang menggantikannya. Tujuan yang luhur dan mulia yang dikandung proklamasi 17 Agutus telah dirusak dan  sebagian yang besar yang penting-penting telah dibuang oleh Orde Baru serta penerus-penerusnya.

Moral bangsa kita sekarang dan  moral di bawah Bung Karno

Moral bangsa kita sekarang sudah tidak seperti seperti moral bangsa di bawah pimpinan Bung Karno,   yaitu moral revolusioner dan moral perjuangan, moral pengabdian kepada rakyat, moral persatuan bangsa yang menjunjung tinggi Pancasila dan Bhinneka Tunggal Ika, Sebagian moral bangsa kita sudah rusak, sebagian lagi  sudah busuk, dan sebagian lainnya lagi sudah lapuk. Untung, masih ada juga sebagian lainnya lagi yang masih  baik dan bebas dari segala dekadensi dan degenerasi.

Mereka ini kebanyakan (artinya, tidak semuanya, tentu saja) terdiri dari macam-macam orang dari golongan yang menentang rejim militer Orde Baru, golongan pendukung politik Bung Karno, golongan kiri pada umumnya, golongan korban Orde Baru dan golongan yang menentang neo-liberalisme, golongan yang mau meneruskan Revolusi Rakyat menurut ajaran-ajaran revolusioner Bung Karno, golongan yang mau bersama-sama memperjuangkan masyarakat adil dan makmur.

Setelah 65 tahun merdeka, ketika kita merayakan 17Agustus dalam tahun 2010 sekarang ini kita melihat bahwa negara dan bangsa kita makin rusak, moral sebagian bangsa (terutama kalangan atas)  makin membusuk dari pada yang sudah-sudah sejak pemerintahan Orde Baru. Kalau kita lihat televisi dan baca media pers,  maka luar biasa banyaknya korupsi dan penyalahgunaan kekuasaan yang makin menjadi-jadi, dan di segala bidang pula, baik yang di Jakarta maupun (dan bahkan apalagi !!!) di daerah-daerah..

Hukum dilecehkan atau dilanggar sendiri oleh orang-orang  besar di bidang eksekutif, legislatif, dan judikatif di semua tingkat. Keadilan bisa diperjual-belikan dan sering dimenangkan oleh para tokoh-tokoh reaksioner dan korup, baik yang di kalangan pemerintahan maupun di kalangan swasta  Kerusakan iman di kalangan pemuka-pemuka agama pun  sangat parah dan menyedihkan (dan memalukan !!!).

Negara dan bangsa perlu ”dibersihkan” dengan Revolusi Rakyat

Kerusakan atau pembusukan ini  -- terutama kerusakan moral atau  pembusukan akhlak  --  sudah begitu luasnya dan juga begitu parahnya sehingga tidak bisa dibenahi oleh pemimpin Indonesia sekarang  yang manapun juga, atau oleh pemerintahan yang bagaimanapun juga, atau oleh partai politik yang apapun juga, dan dengan program atau sistem yang macam apapun juga. Negara kita sudah terlalu kelewatan kotornya  oleh berbagai  sampah bangsa, dan sudah terlalu parah dengan berbagai penyakit yang akut.

Negara kita yang sedang sakit oleh berbagai kotoran, penyakit dan sampah sekarang ini harus “dibersihkan dan disaring” , demi kelangsungan negara dan bangsa serta demi anak cucu kita di kemudian hari. Dan,  menurut pengalaman kita selama ini,  pembersihan negara dan bangsa ini hanya bisa dilakukan  lewat dan oleh Revolusi Rakyat, suatu cara dan jalan yang sudah ditunjukkan dengan gemilang oleh PBR (Pemimpin Besar Revolusi) Bung Karno.

Revolusi Rakyat yang demikian itu akan meningkatkan tinggi-tinggi  kesedaran politik revolusioner bangsa kita, yang bisa merupakan  satu-satunya obat ampuh guna menghapus segala macam dekadensi dan degenerasi politik maupun moral, sumber dari segala penyakit bangsa kita dewasa ini. Sebab, obat ampuh lainnya sudah sulit (bahkan, tidak mungkin )  ditemukan, baik dari ajaran-ajaran agama yang manapun,  ataupun dari teori-teori lainnya yang apapun, yang bisa ditrapkan dengan kondisi kongkrit di Indonesia sekarang.

Revolusi Rakyat yang pernah digerakkan dan dikobarkan di bawah ajaran dan pimpinan Bung Karno puluhan tahun, dan yang kemudian dimatikan atau dilumpuhkan secara khianat oleh Suharto bersama jenderal-jenderalnya,  sekarang perlu dihidupkan dan dikobarkan lagi, untuk mengadakan pembersihan negara dan bangsa secara besar-besaran.



Seluruh kekuatan demokratis harus  persatukan diri

dalam tugas “pembersihan”

Untuk menghidupkan lagi serta mengkobarkan atau menggelorakan Revolusi Rakyat ini, maka sesuai dengan ajaran-ajaran revolusioner Bung Karno, perlulah kiranya semua kekuatan progresif dan revolusioner yang ada di Indonesia, berusaha sekuat mungkin serta melalui berbagai cara dan bentuk untuk mempersatukan diri dalam tugas besar ini, yaitu pembersihan negara dan bangsa dari segala penyakit dan kotroran atau sampah.

Untuk mempersiapkan dapat digerakkannya dan dikobarkannya  Revolusi Rakyat  -- sekali lagi menurut ajaran-ajaran revolusioner Bung Karno !  -- maka seluruh kekuatan demokratis perlu melakukan berbagai macam aksi atau kegiatan untuk membela kepentingan rakyat banyak dan menentang segala macam kejahatan (antara lain korupsi) yang dilakukan oleh segenap kaum reaksioner dalam negeri dan luar negeri.

Berbagai macam aksi (antara lain demo-demo) atau kegiatan sosial-politik, yang dilakukan oleh bermacam-macam organisasi dan golongan masyarakat, seperti yang dilakukan baru-baru ini di Jakarta (dan tempat-tempat lainnya) adalah bagian penting dari pemupukan kesedaran politik dan kesedaran perjuangan revolusioner, yang merupakan salah satu di antara syarat-syarat penting untuk dilancarkannya Revolusi Rakyat.

Aksi-aksi yang berkaitan dengan soal-soal sosial-ekonomis  dan perjuangan di bidang politik, yang dilancarkan oleh massa pemuda, mahasiswa, buruh, tani, penganggur, rakyat miskin dan bagian-bagian masyarakat lainnya, adalah penting untuk mengantar aspirasi rakyat menghadapi masalah-masalah kongkrit dan mendesak dalam jangka dekat. Berbagai macam aksi untuk membongkar kasus Bank Century dan persoalan korupsi yang pernah menggelora di seluruh  negeri ( yang sekarang kelihatan agak loyo) perlu dikobarkan lagi bersama-sama. Ditambah sekarang ini dengan masalah kenaikan TDL (Tarif Dasar Listrik) dan makin membubungnya  harga-harga  kebutuhan hidup sehari-hari, dan persoalan–persoalan hangat lainnya.

Semua macam aksi tuntutan sosial-ekonomis dan perjuangan dalam bidang politik yang digelorakan oleh macam-macam golongan masyarakat ini bisa mendekatkan rakyat dengan Revolusi Rakyat yang harus dikobarkan lagi untuk diteruskan.  Aksi dan kegiatan berbagai kalangan dalam masyarakat ini juga maha penting sebagai investasi politik bagi Revolusi Rakyat sebagai satu-satunya jalan untuk membersihkan negara, menyehatkan bangsa, dan menyelamatkan generasi kita selanjutnya.

Sekali lagi, dan untuk kesekian kalinya, perlu diulangi lagi bahwa Revolusi Rakyat menurut ajaran-ajaran revolusioner Bung Karno adalah satu-satunya jalan untuk membersihkan negara dan menyehatkan bangsa kita dari segala macam penyakit dan kebusukan. Tidak ada jalan lain !!! 
