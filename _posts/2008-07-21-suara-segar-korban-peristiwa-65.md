---
layout: post
title: Suara segar korban peristiwa 65
date: 2008-07-21
---

Berikut di bawah ini disajikan kutipan dari dua berita di harian Kompas dan satu berita dari Sinar Harapan mengenai masalah tragedi kemanusiaan 1965 yang merenggut nyawa jutaan jiwa orang tidak bersalah dan dipenjarakannya ratusan ribu orang (yang juga tidak bersalah apa-apa !) dalam jangka waktu berpuluh-puluh tahun.

Dalam berita-berita ini dikemukakan pentingnya masalah pembunuhan massal ini diangkat di forum internasional oleh berbagai kalangan di dalam negeri maupun di luar negeri, dan digalangnya sinergi di antara seluruh kekuatan demokratis yang mendambakan keadilan untuk tetap menjadikan masalah 65 sebagai masalah penting yang dihadapi bangsa.

Adanya pertemuan di antara para korban (di kantor Kontras, Jakarta), yang dihadiri juga oleh berbagai tokoh organisasi yang berjuang di bidang HAM, dan yang menyuarakan hal-hal yang berkaitan dengan berbagai masalah yang berkaitan dengan pelanggaran HAM besar-besaran tahun 1965, merupakan peringatan bagi banyak kalangan bahwa sepuluh tahun sesudah jatuhnya Suharto masih banyak masalah pembantaian 65 - dan pelanggaran-pelanggaran HAM lainnya yang menyusul kemudian -- yang belum dibongkar.

Adanya seruan untuk mengangkat terus masalah 65 di forum internasional, baik oleh kalangan eksil maupun berbagai kalangan di dalam negeri merupakan dorongan sangat penting – dan suara segar -- bagi semua kalangan dan golongan yang selama ini sudah melakukan berbagai kegiatan dalam membela korban 65 dan melawan politik rejim militer Orde Baru serta sisa-sisanya.

Yang penting dan menarik dalam pertemuan di kantor Kontras itu adalah terdapatnya wawasan bahwa masalah 65 bukanlah hanya masalah para korban saja, tetapi adalah masalah seluruh bangsa, atau adalah masalah nasional. Mengangkat masalah 65 bukanlah hanya mempersoalkan pembantaian terhadap jutaaan simpatisan golongan kiri (termasuk para pendukung politik PKI dan Bung Karno) melainkan juga mengangkat berbagai politik rejim militer Orde Baru yang terbukti sudah menyebabkan kerusakan-kerusakan besar dan parah bagi bangsa dan negara.

Oleh karena itu, di samping sangat penting untuk mengangkat masalah 65 di forum internasional adalah juga penting - dan menarik - adanya seruan untuk mengajak atau mendorong kalangan muda ikut aktif dalam kegiatan mengangkat masalah 65 ini, dalam rangka usaha bersama kita untuk memupuk dan mengembangkan kesadaran tentang pentngnya HAM dalam perjuangan untuk demokrasi dan penyelenggaraan negara yang pro-rakyat.

Sebab, mengangkat masalah 65 berarti membongkar juga persekongkolan segolongan pimpinan militer dengan fihak nekolim (terutama AS) untuk menggulingkan Presiden Sukarno dan menghancurkan kekuatan pendukung utama Bung Karno, termasuk yang tergabung dalam PKI. Dari sudut ini, nyatalah bahwa yang telah dirugikan atau menjadi korban politik rejim militer Suharto adalah luas sekali, dan bukan hanya golongan PKI saja.

Dengan mengajak atau mendorong kalangan muda, dari golongan atau aliran politik yang mana pun juga, untuk aktif dalam berbagai kegiatan mengangkat masalah 65, berarti kita semua juga menyiapkan mereka sebagai generasi penerus yang berlainan sama sekali dari generasi Orde Baru.

Ini penting sekali. Sebab, sudah jelas bahwa Indonesia di kemudian hari tidak akan bisa menjadi lebih baik dari pada sekarang, selama masih dikuasai oleh politisi dan pejabat-pejabat yang bermental Orde Baru, baik yang tergabung dalam Golkar maupun partai-partai yang pernah menyokong Suharto.

Bisa diharapkan bahwa pertemuan tentang masalah 65 di kantor Kontras merupakan peristiwa yang membawa angin segar bagi berbagai kalangan yang selama ini sudah berjuang dengan gigih dan susah payah untuk meluruskan sejarah yang berkaitan dengan peristiwa 65, untuk rehabilitasi para korban Orde Baru, dan untuk menegakkan keadilan dan kebenaran. Semoga angin segar ini dapat diikuti oleh langkah-langkah lainnya untuk mengobarkan lebih besar lagi semangat bagi semuanya yang berusaha untuk hilangnya aib besar bangsa ini.

Perjuangan kita semua untuk menegakkan keadilan dan perikemanusiaan dengan mengangkat masalah peristiwa 65 adalah benar, adil, mulia dan luhur.

A. Umar Said

· * *

Berikut adalah kutipan berita-berita dari Kompas dan Sinar Harapan :

Korban 1965 Angkat Persoalan ke Forum Internasional


Kompas, 21 Juli 2008

Pertemuan para korban 1965 yang berlangsung di Kantor Kontras, Menteng, Jakarta Pusat, Senin (21/7) memunculkan sebuah pemikiran perlunya sinergi gerakan di antara para korban 1965, baik yang berada di Indonesia maupun di luar negeri. Meskipun optimistis, upaya tersebut harus diikuti oleh kesadaran bahwa perjuangan itu akan menempuh jalan panjang untuk mendapatkan keadilan.

Salah seorang korban 1965, Arif Harsana menyambut baik pemikiran tersebut. Arif adalah salah satu mahasiswa Indonesia yang mendapatkan beasiswa pada masa pemerintahan Soekarno untuk menuntut ilmu di Moskwa, Uni Soviet (sat ini Russia) pada tahun 1965. Namun, ia dilarang kembali ke tanah air setelah menolak menandatangani surat kesetiaan terhadap Soeharto pascalengsernya Soekarno.

Menurut Arif, tragedi kemanusiaan 1965 yang merenggut nyawa jutaan orang harus menjadi isu internasional. Meski peristiwanya di Indonesia, ia mengatakannya sebagai tragedi kemanusiaan dan tragedi dunia.

"Itu pembunuhan massal yang direncanakan tanpa proses hukum kepada orang yang tak bersalah. Peristiwa ini sangat penting untuk dimasukkan dalam kancah perjuangan internasional," kata Arif yang hingga saat ini masih menetap di Jerman.

Namun, lanjut Arif, perjuangan yang bersifat internasional harus ada kekuatan politik. Tanpa itu, akan mengalami kesulitan.

"Saya akan berusaha membawa persoalan ini ke forum internasional. Bagaimana caranya, saya belum bisa menyampaikannya secara detail. Tapi, perlu ada konsolidasi dari masing-masing kita, baik yang di Indonesia maupun kami yang di luar negeri," kata Arif.

Ia menekankan, gerakan untuk mendapatkan keadilan tidak hanya dilakukan para korban. Menurut dia, perlunya perjuangan yang didukung oleh keluarga korban dan generasi muda.

"Karena, kalau hanya peranan korban yang dikedepankan, maka ini hanya akan menjadi persoalan korban. Tragedi 65 adalah tragedi bagi semua orang yang merasa manusia dan mempunyai rasa kemanusiaan. Apalagi, para korban saat ini sudah beranjak tua," ujarnya.

* * *


Korban 1965 Kumpul di Menteng

Kompas 21 Juli 2008

Empat puluh tiga tahun berlalu ternyata tak membuat para korban 1965 berhenti bersuara. Mereka masih meneriakkan tuntutan akan sebuah keadilan. Sekitar 30 orang korban 1965, Senin (21/7), berkumpul di Kantor Komisi untuk Orang Hilang dan Tindak Kekerasan (Kontras), Menteng, Jakarta Pusat.

Silaturahmi kembali mereka adakan untuk mengsinergikan gerakan mereka memperjuangkan keadilan. Beberapa korban bercerita, meski telah menjalani masa tahanan berpuluh-puluh tahun, kebebasan tak mereka dapatkan sepenuhnya kala keluar dari penjara.

"Sepuluh tahun reformasi ternyata tak membawa perubahan substansi yang signifikan terhadap para korban 1965. Kebanyakan dari kita sudah bebas pada tahun 1979, tapi justru masuk lagi ke tahanan yang lebih luas. Masih banyak pembatasan-pembatasan, kita tidak boleh menjadi caleg dan sebagainya," ujar Bedjo Untung, Ketua Yayasan Penelitian Korban Pembunuhan 1965/1966.

Puluhan orang yang mengatasnamakan dirinya sebagai korban 1965 adalah mereka yang ditahan tanpa proses pengadilan karena dituduh terlibat dalam gerakan G30S PKI.

Bedjo berkisah, saat ditahan pada 1965 ia masih berusia 17 tahun. "Sekarang kita sudah masuk usia 60 tahun atau lebih. Mumpung usia masih ada, kita harus bangkit dan teruskan perjuangan untuk mendapatkan keadilan," kata Bedjo.

Korban 1965 yang terasing di Eropa, Arief Harsana, juga berbagi cerita. Saat meletusnya peristiwa G30S PKI, ia baru saja berangkat ke Moskwa, Rusia, selama dua minggu. Ia adalah mahasiswa termuda yang diutus pemerintahan Soekarno untuk menimba ilmu di negara tersebut. Namun, karena menolak menandatangani surat kesetiaan kepada Soeharto, ia dilarang kembali ke Tanah Air, bahkan paspornya ditahan.

"Padahal, kita dikirim untuk menimba ilmu dan memberikan kontribusi bagi negara. Tapi, setelah ilmu didapat justru tidak boleh kembali. Paspor saya dicabut. Akhirnya dari Moskwa karena tidak punya paspor, saya hanya bisa keluar ke Berlin Barat. Hanya ke negara itu yang tidak membutuhkan visa. Itu berlangsung dari tahun 1977-1983," ujar Arief.

Pada tahun 1983 ia bersama beberapa rekannya mendirikan Pusat Informasi Asia Tenggara. Lembaga itu berfokus pada penegakan HAM serta mewujudkan masyarakat yang demokratis dan berkeadilan sosial. "Meskipun saya berada di luar negeri, kita semua harus memperkuat jalinan dan meneruskan perjuangan. Tapi, harus memikirkan kembali kekuatan politik alternatifnya," tuturnya.

Sementara itu, aktivis Kontras, Yayan, mengatakan, Kontras selama ini berupaya untuk menghubungkan korban dengan negara melalui institusi Dephuk dan HAM dengan cara melakukan audiensi. "Kita coba suarakan suara korban. Dari proses tersebut, fokus kita adalah pendataan dan pertemuan lintas korban," ujar Yayan.

Pengungkapan kasus 1965, dalam kacamata Kontras, menjadi pijakan untuk sejarah penegakan HAM di Indonesia. "Kalau kita bilang negara kita negara hukum dan menghormati HAM, kasus ini harus diungkap. Itulah sebabnya mengapa Kontras ikut melakukan advokasi terhadap perjuangan korban 1965," katanya

* * *

Eksil 65 Harus Buka Jaringan Internasional
Sinar Harapan, 22 Juli 2008

VHRmedia.com, Jakarta - Korban peristiwa 1965 yang kini menetap di luar negeri (eksil) diminta membangun jaringan solidaritas internasional. Tekanan masyarakat internasional diharapkan mampu mendesak pemerintah Indonesia membuka kesempatan pelurusan sejarah tragedi kemanusiaan 1965.

Ketua Ikatan Keluarga Orang Hilang Indonesia (IKOHI) Mugiyanto mengatakan, kelompok eksil harus lebih membuka diri dengan berbagai jaringan di luar negeri. Hal itu pernah dilakukan oleh korban pelanggaran HAM di Filipina pada pemerintahan Presiden Ferdinand Marcos. Mereka dapat mendesak digelarnya pengadilan di Hawaii yang memutuskan pemberian kompensasi kepada seluruh korban rezim Marcos.

Kerja sama juga dapat dilakukan dengan partai politik, akademisi, dan advokat hak asasi manusia. Cara tersebut mampu membangun sinergitas perjuangan meski tekanan antikomunis di dalam negeri masih kuat. "Yang paling menentukan adalah tekad para korban sendiri," ujar Mugiyanto, Senin (21/7).

Arief Harsana, eksil yang kini menetap di Jerman, mengatakan kasus 1965 bukanlah pelanggaran HAM yang hanya menimpa korban. Kasus kemanusiaan ini seharusnya menjadi isu bersama yang juga diperjuangkan bersama.

"Itu tragedi kemanusiaan dan nasional. Itu pembunuhan massal yang direncanakan tanpa proses hukum. Oleh karena itu sangat penting dimasukkan dalam perjuangan internasional dan harus ada perjuangan politik," katanya.

Arief menyarankan para korban menciptakan kader-kader muda untuk mengembangkan kekuatan di dalam negeri. Pertimbangannya, trauma yang menimpa keluarga korban saat ini masih sangat tinggi, padahal para korban sudah berusia lanjut.

"Perlu jaringan internasional. Kami putuskan untuk temu Eropa kedua. Walaupun pentingnya jaringan di luar negeri, yang penting adalah dalam negeri, yang dapat menyatukan seluruh kekuatan demokrasi," katanya. (E1)
