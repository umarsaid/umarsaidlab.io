---
layout: post
title: Kebangkitan sepakbola Indonesia penting untuk persatuan bangsa
date: 2010-12-20
---

Di tengah-tengah berbagai macam hiruk-pikuk tentang keistimemawaan daerah Jogyakarta, tentang kasus Gayus Tambunan, tentang pergesekan di Mahkamah Konstitusi, tentang kelanjutan kasus besar Bank Century, maka selama beberapa hari sejak tanggal 16 Desember ini perhatian rakyat Indonesia banyak tertuju kepada Gelora Bung Karno. Berlainan dengan masa-masa yang lalu, terutama sejak jaman Orde Baru, Gelora Bung Karno kembali – seperti di masa-masa yang lalu,  sebelum Orde Baru --  menjadi tempat rakyat banyak untuk ramai-ramai memanifestasikan rasa kebanggaan kepada tanah-air dan mencurahkan kecintaan kepada bangsa.

Gelora Bung Karno, stadion olah-raga yang terbesar di Asia Tenggara ini dan yang berkapasitas 88.000 penonton, pada tanggal 16 dan 19 Desember telah dipenuhi orang untuk menyaksikan pertandingan semi final antara tim nasional Indonesia melawan tim nasional Filipina.  Dengan letupan kegembiraan yang menggelora, dan sorak-sorai yang gemuruh di seluruh stadion, puluhan ribu penonton itu mengelu-elukan kemenangan timnas Indonesia atas timnas Filipina dengan angka 1-0 untuk tanggal 16 dan juga 1-0 untuk tanggal 19 Desember.

Kemenangan Indonesia atas Filipina dalam laga pertama dan kedua itu menunjukkan bahwa dunia persepakbolaan negeri kita memang mulai memperoleh kemajuan yang patut dibanggakan. Kita tahu, bahwa selama ini persepakbolaan kita adalah sangat menyedihkan, karena hanya  tergolong kelas enteng atau hanya tingkat bawah saja di Asia.

Hal-hal penting sekitar pertandingan Indonesia-Filipina

Namun, di samping adanya petunjuk yang menggembirakan seluruh bangsa dengan adanya kemajuan yang diperoleh timnas di bawah asuhan pelatih Alfred Friedl (dari Austria), ada juga hal-hal yang sangat penting dengan adanya pertandingan internasional tingkat tinggi di Gelora Bung Karno ini.

Penonton kedua pertandingan (tanggal 16 dan 19 Desember) di Gelora Bung Karno yang berjumlah sekitar 88 000 itu telah menunjukkan kepada seluruh bangsa bahwa  --  sebetulnya, kalau sudah datang saatnya dan diperlukan oleh situasi tertentu  -- rakyat kita bisa benar-benar bersatu, dan mempunyai satu keinginan atau satu kemauan, tanpa mempersoalkan  keyakinan politik, agama, suku, kedudukan dan asal sosial yang dipunyai masing-masing. Mereka bersatu di sekitar tujuan bersama : mendukung tim nasional dan bersama-sama mencintai Merah Putih dan tanah-air Indonesia.

Fenomena persatuan yang demikian itu kelihatan sekali pada banyaknya orang yang memasang tanda Merah Putih pada bajunya, atau mengibarkan bendera kecil Merah Putih, atau mencat badannya atau mukanya. Di samping itu di mana-mana terdengar yel-yel atau slogan-slogan oleh ribuan orang « Indonesia menang, Indonesia menang !». Jelas sekali bahwa pertandingan sepakbola kali ini menjadi gelanggang besar untuk memanifestasikan persatuan rakyat dan bangsa. Fenomena semacam ini sudah lama sekali tidak terjadi, kecuali di bawah pemerintahan Bung Karno dulu.

Yang amat menarik dan juga patut dicermati artinya ialah didengungkannya oleh ribuan orang tidak henti-hentinya lagu « Garuda di dadaku, Garuda kebanggaanku », dengan menirukan lagu asal dari Papua « Apuse kokondao ». Seperti dapat dilihat dalam layar televisi di seluruh Indonesia, lagu « Garuda di dadaku » banyak dinyanyikan beramai-ramai oleh berbagai kalangan, dengan antusiasme yang berkobar-kobar, di banyak tempat di seluruh negeri. Di banyak tempat di seluruh negeri juga ada kebiasaan baru yang makin meluas, yaitu « nobar » (nonton bareng).

Agaknya, banyak orang yang tidak tahu bahwa lagu yang banyak dinyanyikan ketika hari-hari pertandingan timnas Indonesia di Gelora Bung Karno itu sebenarnya pernah menjadi nyanyian yang sangat populer sewaktu pemerintahan  ada di bawah presiden Sukarno. Waktu itu, lagu ini diperdengarkan  - dengan teks aslinya dalam bahasa Papua -- dalam pertemuan-pertemuan besar berbagai organisasi, dan juga diajarkan di sekolah-sekolah. Namun, selama pemerintahan Orde Baru, lagu « Apuse » ini, yang sangat populer di « jaman Orla » itu,  tidak terdengar lagi. Baru sekarang inilah kembali terdengar lagi secara meluas, dengan teksnya yang baru (« Garuda di dadaku »).

Pesta besar yang menggambarkan persatuan rakyat

Fenomena yang penting lainnya yang kelihatan hari-hari terakhir ini adalah sangat banyaknya kaos merah yang dipakai oleh berbagai kalangan masyarakat, yang dihiasi dengan lambang Garuda. Kita tahu bahwa dalam lambang Garuda kita terdapat simbul-simbul yang menggambarkan Pancasila. Sangat banyaknya penjualan kaos yang berlambang Garuda, baik di Jakarta maupun di kota-kota besar lainnya (sampai banyak pedagang kaos kehabisan stock) juga merupakan indikasi bahwa  olahraga sepakbola sudah membikin banyak orang seperti kena hypnose.

Tanpa diketahui oleh banyak orang yang memasang kaos merah dengan lambang Garuda itu, dengan memasang lambang Garuda di dada mereka, sebenarnya juga merupakan penghormatan (setidak-tidaknya : ingatan) tidak langsung kepada Bung Karno, yang menekuni pembuatan lambang  ini dan menginstruksikan penyempurnaan gambar lambang Garuda kepada sebuah Panitia, berdasarkan idee-idee dasar Sultan Hamid II dari Pontianak.

Juga dari tekad banyak orang untuk bersusah-payah antri panjang sejak pagi untuk berebut membeli tiket menggambarkan betapa besarnya dukungan dan kecintaan berbagai kalangan rakyat (termasuk para supporter wanita) terhadap tim nasional kita. Bahkan, banyak juga yang datang ke Jakarta dari daerah-daerah yang jauh, karena mereka mau menonton langsung di stadion, dan tidak puas hanya dengan melihat di televisi saja.

Bolehlah kiranya dikatakan bahwa pertandingan sepakbola kali ini  betul-betul menjadi pesta besar-besaran yang memanifestasikan persatuan rakyat yang bersifat pluralisme, dan memperlihatkan kesatuan bangsa yang dewasa ini terasa goyah, tercabik-cabik oleh berbagai persoalan besar di bidang politik, ekonomi, sosial, agama, dan  -- terutama !!! -  oleh korupsi yang sudah lama merajalela.

Bung Karno, pemimpin yang menjunjung tinggi-tinggi olahraga

Sekarang di Indonesia, sepakbola menjadi instrumen yang ampuh untuk membangkitkan kembali nasionalisme yang sudah terasa mulai memudar, menggelorakan lagi kebanggaan menjadi bangsa yang besar yang pernah menjadi ciri-ciri utama bangsa selama di bawah Bung Karno. Jadi, sepakbola tidak hanya  menjadi hiburan bagi sebagian terbesar rakyat kita, melainkan juga menjadi perekat persatuan bangsa.

Kebangkitan persepakbolaan Indonesia kali ini, yang dimulai dengan kemenangan atas timnas Filipina mengingakan kembali kepada segala kebesaran dan keagungan Bung Karno, satu-satunya pemimpin Indonesia yang sejak dini sekali menunjukkan perhatian besar kepada sport sebagai bagian penting dari kehidupan dan kebudayaan bangsa, yang dirumuskannya dengan « nation and character building ».

Bung Karno-lah yang dalam tahun 1950 mempunyai gagasan, tekad untuk membangun bagi bangsa stadion raksasa yang sekarang terkenal sebagai Gelora Bung Karno. Pada waktu itu, ketika berbicara dengan Perdana Menteri Uni Soviet, Nikita Krushchev, yang berkunjung ke Indonesia, ia mengatakan bahwa ia menginginkan didirikannya sebuah  stadion yang besar, megah,, dan bisa menjadi kebanggaan seluruh bangsa sampai ratusan tahun

Keinginannya yang kuat untuk membuat sesuatu yang besar dan megah bagi seluruh bangsa ini kemudian terealisasi dengan pemberian kredit jangka panjang sebesar 12, 5 juta dollar AS (jumlah yang sangat besar waktu itu), dan bantuan pengiriman ahli-ahli Soviet di berbagai bidang pembangunan . Untuk menentukan lokasi tempat didirikannya stadion besar ini Bung Karno dengan didampingi oleh arsitek ternama Friedrich Silaban (orang dari keluarga Tapanuli dan penganut agama Kristen Protestan) terbang dengan helikopter berputar-putar mengelilingi udara di atas Dukuh Atas, Pondok Pinang, Cinere, dan Senayan. Akhirnya dipilihlah Senayan setelah berdiskusi panjang dengan arsitek yang dibanggakannya itu.

GANEFO yang mengangkat nama bangsa Indonesia

Pandangan Bung Karno tentang pentingnya olahraga bagi kehidupan bangsa tidak hanya terlihat pada pembangunan stadion yang besar yang nama  lengkapnya adalah Stadion Utama Gelanggang Olahraga Bung Karno, tetapi  juga dengan diselenggarakannya Asian Games dalam tahun 1962 dan kemudian dengan penyelenggaraan GANEFO (Games of the New Emerging Forces), dalam bulan November 1963.

Sebagai tokoh raksasa dalam perjuangan melawan kolonialisme dan imperialisme sejak mudanya, Bung Karno mengatakan berkali-kali dalam pidato-pidatonya bahwa juga sport tidak bisa dipisahkan dari politik. Itu sebabnya, untuk Asian Games dalam tahun 1962 di Jakarta itu Indonesia melarang ikutnya Israel dan Taiwan. Ini untuk melaksanakan politik yang bersahabat dengan RRT (Republik Rakyat Tiongkok) dan menunjukkan simpati yang besar kepada perjuangan rakyat-rakyat Arab melawan Israel.

Sikap Indonesia ini diprotes oleh Komite Olimpiade Internasional (KOI) yang mempertanyakan keabsahan (legitimasi) Asian Games di Jakarta. Sebagai buntutnya akhirnya Indonesia diskors oleh KOI untuk mengikuti Olimpiade di Tokio tahun 1964. Sudah tentulah Bung Karno marah sekali dengan keputusan Komite Olimpiade Internasional dan Federasi Asian Games yang demikian ini. Karenanya Bung Karno menyatakan Indonesia keluar dari Komite Olimpiade Internasional dan  satu tahun kemudian Indonesia mengadakan Olimpiade tandingan dalam bulan November 1963, yang dinamakan GANEFO.

Keberanian Bung Karno untuk menyatakan Indonesia keluar dari Komite Olimpiade Internasional dan juga memutuskan menyelenggarakan olimpiade tandingan di Indonesia ini merupakan gebrakan politik  yang sangat mengejutkan  waktu itu dan yang sangat besar gemanya di dunia internasioanal, terutama di negara-negara Asia-Afrika-Amerika Latin. Nama, prestise, atau kehormatan Indonesia dan juga nama Bung Karno sering disebut-sebut dan dielu-elukan oleh rakyat di banyak negara.

Meskipun dunia Barat (beserta sekutu-sekutunya di berbagai negeri) berusaha memboikot  GANEFO dengan macam-macam cara dan jalan, GANEFO bisa berlangsung dengan sukses. Seluruh kekuatan demokratis dan progresif (atau kiri) di Indonesia waktu itu sepenuhnya mendukung dan ikut ambil bagian dalam bermacam-macam kegiatan dalam projek besar Bung Karno di bidang olahraga dan politik waktu itu.

GANEFO yang pernah menjadi kebanggaan rakyat Indonesia (dan rakyat berbagai negara lainnya) telah diikuti oleh 2.200 atlit dari 48 negara di Asia, Afrika, Amerika Latin dan Eropa, dengan 450 wartawan dari berbagai negara.  Ini merupakan peristiwa yang amat besar dan membanggakan dalam sejarah bangsa Indonesia.

Dari penyelenggaraan GANEFO dalam tahun 1963 (artinya, 2 tahun saja sebelum G30S) kelihatan jelas bahwa Bung Karno adalah satu-satunya pemimpin Indonesia yang menjunjung dunia olahraga Indonesia ke tingkat tinggi sekali sehingga mendapat banyak penghargaan dari dalam dan luar negeri.
Di bawah pimpinan Bung Karno negara dan bangsa Indonesia bisa memperlihatkan keunggulannya di banyak bidang.

Stadion Senayan menjadi Gelora Bung Karno

Oleh karena itu, kita bisa mengerti bahwa selama pemerintahan rejim mIliter Suharto, tidak hanya nama Bung Karno saja yang diusahakan dihapus dari sejarah bangsa, melainkan juga karya-karya agungnya dan politiknya yang amat penting dan berguna bagi rakyat (termasuk sejarah GANEFO).Semua golongan reaksioner dalam negeri waktu itu menyerang politik Bung Karno itu sebagai tindakan yang buang-buang uang dan tenaga secara percuma saja, atau megalomania, dan mabuk dengan yang serba « mercu suar » yang banyak disebut-sebut waktu itu.

Dalam kaitan ini patutlah kiranya kita sama-sama ingat bahwa bahwa nama Gelora Bung Karno dilarang disebutkan dalam masa pemerintahan Orde Baru selama 32 tahun, dalam rangka de-Sukarnoisasi secara besar-besaran. Artinya, apa yang « berbau » Bung Karno harus dilarang. Baru dalam tahun 2001, presiden Abdurrachman Wahid merobah nama stadion Senayan menjadi Gedung Olahraga Bung Karno.

Arti gemuruhnya « Garuda di dadaku » dan kenangan kepada  masa lalu

Kali ini, dengan diselenggarakannya dua pertadingan Indonesia melawan Filipina di Gelora Bung Karno, dan dengan kemenangan tim nasional Garuda, maka seluruh suasana selama beberapa hari itu terasa seperti pesta besar rakyat yang mengingatkan kepada masa-masa jayanya dunia sport Indonesia sewaktu Asian Games di Jakarta dan GANEFO.

Dengan gemuruhnya kumandang lagu « Garuda di dadaku » di seluruh negeri, dan seringnya disebut-sebut Gelora Bung Karno oleh banyak kalangan, maka bisa dikatakan bahwa sekarang nampak adanya kebangkitan kembali dunia olahraga Indonesia, seperti yang pernah dibanggakan di masa lalu. Bersamaan dengan itu, atau seiring dengan itu, kebangkitan kembalinya dunia olahraga kita juga berarti  - secara langsung atau tidak langsung, dan setapak demi setapak  – hidup kembalinya berbagai gagasan-gagasan besar Bung Karno, yang sudah berpuluh-puluh tahun diusahakan padam atau mati  oleh Orde Baru.

Nantinya, sejarah bangsa kita akan menyaksikan bahwa juga melalui  Gelora Bung Karno-lah lambat-laun akan dikubur, untuk selama-lamanya,  berbagai  hal yang berbau Orde Baru.  Itulah  arah perjalanan sejarah rakyat Indonesia selanjutnya.
