---
layout: post
title: Para korban 65 menggugat 5 Presiden RI
date: 2005-03-13
---

Berbagai golongan dari korban 65 telah menggugat lima presiden, mulai dari mantan Presiden Suharto, B.J. Habibi, Abdurrahman Wahid, Megawati, sampai Presiden Susilo Bambang Yudoyono. Gugatan mereka ini ( yang merupakan class action) telah didaftarkan oleh Lembaga Bantuan Hukum (LBH) Jakarta, selaku kuasa hukum mereka, di Pengadilan Jakarta Pusat pada tanggal 9 Maret 2005.

Peristiwa ini mempunyai arti penting untuk mengingatkan banyak fihak di Indonesia dewasa ini bahwa ketika bangsa kita sedang menghadapi berbagai masalah besar dan rumit (antara lain : korupsi yang terus merajalela, penanganan soal bencana Aceh, pertikaian dengan Malaysia, geger soal kenaikan harga BBM) masih ada soal serius lainnya yang harus ditangani bersama. Yaitu, soal nasib para korban peristiwa 65, beserta keluarga atau sanak-saudara mereka, yang jumlahnya diperkirakan tidak kurang dari 20 juta orang.

Kalau kita renungkan bersama-sama secara dalam-dalam, dan dengan fikiran yang jernih pula, maka akan kelihatanlah bahwa adalah tindakan yang masuk akal, wajar, benar, sah, dan adil (!!!) , kalau para korban 65 ini mengajukan gugatan (class action) terhadap Pemerintah RI, mengenai pelanggaran hak-hak fondamentil mereka sebagai manusia dan hak-hak elementer mereka sebagai warganegara RI.

Sebab, pelanggaran HAM besar-besaran ini sudah berlangsung terus-menerus selama hampir 40 tahun, tanpa ada tindakan nyata untuk menghentikannya atau memperbaikinya, dari fihak pemerintahan yang sudah silih berganti, sejak zaman rezim militer Suharto sampai pemerintahan yang dipimpin oleh Presiden SBY sekarang ini.

Karena banyaknya, maka tidak terhitung lagilah jumlah orang mengalami berbagai penderitaan yang menyedihkan selama puluhan tahun itu, sebagai akibat dari politik yang tidak berperikemanusiaan dari para penguasa tinggi RI pada waktu itu.

Kita semua patut menyadari bahwa perlakuan berbagai pemerintah (yang disokong oleh sebagian masyarakat) terhadap para korban 65 selama ini adalah aib besar bangsa kita, yang harus kita buang jauh-jauh. Kita tidak boleh - atau tidak patut !!! - merasa bangga dengan tindakan sewenang-wenang yang telah dilakukan terhadap begitu banyak orang yang tidak bersalah apa-apa, dan dalam jangka waktu yang begitu lama pula. Kasus para korban 65 merupakan dosa besar. Dosa besar yang telah dibuat (atau diteruskan) oleh berbagai pemerintahan RI ini, tidak boleh diwariskan kepada generasi-generasi bangsa kita yang akan datang.

## Arti Gugatan Terhadap 5 Presiden RI

Dengan pengertian semacam itulah kita bisa melihat arti penting gugatan para korban 65 lewat Lembaga Bantuan Hukum Jakarta yang disampaikan kepada Pengadilan Negeri Jakarta Pusat. Gugatan ini tidak hanya merupakan usaha untuk menghentikan berbagai macam penderitaan yang sudah berlarut-larut – batin atau fisik – bagi puluhan juta orang, dan memperjuangkan hak-hak mereka yang sah sebagai warganegara RI dan juga sebagai manusia biasa seperti orang lainnya.

Gugatan kepada 5 Presiden RI ini juga merupakan sumbangan untuk menjadikan bangsa kita sebagai bangsa beradab dan patut dihormati oleh bangsa lain di dunia. Karena, apa yang terjadi dengan pembunuhan besar-besaran dan penahanan jangka lama terhadap jutaan orang tahun 65 adalah suatu kebiadaban yang tidak tanggung-tanggung yang dilakukan oleh sebagian kecil bangsa kita terhadap sesama anak bangsa.

Adalah dosa besar bagi kita semua untuk tidak mengutuk kebiadaban ini. Tidak mengutuknya bisa diartikan menyetujuinya. Dan menyetujui kebiadaban ini adalah sama dengan memerosotkan diri kita menjadi sama-sama biadab pula. Apalagi, membiarkan terus atau ikut melestarikan kebiadaban ini, dosa kita menjadi makin berlipat-ganda.

Selama masalah akibat pembunuhan besar-besaran dan penahanan sewenang-wenang berjangka lama terhadap begitu banyak orang tidak dihentikan atau diselesaikan secara baik, maka tidak pantaslah kiranya bagi bangsa kita untuk membanggakan diri sebagai bangsa yang sejajar dengan bangsa lain yang menghormati hak-hak manusia.

Sebenarnya, kalau kita fikirkan dengan hati yang jernih, maka kita akan bisa melihat bahwa gugatan para korban 65 terhadap 5 Presiden RI itu sama sekali tidak merugikan siapa-siapa. Gugatan ini, yang tujuannya untuk menghilangkan aib besar dan menghapus dosa berat bangsa kita, sebenarnya menguntungkan kita semuanya secara keseluruhan. Hilangnya aib besar dan hapusnya dosa berat ini akan mendatangkan kebaikan untuk memupuk rasa kebersamaan antara kita sesama warganegara RI dan rasa kesetaraan sebagai sesama manusia.

Mengingat masih berlarut-larutnya penderitaan para korban 65, maka terasa sekali bahwa negara dan bangsa kita kita tidak pantas (dan tidak pula berhak!!!) untuk menyombongkan diri sebagai bangsa yang dapat dicontoh oleh banyak negeri lainnya. Ketika bangsa kita akan menyelenggarakan (dalam bulan April yad) secara besar-besaran peringatan 50 tahun Konferensi Bandung, dengan dihadiri oleh banyak kepala negara dan tokoh-tokoh terkemuka dari ratusan negeri di seluruh dunia, maka kelihatan lebih menonjol lagi citra buruk kita.

Gugatan para korban 65 terhadap 5 Presiden RI merupakan langkah penting, untuk -- setahap demi setahap dan sebagian demi sebagian -- menangani masalah besar, yang diwariskan oleh regim militer Suharto, dan yang sampai sekarang masih tetap merupakan duri atau racun bagi persatuan bangsa. Adalah betul-betul omong-kosong belaka kalau ada orang bicara tentang perlunya persatuan bangsa, demokrasi, atau hak-hak manusia, tetapi masih menyetujui dilestarikannya segala perlakuan yang tidak manusiawi terhadap para korban 65. Sebab, perlakuan terhadap para korban 65 adalah, pada hakekatnya, satu KEJAHATAN.

## Mereka Yang Menggugat 5 Presiden RI

Menurut harian Suara Pembaruan (10 Maret 2005), para koran 65 yang telah mengajukan gugatan itu terdiri dari berbagai kelompok. Mereka itu terdiri dari :

* Adum Sadelo dan Marwoto, mewakili kelompok yang dipaksa mengundurkan diri atau diberhentikan dan secara sepihak dari pekerjaannya tanpa diberi gaji, pesangon atau tunjangan.
* John Pakasi dan Mbong Soedijatmo, wakil kelompok I dan II, yang diberhentikan kerja tetapi belum mendapatkan pensiun PNS, TNI dan Polri.
* Harifin dan Suswardoyo mewakili kelompok korban penelitian khusus dan tidak bersih lingkungan sehingga dikeluarkan dari tempat kerjanya, selanjutnya tidak dapat pekerjaan lagi.
* Ahmad Soebarto dan Tjasman Setiaprawiro, wakil dari kelompok yang dicabut tunjangan veteran dan jasa-jasa kepahlawannnya karena dituding terlibat PKI.
* Margondo dan Hartono TR, wakil kelompok yang dirampas tanah, bangunan yang dirusak, dibakar, dan dihilangkan harta bendanya.
* M Fatah Maryunani dan Gorma Hutajulu, wakil kelompok yang dikeluarkan dari sekolah dan tidak dapat melanjutkan jenjang pendidikan karena dituduh terlibat G30S PKI. Atau dituduh tidak bersih lingkungan karena diduga orangtuanya terlibat PKI.
* Misbach Thamrin dan Pramudya Ananta Toer, wakil kelompok yang dihambat kreasi seninya. Juga yang dihambat mempublikasikan hasil-hasil pemikirannya berupa buku-buku dan seni pertunjukan.

Jelaslah bahwa gugatan 7 kelompok korban 65 ( korban stigma PKI) terhadap 5 Presiden RI mencakup banyak sekali politik atau kebijaksanaan pemerintahan Orde Baru di berbagai bidang, yang mengakibatkan banyak penderitaan dan kerugian besar bagi sejumlah besar orang.

Sekarang, secara garis besar bolehlah dikatakan bahwa Orde Baru sudah dinajiskan oleh sebagian besar rakyat kita, sejak dijatuhkan dalam tahun 1998 berkat perlawanan oleh generasi muda. Berbagai politiknya sudah ada yang dirobah, diganti, atau dicampakkan. Tetapi, masih banyak juga undang-undang, atau peraturan-peraturan, atau kebijakan-kebijakan Orde Baru, yang sampai sekarang masih belum direformasi, termasuk yang berkaitan dengan perlakuan terhadap para korban 65, dan para eks-tapol, beserta keluarga mereka.

Sekarang, makin banyak orang yang sadar atau yakin bahwa Orde Baru telah melakukan banyak sekali kejahatan yang serius, atau berbagai pelanggaran HAM yang parah, atau kesalahan-kesalahan yang besar, dan dalam jangka puluhan tahun pula. Salah satu di antara kesalahan besar atau pelanggaran HAM yang parah itu (sekali lagi, yang hakekatnya adalah : KEJAHATAN) yalah perlakuan terhadap puluhan juta orang yang dicap sebagai PKI atau simpatisan PKI.

Dalam rangka ini patut dicatat sikap positif atau pendapat yang berharga dari berbagai tokoh lembaga penting negara kita (antara lain : Bagir Manan dari Mahkamah Agung, dan Abdul Hakim Garuda Nusantara dari Komnas HAM) yang berkaitan dengan kasus para korban 65.

## Para Korban 65 Angkat Suara

Gugatan para korban 65 terhadap 5 Presiden RI mempunyai arti penting lainnya, yang juga merupakan perkembangan yang menggembirakan. Gugatan ini merefleksikan kebangkitan para korban, yang sudah puluhan tahun diliputi trauma atau ketakutan, karena hebatnya penindasan terhadap mereka. Seolah-olah dengan gugatan ini mereka bangkit dan tegak berdiri sambil menuding : « kalian di fihak yang salah, dan kami sekarang menuntut keadilan !».

Kalau selama lebih dari 30 tahun jutaan orang ini tidak bisa bersuara, maka melalui gugatan terhadap 5 Presiden RI mereka seperti bersuara lantang sekali dengan berani. Suara lantang ini merupakan dorongan moril yang besar bagi para korban yang begitu banyak jumlahnya itu. Kepada mereka itu semuanya, sudah sepatutnya ikut kita sampaikan pesan : kalian benar dan mereka yang salah !

Tetapi, kita semua mesti menyadari bahwa perjuangan ini tidak mudah. Namun, terlepas dari apakah aksi-aksi lewat gugatan ini akan cepat mencapai hasil atau tidak di kemudian hari, tetapi tetap perlu terus kita perjuangkan. Sebab, ini untuk kepentingan seluruh bangsa kita dewasa ini, dan juga demi kepentingan generasi kita yang akan datang. Jadi, perjuangan ini mungkin berjangka panjang, dan mungkin harus menempuh jalan yang berliku-liku.

Itulah sebabnya, alangkah baiknya kalau sebagian terbesar korban 65 bisa menyatukan diri di belakang gugatan terhadap 5 Presiden RI ini, tanpa terlalu mementingkan perbedaan atau terlalu mendahulukan kepentingan masing-masing golongan. Demikian juga, dari seluruh kekuatan demokratis, baik yang berbentuk partai, LSM, ataupun segala macam organisasi kemasyarakatan lainnya, diharapkan dukungan mereka terhadap gugatan ini, dalam beraneka bentuk dan cara. Karena, ini adalah masalah besar perikemanusiaan, masalah serius peradaban, masalah penting kesedaran berbangsa. Kita semua berkepentingan bahwa di Republik yang kita cintai bersama ini bisa sungguh-sungguh tercermin itu semua secara nyata.

Dan kepada Direktur LBH Jakarta, Sdr. Uli Parulian Sihombing, yang ikut memikul tugas untuk memperjuangkan gugatan terhadap 5 Presiden RI ini, patut kita sampaikan : « Selamat berjuang ! Perjuangan ini adalah baik, benar, adil, dan karenanya, adalah sah pula ».
