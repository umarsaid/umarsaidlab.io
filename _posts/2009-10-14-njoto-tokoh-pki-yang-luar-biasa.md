---
layout: post
title: Njoto tokoh PKI yang luar biasa !
date: 2009-10-14
---

Majalah Tempo edisi 5-11 Oktober 2009 telah menyajikan edisi khusus “Njoto dan tragedi G30S”, yang terdiri dari 36 halaman (dari halaman 49 sampai 85). Edisi khusus ini merupakan hasil dari usaha banyak orang yang sudah mencurahkan waktu dan tenaga, untuk menghimpun bermacam-macam bahan mengenai sejarah hidup Njoto, wakil ketua II  CC PKI, yang dibunuh oleh militer secara gelap, tanpa pemeriksaan dan tanpa pengadilan, dan karenanya tidak diketahui kuburannya sampai sekarang.

 “Njoto dan tragedi G30S” bisa dibaca sebagai bunga rampai yang disusun oleh tim edisi khusus Tempo, sesuai dengan gaya dan selera jurnalistik majalah ini,  tentang berbagai soal yang berkaitan dengan keluargannya, kegiatannya sebagai pimpinan PKI,  kelebihan-kelebihannya sebagai politikus sekaligus sebagai seniman, hubungannya yang erat dengan Bung Karno, masalah-masalah pribadinya yang berkaitan dengan asmara, dan banyak segi-segi lainnya, termasuk masalah G30S.

Dengan dibuatnya edisi khusus “Njoto dan tragedi G30S” maka masyarakat umum mendapat tambahan bahan bacaan atau informasi, yang bisa memperkaya pengetahuan kita bersama mengenai putra bangsa yang termasuk brilian di banyak bidang ini. Mudah-mudahan, inisiatif redaksi Tempo dengan menyajikan edisi khusus tentang Njoto ini menggugah berbagai orang (terutama yang pernah mengenalnya dari dekat) untuk membuat kesaksian atau karya-karya lainnya dalam macam-macam bentuk  mengenai tokoh komunis yang luarbiasa ini.  

Sebab, Njoto adalah salah satu di antara banyak sekali (artinya , puluhan ribu atau bahkan ratusan ribu ?) kader-kader PKI  -- dari berbagai tingkat -- yang sudah dibunuh di banyak tempat di seluruh Indonesia, yang umumnya terdiri dari pejuang-pejuang rakyat yang sudah lama berjuang dengan tulus ikhlas dan tidak mementingkan diri sendiri, bersama-sama Bung Karno, untuk tercapainya cita-cita masyarakat adil dan makmur.

Tulisan berikut di bawah ini adalah sekelumit dari kesaksian saya, sebagai seorang yang mendapat kesempatan untuk mengenalnya dan juga  pernah bersama-sama bekerja di Harian Rakyat, dan berhubungan dekat  dengannya dalam berbagai kegiatan internasional untuk menjalankan garis politik Bung Karno (antara lain : Konferensi Wartawan Asia-Afrika, Konferensi Pengarang Asia-Afrika, Konferensi Internasional Anti Pangkalan Militer Asing).

Sebagian kecil dari tulisan yang berikut di bawah ini telah diambil oleh redaksi Tempo untuk dirangkum dalam edisi khusus “Njoto dan tragedi G30S” tersebut.



Umar Said


Paris,  14 Oktober 2009



*  * *                                                                                                                                                                                                                         

Saya mengenal dekat Bung Njoto sejak saya diajaknya untuk menjadi wartawan di Harian Rakyat akhir  tahun 1953, yaitu sesudah kembali dari perjalanan saya ke luarnegeri untuk pertama kalinya. Waktu itu  saya menghadiri Konferensi Hak-hak Pemuda Sedunia di Wina (Austria) sebagai anggota (merangkap penterjemah bahasa Inggris) delegasi pemuda Indonesia, mewakili golongan wartawan muda. Sesudah konferensi di Wina selesai, bersama-sama Bung Suryono Hamzah (yang ketika peristiwa 65 terjadi bekerja di Sekretariat Negara urusan tamu-tamu negara) saya pergi ke Bukares (Rumania) untuk mempersiapkan ikut sertanya Indonesia dalam Festival Pemuda Sedunia. Sesudah selesai mengunjungi Bukares kami berdua diundang oleh Gabungan Pemuda Demokratik se-Tiongkok untuk berkunjung ke Peking.

Kunjungan kami berdua ke Peking dalam tahun 1953 ini sangat besar pengaruhnya bagi perkembangan pandangan politik  saya tentang berbagai hal, terutama mengenai Tiongkok.  Saya datang di Peking ketika kemerdekaan Tiongkok baru 4 tahun diproklamasikan oleh Mao Tsetung di Tian An Men, yang waktu itu masih merupakan lapangan yang tidak begitu besar.   

Selama dalam perjalanan itu saya membuat  reportase bersambung (terdiri dari banyak artikel) yang dimuat oleh suratkabar SIN PO, yang membiayai tiket pesawat terbang KLM  untuk saya dari Jakarta-Zurich. Rupanya tulisan-tulisan saya ini menarik perhatian banyak orang, termasuk Bung Njoto, yang waktu itu sudah aktif sejak lama di Harian Rakyat. Dalam suatu kesempatan, waktu bertemu dengannya (di pertengahan atau akhir 1953), ia menawari saya untuk bekerja sebagai anggota redaksi Harian Rakyat. Sejak itulah saya sering bertemu dengannya, terutama pada malam hari ketika ia datang untuk menulis artikel atau editorial dan pojok untuk Harian Rakyat, sesudah kesibukannya yang padat sekali sebagai pimpinan PKI di samping berbagai kegiatannya yang lain-lain.

Kerja bersama di Harian Rakyat

Dari pekerjaan saya sebagai anggota redaksi Harian Rakyat antara akhir  1953 sampai pertengahan 1956, dan sering bekerja bersama-sama dengannya, saya kagum terhadap sosok pemimpin PKI yang masih muda sekali (sekitar 27 tahun waktu itu) tetapi yang kelihatan hebat sekali dalam banyak hal. Dalam diskusi-diskusi tentang berbagai hal yang berkaitan dengan isi suratkabar partai ini  kelihatan sekali ia menguasai banyak soal secara baik. Sudah tentu, sebagai wakil ketua CC PKI, ia tahu banyak mengenai seluk-beluk dunia politik Indonesia  waktu itu.

Tetapi, dalam diskusi-diskusi dalam redaksi itu juga kelihatan segi-seginya  yang kuat dan menonjol dalam masalah sastra, musik, atau seni pada umumnya. Bagi saya, Bung Njoto adalah seorang seniman yang ulung di samping seorang politikus yang sangat brilian, dan seorang publisis atau wartawan dan sekaligus sastrawan yang berbakat. Karena itu, tidak salahlah kiranya Bung Karno memilihnya sebagai pembantu dan teman seperjuangannya.

Seingat saya, selama saya bekerja di redaksi Harian Rakyat, ia tidak pernah marah-marah dengan kasar terhadap siapa saja. Sidang-sidang redaksi di bawah pimpinannya selalu  berlangsung dengan sejuk dan menyenangkan. Yang sampai sekarang masih saya ingat adalah peringatannya dalam menulis sesuatu. Ia mengatakan supaya semua tulisan-tulisan diperiksa berkali-kali sebelum diserahkan kepada redaktur yang bertanggungjawab. Sebab, sekali ada kesalahan, maka akan besar akibatnya kalau sudah cetak.

Seingat saya lagi, tidak seorang pun di antara anggota redaksi Harian Rakyat, yang menganggap sosok Bung Njoto sebagai orang yang sombong atau sok, walaupun ia adalah wakil ketua CC PKI dan dekat dengan Bung Karno, dan disukai oleh banyak orang di Lekra atau berbagai kalangan yang luas lainnya.  

Dalam pergaulan di luar pekerjaan redaksi, Bung Njoto juga cukup luwes, lincah dan hangat. Saya masih ingat ketika kami sering bersama-sama dengannya makan nasi dengan gulai kambing di jalan Gondangdia Lama pagi-pagi sekali (masih gelap) sesudah koran mulai dicetak. Atau makan bakmi di jalan Krekot. Tetapi suatu pengalaman yang tetap terkenang dalam ingatan saya sampai sekarang (sesudah lebih dari 50 tahun) ialah ketika saya berangkat dengan pesawat terbang menuju Padang dalam tahun 1956 untuk mulai memimpin suatu suratkabar.

“Orang luar” memasuki daerah Minangkabau

Ceritanya begini. Pada suatu hari, dalam pertengahan tahun 1956, Bung Njoto memberitahukan kepada saya bahwa ada suratkabar di Padang (Harian   PENERANGAN) , yang dterbitkan oleh pimpinan Baperki cabang Padang,  yang juga seorang  tokoh Katolik, bernama Lie Oen Sam, membutuhkan penggantian pimpinan redaksi.  Ia menawarkan lowongan yang baik ini kepada saya, dan menanyakan apakah saya bersedia menerimanya. Ia menyakan itu karena waktu itu di Sumatra Barat sedang terjadi ketegangan karena pergolakan politik yang pada pokoknya menentang berbagai politik pemerintah pusat (Jakarta). Dan suara-suara anti Bung Karno dan anti-PKI juga sudah sejak lama berkumandang dengan lantang waktu itu.

Bagi saya, tawaran Bung Njoto ini adalah suatu hal yang membanggakan hati saya. Sebab, waktu itu saya berumur 28 tahun, dan belum kawin.  Dan walaupun pengalaman saya sebagai wartawan baru 5 tahun lebih (di harian Indonesia Raya dari 1950-1953, dan di Harian Rakyat dari akhir 1953 sampai pertengahan 1956) saya bertekad untuk mengambil risiko dengan memimpin suatu suratkabar di daerah yang sedang bergolak dan anti-Sukarno sekaligus anti-PKI.

Rupanya, Bung Njoto juga cukup mengerti akan risiko yang mungkin saya hadapi dengan pekerjaan memimpin suratkabar di daerah yang dikuasai secara mutlak oleh Masyumi (bersama PSI)  yang bersekutu dengan golongan militer yang dipimpin oleh Letkol Achmad Husein. Oleh karena itu saya dianjurkannya untuk bertemu dan minta nasehat-nasehat kepada Bung Bachtarudin, anggota CC PKI yang berasal dari Sumatra Barat dan yang cukup terkenal di daerahnya karena perjuangannya di jaman revolusi 45 sebagai komisaris besar polisi.

Keprihatinan (atau kehati-hatian) Bung Njoto terhadap segala risiko yang saya hadapi di daerah yang sedang bergolak waktu itu adalah wajar dan ada dasarnya. Sebab, saya adalah wartawan yang terang-terangan telah bekerja di Harian Rakyat, organ sentral PKI. Ditambah lagi saya berasal dari Jawa Timur. Bagi “orang luar” semacam saya, ditambah dengan suasana daerah yang tidak ramah kepada golongan yang mendukung politik Bung Karno pada waktu itu, adalah sulit sekali untuk “memasuki “ daerah Minangkabau.  

Berkat bantuan Bung Njoto saya beruntung mendapatkan berbagai nasehat yang berharga sekali  bagi saya dari Bung Bachtarudin untuk memasuki masyarakat Minangkabau dengan memimpin Harian PENERANGAN, sampai tahun 1960.  

Bahwa Bung Njoto memberi perhatian besar kepada pekerjaan baru saya sebagai pemimpin redaksi di daerah Masyumi dan PSI yang bersekutu dengan Letkol Achmad Husein ( yang kemudian menjadi pusat pembrontakan PRRI) termanifestasi ketika ia memerlukan mengantar keberangkatan saya ke Padang di lapangan terbang Kemajoran.

 Tindakannya yang demikian itu sangat membesarkan hati saya waktu itu. Sama besarnya ketika  saya ditugaskan olehnya sebagai wartawan Harian Rakyat untuk meng-cover Konferensi AA di Bandung yang bersejarah itu dalam tahun1955.

Titik-titik pertemuan antara PWAA, Bung Njoto dan Bung Karno

Ketika saya sudah pindah ke Jakarta dalam tahun 1960 untuk menjadi pemimpin redaksi suratkabar Harian EKONOMI NASIONAL maka hubungan saya dengan Bung Njoto mulai makin dekat dan juga makin sering bertemu. Sebab, ia memberikan perhatian besar terhadap teman-teman wartawan yang melakukan kegiatan-kegiatan melalui PWI, PWAA, KPAA (Konferensi Pengarang Asia-Afrika) OISRAA (Organisasi Indonesia untuk Setiakawan Rakyat Asia Afrika), Ganefo, KIAPMA  (Konferensi Internasional Anti Pangkalan Militer Asing), Komite Perdamaian dll dll.

Karena saya dipilih sebagai bendahara Panitia Pusat KWAA (Konferensi Wartawan Asia-Afrika) dan kemudian juga dipilih sebagai bendahara PWI Pusat merangkap bendahara PWAA, maka hubungan saya dengan Bung Njoto menjadi makin erat. Karena saya waktu itu sering sekali melakukan berbagai misi untuk PWAA ke luar negeri dan ikut menjalankan politik Bung Karno mengenai perjuangan melawan nekolim (neokolonialisme dan imperialisme) dalam skala Asia–Afrika maka saya menyaksikan bahwa dalam banyak hal ada titik-titik pertemuan antara PWAA, Bung Njoto dan Bung Karno.

Dari kegiatan-kegiatan itu semualah saya dapat mengetahui bahwa Bung Njoto mempunyai hubungan atau kenalan yang banyak dengan tokoh-tokoh besar di dalam negeri dan di luar negeri, dan karenanya mempunyai wawasan yang cukup luas tentang masalah internasional, terutama yang berkaitan dengan perjuangan rakyat Asia-Afrika melawan nekolim. Mungkin, ini semua jugalah yang  merupakan latarbelakang  kedekatan politik dan jiwa antara Bung Karno dengan Bung Njoto. Tidak salahlah kalau ada orang mengatakan bahwa di antara tokoh-tokoh politik di Indonesia pada waktu itu, baik yang dari golongan nasionalis, agama maupun komunis, Njotolah yang termasuk paling dekat dengan Bung Karno.

Saya tidak kenal dari dekat dengan pemimpin-pemimpin tingkat tinggi PKI lainnya, umpamanya Aidit, Lukman, Sudisman, Sakirman, atau Oloan Hutapea. Namun saya merasa pernah merasa dekat dengan Njoto. Karenanya, dengan tidak ragu-ragu saya bisa mengatakan bahwa Njoto adalah tokoh besar PKI yang mempunyai banyak keunggulan yang menonjol sekali, dan karenanya ia telah menjadi kebanggaan dan kekaguman bagi banyak orang.
