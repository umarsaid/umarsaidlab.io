---
layout: post
title: Pembusukan moral di kalangan elite kita amat parah
date: 2003-06-19
---

Sebagai seorang yang sudah tinggal di Paris selama 29 tahun ( sejak 1974), penulis merasa muak dan sekaligus naik darah membaca cerita tentang tingkah laku delegasi MPR yang baru-baru ini berkunjung ke Paris. Menurut berita itu, delegasi itu sebelum ke Paris sudah berkunjung ke London, Roma dan Madrid, dan kemudian menuju negeri Belanda. Secara resminya, kunjungan itu dilakukan dalam rangka « studi banding ».

Kunjungan delegasi MPR ke Paris ini, seperti yang dipaparkan Bisnis Indonesia tanggal 17 Juni 2003 patut dijadikan masalah besar oleh berbagai kalangan. Sebab, banyak aspek dari kunjungan ini yang menimbulkan citra yang amat buruk tentang negara dan pemerintah Republik Indonesia, tentang integritas moral dan politik kalangan elite bangsa kita. Pembusukan moral yang terjadi selama rezim militer Orde Baru rupanya masih berlangsung terus sampai sekarang, terutama di kalangan “atas”.

Negara kita sedang menghadapi kesulitan ekonomi yang besar, hutang luarnegeri kita masih bertumpuk-tumpuk (di atas 100 milyar US$), pengangguran mencapai sekitar 40 juta orang. Angka kemiskinan mencapai 35 juta orang, dan dari 35 juta jiwa 22 juta berada di pedesaan. Data pada 2001 mengungkapkan 55% dari penduduk miskin di Indonesia adalah petani (Sinar Harapan 17/5/2003).

Ketika negara dan bangsa kita sedang menghadapi kesulitan-kesulitan yang begitu besar itu kita membaca bahwa delegasi MPR yang berkunjung di Paris menginap di Hotel Crillon yang tarifnya satu kamar antara Rp Rp 6,5 juta sampai Rp18,5 juta per malam. Rombongan itu terdiri dari 21 orang. Bukan itu saja ! Ada pula kisah tentang pemakaian mobil limousin super-mewah, yang membikin banyak orang Perancis ternganga-nganga keheranan. Kita tidak tahu apa sajakah yang sudah di”studi-bandingkan” selama tiga hari di Paris, dan berapa pula pengeluaran untuk foya-foya ini. Juga tidak ada informasi tentang “studi banding” yang dilakukan oleh rombongan tersebut di London, Roma, Madrid dan negeri Belanda.

Untuk jelasnya, berikut di bawah ini disajikan kembali berita yang ditulis oleh wartawan Bisnis Indonesia Rofikoh Rokhim, yang bertugas di Paris:

Anggota MPR berlimusin-ria di Champs Elysee

"Gilingan abis," ungkap Amy, pemuda berperawakan gemuk berumur 30-an tahun ketika melihat serombongan warga Indonesia keluar masuk Hotel Crillon yang terletak pas di jantung kota Paris, di pertemuan jalan protokol Avenue Champs Elysee, Rivoli dan Place de la Concorde, Jumat malam lalu. "Itu hotel kan untuk menginap tamu negara Prancis. Mahal sekali lho... Siapa orang Indonesia yang nginap di situ. Nggak ada kata krisis ekonomi nih," timpal Valerie, 25, warga Prancis yang sering berkunjung ke Indonesia untuk kegiatan kemanusiaan di Kalimantan, Maluku dan Papua.

"Jangan berprasangka dulu, mungkin mereka pejabat dari Filipina, Thailand, Kamboja, Malaysia atau Laos," sambung Rudi, 45, pekerja gelap asal Jawa Tengah yang berprofesi sebagai petugas pembersih rumah dan restoran yang tinggal di Paris lima tahun terakhir. Usut punya usut, ternyata rombongan yang terdiri dari 21 orang itu adalah anggota Majelis Permusyawaratan Rakyat (MPR) bersama istri, anak dan para staf.

Di hotel supermewah itu mereka menyewa beberapa jenis kamar, mulai dari yang standar hingga suite room. Berapa tarif hotel itu per malam? Antara 655 euro hingga 1850 euro atau Rp6,5 juta-Rp18,5 juta per malam. Bagi orang Prancis, tarif kamar setinggi itu dianggap wajar karena memang hotel itu lebih untuk tamu negara setingkat presiden atau perdana menteri. Tercatat pejabat Indonesia yang pernah menginap di situ adalah Presiden Soekarno dan Presiden Soeharto saat menjadi tamu negara pemerintah Prancis.

Presiden Abdurrahman Wahid, ketika menjadi tamu Presiden Jacques Chirac tiga tahun lalu, tidak menginap di situ, tetapi di Hotel Intercontinental. Dan ketika menerima penghargaan doctor honoris causa dari Universite Paris I Pantheon Sorbonne, dia menginap di Hotel Nikko.

Selidik punya selidik lagi, ternyata menurut rencana para anggota MPR itu berada di Paris selama tiga hari untuk urusan dinas, alias bertemu anggota parlemen Prancis. Tapi apa lacur? Salah jadwal! Tidak satu pun anggota parlemen yang dapat menemui rombongan MPR itu karena mereka berkunjung saat weekend. Rupanya rombongan MPR itu sebelumnya telah berkunjung ke London, Roma dan Madrid dan setelah Prancis, mereka akan melanjutkan perjalanan ke Belanda.

"Ini road show yang memakan waktu, tenaga, pikiran dan dana yang tidak sedikit karena bertujuan melakukan studi banding. Tapi mana ada pejabat Prancis yang mau berdinas pada hari libur. Juarang buanget. Kalau niat ya...hari kerja dong," komentar Valerie yang fasih berbahasa Indonesia itu. Alhasil dikabarkan rombongan itu pun lebih memanfaatkan waktu untuk leisure-misalnya nonton kabaret di Lido pada kelas VIP seharga 160 euro, makan di restoran mahal kelas Fuquet dan belanja di toko bebas pajak Paris Look. Tidak ketinggalan berfoto ria di Menara Eiffel dan Arc de Triomph.

Kritikan pedas dari orang Prancis tidak hanya berhenti sampai di situ. Ketika Bisnis berada di Champs Elysee, jalan raya teramai dan terbesar di Paris, pada Minggu siang, perilaku anggota MPR itu bikin heboh turis maupun warga Prancis yang berdesakan di trotoar terbesar di dunia itu. Rupanya para wakil rakyat itu menyewa dua limusin berwarna putih. Limusin masih dianggap sebagai barang supermewah kendati di Prancis banyak orang kaya. Hanya selebritis dan pebisnis saja yang mampu menyewanya.

Alhasil banyak orang terhenyak dan pingin tahu orang top atau selebritis mana yang keluar dari restoran Cina itu. Tabrakan kecil dua mobil sempat terjadi di Avenue Champs Elysee gara-gara pengemudi meleng karena ingin melihat siapa yang akan keluar atau naik limusin putih itu. Siapa tahu Bruce Wills, Nicole Kidman atau Tom Cruise atau Bill Gates. Lumayan kan kalau bisa dapat tanda tangan atau potret bersama. Ternyata yang keluar adalah 16 orang anggota MPR Repulik Indonesia, yang langsung berebut naik limusin. Sisanya, para staf, naik mobil lainnya.

Menurut informasi, limusin tersebut disewa seharga 300 euro (Rp3 juta) hanya untuk berkendara selama 15 menit dari Champs Elysee ke Gare du Nord, stasiun kereta api yang menuju Belanda. Gile! Gill, 25, arsitek Prancis yang sering datang ke Indonesia untuk mencari kayu mahoni dan jati dan kebetulan sedang jalan dengan Bisnis pun takjub.

"C'est fou [Ini gila]. Katanya negara kamu sedang krisis dan minta keringanan utang kepada negara lain dan pemerintah kami. Tapi lihat saja pejabat kamu berlebihan di sini. Kami akan protes kalau negara kami memberi keringanan utang pada negara kamu. Hemat dulu dong kalau mau minta keringanan utang. Saya yakin mereka bukan tamu negara sebab pemerintah kami sangat sederhana kalau memberikan jamuan,'' paparnya emosi.

Siapa yang membiayai pengeluaran itu semua. Kantong sendiri? Kaya sekali para anggota majelis ini! Anggaran negara? Boros amat KBRI! Mana ada duit sebesar itu. Swasta? Siapa? Apa kepentingannya? Kabarnya, sebuah bank negara papan atas dan terbesar menyumbang pembiayaan itu- entah sebagai uang entertainment, atau terkait tujuan politis tertentu, ataukah dengan tekanan. Huwallahhuallambisawab!

Yang jelas, Ketua MPR Amien Rais, ketika berpidato di hadapan tukang becak di Makasssar pekan lalu, menyerukan agar dalam pemilu nanti jangan memilih partai yang anggotanya suka jalan-jalan ke luar negeri. Bahkan, Presiden Megawati juga berpesan agar kita semua hidup sederhana. Rakyat berharap kepergian para pejabat ke luar negeri untuk tujuan yang mulia, misalnya melakukan lobi guna memecahkan kesulitan negara. Yang terjadi kok malah sebaliknya?” (kutipan selesai).

## Kejahatan Moral Dan Pembusukan Hati-Nurani

Setelah membaca berita di atas, bisa orang bertanya-tanya, umpamanya : Gejala apa ini semua? Mengapa bisa terjadi? Siapa yang harus bertanggungjawab? Apa praktek buruk ini bisa dibiarkan terus? Apa yang harus kita lakukan bersama menghadapi persoalan semacam ini?

Kasus rombongan MPR ke Paris (dan kota-kota besar Eropa lainnya) mencerminkan kemerosotan moral yang tidak kepalang tanggung di kalangan elite bangsa kita dewasa ini. Pembusukan hati-nurani ini sudah sedemikian parahnya sehingga mereka tidak segan-segan untuk berfoya-foya dengan serba mewah ketika bangsa kita di Sumatera, di Jawa, di Kalimantan, di Sulawesi, di Maluku, di Nusa Tenggara, di Papua sedang mengalami kesulitan untuk hidup sehari-hari.

Bagi para anggota MPR (atau DPR) menginap di Hotel Crillon di Paris yang tarifnya paling rendah adalah selalu di atas 600 euro (atau kurang lebih Rp 6 juta) per kamar dan per malam adalah perbuatan yang tidak menimbulkan rasa hormat. Sebaliknya, perbuatan ini mengundang cacian, umpatan, grundelan, gugatan dan kemarahan orang banyak. Gugatan atau kemarahan ini adalah berdasar dan adil. Sebab, mereka itu menamakan diri dan disebut-sebut sebagai wakil rakyat. Gaji dan pengeluaran untuk pekerjaan mereka ditanggung oleh nagara, artinya dari pajak yang dibayar –dalam berbagai bentuk – oleh rakyat.

## Kegiatan Ekstra-Parlementer Perlu

Entah dengan cara apa, dan melalui jalur yang mana, dan dalam bentuk yang bagaimana, perlu sekali rasanya adanya tuntutan dari berbagai kalangan untuk diadakannya pengusutan atau pemeriksaan terhadap kasus rombongan “studi banding” MPR ke London, Roma, Madrid, Paris dan Holland ini. Perlu diselidiki apa betul mereka itu melakukan “studi banding” dan apa pula hasilnya? Apa mereka itu dimana-mana selalu menginap di hotel yang super-mewah seperti yang terjadi di Hotel Crillon Paris? Berapa pengeluaran perjalanan rombongan MPR ini ke bebagai tempat itu?

Ini semua perlu dilakukan untuk dihentikannya penghamburan uang negara (yang hakekatnya adalah uang rakyat) dengan perjalanan foya-foya di luarnegeri dengan dalih “studi banding”. Karena perbuatan sejenis ini justru banyak dilakukan oleh “wakil-wakil rakyat” (termasuk DPR dan DPRD) maka kontrol dari masyarakat ini bisa dilakukan oleh organisasi-organisasi non pemerintah (Ornop) atau LSM dan pers (termasuk lewat Internet). Ketika gejala pembusukan parah sudah juga menyerang badan-badan legislatif, maka peran kegiatan ekstra-parlementer adalah amat penting. Gerakan moral (dan gerakan politik) ekstra-parlementer bisa dilakukan dengan berbagai cara dan bentuk.

Kesulitan-kesulitan besar yang dihadapi bangsa dewasa ini, adalah pada dasarnya bersumber pada kerusakan-kerusakan yang ditimbulkan Orde Baru selama lebih dari 30 tahun di bidang politik, ekonomi, sosial, kebudayaan dan.......moraL Pembusukan parah inilah yang sampai sekarang masih diteruskan oleh sebagian dari elite kita. Banyak sekali ahli-ahli dan orang-orang pintar yang bertitel sarjana (atau tidak) yang memegang kedudukan penting, tetapi moralnya bejat. Dari mereka-mereka ini tidak bisa diharapkan untuk bisa menanggulangi pengangguran yang 40 juta orang atau memberantas korupsi yang merajalela di mana-mana. Orang-orang yang berpandangan politik anti rakyat tidak mungkin melahirkan atau menyumbangkan gagasan-gagasan besar untuk kebangkitan bangsa dari keterpurukan dewasa ini.
