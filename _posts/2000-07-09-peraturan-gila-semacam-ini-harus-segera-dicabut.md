---
layout: post
title: Peraturan gila semacam ini harus segera dicabut
date: 2000-07-09
---

Pertama-tama, mohon ma'af terlebih dulu atas pemberian judul tulisan ini dengan kalimat yang mungkin terasa kasar atau terdengar kelewatan "emosional", dan juga karena memakai tanda seru lima kali. Tetapi, percayalah hendaknya, bahwa apa yang dikemukakan berikut ini adalah berdasarkan kepala dingin, dan nalar yang wajar-wajar saja. Namun, harap dimengertilah kiranya, bahwa karena persoalan yang diungkap berikut ini adalah sesuatu yang sudah sangat keterlaluan, maka tidak bisa tercegah lagi bahwa di sana-sini muncul luapan kemarahan dan ledakan-ledakan emosi, bahkan juga cacian atau umpatan. Semoga saja, dengan dengan fikiran yang jernih, dan hati nurani yang bersih, para pembaca mungkin akan bisa ikut menghayati gejala yang gila dan aneh seperti yang disajikan di bawah ini.

Singkat-padatnya adalah begini : sejumlah besar mantan tapol yang tinggal di Yogyakarta, yang pada umumnya sudah lanjut usia, SAMPAI SEKARANG masih diharuskan untuk memenuhi peraturan Santiaji tiap 2 bulan sekali. Kita tidak tahu, berapa jumlah mantan Tapol yang diharuskan mengikutinya, dan juga apa bentuk dan caranya. Apakah sekedar melaporkan diri, atau mendengar petuah, atau wejangan, atau penataran, atau kursus singkat, atau petunjuk, seperti yang sudah biasa dilakukan dalam masa-masa Orde Baru? Kita tidak tahu juga dengan persis lagi, sekarang, apakah Santiaji (apa pula barang aneh ini ???) ini diberikan oleh kepolisian, atau oleh petugas militer yang berpangkat kopral, sersan atau letnan atau mayor.

Dan, mungkin seperti Anda juga, kita semua bisa mempertanyakan apakah gerangan isi Santiaji ini ? Apakah masih seperti "penataran Pancasila" yang dipaksakan selama rezim militernya Suharto dkk dulu itu? Kalau masih itu-itu juga, maka patut-patutlah sajalah kiranya kalau Anda ikut berteriak "bedebah" (atau makian lainnya, yang lebih kasar dan bahkan lebih kotor lagi). Sebab, bukan saja bahwa Santiaji yang dipaksakan puluhan tahun itu munafik, palsu, kosong,dan intinya adalah "gombal" belaka, melainkan lebih dari itu. Karenanya, pada umumnya, para pendukung rezim Orde Baru tidaklah berhak sama sekali untuk mengajari bagaimana seseorang harus menjaga moral, atau memelihara persatuan bangsa dan patriotisme. Pendukung rezim militer juga sama sekali tidak pantas untuk berkhotbah tentang kesetiaan kepada negara dan rakyat, tentang kejujuran dalam mengabdi kepada bangsa. Rezim ini bukan saja telah memelintir dan membengkok-bengkokkan itu semuanya, bahkan sudah merusaknya dan membuangnya!

Yang perlu sama-sama kita renungkan secara tenang dan fikiran jernih adalah yang berikut : para mantan tapol di Jogyakarta itu (dan mungkin juga di daerah-daerah lainnya di Indonesia) TIDAK BERSALAH APAPUN terhadap undang-undang, atau tidak berdosa SAMA SEKALI terhadap negara dan rakyat. Mereka itu, sudah mengalami ketidak-adilan dan pelanggaran berat di bidang Hak Asasai Manusia yang dilakukan oleh rezim militer Orde Baru (beserta pendukung-pendukung setianya di kalangan militer, dan di kalangan Golkar, atau juga di kalangan berbagai golongan dalam masyarakat, baik yang Islam maupun yang bukan). Perlakuan tidak adil dan pelanggaran berat HAM ini bukan saja berbentuk penahanan atau pemenjaraan terhadap mereka dalam jangka lama sekali (harap ingat : tanpa pengadilan !!!). Mereka itu, sesudah keluar dari pemenjaraan yang sewenang-wenang pun masih terus disiksa selama puluhan tahun.

Salah satu contohnya adalah petisi/surat mantan tapol Jogyakarta kepada Gubernur Kepala Daerah Istimewa Jogyakarta, seperti yang bisa dibaca lebih lanjut di bawah ini. Marilah sama-sama kita renungkan dari lubuk hati yang dalam dan sama-sama kita bayangkan, betapa pedihnya hati bagi mereka : sesudah keluar dari tahanan/pemenjaraan, mereka setiap 2 bulan sekali harus melapor untuk mendapatkan "Santiaji", dan itu pun sudah selama 24 tahun. Artinya, orang-orang yang tidak bersalah apa-apa ini, selama puluhan ini harus datang, sebagai pesakitan atau penjahat, untuk melapor dan mendapat pendidikan dari petugas-petugas Orde Baru. Seolah-olah mereka itu adalah orang-orang yang sesat fikiran dan jahat kelakuannya, atau orang-orang yang melakukan kesalahan dan karenanya perlu mendapat "ajaran" atau "pendidikan". Du'ilah, sungguh-sungguh edan!

Sebab, sekarang setiap hari kita dapat membaca dalam pers, dan juga melihat dalam televisi, bahwa justru yang harus mendapat penataran, atau pendidikan, atau penghayatan Pancasiila adalah mereka para pendukung setia Orde Baru, baik yang terdiri para pembesar militer, pimpinan Golkar, atau partai PPP dan PDI dan konglomerat, maupun tokoh-tokoh di berbagai kalangan (para hakim agung maupun yang tidak agung sama sekali, para anggota DPR dan MPR, para pemuka agama dan intelektual). Mereka inilah yang, sambil membunuh demokrasi dan merusak secara besar-besaran dan serius sendi-sendi Republik Indonesia selama puluhan tahun, telah juga sekaligus menjarah kekayaan negara, merampok bank-bank, menyembunyikan hasil korupsi di luarnegeri, atau mendirikan rumah-rumah mewah dan menumpuk kekayaan dengan cara-cara haram dan dengan jalan kriminal. Mereka itu pulalah, yang merupakan kekuatan-kekuatan gelap sisa-sisa Orde Baru, dan yang dewasa ini sedang beramai-ramai mengadakan aksi-aksi subversif dan berbagai kekacauan, untuk menggagalkan jalannya reformasi di berbagai bidang!

Untuk memberikan gambaran yag lebih jelas, maka di bawah ini disajikan surat mengenai mantan tapol Yogyakarta tersebut :

> Kepada Yth:
> Gubernur Kepala Daerah Istimewa Yogyakarta
> Kantor Gubernur/Kepatihan
> Yogyakarta.
>
> Dengan hormat,
>
> Menunjuk surat Bp. Saderi Joko Susanto (70 th) dan Dirdjo Kandar (80 th) mantan tapol yang tinggal di Yogyakarta kepada Walikota Yogyakarta tertanggal 22 Juni 2000 tentang masalah tuntutan penghentian Santiaji di Kodya Yogyakarta untuk golongan A, B, C yang sudah berlangsung 24 th yaitu sejak tahun 1976 - hingga kini yang termuat dalam harian Jawa Pos tanggal 22 dan 24-6-2000, maka mengingat bahwa:
>
> 1. Sekarang ini rakyat Indonesia tidak berada lagi dalam jaman Orde Baru - Suharto, tetapi sudah dalam era reformasi dan sudah adanya pemerintahan konstitusionil demokratis dengan Presidennya Gus Dur.
> 2. Peristiwa Oktober 1965 sebenarnya adalah peristiwa usaha dan kup Suharto bukan kup PKI.
> 3. Sebagian terbesar yang ditangkap, dibunuh, ditahan, dan dimatikan perdatanya bukanlah anggota PKI, tetapi di G30 S PKI-kan Suharto.
> 4. Lembaga inkonstitusi Kopkamtib yang diubah menjadi Bokorstanas dan Bakorstanasda dan Litsus sudah dibubarkan bahkan sekarang sedang diusahakan Kodam, Kodim, Korem, Koramil, Babinsa akan dihapus.
> 5. Adanya Permendagri no.19/1988 tentang penghapusan KTP dengan tanda ET/OT dan KTP sudah seumur hidup.
> 6. Santiaji selama 24 tahun sudah merupakan hukuman tambahan yang merupakan beban berat bagi mantan tapol/napol Kodya Yogyakarta yang tidak manusiawi dan tidak adil, sedangkan di daerah lain sudah ditiadakan.
> 7. Tuntutan penghentian Santiaji tersebut hal yang wajar, masuk akal, adil dan menusiawi.
>
> Maka kami Forum Pemulihan Hak Sipil Politik Mantan Tapol - Napol Jawa Tengah ikut memperkuat tuntutan Mantan Tapol - Napol Kodya Yogyakarta agar Santiaji untuk golongan A, B, C dihapuskan sekarang juga karena tidak lagi mempunyai dasar hukum, politik, tidak manusiawi dan melanggar HAM PBB.
>
> Atas segala perhatian Gubernur Kepala Daerah Istimewa Yogyakarta kami ucapkan terima kasih.
>
> Semarang, 5 Juli 2000.
>
> Hormat kami,
> Ketua (Warsono)
> Sekretaris (Sardjono)

Kasus para mantan tapol di Jogya adalah hanya sebagian kecil saja dari masalah para mantan tapol yang jumlahnya begitu besar di seluruh Indonesia. Seperti yang sudah diungkapkan dalam tulisan terdahulu "Rehabilitasi hak sipil dan politik bagi para ex-tapol", jutaan orang ex-tapol beserta keluarga (dan sanak-saudara terdekat mereka) SAMPAI SEKARANG masih mendapat perlakuan yang tidak sewajarnya sebagai manusia dan sebagai warganegara republik kita. Rehabilitasi hak sipil bagi mereka adalah sesuatu yang HARUS dilakukan oleh kita semua, kalau kita mau betul-betul menegakkan hukum dan keadilan. Ini bukan semata-mata masalah kemurahan hati, atau sesuatu yang sekedar untuk menunjukkan sikap mau rujuk-rujukan saja. Rehabilitasi hak sipil dan politik mereka berkaitan dengan prinsip-prinsip dasar perikemanusiaan dan ketetanegaraan republik kita. (Harap, sudilah kiranya membaca kembali tulisan tersebut).

Mengingat reaksi dari berbagai pembaca terhadap tulisan tersebut, maka di bawah ini disajikan bahan tambahan tentang petisi Forum Pemulihan Hak Sipil dan politik mantan Tapol-Napol Jawa Tengah itu, yang selengkapnya berbunyi sebagai berikut :

> Kepada Yth
> Menteri Negara Koordinator
> Bidang Politik dan Keamanan
> Republik Indonesia di Jakarta
>
> Menunjuk surat Kantor Menteri Negara Koordinator Bidang Politik dan Keamanan Republik Indonesia No. 139/Ses/As 1/5/2000 tertanggal 8 Mei 2000, perihal permohonan silaturrahmi, perkenankanlah kami delegasi Forum yang terdiri dari 11 orang mewakili berbagai Cabang Forum menghadap Menko Polkam untuk menyampaikan hal-hal berkenaan dengan perlakuan yang melanggar hak-hak kami sebagai warga negara dan sebagai manusia - Hak azasi manusia.
>
> Namun sebelum menyampaikan hal-hal ini lebih lanjut ijinkanlah kami menjelaskan serba singkat tentang Forum tersebut diatas dan kegiatannya. Forum tersebut kami bentuk pada tanggal 16 Januari 2000 dengan maksud dan tujuan untuk memperjuangkan kepentingan mantan tapol/napol korban orde baru beserta keluarganya. Sampai kini Forum telah mempunyai 25 cabang di Kabupaten dan Kotamadya Jawa Tengah, sedangkan yang tergabung didalam meliputi unsur mantan TNI AD, Laut dan Udara; mantan pegawai negeri sipil, BUMN, buruh, tani, pamong desa, pengusaha, dan lain-lainnya.
>
> Jumlah mantan tapol/napol Jawa Tengah; golongan A sekitar 131 orang; B sekitar 6.384 orang, C sekitar 216.595 orang dan wajib lapor (mantan) sekitar 31.587 orang. Ini belum terhitung yang dibunuh dan hilang serta keluarga mereka semua.
>
> Adapun kegiatan Forum selama ini sudah pernah menghadap DPRD Tingkat I pada tanggal 20 Januari 2000, diulangi tanggal 15 Maret 2000; menghadap Gubernur Jawa Tengah yang diterima Kepala Sospol Propinsi Jawa Tengah pada tanggal 21 Maret 2000 dan ke Kodam Diponegoro Jawa Tengah pada tanggal 7 Maret 2000. Dalam silaturrahmi tersebut, kami sampaikan masalah hak-hak kami yang pernah dilanggar dan dirugikan oleh pemerintahan orde baru.
>
> Sangat disayangkan bahwa semua pihak menjawab bahwa persoalan kami akan disampaikan ke pemerintah pusat, karena hal-hal tersebut menjadi wewenang Pemerintahan Pusat untuk diselesaikan.
>
> Karena itu pada hari ini kami menghadap Bapak untuk menjumpai hal-hal yang menyangkut perlakuan yang menimpa diri kami pribadi dan seluruh keluarga yang berlangsung sejak dari akhir tahun 1965 yang hingga kini sisa-sisanya masih dirasakan.
>
> Wujud perlakuan tersebut seperti:
>
> 1. Penangkapan yang sewenang-wenang oleh aparat negara dan anggota organisasi tertentu.
> 2. Perlakuan yang tidak berperikemanusiaan selama ditahan.
> 3. Perlakuan petugas pemeriksa waktu memeriksa sangat kejam dan tidak berperikemanusiaan, baik terhadap laki-laki maupun perempuan.
> 4. Perampasan hak milik tapol/napol dan keluarganya secara melawan hukum oleh pejabat, petugas atau anggota ormas tertentu.
> 5. Perampasan terhadap istri dari sementara orang yang ditahan.
> 6. Penyiksaan dan pembunuhan yang sangat biadab.
> 7. Pemecatan dari pekerjaan tanpa prosedur hukum dan sewenang-wenang.
> 8. Pemutusan hak pensiun dan pensiun bagi mereka yang sudah berhak dan menerima pensiun.
> 9. Pengucilan keluarga dari kehidupan bermasyarakat.
> 10. Anak dan keluarga dua keturunan tidak boleh menjadi pegawai negeri, tentara, polisi, dan bekerja di perusahaan vital.
> 11. Sementara tahanan baik di LP Tangerang, Nusakambangan, Pulau Buru, Plantungan Kendal dan lainnya di Jawa dan Luar Jawa dikerjapaksakan tanpa dibayar, dan hasilnya diambil pejabat.
> 12. Kepada keluarga mereka yang ditahan kalau masih dapat bekerja tidak diberi hak naik pangkat atau gaji.
> 13. Setelah kami dibebaskan juga mendapatkan perlakuan yang tidak adil, termasuk keluarga kami seperti di KTP ditulis ex tapol/napol, wajib lapor dan sebagainya.
> 14. Kepada yang dibebaskan dengan keterangan tidak terdapat indikasi atau dikategorikan golongan C (1+2) juga tidak bisa diterima kembali bekerja.
> 15. Sampai hari ini kami masih menjadi sasaran isu politik dari sementara golongan tentang bahaya komunis dsb-nya.
> 16. Masih ada pengawasan dari Rt, Rw, Kelurahan dan aparat pemerintahan seperti dinyatakan dalam surat dari instansi pemerintahan.
>
> Karena itu demi ditegakkannya keadilan, hak warga negara dan hak azasi manusia kami mengajukan tuntutan:
>
> 1. Dihapuskan dengan segera seluruh stigmasi terhadap mantan tapol/napol korban orde baru dan seluruh anggota keluarganya.
> 2. Diadakan rehabilitasi dan kompensasi.
> 3. Segera dilaksanakan rekonsiliasi nasional dengan diadakan UU Pengadilan Pelanggaran HAM dan Komisi Kebenaran dan Rekonsiliasi.
> 4. Dipulihkan kembali hak azasi manusia bagi mantan tapol/napol, keluarganya serta semua korban orde baru.
> 5. Disosialisasikan semua peraturan pemerintah tentang pencabutan pembatasan terhadap mantan tapol/napol dan seluruh korban orde baru ke seluruh instansi pemerintahan sampai tingkat yang paling bawah.
> 6. Dikembalikannya seluruh hak milik yang pernah disita dengan melawan hukum oleh aparat dan lainnya.
> 7. Pemerintah memberi kompensasi terhadap kerugian materiel dan nonmateriel yang dialami mantan tapol/napol korban orde baru dan keluarganya.
> 8. Agar pemerintah bersama LSM prodemokrasi dan kemanusiaan segera membentuk komisi khusus untuk meneliti korban peristiwa 1965-1966.
> 9. Dipercepatnya ratifikasi konvensi Internasional tentang hak sipil/politik.
>
> Besar harapan kami tuntutan ini dapat dikabulkan oleh pemerintah Presiden Gus Dur yang telah mendapat legitimasi rakyat melalui MPR.
>
> Atas segala perhatian Menko Polkam dan Presiden RI kami ucapkan terima kasih.
>
> Semarang, 8 Juni 2000
>
> Pengurus Forum Pemulihan Hak Sipil - Politik Mantan Tapol/Napol Jawa Tengah
>
> Ketua
> Warsono
>
> Sekretaris
> Y. Sarjono
>
> Penasehat
> S. Utomo
>
> Tembusan :
>
> 1. Presiden RI
> 2. Wakil Presiden RI
> 3. Ketua/Wakil Ketua MPR
> 4. Ketua/Wakil Ketua DPR
> 5. Panglima TNI
> 6. Ketua Mahkamah Agung RI
> 7. Jaksa Agung RI
> 8. Menteri HAM RI
> 9. Komnas HAM
> 10. Ketua Partai-partai Politik.
> 11. LBHI Jakarta
> 12. PBHI Jakarta
> 13. Pers, Radio dan Televisi
>
> (PS. Perlu diketahui bahwa Forum Pemulihan Hak Sipil Politik Jawa Tengah yang:didirikan pada tanggal 16 Januari 2000 telah mendatangi DPRD Tingkat I Jawa Tengah pada tanggal 20 Januari dan pada tanggal 4 Maret 2000 untuk menyampaikan Petisi yang sama dan juga pernah mendatangi Kodam IV Diponegoro dan Gubernur Jawa Tengah. Forum ini mempunyai 25 Cabang di seluruh Jateng di Tingkat Kabupaten dan Kotamadya. Forum Cabang Kabupaten Semarang pernah juga mendatangi DPRD Tingkat II Kab Semarang untuk menyampaikan Petisi. Demikian juga pada tanggal 5 Juli tahun 2000 Forum memberi dukungan pada tuntutan mantan tapol/napol Kodya Yogyakarta yang sejak th 1976 hingga kini masih diwajibkan mengikuti Santiaji di Kecamatan-kecamatan tiap 2 bulan sekali).

Kasus-kasus ini perlu kita angkat ramai-ramai. Segala macam cara dan bentuk patutlah dan perlu kita tempuh, supaya peraturan yang keliru dan praktek buruk ini segera dihentikan secara RESMI dan TERBUKA. Aksi-aksi, entah dalam bentuk apapun, perlu digelar terus-menerus, oleh berbagai fihak. Kegiatan serius harus dilancarkan (umpamanya oleh : LBH, fakultas/universitas, organisasi non-pemerintah atau LSM, berbagai golongan dalam masyarakat madani, pers dll) untuk melakukan penelitian dan kemudian juga mempersoalkannya. Para mantan tapol sendiri juga perlu dihimbau untuk lebih banyak bersuara, dan mengadakan berbagai kegiatan, untuk mengingatkan terus-menerus semua fihak tentang pelanggaran HAM yang sudah berlangsung begitu lama itu.

Perlu jelas bagi semua fihak (termasuk kita sendiri) bahwa mempersoalkan nasib mantan tapol bukanlah bertujuan untuk membalas dendam. Bukan pula untuk mengipasi permusuhan atau meruncingkan pertentangan, yang selama puluhan tahun ini memang sudah disebar-luaskan oleh rezim Orde Baru beserta pendukung-pendukung setianya. Sebaliknya, mempersoalan rehabilitasi mantan tapol adalah salah satu di antara banyak cara untuk menjadikan bangsa kita bangsa yang beradab. Mengangkat masalah korban pembunuhan 65/66 adalah sumbangan bagi tegaknya keadilan dan bagi kemenangan prinsip-prinsip Deklarasi Universal Hak Asasi Manusia di bumi Indonesia. Dan, kemenangan ini, pada hakekatnya dan pada akhirnya, adalah kemenangan kita semua, tidak peduli dari kalangan ideologi ataupun golongan SARA yang manapun juga.
