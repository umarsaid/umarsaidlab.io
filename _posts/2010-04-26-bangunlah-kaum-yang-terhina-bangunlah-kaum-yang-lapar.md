---
layout: post
title: Bangunlah kaum yang terhina bangunlah kaum yang lapar
date: 2010-04-26
---

Menjelang datangnya Hari Buruh Sedunia tanggal 1 Mei yang akan datang, yang akan dirayakan di banyak penjuru dunia sebagai salah satu peringatan kemenangan perjuangan kaum buruh/pekerja melawan penindasan, pemerasan atau penghisapan kaum kapitalis reaksioner, maka di bawah berikut ini disajikan teks atau lirik lagu « Internasionale » dalam bahasa Indonesia.



Bangunlah kaum jang terhina,

Bangunlah kaum jang lapar.
Kehendak jang mulja dalam dunia
senantiasa tambah besar.
Lenjapkan adat dan faham tua
kita Rakjat sedar-sedar.
Dunia sudah berganti rupa
untuk kemenangan kita.
Perdjuangan penghabisan,
kumpullah berlawan.
Dan Internasionale
pastilah di dunia.

Kitalah kaum pekerja s'dunia,
Tent'ra kerja nan perkasa.
Semuanya mesti milik kita,
Tak biarkan satupun penghisap!
Kala petir dahsyat menyambar
Di atas si angkara murka,
Tibalah saat bagi kita
surya bersinar cemerlang!
Perdjuangan penghabisan
kumpullah berlawan.
Dan Internasionale
pastilah di dunia



Dikumandangkan oleh para perintis kemerdekaan

Lagu yang dijadikan oleh berbagai gerakan buruh dan golongan kiri di dunia sebagai lambang perjuangan ini juga sudah dikenal d Indonesia sejak jaman pemerintahan kolonial Belanda, dan dinyanyikan oleh gerakan–gerakan nasionalis  perintis kemerdekaan, serikat buruh dan anggota serta simpatisan PKI, baik secara terang-terangan maupun secara sembunyi-sembunyi.



Namun, seperti yang dialami atau diketahui oleh banyak orang di Indonesia, lagu « Internasionale » ini telah tidak bisa disuarakan dengan terbuka atau terang-terangan dalam jangka yang lama sekali, karena dilarang oleh rejim militer Orde Baru sebagai barang taboo dan dianggap berbahaya karena « berbau komunis ». Karena itu, lagu « Internasionale » ini kurang dikenal di Indonesia, tidak seperti di jaman pemerintahan Bung Karno ketika nyanyian ini sering terdengar dimana-mana.



Sedangkan, ketika « Internasionale » ini dilarang di Indonesia oleh rejim militer Suharto pada waktu itu, di banyak negeri di seluruh dunia lagu ini disuarakan dengan lantang oleh berbagai rakyat dalam macam-macam bahasa, yang jumlahnya melebihi 300 bahasa dan dialek, dan diterjemahkan dengan sedikit perobahan-perobahan dari teks aslinya. Di seluruh dunia, hanya di Indonesia di bawah Suhartolah lagu Internasionale dilarang atau ditabookan secara terang-terangan dan menjadi sikap politik yang resmi dari rejim Orde Baru.



Diciptakan semasa Komune  Paris



Padahal, sejak lagu ini diciptakan pada tanggal 30 Juni tahun 1871 oleh seorang penyair dan tukang kayu di Perancis, Eugene Pottier, sudah menjadi sangat terkenal secara internasional di kalangan gerakan buruh dan revolusioner. Eugene Pottier adalah anggota Komune Paris yang terkemuka, yang merupakan pemerintahan klas buruh yang pertama kali di dunia, yang berkuasa selama  72 hari di Prancis  



Pemerintahan klas buruh Komune Paris dikalahkan  oleh kaum borjuis reaksioner, dan ribuan pendukung Komune dibunuh dan sekitar  50 000 orang diadili. Eugene Pottier sendiri dijatuhi hukuman mati in absensia, sehingga terpaksa melarikan diri untuk sementara ke Amerika Serikat, dan kemudian kembali lagi ke Prancis. Syair gubahannya, yang mengumandangkan semangat pembrontakannya melawan  penindasan, penghisapan dan kemiskinan  oleh klas borjuasi, kemudian diisi sebagai musik oleh Pierre Degeyter dalam tahun 1888.



Sejak itu, lagu Internasionale yang teksnya membangkitkan semangat perjuangan kaum buruh dan gerakan revolusioner atau gerakan kiri di banyak negeri di dunia, menjadi lagu yang disuarakan oleh berbagai golongan yang tergabung dalam macam-macam gerakan komunis, sosialis demokrat,  trotskis, anarchis, atau golongan progresif lainnya.





Disuarakan oleh gerakan revolusioner di dunia



Sepanjang  sejarah perjuangan berbagai rakyat di  macam-macam negara di dunia dalam melawan fasisme dan kaum kapitalis dan klas borjuis reaksioner,  lagu Internasional merupakan sumber semangat yang besar dan  penting bagi banyak orang. Lagu ini dinyanyikan dalam kalangan Yahudi di ghetto-ghetto Warsawa ketika melawan fasisme Jerman-Hitler. Juga oleh pasukan-pasukan pembrontakan Spanyol melawan kekuasaan  Jenderal Franco, atau di berbagai gerakan revolusioner menentang para diktator reaksioner di Amerika Latin.



Jadi, selama lebih dari 100 tahun lagu Internasionale sudah mempunyai daya yang besar untuk membangkitkan keberanian dan mendorong tekad bagi mereka yang mau mengadakan perlawanan terhadap segala macam penindasan,  penghisapan dan ketidakadilan di berbagai penjuru dunia.



Pada dewasa ini, lagu Internasionale masih terus sering terdengar dalam pertemuan-pertemuan  penting dalam gerakan menentang neo-liberalisme dan globalisasi, yang dilakukan oleh bermacam-macam gerakan yang luas, dan tidak terbatas hanya oleh kalangan kiri, progresif, komunis, atau sosialis saja. Ini dapat disaksikan dalam kegiatan-kegiatan di kalangan Third World Forum, World Forum for Alternatives , di pertemuan-pertemuan di Porto Allegre (Brasilia), di Caracas (Venezuela), di Bogota (Bolivia), di Afrika Selatan, Mali, Kenya, atau di Prancis, Jerman, Italia, Spanyol dan negeri-negeri Skandinavia.



Mengingat itu semua, maka kiranya untuk selanjutnya di Indonesia pun lagu Internasional juga perlu dipakai sebagai pembangkit semangat perjuangan gerakan buruh, dan mendorong persatuan serta menggalang setiakawan dalam melawan segala macam ketidak-adilan dan pemerasan oleh kalangan kapitalis reaksioner nasional maupun internasional, yang bersekongkol dengan  para penguasa dalam pemerintahan SBY.



Tidak perlu takut-takut lagi menyanyikan Internasionale



Di kalangan gerakan buruh Indonesia, dan juga di kalangan kaum kiri atau golongan progresif dalam masyarakat, sudah tidak perlu takut-takut lagi untuk mengumandangkan lagu Internasioale  dalam kegiatan-kegiatan penting. Sekarang ini, pemerintahan SBY, atau juga pemerintahan-pemerintahan lainnya sudah tidak bisa melarang atau mencegah lagi diperdengarkannya  lagu Internasionale. Sebab, tindakan sebodoh itu hanya akan membikin  lebih buruk lagi muka pemerintahan SBY, yang selama ini sudah terlalu buruk oleh karena berbagai persoalan besar dan kerusakan atau kebejatan yang meluas. Selain itu, pasti akan mendapat perlawanan dari masyarakat luas dan sebagian terbesar rakyat tidak mematuhi larangan  semacam itu.



Sebab, sebagai contohnya, walaupun  larangan terhadap beredarnya di negeri kita marxisme resminya masih belum dicabut, namun dalam prakteknya sudah banyak orang atau penerbit yang berani mengedarkan buku-buku yang  berisi ajaran-ajaran marxisme (atau berbau-bau komunis/PKI)

.

Marxisme lewat Google di Internet



Lagi pula, dengan adanya Internet, maka larangan terhadap diedarkannya marxisme di Indonesia menjadi soal yang bisa dianggap kentut saja oleh banyak orang. Sekarang ini, melalui Internet, setiap orang yang berminat terhadap marxisme, komunisme, atau sosialisme, dapat memperoleh bacaan yang beratus-ratus ribu halaman setiap waktu.dan secara bebas, dengan membuka Google.



Contohnya : dalam website http://umarsaid.free.fr tersedia di halaman utama (index) rubrik yang berjudul Marxisme. Dalam rubrik ini dapat dibuka, setiap hari atau setiap saat,  berbagai macam bahan bacaan tentang sejarah marxisme di Indonesia, Tan Malaka, Leon Trotsky, Lenin, dan seabrek-abrek bahan lainnya tentang marxisme).



Apa yang dikatakan Bung Karno



Dengan demikian, kiranya makin jelaslah kebenaran apa yang dikatakan Bung Karno dalam tahun 1966 (setahun sesudah peristiwa G30S) bahwa marxisme tidak bisa dilarang  dan juga ketika ia mengatakan sebagai berikut : « Apa lagu Internasionale itu hanya  dinyanyikan oleh komunis tok ? Seluruh buruh ! Komunis atau niet communist, right wing atau left wing, semuanya menyanyikan lagu Internasionale. Janganlah orang tidak tahu lantas berkata, siapa melagukan Internasionale, ee,  PKI ! God dorie (bahasa Belanda,kira-kira artinya :  Astaga atau Masya Allah) Lagu Internasionale dinyanyikan di London, di Nederland, di Paris, di Brussel, di Bonn, di Moskow, di Peking, di Tokio. Pendek, dimana-mana ada kaum buruh mengadakan serikat, menyanyikan lagu Internasionale » (dikutip dari buku  « Revolusi belum selesai ,  jilid II, halaman 313).



Dewasa ini, bagi kita di Indonesia, mengumandangkan lagu Internasionale merupakan penghormatan kepada pengorbanan para Digulis yang banyak meninggal di pembuangan Tanah Merah dan para perintis kemerdekaan lainnya. Menyanyikan lagu Internasionale bisa juga berarti menunjukkan sikap marah dan kebencian kepada rejim militer Suharto beserta para pendukung setianya. Di samping itu, mengumandangkan lagu Internasionale juga berarti ikut melestarikan jiwa perjuangan revolusioner Bung Karno.
