---
layout: post
title: Mengantisipasi ledakan lebih besar skandal raksasa Bank Century
date: 2009-11-25
---

Setelah berminggu-minggu banyak sekali dari berbagai kalangan masyarakat Indonesia dibanjiri berita-berita ruwet dan rumit  -- yang menghebohkan dan membikin resah  --  mengenai persoalan  skandal raksasa Bank Century dan kisruh antara Polri-Kejaksaan-KPK (dan DPR) akhirnya presiden SBY mengumumkan tanggal 23 November 2009 malam sikapnya mengenai persoalan-persoalan  besar ini.

Sikap presiden SBY ini sudah ditunggu-tunggu sejak lama oleh banyak kalangan dan golongan, sesudah Tim 8 yang dibentuknya untuk melakukan pencarian fakta dan verifikasi sekitar kasus kriminalisasi KPK (atau kasus Bibit-Chandra) selesai dengan missinya selama seminggu, dan sesudah presiden SBY memerlukan waktu seminggu pula untuk menentukan sikapnya.

Dalam menghadapi kasus kriminalisasi KPK ini presiden SBY kelihatan amat ragu-ragu, atau hati-hati sekali dan « tidak mau buru-buru » mengambil sikap. Hal yang demikian ini bisa dimengerti atau wajar, sebab masalah yang harus dihadapinya sebagai kepala negara dan kepala pemerintahan memang berat, besar dan parah sekali. Karena,  sejak dibeberkannya  berbagai kebejatan moral pejabat-pejabat tinggi kepolisian dan kejaksaan oleh pemutaran rekaman telpun mereka di Mahkamah Konstitusi maka kelihatanlah dengan jelas sekali bahwa moral sejumlah besar pimpinan aparat penegakan hukum negara kita memang sudah amat bobrok.

Baru hanya seperti gunung es

Keputusan presiden yang memerlukan waktu dua minggu untuk menimbang-nimbang, mempelajarinya dari berbagai segi dan sudut pandang, dan mengumpulkan pendapat dari banyak kalangan, menunjukkan bahwa persoalan kriminalisasi KPK ini sunguh-sungguh merupakan persoalan besar di kehidupan negara dan bangsa kita. Soal Polri-Kejaksaan-KPK ini hanyalah permunculan sebagian kecil saja, yang ibaratnya seperti gunung es,  dari sudah rusaknya moral para pejabat di negara kita, yang sarat dan setengah tenggelam akibat  korupsi yang dimainkan oleh mafia hukum, mafia peradilan, mafia kekuasaan, dan mafia ekonomi.

Oleh karena itu harapan rakyat adalah besar sekali bahwa presiden SBY mau memperhatikan, atau menghormati, atau mempertimbangkan rekomendasi hasil kerja Tim 8 yang dibentuknya sendiri. Laporan dan rekomendasi Tim 8 yang diajukan kepada presiden itu sebenarnya dengan jelas dan juga  tegas sudah mengajukan usul atau fikiran-fikiran yang bisa diambil oleh presiden sebagai kepala negara dan kepala pemerintahan untuk mengatasi kekisruhan yang carut-marut antara Polri-Kejaksaan-KPK, termasuk kasus Bibit-Chandra.

Sebenarnya, pada Senin malam tanggal 23 November itu sebagian besar rakyat menaruh harapan besar sekali bahwa presiden akan mengumumkan dengan  tegas dihentikannya diajukannya Bibit-Candra ke pengadilan sesuai dengan rekomendasi Tim 8 (umpamanya, di banyak tempat masyarakat berkerumun di depan televisi dan juga mengadakan « nonton bareng »).  Namun,  dalam pidatonya yang 30 menit itu, presiden SBY tidak secara terang-terangan dan tegas menyatakan sikapnya. Dengan bahasa yang oleh  banyak orang dianggap membingungkan atau tidak jelas presiden SBY mengusulkan supaya polisi dan kejaksaan tidak membawa persoalan Bibit-Chandra ke pengadilan.

Dengan mengemukakan bahwa ia tidak mau mengintervensi atau memasuki bidang hukum yang bukan wilayahnya, ia dengan panjang lebar mengutarakan pandangannya tentang bagaimana menyelesaikan berbagai persoalan negara, termasuk masalah skandal besar Bank Century.

Bank Century yang penuh keruwetan

Dari pidatonya yang selama 30 menit itu banyak orang kecewa sekali, bahkan juga marah, karena dianggap tidak memenuhi harapan rakyat banyak yang sudah dituangkan juga dalam laporan dan rekomendasi Tim 8.  Sesudah mendengar pidato presiden SBY itu banyak orang yang tidak tahu atau tidak mengerti bagaimana akhirnya Bibit Samad Riyanto dan Chandra Hamzah, apakah ia akan kembali ke KPK atau tidak. Atau, juga bagaimana akhirnya persoalan Anggodo, yang merupakan « dedengkot penting dan besar » dalam skandal KPK itu.

Juga mengenai skandal raksasa Bank Century didapat kesan bahwa presiden SBY berusaha untuk menutupi besarnya keruwetan yang diakibatkan oleh kesalahan atau pelanggaran atau kejahatan yang sudah  terjadi yang berkaitan dengan kerugian negara sebesar Rp 6,7 triliun itu. Ia menyatakan bahwa menyambut baik adanya hak angket oleh DPR mengenai persoalan besar ini, dan akan memerintahkan Menteri Keuangan (Sri Mulyani) dan BI memberikan penjelasan atau klarifikasi mengenai itu semua. Marilah sama-sama kita amati bagaimana kelanjutan perkara besar yang sangat serius ini, yang mungkin akan makan waktu yang panjang sekali dan memunculkan berbagai « surprise » pula.

Sesudah adanya laporan dan rekomendasi dari Tim 8 kepada presiden, dan sesudah presiden SBY mengumumkan sikapnya mengenai berbagai masalah besar dan parah tersebut (kasus KPK dan kasus Bank Century) maka kita semua umumnya merasa kecewa sekali. Juga amat kuatir campur curiga tentang kelanjutan urusan-urusan besar negara dan bangsa kita itu semuanya.

Produk « ipoleksosbud » Orde Baru

Berdasarkan pengalaman yang sudah-sudah, maka kita sama sekali tidak boleh menaruh harapan yang terlalu besar bahwa urusan kisruh di KPK ini akan dapat diselesaikan dengan cepat, , dan tuntas, apalagi (sekali lagi, apalagi !!!) soal skandal raksasa Bank  Century. Sebab, jaring-jaringan mafia hukum, mafia peradilan, mafia kekuasaan, mafia politik, dan mafia ekonomi yang menjadi ciri-ciri utama rejim militer Orde Baru masih bisa memainkan peran mereka yang merusak negeri kita, sampai sekarang.

Dengan sistem politik dan ekonomi pro neo-liberal yang dijalankan oleh berbagai kalangan « pimpinan » yang umumnya adalah produk busuk  -- secara langsung atau tidak langsung, dari « ipolisekbud » (ideologi, politik, sosial, ekonomi, dan kebudayaan) selama 32 tahun Orde Baru – maka sulitlah kiranya akan  adanya perbaikan atau perubahan, yang drastis, yang bisa memberantas korupsi, mafia hukum, dan mafia peradilan, atau mafia ekonomi dalam waktu dekat.

Kerusakan moral atau kebejatan akhlak yang dipertontonkan kepada seluruh negeri  melalui heboh kriminalisasi KPK (dan juga dalam kasus Bank Century) adalah manifestasi kerusakan moral yang memuncak. Agaknya, perlu sama-sama kita ingat bahwa dalam semua sistem politik, atau sistem pemerintahan, atau juga dalam sistem ekonomi, masalah kesehatan moral adalah sangat penting   Sedangkan, seperti yang kita ketahui bersama, kebanyakan dari para pejabat negara kita (atau elite kita di kalangan sipil dan swasta) sekarang ini pada umumnya tidaklah bisa dikatakan bermoral tinggi, dan juga tidak bisa dikategorikan sebagai pengabdi rakyat, apalagi (harap catat : apalagi !) dianggap sebagai patriot. Sebab, sekali lagi, mereka itu dididik, dibesarkan, dan dibina oleh  indoktrinasi atau « kebudayaan » rejim militer Orde Barunya Suharto.

Apa sebabnya kerusakan moral yang parah ini ?

Begitu parahnya kebejatan moral pejabat-pejabat tinggi di Polri, Kejaksaan, KPK, pengadilan, dan lembaga-lembaga pemerintahan lainnya (jangan lupa, di DPR juga !) sekarang ini menimbulkan pertanyaan mengapa hal semacam itu bisa terjadi, dan apa sajakah sebab-sebabnya dan apakah tidak bisa diperbaiki ? Dan mengapa di jaman pemerintahan di bawah Bung Karno yang selama 20 tahun itu korupsi dan kerusakan moral tidak separah yang kita saksikan sejak puluhan tahun Orde Baru sampai sekarang ?

Kalau dalam kasus KPK sering terdengar adanya uang suapan (dari Anggoro dan Anggodo) yang sampai bermiliar- miliar rupiah, dan bahkan dalam kasus Bank Century disebut-sebut dicurinya dana  publik sampai Rp 6,7 triliun (artinya, supaya lebih jelas, Rp 6,7 000 000 000 000 atau Rp 6,7 juta dikalikan satu juta), maka korupsi yang dilakukan pejabat-pejabat negara di bawah pemerintahan Bung Karno itu adalah ibaratnya sama dengan pegunungan Himalaya dibandingkan dengan bukit-bukit di Gunung Kidul.

Agaknya, kebanyakan « kalangan tua » yang pernah hidup di jaman pemerintahan Bung Karno, dan juga « kalangan muda » yang memperhatikan dengan cermat sejarah bangsa, akan  melihat dengan jelas sekali bahwa sekarang ini ada kemerosotan yang tajam sekali (bahkan, keambrukan ) moral di kalangan pejabat-pejabat negara (dan juga pemuka-pemuka masyarakat)  dibandingkan dengan jaman pemerintahan sebelum Orde Baru.

Jadi, kiranya makin jelas bahwa banyaknya korupsi besar-besaran yang merajalela sejak lama bersumber dari kerusakan moral, baik secara individual ataupun kolektif, yang disuburkan atau didorong oleh  sistem pemerintahan Suharto. Kalau presidennya saja (bersama anak-anaknya) sudah memberikan contoh jelek dengan mencuri harta rakyat dan negara secara besar-besaran, maka otomatis banyak orang yang mendukungnya akan juga melakukan hal yang sama.

Adalah sangat menarik untuk kita amati bersama, bahwa kerusakan moral secara besar-besaran dan yang menyebabkan suburnya korupsi, dan maraknya mafia hukum, mafia peradilan, mafia kekuasaan, dan mafia ekonomi (sejak Orde Baru sampai sekarang) terjadi  sesudah digulingkannya presiden Sukarno dan digulungnya kekuatan kiri atau kekuatan revolusioner pendukung politik Bung Karno. Artinya, kekuatan kiri atau revolusioner yang tergabung dalam berbagai partai, ormas atau golongan, di bawah pimpinan dan disemangati oleh ajaran-ajaran revolusioner Bung Karno merupakan rem kolektif atau penghalang terhadap terjadinya kemerosotan moral publik dan korupsi.

Mungkin karena itu jugalah makanya walaupun situasi ekonomi pada waktu itu sangat sulit selama revolusi 45 dan sebagai akibat perjuangan melawan Belanda, dan kemudian melawan subversi (PRRI-Permesta) dan sabotase ekonomi politik oleh nekolim (neo-kolonialisme dan imperialisme Barat) tetapi moral para pejabat dan moral publik pada waktu itu adalah jauh sekali (jauh sekali dengan garis bawah tebal) lebih baik dari pada di waktu era Orde Baru, sampai sekarang.

Bangsa kita kehilangan pedoman moral

Jadi, kalau kita amati dengan seksama, maka akan kelihatanlah bahwa sejak dikhianatinya pemimpin besar rakyat Indonesia, Bung Karno, oleh Suharto dan para pendukungnya (sipil maupun militer) dan juga dilumpuhkannya kekuatan kiri dengan cara-cara ganas dan biadab dan besar-besaran, maka negeri kita dilanda oleh kemerosotan moral, yang sebagian kecilnya saja sudah  kelihatan dalam, kasus KPK dan skandal besar Bank Century. Karena dihilangkannya kepemimpinan Bung Karno dan sekaligus  juga karena pembasmian pendukung utamanya (PKI) maka bangsa Indonesia juga kehilangan pedoman moral. Inilah juga dosa yang besar sekali Suharto beserta para pendukung setianya.

Dari sudut pandang yang demikian, maka bisalah kiranya dikatakan bahwa kemerosotan moral (dan korupsi) adalah pada dasarnya, atau pada intinya,  ciri-ciri anti-kiri dan anti ajaran-ajaran Bung Karno.
Adalah menarik sekali untuk sama-sama kita perhatikan bahwa kebanyakan (sekali lagi kebanyakan, artinya, tidak semuanya) para koruptor kakap (sipil maupun militer)  adalah terdiri dari orang-orang yang bermoral busuk dan anti-kiri atau anti-Bung Karno, yang pada hakekatnya adalah anti-rakyat.
Sekali lagi, perlu digarisbawahi, bahwa anti ajaran-ajaran revolusioner Bung Karno adalah sama saja dengan anti-rakyat. Dengan kalimat lain,  orang tidak bisa gembar-gembor  --  walaupun dengan lantang setinggi langit --   « memperjuangkan kepentingan rakyat Indonesia » tetapi sekaligus dengan bersikap anti-kiri atau anti ajaran-ajaran revolusioner Bung Karno.

Dan dari sudut pandang ini pulalah kiranya kita juga bisa  melihat berbagai masalah yang sedang kita hadapi bersama, baik mengenai kelanjutan kisruh KPK (kasus Bibit-Chandra), dan masalah besar Bank Century, maupun perjuangan bersama untuk memberantas mafia hukum, mafia peradilan, mafia kekuasaan, mafia ekonomi (termasuk mafia pengacara dan mafia makelar kasus).

Ledakan-ledakan sekitar kasus Bank Century

Karena banyaknya masalah parah dan besar di banyak bidang (artinya, bukan hanya Bank Century saja), dan mengingat sikap moral kebanyakan para pejabat dan kalangan elite umumnya (antara lain DPR !!!) maka rakyat harus lebih berani dan lebih banyak lagi (dari pada yang sudah-sudah) melakukan kontrol atas jalannya pemerintahan, dengan melancarkan berbagai aksi atau kegiatan extra-parlementer.

Dengan masih hangatnya lanjutan persoalan KPK dan akan makin meledak-ledaknya dengan lebih hebat lagi kasus Bank Century di masa datang yang dekat ini, maka kita bisa meramalkan bahwa gerakan rakyat melalui bermacam-macam kegiatan atau aksi akan masih berlangsung terus atau bahkan makin berkembang. Aksi-aksi yang dilancarkan oleh kalangan muda (mahasiswa dan ormas-ormas pemuda lainnya) yang sudah bersemarak di banyak kota mungkin akan lebih meluas lagi di kalangan masyarakat lainnya (buruh, pegawai, tani, perempuan dll) sesuai dengan meningkatnya suhu berbagai masalah itu sendiri.

Itu semua merupakan perkembangan yang baik sekali bagi kehidupan bangsa kita. Dan dengan banyaknya berita dan artikel dalam pers, dan siaran-siaran televisi (terutama sekali Metro TV dan TVOne ) ditambah dengan berbagai rapat-rapat dan macam-macam pertemuan akan merupakan pendidikan politik secara besar-besaran bagi rakyat yang jarang terjadi sebelumnya.

Pendidikan politik besar-besaran ini sangat besar sumbangannya kepada perjuangan bersama untuk melawan neo-liberalisme, mengekspose lebih telanjang lagi kebusukan Orde Baru dan kejahatan sisa-sisa kekuatannya, dan bersamaan dengan itu sekaligus juga mengembangkan lebih luas dan lebih besar lagi kekuatan demokratis dengan berpedoman kepada ajaran-ajaran revolusioner Bung Karno.

Ini semua adalah penting ketika rakyat dan negara kita sedang mengantisipasi terjadinya ledakan-ledakan yang lebih mengejutkan lagi sekitar skandal raksasa Bank Century.
