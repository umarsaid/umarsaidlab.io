---
layout: post
title: Eks anggota PKI harus didaftar ?
date: 2004-01-19
---

Ada persoalan serius yang menuntut perhatian dan kewaspadaan kita semuanya. Begitu seriusnya persoalan ini, sehingga kalau tidak dilawan bersama-sama bisa berkermbang menjadi bencana yang lebih membahayakan persatuan rakyat atau menambah rumitnya banyak persoalan yang sedang dihadapi negara dan bangsa Persoalan serius ini diungkap oleh harian Sinar Indonesia Baru (Medan) tanggal 17 Januari 2004, yang antara lain memberitakan sebagai berikut :

« Menghadapi Pemilu 2004 yang sudah diambang pintu, Koramil harus benar-benar bekerja keras mencari data-data yang jelas dan lengkap tentang keberadaan penduduk yang tinggal di wilayahnya masing-masing mulai dari data masyarakat hingga data eks PKI. Jika memang masih ada ditemukan eks anggota PKI di wilayah Koramil 02/ST, Danramil harus benar-benar menguasai alamat mereka yang pasti. Demikian dikatakan Dandim 0207/Simalungun Letkol Inf Marwan Saragih saat kunjungan kerja (kunker) di Makoramil 02/ST, Kamis (15/1).

« Turut hadir Danramil 02/ST, tokoh masyarakat Drs J Pasaribu, Camat Siantar Timur Drs R Samosir, Kapos Pol Pantoan Aiptu Maruli S, Ketua Panwaslu Siantar Timur, para lurah se kecamatan Siantar Timur, pengurus dan anggota Persit KCK, OKP, masyarakat.

« Dikatakan, pekerjaan mencari data-data yang lengkap tentang eks PKI bukanlah pekerjaan yang main-main dan tidak bisa ditawar-tawar. Untuk itu Koramil 02/ST diminta benar-benar serius melaksanakan tugas mencari data-data yang lengkap terhadap eks anggota PKI yang ada di wilayah Koramil 02/ST. Jika memang eks anggota PKI sudah meninggal, Danramil harus memiliki data lengkap di mana kuburannya dan jika memang masih hidup di mana alamatnya yang jelas.

« Keseriusan Koramil sangat menunjang keberhasilan pelaksanaan Pemilu 2004 yang akan datang. Oleh sebab itu kedekatan dan hubungan TNI baik dengan masyarakat serta Polri harus benar-benar ditanamkan agar apa yang diharapkan dapat terwujud sesuai dengan harapan kita bersama yakni negara serta masyarakat yang aman, tenteram dan kondusif. (kutipan selesai)

## Apakah Ini Sikap Pemerintah/TNI-AD?

Setelah membaca berita tersebut di atas, maka wajarlah bahwa banyak pertanyaan bisa muncul di kepala banyak orang. Umpamanya : Apakah pernyataan Komandan Kodim Simalungun itu mencerminkan sikap atau politik pemerintah secara nasional ? Apakah ada peraturan (ketentuan) atau kebijakan pemerintah yang mengharuskan pencarian data-data tentang eks anggota PKI ? Apakah instruksi Komandan Kodim tersebut diketahui dan disetujui oleh pimpinan TNI-AD ? Mengapa secara khusus dinyatakan bahwa alamat pasti para eks anggota PKI harus benar-benar dikuasai oleh Koramil? Apa yang dimaksudkan kalimat bahwa « pekerjaan mencari data-data yang lengkap tentang eks PKI bukanlah pekerjaan yang main-main dan tidak bisa ditawar-tawar »? Apakah tidak « keterlaluan » adanya instruksi untuk memiliki data lengkap tentang keberadaan kuburan bagi anggota PKI yang sudah meninggal ? Bagaimanakah sikap pemerinath, DPR atau DPRD mengenai instruksi Komandan Kodim ini ?

Logisnya, Komandan Kodim yang bersangkutan, tidak bisa atau tidak berhak mengeluarkan instruksi yang demikian penting, tanpa garis yang ditetapkan oleh atasannya. Kalau betul menurut garis atasannya, maka atasannya sampai tingkat yang mana ? Dan kalau bukan garis dari atasannya, maka apakah tindakan yang dilakukan secara perseorangan ini tidak harus diberi hukuman (sanksi) oleh pimpinan TNI-AD?

Kalau tindakan Komandan Kodim tersebut berdasarkan garis dari « atasan » maka persoalannya menjadi lebih serius lagi, karena bisa mempunyai dimensi yang lebih besar dan menyangkut berbagai soal nasional yang sensitif.

## Mengingatkan Pembantaian Besar-Besaran Tahun 65

Tidak bisa disangkal bahwa pernyataan Komandan Kodim Simalungun tentang pengumpulan data-data eks anggota PKI itu bisa mengingatkan kepada masa-masa suram dalam tahun 65/66 ketika banyak anggota-anggota PKI – dan bukan anggota – dibunuhi di berbagai tempat di seluruh Indonesia oleh militer. Di antara mereka banyak yang, sesudah terjadinya G30S, mematuhi seruan penguasa militer untuk melaporkan diri. Mereka ini banyak yang « dihabisi » oleh militer setelah melapor, atau « dicomot » dari tempat tahanan dan penjara.

Pengumpulan data-data eks anggota PKI juga mengingatkan kepada tindakan Kopkamtib dan Kopkamtibda dalam mempersekusi golongan kiri beserta keluarga mereka, menahan secara sewenang-wenang ratusan ribu tapol dalam jangka waktu yang lama, bahkan banyak yang sampai puluhan tahun.

Karena itu, pernyataan Komandan Kodim Simalungun menimbulkan pertanyaan yang mengandung penuh kecurigaan : « untuk apa dan mengapa ada pengumpulan data » ? Sebab, kalau demi keamanan dan ketertiban, maka jelaslah bahwa tindakan penguasa militer ini salah arah atau salah alamat. Hal ini sebenarnya sudah pasti diketahui sendiri – bahkan sampai secara detail - oleh berbagai instansi pemerintah, temasuk instansi militer di semua tingkat.

Seperti yang sudah sama-sama kita saksikan selama ini, bahaya yang mengancam keamanan dan ketertiban masyarakat tidaklah datang dari kalangan eks PKI, tetapi dari kalangan yang justru memusuhi PKI atau memusuhi pendukung Bung Karno umumnya. Mereka ini kebanyakan terdiri dari unsur-unsur pendukung Orba, dan juga unsur-unsur lainnya (antara lain sebagian dari kalangan Islam) yang tidak senang dengan proses demokratisasi dan reformasi.

## Tidak Menguntungkan Usaha Rekonsiliasi

Jelas kiranya bagi banyak orang, bahwa instruksi Komandan Kodim Simalungun itu tidak menguntungkan suasana yang sedang diusahakan diciptakan oleh berbagai kalangan ke arah tercapainya rekonsiliasi nasional. Instruksi pengumpulan data-data eks anggota PKI « yang tidak bisa ditawar-tawar » ini sarat dengan muatan politik yang sudah kedaluwarsa dan juga berbahaya. Sebab, merupakan dosa dan aib yang monumental kalau, sekarang ini dalam tahun 2004, TNI-AD masih mau meneruskan kesalahannya yang sudah dilakukan selama lebih dari 32 tahun, yaitu menyiksa atau mencelakakan banyak orang-orang tak bersalah.

Instruksi pengumpulan data-data eks anggota PKI, entah di Simalungun atau di tempat-tempat lainnya, mempunyai konotasi (atau mengandung pengertian) bahwa TNI-AD masih mempertahankan « teori » yang salah bahwa anggota-anggota PKI adalah orang-orang yang berbahaya dan karenanya perlu diawasi terus, walaupun mereka sudah terpaksa dibebaskan dari tahanan karena terbukti tidak bersalah apa-apa. Opini umum di Indonesia sekarang ini sudah menyetujui pendapat bahwa pembantaian besar-besaran tahun 65 dan penahanan sewenang-wenang terhadap tapol adalah pelanggaran besar yang dilakukan oleh penguasa militer terhadap kemanusiaan.

Opini umum di negeri kita juga mulai mempertanyakan kebenaran versi penguasa militer (Orba) mengenai sejarah terjadinya G30S, mengenai peran Suharto dan Bung Karno dalam G30S. Banyak bukti sekaang muncul mengenai campurtangan kekuatan asing (CIA) di belakang kemelut penggulingan Bung Karno dan dilumpuhkannya kekuatan kiri pendukung politik Bung Karno. Sekarang makin jelas bahwa Bung Karno telah digulingkan oleh kekuatan reaksioner dalamnegeri yang dipelopori oleh TNI-AD dengan dukungan kekuatan Barat (terutama AS) pada waktu itu.

## Harus Dipersoalkan Secara Nasional

Mengingat itu semua, jelaslah bahwa instruksi pengumpulan data eks anggota PKI oleh Dandim Simalungun itu mengandung tujuan yang tidak mendatangkan kebaikan bagi bangsa kita. Oleh karena itu masalah ini perlu diangkat tinggi-tinggi, dipersoalkan secara beramai-ramai dan secara nasional. Perlu ditempuh berbagai jalan dan cara untuk menyampaikan persoalan serius ini kepada Pemerintah Pusat , DPR dan pimpinan tertinggi TNI-AD.

Perlu disarankan kepada pimpinan TNI-AD supaya kasus Simalungun diperiksa secara baik-baik, demi perbaikan citra TNI-AD dalam usaha pengkoreksian kesalahan-kesalahan TNI-AD di masa yang lalu, baik mengenai Bung Karno maupun terhadap para pendukung beliau, termasuk eks anggota PKI. Melalui kasus Simalungun kita bisa melihat bahwa sikap TNI-AD (mungkin sebagian ?) yang salah mengenai eks anggota PKI pada pokoknya masih belum berobah. Dan ini patut disayangkan ! Sebab sikap yang demikian ini tidak menguntungkan usaha bersama untuk mempersatukan bangsa.

Instruksi pengumpulan data mengenai eks anggota PKI oleh instansi militer akan berdampak memarginalkan atau mengucilkan sebagian dari bangsa kita, dan menimbulkan kecurigaan atau permusuhan. Instruksi ini juga akan menimbulkan ketegangan (yang tidak perlu !) dengan TNI-AD, Instruksi ini juga bisa menjurus kepada pelestarian pendapat bahwa TNI-AD adalah musuh PKI dan Bung Karno. Adanya pendapat bahwa TNI-AD memusuhi PKI (dan Bung Karno) tidaklah menguntungkan sama sekali citra TNI-AD sendiri sebagai alat negara. Citra buruk yang telah disandang TNI-AD selama lebih dari 32 tahun ini harus dibuang jauh-jauh.

## TNI-AD Harus Memperbaiki Diri

Kasus Simalungun membuktikan bahwa TNI-AD masih perlu terus-menerus memperbaiki diri dan menjalankan reformasi. Sekarang sudah terbukti bahwa kesalahan-kesalahan terbesar dari TNI-AD adalah menggulingkan Bung Karno, menindas kekuatan kiri yang dipimpin PKI, mendirikan diktatur militer Orde Baru, melakukan pembunuhan besar-besaran tahun 65, menahan tapol secara sewenang-wenang dalam jangka puluhan tahun, menindas kehidupan demokrasi, menjalankan korupsi secara besar-besaran.

Sudah tibalah sekarang saatnya bagi TNI-AD untuk mengkoreksi berbagai kesalahan-kesalahan serius yang sudah dilakukannya selama 32 tahun Orde Baru. TNI-AD tidak bisa lagi bersikap tetap seperti masa-masa ketika Bung Karno digulingkan. Situasi dalamnegeri sedang terus mengalami perobahan. Situasi internasional juga sudah banyak berobah. Perang dingin sudah menjadi sejarah. Yang tadinya menjadi sekutu AS (secara nasional dan internasional) sudah banyak yang menjadi lawan AS sekarang. Aliansi baru dan regrouping baru juga terus muncul di dunia. Begitu jugalah di Indonesia. Yang pernah mendukung Orba dan ikut menentang politik Bung Karno sekarang banyak yang sudah berbalik.

TNI-AD sekarang ini haruslah berbeda dari TNI-AD yang di bawah pimpinan Suharto !
