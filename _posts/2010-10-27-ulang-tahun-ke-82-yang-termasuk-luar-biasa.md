---
layout: post
title: Ulang Tahun ke-82 yang termasuk luar biasa
date: 2010-10-27
---

Tulisan kali ini menyajikan hal-hal yang berkaitan dengan hari ulangtahun saya yang ke 82, yang saya anggap agaknya mengandung berbagai aspek yang cukup menarik untuk disimak sekadarnya oleh para pembaca. Sebab, dalam cerita tentang perayaan hari ulang tahun kali ini, terselip soal-soal yang menyinggung persoalan-persoalan yang bukan hanya masalah pribadi saja, melainkan  juga berbagai hal yang berkaitan dengan masalah politik, perjuangan untuk HAM  persahabatan dan solidaritas yang berkesinambungan, dan perjuangan untuk demokrasi.

Untuk memulai kisahnya, baiklah disebutkan terlebih dulu, bahwa seperti biasanya dilakukan selama bertahun-tahun, saya dan istri tidak merencanakan mengadakan perayaan apa-apa , kecuali makan bersama sekeluarga, dengan anak dan menantu dan cucu (umur 7 tahun).

Namun, seorang teman wanita Prancis, Danielle DESGUEES bersama suaminya, mengusulkan supaya ultah saya yang kali ini dirayakan di rumah mereka bersama-sama teman lama lainnya  . Karena itu, pada tanggal 23 Oktober malam telah hadir dalam pertemuan yang kecil itu 14  orang, yang terdiri dari teman-teman dekat sekali. Untuk memberi gambaran sekedarnya tentang mereka yang hadir malam itu dan arti pentingnya, maka berikut ini adalah sedikit keterangannya :

Yang membantu berdirinya Restoran INDONESIA

Danielle DESGUEES adalah sekarang pimpinan suatu badan di Paris yang bernama Boutique de Gestion de Paris, yang missinya adalah membantu atau memberi nasehat kepada orang-orang yang mau mendirikan atau mengembangkan perusahaan-perusahaan mereka. Sampai sekarang sudah ada lebih dari  12 000 perusahaan di Paris dan sekitarnya yang pernah dibantu dengan berbagai cara. Dan di seluruh Prancis sudah ada kurang lebih 100. 000 perusahaan yang pernah mendapat berbagai macam bantuan (nasehat, konsultasi, petunjuk, kemitraan dll). Oleh karena itu ia mendapat penghargaan yang cukup tinggi dan terhormat dari pemerintah Prancis, yaitu  gelar Legion d’honneur.

Danielle DESGUEES adalah juga seorang yang dalam tahun 1982 (artinya 28 tahun yl) membantu dalam pendirian atau pembukaan Restoran Indonesia, antara lain dengan memperjuangkan didapatkannya subsidi dari pemerintah sebanyak 160.000 francs untuk modal restoran kooperasi (dari jumlah 500. 000 francs yang dibutuhkan). Sejak itu, sampai sekarang, ia bersama suaminya, François, menjadi sahabat restoran koperasi kita. Waktu ia beserta seluruh keluarganya (4 orang) vakansi ke Indonesia mereka disambut hangat sekali oleh keluarga bung Joesoef Isak di Jakarta dan menginap di rumah mereka beberapa hari,  dan mulai dari saat itu mereka juga menjadi sahabat dekat.

Hadir juga pada malam itu bung Suyoso, seorang yang sama-sama dengan saya sebagai orang « klayaban » terpaksa hidup di Prancis dengan menciptakan kerja sendiri dengan membuka Restoran Koperasi INDONESIA. Ia bertugas sebagai wakil manager (menggantikan saya) sejak tahun 1986,dan kemudian menjadi managernya.

Langganan restoran selama 28 tahun

Malam itu datang juga seorang pegawai kantor pos, Jean Claude, penganut « kental » agama Katolik, yang sejak restoran berdiri menjadi pengunjung yang luar biasa setianya. Selama bertahun-tahun, ia makan di restoran kita, boleh dikatakan setiap minggu. Entah berapa uang yang telah dipakainya untuk makan dan minum di restoran kita selama itu, sampai sekarang. Sudah jelas, bahwa ia begitu sering mengunjungi restoran kita itu bukan hanya karena makanannya saja, melainkan karena ia merasakan adanya hal-hal lainnya yang mengikat hatinya.

Kita semua senang malam itu betemu dengan Jean Mata, seorang aktivis LSM yang hidup dalam gerakan ekonomi alternatif. Ia pernah bekerja selama 6 atau 7 tahun untuk mengurusi pembukuan atau administrasi keuangan ketika restoran kita baru berdiri dalam tahun 1982. Ia pernah mengatakan bahwa ia mendukung tujuan-tujuan , yang intinya adalah justru untuk menciptakan persaudaraan dan kemanusiaan. Oleh karena itu ia sampai sekarang tetap mempunyai simpati yang besar kepada restoran kita beserta para karyawannya.

Juga hadir dalam pertemuan malam itu Ibarurri, yang bersama almarhum suaminya ikut mengumpulkan dana (sumbangan dan pinjaman) dari berbagai teman Indonesia di Holland dan Jerman ketika restoran baru dibuka.

Seorang wanita Prancis (namanya Ghislaine) yang sudah lama sekali  jatuh cinta kepada Indonesia, dan karenanya sudah mengunjungi berbagai pulau kita sampai  17 kali, juga ikut menghadiri perayaan ultah malam itu. Di samping itu, malam itu juga dibikin hangat dengan hadirnya Martin bersama istrinya. Martin adalah aktifis politik di Amerika Latin, dan pernah dipenjara selama 8 tahun oleh rejim militer Argentina. Istri saya Ninon, dengan sendirinya juga ikut serta dalam perayaan itu.

Bukan perayaan ultah yang biasa saja

Agaknya, dari komposisi yang hadir dalam pertemuan itu saja – seperti yang disebutkan di atas -- sudah kelihatan bahwa perayaan ulangtahun malam itu bukan hanya perayaan ulangtahun yang biasa saja,  tetapi mempunyai warna dan isi yang lain, yang berkaitan dengan masalah-masalah sejarah perjuangan untuk menentang Suharto dengan Orde Barunya, dengan penggalangan solidaritas untuk membela HAM, dengan kegiatan untuk mengembangkan gerakan koperasi atau ekonomi alternatif.

Namun, puncak dari « ketidak-biasaan » perayaan ulangtahun malam itu adalah hadirnya seorang besar di Prancis, Louis JOINET, seorang ahli hukum yang terkenal dan dihormati oleh banyak kalangan, baik di Prancis maupun di luar Prancis.  Arti kehadirannya itu sangat penting  -- dan juga menarik – dan karenanya bisa diceritakan secara pokok-pokok sebagai berikut :

Louis JOINET, sekarang usia 76 tahun dan sudah pensiun, adalah mantan penasehat Presiden Prancis, François MITTERRAND. Dan karena terkenal sebagai ahli hukum yang mempunyai integritas yang tinggi, dan disenangi oleh banyak kalangan, ia pernah dipercayai untuk menjabat sebagai penasehat hukum (conseiller juridique) oleh 5 (harap perhatikan : lima !) Perdana Menteri Prancis secara terus-menerus dan berturut-turut, selama Partai Sosialis memegang pemerintahan.

Selama lebih dari 25 tahun ia adalah wakil Prancis (dan ketua) di badan PBB di Jenewa yang membidangi angket mengenai pelanggaran HAM di dunia. Oleh karena itu ia sering bepergian kemana-mana untuk mengurusi pelanggaran HAM, pemenjaraan sewenang-wenang, penganiayaan dll. Untuk itu ia sudah beberapa kali ke Timor Timur, dan juga mengunjungi penjara Cipînang di Jakarta untuk bertemu dengan Xanana Gusmao ketika ia masih dalam tahanan. Ia dekat sekali dengan Jose Ramos Horta yang dulu sering berhubungan dengan kantor PBB di Jenewa.

Laporan-laporan dia menjadi dokumen internasional dan disiarkan oleh PBB, dan ia sudah menulis buku tentang Impunitas yang terjadi di dunia. Sebelum menjadi penasehat hukum dari 5 Perdana Menteri Prancis ia dipilih sebagai sekjen dari Persatuan Hakim-hakim Prancis.

Bagaimana Louis JOINET kecantol soal-soal Indonesia

Lalu, bagaimana ceritanya maka ia sampai « kecantol » kepada soal-soal Indonesia ? Cerita singkatnya begini :

Dalam tahun 1976, sebagai salah satu dari pendiri Komite Timor di Paris saya mencari hubungan dengan Louis Joinet, yang waktu itu menjabat sebagai sekjen Persatuan Hakim-hakim Prancis. Karena,  ia waktu itu sudah menyatakan protesnya kepada rejim Suharto yang mengagresi Timor Timur. Sejak itu, hubungan saya sebagai political refugee dari Indonesia dengannya menjadi dekat, karena ia mengikuti kejadian-kejadian di Indonesia secara cukup teratur. Waktu itu ia juga menjadi tokoh dari badan internasional  Tribunal du Peuple, yang punya hubungan dengan Bertrand Russel Foundation yang terkenal itu.

Karena itu, ketika Komite Timor dan Komite Tapol melakukan macam-macam kegiatan di Prancis ia juga mengikutinya, suatu bukti bahwa perhatiannya terhadap masalah pelanggaran dan kejahatan rejim Suharto adalah besar sekali. Dengan sendirinya, perhatian dan solidaritasnya terhadap kami yang jadi political refugee di Prancis juga besar sekali.

Sikap yang mencerminkan pandangannya yang kiri, atau progressif, atau humanis ini juga ditunjukkan kepada usaha kolektif kita, Restoran INDONESIA. Selama 28 tahun berdirinya  restoran koperasi ini, ia berkali-kali memberi bantuan untuk menyelesaikan soal-soal ketika menghadapi persoalan-persoalan. Antara lain, yaitu  ketika restoran kita dikenakan larangan bank dan, karenanya,  restoran kita tidak boleh menggunakan cheque untuk pembayaran apa pun juga. Oleh karena masalah ini tidak datang dari kesalahan restoran kita, maka ketika kasus ini dilaporkan kepada Louis JOINET, maka ia segera menghubungi Bank de France. Karena urusan ini ditangani oleh Louis JOINET, penasehat hukum Perdana Menteri, maka dengan begitu selesailah dengan segera persoalannya.

Bagi pekerja-pekerja restoran kita, sikapnya yang luar biasa simpatiknya adalah yang berikut : setiap kali ia melewati depan restoran kita dengan mobilnya, ia minta kepada sopirnya untuk berhenti dan menunggunya sebentar, karena ia mau sekadar mengatakan « bon jour » (selamat siang) kepada para pegawai, termasuk yang bekerja di dapur. Sungguh, jarang ada pembesar yang begitu tinggi pangkatnya, yang bertindak seperti itu. Apalagi, ia tahu bahwa para pegawai restoran kita sebagian terbesar adalah political refugee dari Indonesia, yang dimusuhi oleh penguasa-penguasa negara mereka sendiri.

Beberapa anecdote tentang pengalaman Louis JOINET

Pada malam pertemuan itu, Louis JOINET menceritakan berbagai peristiwa, termasuk anecdote-anecdote yang menarik tentang « kerjasama » antara dia dengan kita. Di antaranya ia menceritakan bahwa suatu waktu ia perlu rundingan tentang soal yang pelik sekali dengan seorang. Karena ia tidak bisa menggunakan kantornya di gedung Perdana Menteri (namanya gedung Matignon) mengingat peliknya persoalan, maka ia minta kepada kami untuk bisa menggunakan ruangan bawah restoran sehingga tidak bisa diketahui orang lain. Peristiwa ini sering diceritakan kembali oleh Louis JOINET. Tentunya, masalahnya adalah pelik sekali, sehingga walaupun sudah lewat puluhan tahun, ia masih ingat saja kepada peristiwa itu.

Ia juga menceritakan bahwa untuk merayakan hari ulang tahun istrinya (namanya Germaine) diundang puluhan kawan-kawannya, yang termasuk orang-orang penting. (Di antara tamu-tamu yang semuanya bule-bule itu hanya kami berdua, dengan istri, yang orang Asia. Dari situ kami dapat mengukur perhatiannya terhadap kami, political refugee dari Indonesia)  Louis JOINET menceritakan kepada yang hadir malam itu bahwa saya membawa sepasang wayang golek sebagai hadiah ulang tahun istrinya, dan bahwa hadirin malam itu tertarik sekali kepada adegan (suatu dialog antara suami istri) yang saya mainkan sebentar malam itu.

Siapa menyangka, orang Blitar diundang ke Istana Elysée

Pada malam pertemuan antara sahabat-sahabat lama itu, saya menceritakan betapa besar emosi saya ketika mendapat undangan dari Palais de l’Elysée (Istana Kepresidenan) untuk menghadiri malam musik dan jamuan makan. Ketika berada di ruangan besar istana dengan karpet dan dekorasi yang serba indah itu, saya mengatakan kepada istri saya : »Siapa bisa menyangka bahwa orang dari Blitar  dan dari Solok seperti kita ini bisa hadir dalam malam yang seperti ini di Istana Elysée ? ». (Ketika kami lihat ke segala jurusan, maka nampak bahwa hanya kami berdualah yang berwajah Asia di antara ratusan undangan itu).

Sampai sekarang saya tidak tahu siapa yang mengusulkan nama saya (dan istri) dalam daftar orang-orang yang diundang untuk hadir dalam malam musik dan jamuan makan itu. Tentu saja, adalah  orang atau orang-orang yang tahu betul berbagai kegiatan mengenai persoalan Indonesia. Apa Louis JOINET ?
Saya tidak mau dan tidak merasa perlu menanyakannya kepadanya, walaupun saya bertemu dengannya di Istana malam itu. Saya peluk dia, sambil menangis, karena merasa terharu, dengan kehormatan yang berupa undangan itu.

Itu semua saya ceritakan serba singkat dan pokok-pokok untuk menyampaikan bahwa perayaan ultah saya kali ini diwarnai atau diisi dengan berbagai hal yang berkaitan dengan kenangan kepada masa-masa persahabatan dalam perjuangan untuk menentang rejim militer Suharto, untuk membela perjuangan rakyat Timor Timur, untuk memperjuangkan HAM, untuk membela orang-orang yang dimusuhi oleh Orde Baru, baik di luar negeri maupun di Indonesia.

Pertemuan malam itu, walaupun hanya dihadiri oleh belasan orang, menunjukkan bahwa perjuangan melawan diktator yang kejam, biadab, dan korup yang namanya Suharto mendapat dukungan simpati atau  solidaritas dari berbagai kalangan di Prancis ( dan juga di negara-negara lainnya di dunia).

Malam perayaan ultah saya itu berarti bukan malam ultah biasa, dan tidak hanya untuk merayakan sesuatu tentang saya saja. Melainkan, melalui malam ini dirayakan juga keindahan dan kebesaran jiwa persahabatan dalam perjuangan yang sudah dipupuk dan ditunjukkan selama puluhan tahun. Malam itu juga mengandung arti penghargaan atau penghormatan kepada semua orang, -- dimana saja, walaupun tidak ikut hadir dalam pertemuan itu --  yang berjuang menentang Orde Barunya Suharto. Karena itu, patutlah agaknya bahwa semua mereka yang berjuang untuk keadilan dan perikemanusiaan juga menghargai dan ikut bangga terhadap sikap seperti yang telah ditunjukkan oleh Louis JOINET dan Danielle DESGUEES (serta sahabat-sahabat lainnya) selama jangka waktu yang begitu panjang..

Dengan perasaan yang lega dan ingatan indah semacam itulah saya lalui usia yang ke-82 tahun ini.
