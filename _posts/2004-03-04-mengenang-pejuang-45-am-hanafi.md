---
layout: post
title: Mengenang pejuang 45 A.M. Hanafi
date: 2004-03-04
---

Wafat 2 Maret 2004 di Paris

Membuat tulisan tentang wafatnya seorang kawan adalah suatu hal yang tidak mudah, karena berkaitan dengan berbagai emosi, kenangan, dan pandangan. Apalagi, kalau kawan itu termasuk tokoh yang cukup menonjol dalam sejarah perjuangan lahirnya Republik Indonesia, yaitu A. M. Hanafi (lengkapnya : Anak Marhaen Hanafi). Tulisan kali ini, dimaksudkan sebagai tanda penghormatan kepada seorang yang sejak mudanya, sampai usia lanjutnya yang 87 tahun itu, terus dengan gigih atau konsekwen menjadi pendukung dan pembela gagasan-gagasan politik Bung Karno

Pada tanggal 2 Maret 2004 jam 22.40 ia telah wafat di rumahsakit George Pompidou, Paris, setelah dirawat sejak sehari sebelumnya, karena pendarahan pencernaan.

Bapak A.M. Hanafi adalah salah seorang dari kelompok pemuda Menteng 31, yang dalam 1945 memainkan peran penting dalam hari-hari bersejarah menjelang proklamasi 17 Agustus 1945, dan dalam masa revolusi. Semasa hidupnya, ia terkenal sebagai kader Bung Karno dan pernah menjadi ketua Partindo, pimpinan Front Nasional, dan Menteri Pengerahan Tenaga Rakyat. Kepadanya telah dianugerahkan Bintang Mahaputera dan pangkat tituler Letnan Jenderal TNI.

Dalam usia yang sudah sangat lanjut, ia masih selalu sangat memperhatikan perkembangan situasi politik di Indonesia. Dengan wafatnya, barisan pendukung politik dan ajaran Bung Karno telah kehilangan salah seorang tokohnya.

## Penghormatan Dari Banyak Fihak

Berita tentang wafatnya Pak A. M. Hanafi telah tersiar sejak tanggal 2 Maret malam, baik melalui telpon maupun lewat Internet, yang dilakukan oleh anggota keluarganya maupun oleh berbagai kawan di Paris dan negeri-negeri lain. Sejak itu, para anggota keluarga Bapak A.M. Hanafi terus-menerus menerima telpon tanpa berhenti. Pada tanggal 3 Maret siang diadakan upacara penghormatan jenazah yang dilakukan di rumahsakit George Pompidou, Paris, dengan mendapat perhatian besar dari berbagai kalangan.

Dubes RI untuk Perancis, A. Silalahi dan Nyonya, beserta banyak anggota staf KBRI memerlukan datang ke rumahsakit untuk memberikan penghormatan. Demikian juga keluarga besar restoran koperasi INDONESIA.

Kalau dilihat dari sudut sejarah dan perjuangan rakyat Indonesia, perhatian atau ponghormatan KBRI di Paris atas wafatnya Bapak A.M. Hanafi ini adalah wajar dan sudah semestinya atau sepatutnya. Sebagai wakil pemerintah, atau wakil negara, Dubes RI memang patut memberikan penghormatan kepada arwah seorang pejuang bersejarah, yang dalam hidupnya pernah memainkan peran penting untuk proklamasi 17 Agustus 1945, bersama-sama dengan tokoh-tokoh pemuda lainnya, seperti Sukarni, Chaerul Saleh, Wikana, Sidik Kertapati, Aidit, dll. dll

## Seperti Bung Karno, Korban Orde Baru

Kalau dikaji dalam-dalam, wafatnya Pak A.M. Hanafi bisa mengandung berbagai arti yang cukup serius, dari segi politik dan sejarah. Kasus Bapak A.M. Hanafi adalah contoh yang gamblang dari keadaan yang kacau-balau atau “jungkir-balik” yang ditimbulkan oleh rezim militer Orde Baru, seperti halnya dengan kasus Bung Karno,

Sebab, jelas bahwa Pak Hanafi tidak ada sangkut-pautnya dengan G30S. Karena, ketika peristiwa itu meletus ia menjabat sebagai Duta Besar RI di Kuba. Tetapi, hanya karena kedekatannya dengan Bung Karno-lah maka ia kemudian di-“kucil”-kan dan disingkirkan dari jabatannya sebagai Duta Besar. Pak Hanafi kemudian terpaksa meninggalkan Kuba dan minta suaka politik di Prancis, di mana ia terpaksa tinggal lebih dari 30 tahun, sampai wafatnya. Pak Hanafi, yang ikut memperjuangkan kelahiran Republik Indonesia, terpaksa wafat di tanah pengasingan, dan bukan di tanah-airnya sendiri yang ia cintai. Satu hal yang ironis sekali.

Jelas bahwa Pak Hanafi adalah korban politik rezim militer Orde Baru, seperti halnya Bung Karno. Mereka berdua, seperti banyak orang lainnya, telah menjadi korban dari pengkhianatan Suharto beserta para pendukungnya. Sekarang makin banyak bukti-bukti sejarah yang menjelaskan bahwa Suharto dan para pendukungnya telah melakukan pengkhianatan terhadap revolusi rakyat Indonesia dengan bantuan kekuatan asing (antara lain imperialisme AS, Inggris, Australia)

Menurut cerita kalangan keluarga Pak Hanafi, akhir-akhir ini beliau sering sekali berbicara tentang pentingnya rehabilitasi Bung Karno. Karena, sebagai seorang yang sangat dekat dengan Bung Karno, ia merasa betul bahwa Bung Karno sudah diperlakukan secara tidak adil dan secara kejam oleh para penguasa Orde Baru. Sebab, Bung Karno yang sudah memimpin perjuangan rakyat Indonesia sejak umur mahasiswa dan menjadi proklamator kemerdekaan RI (bersama Bung Hatta) akhirnya dibikin sebagai tahanan politik oleh Suharto dkk.

## Akan Dimakamkan Di Indonesia

Dalam satu percakapan, Dubes Silalahi menegaskan bahwa sebagai tokoh perjuangan yang bersejarah Pak Hanafi memang sudah sepatutnya mendapat penghormatan selayaknya dari KBRI dan dari kita semua. Oleh karenanya, KBRI akan memberi bantuan semaksimal mungkin kepada keluarga Bapak A.M. Hanafi, apa pun keputusan yang diambil nantinya.

Sedangkan dari keluarga Bapak A.M. Hanafi didapat keterangan bahwa jenazahnya akan diterbangkan ke Indonesia. Memang tadinya ada rencana untuk dimakamkan di Paris di samping makam istrinya, yang sudah mendahuluinya setahun yang lalu. Tetapi ada saran-saran dari banyak kalangan di Jakarta, yang menganjurkan supaya jenazah Pak Hanafi dimakamkan di Jakarta saja, supaya mudah dikenang oleh banyak orang.

Oleh karena itu keluarga Pak Hanafi sudah memutuskan untuk memakamkan jenazahnya di Indonesia. Tetapi, karena Pak Hanafi berkali-kali pesan bahwa kalau ia wafat maka makamnya haruslah berdampingan dengan makam Ibu Hanafi, maka untuk menghormati pesan itu makam Ibu Sukendah Hanafi akan dipindahkan kemudian hari dari Paris ke Indonesia.

Untuk itu, para puteranya, beserta kawan-kawan seperjuangannya di Indonesia, yang umumnya terdiri dari para pendukung politik Bung Karno, akan mengusahakan supaya pemakaman Pak Hanafi di Indonesia berlangsung secara selayaknya.

## Kecintaan Kepada Bung Karno

Dalam kehidupannya di Paris selama lebih dari 30 tahun, karakteristik Pak Hanafi yang menonjol sekali adalah kesetiaannya, kekagumannya, atau kecintaannya kepada Bung Karno. Tidak salahlah kalau dikatakan bahwa kecintaannya terhadap Bung Karno merupakan sumber semangat perjuangannya, baik selama di tanahair maupun di luar negeri.

Hal i ni adalah wajar dan bisa sepenuhnya bisa dimengerti, kalau diiingat bahwa Pak A.M. Hanafi telah diambil sebagai anak angkat Bung Karno ketika beliau di-“buang” ke Bengkulu oleh pemerintah kolonial Belanda.

Yang tidak sempat disaksikan olehnya, karena keburu wafat, adalah datangnya masa yang menjadi idamannya, yaitu rehabilitasi bagi Bung Karno. Tetapi, pada saatnya, rehabilitasi Bung Karno ini pasti akan menjadi kenyataan.
