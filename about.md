---
layout: page
title: Tentang website ini
---
Mengapa dibuat website personal ini?

Pertama-tama, dimaksudkan sebagai dokumen “pribadi”, yang sekaligus juga sebagai arsip keluarga. Oleh karena isinya berkaitan dengan berbagai soal, maka juga bisa berfungsi sebagai kenang-kenangan bagi sanak-saudara keluarga saya (baik dekat maupun jauh), dan juga bagi anak-cucu di kemudian hari. Oleh karena itu, dalam arsip ini ada bagian-bagian yang berkaitan dengan sejarah hidup dan kegiatan-kegiatan saya, yang seperti halnya banyak orang lainnya juga, penuh dengan lika-liku.

Dalam arsip ini, terdapat sebagian dari tulisan-tulisan, baik yang pernah disiarkan lewat (dan oleh) berbagai saluran, maupun yang belum. Kumpulan tulisan-tulisan ini, yang mengangkat berbagai persoalan, merupakan bagian yang terbesar dan terutama dari arsip ini.

Jadi, website ini bisa merupakan semacam “buku”, yang isinya mengandung aspek-aspek yang bersifat biografi (atau auto-biografi), dan sekaligus juga arsip tulisan dan bahan renungan tentang berbagai persoalan politik, ekonomi, sosial, moral dan kebudayaan dan lain-lain, di Indonesia. Di samping itu, website ini juga merupakan wahana untuk komunikasi dengan berbagai sahabat dan kenalan (atau bahkan juga yang belum kenal) serta masyarakat luas, setiap waktu. Karena “buku” atau arsip ini berbentuk website, maka bisa merupakan bahan bacaan yang hidup, yang selalu berobah terus dengan adanya bahan-bahan baru.

Di samping itu, website ini juga bisa mempunyai fungsi lain. Walaupun dengan cara lain setiap pemakai komputer dapat juga menggunakan Internet untuk mengikuti situasi Indonesia atau mencari bahan-bahan informasi tentang berbagai soal tentang Indonesia, website ini juga bisa dipakai untuk tujuan semacam itu. Untuk itu, disediakan daftar media massa Indonesia (koran, majalah dll), dan berbagai sarana untuk mengadakan hubungan-hubungan (links).

Website ini diluncurkan menjelang umur saya mencapai 74 tahun. Kiranya, bolehlah dikatakan bahwa peluncuran website personal ini adalah sebagai tanda untuk memperingati hari ulang tahun saya, yang jatuh pada tanggal 26 Oktober. Seperti yang bisa disimak dalam CV singkat saya, maupun dalam tulisan tentang perjalanan hidup saya, maka banyak hal yang bisa diceritakan.

Namun, seperti sejarah hidup banyak orang lainnya, maka jelaslah bahwa apa yang tertulis dalam website ini hanyalah sebagian kecil sekali dari apa yang pernah terjadi dalam perjalanan hidup saya. Banyak soal yang bisa diceritakan, namun lebih banyak lagilah kiranya yang tidak bisa atau tidak perlu diceritakan, mengingat berbagai pertimbangan. Manusia adalah mahluk yang kompleks sekali, dan hubungan antara sesama manusia mengandung beraneka-ragam persoalan yang juga kompleks sekali.

Ketika umur makin lanjut, dan hari-hari yang tersisa makin berkurang juga, maka muncullah berbagai pertanyaan dan bahan renungan. Hidup ini untuk apa? Dalam alam semesta yang begitu besar dan rumit ini siapakah kita-kita ini, atau, sebenarnya kita ini apa? Alam semesta sudah berumur ribuan juta tahun, lalu apa artinya kehidupan kita yang kebanyakan hanya berlangsung tidak lebih dari seratus tahun saja? Terlalu banyak persoalan yang bisa direnungkan tentang masalah manusia, masalah di negeri kita, masalah dunia, dan berbagai masalah di semesta alam ini. Sebagian dari berbagai renungan ini dicoba untuk dipertanyakan dalam website ini.

Website ini juga dimaksudkan untuk menambah adanya sarana bagi banyak orang, untuk menyalurkan pendapat, perasaan dan renungan, tentang berbagai persoalan penting yang dihadapi oleh rakyat kita. Tulisan-tulisan yang bertujuan untuk mendorong pelaksanaan demokrasi, membela Hak Asasi Manusia, menganjurkan toleransi, memperkuat persatuan bangsa, mengutamakan kepentingan rakyat banyak, akan dimuat. Segala tulisan atau ungkapan yang dengan jelas menganjurkan kebencian dan permusuhan suku, ras dan agama tidak akan disalurkan lewat Website ini. Sebaliknya, tulisan-tulisan yang bersifat kritis terhadap kejahatan moral, KKN, penyalahgunaan kekuasaan atau pengaruh, pelanggaran hukum dan lain-lain, akan diutamakan.

Sesuai dengan kebutuhan dan juga perkembangan situasi di kemudian hari, website ini selanjutnya masih akan mengalami perubahan-perubahan, baik di bidang penampilannya maupun isinya.

Selamat membaca, dan sampai jumpa di lain kali.
