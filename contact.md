---
layout: page
title: Alamat dan kontakt
---
## Ninon Martis

* Alamat : [15 Allée de la Noiseraie, 93160 Noisy-le-Grand, France](https://www.google.com/maps/place/15+All%C3%A9e+de+la+Noiseraie,+93160+Noisy-le-Grand,+France/@48.83947,2.5756778,17z/data=!3m1!4b1!4m5!3m4!1s0x47e60e3aedb7c9cf:0x699ce1aeaf9fc8a9!8m2!3d48.83947!4d2.5778665?q=15+allee+de+la+noiseraie&um=1&ie=UTF-8&sa=X&ved=0ahUKEwji2c3lzqXVAhVIuhoKHUlHCdcQ_AUICigB)
* Telepon : +33 1 43 05 77 15
* E-mail : [ninon.aumars@gmail.com](mailto:ninon.aumars@gmail.com)

## Benoit Aumars (Budi Setia Nusa)

* E-mail : [setianusa07@gmail.com](mailto:setianusa07@gmail.com)

## Pradana Aumars

* E-mail : [pradana.aumars001@gmail.com](mailto:pradana.aumars001@gmail.com)
